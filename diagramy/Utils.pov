/*###################################################################
### This PovRay document was generated by Inkscape
### http://www.inkscape.org
### Created: Sun Apr 17 18:56:23 2016
### Version: 0.91 r13725
#####################################################################
### NOTES:
### ============
### POVRay information can be found at
### http://www.povray.org
###
### The 'AllShapes' objects at the bottom are provided as a
### preview of how the output would look in a trace.  However,
### the main intent of this file is to provide the individual
### shapes for inclusion in a POV project.
###
### For an example of how to use this file, look at
### share/examples/istest.pov
###
### If you have any problems with this output, please see the
### Inkscape project at http://www.inkscape.org, or visit
### the #inkscape channel on irc.freenode.net . 
###
###################################################################*/


/*###################################################################
##   Exports in this file
##==========================
##    Shapes   : 0
##    Segments : 0
##    Nodes    : 0
###################################################################*/





/*###################################################################
### E N D    F I L E
###################################################################*/


