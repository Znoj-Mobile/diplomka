- [**Description**](#description)
- [**Technology**](#technology)
- [**Year**](#year)
- [**Presentations**](#presentations)
  - [**Enterprise Application Development - school subject**](#enterprise-application-development-school-subject)
  - [**Thesis defense**](#thesis-defense)
- [**Text of diploma work**](#text-of-diploma-work)
  - [**JavaDoc**](#java-doc)
  - [**Source code**](#source-code)
- [**Functionality**](#functionality)
- [**Screenshots**](#screenshots)
  - [Original web page:](#original-web-page)
  - [Login example on multiple resolutions:](#login-example-on-multiple-resolutions)
  - [Phone:](#phone)
  - [Tablet:](#tablet)
- [**Diagrams**](#diagrams)


### **Description**
Diploma work - mobile version of LMS e-learning system.  
The aim of this thesis was to deal with analysis, design and implementation of the system eLogika for tablets with Android operating system version 4.2 or newer and mobile phones with Android operating system in version not older than 4.1. In the introduction it is described the motivation for creating android applications in the system eLogika. The text below introduces this work, focusing on analysis and design of the application, which covers given requirements. The following chapter deals with the design of communication of the application with systém server eLogika. The chapter after that one focuses on all stuff around implementation of the application serving the purpose of teaching subject Mathematic logic on VŠB-TUO. At the end of this thesis it is written the summary of the implemented application and diploma work.

---
### **Technology**
Android

---
### **Year**
2016

---
### **Presentations**  
#### **Enterprise Application Development - school subject**  
[presentation with android animations](/znoj_prezentace_animace.pptx)  
[presentation without animation pdf](/znoj_prezentace_neanimace.pdf)
  
#### **Thesis defense**   
[presentation in pdf](/zno0011%20-%20eLogika%20pro%20Android.pdf)

---
### **Text of diploma work**  
[eLogika for Android System](/DiplomovaPrace_zno0011.pdf)

---
#### **JavaDoc**  
[eLogika for Android System](/Struktura_DVD/Diplomov%C3%A1%20pr%C3%A1ce/JavaDoc/)
![javaDoc](javaDoc.png)

---
#### **Source code**  
[eLogika for Android System](/Struktura_DVD/Diplomov%C3%A1%20pr%C3%A1ce/ProjektAndroidStudia/)
![javaDoc](sourceTree.png)
![tfs](tfs.png)

---
### **Functionality**  
[functionality](/funkcnost.pdf)

---
### **Screenshots**  
#### Original web page:  
![eLogika_web](eLogika_web.png)  
  
#### Login example on multiple resolutions:  
![screen_login_emulated](Screeny/screen_login_emulated.png)  
  
#### Phone:  
![Screenshot_2015-09-20-17-52-17](Screenshot_2015-09-20-17-52-17.png)
![Screenshot_2015-09-20-17-47-15](Screenshot_2015-09-20-17-47-15.png)
![Screenshot_2015-09-20-17-48-22](Screenshot_2015-09-20-17-48-22.png)
![Screenshot_2016-04-26-14-09-40_cz.vsb.elogika](Screeny/Screenshot_2016-04-26-14-09-40_cz.vsb.elogika.png)
![Screenshot_2016-04-26-14-12-38_cz.vsb.elogika](Screeny/Screenshot_2016-04-26-14-12-38_cz.vsb.elogika.png)
![Screenshot_2015-09-20-17-56-04](Screenshot_2015-09-20-17-56-04.png)
![screenNexus4_API16_NahlasitChybu](Screeny/screenNexus4_API16_NahlasitChybu.png)
  
#### Tablet:  
![Screenshot_2016-04-26-13-55-23-848](Screeny/Screenshot_2016-04-26-13-55-23-848.jpeg)
![Screenshot_2015-09-20-17-40-08-810](Screenshot_2015-09-20-17-40-08-810.jpeg)
![Screenshot_2016-04-26-15-02-14-581](Screeny/Screenshot_2016-04-26-15-02-14-581.jpeg)
![Screenshot_refresh](Screeny/Screenshot_refresh.jpeg)
![Screenshot_2016-04-26-14-30-30-398](Screeny/Screenshot_2016-04-26-14-30-30-398.jpeg)
![Screenshot_2015-09-01-09-32-24-119](Screeny/Screenshot_2015-09-01-09-32-24-119.jpeg)
![Screenshot_2016-04-26-14-25-58-189](Screeny/Screenshot_2016-04-26-14-25-58-189.jpeg)

---
### **Diagrams**  
Diagrams are in folder [diagramy](/diagramy/)  

Use case Diagram for a role Student:  
![](/UseCaseStudent.png)  
  
Use case Diagram for a role Garant:  
![](/UseCaseGarant.png)  
  
Use case Diagram for a role Tutor:  
![](/UseCaseTutor.png)  
  
Secvencial diagram for registration for examinations:  
![secventialDiagram_registrationForExaminations](diagramy/secventialDiagram_registrationForExaminations.png)  
  
Activity diagram for login:  
![ActivityDiagram_login](diagramy/ActivityDiagram_login.png)  
  
Factory design pattern:  
![Factory](diagramy/Factory.png)  
  
Utils class:  
![Utils](diagramy/Utils.png)  
