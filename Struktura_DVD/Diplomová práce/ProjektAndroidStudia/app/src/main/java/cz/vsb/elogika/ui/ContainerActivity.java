package cz.vsb.elogika.ui;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import cz.vsb.elogika.R;
import cz.vsb.elogika.app.AppController;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.Informations;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.ui.fragments.Fragment_Activities;
import cz.vsb.elogika.ui.fragments.Fragment_ConsultationHours;
import cz.vsb.elogika.ui.fragments.Fragment_DataMining;
import cz.vsb.elogika.ui.fragments.Fragment_Evaluation;
import cz.vsb.elogika.ui.fragments.Fragment_News;
import cz.vsb.elogika.ui.fragments.Fragment_PublicCourses;
import cz.vsb.elogika.ui.fragments.Fragment_RegistrationForExaminations;
import cz.vsb.elogika.ui.fragments.Fragment_TestDrawn;
import cz.vsb.elogika.ui.fragments.Fragment_Theory;
import cz.vsb.elogika.ui.fragments.Profile;
import cz.vsb.elogika.ui.fragments.Schedule.Fragment_Schedule;
import cz.vsb.elogika.ui.fragments.takeAtest.FragmentTest_Layout;
import cz.vsb.elogika.ui.fragments.takeAtest.Fragment_OnlineTest_list;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.SelectFileToUpload;


public class ContainerActivity extends ActionBarActivity
{

    protected ListView menuList;
    View leftLayout;
    DrawerLayout container;
    public static Activity mainActivity;

    int position;


    public Fragment selectFragment(int position)
    {                //změna layoutu, které musí dědit od třídy fragment probíhá zde ve switchi
        Fragment mFragment = null;
        switch (position - 1)
        {
            case 0:
                mFragment = new Fragment_News();
                break;
            case 1:
                mFragment = Fragment_RegistrationForExaminations.getInstance();
                break;
            case 2:
                mFragment = new Fragment_OnlineTest_list();
                break;
            case 3:
                mFragment = new FragmentTest_Layout();
                break;
            case 4:
                mFragment = new Fragment_Evaluation();
                break;
            case 5:
                mFragment = new Fragment_Activities();
                break;
            case 6:
                mFragment = new Fragment_TestDrawn();
                break;
            case 7:
                mFragment= new Fragment_DataMining();
                break;
            case 8:
                mFragment = new Fragment_Schedule();
                break;
            case 9:
                mFragment = new Fragment_PublicCourses();
                break;
            case 10:
                mFragment = new Fragment_Theory();
                break;
            case 11:
                mFragment = new Fragment_ConsultationHours();
                break;
            case 12:
                //mFragment = new Fragment_Info();
                break;
            case 100:
                mFragment = new Profile();
                break;
            default:
                mFragment = null;
                //Toast.makeText(getLayoutInflater().getContext(), "Not implemented yet!", Toast.LENGTH_SHORT).show();
                SnackbarManager.show(Snackbar.with(getLayoutInflater().getContext()).type(SnackbarType.MULTI_LINE).text("Not implemented yet!"));
                break;
        }
        return mFragment;
    }

    ActionBarDrawerToggle mDrawerToggle;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        getApplicationContext().setTheme(R.style.ElogikaAppTheme);
        setContentView(R.layout.container);
        getBaseContext().setTheme(R.style.ElogikaAppTheme);
        container = (DrawerLayout) findViewById(R.id.container);
        mainActivity = this;
        this.position = 1;
        User.isGarant = false;
        User.isTutor = false;
        final FragmentManager fragmentManager = getFragmentManager();
        //Stranka po prihlaseni a vybrani role
        //Logging.logAccess("/Pages/Default.aspx");


        mDrawerToggle = new ActionBarDrawerToggle(
                this,                    /* host Activity */
                container,                    /* DrawerLayout object */
                R.drawable.ic_drawer,             /* nav drawer image to replace 'Up' caret */
                R.string.navigation_drawer_open,  /* "open drawer" description for accessibility */
                R.string.navigation_drawer_close  /* "close drawer" description for accessibility */
        ) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }
        };
        container.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

        container.setDrawerListener(mDrawerToggle);
        
        
        
        // *** Statické nastavení proměnných, které by se měly nastavovat během přihlašování
        // *** A inicializace Contextu v AppController

        Log.d("User.id", User.id + "");
        Log.d("User.schoolID", User.schoolID + "");
        Log.d("User.roleID", User.roleID + "");
        Log.d("User.courseInfoId", User.courseInfoId + "");
        Log.d("User.schoolInfoID", User.schoolInfoID + "");

        Log.d("Informations.yearIDs", Informations.yearIDs + "");
        Log.d("Informations.years", Informations.years + "");
        Log.d("Informations.courseInfoIds", Informations.courseInfoIds + "");
        Log.d("Informations.courseIds", Informations.courseIds + "");
        Log.d("Informations.courseInfoNames", Informations.courseInfoNames + "");




//        logika.mensikovi
//        User.id = 991;
//        User.schoolID = 1;
//        //student
//        User.roleID = 5;
//        //garant
//        //User.roleID = 3;
//        User.courseInfoId = 76;
//
//        User.yearID = 22;
//
//        Informations.yearIDs.add(24);
//        Informations.yearIDs.add(22);
//
//        Informations.years.add("2014/2015");
//        Informations.years.add("2013/2014");
//
//        Informations.courseInfoIds.add(76);
//        Informations.courseIds.add(49);
//
//        //Informations.courseInfoIds.add(97);
//        //Informations.courseInfoIds.add(98);
//
//        //Informations.courseInfoNames.add("testovaci_kurz_verejny [ rok ]");
//        //Informations.courseInfoNames.add("testovaci [ rok ]");
//
//        Informations.courseInfoNames.add("testovaci [ Zimní ]");
//
//        User.schoolInfoID = 25;


        AppController.setActivity(this);
        // *** Takže nakonec vymazat ***

        menuList = (ListView) findViewById(R.id.left_menu_list);
        menuList.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {

                selectItem(position);
                ContainerActivity.this.position = position;

                try
                {
                    fragmentManager.beginTransaction().replace(R.id.container_middle_frame, selectFragment(position)).commit();
                }
                catch (NullPointerException ex)
                {
                    //   Toast.makeText(getLayoutInflater().getContext(), "Fragment Not Found", Toast.LENGTH_SHORT).show();
                }
            }
        });


        View profileView = getLayoutInflater().inflate(R.layout.menu_profile_item, null);
        ((TextView) profileView.findViewById(R.id.name)).setText(User.degreeBeforeName + " " + User.name + " " + User.surname + " " + User.degreeAfterName);
        ((TextView) profileView.findViewById(R.id.school)).setText(User.school);

        final Spinner school_select = (Spinner) profileView.findViewById(R.id.school_select);
        if (Informations.schools.size() == 0)
        {
            school_select.setVisibility(View.GONE);
            profileView.findViewById(R.id.school_empty).setVisibility(View.VISIBLE);
        }
        final Spinner role_select = (Spinner) profileView.findViewById(R.id.role_select);
        if (Informations.roles.size() == 0)
        {
            role_select.setVisibility(View.GONE);
            profileView.findViewById(R.id.role_empty).setVisibility(View.VISIBLE);
        }
        Spinner years = (Spinner) profileView.findViewById(R.id.years);
        if (Informations.years.size() == 0)
        {
            years.setVisibility(View.GONE);
            profileView.findViewById(R.id.years_empty).setVisibility(View.VISIBLE);
        }
        final Spinner courses = (Spinner) profileView.findViewById(R.id.courses);
        if (Informations.courseInfoNames.size() == 0)
        {
            courses.setVisibility(View.GONE);
            profileView.findViewById(R.id.courses_empty).setVisibility(View.VISIBLE);
        }
        school_select.setAdapter(new ArrayAdapter(this, R.layout.spinner_item, Informations.schools));
        school_select.setSelection(Informations.schoolIds.indexOf(User.schoolID), false);
        role_select.setAdapter(new ArrayAdapter(this, R.layout.spinner_item, Informations.roles));
        role_select.setSelection(Informations.roleIds.indexOf(User.roleID), false);
        years.setAdapter(new ArrayAdapter(this, R.layout.spinner_item, Informations.years));
        years.setSelection(Informations.yearIDs.indexOf(User.yearID), false);
        courses.setAdapter(new ArrayAdapter(this, R.layout.spinner_item, Informations.courseInfoNames));
        courses.setSelection(Informations.courseInfoIds.indexOf(User.courseInfoId), false);

        school_select.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(Informations.schoolIds.indexOf(User.schoolID) == i){
                    return;
                }
                Intent intent = new Intent(ContainerActivity.this, LoginRoleSelectActivity.class);
                intent.putIntegerArrayListExtra("schoolIDs", (ArrayList<Integer>) Informations.schoolIds);
                intent.putIntegerArrayListExtra("roleIDs", (ArrayList<Integer>) Informations.roleIds);
                intent.putStringArrayListExtra("schools", (ArrayList<String>) Informations.schools);
                intent.putStringArrayListExtra("roles", (ArrayList<String>) Informations.roles);
                intent.putExtra("schoolId", Informations.schoolIds.get(i));
                startActivity(intent);
                finish();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        role_select.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
            {
                if(Informations.roleIds.indexOf(User.roleID) == i){
                    return;
                }
                Intent intent = new Intent(ContainerActivity.this, LoginRoleSelectActivity.class);
                intent.putIntegerArrayListExtra("schoolIDs", (ArrayList<Integer>) Informations.schoolIds);
                intent.putIntegerArrayListExtra("roleIDs", (ArrayList<Integer>) Informations.roleIds);
                intent.putStringArrayListExtra("schools", (ArrayList<String>) Informations.schools);
                intent.putStringArrayListExtra("roles", (ArrayList<String>) Informations.roles);
                intent.putExtra("roleId", Informations.roleIds.get(i));
                startActivity(intent);
                finish();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });
        years.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
            {
                User.yearID = Informations.yearIDs.get(i);
                getCourses(courses);
                LoginRoleSelectActivity.getSchoolInfoID();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });

        courses.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                User.courseInfoId = Informations.courseInfoIds.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        menuList.addHeaderView(profileView);

        //seznam položek v levé nabídce
        menuList.setAdapter(new ArrayAdapter(this, android.R.layout.simple_list_item_activated_1, android.R.id.text1, getNames()));

        //TODO - tady to obcas pada kdyz se odhlasim a pak prihlasim...
        //java.lang.RuntimeException: Unable to start activity ComponentInfo{cz.vsb.elogika/cz.vsb.elogika.ui.ContainerActivityGarant}: java.lang.NullPointerException

        fragmentManager.beginTransaction().setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out).replace(R.id.container_middle_frame, selectFragment(position)).commit();
        menuList.setItemChecked(position, true);
        leftLayout = findViewById(R.id.left_menu);
    }

    protected String[] getNames(){
        User.isGarant = false;
        User.isTutor = false;
        return new String[]{
                getString(R.string.news),
                getString(R.string.registration_for_examination),
                getString(R.string.take_a_test),
                getString(R.string.offline_tests),
                getString(R.string.course_evaluation),
                getString(R.string.activities),
                getString(R.string.tests_drawn),
                getString(R.string.data_mining),
                getString(R.string.schedule),
                getString(R.string.public_courses),
                getString(R.string.theory),
                getString(R.string.consultation_Hours)
                //getString(R.string.info
                };
    }


    private void getCourses(final Spinner courses)
    {
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                if (response.length() > 0)
                {
                    courses.setVisibility(View.VISIBLE);
                    findViewById(R.id.courses_empty).setVisibility(View.GONE);
                    User.courseInfoId = Integer.parseInt(response.getJSONObject(0).getString("IDKurzInfo"));
                }
                else
                {
                    courses.setVisibility(View.GONE);
                    findViewById(R.id.courses_empty).setVisibility(View.VISIBLE);
                }

                Informations.courseInfoIds.clear();
                Informations.courseInfoNames.clear();
                for (int j = 0; j < response.length(); j++)
                {
                    Informations.courseInfoIds.add(Integer.parseInt(response.getJSONObject(j).getString("IDKurzInfo")));
                    Informations.courseIds.add(response.getJSONObject(j).getInt("IDKurz"));
                    Informations.courseInfoNames.add(response.getJSONObject(j).getString("KurzSemestr"));
                }

                courses.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_item, Informations.courseInfoNames));
            }
        };
        if(User.isGarant){
            Request.get(URL.Course.CourseByGarant(User.yearID, User.id), arrayResponse);
        }
        else{
            Request.get(URL.Course.CourseByStudent(User.yearID, User.id), arrayResponse);
        }
    }


    public void exit(View view)
    {
        finish();
    }

    public void logOut(View view)
    {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    public void profil(View view)
    {
        this.position = 101;
        getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, new Profile()).commit();
        this.container.closeDrawers();
    }

    public void deleteDatabase(View view)
    {
        BaseActivity.getDB().eraseDB();
        Log.d("Elogika", "databaze vymazana");
    }

    @Override
    public void onBackPressed()
    {
        if (container.isDrawerOpen(leftLayout)) container.closeDrawers();
        else if (Actionbar.isInRoot()) container.openDrawer(leftLayout);
        else Actionbar.jumpBack();
    }
    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        // super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        SelectFileToUpload.selecting.onActivityResult(requestCode,resultCode,data);
    }

    private void selectItem(int position)
    {
        menuList.setItemChecked(position, true);
        container.closeDrawer(leftLayout);
    }
}