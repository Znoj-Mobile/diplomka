package cz.vsb.elogika.ui.fragments.garant.question;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.vsb.elogika.R;
import cz.vsb.elogika.app.AppController;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.dataTypes.Category;
import cz.vsb.elogika.dataTypes.Category_step;
import cz.vsb.elogika.dataTypes.Chapter;
import cz.vsb.elogika.dataTypes.Question;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.TimeLogika;

public class Fragment_Question extends Fragment {

    ArrayAdapter<Chapter> chapteradapter;
    ArrayAdapter<Category> categoryadapter;
    ArrayAdapter<Question> questionAdapter;
    ArrayList<Category_step> steplist = new ArrayList<>();
    ArrayList<Chapter> chapter_list = new ArrayList<>();
    ArrayList<Category> category_list = new ArrayList<Category>();
    ArrayList<Question> questionList = new ArrayList<>();
    int selectedChapterID = -1;
    int selectedChapterPosition;

    int selectedCategoryID = -1;
    int selectedInclusionID = -1;

    Spinner spinner_kapitola;
    Spinner spinner_kategorie;
    Spinner spinner_zarazeni;
    ProgressBar progressBar;
    View nothingToShow;

    CheckBox checkedCheckBox;
    CheckBox notcheckedCheckBox;

    /**
     * Inicializace UI
     */
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_question, container, false);
        Actionbar.newCategory(R.string.questions_managing, this);
        nothingToShow = rootView.findViewById(R.id.question_nothing_to_show);
        progressBar = (ProgressBar) rootView.findViewById(R.id.category_progress);
        initizeAddButton(rootView);

        checkedCheckBox = (CheckBox) rootView.findViewById(R.id.question_checked_checkbox);
        checkedCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!checkedCheckBox.isChecked() && !notcheckedCheckBox.isChecked())
                    notcheckedCheckBox.setChecked(true);
                getQuestionALLFromServer();
            }
        });
        notcheckedCheckBox = (CheckBox) rootView.findViewById(R.id.question_not_checked_checkbox);
        notcheckedCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!notcheckedCheckBox.isChecked() && !checkedCheckBox.isChecked())
                    checkedCheckBox.setChecked(true);
                getQuestionALLFromServer();
            }
        });

        ListView listView = (ListView) rootView.findViewById(R.id.question_listView);
        questionAdapter = new ArrayAdapter<Question>(getActivity(),R.layout.fragment_question_list_item, R.id.question_zadani, questionList) {
            @Override
            public View getView(final int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                ((TextView) view.findViewById(R.id.question_zadani)).setText(questionList.get(position).Zadani);
                ((TextView) view.findViewById(R.id.question_datum_evidence)).setText(questionList.get(position).createdText);
                ((TextView) view.findViewById(R.id.question_datum_kontrola)).setText(questionList.get(position).checkedText);

                if (questionList.get(position).checkedText.equals(""))
                    ((TextView) view.findViewById(R.id.question_datum_kontrola)).setText(R.string.no);
                else
                    ((TextView) view.findViewById(R.id.question_datum_kontrola)).setText(questionList.get(position).checkedText);

                ((TextView) view.findViewById(R.id.question_item_percentage_text)).setText( (questionList.get(position).obtiznost ) + "%");
                view.findViewById(R.id.question_item_percentage_bar).getLayoutParams().width = pxFromDp(getActivity(),questionList.get(position).obtiznost );
                view.findViewById(R.id.question_item_percentage_bar).requestLayout();


                ((ImageButton) view.findViewById(R.id.question_delete_button)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deleteQuestion(position);
                    }
                });

                return view;
            }
        };
        listView.setAdapter(questionAdapter);

      /*  listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, new Fragment_Category_Create().editCategory(chapteradapter, chapter_list, category_list.get(position), selectedChapterPosition)).commit();

            }
        });*/

        return rootView;
    }



    private void initizeAddButton(View rootView) {
        rootView.findViewById(R.id.question_add_new_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, new Fragment_Question_New().setSpinnersData(chapteradapter, chapter_list, selectedChapterPosition, categoryadapter, category_list, 0, 0)).commit();
            }
        });
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        spinner_kapitola = (Spinner) view.findViewById(R.id.question_kapitola_spinner);
        spinner_kategorie = (Spinner) view.findViewById(R.id.question_kategorie_spinner);
        spinner_zarazeni = (Spinner) view.findViewById(R.id.question_zarazeni_spinner);

        initChapterSpinner();
        initCategorySpinner();
        initInclusionSpinner();
        getChaptersFromServer();
    }


    private void initChapterSpinner(){
        chapteradapter = new ArrayAdapter<Chapter>(getActivity(),
                R.layout.spinner_item, chapter_list);
        chapteradapter.setDropDownViewResource(R.layout.spinner_item);
        spinner_kapitola.setAdapter(chapteradapter);
        spinner_kapitola.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                categoryadapter.clear();
                getCategoryFromServer(chapter_list.get(position).IDKapitola);
                selectedChapterPosition = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initCategorySpinner(){
        categoryadapter = new ArrayAdapter<Category>(getActivity(),
                R.layout.spinner_item, category_list);
        categoryadapter.setDropDownViewResource(R.layout.spinner_item);
        spinner_kategorie.setAdapter(categoryadapter);
        spinner_kategorie.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedCategoryID = category_list.get(position).IDKategorie;
                getQuestionALLFromServer();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }




    private void initInclusionSpinner(){
        //TODO connect with EnumLogika
        List<String> list = new ArrayList<String>();
        list.add(AppController.getContext().getResources().getString(R.string.not_by_inclusion));
        list.add(AppController.getContext().getResources().getString(R.string.main));
        list.add(AppController.getContext().getResources().getString(R.string.practise));
        list.add(AppController.getContext().getResources().getString(R.string.example_adj));

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                R.layout.spinner_item, list);
        dataAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinner_zarazeni.setAdapter(dataAdapter);
        spinner_zarazeni.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedInclusionID = position;
                if (selectedInclusionID == 0) selectedInclusionID = -1;
                getQuestionALLFromServer();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    /**
     * metoda pro získání kapitol ze serveru
     */
    private void getChaptersFromServer() {
        progressBar.setVisibility(View.VISIBLE);
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                chapteradapter.clear();
                Log.d("getChaptersFromServer response", response.toString());
                if (response.length() > 0)
                {
                    for(int i = 0;i<response.length();i++)
                    {
                        Chapter c = new Chapter();
                        c.IDKapitola = response.getJSONObject(i).getInt("IDKapitola");
                        c.Nazev = response.getJSONObject(i).getString("Nazev");
                        c.Smazana = response.getJSONObject(i).getBoolean("Smazana");
                        chapter_list.add(c);
                    }
                    // nothinglayout.setVisibility(View.VISIBLE);
                }
                else{
                    nothingToShow.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                    questionAdapter.clear();
                }
                chapteradapter.notifyDataSetChanged();
                // TODO když se vtatím zpět na kategorie tak semi resetne výber
                // spinner.setSelection(selectedChapterPosition);
            }
        };
        //     Log.d("course", "id" + User.courseInfoId);
        Request.get(URL.Chapter.chaptersbycourse(User.courseInfoId), arrayResponse);
    }

    /**
     * metoda pro získání kategorii v zadané kapitole ze serveru
     *
     * @param IDKapitola id kapitoly
     *
     */
    private void getCategoryFromServer( final int IDKapitola) {
        progressBar.setVisibility(View.VISIBLE);
        category_list.clear();
        selectedChapterID = IDKapitola;
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                categoryadapter.clear();

                Category e = new Category();
                e.Nazev = getActivity().getString(R.string.dont_choose_by_category);
                e.IDKategorie = -1;
                category_list.add(e);

                if (response.length() > 0)
                {
                    for(int i = 0;i<response.length();i++)
                    {
                        Category c = new Category();
                        c.IDKategorie = response.getJSONObject(i).getInt("IDKategorie");
                        c.Nazev = response.getJSONObject(i).getString("Nazev");
                        c.Popis = response.getJSONObject(i).getString("Popis");
                        c.Rovnice = response.getJSONObject(i).getString("Rovnice");
                        c.Hodnota = response.getJSONObject(i).getInt("Hodnota");
                        c.Smazana = response.getJSONObject(i).getBoolean("Smazana");
                        c.IDKategoriePuv = response.getJSONObject(i).getInt("IDKategoriePuv");
                        category_list.add(c);

                    }
                    // nothinglayout.setVisibility(View.VISIBLE);
                }
                else{
                    nothingToShow.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                    questionAdapter.clear();
                }
                categoryadapter.notifyDataSetChanged();
            }
        };
        Log.d("idkapitoly", ""+IDKapitola);
        Request.get(URL.Category.getCategories(IDKapitola), arrayResponse);
    }

    private void getQuestionALLFromServer()
    {
        progressBar.setVisibility(View.VISIBLE);
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                questionAdapter.clear();
                Log.d("getQuestionFromServer", response.toString());
                if (response.length() > 0)
                {
                    nothingToShow.setVisibility(View.GONE);
                    for(int i = 0;i<response.length();i++)
                    {
                        Question q = new Question();
                        q.IDOtazka = response.getJSONObject(i).getInt("IDOtazka");
                        q.IDVygenerovanyTest = response.getJSONObject(i).getInt("IDVygenerovanyTest");
                        q.TypOdpovedi = response.getJSONObject(i).getInt("TypOdpovedi");
                        q.IDVygenerovanaOtazka = response.getJSONObject(i).getInt("IDVygenerovanaOtazka");
                        q.IDZarazeni = response.getJSONObject(i).getInt("IDZarazeni");
                        q.IDKategorie = response.getJSONObject(i).getInt("IDKategorie");
                        q.IDUzivatel = response.getJSONObject(i).getInt("IDUzivatel");
                        q.IDRole = response.getJSONObject(i).getInt("IDRole");
                        q.Zadani = response.getJSONObject(i).getString("Zadani");
                        q.PredpCasPrecteni = response.getJSONObject(i).getInt("PredpCasPrecteni");
                        q.Smazana = response.getJSONObject(i).getBoolean("Smazana");
                        q.IDBlok = response.getJSONObject(i).getInt("IDBlok");
                        q.DatumEvidence = response.getJSONObject(i).getString("DatumEvidence");
                        q.IDUzivatelKontrola = response.getJSONObject(i).getInt("IDUzivatelKontrola");
                        q.DatumKontrola = response.getJSONObject(i).getString("DatumKontrola");
                        q.Prekontrolovano = response.getJSONObject(i).getBoolean("Prekontrolovano");
                        q.Vlastnik = response.getJSONObject(i).getBoolean("Vlastnik");
                        q.KategorieNazev = response.getJSONObject(i).getString("KategorieNazev");
                        q.KapitolaNazev = response.getJSONObject(i).getString("KapitolaNazev");
                        q.Variant = response.getJSONObject(i).getInt("Variant");
                        q.obtiznost = (int)(response.getJSONObject(i).getDouble("Obtiznost")*100);
                        q.createdText = response.getJSONObject(i).getString("ZadavatelJmeno") + " " + response.getJSONObject(i).getString("ZadavatelPrijmeni") + " - " + TimeLogika.getFormatedDate(response.getJSONObject(i).getString("DatumEvidence"));
                        q.checkedText = response.getJSONObject(i).getString("PrekontrolovalJmeno") + " " + response.getJSONObject(i).getString("PrekontrolovalPrijmeni") + " - " + TimeLogika.getFormatedDate(response.getJSONObject(i).getString("DatumKontrola"));
                        questionList.add(q);

                    }
                    // nothinglayout.setVisibility(View.VISIBLE);
                }
                else{
                    nothingToShow.setVisibility(View.VISIBLE);
                }
                questionAdapter.notifyDataSetChanged();
                progressBar.setVisibility(View.GONE);
            }
        };
        Request.get(URL.Question.GetQuestionsAllFull(selectedCategoryID, selectedInclusionID, selectedChapterID, checkedCheckBox.isChecked(), notcheckedCheckBox.isChecked(), User.id, true), arrayResponse);
    }

    private void deleteQuestion(final int position) {
        progressBar.setVisibility(View.VISIBLE);
        Response r = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException {
                if(response.getBoolean("Success")) {
                    questionList.remove(position);
                    questionAdapter.notifyDataSetChanged();
                    progressBar.setVisibility(View.GONE);
                }
            }
        };
        Request.get(URL.Question.Remove(questionList.get(position).IDOtazka), r);
    }

    public static int pxFromDp(final Context context, final int dp) {
        return (int) (dp * context.getResources().getDisplayMetrics().density);
    }
}
