package cz.vsb.elogika.utils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Base64;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import cz.vsb.elogika.R;

/**
 * Třída slouží k načtení souborů (různých typů) z uložiště do aplikace
 */
public abstract class SelectFileToUpload{

    Activity activity;
    public static SelectFileToUpload selecting;

    /**
     * Na základě verze androidu vyvolá dialog pro výběr souboru
     * @param activity předání kontextu statické třídě
     */
    public SelectFileToUpload(Activity activity) {
        this.activity = activity;
        selecting = this;
        Intent intent;
        if(Build.VERSION.SDK_INT <19) {
            intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("*/*");
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            try {
                activity.startActivityForResult(Intent.createChooser(intent, activity.getResources().getString(R.string.select_file_to_upload)), 1);
            } catch (android.content.ActivityNotFoundException ex) {
                // Potentially direct the user to the Market with a Dialog
                Toast.makeText(activity, activity.getResources().getString(R.string.please_install_fileManager_first), Toast.LENGTH_SHORT).show();
            }
        }
        else{
            intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.setType("*/*");
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            try {
                activity.startActivityForResult(intent, 0);
            } catch (android.content.ActivityNotFoundException ex) {
                // Potentially direct the user to the Market with a Dialog
                Toast.makeText(activity, activity.getResources().getString(R.string.please_install_fileManager_first), Toast.LENGTH_SHORT).show();
            }
        }
  }

    /**
     *
     * @param requestCode signalizuje, jestli se jedná o api < 19 či nikoli (KitKat a novější)
     * @param resultCode jak dopadl výběr vybraného souboru
     * @param data vybraná data
     */
   // @Override
    @TargetApi(Build.VERSION_CODES.KITKAT)
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == activity.RESULT_OK) {
            Uri uri;
            switch (requestCode) {
                case 1:
                        // Get the Uri of the selected file
                        uri = data.getData();
                        //Toast.makeText(getActivity(), uri.toString(), Toast.LENGTH_SHORT).show();
                        Log.d("choose file onActivityResult", uri.getEncodedPath() + " -> " + uri.toString() + " --> " + uri.getLastPathSegment());

                        functionForManagingFiles(uri, true);

                      //  activity.onActivityResult(requestCode, resultCode, data);

                    break;
                case 0:
                    uri = data.getData();
                    final int takeFlags =
                         (Intent.FLAG_GRANT_READ_URI_PERMISSION
                        | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                    // Check for the freshest data.
                    activity.getContentResolver().takePersistableUriPermission(uri, takeFlags);
                    functionForManagingFiles(uri, false);
                    break;
            }
        }
    }

    /**
     * Abstraktní metoda volající se pro získání jména souboru, typu souboru a samotných dat
     * @param FileName Název souboru
     * @param FileType Typ souboru
     * @param FileinBase64 Data souboru
     */
    public abstract void selectedFile(String FileName, String FileType, String FileinBase64);

    /**
     * Funkce se stará o získání potřebných dat (jména souboru, typu a dat) z uri
     * @param uri cesta k souboru
     * @param isOld příznak značící jestli se jedná o api < 19 či nikoli
     */
    private void functionForManagingFiles(Uri uri, boolean isOld){
        byte[] byteArray;
        String FileName;
        String FileType;

        File f = new File(uri.getPath());
        Log.d("file onActivityResult", "name: " + f.getName() + ", abs. path: " + f.getAbsolutePath() + ", path: " + f.getPath());
        String result;
        Cursor cursor = activity.getContentResolver().query(uri, null, null, null, null);
        // Zdrojem je Dropbox nebo nejaka podobna lokalni cesta
        if (cursor == null) {
            result = uri.getPath();

            FileName = f.getName();
            Log.d("onActivityResult - noCcursor:", FileName);
            String type = null;
            String extension = MimeTypeMap.getFileExtensionFromUrl(f.getPath());
            if (extension != null) {
                MimeTypeMap mime = MimeTypeMap.getSingleton();
                type = mime.getMimeTypeFromExtension(extension);
                if(type == null){
                    type = "text/" + extension;
                }
            }
            FileType = type;
            Log.d("onActivityResult - mimeType:", FileType);
        }
        else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            Log.d("column", cursor.getColumnName(0));
            //snad reseni problemu s kurzorem v androidu KitKat a novějším
            if(idx > -1){
                result = cursor.getString(idx);
            }
            else{
                result = null;
            }

            FileName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
            Log.d("onActivityResult - cursor:", FileName);
            FileType = activity.getContentResolver().getType(uri);
            Log.d("onActivityResult - mimeType:", FileType);

            cursor.close();
        }

        SnackbarManager.show(
                Snackbar.with(activity)
                        .text(FileName + " " + activity.getResources().getString(R.string.is_chosen)));

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        FileInputStream fis;

        //zvladnuti obrazku
        if((FileType.equals("image/jpeg")) || (FileType.equals("image/jpg")) || (FileType.equals("image/png"))) {
            fis = null;
            Bitmap bm = null;
            try {
                if(isOld) {
                    fis = new FileInputStream(new File(result));
                    bm = BitmapFactory.decodeStream(fis);
                    if (fis != null) {
                        try {
                            fis.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                else{
                    InputStream is = activity.getContentResolver().openInputStream(uri);
                    if(is == null){
                        return;
                    }
                    bm = BitmapFactory.decodeStream(is);
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return;
            }


            int widthLimit = 800;
            int heightLimit = 800;
            int width = bm.getWidth();
            int height = bm.getHeight();
            if (width > widthLimit)
            {
                height = widthLimit * height / width;
                width = widthLimit;
            }
            if (height > heightLimit)
            {
                width = heightLimit * width / height;
                height = heightLimit;
            }
            bm = Bitmap.createScaledBitmap(bm, width, height, false);
            baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);

            byteArray = baos.toByteArray();
            try {
                baos.close();
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
        }

        //text
        else{
            try {

                if(isOld) {
                    fis = new FileInputStream(new File(uri.getPath()));
                    byte[] buf = new byte[1024];
                    int n;
                    while (-1 != (n = fis.read(buf)))
                        baos.write(buf, 0, n);
                    if (fis != null) {
                        fis.close();
                    }
                }
                else{
                    InputStream is = activity.getContentResolver().openInputStream(uri);
                    byte[] buf = new byte[1024];
                    int n;
                    while (-1 != (n = is.read(buf)))
                        baos.write(buf, 0, n);
                }

            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
            byteArray = baos.toByteArray();
        }
        Log.d("SelectedFileToUpload", "FileName: " + FileName + ", FileType: " + FileType + ", ByteArray: " + byteArray);
        selectedFile(FileName, FileType, Base64.encodeToString(byteArray, Base64.DEFAULT));
    }
}
