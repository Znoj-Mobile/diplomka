package cz.vsb.elogika.ui.fragments;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Base64;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.Logging;

public class Profile extends Fragment
{
    private JSONObject data;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        Actionbar.newCategory(R.string.profile, this);

        final View rootView = inflater.inflate(R.layout.fragment_profile, container, false);

        final Button roundButton = (Button) rootView.findViewById(R.id.roundButton);
        roundButton.setVisibility(View.GONE);
        roundButton.setText("✎");
        roundButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                ProfileEditation profileEditation = new ProfileEditation();
                Bundle bundle = new Bundle();
                bundle.putString("data", data.toString());
                profileEditation.setArguments(bundle);
                getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, profileEditation).commit();
            }
        });

        loadProfile(rootView, roundButton);

        Logging.logAccess("/Pages/Zakladni/Profil.aspx");

        return rootView;
    }


    /**
     * Načte profil a zobrazí ho.
     * @param rootView inflatnutý celý layout
     * @param roundButton kulaté tlačítko na odeslání formuláře
     */
    private void loadProfile(final View rootView, final View roundButton)
    {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                getActivity().findViewById(R.id.loading_progress).setVisibility(View.GONE);

                data = response;

                ((TextView) rootView.findViewById(R.id.user_name)).setText(response.getString("Jmeno"));
                ((TextView) rootView.findViewById(R.id.user_surname)).setText(response.getString("Prijmeni"));
                ((TextView) rootView.findViewById(R.id.user_birth_surname)).setText(response.getString("PrijmeniRodne"));
                ((TextView) rootView.findViewById(R.id.user_login)).setText(response.getString("Login"));
                ((TextView) rootView.findViewById(R.id.user_email)).setText(response.getString("Email"));
                ((TextView) rootView.findViewById(R.id.user_language)).setText(response.getString("Jazyk"));
                ((TextView) rootView.findViewById(R.id.user_street)).setText(response.getString("Ulice"));
                ((TextView) rootView.findViewById(R.id.user_land_registry_number)).setText(response.getString("CisloPop"));
                ((TextView) rootView.findViewById(R.id.user_postcode)).setText(response.getString("PSC"));
                ((TextView) rootView.findViewById(R.id.user_city)).setText(response.getString("Mesto"));
                ((TextView) rootView.findViewById(R.id.user_degree_in_front_of_name)).setText(response.getString("TitulPred"));
                ((TextView) rootView.findViewById(R.id.user_degree_behind_name)).setText(response.getString("TitulZa"));

                if (response.getString("Fotka").isEmpty())
                {
                    rootView.findViewById(R.id.label_avatar).setVisibility(View.GONE);
                    rootView.findViewById(R.id.avatar).setVisibility(View.GONE);
                }
                else
                {
                    byte[] decodedString = Base64.decode(response.getString("Fotka"), Base64.DEFAULT);
                    Bitmap bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                    Display display = getActivity().getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int width = size.x - (32 * (int) getActivity().getResources().getDisplayMetrics().density);
                    int height = (size.x - (32 * (int) getActivity().getResources().getDisplayMetrics().density)) * bitmap.getHeight() / bitmap.getWidth();
                    if (bitmap.getWidth() < width)
                    {
                        width = bitmap.getWidth();
                        height = bitmap.getHeight();
                    }
                    ((ImageView) rootView.findViewById(R.id.avatar)).setImageBitmap(Bitmap.createScaledBitmap(bitmap, width, height, false));
                }

                getActivity().findViewById(R.id.rootLayout).setVisibility(View.VISIBLE);
                roundButton.setVisibility(View.VISIBLE);
            }
        };
        Request.get(URL.User.Get(User.id), response);
    }
}