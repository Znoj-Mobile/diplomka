package cz.vsb.elogika.ui.fragments.takeAtest;

import android.app.Dialog;
import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ImageSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import cz.vsb.elogika.R;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.LogicParser;
import cz.vsb.elogika.utils.Matematika;
import cz.vsb.elogika.utils.TimeLogika;
import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Fragment pro zobrazení konkretní otázky
 */
public class Taking_a_Test_Fragment extends Fragment {
    public Taking_a_Test_Fragment() { }


    /**
     * metoda oznamující viditelnost otázky uživateli
     *
     * @param isVisibleToUser otázka je ne/viditelná
     */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        //pokud uživatel změnil fragment(další otazka) a něco změnil tak se to uloží na server
        if (!isVisibleToUser && questionAnswersChanged  && questionNumber != -1 && User.isOnline && Taking_a_test.mode != Taking_a_test.TESTDRAWN)
        {
            questionAnswersChanged = false;
            Log.d("saveGenerateTest" , "true" + questionNumber);
                try{
                 //TODO odkomentovat   Taking_a_test.getCurrentInstance.saveGenerateTest(false);
                   //TODO delete  saveGenerateTest();
                } catch (Exception e) {}
        }
    }


    DisplayMetrics dm = new DisplayMetrics();
    int questionNumber = -1;
    boolean questionAnswersChanged = false;
    CheckBox[] checkBoxes;

    /**
     * Inicializace UI
     * zde se napní otázka textem,  text se transformuje ze serveru na text se speciálními znaky,
     * do textu se také přidají obrázkya nastaví se metody pro zvětšování obrázku po kliknutí,
     *
     * jedná-li se o cvičný test pak jsou k jednotlivým otázkam zobrazeny výsledky
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_taking_a_test_question, container, false);
        final LinearLayout layout = (LinearLayout) rootView.findViewById(R.id.question);
        TextView questiontext = (TextView) rootView.findViewById(R.id.taking_a_test_textOtazky);
        final String[] a = getTag().split(":");
        questionNumber = Integer.parseInt(a[a.length-1]);
        if(Taking_a_test.mode == Taking_a_test.TESTDRAWN){
            rootView.findViewById(R.id.testdrawnscore).setVisibility(View.VISIBLE);

            String s = "  " + getString(R.string.unanswered);
            if (Taking_a_test.testDrawnResponse.length() > 0) {
                for (int j = 0; j < Taking_a_test.testDrawnResponse.length(); j++) {
                    try {
                        int foundquestionnumber = -1;
                        for (int find = 0; find < Taking_a_test.testDrawnResponse.length();find++)

                            if(Taking_a_test.jsonQuestions[questionNumber].getInt("IDOtazka") ==
                                    Taking_a_test.testDrawnResponse.getJSONObject(find).getInt("IDOtazka"))
                            {
                                foundquestionnumber = find;
                                        break;
                            }


                        s = "  " + Matematika.zaokrouhleni(Taking_a_test.testDrawnResponse.getJSONObject(foundquestionnumber).getString("BodyOtazka"))
                            + " / " +
                            Matematika.zaokrouhleni(Taking_a_test.testDrawnResponse.getJSONObject(foundquestionnumber).getString("MaxBodyOtazka"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            ((TextView) rootView.findViewById(R.id.testdrawnscoretext)).setText(s);
        }
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm); // měřítko pro obrazky
        try {
            questiontext.setText((Html.fromHtml(LogicParser.StringToFormula(Taking_a_test.jsonQuestions[questionNumber].getJSONObject("Question").getString("Zadani")))));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //obrazky do odpovedi
        SpannableString styledString
                = new SpannableString(questiontext.getText());

        String[] picturelocation = (questiontext.getText()).toString().split("@@");

        int pointer = 0;
        if (picturelocation.length>1)
            for(int p = 0; p<picturelocation.length; p++)
            {
                pointer += picturelocation[p].length();
                if(p%2 == 1)
                {
                    final int picID = Integer.parseInt(picturelocation[p]);
                    int pointerEnd = pointer;
                    pointerEnd += 4;

                    float meritko = Taking_a_test.questionImages[questionNumber][picID-1].getWidth()/Taking_a_test.questionImages[questionNumber][picID-1].getHeight();

                    if (meritko < 1) meritko = 1;
                    float pixelSizeX = 44 * meritko * dm.scaledDensity;
                    float pixelSizeY = 44 * dm.scaledDensity;
                    Bitmap resizedBitmap = Bitmap.createScaledBitmap(Taking_a_test.questionImages[questionNumber][picID-1], (int)pixelSizeX,(int) pixelSizeY, false);

                    styledString.setSpan(new ClickableSpan() {
                        @Override
                        public void onClick(View widget) {

                            final Dialog mydialog = new Dialog(getActivity());
                            mydialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            mydialog.setContentView(getActivity().getLayoutInflater().inflate(R.layout.fragment_taking_a_test_imagedialog, null));
                            ImageView imv = (ImageView) mydialog.findViewById(R.id.taking_a_test_imagedialog);
                            View close = mydialog.findViewById(R.id.taking_a_test_dialog_close);
                            close.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mydialog.cancel();
                                }
                            });
                            imv.setImageBitmap(Taking_a_test.questionImages[questionNumber][picID - 1]);
                            PhotoViewAttacher photoViewAttacher = new PhotoViewAttacher(imv);
                            photoViewAttacher.setScaleType(ImageView.ScaleType.CENTER);
                            photoViewAttacher.setMaxScale(5f);
                            photoViewAttacher.setMinScale(0.1f);
                            mydialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                            lp.copyFrom(mydialog.getWindow().getAttributes());
                            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                            lp.height = WindowManager.LayoutParams.MATCH_PARENT;
                            mydialog.show();
                            mydialog.getWindow().setAttributes(lp);
                        }
                    }, pointer-picturelocation[p].length(), pointerEnd, 0);

                    styledString.setSpan(new ImageSpan(resizedBitmap), pointer-picturelocation[p].length(), pointerEnd, 0);
                    pointer = pointerEnd;
                }
            }
        questiontext.setText(styledString);
        questiontext.setMovementMethod(LinkMovementMethod.getInstance());

        checkBoxes = new CheckBox[Taking_a_test.jsonQuestionOptions[questionNumber].length];
        TextView[] textViews = new TextView[Taking_a_test.jsonQuestionOptions[questionNumber].length];

        for (int i = 0; i<textViews.length; i++)
        {
            View divider = inflater.inflate(R.layout.textdivider,null);
            layout.addView(divider);
            LinearLayout l = new LinearLayout(getActivity());


            try {
                switch (Taking_a_test.jsonQuestions[questionNumber].getInt("AnswerType"))
                {
                    case 0 : checkBoxes[i] = new CheckBox(getActivity()); break;
                    case 1 : checkBoxes[i] = new CheckBox(new ContextThemeWrapper(getActivity(), android.R.style.Widget_CompoundButton_RadioButton), null, 0); break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            textViews[i] = new TextView(getActivity());
            if(Taking_a_test.mode == Taking_a_test.TESTDRAWN)
            {   //Vypracované testy
                for (int findQ = 0; findQ < Taking_a_test.testDrawnQuestionAnswers.length; findQ++)
                for (int findA = 0; findA < Taking_a_test.testDrawnQuestionAnswers[findQ].length(); findA++)
                    try {
                        if (Taking_a_test.jsonQuestionOptions[questionNumber][i].getInt("IDOdpoved") ==  Taking_a_test.testDrawnQuestionAnswers[findQ].getJSONObject(findA).getInt("IDOdpoved"))
                        {
                            checkBoxes[i].setChecked(true);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                checkBoxes[i].setClickable(false);
            }else
                checkBoxes[i].setChecked(Taking_a_test.answers[questionNumber][i]);
            final int answerNumber = i;

            checkBoxes[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean b = !Taking_a_test.answers[questionNumber][answerNumber];
                    questionAnswersChanged = true;
                    Taking_a_test.answers[questionNumber][answerNumber] = b ; //změní hodnotu
                    if(b)
                        Taking_a_test.answersClicks[questionNumber][answerNumber]++; //přicte pokus


                    try {

                        if (Taking_a_test.jsonQuestions[questionNumber].getInt("AnswerType") == 1)
                        {
                            for (int j = 0; j < checkBoxes.length; j++)
                            {
                                if ( j == answerNumber) continue;
                                checkBoxes[j]. setChecked(false);
                                Taking_a_test.answers[questionNumber][j] = false;
                            }
                        }

                        // pro serverovou metodu addlog
                        JSONObject field = new JSONObject();
                        field.put("QuestionId", Taking_a_test.jsonQuestions[questionNumber].getInt("IDOtazka"));
                        field.put("QuestionOrder", questionNumber);
                        field.put("AnswerId", Taking_a_test.jsonQuestionOptions[questionNumber][answerNumber].getInt("IDOdpoved"));
                        field.put("AnswerOrder", answerNumber);
                        field.put("CreatedAt", TimeLogika.currentServerTime());
                        field.put("Checked", b);
                        Taking_a_test.log.put(field);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });

            /*
            checkBoxes[i].setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    //zaznamená zakliknuté otazky
                    questionAnswersChanged = true;
                    Taking_a_test.answers[questionNumber][finalI] = b; //změní hodnotu
                    if(b)
                    Taking_a_test.answersClicks[questionNumber][finalI]++; //přicte pokus

                    try { // pro serverovou metodu addlog
                        JSONObject field = new JSONObject();
                        field.put("QuestionId", Taking_a_test.jsonQuestions[questionNumber].getInt("IDOtazka"));
                        field.put("QuestionOrder", questionNumber);
                        field.put("AnswerId", Taking_a_test.jsonQuestionOptions[questionNumber][finalI].getInt("IDOdpoved"));
                        field.put("AnswerOrder", finalI);
                        field.put("CreatedAt", TimeLogika.currentServerTime());
                        field.put("Checked", b);
                        Taking_a_test.log.put(field);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });

            */

            //text se převede na html tvar a pak se zobrazí i se specialníma znakama
            try {
                textViews[i].setText((Html.fromHtml(LogicParser.StringToFormula(Taking_a_test.jsonQuestionOptions[questionNumber][i].getString("TextOdpovedi")))));
                if(Taking_a_test.mode == Taking_a_test.TESTDRAWN){
                    if (Taking_a_test.jsonQuestionOptions[questionNumber][i].getBoolean("Spravna"))
                        textViews[i].setBackgroundColor(Color.GREEN);
                    else
                        textViews[i].setBackgroundColor(getResources().getColor(R.color.elogika_mandatory));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            textViews[i].setPadding(10, 10, 10, 10);


            //obrazky do odpovedi
            styledString = new SpannableString(textViews[i].getText());

                picturelocation = (textViews[i].getText()).toString().split("@@");

                pointer = 0;
                if (picturelocation.length>1)
                for(int p = 0; p<picturelocation.length; p++)
                {
                    pointer += picturelocation[p].length();
                    if(p%2 == 1)
                    {
                        final int picID = Integer.parseInt(picturelocation[p]);
                        int pointerEnd = pointer;
                        pointerEnd += 4;
                        float meritko = Taking_a_test.answerImages[questionNumber][i][picID-1].getWidth()/Taking_a_test.answerImages[questionNumber][i][picID-1].getHeight();

                        if (meritko < 1) meritko = 1;
                        float pixelSizeX = 44 * meritko * dm.scaledDensity;
                        float pixelSizeY = 44 * dm.scaledDensity;
                        Bitmap resizedBitmap = Bitmap.createScaledBitmap(Taking_a_test.answerImages[questionNumber][i][picID-1], (int)pixelSizeX,(int) pixelSizeY, false);
                        final int answernumber = i;
                        styledString.setSpan(new ClickableSpan() {
                        @Override
                            public void onClick(View widget) {

                            final Dialog mydialog = new Dialog(getActivity());
                            mydialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            mydialog.setContentView(getActivity().getLayoutInflater().inflate(R.layout.fragment_taking_a_test_imagedialog, null));
                            ImageView imv = (ImageView) mydialog.findViewById(R.id.taking_a_test_imagedialog);
                            View close = mydialog.findViewById(R.id.taking_a_test_dialog_close);
                            close.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mydialog.cancel();
                                }
                            });
                            imv.setImageBitmap(Taking_a_test.answerImages[questionNumber][answernumber][picID - 1]);
                            PhotoViewAttacher photoViewAttacher = new PhotoViewAttacher(imv);
                            photoViewAttacher.setScaleType(ImageView.ScaleType.CENTER);
                            photoViewAttacher.setMaximumScale(5f);
                            photoViewAttacher.setMinimumScale(0.1f);
                            mydialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                            lp.copyFrom(mydialog.getWindow().getAttributes());
                            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                            lp.height = WindowManager.LayoutParams.MATCH_PARENT;
                            mydialog.show();
                            mydialog.getWindow().setAttributes(lp);
                            }
                        }, pointer-picturelocation[p].length(), pointerEnd, 0);
                        styledString.setSpan(new ImageSpan(resizedBitmap), pointer-picturelocation[p].length(), pointerEnd, 0);

                        pointer = pointerEnd;
                    }
                }
            textViews[i].setText(styledString);
            textViews[i].setMovementMethod(LinkMovementMethod.getInstance());

            l.addView(checkBoxes[i]);
            l.addView(textViews[i]);
            layout.addView(l);
        }


        if(Taking_a_test.jsonQuestionOptions[questionNumber].length == 0)
        {
            EditText editText = new EditText(getActivity());
            editText.setText(Taking_a_test.writtenAnswers[questionNumber]);
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    Taking_a_test.writtenAnswers[questionNumber] = editable.toString();
                }
            });
            layout.addView(editText);
        }

        return rootView;
    }




    static TextView countdowntext;
    /**
     * Fragment pro poslední položku v seznamu, kde se test odevdává/ukládá
     */
    public static class Taking_a_Test_Fragment_Confirmation extends Fragment {
        public Taking_a_Test_Fragment_Confirmation(){        }

        /**
         * Inicializace UI
         */
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            final View rootView = inflater.inflate(R.layout.fragment_taking_a_test_confirmation, container, false);

            TextView testName = (TextView) rootView.findViewById(R.id.taking_a_test_testname);
            testName.setText(Taking_a_test.testName);
            countdowntext = (TextView) rootView.findViewById(R.id.taking_a_test_countdown_text);
            countdowntext.setText(TimeLogika.getTimeFromMilis(Taking_a_test.millisUntilFinished));

            final Button evaluate = (Button) rootView.findViewById(R.id.evaluateTestButton);
            evaluate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    evaluate.setVisibility(View.GONE);
                    rootView.findViewById(R.id.evaluateTestProgress).setVisibility(View.VISIBLE);
                    Taking_a_test.getCurrentInstance.evaluate();

                }
            });
            return rootView;

        }
    }

}
