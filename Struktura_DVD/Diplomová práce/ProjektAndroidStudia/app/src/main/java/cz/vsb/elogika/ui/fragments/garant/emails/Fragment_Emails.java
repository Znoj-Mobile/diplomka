package cz.vsb.elogika.ui.fragments.garant.emails;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.Informations;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.DividerItemDecoration;
import cz.vsb.elogika.utils.DownloadFile;
import cz.vsb.elogika.utils.Logging;
import cz.vsb.elogika.utils.TimeLogika;

public class Fragment_Emails extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private View mainView;
    private View rootView;
    private RecyclerView recyclerView;

    LinearLayout nothinglayout;
    SwipeRefreshLayout swipeLayout;

    protected RecyclerView.Adapter adapter;
    protected RecyclerView.Adapter adapter_att;

    ArrayList<Map<String, String>> email_list;
    ArrayList<JSONArray> attachment_list;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mainView = inflater.inflate(R.layout.fragment_emails, container, false);
        rootView = mainView.findViewById(R.id.recycler_view_with_button_swipe);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        Logging.logAccess("/Pages/Other/EmailMessages/EmailMessage.aspx");
        final Button roundButton = (Button) rootView.findViewById(R.id.roundButton);
        roundButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment_email_add fragment_email_add = new Fragment_email_add();
                getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment_email_add).commit();
            }
        });

        Actionbar.newCategory(R.string.email_messages, this);
        email_list = new ArrayList<>();
        attachment_list = new ArrayList<>();

        swipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_update);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeResources(R.color.darker_cyan, R.color.material_blue_grey_800);

        nothinglayout = (LinearLayout) rootView.findViewById(R.id.nothing_to_show);
        nothinglayout.setVisibility(View.GONE);


        this.adapter = new RecyclerView.Adapter() {
            class ViewHolderHeader extends RecyclerView.ViewHolder {
                public ViewHolderHeader(View itemView) {
                    super(itemView);
                }
            }

            class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
                public TextView subject;
                public TextView sent;
                public ImageButton state_image;
                public Button state;
                public Button show;
                public Button delete;

                public ViewHolder(View itemView) {
                    super(itemView);
                    itemView.setOnClickListener(this);
                    this.subject = (TextView) itemView.findViewById(R.id.emails_subject);
                    this.sent = (TextView) itemView.findViewById(R.id.emails_sent);
                    this.state_image = (ImageButton) itemView.findViewById(R.id.emails_state_image);
                    this.state = (Button) itemView.findViewById(R.id.emails_state);
                    this.show = (Button)  itemView.findViewById(R.id.emails_show);
                    this.delete = (Button) itemView.findViewById(R.id.emails_delete);
                }


                @Override
                public void onClick(View view) {
                    //final int position = getAdapterPosition();
                }
            }

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view;
                switch (viewType) {
                    case 0:
                        view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_emails_header, parent, false);
                        return new ViewHolderHeader(view);
                    case 1:
                        view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_emails_list, parent, false);
                        return new ViewHolder(view);
                    default:
                        return null;
                }
            }
            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
                if (holder.getItemViewType() == 1) {
                    final ViewHolder viewHolder = (ViewHolder) holder;

                    viewHolder.subject.setText(email_list.get(position).get("subject"));
                    viewHolder.sent.setText(email_list.get(position).get("sentAt"));
                    //todo rest of states
                    if(Integer.valueOf(email_list.get(position).get("state")) == 0){
                        viewHolder.state.setVisibility(View.GONE);
                        viewHolder.state_image.setVisibility(View.VISIBLE);
                        viewHolder.state_image.setImageResource(R.drawable.ic_done_white_24dp);
                    }
                    else if(Integer.valueOf(email_list.get(position).get("state")) == 2){
                        viewHolder.state.setVisibility(View.GONE);
                        viewHolder.state_image.setVisibility(View.VISIBLE);
                        viewHolder.state_image.setImageResource(R.drawable.ic_info_outline_white_24dp);
                    }
                    else{
                        viewHolder.state_image.setVisibility(View.GONE);
                        viewHolder.state.setText(email_list.get(position).get("state"));
                    }
                    final int finalPosition = position;

                    viewHolder.show.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final AppCompatDialog dialog = new AppCompatDialog(getActivity());
                            dialog.setTitle(R.string.email_message);
                            dialog.setContentView(R.layout.email_message_dialog);

                            ((TextView) dialog.findViewById(R.id.email_dialog_subject)).setText(email_list.get(finalPosition).get("subject"));
                            ((TextView) dialog.findViewById(R.id.email_dialog_time)).setText(email_list.get(finalPosition).get("sentAt"));
                            ((TextView) dialog.findViewById(R.id.email_dialog_addressees)).setText(email_list.get(finalPosition).get("receiver"));
                            ((TextView) dialog.findViewById(R.id.email_dialog_text)).setText(email_list.get(finalPosition).get("body"));

                            RecyclerView recyclerViewDialog = (RecyclerView) (dialog.findViewById(R.id.recycler_view)).findViewById(R.id.recyclerView);
                            show_attachments(recyclerViewDialog, finalPosition);

                            Button close = (Button) dialog.findViewById(R.id.email_dialog_close);
                            close.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                }
                            });
                            dialog.show();
                        }
                    });

                    viewHolder.delete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setTitle(R.string.email_delete);
                            builder.setCancelable(false);
                            builder.setMessage(R.string.email_delete_message);
                            builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog2, int which) {
                                    delete(email_list.get(finalPosition).get("id"), email_list.get(finalPosition).get("subject"));
                                    update();
                                }
                            });

                            builder.setNegativeButton(R.string.no, null).setIcon(android.R.drawable.ic_dialog_alert);
                            Dialog d = builder.create();
                            d.show();
                        }
                    });

                    viewHolder.state_image.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogStatus(finalPosition);
                        }
                    });


                }
            }

            @Override
            public int getItemViewType(int position)
            {
                return 1;
            }

            @Override
            public int getItemCount() {
                //velikost listu pro vyplneni tabulky
                return email_list.size();
            }
        };

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        recyclerView.setHasFixedSize(true);
        //zaruci, ze se bude seznam aktualizovat jen pri swipu uplne nahore...
        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy <= 0) {
                    swipeLayout.setEnabled(true);
                } else {
                    swipeLayout.setEnabled(false);
                }
            }
        });
        recyclerView.setAdapter(adapter);

        update();

        return mainView;
    }

    private void show_attachments(RecyclerView recyclerView, final int finalPosition) {

        adapter_att = new RecyclerView.Adapter() {
            class ViewHolderHeader extends RecyclerView.ViewHolder {
                public ViewHolderHeader(View itemView) {
                    super(itemView);
                }
            }

            class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
                public TextView fileName;
                public Button download;

                public ViewHolder(View itemView) {
                    super(itemView);
                    itemView.setOnClickListener(this);
                    this.fileName = (TextView) itemView.findViewById(R.id.emails_att_name);
                    this.download = (Button) itemView.findViewById(R.id.emails_att_delete);
                }

                @Override
                public void onClick(View view) {
                    try {
                        DownloadFile.download(fileName.getText().toString(), attachment_list.get(finalPosition).getJSONObject(getLayoutPosition()).getString("Content"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view;
                switch (viewType) {
                    case 1:
                        view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_email_att_list, parent, false);
                        return new ViewHolder(view);
                    default:
                        return null;
                }
            }

            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
                if (holder.getItemViewType() == 1) {
                    final ViewHolder viewHolder = (ViewHolder) holder;

                    try {
                        viewHolder.fileName.setText(attachment_list.get(finalPosition).getJSONObject(viewHolder.getAdapterPosition()).getString("Name"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    viewHolder.download.setText(getResources().getString(R.string.download));
                    viewHolder.download.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                DownloadFile.download(viewHolder.fileName.getText().toString(), attachment_list.get(finalPosition).getJSONObject(viewHolder.getAdapterPosition()).getString("Content"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }

            @Override
            public int getItemViewType(int position) {
                return 1;
            }

            @Override
            public int getItemCount() {
                //velikost listu pro vyplneni tabulky
                return attachment_list.get(finalPosition).length();
            }
        };

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        recyclerView.setHasFixedSize(true);

        recyclerView.setAdapter(adapter_att);
    }

    private void dialogStatus(int finalPosition) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.status);
        builder.setMessage(email_list.get(finalPosition).get("errorMessage"));
        builder.setPositiveButton(R.string.ok, null);
        //todo rest
        if(Integer.valueOf(email_list.get(finalPosition).get("state")) == 0){
            builder.setIcon(R.drawable.ic_done_white_24dp);
        }
        else if(Integer.valueOf(email_list.get(finalPosition).get("state")) == 2){
            builder.setIcon(R.drawable.ic_info_outline_white_24dp);
        }
        Dialog d = builder.create();
        d.show();
    }


    public void update() {
        swipeLayout.post(new Runnable() {
            @Override
            public void run() {
                nothinglayout.setVisibility(View.GONE);
                swipeLayout.setRefreshing(true);
                email_list.clear();
                attachment_list.clear();
                adapter.notifyDataSetChanged();
                data_into_list();
            }
        });
    }


    private void delete(String id, final String name) {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                if (response.getString("Success").equals("true")) {
                    //novej toast
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.email_message) + " " + name + " " + getActivity().getString(R.string.email_was_deleted)));
                }
                else{ //nepovedlo se
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.email_message) + " " + name + " " + getActivity().getString(R.string.email_was_not_deleted)));
                }
                update();
            }
        };

        //JSONObject parameters = null;
        Request.post(URL.EmailMessage.Remove(id), response);
    }


    private void data_into_list() {
        ArrayResponse response = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                if (response.length() > 0)
                {
                    for (int j = 0; j < response.length(); j++)
                    {
                        email_list.add(put_data(
                                response.getJSONObject(j).getString("Id"),
                                response.getJSONObject(j).getString("Body"),
                                response.getJSONObject(j).getString("Subject"),
                                response.getJSONObject(j).getString("Sender"),
                                response.getJSONObject(j).getString("Receiver"),
                                response.getJSONObject(j).getString("UserId"),
                                response.getJSONObject(j).getString("CourseInfoId"),
                                response.getJSONObject(j).getString("State"),
                                TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("SentAt")),
                                response.getJSONObject(j).getString("ErrorMessage"),
                                response.getJSONObject(j).getJSONArray("Attachments")
                                ));
                    }
                }
                else{
                    Log.d("Chyba", "fragment emails - data_into_list");
                    nothinglayout.setVisibility(View.VISIBLE);
                }
                swipeLayout.setRefreshing(false);
                adapter.notifyDataSetChanged();
            }
        };
        JSONObject parameters = new JSONObject();
        String[] years = Informations.years.get(Informations.yearIDs.indexOf(User.yearID)).split("/");

        try {
            parameters.put("Id", -1);
            parameters.put("UserId", User.id);
            parameters.put("CourseInfoId", User.courseInfoId);
            //TODO FROM - TO day and month?
            parameters.put("From", TimeLogika.getServerTimeFromFormated("01.09." + Integer.valueOf(years[0])));
            parameters.put("To", TimeLogika.getServerTimeFromFormated("31.07." + Integer.valueOf(years[1])));
            parameters.put("Email", User.email);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Request.post(URL.EmailMessage.Messages(), parameters, response);
    }

    private Map<String, String> put_data(String id, String body, String subject, String sender, String receiver, String userId,
                                         String courseInfoId, String state, String sentAt, String errorMessage, JSONArray attachments) {
        HashMap<String, String> item = new HashMap<String, String>();

        item.put("id",id);
        item.put("body",body);
        item.put("subject",subject);
        item.put("sender",sender);
        item.put("receiver",receiver);
        item.put("userId",userId);
        item.put("courseInfoId",courseInfoId);
        item.put("state",state);
        item.put("sentAt",sentAt);
        item.put("errorMessage", errorMessage);

        attachment_list.add(attachments);
        return item;
    }


    @Override
    public void onRefresh() {
        update();
    }
}
