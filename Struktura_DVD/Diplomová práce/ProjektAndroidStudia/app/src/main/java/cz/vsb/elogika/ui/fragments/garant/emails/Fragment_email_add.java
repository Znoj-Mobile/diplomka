package cz.vsb.elogika.ui.fragments.garant.emails;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cz.vsb.elogika.R;
import cz.vsb.elogika.common.view.SlidingTabLayout;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.Logging;
import cz.vsb.elogika.utils.TimeLogika;

public class Fragment_email_add extends Fragment {
    private View rootView;

    static Fragment_email_classes_list fragment_email_classes_list;
    static Fragment_email_message send_message;


    public static LinearLayout nothinglayout;
    public static ArrayList<Map<String, String>> lesson_list;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.tabs_container, container, false);
        Actionbar.addSubcategory(R.string.list_of_classes_and_students, this);
        Logging.logAccess("/Pages/Other/EmailMessages/SendToClass.aspx");

        nothinglayout = (LinearLayout) rootView.findViewById(R.id.nothing_to_show);
        nothinglayout.setVisibility(View.GONE);

        lesson_list = new ArrayList<>();

        ViewPager mViewPager = (ViewPager) rootView.findViewById(R.id.viewpager);
        mViewPager.setAdapter(new FragmentStatePagerAdapter(getFragmentManager()) {
                                  String[] tabNames = new String[]{
                                          getString(R.string.classes_list),
                                          getString(R.string.send_message)};

                                  @Override
                                  public int getCount() {
                                      return tabNames.length;
                                  }

                                  @Override
                                  public CharSequence getPageTitle(int position) {
                                      return tabNames[position];
                                  }

                                  @Override
                                  public Fragment getItem(int position) {

                                      switch (position) {
                                          case 0:
                                              fragment_email_classes_list = new Fragment_email_classes_list() {};
                                              return fragment_email_classes_list;
                                          case 1:
                                              Bundle bundle = new Bundle();
                                              bundle.putBoolean("class", true);
                                              send_message = new Fragment_email_message() {};
                                              send_message.setArguments(bundle);
                                              send_message.set_class();
                                              return send_message;
                                          default:
                                              return null;
                                      }
                                  }
                              }


        );
        SlidingTabLayout mSlidingTabLayout = (SlidingTabLayout) rootView.findViewById(R.id.sliding_tabs);
        mSlidingTabLayout.setViewPager(mViewPager);

        return rootView;
    }

    public static void update(final RecyclerView.Adapter adapter, final SwipeRefreshLayout swipeLayout) {
        swipeLayout.post(new Runnable() {
            @Override
            public void run() {
                nothinglayout.setVisibility(View.GONE);
                swipeLayout.setRefreshing(true);
                lesson_list.clear();
                adapter.notifyDataSetChanged();
                data_into_leasson_list(adapter, swipeLayout);
            }
        });
    }

    private static void data_into_leasson_list(final RecyclerView.Adapter adapter, final SwipeRefreshLayout swipeLayout) {
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                if (response.length() > 0)
                {
                    for (int j = 0; j < response.length(); j++)
                    {
                        lesson_list.add(put_data(
                                response.getJSONObject(j).getString("IDTrida"),
                                response.getJSONObject(j).getString("IDTutor"),
                                response.getJSONObject(j).getString("Nazev"),
                                response.getJSONObject(j).getString("ZkratkaPredmet"),
                                response.getJSONObject(j).getString("Ucebna"),
                                response.getJSONObject(j).getString("Den"),
                                response.getJSONObject(j).getString("HodinaOD"),
                                response.getJSONObject(j).getString("HodinaDO"),
                                response.getJSONObject(j).getString("Tyden"),
                                response.getJSONObject(j).getString("FormaStudia"),
                                response.getJSONObject(j).getString("Typ"),
                                response.getJSONObject(j).getString("Omezeni"),
                                response.getJSONObject(j).getString("Smazana"),
                                TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("Datum")),
                                response.getJSONObject(j).getJSONArray("Dates"),
                                response.getJSONObject(j).getJSONArray("CombinedClasses"),
                                response.getJSONObject(j).getString("TutorName"),
                                response.getJSONObject(j).getString("TutorSurname")));
                    }
                }
                else{
                    Log.d("Chyba", "classes data_into_leasson_list");
                    nothinglayout.setVisibility(View.VISIBLE);
                }
                swipeLayout.setRefreshing(false);
                adapter.notifyDataSetChanged();
            }
        };
        Request.get(URL.SchoolClass.getgarantclasses(User.courseInfoId), arrayResponse);
    }

    private static Map<String, String> put_data(String idTrida, String idTutor, String nazev, String zkratkaPredmet, String ucebna,
                                                String den, String hodinaOD, String hodinaDO, String tyden, String formaStudia, String typ,
                                                String omezeni, String smazana, String datum, JSONArray dates, JSONArray combinedClasses, String tutorName, String tutorSurname) {
        HashMap<String, String> item = new HashMap<>();

        item.put("idTrida",idTrida);
        item.put("idTutor",idTutor);
        item.put("nazev",nazev);
        item.put("zkratkaPredmet",zkratkaPredmet);
        item.put("ucebna",ucebna);
        item.put("den",den);
        item.put("hodinaOD",hodinaOD);
        item.put("hodinaDO", hodinaDO);
        item.put("tyden",tyden);
        item.put("formaStudia",formaStudia);
        item.put("typ",typ);
        item.put("omezeni",omezeni);
        item.put("smazana",smazana);
        item.put("datum",datum);
        if(combinedClasses.length() > 0){
            item.put("combinedClasses","true");
        }
        else{
            item.put("combinedClasses","false");
        }
        item.put("tutorName",tutorName);
        item.put("tutorSurname",tutorSurname);
        //pro potreby aplikace
        item.put("checked","false");

        return item;
    }
}
