package cz.vsb.elogika.ui.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.DividerItemDecoration;
import cz.vsb.elogika.utils.Logging;
import cz.vsb.elogika.utils.TimeLogika;
/**
 * Nejdříve se načítají termíny, na které je student přihlášen,
 * potom se načtou všechny konzultační hodiny. Nakonec se
 * pro každý termín volá metoda, která zjistí, kolik lidí je
 * přihlášeno na daný termín.
*/
public class Fragment_ConsultationHours extends Fragment
{
    private RecyclerView.Adapter adapter;
    private ArrayList<ConsultationHour> consultationHours;
    private class ConsultationHour
    {

        public int id;
        public String description;
        public String author;
        public String dateTime;
        public String formatedDateTime;
        public String place;
        public int spaces;
        public int loggedCount;
        public boolean isLogged;
        public String reason;
    }
    private class LoggedConsultationHour
    {
        public int id;
        public String dateTime;
        public String reason;
    }


    /**
     * Konstruktor
     */
    public Fragment_ConsultationHours()
    {
        this.consultationHours = new ArrayList();
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        Actionbar.newCategory(R.string.consultation_hours, this);

        View rootView = inflater.inflate(R.layout.recycler_view, container, false);
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        rootView.findViewById(R.id.loading_progress).setVisibility(View.VISIBLE);

        // Vytvoření adaptéru
        this.adapter = new RecyclerView.Adapter()
        {
            class ViewHolder extends RecyclerView.ViewHolder
            {
                public CheckBox checkBox;
                public ProgressBar progressBar;
                public TextView description;
                public TextView time;
                public TextView author;
                public TextView place;
                public TextView spaces;

                public ViewHolder(View itemView)
                {
                    super(itemView);
                    this.checkBox = (CheckBox) itemView.findViewById(R.id.checkbox);
                    this.progressBar = (ProgressBar) itemView.findViewById(R.id.checkbox_progress_bar);
                    this.description = (TextView) itemView.findViewById(R.id.description);
                    this.time = (TextView) itemView.findViewById(R.id.time);
                    this.author = (TextView) itemView.findViewById(R.id.author);
                    this.place = (TextView) itemView.findViewById(R.id.place);
                    this.spaces = (TextView) itemView.findViewById(R.id.spaces);
                }
            }

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
            {
                View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_consultation_hours_item, parent, false);
                return new ViewHolder(view);
            }

            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position)
            {
                final ViewHolder holder = (ViewHolder) viewHolder;
                final ConsultationHour consultationHour = consultationHours.get(position);

                // Zobrazení detailu
                holder.itemView.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        Dialog dialog = new Dialog(getActivity());
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.fragment_consultation_hours_detail);

                        ((TextView) dialog.findViewById(R.id.description)).setText(consultationHour.description);
                        ((TextView) dialog.findViewById(R.id.author)).setText(consultationHour.author);
                        ((TextView) dialog.findViewById(R.id.time)).setText(consultationHour.formatedDateTime);
                        ((TextView) dialog.findViewById(R.id.place)).setText(consultationHour.place);
                        ((TextView) dialog.findViewById(R.id.spaces)).setText(String.valueOf(consultationHour.loggedCount + "/" + String.valueOf(consultationHour.spaces)));

                        if (consultationHour.isLogged && !consultationHour.reason.isEmpty())
                        {
                            ((TextView) dialog.findViewById(R.id.reason)).setText(consultationHours.get(position).reason);
                        }
                        else
                        {
                            dialog.findViewById(R.id.labelReason).setVisibility(View.GONE);
                            dialog.findViewById(R.id.reason).setVisibility(View.GONE);
                        }
                        dialog.show();
                    }
                });

                // Nastavení listeneru na checkbox pro přihlášení nebo odhlášení
                holder.checkBox.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        final CheckBox checkbox = (CheckBox) view;
                        checkbox.setChecked(!checkbox.isChecked());

                        if (!checkbox.isChecked()) // přihlášení
                        {
                            AlertDialog.Builder inputDialog = new AlertDialog.Builder(getActivity());
                            inputDialog.setTitle(R.string.consultation_hours_login_reason_title);
                            final EditText input = new EditText(getActivity());
                            inputDialog.setMessage(R.string.consultation_hours_login_reason);
                            inputDialog.setIcon(android.R.drawable.ic_dialog_info);
                            input.setInputType(InputType.TYPE_CLASS_TEXT);
                            inputDialog.setView(input);
                            inputDialog.setPositiveButton(R.string.OK, new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which)
                                {
                                    String reason = input.getText().toString();
                                    holder.progressBar.setLayoutParams(new LinearLayout.LayoutParams(holder.checkBox.getWidth(), holder.checkBox.getHeight()));
                                    holder.checkBox.setVisibility(View.GONE);
                                    holder.progressBar.setVisibility(View.VISIBLE);
                                    login(position, reason);
                                }
                            });
                            inputDialog.setNegativeButton(R.string.cancel, null);
                            inputDialog.show();
                        }
                        else // odhlášení
                        {
                            new AlertDialog.Builder(getActivity()).setTitle(R.string.logout_title).setMessage(R.string.logout).setPositiveButton(R.string.yes, new DialogInterface.OnClickListener()
                            {
                                public void onClick(DialogInterface dialog, int which)
                                {
                                    holder.progressBar.setLayoutParams(new LinearLayout.LayoutParams(holder.checkBox.getWidth(), holder.checkBox.getHeight()));
                                    holder.checkBox.setVisibility(View.GONE);
                                    holder.progressBar.setVisibility(View.VISIBLE);
                                    logout(position);
                                }
                            }).setNegativeButton(R.string.no, null).setIcon(android.R.drawable.ic_dialog_info).show();
                        }
                    }
                });
                if ((!consultationHour.isLogged && consultationHour.spaces <= consultationHour.loggedCount)) holder.checkBox.setEnabled(false);
                else holder.checkBox.setEnabled(true);
                holder.progressBar.setVisibility(View.GONE);
                holder.checkBox.setVisibility(View.VISIBLE);
                holder.checkBox.setChecked(consultationHour.isLogged);
                holder.description.setText(consultationHour.description);
                holder.time.setText(consultationHour.formatedDateTime);
                holder.author.setText(consultationHour.author);
                holder.place.setText(consultationHour.place);
                holder.spaces.setText(consultationHour.loggedCount + "/" + consultationHour.spaces);
            }

            @Override
            public int getItemCount()
            {
                return consultationHours.size();
            }
        };
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        loadLoggedConsultationHours();

        Logging.logAccess("/Pages/Student/StudentConsultationHours.aspx");

        return rootView;
    }



    /**
     * Zjistí, na kterých termínech je student přihlášený.
     * Až poté se volá loadConsultationHours, která zjistí všechny dostupné hodiny.
     */
    private void loadLoggedConsultationHours()
    {
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                ArrayList<LoggedConsultationHour> loggedConsultationHours = new ArrayList();
                LoggedConsultationHour loggedConsultationHour;
                for (int j = 0; j < response.length(); j++)
                {
                    loggedConsultationHour = new LoggedConsultationHour();
                    loggedConsultationHour.id = response.getJSONObject(j).getInt("ConsultationHoursId");
                    loggedConsultationHour.dateTime = response.getJSONObject(j).getString("ExactDate");
                    loggedConsultationHour.reason = response.getJSONObject(j).getString("Reason");
                    loggedConsultationHours.add(loggedConsultationHour);
                }

                loadConsultationHours(loggedConsultationHours);
            }
        };
        Request.get(URL.ConsultationHours.ConsultationHoursWhichIsLogged(User.courseInfoId, User.id), arrayResponse);
    }



    /**
     * Načte všechny konzultační hodiny a zobrazí je v recyclerView.
     * @param loggedConsultationHours pole hodin, na kterých je student přihlášený
     */
    private void loadConsultationHours(final ArrayList<LoggedConsultationHour> loggedConsultationHours)
    {
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                getActivity().findViewById(R.id.loading_progress).setVisibility(View.GONE);

                JSONObject o;
                JSONArray dates;
                ConsultationHour consultationHour;
                for (int j = 0; j < response.length(); j++)
                {
                    o = response.getJSONObject(j);
                    dates = o.getJSONArray("Dates");
                    for (int i = 0; i < dates.length(); i++)
                    {
                        consultationHour = new ConsultationHour();
                        consultationHour.id = o.getInt("Id");
                        consultationHour.dateTime = dates.getJSONObject(i).getString("Date");
                        consultationHour.formatedDateTime = TimeLogika.getFormatedDateTime(dates.getJSONObject(i).getString("Date"));
                        consultationHour.loggedCount = dates.getJSONObject(i).getInt("LoggedCount");
                        consultationHour.description = o.getString("Description");
                        consultationHour.author = o.getString("PersonTitleBefore") + " " + o.getString("PersonName") + " " + o.getString("PersonSurname") + " " + o.getString("PersonTitleAfter");
                        consultationHour.place = o.getString("Place");
                        consultationHour.spaces = o.getInt("MaxStudents");
                        consultationHour.isLogged = false;
                        consultationHours.add(consultationHour);

                        for (int p = 0; p < loggedConsultationHours.size(); p++)
                        {
                            if (loggedConsultationHours.get(p).id == o.getInt("Id") && loggedConsultationHours.get(p).dateTime.equals(consultationHour.dateTime))
                            {
                                consultationHour.isLogged = true;
                                consultationHour.reason = loggedConsultationHours.get(p).reason;
                                break;
                            }
                        }
                    }
                }

                adapter.notifyDataSetChanged();

                // Zobrazení prázdného layoutu
                if (consultationHours.size() == 0)
                {
                    getActivity().findViewById(R.id.nothing_to_show).setVisibility(View.VISIBLE);
                }
            }
        };
        Request.get(URL.ConsultationHours.ConsultationHoursDatesToLogin(User.courseInfoId, User.id), arrayResponse);
    }


    /**
     * Přihlásí studenta na vybranou konzultační hodinu.
     * @param position index v poli consultationHours
     * @param reason důvod přihlášení, který se od uživatele zjistí těsně před přihlášením pomocí dialogového okna
     */
    private void login(final int position, final String reason)
    {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                if (response.getBoolean("Success") == true)
                {
                    consultationHours.get(position).reason = reason;
                    consultationHours.get(position).loggedCount++;
                    consultationHours.get(position).isLogged = true;
                    adapter.notifyItemChanged(position);
                }
                else // termín je plný
                {
                    consultationHours.get(position).loggedCount = consultationHours.get(position).spaces;
                    adapter.notifyItemChanged(position);
                    SnackbarManager.show(Snackbar.with(getActivity()).type(SnackbarType.MULTI_LINE).text(R.string.error_term_is_full));
                }
            }

            @Override
            public void onError(VolleyError error)
            {
                adapter.notifyItemChanged(position);
                super.onError(error);
            }

            @Override
            public void onException(JSONException e)
            {
                adapter.notifyItemChanged(position);
                // TODO Tuto chybu bude možná dobré vypisovat přímo z Response
                SnackbarManager.show(Snackbar.with(getActivity()).text(R.string.error_reports__server_error));
            }
        };
        JSONObject parameters = new JSONObject();
        try
        {
            parameters.put("ConsultationHoursId", this.consultationHours.get(position).id);
            parameters.put("StudentId", User.id);
            parameters.put("ExactDate", this.consultationHours.get(position).dateTime);
            parameters.put("Reason", reason);
        }
        catch (JSONException e) {}
        Request.post(URL.ConsultationHours.login(), parameters, response);
    }



    /**
     * Odhlásí studenta z vybrané konzultační hodiny
     * @param position index v poli consultationHours
     */
    private void logout(final int position)
    {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                if (response.getBoolean("Success") == true)
                {
                    consultationHours.get(position).loggedCount--;
                    consultationHours.get(position).isLogged = false;
                    adapter.notifyItemChanged(position);
                }
                else onException(null);
            }

            @Override
            public void onError(VolleyError error)
            {
                adapter.notifyItemChanged(position);
                super.onError(error);
            }

            @Override
            public void onException(JSONException e)
            {
                adapter.notifyItemChanged(position);
                // TODO Tuto chybu bude možná dobré vypisovat přímo z Response
                SnackbarManager.show(Snackbar.with(getActivity()).text(R.string.error_reports__server_error));
            }
        };
        JSONObject parameters = new JSONObject();
        try
        {
            parameters.put("ConsultationHoursId", this.consultationHours.get(position).id);
            parameters.put("StudentId", User.id);
            parameters.put("ExactDate", this.consultationHours.get(position).dateTime);
        }
        catch (JSONException e) {}
        Request.post(URL.ConsultationHours.logout(), parameters, response);
    }
}