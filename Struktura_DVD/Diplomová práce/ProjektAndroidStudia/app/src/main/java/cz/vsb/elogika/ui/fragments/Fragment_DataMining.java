package cz.vsb.elogika.ui.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.Informations;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.DividerItemDecoration;
import cz.vsb.elogika.utils.Logging;

public class Fragment_DataMining extends Fragment {
    private View rootView;
    private RecyclerView recyclerView;
    LinearLayout nothinglayout;
    ProgressBar loading_progress;
    ArrayList<Map<String, String>> list = new ArrayList<>();
    private RecyclerView.Adapter adapter;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Actionbar.newCategory(R.string.data_mining, this);
        Logging.logAccess("/Pages/Other/Advices.aspx");
        rootView = inflater.inflate(R.layout.recycler_view, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);


        nothinglayout = (LinearLayout) rootView.findViewById(R.id.nothing_to_show);
        loading_progress = (ProgressBar) rootView.findViewById(R.id.loading_progress);

        this.adapter = new RecyclerView.Adapter(){

            class ViewHolderHeader extends RecyclerView.ViewHolder
            {
                public ViewHolderHeader(View itemView)
                {
                    super(itemView);
                }
            }

            class ViewHolder extends RecyclerView.ViewHolder
            {
                public TextView category;
                public TextView average;
                public TextView average_year;
                public TextView average_all;

                public ViewHolder(View itemView)
                {
                    super(itemView);
                    this.category = (TextView) itemView.findViewById(R.id.dm_category);
                    this.average = (TextView) itemView.findViewById(R.id.dm_average);
                    this.average_year = (TextView) itemView.findViewById(R.id.dm_average_year);
                    this.average_all = (TextView) itemView.findViewById(R.id.dm_average_all);
                }
            }

            class ViewHolderFooter extends RecyclerView.ViewHolder
            {
                public LinearLayout category_description1;
                public TextView categories1;
                public LinearLayout category_description2;
                public TextView categories2;
                public LinearLayout category_description3;
                public TextView categories3;
                public LinearLayout category_description4;
                public TextView categories4;
                public LinearLayout category_description5;
                public TextView categories5;

                public ViewHolderFooter(View itemView)
                {
                    super(itemView);
                    this.category_description1 = (LinearLayout) itemView.findViewById(R.id.dm_category_description1);
                    this.categories1 = (TextView) itemView.findViewById(R.id.dm_categories1);
                    this.category_description2 = (LinearLayout) itemView.findViewById(R.id.dm_category_description2);
                    this.categories2 = (TextView) itemView.findViewById(R.id.dm_categories2);
                    this.category_description3 = (LinearLayout) itemView.findViewById(R.id.dm_category_description3);
                    this.categories3 = (TextView) itemView.findViewById(R.id.dm_categories3);
                    this.category_description4 = (LinearLayout) itemView.findViewById(R.id.dm_category_description4);
                    this.categories4 = (TextView) itemView.findViewById(R.id.dm_categories4);
                    this.category_description5 = (LinearLayout) itemView.findViewById(R.id.dm_category_description5);
                    this.categories5 = (TextView) itemView.findViewById(R.id.dm_categories5);
                }
            }

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view;
                switch (viewType) {
                    case 0:
                        view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_data_mining_list_header, parent, false);
                        return new ViewHolderHeader(view);
                    case 1:
                        view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_data_mining_list, parent, false);
                        return new ViewHolder(view);
                    case 2:
                        view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_data_mining_footer, parent, false);
                        return new ViewHolderFooter(view);
                    default:
                        return null;
                }
            }

            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
                if (holder.getItemViewType() == 1)
                {
                    //kvuli hlavicce
                    position--;
                    final ViewHolder viewHolder = (ViewHolder) holder;

                    viewHolder.category.setText(list.get(position).get("name"));
                    viewHolder.average.setText(String.format("%.2f", Double.valueOf(list.get(position).get("correct"))*100));
                    viewHolder.average_year.setText(String.format("%.2f", Double.valueOf(list.get(position).get("correctYear")) * 100));
                    viewHolder.average_all.setText(String.format("%.2f", Double.valueOf(list.get(position).get("correctAll")) * 100));
                }
                else if(holder.getItemViewType() == 2){
                    String p00_25 = "";
                    String p25_50 = "";
                    String p50_75 = "";
                    String p75_95 = "";
                    String p100 = "";

                    for(Map<String, String> i : list){
                        if (Double.valueOf(i.get("correct")) < 0) {

                        }
                        else if (Double.valueOf(i.get("correct").replace(",", ".")) <= 0.25) {
                            p00_25 += i.get("name") + ", ";
                        }
                        else if (Double.valueOf(i.get("correct").replace(",", ".")) <= 0.50) {
                            p25_50 += i.get("name") + ", ";
                        }
                        else if (Double.valueOf(i.get("correct").replace(",", ".")) <= 0.75) {
                            p50_75 += i.get("name") + ", ";
                        }
                        else if (Double.valueOf(i.get("correct").replace(",", ".")) <= 0.95) {
                            p75_95 += i.get("name") + ", ";
                        }
                        else if (Double.valueOf(i.get("correct").replace(",", ".")) <= 1) {
                            p100 += i.get("name") + ", ";
                        }
                    }

                    final ViewHolderFooter viewHolder = (ViewHolderFooter) holder;
                    if (p00_25.length() > 1) {
                        viewHolder.category_description1.setVisibility(View.VISIBLE);
                        viewHolder.categories1.setText(p00_25.substring(0, p00_25.length()-2));
                    }
                    if (p25_50.length() > 1) {
                        viewHolder.category_description2.setVisibility(View.VISIBLE);
                        viewHolder.categories2.setText(p25_50.substring(0, p25_50.length()-2));
                    }
                    if (p50_75.length() > 1) {
                        viewHolder.category_description3.setVisibility(View.VISIBLE);
                        viewHolder.categories3.setText(p50_75.substring(0, p50_75.length()-2));
                    }
                    if (p75_95.length() > 1) {
                        viewHolder.category_description4.setVisibility(View.VISIBLE);
                        viewHolder.categories4.setText(p75_95.substring(0, p75_95.length()-2));
                    }
                    if (p100.length() > 1) {
                        viewHolder.category_description5.setVisibility(View.VISIBLE);
                        viewHolder.categories5.setText(p100.substring(0, p100.length()-2));
                    }
                }
            }

            @Override
            public int getItemViewType(int position)
            {
                //hlavicka tabulky
                if (position == 0)
                    return 0;
                //text pod tabulkou
                else if(position > list.size())
                    return 2;
                //tabulka
                else
                    return 1;
            }

            @Override
            public int getItemCount() {
                //velikost listu pro vyplneni tabulky + 1 pro text pod tabulkou + 1 je pro nadpis tabulky
                return list.size() + 2;
            }
        };
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        data_into_list();

        return rootView;
    }

    private void data_into_list() {
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                Log.d("data_into_list_dataMining", response.toString());
                list.clear();
                if (response.length() > 0)
                {
                    for (int j = 0; j < response.length(); j++)
                    {
                        Log.d("Chyba", response.toString());

                        list.add(put_data(
                                response.getJSONObject(j).getString("id").replace(",", "."),
                                response.getJSONObject(j).getString("name").replace(",", "."),
                                response.getJSONObject(j).getString("correct").replace(",", "."),
                                response.getJSONObject(j).getString("correctYear").replace(",", "."),
                                response.getJSONObject(j).getString("correctYear").replace(",", ".")));
                    }
                    nothinglayout.setVisibility(View.GONE);
                }
                else{
                    Log.d("Chyba", "Fragment_DataMining data_into_list");
                    nothinglayout.setVisibility(View.VISIBLE);
                }
                loading_progress.setVisibility(View.GONE);
                adapter.notifyDataSetChanged();
            }
        };
        int index = Informations.courseInfoIds.indexOf(User.courseInfoId);
        int index2 = Informations.courseIds.indexOf(0);
        int indexYear = Informations.yearIDs.indexOf(User.yearID);
        //todo vypocet courseId musi probehnout jinak... Otestovat
        //todo nyni datamining nefunguje ani na webu - neni dostupny. Kermaschek o tom nevi nic
        Request.get(URL.Datamining.Statistics(User.id, Informations.courseIds.get(index), Informations.years.get(indexYear)), arrayResponse);
    }

    private Map<String, String> put_data(String id, String name, String correct, String correctYear, String correctAll) {
        HashMap<String, String> item = new HashMap<String, String>();
        item.put("id", id);
        item.put("name", name);
        item.put("correct", correct);
        item.put("correctYear", correctYear);
        item.put("correctAll", correctAll);
        return item;
    }
}
