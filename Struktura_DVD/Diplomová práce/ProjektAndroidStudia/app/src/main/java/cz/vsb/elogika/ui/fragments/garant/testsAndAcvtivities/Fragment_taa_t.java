package cz.vsb.elogika.ui.fragments.garant.testsAndAcvtivities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.ui.fragments.garant.templateAdministration.Fragment_TemplateAdministration;
import cz.vsb.elogika.ui.fragments.garant.courseConditions.Fragment_cc_add_term;
import cz.vsb.elogika.ui.fragments.garant.courseConditions.Fragment_cc_term;
import cz.vsb.elogika.utils.DividerItemDecoration;
import cz.vsb.elogika.utils.EnumLogika;
import cz.vsb.elogika.utils.TimeLogika;

public class Fragment_taa_t extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private View mainView;
    private View rootView;
    private RecyclerView recyclerView;

    LinearLayout nothinglayout;
    SwipeRefreshLayout swipeLayout;

    ArrayList<Map<String, String>> tests_list;
    protected RecyclerView.Adapter adapter;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_taa_t, container, false);
        rootView = mainView.findViewById(R.id.recycler_view_with_button_swipe);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);

        swipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_update);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeResources(R.color.darker_cyan, R.color.material_blue_grey_800);

        nothinglayout = (LinearLayout) rootView.findViewById(R.id.nothing_to_show);
        nothinglayout.setVisibility(View.GONE);

        tests_list = new ArrayList<Map<String,String>>();

        this.adapter = new RecyclerView.Adapter() {
            class ViewHolderHeader extends RecyclerView.ViewHolder {
                public ViewHolderHeader(View itemView) {
                    super(itemView);
                }
            }

            class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
                public TextView title;
                public TextView putting_in;
                public TextView min_max;
                public TextView mandatory;
                public Button template;
                public CheckBox checkbox;
                //public Button btn;

                public ViewHolder(View itemView) {
                    super(itemView);
                    itemView.setOnClickListener(this);
                    /*this.btn = (Button) itemView.findViewById(R.id.taa_a_button);*/
                    this.title = (TextView) itemView.findViewById(R.id.taa_t_title);
                    this.putting_in = (TextView) itemView.findViewById(R.id.taa_t_putting_in);
                    this.min_max = (TextView) itemView.findViewById(R.id.taa_t_min_max);
                    this.mandatory = (TextView) itemView.findViewById(R.id.taa_t_mandatory);
                    this.template = (Button) itemView.findViewById(R.id.taa_t_button);
                    this.checkbox = (CheckBox) itemView.findViewById(R.id.taa_a_checkbox);
                }


                @Override
                public void onClick(View view) {
                    final int position = getAdapterPosition();
                    //Toast.makeText(getActivity(), course_conditions_list.get(position).toString(), Toast.LENGTH_SHORT).show();
                    final Dialog dialog = new Dialog(getActivity());
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.fragment_taa_a_detail);
                    Button btn_term_list = (Button) dialog.findViewById(R.id.btn_taa_a_term_list);
                    btn_term_list.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            toFragment_cc_term(tests_list.get(position).get("idTest"), tests_list.get(position).get("nazev"));
                            dialog.dismiss();
                        }
                    });

                    Button btn_taa_a_add_term = (Button) dialog.findViewById(R.id.btn_taa_a_add_term);
                    btn_taa_a_add_term.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            toFragment_cc_add_term(tests_list.get(position).get("idTest"), tests_list.get(position).get("nazev"));
                            dialog.dismiss();
                        }
                    });

                    Button btn_taa_a_solvers = (Button) dialog.findViewById(R.id.btn_taa_a_solvers);
                    btn_taa_a_solvers.setVisibility(View.GONE);
                    Button btn_taa_a_generated_tests = (Button) dialog.findViewById(R.id.btn_taa_a_generated_tests);
                    btn_taa_a_generated_tests.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            toFragment_taa_generated_tests(tests_list.get(position), tests_list.get(position).get("nazev"));
                            dialog.dismiss();
                        }
                    });

                    Button btn_taa_a_edit = (Button) dialog.findViewById(R.id.btn_taa_a_edit);
                    btn_taa_a_edit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            toFragment_cc_edit(tests_list.get(position));
                            dialog.dismiss();
                        }
                    });

                    Button btn_taa_a_delete = (Button) dialog.findViewById(R.id.btn_taa_a_delete);
                    btn_taa_a_delete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setTitle(R.string.test_delete);
                            builder.setCancelable(false);
                            builder.setMessage(R.string.test_delete_message);
                            builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog2, int which) {
                                    delete(tests_list.get(position));
                                    dialog.dismiss();
                                }
                            });

                            builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog2, int which) {
                                    ;
                                }
                            }).setIcon(android.R.drawable.ic_dialog_alert);
                            Dialog d = builder.create();
                            d.show();
                        }
                    });
                    dialog.show();
                }


            }

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view;
                switch (viewType) {
                    case 0:
                        view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_taa_test_list_header, parent, false);
                        return new ViewHolderHeader(view);
                    case 1:
                        view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_taa_test_list, parent, false);
                        return new ViewHolder(view);
                    default:
                        return null;
                }
            }
            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
                if (holder.getItemViewType() == 1) {
                    final ViewHolder viewHolder = (ViewHolder) holder;
                    viewHolder.title.setText(tests_list.get(position).get("nazev"));
                    viewHolder.putting_in.setText(EnumLogika.zarazeni(tests_list.get(position).get("idZarazeni")));
                    viewHolder.min_max.setText(tests_list.get(position).get("minBody") + " / " + tests_list.get(position).get("maxBody"));
                    if(tests_list.get(position).get("povinny").equals("true")){
                        viewHolder.mandatory.setText(getString(R.string.yes));
                    }
                    else{
                        viewHolder.mandatory.setText(getString(R.string.no));
                    }
                    viewHolder.template.setText(tests_list.get(position).get("sablona"));

                    final int finalPosition = position;
                    viewHolder.template.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Bundle bundle = new Bundle();
                            bundle.putInt("idSablona", Integer.valueOf(tests_list.get(finalPosition).get("idSablona")));
                            Fragment_TemplateAdministration fragment_templateAdministration = new Fragment_TemplateAdministration();
                            fragment_templateAdministration.setArguments(bundle);
                            getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment_templateAdministration).commit();
                        }
                    });

                    //objekt neni v seznamu k exportu → nezaskrtly checkbox
                    if(Fragment_TestsAndActivity.export_list.indexOf(tests_list.get(finalPosition)) == -1){
                        viewHolder.checkbox.setChecked(false);
                    }
                    else{
                        viewHolder.checkbox.setChecked(true);
                    }
                    viewHolder.checkbox.setOnClickListener(new View.OnClickListener() {
                                                               @Override
                                                               public void onClick(View v) {
                                                                   if(viewHolder.checkbox.isChecked()){
                                                                       //pridat k exportu
                                                                       Fragment_TestsAndActivity.export_list.add(tests_list.get(finalPosition));
                                                                   }
                                                                   else{
                                                                       //odebrat z exportu
                                                                       Fragment_TestsAndActivity.export_list.remove(tests_list.get(finalPosition));
                                                                   }
                                                               }
                                                           }
                    );
                }
            }

            @Override
            public int getItemViewType(int position)
            {
                    return 1;
            }

            @Override
            public int getItemCount() {
                return tests_list.size();
            }
        };

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        recyclerView.setHasFixedSize(true);
        //zaruci, ze se bude seznam aktualizovat jen pri swipu uplne nahore...
        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy <= 0) {
                    swipeLayout.setEnabled(true);
                } else {
                    swipeLayout.setEnabled(false);
                }
            }
        });
        recyclerView.setAdapter(adapter);

        if(Integer.valueOf(Fragment_TestsAndActivity.groupActivityId) != 0) {
            update();
        }

        Button roundButton = (Button) rootView.findViewById(R.id.roundButton);
        roundButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment_taa_add_test fragment_taa_add_t = new Fragment_taa_add_test();
                getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment_taa_add_t).commit();
            }
        });
        return mainView;
    }


    public void update() {
        swipeLayout.post(new Runnable() {
            @Override
            public void run() {
                nothinglayout.setVisibility(View.GONE);
                swipeLayout.setRefreshing(true);
                tests_list.clear();
                adapter.notifyDataSetChanged();
                get_tests();
            }
        });
    }

    public void get_tests() {

        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                if (response.length() > 0)
                {
                    for (int j = 0; j < response.length(); j++)
                    {
                        tests_list.add(put_data_t(
                                response.getJSONObject(j).getString("IDTest"),
                                TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("OdevzdaniDO")),
                                response.getJSONObject(j).getString("PocetPokusu"),
                                response.getJSONObject(j).getString("IDSablona"),
                                response.getJSONObject(j).getString("IDZarazeni"),
                                response.getJSONObject(j).getString("Nazev"),
                                response.getJSONObject(j).getString("Cas"),
                                response.getJSONObject(j).getString("MaxBody"),
                                response.getJSONObject(j).getString("MinBody"),
                                response.getJSONObject(j).getString("RozsahIP"),
                                response.getJSONObject(j).getString("ZobrazHotovTest"),
                                response.getJSONObject(j).getString("OdeslaniVysl"),
                                response.getJSONObject(j).getString("PapirovyTest"),
                                response.getJSONObject(j).getString("Offline"),
                                response.getJSONObject(j).getString("Povinny"),
                                response.getJSONObject(j).getString("Smazany"),
                                response.getJSONObject(j).getString("IDTermin"),
                                response.getJSONObject(j).getString("Info4Test"),
                                response.getJSONObject(j).getString("NazevTermin"),
                                TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("AktivniOD")),
                                TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("AktivniDO")),
                                response.getJSONObject(j).getString("NazevSA"),
                                response.getJSONObject(j).getString("IsEditable")));
                    }
                }
                else{
                    Log.d("Chyba", "CourseConditions get_tests");
                    nothinglayout.setVisibility(View.VISIBLE);
                }
                swipeLayout.setRefreshing(false);
                adapter.notifyDataSetChanged();
            }
        };

        Request.get(URL.Test.GetTests(Fragment_TestsAndActivity.groupActivityId, User.id, User.roleID), arrayResponse);
    }

    private Map<String, String> put_data_t(String idTest, String odevzdaniDo, String pocetPokusu, String idSablona, String idZarazeni, String nazev, String cas, String maxBody,
                                           String minBody, String rozsahIP, String zobrazHotovTest, String odeslaniVysl, String papirovyTest, String offline, String povinny,
                                           String smazany, String idTermin, String info4Test, String nazevTermin, String aktivniOD, String aktivniDO, String nazevSA, String isEditable) {

        HashMap<String, String> item = new HashMap<String, String>();

        item.put("idTest", idTest);
        item.put("odevzdaniDo", odevzdaniDo);
        item.put("pocetPokusu", pocetPokusu);
        item.put("idSablona", idSablona);
        item.put("idZarazeni", idZarazeni);
        item.put("nazev", nazev);
        item.put("cas", cas);
        item.put("maxBody", maxBody);
        item.put("minBody", minBody);
        item.put("rozsahIP", rozsahIP);
        item.put("zobrazHotovTest", zobrazHotovTest);
        item.put("odeslaniVysl", odeslaniVysl);
        item.put("papirovyTest", papirovyTest);
        item.put("offline", offline);
        item.put("povinny", povinny);
        item.put("smazany", smazany);
        item.put("idTermin", idTermin);
        item.put("info4Test", info4Test);
        item.put("nazevTermin", nazevTermin);
        item.put("aktivniOD", aktivniOD);
        item.put("aktivniDO", aktivniDO);
        item.put("nazevSA", nazevSA);
        item.put("isEditable", isEditable);
        //jen abych vedel za tohle se tam dava taky, ale v get_templates()
        item.put("sablona", Fragment_TestsAndActivity.add_template_name_into_test(idSablona));

        return item;
    }

    private void toFragment_cc_term(String idTest, String nazev) {
        Bundle bundle = new Bundle();
        bundle.putString("idTest", idTest);
        bundle.putString("aktivitaNazev", nazev);
        Fragment_cc_term fragment_cc_term = new Fragment_cc_term();
        fragment_cc_term.setArguments(bundle);
        getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment_cc_term).commit();
    }

    private void toFragment_cc_add_term(String idTest, String nazev) {
        Bundle bundle = new Bundle();
        bundle.putString("idTest", idTest);
        bundle.putString("aktivitaNazev", nazev);
        Fragment_cc_add_term fragment_cc_add_term = new Fragment_cc_add_term();
        fragment_cc_add_term.setArguments(bundle);
        getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment_cc_add_term).commit();
    }

    private void toFragment_taa_generated_tests(Map<String, String> list, String nameTest) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("testList", (Serializable) list);
        bundle.putString("nazevSA", Fragment_TestsAndActivity.nazevSA);
        bundle.putString("nameTest", nameTest);
        Fragment_taa_generated_tests fragment_taa_generated_tests = new Fragment_taa_generated_tests();
        fragment_taa_generated_tests.setArguments(bundle);
        getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment_taa_generated_tests).commit();
    }

    private void toFragment_cc_edit(Map<String, String> list) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("list", (Serializable) list);
        Fragment_taa_add_test fragment_taa_add_t = new Fragment_taa_add_test();
        fragment_taa_add_t.setArguments(bundle);
        getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment_taa_add_t).commit();
    }



    private void delete(final Map<String, String> list) {
        Response response = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException {
                Log.d("typ response: ", response.getString("Success"));
                if (response.getString("Success").equals("true")) {
                    //novej toast
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .type(SnackbarType.MULTI_LINE)
                                    .text(getActivity().getString(R.string.test) + " " + list.get("nazev") + " " + getActivity().getString(R.string.test_was_deleted)));

                } else { //nepovedlo se
                    Log.d("Fragment_taa_t", "delete(): " + response.toString());
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .type(SnackbarType.MULTI_LINE)
                                    .text(getActivity().getString(R.string.test) + " " + list.get("nazev") + " " + getActivity().getString(R.string.test_was_not_deleted)));
                }
                update();
            }

        };
        Request.post(URL.Test.Remove(list.get("idTest")), response);
    }

    @Override
    public void onRefresh() {
        update();
    }
}
