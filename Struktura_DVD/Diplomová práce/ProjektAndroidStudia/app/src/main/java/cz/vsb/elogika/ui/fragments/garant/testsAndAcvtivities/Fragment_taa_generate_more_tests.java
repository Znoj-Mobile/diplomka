package cz.vsb.elogika.ui.fragments.garant.testsAndAcvtivities;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.DividerItemDecoration;
import cz.vsb.elogika.utils.Logging;
import cz.vsb.elogika.utils.TimeLogika;

public class Fragment_taa_generate_more_tests extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private View mainView;
    private View rootView;
    private RecyclerView recyclerView;

    LinearLayout nothinglayout;
    SwipeRefreshLayout swipeLayout;

    protected RecyclerView.Adapter adapter;

    private String idTest;
    private String idTermin;
    Map<String, String> testList;
    ArrayList<Map<String, String>> students_list;

    JSONArray avaiableTestsJSONArray;
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_taa_generate_more_tests, container, false);
        rootView = (View) mainView.findViewById(R.id.recycler_view_swipe);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);

        swipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_update);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeResources(R.color.darker_cyan, R.color.material_blue_grey_800);

        try {
            testList = (Map<String, String>) getArguments().getSerializable("testList");
            idTest = getArguments().getString("idTest", null);
            idTermin = getArguments().getString("idTermin", null);
        } catch (Exception e){   }

        Actionbar.addSubcategory(R.string.generate_more_tests, this);
        Logging.logAccess("/Pages/Other/TestDetail/GeneratedTestAdvanced.aspx");

        nothinglayout = (LinearLayout) rootView.findViewById(R.id.nothing_to_show);
        nothinglayout.setVisibility(View.GONE);

        students_list = new ArrayList<>();
        Button generate = (Button) mainView.findViewById(R.id.taa_gmt_generate);
        generate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generate_more();
            }
        });
        final LinearLayout ll_students = (LinearLayout) mainView.findViewById(R.id.taa_gmt_ll_students);
        ll_students.setVisibility(View.GONE);
        CheckBox checkbox_generate_fl = (CheckBox) mainView.findViewById(R.id.taa_gmt_generate_for_logged);
        checkbox_generate_fl.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    ll_students.setVisibility(View.VISIBLE);
                }
                else{
                    ll_students.setVisibility(View.GONE);
                }
            }
        });

        getStudentsIntoList();

        this.adapter = new RecyclerView.Adapter() {
            class ViewHolderHeader extends RecyclerView.ViewHolder {
                public ViewHolderHeader(View itemView) {
                    super(itemView);
                }
            }

            class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
                public CheckBox checkbox;
                public TextView login_date;
                public TextView surname;
                public TextView name;
                public TextView system_login;
                public TextView school_login;
                public TextView try_;

                public ViewHolder(View itemView) {
                    super(itemView);
                    itemView.setOnClickListener(this);
                    this.checkbox = (CheckBox) itemView.findViewById(R.id.taa_gmt_checkbox);
                    this.login_date = (TextView) itemView.findViewById(R.id.taa_gmt_login_date);
                    this.surname = (TextView) itemView.findViewById(R.id.taa_gmt_surname);
                    this.name = (TextView) itemView.findViewById(R.id.taa_gmt_name);
                    this.system_login = (TextView) itemView.findViewById(R.id.taa_gmt_system_login);
                    this.school_login = (TextView) itemView.findViewById(R.id.taa_gmt_school_login);
                    this.try_ = (TextView) itemView.findViewById(R.id.taa_gmt_try);
                }


                @Override
                public void onClick(View view) {
                    checkbox.setChecked(!checkbox.isChecked());
                }


            }

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view;
                switch (viewType) {
                    case 0:
                        view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_taa_generate_more_tests_list_header, parent, false);
                        return new ViewHolderHeader(view);
                    case 1:
                        view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_taa_generate_more_tests_list, parent, false);
                        return new ViewHolder(view);
                    default:
                        return null;
                }
            }
            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
                if (holder.getItemViewType() == 1) {
                    final ViewHolder viewHolder = (ViewHolder) holder;

                    final int finalPosition = position;
                    if(students_list.get(finalPosition).get("checked").equals("true")){
                        viewHolder.checkbox.setChecked(true);
                    }
                    else{
                        viewHolder.checkbox.setChecked(false);
                    }
                    viewHolder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            if(viewHolder.checkbox.isChecked()){
                                students_list.get(finalPosition).put("checked", "true");
                            }
                            else{
                                students_list.get(finalPosition).put("checked", "false");
                            }
                        }
                    });

                    viewHolder.login_date.setText(students_list.get(position).get("datumPrihlaseni"));
                    viewHolder.surname.setText(students_list.get(position).get("prijmeni"));
                    viewHolder.name.setText(students_list.get(position).get("jmeno"));
                    viewHolder.system_login.setText(students_list.get(position).get("systemLogin"));
                    viewHolder.school_login.setText(students_list.get(position).get("loginSkola"));
                    viewHolder.try_.setText(students_list.get(position).get("pokus"));
                }
            }

            @Override
            public int getItemViewType(int position)
            {
                /*
                //hlavicka tabulky
                if (position == 0)
                    return 0;
                    //tabulka
                else
                */
                return 1;
            }

            @Override
            public int getItemCount() {
                return students_list.size();
            }
        };

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        recyclerView.setHasFixedSize(true);
        //zaruci, ze se bude seznam aktualizovat jen pri swipu uplne nahore...
        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy <= 0) {
                    swipeLayout.setEnabled(true);
                } else {
                    swipeLayout.setEnabled(false);
                }
            }
        });
        recyclerView.setAdapter(adapter);
        update();

        return mainView;
    }

    private void getStudentsIntoList() {
        ArrayResponse arrayResponse = new ArrayResponse()
    {
        @Override
        public void onResponse(JSONArray response) throws JSONException
        {
            if (response.length() > 0)
            {
                for (int j = 0; j < response.length(); j++)
                {
                    students_list.add(put_data(
                            response.getJSONObject(j).getString("TitulPred"),
                            response.getJSONObject(j).getString("TitulZa"),
                            response.getJSONObject(j).getString("Email"),
                            response.getJSONObject(j).getString("IDUzivatel"),
                            response.getJSONObject(j).getString("SystemLogin"),
                            response.getJSONObject(j).getString("Jmeno"),
                            response.getJSONObject(j).getString("Prijmeni"),
                            response.getJSONObject(j).getString("LoginSkola"),
                            response.getJSONObject(j).getString("Vysledek"),
                            response.getJSONObject(j).getString("Splnil"),
                            TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("DatumPrihlaseni")),
                            response.getJSONObject(j).getString("Pokus")));
                }
            }
            else{
                Log.d("Chyba", "Fragment_taa_generate_more_tests getStudentsIntoList");
                nothinglayout.setVisibility(View.VISIBLE);
            }
            swipeLayout.setRefreshing(false);
            adapter.notifyDataSetChanged();
        }
    };
    //todo - really 0??? Count students
    Request.get(URL.Student.GetStudentSummaryByIDTermin(idTermin, User.schoolInfoID, "0"), arrayResponse);
}

    private Map<String, String> put_data(String titulPred, String titulZa, String email, String idUzivatel, String systemLogin,
                                         String jmeno, String prijmeni, String loginSkola, String vysledek, String splnil,
                                         String datumPrihlaseni, String pokus) {
        HashMap<String, String> item = new HashMap<String, String>();

        item.put("titulPred", titulPred);
        item.put("titulZa", titulZa);
        item.put("email", email);
        item.put("idUzivatel", idUzivatel);
        item.put("systemLogin", systemLogin);
        item.put("jmeno", jmeno);
        item.put("prijmeni", prijmeni);
        item.put("loginSkola", loginSkola);
        item.put("vysledek", vysledek);
        item.put("splnil", splnil);
        item.put("datumPrihlaseni", datumPrihlaseni);
        item.put("pokus", pokus);

        item.put("checked", "false");

        return item;
    }

    public void update() {
        swipeLayout.post(new Runnable() {
            @Override
            public void run() {
                nothinglayout.setVisibility(View.GONE);
                swipeLayout.setRefreshing(true);
                students_list.clear();
                adapter.notifyDataSetChanged();
            }
        });
    }

    private HashMap<String, String> put_data(String idTermin, String idSkupinaAktivit, String idTest, String idAktivita, String nazev, String ucebna, String nazevTyp,
                                             String aktivniOD, String aktivniDO, String prihlaseniDO, String idAutor, String datumPrihlaseni, String datumOdhlaseni,
                                             String nutnePrihlaseni, String pocetStudentu, String smazany, String pocetZapsanych, String aktivitaNazev, String pocetPokusu, String zapsan) {
        HashMap<String, String> item = new HashMap<String, String>();

        item.put("idTermin",idTermin);
        item.put("idSkupinaAktivit",idSkupinaAktivit);
        item.put("idTest",idTest);
        item.put("idAktivita",idAktivita);
        item.put("nazev",nazev);
        item.put("ucebna",ucebna);
        item.put("nazevTyp",nazevTyp);
        item.put("aktivniOD",aktivniOD);
        item.put("aktivniDO",aktivniDO);
        item.put("prihlaseniDO",prihlaseniDO);
        item.put("idAutor", idAutor);
        item.put("datumPrihlaseni",datumPrihlaseni);
        item.put("datumOdhlaseni",datumOdhlaseni);
        item.put("nutnePrihlaseni",nutnePrihlaseni);
        item.put("pocetStudentu",pocetStudentu);
        item.put("smazany",smazany);
        item.put("pocetZapsanych",pocetZapsanych);
        item.put("aktivitaNazev",aktivitaNazev);
        item.put("pocetPokusu",pocetPokusu);
        item.put("zapsan", zapsan);

        return item;

    }



    private void getAvaiableTest(final int testID, final int cas)
    { //dostanu seznam dostupných testů
        ArrayResponse res = new ArrayResponse() {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {

                if (adapter != null) adapter.notifyDataSetChanged();
                avaiableTestsJSONArray = response;
                if (response.length() > 0) {
                    for (int j = 0; j < response.length(); j++) {
                        String remaintogenerate = "0";
                        try{
                            remaintogenerate= response.getJSONObject(j).getString("RemainToGenerate");
                        }
                        catch (JSONException e){}

                        addItem(response.getJSONObject(j).getInt("TestId"),
                                response.getJSONObject(j).getString("Name"),
                                response.getJSONObject(j).getString("GroupActivityName"),
                                response.getJSONObject(j).getString("Min"),
                                response.getJSONObject(j).getString("Max"),
                                response.getJSONObject(j).getString("DateAvailableFrom"),
                                response.getJSONObject(j).getString("DateAvailableTo"),
                                // response.getJSONObject(j).getString("Attempts"),
                                remaintogenerate,
                                response.getJSONObject(j).getBoolean("Required"),
                                response.getJSONObject(j).getInt("TimeMinutes")
                        );
                    }
                }

                else{ nothinglayout.setVisibility(View.VISIBLE);}
                if (testID !=-1)
                    generate_more();
            }
        };
        Request.get(URL.GeneratedTest.getAvaiableTests(User.id, User.courseInfoId), res);
    }

    private void addItem(int testID, String name, String activitiesGroup, String minPoints, String maxPoints,
                         String activeFrom, String activeTo, String trialsRemaining, boolean mandatory, int timeMinutes) {

        HashMap<String, String> item = new HashMap<String, String>();
        item.put("name", name);
        item.put("activitiesGroup", activitiesGroup);
        item.put("minPoints", minPoints);
        item.put("maxPoints", maxPoints);
        item.put("active", TimeLogika.getFormatedDateTime(activeFrom) + " - " + TimeLogika.getFormatedDateTime(activeTo));
        item.put("trialsRemaining", trialsRemaining);
        if (mandatory)
            item.put("mandatory", "T" + testID);
        else item.put("mandatory", "F" + testID);
        item.put("timeMinutes", String.valueOf(timeMinutes));
//        this.itemList.add(item);
        if (this.adapter != null) this.adapter.notifyDataSetChanged();
    }


    //TODO
    private void generate_more() {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                /*
                progressPercentage.setText(1 + " %");
                */
                Log.d("generate test response", response.toString());
                int GeneratedTestId = response.getJSONObject("Result").getInt("GeneratedTestId");
                Log.d("generate test", "žádám o test generatedTestId " +GeneratedTestId);

                Log.w("generatedTest", response.toString());
            }

            @Override
            public void onError(VolleyError error) {
                Toast.makeText(getActivity(), "Chyba v testu,", Toast.LENGTH_LONG).show();
                //myDialog.setCancelable(true);
            }
        };
        JSONObject parameters = new JSONObject();
        try
        {
            JSONObject object = new JSONObject();

            object.put("TestId", testList.get("idTest"));
            //object.put("Attempts" ,testList.get("pocetPokusu"));
            object.put("TemplateId" ,testList.get("idSablona"));
            object.put("InclusionId" ,testList.get("idZarazeni"));
            object.put("Name", testList.get("nazev"));
            object.put("TimeMinutes", testList.get("cas"));
            object.put("Max", testList.get("maxBody"));
            object.put("Min", testList.get("minBody"));
            object.put("IPRange", testList.get("rozsahIP"));
            object.put("ShowResult", testList.get("zobrazHotovTest"));
            object.put("CanSendResult", testList.get("odeslaniVysl"));
            object.put("PaperTest", testList.get("papirovyTest"));
            object.put("Required", testList.get("povinny"));
            object.put("Deleted", testList.get("smazany"));
            object.put("DateId", testList.get("idTermin"));
            object.put("DateName", testList.get("nazevTermin"));
            object.put("DateAvailableFrom", TimeLogika.getServerTimeFromFormated(testList.get("aktivniOD")));
            object.put("DateAvailableTo", TimeLogika.getServerTimeFromFormated(testList.get("aktivniDO")));
            object.put("DateSendTo", TimeLogika.getServerTimeFromFormated(testList.get("odevzdaniDo")));
            object.put("GroupActivityName", testList.get("nazevSA"));
            object.put("isEditable", testList.get("isEditable"));
            object.put("Offline", testList.get("offline"));
            if(testList.get("pocetPokusu").length() == 0){
                object.put("RemainToGenerate", "0");
            }
            else{
                object.put("RemainToGenerate", testList.get("pocetPokusu"));
            }

            object.put("info4Test", testList.get("info4Test"));

            parameters.put("Test", object);
            // parameters
            parameters.put("StartAt", TimeLogika.currentServerTime() );
            parameters.put("IPAddress", URL.getLocalIpAddress());
            parameters.put("StudentId", User.id);
            parameters.put("CourseInfoId", User.courseInfoId);
        } catch (JSONException e) {Log.d("elogika","exception at fragment_taa_generated_tests" + e.toString());}

        Log.d("generate test request",parameters.toString());
        Request.post(URL.GeneratedTest.generateTest(), parameters, response);
    }

    @Override
    public void onRefresh() {
        update();
    }
}
