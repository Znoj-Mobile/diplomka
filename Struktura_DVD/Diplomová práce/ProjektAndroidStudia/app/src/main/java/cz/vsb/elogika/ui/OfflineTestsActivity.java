package cz.vsb.elogika.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import cz.vsb.elogika.R;
import cz.vsb.elogika.ui.fragments.takeAtest.FragmentTest_Layout;

public class OfflineTestsActivity extends ActionBarActivity
{
    /**
     * Nastaví UI pro fragmenty
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.offline_tests);
        getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, new FragmentTest_Layout()).commit();
    }
    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        // super.onSaveInstanceState(outState);
    }

    /**
     * Při kliknuté na tlačítko zpět se zavře aktuální aktivita a přejde se zpátky na login obrazovku
     */
    @Override
    public void onBackPressed(){
        Intent intent = new Intent(OfflineTestsActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

}
