package cz.vsb.elogika.model;

public class User
{
    public final static int GARANT = 3;
    public final static int TUTOR = 4;
    public final static int STUDENT = 5;

    public static int id;
    public static String name;
    public static String surname;
    public static String degreeBeforeName;
    public static String degreeAfterName;
    public static String email;
    public static String loginSchool;

    public static int schoolID;
    public static int roleID;
    public static int yearID;
    public static int courseInfoId;
    public static boolean isOnline;
    public static int schoolInfoID;
    public static String school;
    public static String role;


    public static boolean isGarant = false;
    public static boolean isTutor = false;
}
