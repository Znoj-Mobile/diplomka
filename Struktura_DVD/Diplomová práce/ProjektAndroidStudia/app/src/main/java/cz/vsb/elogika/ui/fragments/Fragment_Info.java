package cz.vsb.elogika.ui.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.Informations;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.ui.LoginRoleSelectActivity;

public class Fragment_Info extends Fragment
{
    private Spinner years;
    private Spinner semesters;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        //getActivity().getActionBar().setTitle(R.string.info);

        View rootView = inflater.inflate(R.layout.fragment_info, container, false);

        final TextView nameSurname = (TextView) rootView.findViewById(R.id.name_surname);
        final TextView school = (TextView) rootView.findViewById(R.id.school);
        this.years = (Spinner) rootView.findViewById(R.id.years);
        this.semesters = (Spinner) rootView.findViewById(R.id.semesters);

        nameSurname.setText(User.name + " " + User.surname);
        school.setText(User.school);

        this.years.setAdapter(new ArrayAdapter(getActivity(), R.layout.spinner_item, Informations.years));
        this.semesters.setAdapter(new ArrayAdapter(getActivity(), R.layout.spinner_item, Informations.courseInfoNames));

        this.years.setSelection(Informations.yearIDs.indexOf(User.yearID), false);
        this.semesters.setSelection(Informations.courseInfoIds.indexOf(User.courseInfoId), false);

        this.years.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
            {
                User.yearID = Informations.yearIDs.get(i);
                getCourses();
                LoginRoleSelectActivity.getSchoolInfoID();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });

        this.semesters.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
            {
                User.courseInfoId = Informations.courseInfoIds.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });

        return rootView;
    }



    private void getCourses()
    {
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                if (response.length() > 0)
                {
                    User.courseInfoId = Integer.parseInt(response.getJSONObject(0).getString("IDKurzInfo"));
                }
                else ; // chyba

                Informations.courseInfoIds.clear();
                Informations.courseInfoNames.clear();
                for (int j = 0; j < response.length(); j++)
                {
                    Informations.courseInfoIds.add(Integer.parseInt(response.getJSONObject(j).getString("IDKurzInfo")));
                    Informations.courseIds.add(response.getJSONObject(j).getInt("IDKurz"));
                    Informations.courseInfoNames.add(response.getJSONObject(j).getString("KurzSemestr"));
                }

                semesters.setAdapter(new ArrayAdapter(getActivity(), R.layout.spinner_item, Informations.courseInfoNames));

                // Změna i v menu
                ((Spinner) getActivity().findViewById(R.id.semesters)).setAdapter(new ArrayAdapter(getActivity(), R.layout.spinner_item, Informations.courseInfoNames));
            }
        };
        Request.get(URL.Course.CourseByStudent(User.yearID, User.id), arrayResponse);
    }
}
