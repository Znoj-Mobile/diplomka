package cz.vsb.elogika.ui.fragments.garant.templateAdministration;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.dataTypes.Template;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.DividerItemDecoration;

public class Fragment_TemplateAdministration extends Fragment
{
    private RecyclerView.Adapter adapter;
    private View loadingProgress;
    private View nothingToShow;
    private ArrayList<Template> templates;
    private int templateId;


    public Fragment_TemplateAdministration()
    {
        this.templates = new ArrayList();
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Pro zobrazeni sablon ze Spravy testu a aktivit
        Bundle arguments = getArguments();
        if (arguments == null) this.templateId = 0;
        else this.templateId = arguments.getInt("idSablona", 0);

        if(this.templateId == 0) Actionbar.newCategory(R.string.template_administration, this);
        else Actionbar.addSubcategory(R.string.template_administration, this);

        final View rootView = inflater.inflate(R.layout.recycler_view_with_button, container, false);
        ((ViewGroup) rootView.findViewById(R.id.recyclerViewLayout)).addView(LayoutInflater.from(getActivity()).inflate(R.layout.fragment_template_administration_header, container, false), 0);
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        this.loadingProgress = rootView.findViewById(R.id.loading_progress);
        this.nothingToShow = rootView.findViewById(R.id.nothing_to_show);
        this.loadingProgress.setVisibility(View.VISIBLE);

        rootView.findViewById(R.id.roundButton).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, new Fragment_TemplateAdministrationCreationAndModification()).commit();
            }
        });


        // Vytvoření adaptéru
        this.adapter = new RecyclerView.Adapter()
        {
            class ViewHolder extends RecyclerView.ViewHolder
            {
                public TextView name;
                public TextView blockMix;
                public TextView description;
                public TextView blockCount;
                public Button edit;
                public Button delete;

                public ViewHolder(View itemView)
                {
                    super(itemView);
                    this.name = (TextView) itemView.findViewById(R.id.name);
                    this.blockMix = (TextView) itemView.findViewById(R.id.blockMix);
                    this.description = (TextView) itemView.findViewById(R.id.description);
                    this.blockCount = (TextView) itemView.findViewById(R.id.blockCount);
                    this.edit = (Button) itemView.findViewById(R.id.edit);
                    this.delete = (Button) itemView.findViewById(R.id.delete);
                }
            }

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
            {
                return new ViewHolder(LayoutInflater.from(getActivity()).inflate(R.layout.fragment_template_administration_item, parent, false));
            }

            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position)
            {
                final ViewHolder holder = (ViewHolder) viewHolder;
                final Template template = templates.get(position);

                holder.name.setText(template.name);
                holder.blockMix.setText(template.blockMix ? R.string.yes : R.string.no);
                holder.description.setText(template.description);
                holder.blockCount.setText(String.valueOf(template.blockCount));

                // Nastavení listenerů na tlačítka
                holder.edit.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        int position = holder.getAdapterPosition();
                        Bundle bundle = new Bundle();
                        bundle.putInt("id", templates.get(position).id);
                        bundle.putString("name", templates.get(position).name);
                        bundle.putString("description", templates.get(position).description);
                        bundle.putInt("blockCount", templates.get(position).blockCount);
                        bundle.putBoolean("allMix", templates.get(position).allMix);
                        bundle.putBoolean("blockMix", templates.get(position).blockMix);
                        Fragment_TemplateAdministrationCreationAndModification fragment_templateAdministrationCreationAndModification = new Fragment_TemplateAdministrationCreationAndModification();
                        fragment_templateAdministrationCreationAndModification.setArguments(bundle);
                        getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment_templateAdministrationCreationAndModification).commit();
                    }
                });

                holder.delete.findViewById(R.id.delete).setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        new AlertDialog.Builder(getActivity()).setTitle(R.string.are_you_sure).setMessage(R.string.template_deletion_question).setPositiveButton(R.string.yes, new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int which)
                            {
                                removeTemplate(holder.getAdapterPosition());
                            }
                        }).setNegativeButton(R.string.no, null).setIcon(android.R.drawable.ic_dialog_alert).show();
                    }
                });
            }

            @Override
            public int getItemCount()
            {
                return templates.size();
            }
        };
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        loadTemplates();

        return rootView;
    }



    private void loadTemplates()
    {
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                loadingProgress.setVisibility(View.GONE);

                JSONObject o;
                Template template;

                for (int j = 0; j < response.length(); j++)
                {
                    o = response.getJSONObject(j);
                    template = new Template();
                    template.id = o.getInt("IDSablona");
                    template.name = o.getString("Nazev");
                    template.description = o.getString("Popis");
                    template.blockCount = o.getInt("PocetBloku");
                    template.blockMix = o.getBoolean("MichaniBloku");
                    template.allMix = o.getBoolean("MichatCele");

                    templates.add(template);
                }
                adapter.notifyDataSetChanged();

                // Zobrazení prázdného layoutu
                if (templates.size() == 0)
                {
                    nothingToShow.setVisibility(View.VISIBLE);
                }
            }
        };

        Request.get(URL.Template.GetTemplatesWithBlocks(User.courseInfoId, User.id, 1), arrayResponse);
    }



    /**
     * Vymaže šablonu.
     * @param position index v poli templates
     */
    private void removeTemplate(final int position)
    {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                templates.remove(position);
                adapter.notifyItemRemoved(position);
                SnackbarManager.show(Snackbar.with(getActivity()).type(SnackbarType.MULTI_LINE).text(R.string.template_deleted));
            }
        };
        Request.post(URL.Template.Remove(this.templates.get(position).id), response);
    }
}
