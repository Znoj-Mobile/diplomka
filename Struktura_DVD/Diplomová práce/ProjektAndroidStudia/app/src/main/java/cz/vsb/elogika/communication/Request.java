package cz.vsb.elogika.communication;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.vsb.elogika.app.AppController;
import cz.vsb.elogika.ui.ContainerActivity;

public class Request
{
    private static com.android.volley.RequestQueue requestQueue;



    /**
     * Pošle GET žádost na server a odpověď se zpracuje dle implementace v Response.
     * Odpověď serveru musí být ve tvaru JSONObject.
     * @param url URL adresa
     * @param response Objekt typu Response s implementovanou metodou onResponse, případně překrytými metodami onError a onException
     */
    public static void get(String url, final Response response)
    {
        addRequest(com.android.volley.Request.Method.GET, url, null, response);
    }


    /**
     * Pošle GET žádost na server a odpověď se zpracuje dle implementace v ArrayResponse.
     * Odpověď serveru musí být ve tvaru JSONArray.
     * @param url URL adresa
     * @param response Objekt typu ArrayResponse s implementovanou metodou onResponse, případně překrytými metodami onError a onException
     */
    public static void get(String url, final ArrayResponse response)
    {
        addRequest(com.android.volley.Request.Method.GET, url, null, response);
    }


    /**
     * Pošle POST žádost bez parametrů na server a odpověď se zpracuje dle implementace v Response.
     * Odpověď serveru musí být ve tvaru JSONObject.
     * @param url URL adresa
     * @param response Objekt typu Response s implementovanou metodou onResponse, případně překrytými metodami onError a onException
     */
    public static void post(String url, final Response response)
    {
        addRequest(com.android.volley.Request.Method.POST, url, "", response);
    }


    /**
     * Pošle POST žádost s parametry ve tvaru JSONObject na server a odpověď se zpracuje dle implementace v Response.
     * Odpověď serveru musí být ve tvaru JSONObject.
     * @param url URL adresa
     * @param response Objekt typu Response s implementovanou metodou onResponse, případně překrytými metodami onError a onException
     */
    public static void post(String url, JSONObject parameters, final Response response)
    {
        addRequest(com.android.volley.Request.Method.POST, url, parameters.toString(), response);
    }

    /**
     * Pošle POST žádost s parametry ve tvaru JSONObject na server a odpověď se zpracuje dle implementace v Response.
     * Odpověď serveru musí být ve tvaru ArrayResponse.
     * @param url URL adresa
     * @param response Objekt typu ArrayResponse s implementovanou metodou onResponse, případně překrytými metodami onError a onException
     */
    public static void post(String url, JSONObject parameters, final ArrayResponse response)
    {
        addRequest(com.android.volley.Request.Method.POST, url, parameters.toString(), response);
    }


    /**
     * Pošle POST žádost s parametry ve tvaru JSONArray na server a odpověď se zpracuje dle implementace v Response.
     * Odpověď serveru musí být ve tvaru JSONObject.
     * @param url URL adresa
     * @param response Objekt typu Response s implementovanou metodou onResponse, případně překrytými metodami onError a onException
     */
    public static void post(String url, JSONArray parameters, final Response response)
    {
        addRequest(com.android.volley.Request.Method.POST, url, parameters.toString(), response);
    }


    /**
     * Pošle DELETE žádost na server a odpověď se zpracuje dle implementace v Response.
     * Odpověď serveru musí být ve tvaru JSONObject.
     * @param url URL adresa
     * @param response Objekt typu Response s implementovanou metodou onResponse, případně překrytými metodami onError a onException
     */
    public static void delete(String url, final Response response)
    {
        addRequest(com.android.volley.Request.Method.DELETE, url, null, response);
    }


    private static void addRequest(int method, String url, String body, final Response r)
    {
        JsonRequest<String> request = new JsonRequest<String>(method, url, body, new com.android.volley.Response.Listener<String>()
        {
            @Override
            public void onResponse(String response)
            {
                try
                {
                    r.onResponse(new JSONObject(response));
                }
                catch (JSONException e)
                {
                    r.onException(e);
                    e.printStackTrace();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }, new com.android.volley.Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                r.onError(error);
                error.printStackTrace();
            }
        })
        {
            @SuppressLint("LongLogTag")
            @Override
            protected com.android.volley.Response<String> parseNetworkResponse(NetworkResponse networkResponse)
            {
                try
                {
                    String string = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers));
                    return com.android.volley.Response.success(string, HttpHeaderParser.parseCacheHeaders(networkResponse));
                }
                catch (UnsupportedEncodingException e)
                {
                    return com.android.volley.Response.error(new ParseError(e));
                }
            }
        };

        //rozsireni TimeOutu na 8 vterin

        request.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 15000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 0;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {
                return; //mame jen 1 pokus
            }
        });

        RequestQueue.get().add(request);
    }


    private static void addRequest(int method, String url, String body, final ArrayResponse r)
    {
        JsonRequest<String> request = new JsonRequest<String>(method, url, body, new com.android.volley.Response.Listener<String>()
        {

            @Override
            public void onResponse(String response)
            {
                try
                {
                    r.onResponse(new JSONArray(response));
                }
                catch (JSONException e)
                {
                    r.onException(e);
                    e.printStackTrace();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }, new com.android.volley.Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                r.onError(error);
                error.printStackTrace();
            }
        })
        {
            @Override
            protected com.android.volley.Response<String> parseNetworkResponse(NetworkResponse networkResponse)
            {
                try
                {
                    String string = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers));
                    return com.android.volley.Response.success(string, HttpHeaderParser.parseCacheHeaders(networkResponse));
                }
                catch (UnsupportedEncodingException e)
                {
                    return com.android.volley.Response.error(new ParseError(e));
                }
            }
        };

        //rozsireni TimeOutu na 8 vterin
        request.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 15000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 0;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {
                return; //mame jen 1 pokus
            }
        });

        if (requestQueue == null) requestQueue = Volley.newRequestQueue(AppController.getActivity());
        requestQueue.add(request);
    }



    public static void cancelAllRequests()
    {
        if (requestQueue == null) return;

        requestQueue.cancelAll(new com.android.volley.RequestQueue.RequestFilter()
        {
            @Override
            public boolean apply(com.android.volley.Request<?> request)
            {
                return true;
            }
        });
    }



    public boolean isOnline()
    {
        ConnectivityManager cm = (ConnectivityManager) ContainerActivity.mainActivity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}