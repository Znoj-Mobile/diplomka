package cz.vsb.elogika.ui.fragments.garant.courseConditions;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.EnumLogika;
import cz.vsb.elogika.utils.Logging;

public class Fragment_cc_update_ag extends Fragment {
    View rootView;

    NumberPicker cc_add_ag_min1;
    NumberPicker cc_add_ag_min2;
    NumberPicker cc_add_ag_min3;

    NumberPicker cc_add_ag_max1;
    NumberPicker cc_add_ag_max2;
    NumberPicker cc_add_ag_max3;

    NumberPicker cc_add_ag_number_of_efforts1;
    NumberPicker cc_add_ag_number_of_efforts2;
    NumberPicker cc_add_ag_number_of_efforts3;

    List<String> toSpin = new ArrayList<String>();
    Spinner cc_add_ag_spinner;
    ArrayAdapter<String> adapter_ag;

    CheckBox cc_add_ag_mandatory;
    CheckBox cc_add_ag_include_to_result;
    CheckBox cc_add_ag_replace_negative_results;

    RadioGroup cc_add_ag_radiogroup;

    EditText cc_add_ag_title;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_cc_add_ag, container, false);

        Actionbar.addSubcategory(R.string.update_activity_group, this);
        Logging.logAccess("/Pages/Garant/PodminkyDetail/PodminkyKurzuForm.aspx?IDSA=" + getArguments().getString("idSkupinaAktivit"));
        setComponents();
        return rootView;
    }

    private void setComponents() {
        //nastaveni komponent NumberPicker
        cc_add_ag_min1 = (NumberPicker) rootView.findViewById(R.id.cc_add_ag_min1);
        cc_add_ag_min1.setMaxValue(9);
        cc_add_ag_min1.setMinValue(0);
        cc_add_ag_min2 = (NumberPicker) rootView.findViewById(R.id.cc_add_ag_min2);
        cc_add_ag_min2.setMaxValue(9);
        cc_add_ag_min2.setMinValue(0);
        cc_add_ag_min3 = (NumberPicker) rootView.findViewById(R.id.cc_add_ag_min3);
        cc_add_ag_min3.setMaxValue(9);
        cc_add_ag_min3.setMinValue(0);
        //nastaveni hodnoty komponent
        int value = Integer.valueOf(getArguments().getString("minimum"));
        cc_add_ag_min1.setValue(value/100);
        value %= 100;
        cc_add_ag_min2.setValue(value/10);
        value %= 10;
        cc_add_ag_min3.setValue(value);


        cc_add_ag_max1 = (NumberPicker) rootView.findViewById(R.id.cc_add_ag_max1);
        cc_add_ag_max1.setMaxValue(9);
        cc_add_ag_max1.setMinValue(0);
        cc_add_ag_max2 = (NumberPicker) rootView.findViewById(R.id.cc_add_ag_max2);
        cc_add_ag_max2.setMaxValue(9);
        cc_add_ag_max2.setMinValue(0);
        cc_add_ag_max3 = (NumberPicker) rootView.findViewById(R.id.cc_add_ag_max3);
        cc_add_ag_max3.setMaxValue(9);
        cc_add_ag_max3.setMinValue(0);
        //nastaveni hodnoty komponent
        value = Integer.valueOf(getArguments().getString("maximum"));
        cc_add_ag_max1.setValue(value/100);
        value %= 100;
        cc_add_ag_max2.setValue(value/10);
        value %= 10;
        cc_add_ag_max3.setValue(value);

        cc_add_ag_number_of_efforts1 = (NumberPicker) rootView.findViewById(R.id.cc_add_ag_number_of_efforts1);
        cc_add_ag_number_of_efforts1.setMaxValue(9);
        cc_add_ag_number_of_efforts1.setMinValue(0);
        cc_add_ag_number_of_efforts2 = (NumberPicker) rootView.findViewById(R.id.cc_add_ag_number_of_efforts2);
        cc_add_ag_number_of_efforts2.setMaxValue(9);
        cc_add_ag_number_of_efforts2.setMinValue(0);
        cc_add_ag_number_of_efforts3 = (NumberPicker) rootView.findViewById(R.id.cc_add_ag_number_of_efforts3);
        cc_add_ag_number_of_efforts3.setMaxValue(9);
        cc_add_ag_number_of_efforts3.setMinValue(0);
        //nastaveni hodnoty komponent
        value = Integer.valueOf(getArguments().getString("pocetPokusu"));
        cc_add_ag_number_of_efforts1.setValue(value/100);
        value %= 100;
        cc_add_ag_number_of_efforts2.setValue(value/10);
        value %= 10;
        cc_add_ag_number_of_efforts3.setValue(value);


        //nastaveni komponenty Spinner
        toSpin.add(getResources().getString(R.string.garant));
        toSpin.add(getResources().getString(R.string.tutor));

        cc_add_ag_spinner = (Spinner) rootView.findViewById(R.id.cc_add_ag_spinner);
        adapter_ag = new ArrayAdapter<String>(getActivity(),R.layout.spinner_item,toSpin);
        cc_add_ag_spinner.setAdapter( adapter_ag );

        //jen garant nebo tutor
        value = Integer.valueOf(getArguments().getString("idRole"));
        if(value == 3) {
            cc_add_ag_spinner.setSelection(0);
        }
        else if(value == 4){
            cc_add_ag_spinner.setSelection(1);
        }


        //checkboxy
        cc_add_ag_mandatory = (CheckBox) rootView.findViewById(R.id.cc_add_ag_mandatory);
        cc_add_ag_mandatory.setChecked(false);
        cc_add_ag_include_to_result = (CheckBox) rootView.findViewById(R.id.cc_add_ag_include_to_result);
        cc_add_ag_include_to_result.setChecked(false);
        cc_add_ag_replace_negative_results = (CheckBox) rootView.findViewById(R.id.cc_add_ag_replace_negative_results);
        cc_add_ag_replace_negative_results.setChecked(false);

        if(getArguments().getString("mandatory").equals("true")){
            cc_add_ag_mandatory.setChecked(true);
        }
        if(getArguments().getString("zapocitatDoVysledku").equals("true")){
            cc_add_ag_include_to_result.setChecked(true);
        }
        if(getArguments().getString("nulovat").equals("true")){
            cc_add_ag_replace_negative_results.setChecked(true);
        }

        //radioButtony
        cc_add_ag_radiogroup = (RadioGroup) rootView.findViewById(R.id.cc_add_ag_radiogroup);
        //pokud je tab == 1, pak to znamena, ze form == 0 → prezencni studium
        if(getArguments().getString("tab").equals("1")){
            cc_add_ag_radiogroup.check(R.id.cc_add_ag_radiobutton_present);
        }
        else{
            cc_add_ag_radiogroup.check(R.id.cc_add_ag_radiobutton_combinate);
        }

        //EditText
        cc_add_ag_title = (EditText) rootView.findViewById(R.id.cc_add_ag_title);
        cc_add_ag_title.setText(getArguments().getString("nazev"));

        Button cc_add_ag_btn_send = (Button) rootView.findViewById(R.id.cc_add_ag_btn_send);
        cc_add_ag_btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(R.string.activity_group_update);
                builder.setCancelable(false);
                builder.setMessage(R.string.activity_group_update_message);
                builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog2, int which) {
                        update();
                    }
                });
                builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog2, int which) {
                        ;
                    }
                }).setIcon(android.R.drawable.ic_dialog_alert);
                Dialog d = builder.create();
                d.show();
            }
        });

        Button cc_add_ag_btn_remove = (Button) rootView.findViewById(R.id.cc_add_ag_btn_remove);
        cc_add_ag_btn_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(R.string.activity_group_cancel_update);
                builder.setCancelable(false);
                builder.setMessage(R.string.activity_group_cancel_update_message);
                builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog2, int which) {
                        Bundle bundle = new Bundle();
                        Actionbar.jumpBack();
                        /*
                        //vrati se na spravnou zalozku
                        bundle.putInt("tab", Integer.valueOf(getArguments().getString("tab")));
                        Fragment_CourseConditions fragment_cc = new Fragment_CourseConditions();
                        fragment_cc.setArguments(bundle);
                        getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment_cc).commit();
                        if (ab != null) {
                            ab.setDisplayShowCustomEnabled(false);
                            ab.setDisplayShowHomeEnabled(true);
                            ab.setDisplayShowTitleEnabled(true);
                        }
                        */
                    }
                });

                builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog2, int which) {
                        ;
                    }
                }).setIcon(android.R.drawable.ic_dialog_alert);
                Dialog d = builder.create();
                d.show();
            }
        });
    }

    private void update() {
        Logging.logAccess("/Pages/Garant/PodminkyDetail/PodminkyKurzuSummary.aspx");
        final boolean return_value = false;
        Response response = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException {
                Log.d("typ response: ", response.getString("Success"));
                if (response.getString("Success").equals("true")) {
                    //novej toast
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.activity_group) + " " + getActivity().getString(R.string.activity_group_was_updated)));

                    Bundle bundle = new Bundle();
                    Actionbar.jumpBack();
                    /*
                    //vrati se na spravnou zalozku
                    bundle.putInt("tab", Integer.valueOf(getArguments().getString("tab")));
                    Fragment_CourseConditions fragment_cc = new Fragment_CourseConditions();
                    fragment_cc.setArguments(bundle);
                    getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment_cc).commit();
                    if (ab != null) {
                        ab.setDisplayShowCustomEnabled(false);
                        ab.setDisplayShowHomeEnabled(true);
                        ab.setDisplayShowTitleEnabled(true);
                    }
                    */
                } else { //nepovedlo se
                    Log.d("Fragment_cc_update_ag", "update(): " + response.toString());
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.activity_group) + " " + getActivity().getString(R.string.activity_group_was_not_updated)));
                }
            }
        };
        JSONObject parameters = new JSONObject();
        try {
            parameters.put("IDSkupinaAktivit", getArguments().getString("idSkupinaAktivit"));
            int pocet = cc_add_ag_number_of_efforts1.getValue() * 100 + cc_add_ag_number_of_efforts2.getValue() * 10 + cc_add_ag_number_of_efforts3.getValue();
            parameters.put("PocetPokusu", pocet);
            parameters.put("Nazev", cc_add_ag_title.getText());
            parameters.put("IDRole", EnumLogika.idRole(cc_add_ag_spinner.getSelectedItem().toString()));
            parameters.put("IDKurzInfo", User.courseInfoId);
            pocet = cc_add_ag_min1.getValue() * 100 + cc_add_ag_min2.getValue() * 10 + cc_add_ag_min3.getValue();
            parameters.put("Minimum", pocet);
            pocet = cc_add_ag_max1.getValue() * 100 + cc_add_ag_max2.getValue() * 10 + cc_add_ag_max3.getValue();
            parameters.put("Maximum", pocet);
            if(cc_add_ag_mandatory.isChecked()) {
                parameters.put("Povinny", true);
            }
            else{
                parameters.put("Povinny", false);
            }
            if(cc_add_ag_include_to_result.isChecked()) {
                parameters.put("ZapocitatDoVysledku", true);
            }
            else{
                parameters.put("ZapocitatDoVysledku", false);
            }
            String studyForm = ((RadioButton) rootView.findViewById(cc_add_ag_radiogroup.getCheckedRadioButtonId())).getText().toString();
            parameters.put("FormaStudia", EnumLogika.idFormaStudia(studyForm));
            if(cc_add_ag_replace_negative_results.isChecked()) {
                parameters.put("Nulovat", true);
            }
            else{
                parameters.put("Nulovat", false);
            }
            //je potreba?
            parameters.put("Smazana", false);
            parameters.put("IsEditable", true);
            Log.d("", parameters + "");
        } catch (JSONException e) {
            Log.d("Fragment_cc_update_ag", "update() param: " + e.getMessage());
        }
        Request.post(URL.GroupActivities.updategroupactivity(), parameters, response);
    }
}
