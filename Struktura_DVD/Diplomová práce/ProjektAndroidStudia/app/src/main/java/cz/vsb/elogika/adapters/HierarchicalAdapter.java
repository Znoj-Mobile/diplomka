package cz.vsb.elogika.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import cz.vsb.elogika.R;

/**
 * Hierarchický adaptér pro RecyclerView.
 *
 * Důležíté je u vytváření holderů, vložit do layoutu tlačítko +, -
 * pro seskupování a zobrazování potomků. K tomu slouží např. metody
 * wrapLayout nebo getGroupingAndExpandingButton, které je možné
 * umístit do layoutu podle potřeby. Dá se použít jen pro odsazení
 * popisku sloupců (hlavičky). Defaultně je totiž neviditelný.
 *
 * Též se dá měnit mezera mezi úrovněmi - proměnná SPACE.
 */
public abstract class HierarchicalAdapter extends RecyclerView.Adapter<HierarchicalAdapter.Holder>
{
    /**
     * Nejvyšší kategorie, která ale není zobrazována.
     * Zobrazováni jsou až její potomci. Uchovává celkový
     * i aktuální počet záznamů a adaptér, ve kterém je
     * vytvořena.
     */
    private Item rootItem;

    /**
     *Cache paměť
     */
    private Item[] rows;

    /**
     * Velikost mezery v dp mezi jednotlivými úrovněmi.
     */
    public int SPACE;



    /**
     * Konstruktor
     */
    public HierarchicalAdapter()
    {
        init();
    }


    /**
     * Konstruktor
     * @param categories již vytvořená datová struktura
     */
    public HierarchicalAdapter(ArrayList<Item> categories)
    {
        init();
        for (Item category: categories) this.addCategory(category);
    }



    /**
     * Inicializace proměnných.
     */
    private final void init()
    {
        this.SPACE = 10;
        this.rootItem = new Item();
        this.rootItem.adapter = this;
        this.rootItem.level = -1;
    }



    /**
     * Vytvoří vazbu mezi 2 prvky, kdy první je potomkem pro druhý.
     * @param item první prvek je potomek druhého prvku
     * @param parent druhý prvek je předek prvního prvku
     */
    public static void addItem(Item item, Item parent)
    {
        item.parent = parent;
        if (parent.level != -1) changeLevelAccordingToHisParent(item);
        item.position = item.parent.children.size();
        item.parent.children.add(item);
        incrementCountOfChildrenAtAllParents(item, item.countOfChildren + 1);
        incrementCountOfVisibleChildrenAtAllParents(item, item.isGrouped ? 1 : item.countOfVisibleChildren + 1);
    }



    /**
     * Přidá prvek na nejvyšší úrovni.
     * @param category prvek
     */
    public void addCategory(Item category)
    {
        addItem(category, this.rootItem);
    }



    /**
     * Přidá prvek na nejvyšší úrovni a nastaví mu typ, který je posílán do metody onCreateViewHolder jako viewType.
     * Typ je možné nastavit i v konstruktoru třídy zděděné od třídy Item.
     * @param category prvek
     * @param type (ViewType)
     */
    public void addCategory(Item category, int type)
    {
        category.type = type;
        addCategory(category);
    }


    /**
     * Odstraní vazbu mezi prvekem a jeho rodičem.
     * Tím dojde k odpojení celé větve počínaje tímto prvkem.
     * @param item prvek
     * @throws IllegalArgumentException pokud prvek nemá rodiče
     */
    public static void removeItem(Item item) throws IllegalArgumentException
    {
        if (item.parent == null) throw new IllegalArgumentException("Item musí mít rodiče!");
        decrementCountOfChildrenAtAllParents(item, item.countOfChildren + 1);
        decrementCountOfVisibleChildrenAtAllParents(item, item.isGrouped ? 1 : item.countOfVisibleChildren + 1);
        item.parent.children.remove(item);
        for (int j = item.position; j < item.parent.children.size(); j++)
        {
            item.parent.children.get(j).position--;
        }
    }



    /**
     * Vyhledá item na základě pozice v adaptéru.
     * @param position pozice v adaptéru
     * @return vyhledaný item
     */
    private Item findItem(int position)
    {
        // Na začátku vytvoříme pamět
        if (this.rows == null) createCache();
        if (this.rows.length != this.rootItem.countOfChildren) createCache();

        Item tempItem;
        if (rows[position] == null) // Pokud není v paměti
        {
            int distance = 1;
            Item wantedItem;
            boolean isTopDirectionInEnd = false;
            boolean isBottomDirectionInEnd = false;
            while (true)
            {
                // Podíváme se směrem nahoru
                if (position - distance >= 0)
                {
                    wantedItem = rows[position - distance];
                    if (wantedItem != null)
                    {
                        tempItem = findItemDown(wantedItem, new AtomicInteger(distance));
                        break;
                    }
                }
                else isTopDirectionInEnd = true;

                // Podíváme se směrem dolů
                if (position + distance < getItemCount())
                {
                    wantedItem = rows[position + distance];
                    if (wantedItem != null)
                    {
                        tempItem = findItemUp(wantedItem, new AtomicInteger(distance));
                        break;
                    }
                }
                else isBottomDirectionInEnd = true;

                // Pokud už jsme na konci v obou směrech, vybereme první prvek
                if (isTopDirectionInEnd && isBottomDirectionInEnd)
                {
                    tempItem = findItemDown(this.rootItem.children.get(0), new AtomicInteger(position));
                    break;
                }
                distance++;
            }

            // Uložíme do paměti
            this.rows[position] = tempItem;
        }
        else tempItem = this.rows[position];

        return tempItem;
    }



    /**
     * Vyhledá item na základě pozice v adaptéru. Vyhledávání probíhá směrem dolů od proměnné foundItem.
     * @param foundItem item, od kterého se začne s vyhledáváním
     * @param remain kolik řádků je hledaný item od foundItem, AtomicInteger je použit kvůli rekurzi
     * @return vyhledaný item
     */
    private Item findItemDown(Item foundItem, AtomicInteger remain)
    {
        // Není to přímo on?
        if (remain.get() == 0) return foundItem;

        // Má potomky? Nejsou seskupeni?
        if (!foundItem.isGrouped && foundItem.countOfVisibleChildren > 0)
        {
            // Potomků je méně než hledám, tak jejich počet odečtu a přeskočím je
            if (foundItem.countOfVisibleChildren < remain.get()) remain.addAndGet(-foundItem.countOfVisibleChildren);
            else
            {
                remain.decrementAndGet();
                return findItemDown(foundItem.children.get(0), remain);
            }
        }

        Item temporaryItem = foundItem;
        while (temporaryItem.parent != null)
        {
            if (temporaryItem.parent.children.size() - temporaryItem.position > 1)
            {
                remain.decrementAndGet();
                return findItemDown(temporaryItem.parent.children.get(temporaryItem.position + 1), remain);
            }
            else temporaryItem = temporaryItem.parent;
        }

        return null;
    }



    /**
     * Vyhledá item na základě pozice v adaptéru. Vyhledávání probíhá směrem nahoru od proměnné foundItem.
     * @param foundItem item, od kterého se začne s vyhledáváním
     * @param remain kolik řádků je hledaný item od foundItem, AtomicInteger je použit kvůli rekurzi
     * @return vyhledaný item
     */
    private Item findItemUp(Item foundItem, AtomicInteger remain)
    {
        // Není to přímo on?
        if (remain.get() == 0) return foundItem;

        // Má před sebou položky ve stejné kategorii?
        remain.decrementAndGet();
        if (foundItem.position > 0)
        {
            Item temporaryItem = foundItem.parent.children.get(foundItem.position - 1);
            while (temporaryItem.children.size() > 0)
            {
                temporaryItem = temporaryItem.children.get(temporaryItem.children.size() - 1);
            }
            return findItemUp(temporaryItem, remain);
        }
        else return findItemUp(foundItem.parent, remain); // Je první ve své kategorii, takže před ním je jen jeho rodič
    }



    @Override
    public int getItemViewType(int position)
    {
        return findItem(position).type;
    }



    @Override
    public void onBindViewHolder(final Holder holder, int position)
    {
        final Item item = findItem(position);
        holder.itemView.setPadding((int) (item.level * SPACE * holder.itemView.getContext().getResources().getDisplayMetrics().density), 0, 0, 0);
        if (holder.groupingButton != null)
        {
            if (item.countOfVisibleChildren == 0) holder.groupingButton.setVisibility(View.INVISIBLE);
            else
            {
                holder.groupingButton.setVisibility(View.VISIBLE);
                holder.groupingButton.setImageResource(item.isGrouped ? R.drawable.icon_plus : R.drawable.icon_minus);
                holder.groupingButton.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        item.isGrouped = !item.isGrouped;
                        if (item.isGrouped) groupItems(item, holder.getAdapterPosition());
                        else expandItems(item, holder.getAdapterPosition());
                    }
                });
            }
        }

        onBindViewHolder(holder, position, item);
    }



    public abstract void onBindViewHolder(final Holder holder, int position, final Item item);



    @Override
    public int getItemCount()
    {
        return this.rootItem.countOfVisibleChildren;
    }



    /**
     * Vrátí View (přetypován na ViewGroup), ve kterém je tlačítko +, - pro seskupování a zobrazování potomků.
     * Je nutné přidat tento View někam do layoutu, aby bylo funkční seskupování
     * a zobrazování.
     * Je možné využít i jiné podobné metody.
     * @param parent budoucí rodič tohoto tlačítka
     * @return View, ve kterém je tlačítko +, -
     */
    public static ViewGroup getGroupingAndExpandingButton(ViewGroup parent)
    {
        return (ViewGroup) LayoutInflater.from(parent.getContext()).inflate(R.layout.hierarchical_adapter_item, parent, false);
    }



    /**
     * Obalí layout novým LinearLayoutem a přidá dovnitř tlačítko +, - pro seskupování a zobrazování potomků.
     * Je možné využít i jiné podobné metody.
     * @param parent ViewGroup, které posílá metoda onCreateViewHolder
     * @param layout id layout, který se má obalovat
     * @return LinearLayout, který obsahuje tlačítko a layout volajícího
     */
    public static View wrapLayout(ViewGroup parent, int layout)
    {
        LinearLayout.LayoutParams parameters = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout rootLayout = new LinearLayout(parent.getContext());
        rootLayout.setLayoutParams(parameters);
        rootLayout.setGravity(Gravity.CENTER_VERTICAL);
        View groupingButton = LayoutInflater.from(parent.getContext()).inflate(R.layout.hierarchical_adapter_item, rootLayout, false);
        rootLayout.addView(groupingButton);
        View view = LayoutInflater.from(parent.getContext()).inflate(layout, rootLayout, false);
        rootLayout.addView(view);
        return rootLayout;
    }



    /**
     * Seskupí všechny prvky, které má item nastavené jako potomky.
     * @param item tento item bude seskupen
     * @param position pozice v adaptéru získána z holderu metodou getAdapterPosition
     */
    protected void groupItems(Item item, int position)
    {
        item.rows = new Item[item.countOfVisibleChildren];
        System.arraycopy(this.rows, position + 1, item.rows, 0, item.countOfVisibleChildren);
        if (position + item.countOfVisibleChildren + 1 < getItemCount()) System.arraycopy(this.rows, position + item.countOfVisibleChildren + 1, this.rows, position + 1, getItemCount() - position - item.countOfVisibleChildren - 1);
        decrementCountOfVisibleChildrenAtAllParents(item, item.countOfVisibleChildren);
        notifyItemChanged(position);
        notifyItemRangeRemoved(position + 1, item.countOfVisibleChildren);
    }



    /**
     * Zobrazí všechny prvky, které má item nastavené jako potomky.
     * @param item tento item bude seskupen
     * @param position pozice v adaptéru získána z holderu metodou getAdapterPosition
     */
    protected void expandItems(Item item, int position)
    {
        if (position + 1 < getItemCount()) System.arraycopy(this.rows, position + 1, this.rows, position + item.countOfVisibleChildren + 1, getItemCount() - position - 1);
        if (item.rows == null) item.rows = new Item[item.countOfVisibleChildren];
        System.arraycopy(item.rows, 0, this.rows, position + 1, item.countOfVisibleChildren);
        item.rows = null;
        incrementCountOfVisibleChildrenAtAllParents(item, item.countOfVisibleChildren);
        notifyItemChanged(position);
        notifyItemRangeInserted(position + 1, item.countOfVisibleChildren);
    }



    /**
     * Nalezne prvek, který má nejvyšší postavení v hierarchické struktuře (takový nemá rodiče).
     * @param item prvek, jehož nejvyššího předka hledáme
     * @return nejvyšší předek prvku, pokud už prvek nemá rodiče, je vrácen ten stejný prvek
     */
    private static Item findRootItem(Item item)
    {
        if (item == null) return null;

        Item temporaryItem = item;
        while (temporaryItem.parent != null)
        {
            temporaryItem = temporaryItem.parent;
        }
        return temporaryItem;
    }



    /**
     * Nalezne, ke kterému adaptéru prvek přísluší.
     * @param item prvek
     * @return adaptér nebo null, pokud není přiřazený k žádnému adaptéru
     */
    private static HierarchicalAdapter getAdapter(Item item)
    {
        Item temporaryItem = findRootItem(item);
        if (temporaryItem == null) return null;
        return temporaryItem.adapter;
    }



    /**
     * Vytvoří novou cache. Stará je zahozena.
     */
    protected void createCache()
    {
        this.rows = new Item[this.rootItem.countOfChildren];
    }



    /**
     * Po připojení nějaké větve hierarchické struktury je nutné
     * upravit úrovně všech prvků v takové větvi.
     * @param item prvek, který je připojován
     */
    private static void changeLevelAccordingToHisParent(Item item)
    {
        item.level = item.parent.level + 1;
        for (Item child: item.children) changeLevelAccordingToHisParent(child);
    }



    /**
     * Přičte k proměnné countOfChildren číslo z count a to se provede pro všechny předky předávaného itemu.
     * @param item jeho předek bude první, u kterého se bude přičítat
     * @param count o kolik se má proměnná navýšit
     */
    private static void incrementCountOfChildrenAtAllParents(Item item, int count)
    {
        if (item.parent == null) return;
        item.parent.countOfChildren += count;
        incrementCountOfChildrenAtAllParents(item.parent, count);
    }



    /**
     * Odečte od proměnné countOfChildren číslo z count a to se provede pro všechny předky předávaného itemu.
     * @param item jeho předek bude první, u kterého se bude odčítat
     * @param count o kolik se má proměnná snížit
     */
    private static void decrementCountOfChildrenAtAllParents(Item item, int count)
    {
        if (item.parent == null) return;
        item.parent.countOfChildren -= count;
        decrementCountOfChildrenAtAllParents(item.parent, count);
    }



    /**
     * Přičte k proměnné countOfVisibleChildren číslo z count a to se provede pro všechny předky předávaného itemu.
     * @param item jeho předek bude první, u kterého se bude přičítat
     * @param count o kolik se má proměnná navýšit
     */
    private static void incrementCountOfVisibleChildrenAtAllParents(Item item, int count)
    {
        if (item.parent == null) return;
        item.parent.countOfVisibleChildren += count;
        incrementCountOfVisibleChildrenAtAllParents(item.parent, count);
    }



    /**
     * Odečte od proměnné countOfVisibleChildren číslo z count a to se provede pro všechny předky předávaného itemu.
     * @param item jeho předek bude první, u kterého se bude odčítat
     * @param count o kolik se má proměnná snížit
     */
    private static void decrementCountOfVisibleChildrenAtAllParents(Item item, int count)
    {
        if (item.parent == null) return;
        item.parent.countOfVisibleChildren -= count;
        decrementCountOfVisibleChildrenAtAllParents(item.parent, count);
    }



    /**
     * Základní holder, od kterého musí všechno ostatní dědit.
     */
    public class Holder extends RecyclerView.ViewHolder
    {
        // Tlačítko +, -
        public ImageView groupingButton;

        public Holder(View itemView)
        {
            super(itemView);
            this.groupingButton = (ImageView) itemView.findViewById(R.id.grouping_button);
        }
    }



    /**
     * Základní jednotka hierarchické struktury.
     * Všechny typy dat musí dědit od této třídy
     * a jsou přidávány do adaptéru různými způsoby
     * (např. pomocí konstruktoru, addCategory,
     * addChild a další). Všechny tyto způsoby jsou
     * rovnocené a je možné využít jakoukoli z nich.
     *
     * V konstruktoru nově vytvořené třídy, která
     * dědí od této třídy, je možné nastavit
     * proměnnou type, která určuje typ dané třídy
     * a bude posílána v metodě onCreateViewHolder
     * jako proměnná viewType. Nastavení typu dat
     * se dá též udělat pomocí metody addCategory
     * či addChild druhým parametrem.
     */
    public static class Item
    {
        protected int type;
        private HierarchicalAdapter adapter;
        private int countOfChildren;
        private int countOfVisibleChildren;
        private int level;
        private Item parent;
        private boolean isGrouped;
        private Item[] rows;
        private ArrayList<Item> children;
        private int position;



        /**
         * Konstruktor
         */
        public Item()
        {
            this.children = new ArrayList();
        }



        /**
         * Přidá prvku potomka.
         * @param child potomek
         */
        public void addChild(Item child)
        {
            HierarchicalAdapter.addItem(child, this);
        }



        /**
         * Přidá prvku potomka a nastaví mu typ, který je posílán do metody onCreateViewHolder jako viewType.
         * Typ je možné nastavit i v konstruktoru třídy zděděné od třídy Item.
         * @param child potomek
         * @param type (viewType)
         */
        public void addChild(Item child, int type)
        {
            child.type = type;
            addChild(child);
        }



        /**
         * Smaže tento prvek ze struktury a s ním samozřejmě i jeho potomky.
         */
        public void removeItem()
        {
            HierarchicalAdapter.removeItem(this);
        }



        /**
         * Vrací rodiče prvku.
         * @return rodič
         */
        public Item getParent()
        {
            if (this.parent == null) return null;
            if (this.parent.level >= 0) return this.parent;
            else return null;
        }



        /**
         * Vrací typ prvku.
         * @return typ
         */
        public int getType()
        {
            return this.type;
        }



        /**
         * Vrací pole potomků prvku.
         * @return pole potomků
         */
        public Item[] getChildren()
        {
            return this.children.toArray(new Item[0]);
        }



        /**
         * Seskupí potomky daného prvku. Tato metoda může být zavolána pouze při vytváření datové struktury.
         * @throws IllegalStateException po první zavolání notifyDataSetChanged, už není možné tuto metodu zavolat
         */
        public void groupItems() throws IllegalStateException
        {
            if (this.rows != null) throwExceptionBecauseOfStateOfThisObject();
            this.isGrouped = true;
            decrementCountOfVisibleChildrenAtAllParents(this, this.countOfVisibleChildren);
        }



        /**
         * Zobrazí potomky daného prvku. Tato metoda může být zavolána pouze při vytváření datové struktury.
         * @throws IllegalStateException po první zavolání notifyDataSetChanged, už není možné tuto metodu zavolat
         */
        public void expandItems() throws IllegalStateException
        {
            if (this.rows != null) throwExceptionBecauseOfStateOfThisObject();
            this.isGrouped = false;
            incrementCountOfVisibleChildrenAtAllParents(this, this.countOfVisibleChildren);
        }



        private void throwExceptionBecauseOfStateOfThisObject()
        {
            throw new IllegalStateException("Tuto metodu nelze volat při procesu zobrazování (tzn. po prvním zavolání notifyDataSetChanged).");
        }
    }
}
