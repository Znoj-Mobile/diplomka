package cz.vsb.elogika.ui.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TableLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.vsb.elogika.R;
import cz.vsb.elogika.common.view.SlidingTabLayout;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.Logging;
import cz.vsb.elogika.utils.SpecialAdapter;
import cz.vsb.elogika.utils.TimeLogika;

public class Fragment_Evaluation extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    //http://elogika.vsb.cz:8090/api/StudentEvaluation/getdata?courseInfoId=76&userId=991
    //ziskat 74 a zbytek zobrazit a pak...
    //http://elogika.vsb.cz:8090/api/StudentsSummary/getdata?groupActivityId=74&userId=991

    private View rootView;

    SwipeRefreshLayout swipeLayout;
    LinearLayout nothinglayout;

    SimpleAdapter adapter;
    SimpleAdapter adapter_total;

    private ListView listView;
    //private ListView[] listViews;
    private ListView listViews;

    ArrayList<Map<String, String>>[] evaluation_list;
    //ArrayList<Map<String, String>>evaluation_list;
    ArrayList<Map<String, String>> evaluation_total_list;

    ArrayList<Map<String, String>> school_classes_list;
    private SlidingTabLayout mSlidingTabLayout;
    private ViewPager mViewPager;

    double summarySA = 0;
    int idClassYear = 0;
    int idClassClass = 0;
    List<Double> resultsClass = new ArrayList<>();

    String[] field;
    int pocetTabu;

    //synchronizacni promenna
    int isRunning;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_evaluation, container, false);
        Actionbar.newCategory(R.string.course_evaluation, this);

        swipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_update);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeResources(R.color.darker_cyan, R.color.material_blue_grey_800);

        nothinglayout = (LinearLayout) rootView.findViewById(R.id.evaluation_nothing_to_show);

        evaluation_total_list = new ArrayList<>();
        school_classes_list = new ArrayList<>();

        update();


        //volano z evaluation_total_list
        //evaluation_list = Data_into_evaluation_list();

        return rootView;
    }

    private void  Data_into_school_classes_list() {
        final ArrayList<Map<String, String>> list = new ArrayList<>();
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                if (response.length() > 0)
                {
                    for (int j = 0; j < response.length(); j++)
                    {
                        school_classes_list.add(put_data_classes(
                                response.getJSONObject(j).getString("IDTrida"),
                                response.getJSONObject(j).getString("Typ")));
                        if(response.getJSONObject(j).getString("Typ").equals("0")){
                            idClassYear = Integer.valueOf(response.getJSONObject(j).getString("IDTrida"));
                        }
                        else{
                            idClassClass = Integer.valueOf(response.getJSONObject(j).getString("IDTrida"));
                        }
                    }
                }
                else{
                    Log.d("Chyba", "Fragment_Evaluation Data_into_school_classes_list");
                }
                isRunning--;
                if(isRunning <= 0){
                    countAvg();
                }
            }
        };
        Request.get(URL.StudentSummary.GetSchoolClassesByStudent(User.id, User.courseInfoId), arrayResponse);
    }

    private void countAvg() {
        isRunning = 0;
        for(int i = 0; i < pocetTabu-1; i++) {
            isRunning += evaluation_list[i].size();
        }

        for(int i = 0; i < pocetTabu-1; i++) {
            for(int position = 0; position < evaluation_list[i].size(); position++) {
                if(evaluation_list[i].get(position).get("zapocitat").equals("true")) {
                    averageCalculate(idClassClass, evaluation_list[i].get(position), true);
                    averageCalculate(idClassYear, evaluation_list[i].get(position), false);
                }
            }
        }
        swipeLayout.setRefreshing(false);
    }

    private void averageCalculate(final int idClass, final Map<String, String> list, final boolean calculateClassAverage) {
        final int idTest = Integer.valueOf(list.get("idTest"));
        final double vysledek = Double.valueOf(list.get("vysledek"));

        final List<Double> results = new ArrayList<>();
        final ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                if (response.length() > 0)
                {
                    for (int j = 0; j < response.length(); j++)
                    {
                        results.add(response.getDouble(j));
                    }

                    //prevzato ze serveru

                    if(idTest != 0 && results.size() > 0) {
                        Collections.sort(results);
                        double suma = 0;
                        int c = -1;
                        String text = null;
                        for (double i : results) {
                            c++;
                            if (i >= 0)
                                suma += i;
                            if(calculateClassAverage){
                                if (i >= vysledek && text == null)
                                    text = " ( " + (results.size() - c + 1) + "/" + results.size() + ") ";
                            }
                            else{
                                if (i > vysledek && text == null)
                                    text = " ( " + (results.size() - c + 1) + "/" + results.size() + ") ";
                            }
                        }
                        if (text == null) {
                            text = " ( " + 1 + "/" + results.size() + ") ";
                        }
                        text = String.format("%.2f", (suma / results.size())) + text;
                        if(calculateClassAverage) {
                            list.put("poradiTrida", text);
                        }
                        else {
                            list.put("poradiRocnik", text);
                        }

                    }


                    /*
                    //alg. pro vsechny vysledky...
                    if(results.size() > 0) {
                        Collections.sort(results);
                        String text = null;

                        int firstIndex = results.indexOf(vysledek);
                        int lastIndex = results.lastIndexOf(vysledek);
                        text = " (";
                        if (firstIndex != lastIndex) {
                            text += (firstIndex + 1) + " - " + (lastIndex + 1);
                        }
                        else{
                            text += String.valueOf(firstIndex + 1);
                        }
                        text += "/" + results.size() + ") ";

                        if(calculateClassAverage) {
                            list.put("poradiTrida", list.get("prumerTrida") + " " + text);
                        }
                        else {
                            list.put("poradiRocnik", list.get("prumerRocnik") + " " + text);
                        }
                    }
                    */
                    adapter.notifyDataSetChanged();
                }
                else{
                    Log.d("Chyba", "Fragment_Evaluation averageCalculate");
                }
            }
        };
        Request.get(URL.StudentSummary.GetSummaryBySchoolClassFromTest(idClass, idTest), arrayResponse);
    }

    private Map<String, String> put_data_classes(String idTrida, String typ) {
        HashMap<String, String> item = new HashMap<String, String>();
        item.put("idTrida", idTrida);
        item.put("typ", typ);
        return item;
    }
    public void onViewCreated(View view, Bundle savedInstanceState){
        // BEGIN_INCLUDE (setup_viewpager)
        // Get the ViewPager and set it's PagerAdapter so that it can display items
        mViewPager = (ViewPager) view.findViewById(R.id.evaluation_viewpager);
        // - vola se az pri obdrzeni odpovedi ze serveru
        mViewPager.setAdapter(new SamplePagerAdapter());
        // END_INCLUDE (setup_viewpager)

        // BEGIN_INCLUDE (setup_slidingtablayout)
        // Give the SlidingTabLayout the ViewPager, this must be done AFTER the ViewPager has had
        // it's PagerAdapter set.
        mSlidingTabLayout = (SlidingTabLayout) view.findViewById(R.id.evaluation_sliding_tabs);
        // - vola se az pri obdrzeni odpovedi ze serveru
        mSlidingTabLayout.setViewPager(mViewPager);
        // END_INCLUDE (setup_slidingtablayout)
    }

    private void Data_into_evaluation_total_list() {
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                if (response.length() > 0)
                {
                    for (int j = 0; j < response.length(); j++)
                    {
                        evaluation_total_list.add(put_data_total(
                                response.getJSONObject(j).getString("IDSA"),
                                response.getJSONObject(j).getString("Nazev"),
                                TimeLogika.getFormatedDate(response.getJSONObject(j).getString("DatumVlozeni")),
                                response.getJSONObject(j).getString("Minimum"),
                                response.getJSONObject(j).getString("Maximum"),
                                response.getJSONObject(j).getString("Vysledek"),
                                response.getJSONObject(j).getString("VysledekRound"),
                                response.getJSONObject(j).getString("Splnil"),
                                response.getJSONObject(j).getString("IDZapsal"),
                                response.getJSONObject(j).getString("Jmeno"),
                                response.getJSONObject(j).getString("Prijmeni"),
                                response.getJSONObject(j).getString("TitulPred"),
                                response.getJSONObject(j).getString("TitulZa"),
                                response.getJSONObject(j).getString("MinMax"),
                                response.getJSONObject(j).getString("JmenoPosledniZmena")));
                    }
                }
                else{
                    Log.d("Chyba", "EvaluationSetData");
                    nothinglayout.setVisibility(View.VISIBLE);
                    swipeLayout.setRefreshing(false);
                }
    //            listView.setAdapter(adapter_total);
                field = new String[response.length()];
                pocetTabu = response.length()+1;
                evaluation_list = new ArrayList[pocetTabu];

                //listViews = new ListView[pocetTabu];

                //volano zde - az mam potrebny parametr groupActivityId;
                for(int i = 0; i < response.length(); i++){
                    field[i] = evaluation_total_list.get(i).get("nazev");
                    Log.d("evaluation", Integer.valueOf(evaluation_total_list.get(i).get("idsa")) + " - " + i);
                    Data_into_evaluation_list(Integer.valueOf(evaluation_total_list.get(i).get("idsa")), i);
                    //prirazovani listu probiha uvnitr Data_into_evaluation_list
                    //evaluation_list = Data_into_evaluation_list(Integer.valueOf(evaluation_total_list.get(i).get("idsa")), i);
                }
                mViewPager.setAdapter(new SamplePagerAdapter(field, response.length()+1));
                mSlidingTabLayout.setViewPager(mViewPager);
                isRunning--;
                if(isRunning <= 0){
                    countAvg();
                }
            }
        };
        Request.get(URL.StudentEvaluation.getdata(User.courseInfoId, User.id), arrayResponse);
    }

    private Map<String, String> put_data_total(String idsa, String nazev, String datumVlozeni, String minimum, String maximum, String vysledek,
                                               String vysledekRound, String splnil, String idZapsal, String jmeno, String prijmeni, String titulPred,
                                               String titulZa, String minMax, String jmenoPosledniZmena) {
        HashMap<String, String> item = new HashMap<String, String>();
        item.put("idsa", idsa);
        item.put("nazev", nazev);
        item.put("datumVlozeni",datumVlozeni);
        item.put("minimum",minimum);
        item.put("maximum",maximum);
        item.put("vysledek",vysledek);
        item.put("vysledekRound",vysledekRound);
        item.put("splnil",splnil);
        item.put("idZapsal",idZapsal);
        item.put("jmeno",jmeno);
        item.put("prijmeni", prijmeni);
        item.put("titulPred",titulPred);
        item.put("titulZa",titulZa);
        item.put("minMax",minMax);
        item.put("jmenoPosledniZmena",jmenoPosledniZmena.substring(0, jmenoPosledniZmena.length()-3));
        item.put("celkem_sa", "0");
        return item;
    }


    private ArrayList<Map<String, String>> Data_into_evaluation_list(int groupActivityId, final int rank) {
        final ArrayList<Map<String, String>> list = new ArrayList<Map<String, String>>();
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                if (response.length() > 0)
                {
                    summarySA = 0;
                    for (int j = 0; j < response.length(); j++)
                    {
                        Log.d("evaluatiopn-response", response.getJSONObject(j).getString("NazevAktivita"));
                        list.add(put_data(
                                response.getJSONObject(j).getString("IDAktivita"),
                                response.getJSONObject(j).getString("IDTest"),
                                response.getJSONObject(j).getString("IDHodnoceniTest"),
                                response.getJSONObject(j).getString("NazevAktivita"),
                                response.getJSONObject(j).getString("ImageURL"),
                                TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("DatumVlozeni")),
                                response.getJSONObject(j).getString("Minimum"),
                                response.getJSONObject(j).getString("Maximum"),
                                response.getJSONObject(j).getString("Vysledek"),
                                response.getJSONObject(j).getString("DatumPlatnosti"),
                                TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("PosledniZmena")),
                                response.getJSONObject(j).getString("Zapocitat"),
                                response.getJSONObject(j).getString("Splnil"),
                                response.getJSONObject(j).getString("IDZapsal"),
                                response.getJSONObject(j).getString("Jmeno"),
                                response.getJSONObject(j).getString("MinMax"),
                                response.getJSONObject(j).getString("Prijmeni"),
                                response.getJSONObject(j).getString("TitulPred"),
                                response.getJSONObject(j).getString("TitulZa"),
                                response.getJSONObject(j).getString("JmenoPosledniZmena"),
                                response.getJSONObject(j).getString("VysledekRound"),
                                response.getJSONObject(j).getString("PrumerTrida"),
                                response.getJSONObject(j).getString("PrumerRocnik")));
                        //zbytek prace se zapocitat v SpecialAdapterEvaluation
                        if(response.getJSONObject(j).getString("Zapocitat").equals("true")) {
                            //vypocet celkoveho vysledku
                            summarySA += Double.valueOf(response.getJSONObject(j).getString("VysledekRound"));
                            //ulozeni vypocitaneho celkoveho vysledku skupiny aktivit
                            evaluation_total_list.get(rank).put("celkem_sa", String.valueOf(summarySA));
                        }
                    }
                }
                else{
                    Log.d("Chyba", "EvaluationSetData");
                    swipeLayout.setRefreshing(false);
                }
                //listViews[0].setAdapter(adapter);
//                listViews.setAdapter(adapter);
                //adapter.notifyDataSetChanged();
                evaluation_list[rank] = list;
                //evaluation_list = list;
                //vzdy nove prekresleni
                mViewPager.setAdapter(new SamplePagerAdapter(field, pocetTabu));
                mSlidingTabLayout.setViewPager(mViewPager);
                //jeden tab je celkove hodnoceni a - 1 protoze pocitani od nuly...
                if(rank == pocetTabu - 2) {
                    isRunning--;
                    if(isRunning <= 0){
                        countAvg();
                    }
                }
                else{
                    swipeLayout.setRefreshing(false);
                }
            }
        };
        Request.get(URL.StudentSummary.GetSummaryByGroupActivityAndStudent(String.valueOf(groupActivityId), String.valueOf(User.id)), arrayResponse);
         return list;
    }

    private Map<String,String> put_data(String idAktivita, String idTest, String idHodnoceniTest,
                                        String nazevAktivita, String imageURL, String datumVlozeni,
                                        String minimum, String maximum, String vysledek, String datumPlatnosti,
                                        String posledniZmena, String zapocitat, String splnil, String idZapsal,
                                        String jmeno, String minMax, String prijmeni, String titulPred, String titulZa,
                                        String jmenoPosledniZmena, String vysledekRound, String prumerTrida, String prumerRocnik) {
        HashMap<String, String> item = new HashMap<String, String>();
        item.put("idAktivita", idAktivita);
        item.put("idTest", idTest);
        item.put("idHodnoceniTest", idHodnoceniTest);
        item.put("nazevAktivita", nazevAktivita);
        item.put("imageURL", imageURL);
        item.put("datumVlozeni",datumVlozeni);
        item.put("minimum",minimum);
        item.put("maximum",maximum);
        item.put("vysledek",vysledek);
        item.put("datumPlatnosti",datumPlatnosti);
        item.put("posledniZmena",posledniZmena);
        item.put("zapocitat",zapocitat);
        item.put("splnil",splnil);
        item.put("idZapsal",idZapsal);
        item.put("jmeno",jmeno);
        item.put("minMax",minMax);
        item.put("prijmeni",prijmeni);
        item.put("titulPred",titulPred);
        item.put("titulZa",titulZa);
        item.put("jmenoPosledniZmena",jmenoPosledniZmena.substring(0, jmenoPosledniZmena.length()-3));
        item.put("vysledekRound",vysledekRound);
        item.put("prumerTrida",prumerTrida);
        item.put("prumerRocnik",prumerRocnik);
        return item;
    }


    public void onRefresh() {
        update();
    }
    void clear_list(){
        school_classes_list.clear();
        evaluation_total_list.clear();
        if(evaluation_list != null) {
            for (int i = 0; i < evaluation_list.length; i++) {
                if(evaluation_list[i] != null) {
                    evaluation_list[i].clear();
                }
            }
        }
    }

    private void update() {
        swipeLayout.post(new Runnable() {
            @Override
            public void run() {
                //synchronizacni promenna
                isRunning = 3;
                nothinglayout.setVisibility(View.GONE);
                swipeLayout.setRefreshing(true);
                clear_list();
                if(adapter != null) {
                    adapter.notifyDataSetChanged();
                }
                if(adapter_total != null) {
                    adapter_total.notifyDataSetChanged();
                }
                Data_into_school_classes_list();
                Data_into_evaluation_total_list();
            }
        });
    }


    class SamplePagerAdapter extends PagerAdapter {

        int count;
        String[] tabNames;

        SamplePagerAdapter(String[] names, int count_p){
            count = count_p;
            tabNames = new String[count];
            tabNames = names;
            Log.d("SamplePagerAdapter", tabNames[0] + " - " + count);
        }

        SamplePagerAdapter(){
            tabNames = null;
            count = 1;
        }
        /**
         * @return the number of pages to display
         */
        @Override
        public int getCount() {
            return count;
        }

        /**
         * @return true if the value returned from {@link #instantiateItem(ViewGroup, int)} is the
         * same object as the {@link View} added to the {@link ViewPager}.
         */
        @Override
        public boolean isViewFromObject(View view, Object o) {
            return o == view;
        }

        // BEGIN_INCLUDE (pageradapter_getpagetitle)
        /**
         * Return the title of the item at {@code position}. This is important as what this method
         * returns is what is displayed in the {@link SlidingTabLayout}.
         * <p>
         * Here we construct one using the position value, but for real application the title should
         * refer to the item's contents.
         */
        @Override
        public CharSequence getPageTitle(int position) {
            if(position == 0){
                return getResources().getString(R.string.evaluation_total_tab);
            }
            if(tabNames != null){
                return tabNames[position - 1];
            }
            else{
                return "tab " + String.valueOf(position + 1);
            }
        }
        // END_INCLUDE (pageradapter_getpagetitle)

        /**
         * Instantiate the {@link View} which should be displayed at {@code position}. Here we
         * inflate a layout from the apps resources and then change the text view to signify the position.
         */
        @Override
        public Object instantiateItem(final ViewGroup container, int position) {
            // Inflate a new layout from our resources
            View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_evaluation_pager,
                    container, false);
            // Add the newly created View to the ViewPager
            container.addView(view);

            /*
            // Retrieve a TextView from the inflated View, and update it's text
            TextView title = (TextView) view.findViewById(R.id.evaluation_title);

            if(position == 0){
                title.setText(getResources().getString(R.string.evaluation_total_tab));
            }
            else {
                if (tabNames != null) {
                    title.setText(tabNames[position - 1] + "");
                } else {
                    title.setText("");
                }
            }
            */
            int[] to = {R.id.e_name, R.id.e_result, R.id.e_minMax, R.id.e_lastChange};
            if(position == 0) {
                Logging.logAccess("/Pages/Student/HodnoceniKurzu.aspx", "Summary");
                listView = (ListView) view.findViewById(R.id.evaluations_listView);
                //title.setText(getResources().getString(R.string.activities_for_current_curse));
                String[] from = {"nazev", "vysledekRound", "minMax", "jmenoPosledniZmena"};
                adapter_total = new SpecialAdapter(getActivity(), evaluation_total_list,
                        R.layout.fragment_evaluation_list, from, to);
                listView.setAdapter(adapter_total);

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        mViewPager.setCurrentItem(position + 1);
                    }
                });
                listView.setOnScrollListener(new AbsListView.OnScrollListener() {

                    @Override
                    public void onScrollStateChanged(AbsListView view, int scrollState) {
                    }

                    @Override
                    public void onScroll(AbsListView view, int firstVisibleItem,
                                         int visibleItemCount, int totalItemCount) {
                        boolean enable = false;
                        if (listView != null) {
                            if (listView.getChildCount() == 0) {
                                enable = true;
                            } else {
                                // check if the first item of the list is visible
                                boolean firstItemVisible = listView.getFirstVisiblePosition() == 0;
                                // check if the top of the first item is visible
                                boolean topOfFirstItemVisible = listView.getChildAt(0).getTop() == 0;
                                // enabling or disabling the refresh layout
                                enable = firstItemVisible && topOfFirstItemVisible;
                            }
                        }
                        swipeLayout.setEnabled(enable);
                    }
                });
            }
            else{
                Logging.logAccess("/Pages/Student/HodnoceniKurzu.aspx", "Concrete");
                listViews = (ListView) view.findViewById(R.id.evaluations_listView);
                listViews.setOnScrollListener(new AbsListView.OnScrollListener() {

                    @Override
                    public void onScrollStateChanged(AbsListView view, int scrollState) {
                    }

                    @Override
                    public void onScroll(AbsListView view, int firstVisibleItem,
                                         int visibleItemCount, int totalItemCount) {
                        boolean enable = false;
                        if (listViews != null) {
                            if(listViews.getChildCount() == 0){
                                enable = true;
                            }
                            else {
                                // check if the first item of the list is visible
                                boolean firstItemVisible = listViews.getFirstVisiblePosition() == 0;
                                // check if the top of the first item is visible
                                boolean topOfFirstItemVisible = listViews.getChildAt(0).getTop() == 0;
                                // enabling or disabling the refresh layout
                                enable = firstItemVisible && topOfFirstItemVisible;
                            }
                        }
                        swipeLayout.setEnabled(enable);
                    }
                });
                String[] from = {"nazevAktivita", "vysledekRound", "minMax", "jmenoPosledniZmena"};
                if (evaluation_list != null && evaluation_list[position - 1] != null) {
                    //newest first
                    Collections.sort(evaluation_list[position - 1], new LastChangeComparator());
                    //included in result first
                    Collections.sort(evaluation_list[position - 1], new InResultComparator());

                    adapter = new SpecialAdapterEvaluation(getActivity(), evaluation_list[position - 1], R.layout.fragment_evaluation_list, from, to, Double.valueOf(evaluation_total_list.get(position-1).get("celkem_sa")));

                    TableLayout tl = (TableLayout) view.findViewById(R.id.evaluation_res);
                    tl.setVisibility(View.VISIBLE);

                    TextView res_e_name = (TextView) view.findViewById(R.id.res_e_name);
                    res_e_name.setText(evaluation_total_list.get(position-1).get("nazev"));
                    TextView res_e_result = (TextView) view.findViewById(R.id.res_e_result);
                    res_e_result.setText(evaluation_total_list.get(position-1).get("celkem_sa"));
                    TextView res_e_minMax = (TextView) view.findViewById(R.id.res_e_minMax);
                    res_e_minMax.setText(evaluation_total_list.get(position-1).get("minMax"));
                    //res_e_minMax.setTypeface(null, Typeface.BOLD);

                }
                listViews.setAdapter(adapter);
            }

            // Return the View
            return view;
        }

        /**
         * Destroy the item from the {@link ViewPager}. In our case this is simply removing the
         * {@link View}.
         */
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        private class InResultComparator implements java.util.Comparator<Map<String, String>> {

            @Override
            public int compare(Map<String, String> lhs, Map<String, String> rhs) {
                if(lhs.get("zapocitat").equals("true")){
                    if(!rhs.get("zapocitat").equals("true")) {
                        return -1;
                    }
                }
                else{
                    if(rhs.get("zapocitat").equals("true")) {
                        return 1;
                    }
                }
                //both has same value
                return 0;
            }
        }

        private class LastChangeComparator implements java.util.Comparator<Map<String, String>> {
            @Override
            public int compare(Map<String, String> lhs, Map<String, String> rhs) {
                return TimeLogika.compare(rhs.get("posledniZmena"), lhs.get("posledniZmena"));
            }
        }
    }


    private class SpecialAdapterEvaluation extends SimpleAdapter {

        ArrayList<Map<String, String>> items;
        double celkem_sa = 0;

        public SpecialAdapterEvaluation(Activity activity, ArrayList<Map<String, String>> items, int fragment_evaluation_list, String[] from, int[] to, double celkem_sa) {
            super(activity, items, fragment_evaluation_list, from, to);
            this.items = items;
            this.celkem_sa = celkem_sa;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = super.getView(position, convertView, parent);

            //prumer trida
            TextView e_average_class_title = (TextView) view.findViewById(R.id.e_average_class_title);
            TextView e_average_class = (TextView) view.findViewById(R.id.e_average_class);
            //prumer rocnik
            TextView e_average_year_title = (TextView) view.findViewById(R.id.e_average_year_title);
            TextView e_average_year = (TextView) view.findViewById(R.id.e_average_year);

            TextView e_name = (TextView) view.findViewById(R.id.e_name);
            TextView e_result = (TextView) view.findViewById(R.id.e_result);
            TextView e_minMax = (TextView) view.findViewById(R.id.e_minMax);
            TextView e_lastChange = (TextView) view.findViewById(R.id.e_lastChange);

            if(items.get(position).get("zapocitat").equals("true")) {


                if(Integer.valueOf(items.get(position).get("idTest")) != -1) {
                    //prumer trida
                    e_average_class_title.setVisibility(View.VISIBLE);
                    e_average_class.setText(items.get(position).get("poradiTrida"));
                    e_average_class.setVisibility(View.VISIBLE);

                    //prumer rocnik
                    e_average_year_title.setVisibility(View.VISIBLE);
                    e_average_year.setText(items.get(position).get("poradiRocnik"));
                    e_average_year.setVisibility(View.VISIBLE);

                    //averageCalculate(idClassClass, Integer.valueOf(items.get(position).get("idTest")), e_average_class, Double.valueOf(items.get(position).get("vysledekRound")), true);
                    //averageCalculate(idClassYear, Integer.valueOf(items.get(position).get("idTest")), e_average_year, Double.valueOf(items.get(position).get("vysledekRound")), false);

                    //a vsechno tucne...
                    e_name.setTextAppearance(getActivity(), R.style.itemNameBoldColored);
                    e_result.setTextAppearance(getActivity(), R.style.itemBoldColored);
                    e_minMax.setTextAppearance(getActivity(), R.style.itemBoldColored);
                    e_lastChange.setTextAppearance(getActivity(), R.style.itemBoldColored);
                    e_average_class.setTextAppearance(getActivity(), R.style.itemBoldColored);
                    e_average_year.setTextAppearance(getActivity(), R.style.itemBoldColored);
                }
            }

            else{
                //vsechno ostatni netucne - je potreba zadat, jinak se to obas meni...
                e_name.setTextAppearance(getActivity(), R.style.itemName);
                e_result.setTextAppearance(getActivity(), R.style.item);
                e_minMax.setTextAppearance(getActivity(), R.style.item);
                e_lastChange.setTextAppearance(getActivity(), R.style.item);
                e_average_class.setTextAppearance(getActivity(), R.style.item);
                e_average_year.setTextAppearance(getActivity(), R.style.item);

                //prumer trida
                e_average_class_title.setVisibility(View.GONE);
                e_average_class.setVisibility(View.GONE);

                //prumer rocnik
                e_average_year_title.setVisibility(View.GONE);
                e_average_year.setVisibility(View.GONE);
            }

            return view;
        }

        private void averageCalculate(final int idClass, final int idTest, final TextView e_average, final double vysledek, final boolean calculateClassAverage) {
            final List<Double> results = new ArrayList<>();
            final ArrayResponse arrayResponse = new ArrayResponse()
            {
                @Override
                public void onResponse(JSONArray response) throws JSONException
                {
                    if (response.length() > 0)
                    {
                        for (int j = 0; j < response.length(); j++)
                        {
                            results.add(response.getDouble(j));
                        }

                        //prevzato ze serveru
                        if(idTest != 0 && results.size() > 0) {
                            Collections.sort(results);
                            double suma = 0;
                            int c = -1;
                            String text = null;
                            for (double i : results) {
                                c++;
                                if (i >= 0)
                                    suma += i;
                                if(calculateClassAverage){
                                    if (i >= vysledek && text == null)
                                        text = " ( " + (results.size() - c + 1) + "/" + results.size() + ") ";
                                }
                                else{
                                    if (i > vysledek && text == null)
                                        text = " ( " + (results.size() - c + 1) + "/" + results.size() + ") ";
                                }
                            }
                            if (text == null) {
                                text = " ( " + 1 + "/" + results.size() + ") ";
                            }
                            text = String.format("%.2f", (suma / results.size())) + text;
                            e_average.setText(text);
                        }
                    }
                    else{
                        Log.d("Chyba", "Fragment_Evaluation Data_into_school_classes_list");
                    }
                }
            };
            Request.get(URL.StudentSummary.GetSummaryBySchoolClassFromTest(idClass, idTest), arrayResponse);
        }
    }
}
