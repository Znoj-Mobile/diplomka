package cz.vsb.elogika.ui.fragments.garant.courseConditions;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.DividerItemDecoration;
import cz.vsb.elogika.utils.TimeLogika;

public class Fragment_cc_term extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private View mainView;
    private View rootView;
    private RecyclerView recyclerView;

    LinearLayout nothinglayout;
    SwipeRefreshLayout swipeLayout;

    protected RecyclerView.Adapter adapter;

    ArrayList<Map<String, String>> cc_term_list;

    String idSkupinaAktivit;
    String idAktivita;
    String idTest;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mainView = inflater.inflate(R.layout.fragment_course_conditions_term, container, false);
        rootView = mainView.findViewById(R.id.recycler_view_with_button_swipe);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);

        Actionbar.addSubcategory(R.string.term_list, this);

        idSkupinaAktivit = null;
        idAktivita = null;
        idTest = null;

        try{
            idSkupinaAktivit = getArguments().getString("idSkupinaAktivit");
        } catch(Exception e){        }
        try{
            idAktivita = getArguments().getString("idAktivita");
        } catch(Exception e){        }
        try{
            idTest = getArguments().getString("idTest");
        } catch(Exception e){        }


        cc_term_list = new ArrayList<Map<String,String>>();

        swipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_update);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeResources(R.color.darker_cyan, R.color.material_blue_grey_800);

        nothinglayout = (LinearLayout) rootView.findViewById(R.id.nothing_to_show);
        nothinglayout.setVisibility(View.GONE);

        this.adapter = new RecyclerView.Adapter() {
            class ViewHolderHeader extends RecyclerView.ViewHolder {
                public ViewHolderHeader(View itemView) {
                    super(itemView);
                }
            }

            class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
                public TextView term_name;
                public TextView term_login_from;
                public TextView term_active_from;
                public TextView term_logged;
                public TextView term_max;
                public TextView term_login_to;
                public TextView term_active_to;
                public Button btn_listing_logged_students;
                public Button btn_delete;

                public ViewHolder(View itemView) {
                    super(itemView);
                    itemView.setOnClickListener(this);
                    this.term_name = (TextView) itemView.findViewById(R.id.cc_term_name);
                    this.term_login_from = (TextView) itemView.findViewById(R.id.cc_term_login_from);
                    this.term_active_from = (TextView) itemView.findViewById(R.id.cc_term_active_from);
                    this.term_logged = (TextView) itemView.findViewById(R.id.cc_term_logged);
                    this.term_max = (TextView) itemView.findViewById(R.id.cc_term_max);
                    this.term_login_to = (TextView) itemView.findViewById(R.id.cc_term_login_to);
                    this.term_active_to = (TextView) itemView.findViewById(R.id.cc_term_active_to);
                    this.btn_listing_logged_students = (Button)  itemView.findViewById(R.id.cc_term_listing_logged_students);
                    this.btn_delete = (Button) itemView.findViewById(R.id.cc_term_delete);
                }


                @Override
                public void onClick(View view) {
                    final int position = getAdapterPosition();
                    Bundle bundle = new Bundle();
                    if(idSkupinaAktivit != null){
                        bundle.putString("idSkupinaAktivit", idSkupinaAktivit);
                    }
                    else if(idAktivita != null){
                        bundle.putString("idAktivita", idAktivita);

                    }
                    else if(idTest != null){
                        bundle.putString("idTest", idTest);

                    }
                    bundle.putSerializable("list", (Serializable) cc_term_list.get(position));
                    Fragment_cc_add_term fragment_cc_add_term = new Fragment_cc_add_term();
                    fragment_cc_add_term.setArguments(bundle);
                    getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment_cc_add_term).commit();
                }
            }

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view;
                switch (viewType) {
                    case 0:
                        view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_cc_term_header, parent, false);
                        return new ViewHolderHeader(view);
                    case 1:
                        view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_cc_term, parent, false);
                        return new ViewHolder(view);
                    default:
                        return null;
                }
            }
            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
                if (holder.getItemViewType() == 1) {
                    final ViewHolder viewHolder = (ViewHolder) holder;

                    viewHolder.term_name.setText(cc_term_list.get(position).get("nazev"));
                    viewHolder.term_login_from.setText(cc_term_list.get(position).get("datumPrihlaseni"));
                    viewHolder.term_active_from.setText(cc_term_list.get(position).get("aktivniOD"));
                    viewHolder.term_logged.setText(cc_term_list.get(position).get("pocetZapsanych"));
                    viewHolder.term_max.setText(cc_term_list.get(position).get("pocetStudentu"));
                    viewHolder.term_login_to.setText(cc_term_list.get(position).get("datumOdhlaseni"));
                    viewHolder.term_active_to.setText(cc_term_list.get(position).get("aktivniDO"));

                    final int finalPosition = position;

                    viewHolder.btn_delete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setTitle(R.string.term_delete);
                            builder.setCancelable(false);
                            builder.setMessage(R.string.term_delete_message);
                            builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog2, int which) {
                                    delete(cc_term_list.get(finalPosition).get("idTermin"), cc_term_list.get(finalPosition).get("nazev"));
                                    update();
                                }
                            });

                            builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog2, int which) {
                                    ;
                                }
                            }).setIcon(android.R.drawable.ic_dialog_alert);
                            Dialog d = builder.create();
                            d.show();
                        }
                    });

                    viewHolder.btn_listing_logged_students.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Bundle bundle = new Bundle();
                            bundle.putString("idSkupinaAktivit", idSkupinaAktivit);
                            bundle.putString("idAktivita", idAktivita);
                            bundle.putString("idAktivita", idTest);
                            bundle.putString("idTermin", cc_term_list.get(finalPosition).get("idTermin"));
                            bundle.putString("tab", getArguments().getString("tab"));
                            bundle.putString("nazevTyp", cc_term_list.get(finalPosition).get("nazevTyp"));
                            bundle.putString("nazev", cc_term_list.get(finalPosition).get("nazev"));
                            bundle.putString("aktivniOD", cc_term_list.get(finalPosition).get("aktivniOD"));
                            bundle.putString("aktivniDO", cc_term_list.get(finalPosition).get("aktivniDO"));
                            Fragment_cc_listing_students fragment_cc_listing = new Fragment_cc_listing_students();
                            fragment_cc_listing.setArguments(bundle);
                            getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment_cc_listing).commit();
                        }
                    });
                }
            }

            @Override
            public int getItemViewType(int position)
            {
                return 1;
            }

            @Override
            public int getItemCount() {
                //velikost listu pro vyplneni tabulky + 1 pro nadpis tabulky
                return cc_term_list.size();
            }
        };

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        recyclerView.setHasFixedSize(true);
        //zaruci, ze se bude seznam aktualizovat jen pri swipu uplne nahore...
        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy <= 0) {
                    swipeLayout.setEnabled(true);
                } else {
                    swipeLayout.setEnabled(false);
                }
            }
        });
        recyclerView.setAdapter(adapter);



        Button roundButton = (Button) rootView.findViewById(R.id.roundButton);
        roundButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("aktivitaNazev", getArguments().getString("aktivitaNazev"));

                if(idSkupinaAktivit != null){
                    bundle.putString("idSkupinaAktivit", idSkupinaAktivit);
                }
                else if(idAktivita != null){
                    bundle.putString("idAktivita", idAktivita);
                }
                else{
                    bundle.putString("idTest", idTest);
                }

                Fragment_cc_add_term fragment_cc_add_term = new Fragment_cc_add_term();
                fragment_cc_add_term.setArguments(bundle);
                getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment_cc_add_term).commit();
            }
        });

        update();

        return mainView;
    }


    public void update() {
        swipeLayout.post(new Runnable() {
            @Override
            public void run() {
                nothinglayout.setVisibility(View.GONE);
                swipeLayout.setRefreshing(true);
                cc_term_list.clear();
                adapter.notifyDataSetChanged();
                data_into_course_list();
            }
        });
    }


    private void delete(String idTermin, final String name) {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                if (response.getString("Success").equals("true")) {
                    //novej toast
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.term) + " " + name + " " + getActivity().getString(R.string.term_was_deleted)));
                }
                else{ //nepovedlo se
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.term) + " " + name + " " + getActivity().getString(R.string.term_was_not_deleted)));
                }
                update();
            }
        };

        //JSONObject parameters = null;
        Request.post(URL.Date.Remove(idTermin), response);
    }


    private void data_into_course_list() {
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                if (response.length() > 0)
                {
                    for (int j = 0; j < response.length(); j++)
                    {
                        cc_term_list.add(put_data(
                                    response.getJSONObject(j).getString("IDTermin"),
                                    response.getJSONObject(j).getString("IDSkupinaAktivit"),
                                    response.getJSONObject(j).getString("IDTest"),
                                    response.getJSONObject(j).getString("IDAktivita"),
                                    response.getJSONObject(j).getString("Nazev"),
                                    response.getJSONObject(j).getString("Ucebna"),
                                    response.getJSONObject(j).getString("NazevTyp"),
                                    TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("AktivniOD")),
                                    TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("AktivniDO")),
                                    TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("PrihlaseniDO")),
                                    response.getJSONObject(j).getString("IDAutor"),
                                    TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("DatumPrihlaseni")),
                                    TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("DatumOdhlaseni")),
                                    response.getJSONObject(j).getString("NutnePrihlaseni"),
                                    response.getJSONObject(j).getString("PocetStudentu"),
                                    response.getJSONObject(j).getString("Smazany"),
                                    response.getJSONObject(j).getString("PocetZapsanych"),
                                    response.getJSONObject(j).getString("AktivitaNazev"),
                                    response.getJSONObject(j).getString("PocetPokusu"),
                                    response.getJSONObject(j).getString("Zapsan")));
                    }
                }
                else{
                    Log.d("Chyba", "cc_term Data_into_course_list");
                    nothinglayout.setVisibility(View.VISIBLE);
                }
                swipeLayout.setRefreshing(false);
                adapter.notifyDataSetChanged();
            }
        };
        if(idSkupinaAktivit != null) {
            Request.get(URL.Date.GetDates(idSkupinaAktivit, "-1", "-1"), arrayResponse);
        }
        else if(idAktivita != null){
            Request.get(URL.Date.GetDates("-1", "-1", idAktivita), arrayResponse);
        }
        else if(idTest != null){
            Request.get(URL.Date.GetDates("-1", idTest, "-1"), arrayResponse);
        }
    }

    private Map<String, String> put_data(String idTermin, String idSkupinaAktivit, String idTest, String idAktivita, String nazev, String ucebna, String nazevTyp,
                                         String aktivniOD, String aktivniDO, String prihlaseniDO, String idAutor, String datumPrihlaseni, String datumOdhlaseni,
                                         String nutnePrihlaseni, String pocetStudentu, String smazany, String pocetZapsanych, String aktivitaNazev, String pocetPokusu, String zapsan) {
        HashMap<String, String> item = new HashMap<String, String>();

        item.put("idTermin",idTermin);
        item.put("idSkupinaAktivit",idSkupinaAktivit);
        item.put("idTest",idTest);
        item.put("idAktivita",idAktivita);
        item.put("nazev",nazev);
        item.put("ucebna",ucebna);
        item.put("nazevTyp",nazevTyp);
        item.put("aktivniOD",aktivniOD);
        item.put("aktivniDO",aktivniDO);
        item.put("prihlaseniDO",prihlaseniDO);
        item.put("idAutor",idAutor);
        item.put("datumPrihlaseni",datumPrihlaseni);
        item.put("datumOdhlaseni",datumOdhlaseni);
        item.put("nutnePrihlaseni",nutnePrihlaseni);
        item.put("pocetStudentu",pocetStudentu);
        item.put("smazany",smazany);
        item.put("pocetZapsanych",pocetZapsanych);
        item.put("aktivitaNazev",aktivitaNazev);
        item.put("pocetPokusu",pocetPokusu);
        item.put("zapsan",zapsan);

        return item;
    }

    @Override
    public void onRefresh() {
        update();
    }
}
