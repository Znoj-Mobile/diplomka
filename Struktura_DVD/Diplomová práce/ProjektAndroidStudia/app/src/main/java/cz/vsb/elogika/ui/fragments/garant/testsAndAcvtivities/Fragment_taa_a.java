package cz.vsb.elogika.ui.fragments.garant.testsAndAcvtivities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.ui.fragments.garant.courseConditions.Fragment_cc_add_term;
import cz.vsb.elogika.ui.fragments.garant.courseConditions.Fragment_cc_term;
import cz.vsb.elogika.utils.DividerItemDecoration;
import cz.vsb.elogika.utils.TimeLogika;

public class Fragment_taa_a extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private View mainView;
    private View rootView;
    private RecyclerView recyclerView;

    LinearLayout nothinglayout;
    SwipeRefreshLayout swipeLayout;

    ArrayList<Map<String, String>> taa_a_list;
    protected RecyclerView.Adapter adapter;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_taa_a, container, false);
        rootView = mainView.findViewById(R.id.recycler_view_with_button_swipe);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);

        swipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_update);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeResources(R.color.darker_cyan, R.color.material_blue_grey_800);

        nothinglayout = (LinearLayout) rootView.findViewById(R.id.nothing_to_show);
        nothinglayout.setVisibility(View.GONE);

        taa_a_list = new ArrayList<Map<String,String>>();


        this.adapter = new RecyclerView.Adapter() {
            class ViewHolderHeader extends RecyclerView.ViewHolder {
                public ViewHolderHeader(View itemView) {
                    super(itemView);
                }
            }

            class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
                public TextView title;
                public TextView evaluation;
                public TextView mandatory;
                public CheckBox checkbox;
                //public Button btn;

                public ViewHolder(View itemView) {
                    super(itemView);
                    itemView.setOnClickListener(this);
                    /*this.btn = (Button) itemView.findViewById(R.id.taa_a_button);*/
                    this.title = (TextView) itemView.findViewById(R.id.taa_a_title);
                    this.evaluation = (TextView) itemView.findViewById(R.id.taa_a_evaluation);
                    this.mandatory = (TextView) itemView.findViewById(R.id.taa_a_mandatory);
                    this.checkbox = (CheckBox) itemView.findViewById(R.id.taa_a_checkbox);
                    this.checkbox.setChecked(false);
                }


                @Override
                public void onClick(View view) {
                    final int position = getAdapterPosition();
                    //Toast.makeText(getActivity(), course_conditions_list.get(position).toString(), Toast.LENGTH_SHORT).show();
                    final Dialog dialog = new Dialog(getActivity());
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.fragment_taa_a_detail);
                    Button btn_term_list = (Button) dialog.findViewById(R.id.btn_taa_a_term_list);
                    btn_term_list.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            toFragment_cc_term(taa_a_list.get(position).get("idAktivita"), taa_a_list.get(position).get("nazev"));
                            dialog.dismiss();
                        }
                    });

                    Button btn_taa_a_add_term = (Button) dialog.findViewById(R.id.btn_taa_a_add_term);
                    btn_taa_a_add_term.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            toFragment_cc_add_term(taa_a_list.get(position).get("idAktivita"), taa_a_list.get(position).get("nazev"));
                            dialog.dismiss();
                        }
                    });

                    Button btn_taa_a_generated_tests = (Button) dialog.findViewById(R.id.btn_taa_a_generated_tests);
                    btn_taa_a_generated_tests.setVisibility(View.GONE);
                    Button btn_taa_a_solvers = (Button) dialog.findViewById(R.id.btn_taa_a_solvers);
                    btn_taa_a_solvers.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            toFragment_cc_solvers(taa_a_list.get(position).get("idAktivita"), taa_a_list.get(position).get("nazev"));
                            dialog.dismiss();
                        }
                    });

                    Button btn_taa_a_edit = (Button) dialog.findViewById(R.id.btn_taa_a_edit);
                    btn_taa_a_edit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            toFragment_cc_edit(taa_a_list.get(position));
                            dialog.dismiss();
                        }
                    });

                    Button btn_taa_a_delete = (Button) dialog.findViewById(R.id.btn_taa_a_delete);
                    btn_taa_a_delete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setTitle(R.string.activity_delete);
                            builder.setCancelable(false);
                            builder.setMessage(R.string.activity_delete_message);
                            builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog2, int which) {
                                    delete(taa_a_list.get(position));
                                    dialog.dismiss();
                                }
                            });

                            builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog2, int which) {
                                    ;
                                }
                            }).setIcon(android.R.drawable.ic_dialog_alert);
                            Dialog d = builder.create();
                            d.show();
                        }
                    });
                    dialog.show();
                }


            }

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view;
                switch (viewType) {
                    case 0:
                        view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_taa_activity_list_header, parent, false);
                        return new ViewHolderHeader(view);
                    case 1:
                        view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_taa_activity_list, parent, false);
                        return new ViewHolder(view);
                    default:
                        return null;
                }
            }
            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
                if (holder.getItemViewType() == 1) {
                    final ViewHolder viewHolder = (ViewHolder) holder;

                    viewHolder.title.setText(taa_a_list.get(position).get("nazev"));
                    viewHolder.evaluation.setText(taa_a_list.get(position).get("doporHodnoceni"));

                    if(taa_a_list.get(position).get("povinny").equals("true")){
                        viewHolder.mandatory.setText(getString(R.string.yes));
                    }
                    else{
                        viewHolder.mandatory.setText(getString(R.string.no));
                    }
                    final int finalPosition = position;
                    //objekt neni v seznamu k exportu → nezaskrtly checkbox
                    if(Fragment_TestsAndActivity.export_list.indexOf(taa_a_list.get(finalPosition)) == -1){
                        viewHolder.checkbox.setChecked(false);
                    }
                    else{
                        viewHolder.checkbox.setChecked(true);
                    }
                    viewHolder.checkbox.setOnClickListener(new View.OnClickListener() {
                                                               @Override
                                                               public void onClick(View v) {
                                                                   if(viewHolder.checkbox.isChecked()){
                                                                       //pridat k exportu
                                                                       Fragment_TestsAndActivity.export_list.add(taa_a_list.get(finalPosition));
                                                                   }
                                                                   else{
                                                                       //odebrat z exportu
                                                                       Fragment_TestsAndActivity.export_list.remove(taa_a_list.get(finalPosition));
                                                                   }
                                                               }
                                                           }
                    );
                }
            }

            @Override
            public int getItemViewType(int position)
            {
                    return 1;
            }

            @Override
            public int getItemCount() {
                return taa_a_list.size();
            }
        };

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        recyclerView.setHasFixedSize(true);
        //zaruci, ze se bude seznam aktualizovat jen pri swipu uplne nahore...
        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy <= 0){
                    swipeLayout.setEnabled(true);
                }
                else{
                    swipeLayout.setEnabled(false);
                }
            }
        });
        recyclerView.setAdapter(adapter);

        if(Integer.valueOf(Fragment_TestsAndActivity.groupActivityId) != 0) {
            update();
        }

        Button roundButton = (Button) rootView.findViewById(R.id.roundButton);
        roundButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment_taa_add_activity fragment_taa_add_a = new Fragment_taa_add_activity();
                getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment_taa_add_a).commit();
            }
        });

        return mainView;
    }

    public void get_activities() {

        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                if (response.length() > 0)
                {
                    for (int j = 0; j < response.length(); j++)
                    {
                        taa_a_list.add(put_data_a(
                                response.getJSONObject(j).getString("IDAktivita"),
                                response.getJSONObject(j).getString("PocetPokusu"),
                                response.getJSONObject(j).getString("Nazev"),
                                response.getJSONObject(j).getString("Popis"),
                                response.getJSONObject(j).getString("VzorVysl"),
                                response.getJSONObject(j).getString("DoporHodnoceni"),
                                response.getJSONObject(j).getString("Maximum"),
                                response.getJSONObject(j).getString("Minimum"),
                                response.getJSONObject(j).getString("Povinny"),
                                response.getJSONObject(j).getString("Vybiratelnost"),
                                TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("VyberDoData")),
                                response.getJSONObject(j).getString("OmezeniStudentu"),
                                response.getJSONObject(j).getString("TypOmezeni"),
                                response.getJSONObject(j).getString("PopisSoubor"),
                                response.getJSONObject(j).getString("VzorVyslSoubor"),
                                response.getJSONObject(j).getString("PopisSouborNazev"),
                                response.getJSONObject(j).getString("VzorVyslSouborNazev"),
                                response.getJSONObject(j).getString("PopisSouborContType"),
                                response.getJSONObject(j).getString("VzorVyslSouborContType"),
                                response.getJSONObject(j).getString("Smazana"),
                                response.getJSONObject(j).getString("IDTermin"),
                                response.getJSONObject(j).getString("NazevTermin"),
                                TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("AktivniOD")),
                                TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("AktivniDO")),
                                response.getJSONObject(j).getString("NazevSA"),
                                response.getJSONObject(j).getString("IsEditable")));
                    }
                }
                else{
                    Log.d("Chyba", "CourseConditions get_activities");
                    nothinglayout.setVisibility(View.VISIBLE);
                }
                swipeLayout.setRefreshing(false);
                adapter.notifyDataSetChanged();
            }
        };
        Request.get(URL.Activity.GetActivities(Fragment_TestsAndActivity.groupActivityId, User.id, User.roleID), arrayResponse);
    }

    private Map<String, String> put_data_a(String idAktivita, String pocetPokusu, String nazev, String popis, String vzorVysl, String doporHodnoceni, String maximum, String minimum, String povinny, String vybiratelnost, String vyberDoData,
                                           String omezeniStudentu, String typOmezeni, String popisSoubor, String vzorVyslSoubor, String popisSouborNazev, String vzorVyslSouborNazev, String popisSouborContType, String vzorVyslSouborContType,
                                           String smazana, String idTermin, String nazevTermin, String aktivniOD, String aktivniDO, String nazevSA, String isEditable) {
        HashMap<String, String> item = new HashMap<String, String>();

        item.put("idAktivita", idAktivita);
        item.put("pocetPokusu", pocetPokusu);
        item.put("nazev", nazev);
        item.put("popis", popis);
        item.put("vzorVysl", vzorVysl);
        item.put("doporHodnoceni", doporHodnoceni);
        item.put("maximum", maximum);
        item.put("minimum", minimum);
        item.put("povinny", povinny);
        item.put("vybiratelnost", vybiratelnost);
        item.put("vyberDoData", vyberDoData);
        item.put("omezeniStudentu", omezeniStudentu);
        item.put("typOmezeni", typOmezeni);
        item.put("popisSoubor", popisSoubor);
        item.put("vzorVyslSoubor", vzorVyslSoubor);
        item.put("popisSouborNazev", popisSouborNazev);
        item.put("vzorVyslSouborNazev", vzorVyslSouborNazev);
        item.put("popisSouborContType", popisSouborContType);
        item.put("vzorVyslSouborContType", vzorVyslSouborContType);
        item.put("smazana", smazana);
        item.put("idTermin", idTermin);
        item.put("nazevTermin", nazevTermin);
        item.put("aktivniOD", aktivniOD);
        item.put("aktivniDO", aktivniDO);
        item.put("nazevSA", nazevSA);
        item.put("isEditable", isEditable);

        return item;
    }

    public void update() {
        swipeLayout.post(new Runnable() {
            @Override
            public void run() {
                nothinglayout.setVisibility(View.GONE);
                swipeLayout.setRefreshing(true);
                taa_a_list.clear();
                adapter.notifyDataSetChanged();
                get_activities();
            }
        });
    }

    private void toFragment_cc_term(String idAktivita, String nazev) {
        Bundle bundle = new Bundle();
        bundle.putString("idAktivita", idAktivita);
        bundle.putString("aktivitaNazev", nazev);
        Fragment_cc_term fragment_cc_term = new Fragment_cc_term();
        fragment_cc_term.setArguments(bundle);
        getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment_cc_term).commit();
    }

    private void toFragment_cc_add_term(String idAktivita, String nazev) {
        Bundle bundle = new Bundle();
        bundle.putString("idAktivita", idAktivita);
        bundle.putString("aktivitaNazev", nazev);
        Fragment_cc_add_term fragment_cc_add_term = new Fragment_cc_add_term();
        fragment_cc_add_term.setArguments(bundle);
        getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment_cc_add_term).commit();
    }

    private void toFragment_cc_edit(Map<String, String> list) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("list", (Serializable) list);
        Fragment_taa_add_activity fragment_taa_add_a = new Fragment_taa_add_activity();
        fragment_taa_add_a.setArguments(bundle);
        getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment_taa_add_a).commit();
    }

    private void toFragment_cc_solvers(String idActivity, String nameActivity) {
        Bundle bundle = new Bundle();

        bundle.putString("idActivity", idActivity);
        bundle.putString("nameActivity", nameActivity);

        Fragment_taa_solvers fragment_taa_solvers = new Fragment_taa_solvers();
        fragment_taa_solvers.setArguments(bundle);
        getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment_taa_solvers).commit();
    }


    private void delete(final Map<String, String> list) {
        Response response = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException {
                Log.d("typ response: ", response.getString("Success"));
                if (response.getString("Success").equals("true")) {
                    //novej toast
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .type(SnackbarType.MULTI_LINE)
                                    .text(getActivity().getString(R.string.one_activity) + " " + list.get("nazev") + " " + getActivity().getString(R.string.activity_was_deleted)));

                } else { //nepovedlo se
                    Log.d("Fragment_taa_a", "delete(): " + response.toString());
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .type(SnackbarType.MULTI_LINE)
                                    .text(getActivity().getString(R.string.one_activity_4p) + " " + list.get("nazev") + " " + getActivity().getString(R.string.activity_was_not_deleted)));
                }
                update();
            }

        };
        Request.post(URL.Activity.Remove(list.get("idAktivita")), response);
    }

    @Override
    public void onRefresh() {
        update();
    }
}
