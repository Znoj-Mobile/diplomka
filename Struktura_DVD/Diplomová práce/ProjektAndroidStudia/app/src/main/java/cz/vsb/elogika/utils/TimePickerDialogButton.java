package cz.vsb.elogika.utils;

import android.app.TimePickerDialog;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.TimePicker;

import java.util.Calendar;
import java.util.TimeZone;

public class TimePickerDialogButton extends Button
{
    public int hour;
    public int minute;
    private static Calendar currentTime;


    /**
     * Konstruktory od Buttonu.
     */

    public TimePickerDialogButton(Context context)
    {
        super(context);
        init();
    }

    public TimePickerDialogButton(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }

    public TimePickerDialogButton(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        init();
    }

//    public TimePickerDialogButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes)
//    {
//        super(context, attrs, defStyleAttr, defStyleRes);
//        init();
//    }



    /**
     * Načte a nastaví aktuální čas.
     * Nastaví na tlačítko listener, který vyvolá dialog pro nastavení času.
     */
    private void init()
    {
        currentTime = Calendar.getInstance();
        currentTime.setTimeZone(TimeZone.getDefault());
        setCurrentTime();

        this.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {
                TimePickerDialog.OnTimeSetListener listener = new TimePickerDialog.OnTimeSetListener()
                {
                    public void onTimeSet(TimePicker view, int hourOfDay, int minuteOfHour)
                    {
                        hour = hourOfDay;
                        minute = minuteOfHour;
                        drawTime();
                    }
                };
                new TimePickerDialog(TimePickerDialogButton.this.getContext(), listener, hour, minute, true).show();
            }
        });
    }


    /**
     * Nastaví hodinu i minutu.
     * @param time čas musí být ve formátu hh:mm
     */
    public void setTime(String time)
    {
        String[] parts = time.split(":");
        this.hour = Integer.parseInt(parts[0]);
        this.minute = Integer.parseInt(parts[1]);
        drawTime();
    }



    /**
     * Nastaví hodinu i minutu.
     * @param hour hodina
     * @param minute minuta
     */
    public void setTime(int hour, int minute)
    {
        this.hour = hour;
        this.minute = minute;
        drawTime();
    }



    /**
     * Nastaví aktuální hodinu i minutu.
     */
    public void setCurrentTime()
    {
        this.hour = currentTime.get(Calendar.HOUR_OF_DAY);
        this.minute = currentTime.get(Calendar.MINUTE);
        drawTime();
    }



    /**
     * Nastaví aktuální hodinu.
     */
    public void setCurrentHour()
    {
        this.hour = currentTime.get(Calendar.HOUR_OF_DAY);
        this.minute = 0;
        drawTime();
    }



    /**
     * Zobrazí na tlačítku čas podle dat.
     */
    public void drawTime()
    {
        this.setText(getTime(this.hour, this.minute));
    }


    /**
     *
     * @return vrátí čas ve formátu hh:mm
     */
    public String getTime()
    {
        return getTime(this.hour, this.minute);
    }



    /**
     * Vrací kompletni datum s časem.
     * @return vrátí datum ve formátu yyyy-MM-dd'T'kk:mm:ss
     */
    public String getDateTime(DatePickerDialogButton date)
    {
        return DatePickerDialogButton.getDateTime(date.year, date.month, date.day, this.hour, this.minute);
    }



    private String getTime(int hours, int minutes)
    {
        return ((hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes));
    }
}
