package cz.vsb.elogika.ui.fragments.garant.courseConditions;

import android.app.Dialog;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cz.vsb.elogika.R;
import cz.vsb.elogika.common.view.SlidingTabLayout;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.Informations;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.Logging;

public class Fragment_CourseConditions extends Fragment {
    static Fragment_cc_pc fragment_cc_p;
    static Fragment_cc_pc fragment_cc_c;

    public static Fragment_CourseConditions reference;
    private View mainView;
    private View rootView;
    Bundle params;
    private int tab = 0;
    //pro minimalizaci dotazu si kolekce pro kombinovanou a prezencni formu studia udrzuji zde:
    static ArrayList<Map<String, String>> course_conditions_list;
    static ArrayList<Map<String, String>> course_conditions_combined_list;

    //pro minimum/maximum kurzu
    NumberPicker cc_dialog_p_min1;
    NumberPicker cc_dialog_p_min2;
    NumberPicker cc_dialog_p_min3;

    NumberPicker cc_dialog_p_max1;
    NumberPicker cc_dialog_p_max2;
    NumberPicker cc_dialog_p_max3;

    NumberPicker cc_dialog_c_min1;
    NumberPicker cc_dialog_c_min2;
    NumberPicker cc_dialog_c_min3;

    NumberPicker cc_dialog_c_max1;
    NumberPicker cc_dialog_c_max2;
    NumberPicker cc_dialog_c_max3;

    LinearLayout nothingLayout;
    ProgressBar progressBar;
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mainView = inflater.inflate(R.layout.fragment_cc, container, false);
        rootView = mainView.findViewById(R.id.tabs_container);
        reference = this;
        //skok na patricnou zalozku (pokud je zaznacen v argumentech)
        try {
            tab = getArguments().getInt("tab");
        }
        catch(Exception e){

        }

        setHead();
        Actionbar.newCategory(R.string.course_condition, this);
        Logging.logAccess("/Pages/Garant/PodminkyKurzu.aspx");

        ViewPager mViewPager = (ViewPager) rootView.findViewById(R.id.viewpager);
        mViewPager.setAdapter(new FragmentStatePagerAdapter(getFragmentManager()) {
                                  String[] tabNames = new String[] {
                                          getResources().getString(R.string.course_condition_present),
                                          getResources().getString(R.string.course_condition_combined)};

                                  @Override
                                  public int getCount() {
                                      return tabNames.length;
                                  }
                                  @Override
                                  public CharSequence getPageTitle(int position) {
                                      return tabNames[position];
                                  }

                                  @Override
                                  public Fragment getItem(int position) {

                                      params = new Bundle();
                                      switch (position)
                                      {
                                          case 0:
                                              //0 == prezencni
                                              params.putInt("study_form", 0);
                                              fragment_cc_p = new Fragment_cc_pc();
                                              fragment_cc_p.setArguments(params);
                                              return fragment_cc_p;
                                          case 1:
                                              //1 == kombinovana forma
                                              params.putInt("study_form", 1);
                                              fragment_cc_c = new Fragment_cc_pc();
                                              fragment_cc_c.setArguments(params);
                                              return fragment_cc_c;
                                          default : return null;
                                      }
                                  }
                              }


        );
        //nastaveni prislusne zalozky, pokud byla poslana jako argument fragmentu (jedna se hlavne o navrat zpet
        if(tab > 0){
            mViewPager.setCurrentItem(tab);
        }

        SlidingTabLayout mSlidingTabLayout = (SlidingTabLayout) rootView.findViewById(R.id.sliding_tabs);
        mSlidingTabLayout.setViewPager(mViewPager);

        //natazeni dat do patricnich seznamu
        course_conditions_list = new ArrayList<>();
        course_conditions_combined_list = new ArrayList<>();
        Data_into_course_list();
        return mainView;
    }

    private void setHead() {
        TextView cc_course = (TextView) mainView.findViewById(R.id.cc_course);
        cc_course.setText(Informations.courseInfoNames.get(Informations.courseInfoIds.indexOf(User.courseInfoId)));

        Button btn_add_activity_group = (Button) mainView.findViewById(R.id.btn_add_activity_group);
        btn_add_activity_group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle params = new Bundle();
                //0 == prezencni
                params.putInt("study_form", 0);
                Fragment_cc_add_ag fragment_cc_add_ag = new Fragment_cc_add_ag();
                fragment_cc_add_ag.setArguments(params);
                getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment_cc_add_ag).commit();
            }
        });

        Button btn_change_minMax_course = (Button) mainView.findViewById(R.id.btn_change_minMax_course);
        btn_change_minMax_course.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(getActivity());
                //obsah dialogu bude nad klavesnici
                dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                dialog.setCancelable(false);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.fragment_cc_dialog);

                nothingLayout = (LinearLayout) dialog.findViewById(R.id.nothing_to_show);
                progressBar = (ProgressBar) dialog.findViewById(R.id.progressbar);

                cc_dialog_p_min1 = (NumberPicker) dialog.findViewById(R.id.cc_dialog_p_min1);
                cc_dialog_p_min1.setMinValue(0);
                cc_dialog_p_min1.setMaxValue(9);
                cc_dialog_p_min2 = (NumberPicker) dialog.findViewById(R.id.cc_dialog_p_min2);
                cc_dialog_p_min2.setMinValue(0);
                cc_dialog_p_min2.setMaxValue(9);
                cc_dialog_p_min3 = (NumberPicker) dialog.findViewById(R.id.cc_dialog_p_min3);
                cc_dialog_p_min3.setMinValue(0);
                cc_dialog_p_min3.setMaxValue(9);

                cc_dialog_p_max1 = (NumberPicker) dialog.findViewById(R.id.cc_dialog_p_max1);
                cc_dialog_p_max1.setMinValue(0);
                cc_dialog_p_max1.setMaxValue(9);
                cc_dialog_p_max2 = (NumberPicker) dialog.findViewById(R.id.cc_dialog_p_max2);
                cc_dialog_p_max2.setMinValue(0);
                cc_dialog_p_max2.setMaxValue(9);
                cc_dialog_p_max3 = (NumberPicker) dialog.findViewById(R.id.cc_dialog_p_max3);
                cc_dialog_p_max3.setMinValue(0);
                cc_dialog_p_max3.setMaxValue(9);

                cc_dialog_c_min1 = (NumberPicker) dialog.findViewById(R.id.cc_dialog_c_min1);
                cc_dialog_c_min1.setMinValue(0);
                cc_dialog_c_min1.setMaxValue(9);
                cc_dialog_c_min2 = (NumberPicker) dialog.findViewById(R.id.cc_dialog_c_min2);
                cc_dialog_c_min2.setMinValue(0);
                cc_dialog_c_min2.setMaxValue(9);
                cc_dialog_c_min3 = (NumberPicker) dialog.findViewById(R.id.cc_dialog_c_min3);
                cc_dialog_c_min3.setMinValue(0);
                cc_dialog_c_min3.setMaxValue(9);

                cc_dialog_c_max1 = (NumberPicker) dialog.findViewById(R.id.cc_dialog_c_max1);
                cc_dialog_c_max1.setMinValue(0);
                cc_dialog_c_max1.setMaxValue(9);
                cc_dialog_c_max2 = (NumberPicker) dialog.findViewById(R.id.cc_dialog_c_max2);
                cc_dialog_c_max2.setMinValue(0);
                cc_dialog_c_max2.setMaxValue(9);
                cc_dialog_c_max3 = (NumberPicker) dialog.findViewById(R.id.cc_dialog_c_max3);
                cc_dialog_c_max3.setMinValue(0);
                cc_dialog_c_max3.setMaxValue(9);

                setLimits(0);    //prezencni
                setLimits(1);    //kombinovani

                Button btn_cc_dialog_save = (Button) dialog.findViewById(R.id.btn_cc_dialog_save);
                btn_cc_dialog_save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        save(0);    //prezencni
                        save(1);    //kombinovani
                        dialog.dismiss();
                    }
                });
                Button btn_cc_dialog_remove = (Button) dialog.findViewById(R.id.btn_cc_dialog_remove);
                btn_cc_dialog_remove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
        Button btn_course_summary = (Button) mainView.findViewById(R.id.btn_course_summary);
        btn_course_summary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment_cc_summary fragment_cc_summary = new Fragment_cc_summary();
                getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment_cc_summary).commit();
            }
        });
    }

    public void Data_into_course_list() {
        if(fragment_cc_p != null)
            fragment_cc_p.start_update();
        if(fragment_cc_c != null)
            fragment_cc_c.start_update();

        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                course_conditions_list.clear();
                course_conditions_combined_list.clear();
                if (response.length() > 0)
                {
                    for (int j = 0; j < response.length(); j++)
                    {
                        if(response.getJSONObject(j).getString("FormaStudia").equals("0")) {
                            course_conditions_list.add(put_data(
                                    response.getJSONObject(j).getString("IDSkupinaAktivit"),
                                    response.getJSONObject(j).getString("PocetPokusu"),
                                    response.getJSONObject(j).getString("Nazev"),
                                    response.getJSONObject(j).getString("IDRole"),
                                    response.getJSONObject(j).getString("IDKurzInfo"),
                                    response.getJSONObject(j).getString("Minimum"),
                                    response.getJSONObject(j).getString("Maximum"),
                                    response.getJSONObject(j).getString("Povinny"),
                                    response.getJSONObject(j).getString("ZapocitatDoVysledku"),
                                    response.getJSONObject(j).getString("FormaStudia"),
                                    response.getJSONObject(j).getString("Smazana"),
                                    response.getJSONObject(j).getString("IsEditable"),
                                    response.getJSONObject(j).getString("Nulovat")));
                        }
                        else{
                            course_conditions_combined_list.add(put_data(
                                    response.getJSONObject(j).getString("IDSkupinaAktivit"),
                                    response.getJSONObject(j).getString("PocetPokusu"),
                                    response.getJSONObject(j).getString("Nazev"),
                                    response.getJSONObject(j).getString("IDRole"),
                                    response.getJSONObject(j).getString("IDKurzInfo"),
                                    response.getJSONObject(j).getString("Minimum"),
                                    response.getJSONObject(j).getString("Maximum"),
                                    response.getJSONObject(j).getString("Povinny"),
                                    response.getJSONObject(j).getString("ZapocitatDoVysledku"),
                                    response.getJSONObject(j).getString("FormaStudia"),
                                    response.getJSONObject(j).getString("Smazana"),
                                    response.getJSONObject(j).getString("IsEditable"),
                                    response.getJSONObject(j).getString("Nulovat")));
                        }
                    }
                }
                else{
                    Log.d("Chyba", "CourseConditions Data_into_course_list");
                }
                //adapter.notifyDataSetChanged();

                if(fragment_cc_p != null)
                    fragment_cc_p.update();
                if(fragment_cc_c != null)
                    fragment_cc_c.update();
            }
        };
        Request.get(URL.CourseConditions.groupactivites(User.courseInfoId, User.id, User.roleID), arrayResponse);
    }

    private Map<String, String> put_data(String idSkupinaAktivit, String pocetPokusu, String nazev, String idRole, String idKurzInfo, String minimum, String maximum, String povinny,
                                         String zapocitatDoVysledku, String formaStudia, String smazana, String isEditable, String nulovat) {
        HashMap<String, String> item = new HashMap<String, String>();
        item.put("idSkupinaAktivit", idSkupinaAktivit);
        item.put("pocetPokusu", pocetPokusu);
        item.put("nazev",nazev);
        item.put("idRole",idRole);
        item.put("idKurzInfo",idKurzInfo);
        item.put("minimum",minimum);
        item.put("maximum",maximum);
        item.put("mandatory",povinny);
        item.put("zapocitatDoVysledku",zapocitatDoVysledku);
        item.put("formaStudia",formaStudia);
        item.put("smazana", smazana);
        item.put("isEditable", isEditable);
        item.put("nulovat", nulovat);
        return item;
    }

    private void setLimits(final int form) {
        Response response = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException {
                if (response.length() > 0)
                {
                    String[] min_max_values = response.getString("Limits").replace("[", "").replace("]","").split(",");
                    int min = Integer.valueOf(min_max_values[0]);
                    int max = Integer.valueOf(min_max_values[1]);
                    if(form == 0) {
                        cc_dialog_p_min1.setValue(min / 100);
                        min %= 100;
                        cc_dialog_p_min2.setValue(min / 10);
                        min %= 10;
                        cc_dialog_p_min3.setValue(min);

                        cc_dialog_p_max1.setValue(max / 100);
                        max %= 100;
                        cc_dialog_p_max2.setValue(max / 10);
                        max %= 10;
                        cc_dialog_p_max3.setValue(max);
                    }
                    else{
                        cc_dialog_c_min1.setValue(min / 100);
                        min %= 100;
                        cc_dialog_c_min2.setValue(min / 10);
                        min %= 10;
                        cc_dialog_c_min3.setValue(min);

                        cc_dialog_c_max1.setValue(max / 100);
                        max %= 100;
                        cc_dialog_c_max2.setValue(max / 10);
                        max %= 10;
                        cc_dialog_c_max3.setValue(max);
                    }
                }
                else{
                    Log.d("Chyba", "Fragment_cc setLimits()");
                }
            }
        };
        Request.get(URL.Course.GetKurzLimits(User.courseInfoId, form), response);
    }

    private void save(final int form) {
        final String form_string;
        if(form == 0) {
            form_string = getActivity().getString(R.string.present);
        }
        else{
            form_string = getActivity().getString(R.string.combinated);
        }


        Response response = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException {
                if (response.getString("Success").equals("true")) {
                    //novej toast
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(form_string + " " + getActivity().getString(R.string.minimum) + ", " + getActivity().getString(R.string.maximum) + " " + getActivity().getString(R.string.minmax_was_updated)));

                } else { //nepovedlo se
                    Log.d("Fragment_cc", "save(): " + response.toString());
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(form_string + " " + getActivity().getString(R.string.minimum) + ", " + getActivity().getString(R.string.maximum) + " " + getActivity().getString(R.string.minmax_was_not_updated)));
                }
            }
        };

        //post
        /*
        JSONObject parameters = new JSONObject();
        try {
            parameters.put("idCourseInfo", User.courseInfoId);
            parameters.put("studyForm", form);
            int pocet;
            //prezencni
            if(form == 0) {
                pocet = cc_dialog_p_min1.getValue() * 100 + cc_dialog_p_min2.getValue() * 10 + cc_dialog_p_min3.getValue();
                parameters.put("", pocet);
                pocet = cc_dialog_p_max1.getValue() * 100 + cc_dialog_p_max2.getValue() * 10 + cc_dialog_p_max3.getValue();
            }
            //kombinovani
            else {
                pocet = cc_dialog_c_min1.getValue() * 100 + cc_dialog_c_min2.getValue() * 10 + cc_dialog_c_min3.getValue();
                parameters.put("", pocet);
                pocet = cc_dialog_c_max1.getValue() * 100 + cc_dialog_c_max2.getValue() * 10 + cc_dialog_c_max3.getValue();
            }
            parameters.put("", pocet);

            Log.d("", parameters + "");

        } catch (JSONException e) {
            Log.d("Fragment_cc_add_ag", "insert() param: " + e.getMessage());
        }
        */

        int min;
        int max;
        //prezencni
        if(form == 0) {
            min = cc_dialog_p_min1.getValue() * 100 + cc_dialog_p_min2.getValue() * 10 + cc_dialog_p_min3.getValue();
            max = cc_dialog_p_max1.getValue() * 100 + cc_dialog_p_max2.getValue() * 10 + cc_dialog_p_max3.getValue();
        }
        //kombinovani
        else {
            min = cc_dialog_c_min1.getValue() * 100 + cc_dialog_c_min2.getValue() * 10 + cc_dialog_c_min3.getValue();
            max = cc_dialog_c_max1.getValue() * 100 + cc_dialog_c_max2.getValue() * 10 + cc_dialog_c_max3.getValue();
        }

        Request.get(URL.Course.SetKurzLimits(User.courseInfoId, form, min, max), response);
    }
}
