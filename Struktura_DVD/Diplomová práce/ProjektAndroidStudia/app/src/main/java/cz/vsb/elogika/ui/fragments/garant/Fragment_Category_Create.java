package cz.vsb.elogika.ui.fragments.garant;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.dataTypes.Category;
import cz.vsb.elogika.dataTypes.Category_step;
import cz.vsb.elogika.dataTypes.Chapter;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.dragndrop_ListView.DragSortRecycler;

public class Fragment_Category_Create extends Fragment {

    ArrayAdapter<Chapter> chapteradapter;
    ArrayList<Category_step> steplist = new ArrayList<>();
    ArrayList<Chapter> chapter_list;

    TextView category_slozitost;
    EditText category_name_text;
    EditText category_popis;

    ProgressBar progressBar;
    NumberPicker stepCounter;

    Category selectedCategory;
    int selectedChapterPosition = 0;
    boolean editing = true;
    boolean binding = false;

    int IDKategorie = -1;

    /**
     * @param chapteradapter adapter pro seznam kapitol
     * @param chapter_list seznam kapitol
     * @return vrací celý  objekt
     *
     */
    public Fragment addNewCategory(ArrayAdapter<Chapter> chapteradapter, ArrayList<Chapter> chapter_list, int selectedChapterPosition){
        this.editing = false;
        this.chapteradapter = chapteradapter;
        this.chapter_list = chapter_list;
        this.selectedChapterPosition = selectedChapterPosition;
        return this;
    }

    /**
     * @param chapteradapter adaptér pro seznam kapitol
     * @param chapter_list seznam kapitol
     * @param selectedCategory odkaz na upravovanou kategorii
     * @return vrací celý objekt
     */
    public Fragment editCategory(ArrayAdapter<Chapter> chapteradapter, ArrayList<Chapter> chapter_list, Category selectedCategory, int selectedChapterPosition){
        this.editing = true;
        this.chapteradapter = chapteradapter;
        this.chapter_list = chapter_list;
        this.IDKategorie = selectedCategory.IDKategorie;
        this.selectedCategory = selectedCategory;
        this.selectedChapterPosition = selectedChapterPosition;
        return this;
    }


    /**
     * Inicializace UI
     */
    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_category_new, container, false);

        progressBar = (ProgressBar) rootView.findViewById(R.id.category_progress);
        category_name_text = (EditText) rootView.findViewById(R.id.category_name);
        category_popis = (EditText) rootView.findViewById(R.id.category_popis);
        category_slozitost = (TextView) rootView.findViewById(R.id.category_slozitost);

        if(editing){
            category_name_text.setText(selectedCategory.Nazev);
            category_popis.setText(selectedCategory.Popis);
        }
        //tlacitko pro skok o patro vys
        rootView.findViewById(R.id.category_add_new_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editing)
                {
                    //TODO update metoda nefunguje updateCategory(selectedCategory);
                    //provizorní řešení takto:
//                    deleteCategory(IDKategorie);
//                    insertCategory();
                    updateCategory(IDKategorie);
                }
                else
                    insertCategory();
                Actionbar.jumpBack();
            }
        });
        return rootView;
    }


    int selectedChapterID = -1;
    RecyclerView.Adapter<Category_step> adap;

    /**
     * PostInicializace UI
     */
    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Spinner spinner = (Spinner) getActivity().findViewById(R.id.category_spinner);
        spinner.setAdapter(chapteradapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedChapterID = chapter_list.get(position).IDKapitola;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner.setSelection(selectedChapterPosition);

        RecyclerView recyclerView = (RecyclerView) getActivity().findViewById(R.id.category_add_new_listview);

        adap = new RecyclerView.Adapter<Category_step>() {
            @Override
            public Category_step onCreateViewHolder(ViewGroup viewGroup, int viewType) {
                View localView = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.fragment_category_new_item, viewGroup, false);
                Log.d("vytvoren", "" + "layout");
                Category_step vh = new Category_step(localView);
                return vh;
            }

            @Override
            public void onBindViewHolder(final Category_step category_step, final int position) {
                Log.d("binded na pozice", "" + position);
                binding = true;

                final Category_step stepFromData = steplist.get(position);

                category_step.category_new_step_position.setText(String.valueOf(steplist.get(position).Poradi));
                category_step.category_new_step_name.setText(steplist.get(position).Nazev_kroku);
                category_step.category_new_complexity.setText(String.valueOf(steplist.get(position).Slozitost));


                category_step.category_new_step_name.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                        if (!binding) stepFromData.Nazev_kroku = s.toString();
                    }
                });
                category_step.category_new_complexity.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                        if(!binding)
                        {
                            if(s.toString().isEmpty())
                                stepFromData.Slozitost = 1;
                            else
                                stepFromData.Slozitost = Integer.valueOf(s.toString());
                            category_slozitost.setText(getCurrentComplexity());
                        }
                    }
                });

                binding = false;
            }

            @Override
            public int getItemCount() {
                return steplist.size();
            }
        };


        recyclerView.setAdapter(adap);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(null);

        DragSortRecycler dragSortRecycler = new DragSortRecycler();
        dragSortRecycler.setViewHandleId(R.id.handler); //View you wish to use as the handle

        dragSortRecycler.setOnItemMovedListener(new DragSortRecycler.OnItemMovedListener() {
            @Override
            public void onItemMoved(int from, int to) {
                Log.d("recycler", "onItemMoved " + from + " to " + to);


                if (from < to) {
                    Category_step saved_category_step = steplist.get(from);
                    for (int i = from; i < to; i++) {
                        steplist.set(i, steplist.get(i + 1));
                    }
                    steplist.set(to, saved_category_step);
                }

                if (to < from) {
                    Category_step saved_category_step = steplist.get(from);
                    for (int i = from; i > to; i--) {
                        steplist.set(i, steplist.get(i - 1));
                    }
                    steplist.set(to, saved_category_step);
                }

                adap.notifyItemMoved(from, to);
            }
        });

        recyclerView.addItemDecoration(dragSortRecycler);
        recyclerView.addOnItemTouchListener(dragSortRecycler);
        recyclerView.setOnScrollListener(dragSortRecycler.getScrollListener());

        stepCounter = (NumberPicker) getActivity().findViewById(R.id.category_stepcounter);
        stepCounter.setMaxValue(120);
        stepCounter.setMinValue(0);
        stepCounter.setValue(1);
        stepCounter.setWrapSelectorWheel(false); //pretekani obsahu

        stepCounter.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                if (oldVal < newVal) {
                    for (int i = oldVal; i < newVal; i++) {
                        addItem((newVal), getActivity().getString(R.string.name) + newVal, newVal, -1);
                        adap.notifyItemInserted(newVal);
                    }
                }
                if (newVal < oldVal) {
                    for (int i = newVal; i < oldVal; i++) {
                        steplist.remove(steplist.size() - 1);
                        adap.notifyItemRemoved(steplist.size() - 1);
                    }
                }
                category_slozitost.setText(getCurrentComplexity());
            }
        });

        if(editing)
        {
            getSteps(IDKategorie);
            Actionbar.addSubcategory(getActivity().getString(R.string.category_edit), this);
        }
        else
        {
            addItem(1, getActivity().getString(R.string.name), 1, -1);
            Actionbar.addSubcategory(getActivity().getString(R.string.create_new_category), this);
        }

        category_slozitost.setText(getCurrentComplexity());
    }

    private void showdata() {
        for (Category_step category_step : steplist) {
            Log.d("iteruju: "," poradi: " + category_step.Poradi + " nazev: " + category_step.Nazev_kroku +" slozitost: "+ category_step.Slozitost);
        }
    }

    /**
     * přidá krok do seznamu kroků
     *
     * @param poradi pořadí kroku
     * @param jmeno název kroku
     * @param slozitost složitost kroku
     *
     */
    private void addItem( int poradi, String jmeno,int slozitost, int id)
    {
        View localView = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_category_new_item, null, false);
        Category_step c = new Category_step(localView);
        c.Poradi = poradi;
        c.Nazev_kroku = jmeno;
        c.Slozitost = slozitost;
        c.IDKrok = id;
        this.steplist.add(c);

    }


    int maxhodnota = 0;

    /**
     * vypočítá aktuální celkovou složitost, kterou uživatel zadal do kroků
     *
     * @return navrátí vypočtenou složitost
     *
     */
    private String getCurrentComplexity() {
        maxhodnota = 0;
        String s = "";
        ArrayList<Integer> list = new ArrayList<Integer>();


        for (Category_step category_step : this.steplist) {
            if (!list.contains(category_step.Slozitost))
            {
                if(maxhodnota < category_step.Slozitost)
                    maxhodnota = category_step.Slozitost;
                list.add(category_step.Slozitost);
                s += "(";
                for (int i = 0; i<steplist.size();i++)
                {
                    if (steplist.get(i).Slozitost == category_step.Slozitost)
                    {
                        s += (i+1) + ",";
                    }
                }
                s = s.substring(0,s.length()-1);
                s += ")+" + category_step.Slozitost + "+";
            }
        }
        if(s.length() == 0) return "0";
        s = s.substring(0,s.length()-1);
        return s;
    }


    /**
     * vloží kapitolu na server
     */
    private void insertCategory(){
        Response r = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException {
                Log.d("insertCategory", response.toString());
                if(response.getString("Success").equals("true")){
                    insertStep(response.getInt("IdCategory"));
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.category) + " " + getActivity().getString(R.string.category_was_inserted)));
                }
                else{
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.category_4p) + " " + getActivity().getString(R.string.category_was_not_inserted)));
                }
            }
        };
        JSONObject parameters = new JSONObject();
        try {
            //    parameters.put("IDKategorie", selectedChapterID);
            parameters.put("Nazev", category_name_text.getText());
            parameters.put("Popis", category_popis.getText());
            parameters.put("Rovnice",getCurrentComplexity());
            parameters.put("Hodnota",maxhodnota);
            //    parameters.put("Smazana",false);

            parameters.put("IDKategoriePuv",0);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Request.post(URL.Category.insert(selectedChapterID), parameters, r);
    }


    /**
     * vloží změny v kategorii na server
     */
    private void updateCategory(final int IDKategorie){
        Response r = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException {
                Log.d("updateCategory response", response.toString());
                if(response.getString("Success").equals("true")){
                    updateStep();
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.category) + " " + getActivity().getString(R.string.category_was_updated)));
                }
                else{
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.category_4p) + " " + getActivity().getString(R.string.category_was_not_updated)));
                }
            }
        };
        JSONObject parameters = new JSONObject();
        try {
            parameters.put("IDKategorie", IDKategorie);
            parameters.put("Nazev", category_name_text.getText());
            parameters.put("Popis", category_popis.getText());
            parameters.put("Rovnice",getCurrentComplexity());
            parameters.put("Hodnota",maxhodnota);
            parameters.put("Smazana","false");
            //  parameters.put("IDKategoriePuv", category.IDKategoriePuv);

            Log.d("update category request", parameters.toString(3));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Request.post(URL.Category.update(selectedChapterID), parameters, r);
    }


    private void deleteCategory(final int IdCategory) {
        Response r = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException {
            }
        };
        Request.post(URL.Category.remove(IdCategory), r);
    }


    /**
     * Stáhne kroky aktuální kategorie ze serveru
     *
     * @param IDKategorie id zvolené kategorie
     *
     */
    private void getSteps(int IDKategorie){
        ArrayResponse r = new ArrayResponse() {
            @Override
            public void onResponse(JSONArray response) throws JSONException {
                stepCounter.setValue(response.length());
                if (response.length() > 0)
                {
                    for (int j = 0; j < response.length(); j++)
                    {
                        addItem(j,
                                response.getJSONObject(j).getString("Nazev"),
                                response.getJSONObject(j).getInt("Obtiznost"),
                                response.getJSONObject(j).getInt("IDKrok"));
                    }
                    adap.notifyDataSetChanged();
                } //else getActivity().findViewById(R.id.news_nothing_to_show).setVisibility(View.VISIBLE);

                category_slozitost.setText(getCurrentComplexity());
            }
        };
        Request.get(URL.Step.GetSteps(IDKategorie), r);
    }

    /**
     * vloží kroky na server
     *
     * @param categoryId do které kategorie se kroky budou vkládat
     *
     */
    private void insertStep(int categoryId){
        Response r = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException {
                Log.d("insertStep", response.toString());
                if(response.getString("Success").equals("true")){
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.steps) + " " + getActivity().getString(R.string.steps_was_inserted)));
                }
                else{
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.steps) + " " + getActivity().getString(R.string.steps_was_not_inserted)));
                }
            }
        };
        JSONArray parameters = new JSONArray();
        try {
            for(int i = 0;i<steplist.size(); i++){
                JSONObject stepObject = new JSONObject();
                //    stepObject.put("IDKrok", -1);
                stepObject.put("Nazev", steplist.get(i).Nazev_kroku);
                stepObject.put("Obtiznost", steplist.get(i).Slozitost);
                stepObject.put("Poradi",(i+1));
                //    stepObject.put("Smazany",false);
                parameters.put(stepObject);
            }

            Log.d("insertStep request", parameters.toString(4));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Request.post(URL.Step.insert(categoryId), parameters, r);
    }

    /**
     * aktualizuje kroky na serveru
     *
     */
    private void updateStep(){
        Response r = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException {
                Log.d("updateStep", response.toString());
                if(response.getString("Success").equals("true")){
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.steps) + " " + getActivity().getString(R.string.steps_was_updated)));
                }
                else{
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.steps) + " " + getActivity().getString(R.string.steps_was_not_inserted)));
                }
            }
        };
        Response r2 = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException {
                Log.d("deleteSteps", response.toString());
            }
        };
        JSONArray parameters = new JSONArray();
        //JSONArray parametersDel = new JSONArray();
        try {
            for(int i = 0;i<steplist.size(); i++){
                JSONObject stepObject = new JSONObject();
                if(steplist.get(i).IDKrok > 0){
                    stepObject.put("IDKrok", steplist.get(i).IDKrok);
                }
                stepObject.put("Nazev", steplist.get(i).Nazev_kroku);
                stepObject.put("Obtiznost", steplist.get(i).Slozitost);
                stepObject.put("Poradi",(i+1));
                //    stepObject.put("Smazany",false);
                parameters.put(stepObject);
            }
            /*
            synchronized (steplist) {
                steplist.clear();
                getSteps(IDKategorie);
            }
            boolean remove = true;
            for(int i = 0;i<steplist.size(); i++){
                JSONObject stepObject2 = new JSONObject();
                for(int p = 0; p < parameters.length(); p++){
                    if(parameters.getJSONObject(p).has("IDKrok")){
                        if(parameters.getJSONObject(p).get("IDKrok").equals(steplist.get(i).IDKrok)){
                            remove = false;
                        }
                    }
                }
                if(remove){
                    stepObject2.put("IDKrok", steplist.get(i).IDKrok);
                    stepObject2.put("Nazev", steplist.get(i).Nazev_kroku);
                    stepObject2.put("Obtiznost", steplist.get(i).Slozitost);
                    stepObject2.put("Poradi",(i+1));
                    //    stepObject.put("Smazany",false);
                    parametersDel.put(stepObject2);
                }

            }

            Log.d("removeSteps request", parametersDel.toString(4));
            */
            Log.d("updateStep request", parameters.toString(4));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //TODO - smazani odebraných kroků
        //Request.post(URL.Step.DeleteFull(), parametersDel, r2);
        Request.post(URL.Step.update(), parameters, r);
    }

}
