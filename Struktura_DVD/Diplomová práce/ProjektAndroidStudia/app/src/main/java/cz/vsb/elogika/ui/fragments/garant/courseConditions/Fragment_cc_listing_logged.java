package cz.vsb.elogika.ui.fragments.garant.courseConditions;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.DividerItemDecoration;
import cz.vsb.elogika.utils.TimeLogika;

public class Fragment_cc_listing_logged extends Fragment implements SwipeRefreshLayout.OnRefreshListener{
    private View mainView;
    private View rootView;
    private RecyclerView recyclerView;

    SwipeRefreshLayout swipeLayout;
    LinearLayout nothinglayout;

    private RecyclerView.Adapter adapter;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_course_condition_listing_logged, container, false);
        rootView = mainView.findViewById(R.id.recycler_view_swipe);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);

        swipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_update);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeResources(R.color.darker_cyan, R.color.material_blue_grey_800);

        nothinglayout = (LinearLayout) rootView.findViewById(R.id.nothing_to_show);
        nothinglayout.setVisibility(View.GONE);
        this.adapter = new RecyclerView.Adapter() {
            class ViewHolderHeader extends RecyclerView.ViewHolder {
                public ViewHolderHeader(View itemView) {
                    super(itemView);
                }
            }

            class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
                public TextView order;
                public TextView login_date;
                public TextView surname;
                public TextView name;
                public TextView system_login;
                public TextView school_login;
                public TextView logged_try;
                public TextView points;
                public Button logout;

                public ViewHolder(View itemView) {
                    super(itemView);
                    itemView.setOnClickListener(this);
                    this.order = (TextView) itemView.findViewById(R.id.cc_logged_order);
                    this.login_date = (TextView) itemView.findViewById(R.id.cc_logged_login_date);
                    this.surname = (TextView) itemView.findViewById(R.id.cc_logged_surname);
                    this.name = (TextView) itemView.findViewById(R.id.cc_logged_person_name);
                    this.system_login = (TextView) itemView.findViewById(R.id.cc_logged_system_login);
                    this.school_login = (TextView) itemView.findViewById(R.id.cc_logged_school_login);
                    this.logged_try = (TextView) itemView.findViewById(R.id.cc_logged_try);
                    this.points = (TextView) itemView.findViewById(R.id.cc_logged_points);
                    this.logout = (Button) itemView.findViewById(R.id.cc_logged_logout);
                }

                @Override
                public void onClick(View view) {

                }
            }

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view;
                switch (viewType) {
                    case 1:
                        view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_course_conditions_listing_logged_list, parent, false);
                        return new ViewHolder(view);
                    default:
                        return null;
                }
            }
            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
                if (holder.getItemViewType() == 1) {
                    final ViewHolder viewHolder = (ViewHolder) holder;

                    viewHolder.order.setText(String.valueOf(position + 1) + ".");
                    viewHolder.login_date.setText(Fragment_cc_listing_students.logged.get(position).get("datumPrihlaseni"));
                    viewHolder.surname.setText(Fragment_cc_listing_students.logged.get(position).get("prijmeni"));
                    viewHolder.name.setText(Fragment_cc_listing_students.logged.get(position).get("jmeno"));
                    viewHolder.system_login.setText(Fragment_cc_listing_students.logged.get(position).get("systemLogin"));
                    viewHolder.school_login.setText(Fragment_cc_listing_students.logged.get(position).get("loginSkola"));
                    viewHolder.logged_try.setText(Fragment_cc_listing_students.logged.get(position).get("pokus"));
                    viewHolder.points.setText(Fragment_cc_listing_students.logged.get(position).get("vysledek"));

                    final int finalPosition = position;

                    if(User.isGarant) {
                        viewHolder.logout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setTitle(R.string.logout_title);
                                builder.setCancelable(false);
                                builder.setMessage(R.string.logout_student_question);
                                builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog2, int which) {
                                        logout(Fragment_cc_listing_students.logged.get(finalPosition).get("idUzivatel"), Fragment_cc_listing_students.logged.get(finalPosition).get("jmeno"), Fragment_cc_listing_students.logged.get(finalPosition).get("prijmeni"));
                                        update();
                                    }
                                });

                                builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog2, int which) {
                                        ;
                                    }
                                }).setIcon(android.R.drawable.ic_dialog_alert);
                                Dialog d = builder.create();
                                d.show();
                            }
                        });
                    }
                    else{
                        viewHolder.logout.setVisibility(View.INVISIBLE);
                    }
                }
            }

            @Override
            public int getItemViewType(int position)
            {
                return 1;
            }

            @Override
            public int getItemCount() {
                return Fragment_cc_listing_students.logged.size();
            }
        };

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        update();

        return mainView;
    }

    public void update() {
        swipeLayout.post(new Runnable() {
            @Override
            public void run() {
                nothinglayout.setVisibility(View.GONE);
                swipeLayout.setEnabled(true);
                swipeLayout.setRefreshing(true);
                Fragment_cc_listing_students.logged.clear();
                adapter.notifyDataSetChanged();
                data_into_list();
            }
        });
    }

    private void data_into_list() {
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                if (response.length() > 0)
                {
                    for (int j = 0; j < response.length(); j++)
                    {
                        Fragment_cc_listing_students.logged.add(put_data(
                                response.getJSONObject(j).getString("TitulPred"),
                                response.getJSONObject(j).getString("TitulZa"),
                                response.getJSONObject(j).getString("Email"),
                                response.getJSONObject(j).getString("IDUzivatel"),
                                response.getJSONObject(j).getString("SystemLogin"),
                                response.getJSONObject(j).getString("Jmeno"),
                                response.getJSONObject(j).getString("Prijmeni"),
                                response.getJSONObject(j).getString("LoginSkola"),
                                response.getJSONObject(j).getString("Vysledek"),
                                response.getJSONObject(j).getString("Splnil"),
                                TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("DatumPrihlaseni")),
                                response.getJSONObject(j).getString("Pokus")));
                    }
                }
                else{
                    Log.d("Chyba", "Fragment_cc_listing_logged data_into_list");
                    nothinglayout.setVisibility(View.VISIBLE);
                }
                swipeLayout.setRefreshing(false);
                adapter.notifyDataSetChanged();
            }
        };

        Request.get(URL.Student.GetStudentSummaryByIDTermin(Fragment_cc_listing_students.idTermin, User.schoolInfoID, Fragment_cc_listing_students.minPoints), arrayResponse);
    }

    private Map<String, String> put_data(String titulPred, String titulZa, String email, String idUzivatel, String systemLogin,
                                         String jmeno, String prijmeni, String loginSkola, String vysledek, String splnil,
                                         String datumPrihlaseni, String pokus) {
        HashMap<String, String> item = new HashMap<String, String>();

        item.put("titulPred", titulPred);
        item.put("titulZa", titulZa);
        item.put("email", email);
        item.put("idUzivatel", idUzivatel);
        item.put("systemLogin", systemLogin);
        item.put("jmeno", jmeno);
        item.put("prijmeni", prijmeni);
        item.put("loginSkola", loginSkola);
        item.put("vysledek", vysledek);
        item.put("splnil", splnil);
        item.put("datumPrihlaseni", datumPrihlaseni);
        item.put("pokus", pokus);

        return item;
    }

    @Override
    public void onRefresh() {
        update();
    }

    private void logout(String studentId, final String jmeno, final String prijmeni) {
        Response response = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException {
                Log.d("typ response: ", response.getString("Success"));
                if (response.getString("Success").equals("true")) {
                    //novej toast
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.student) + " " + jmeno + " " + prijmeni + " " + getActivity().getString(R.string.student_was_logout)));
                    update();
                } else { //nepovedlo se
                    Log.d("Fragment_cc_add_term", "insert(): " + response.toString());
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.student_s) + " " + jmeno + " " + prijmeni + " " + getActivity().getString(R.string.student_was_not_logout)));
                }
            }
        };
        JSONObject parameters = new JSONObject();

        Request.post(URL.Date.LogoutFromDateImmediately(studentId, Fragment_cc_listing_students.idTermin), parameters, response);
    }
}
