package cz.vsb.elogika.ui;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import cz.vsb.elogika.R;
import cz.vsb.elogika.ui.fragments.Fragment_PublicCourses;
import cz.vsb.elogika.utils.Actionbar;

/**
 * Tato aktivita se vytvoří po kliknutí na "Veřejné kurzy"
 * na přihlašovací obrazovce.
 * Zde se jen načte fragment určený pro veřejné kurzy,
 * ve kterém je změněno chvování po kliknutí na checkbox.
 */
public class AnonymousPublicCoursesActivity extends ActionBarActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);

        getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, new Fragment_PublicCourses()).setTransitionStyle(FragmentTransaction.TRANSIT_ENTER_MASK).commit();
    }



    @Override
    public void onBackPressed()
    {
        if (Actionbar.isInRoot()) super.onBackPressed();
        else Actionbar.jumpBack();
    }
}
