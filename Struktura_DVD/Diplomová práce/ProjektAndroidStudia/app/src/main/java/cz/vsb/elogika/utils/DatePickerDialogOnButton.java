package cz.vsb.elogika.utils;

import android.app.DatePickerDialog;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;

import java.util.Calendar;
import java.util.TimeZone;

import cz.vsb.elogika.R;


public class DatePickerDialogOnButton
{
    public int year;
    public int month;
    public int day;
    private boolean isAlreadySet;
    private static Calendar currentDate;
    private Button btn;



    /**
     * Nastaví aktuální čas.
     * Nastaví na tlačítko listener, který vyvolá dialog pro nastavení času.
     * Nastaví aktuální hodinu.
     */
    private void initialize(){
        currentDate = Calendar.getInstance();
        currentDate.setTimeZone(TimeZone.getDefault());
        this.isAlreadySet = false;
        //toto se nikdy nevypise, protoze se hned nastavuje cas na tlacitko,
        //ale muze se vytvorit konstruktor kde se tak nestane
        btn.setText(R.string.date);
        setListener();
    }

    /**
     * Nastaví na tlačítko listener, který vyvolá dialog pro nastavení data.
     * @param button tlačítko, na které se má aplikovat listener
     */
    public DatePickerDialogOnButton(Button button)
    {
        this.btn = button;
        initialize();
        setDate();
    }

    /**
     * Nastaví na tlačítko OnClickListener spolu s listenerem pro výběr data
     * @param ocl listener, který se na tlačítko nastaví
     */
    public void setOnClickListener(final View.OnClickListener ocl){
        btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onClickFunction();
                ocl.onClick(v);
            }
        });
    }

    /**
     * Nastaví na tlačítko listener, který vyvolá dialog pro nastavení data.
     * @param button tlačítko, na které se má aplikovat listener
     * @param year inicializační rok
     * @param month inicializační měsíc
     * @param day inicializační den
     */
    public DatePickerDialogOnButton(Button button, int year, int month, int day)
    {
        this.btn = button;
        initialize();
        this.year = year;
        this.month = month;
        this.day = day;
        this.btn.setText(getDate(this.year, this.month, this.day));
    }



    /**
     * Už uživatel nastavil datum?
     * @return true - ano, nastavil; false - ne, nenastavil
     */
    public boolean isAlreadySet()
    {
        return isAlreadySet;
    }



    /**
     * Nastaví rok, měsíc i den.
     * @param date datum musí být ve formátu yyyy-MM-dd'T'kk:mm:ss, kde hodiny, minuty a sekundy nejsou podstatné nebo ve formátu "dd.MM.yyyy"
     */
    public void setDate(String date)
    {
        if (TimeLogika.isServerTime(date)){
            date = TimeLogika.getFormatedDate(date);
        }
        //muze hazet chybu, pokud se vola spatny datum
        else{
            date = TimeLogika.getDate(date);
        }
        String[] date_field = date.split("\\.");
        this.day = Integer.parseInt(date_field[0]);
        this.month = Integer.parseInt(date_field[1]);
        this.year =Integer.parseInt( date_field[2]);
        this.btn.setText(getDate(this.year, this.month, this.day));
    }

    /**
     * Nastaví aktuální rok, měsíc i den.
     */
    public void setDate()
    {
        this.year = currentDate.get(Calendar.YEAR);
        this.month = currentDate.get(Calendar.MONTH) + 1;
        this.day = currentDate.get(Calendar.DAY_OF_MONTH);
        this.btn.setText(getDate(this.year, this.month, this.day));
    }

    /**
     * Nastaví komponentu pro čas i pro datum z času serveru
     * @param tpdb instance komponenty pro nastavení času
     * @param dateFromServer datum ve formátu serveru
     */
    public void setDateTime(TimePickerDialogOnButton tpdb, String dateFromServer) {
        //nastavi hodinu a minutu
        if (TimeLogika.isFormated(dateFromServer)) {
            tpdb.setTime(TimeLogika.getTime(dateFromServer));
        }
        else if (TimeLogika.isServerTime(dateFromServer)){
            tpdb.setTime(TimeLogika.getFormatedTime(dateFromServer));
        }
        //nejakej chybnej cas - na ten si dát pozor, takze to zatim bude padat :-)
        else{
            tpdb.setTime(TimeLogika.getFormatedTime(dateFromServer));
        }

        setDate(dateFromServer);
    }

    /**
     *
     * @return vrátí datum ve formátu yyyy-MM-dd'T'kk:mm:ss
     */
    public String getDate()
    {
        return createDate(this.year, this.month, this.day);
    }

    /**
     * Vrací kompletni datum s časem
     * @return vrátí datum ve formátu yyyy-MM-dd'T'kk:mm:ss
     */
    public String getDate(TimePickerDialogOnButton tpdb)
    {
        return getDate(this.year, this.month, this.day, tpdb.hour, tpdb.minute);
    }

    /**
     * Nastaví text tlačítka
     * @param text text který se na tlačítko vypíše
     */
    public void setText(String text){
        btn.setText(text);
    }

    private void setListener()
    {
        btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onClickFunction();
            }
        });
    }

    private void onClickFunction() {
        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day)
            {
                isAlreadySet = true;
                DatePickerDialogOnButton.this.year = year;
                DatePickerDialogOnButton.this.month = month + 1;
                DatePickerDialogOnButton.this.day = day;
                btn.setText(getDate(year, month + 1, day));
            }
        };
        new DatePickerDialog(btn.getContext(), listener, year, month - 1, day).show();
    }


    private String getDate(int years, int months, int days)
    {
        return (days + ". " + months + ". " + years);
    }



    private String createDate(int year, int month, int day)
    {
        return year + "-" + (month < 10 ? "0" + month : month) + "-" + (day < 10 ? "0" + day : day) + "T00:00:00";
    }

    private String getDate(int year, int month, int day, int hours, int minutes)
    {
        return year + "-" + (month < 10 ? "0" + month : month) + "-" + (day < 10 ? "0" + day : day) + "T" + (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes);
    }
}
