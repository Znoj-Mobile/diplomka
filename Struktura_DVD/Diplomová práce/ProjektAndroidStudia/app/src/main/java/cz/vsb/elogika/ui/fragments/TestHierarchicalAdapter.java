package cz.vsb.elogika.ui.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import cz.vsb.elogika.R;
import cz.vsb.elogika.adapters.HierarchicalAdapter;

public class TestHierarchicalAdapter extends Fragment
{
    final private int CHAPTER = 5;
    class Chapter extends HierarchicalAdapter.Item
    {
        public String title;
        public boolean isChecked;

        public Chapter()
        {
            this.type = CHAPTER;
        }
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.recycler_view, container, false);
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);

        HierarchicalAdapter hierarchicalAdapter = new HierarchicalAdapter()
        {
            class ViewHolderChapter extends Holder
            {
                public CheckBox checkBox;

                public ViewHolderChapter(View itemView)
                {
                    super(itemView);
                    this.checkBox = (CheckBox) itemView.findViewById(R.id.checkbox);
                }
            }



            @Override
            public Holder onCreateViewHolder(ViewGroup parent, int viewType)
            {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_template_administration_block_item2, parent, false);
                ViewGroup wrapper = getGroupingAndExpandingButton(parent);
                wrapper.addView(view);
                ViewHolderChapter holder = new ViewHolderChapter(wrapper);
                return holder;
            }


            @Override
            public void onBindViewHolder(Holder holder, int position, final Item item)
            {
                ((ViewHolderChapter) holder).checkBox.setText(((Chapter) item).title);
                ((ViewHolderChapter) holder).checkBox.setChecked(((Chapter) item).isChecked);
                ((ViewHolderChapter) holder).checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
                {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b)
                    {
                        ((Chapter) item).isChecked = b;
                    }
                });
            }
        };

        Chapter prvni = new Chapter();
        prvni.isChecked = false;
        prvni.title = "prvni";
        hierarchicalAdapter.addCategory(prvni);
        Chapter item;
        for (int j = 0; j < 20; j++)
        {
            item = new Chapter();
            item.title = "" + j;
            item.isChecked = false;
            hierarchicalAdapter.addCategory(item);
            for (int i = 0; i < 100; i++)
            {
                Chapter chapter = new Chapter();
                chapter.title = j + "." + i;
                chapter.isChecked = false;
                item.addChild(chapter);
                for (int k = 0; k < 100; k++)
                {
                    Chapter chapter2 = new Chapter();
                    chapter2.title = j + "." + i + "." + k;
                    chapter2.isChecked = false;
                    chapter.addChild(chapter2);
                }
            }
        }
        //HierarchicalAdapter.removeItem();

        recyclerView.setAdapter(hierarchicalAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(null);

        recyclerView.scrollToPosition(199000);

        return rootView;
    }
}
