package cz.vsb.elogika.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;

import cz.vsb.elogika.R;
import cz.vsb.elogika.utils.Actionbar;


public class Fragment_Web extends Fragment {
    static final String LOG_TAG = "Fragment_Web";
    private View rootView;
    String url = "";
    int name;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        url = getArguments().getString("url");
        name = getArguments().getInt("name");

        rootView = inflater.inflate(R.layout.web_view, container, false);
        Actionbar.newCategory(name, this);
        WebView wv = (WebView) rootView.findViewById(R.id.webView);
        WebSettings settings = wv.getSettings();
        settings.setJavaScriptEnabled(true);
        wv.loadUrl(url);

        return rootView;
    }
}