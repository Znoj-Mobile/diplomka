package cz.vsb.elogika.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.android.volley.VolleyError;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.Informations;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.Logging;



public class LoginRoleSelectActivity extends Activity
{
    Spinner schoolSelect;
    Spinner roleSelect;
    private int schoolID;
    private int roleID;
    private ProgressDialog progressDialog;
    private ProgressDialog progressDialog2;
    private boolean isItFirstTime; // Event na spinneru se zapíná ihned po načtení obrazovky
    private LoginRoleSelectActivity instance;



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_role_select_layout);

        Intent intent = getIntent();
        Informations.schoolIds = intent.getIntegerArrayListExtra("schoolIDs");
        Informations.schools = intent.getStringArrayListExtra("schools");
        Informations.roleIds = intent.getIntegerArrayListExtra("roleIDs");
        Informations.roles = intent.getStringArrayListExtra("roles");
        this.schoolID = intent.getIntExtra("schoolId", 0);
        this.roleID = intent.getIntExtra("roleId", 0);

        this.schoolSelect = (Spinner) findViewById(R.id.school_select);
        this.roleSelect = (Spinner) findViewById(R.id.role_select);
        this.schoolSelect.setAdapter(new ArrayAdapter(getApplication(), R.layout.spinner_item, Informations.schools));
        this.roleSelect.setAdapter(new ArrayAdapter(getApplication(), R.layout.spinner_item, Informations.roles));
        if(roleID != 0) {
            this.roleSelect.setSelection(Informations.roleIds.indexOf(roleID));
        }
        this.instance = this;
        this.isItFirstTime = false;

        progressDialog = new ProgressDialog(new ContextThemeWrapper(this, android.R.style.Theme_Holo_Light_Dialog));
        progressDialog.setIndeterminate(true);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                Request.cancelAllRequests();
            }
        });

        this.schoolSelect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (isItFirstTime) {
                    schoolID = 0;
                    progressDialog.setMessage(getString(R.string.gettingSchoolDialog));
                    progressDialog.show();
                    loadRoles(Informations.schoolIds.get(i));
                }
                isItFirstTime = true;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        progressDialog2 = new ProgressDialog(new ContextThemeWrapper(this, android.R.style.Theme_Holo_Light_Dialog));
        if(schoolID != 0){
            findViewById(R.id.imageView).setVisibility(View.GONE);
            findViewById(R.id.school_select).setVisibility(View.GONE);
            findViewById(R.id.role_select).setVisibility(View.GONE);
            findViewById(R.id.vstoupit_tlacitko).setVisibility(View.GONE);
            progressDialog2.setIndeterminate(true);
            progressDialog2.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog2.show();


            isItFirstTime = true;
            schoolSelect.setSelection(Informations.schoolIds.indexOf(schoolID));
            schoolID = 0;
            enter();
        }
        Logging.logAccess("/Pages/VyberSkoly/VyberSkoly.aspx");
        if(roleID != 0){
            findViewById(R.id.imageView).setVisibility(View.GONE);
            findViewById(R.id.school_select).setVisibility(View.GONE);
            findViewById(R.id.role_select).setVisibility(View.GONE);
            findViewById(R.id.vstoupit_tlacitko).setVisibility(View.GONE);
            progressDialog2 = new ProgressDialog(new ContextThemeWrapper(this, android.R.style.Theme_Holo_Light_Dialog));
            progressDialog2.setIndeterminate(true);
            progressDialog2.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog2.show();

            roleID = 0;
            enter();
        }
    }




    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(LoginRoleSelectActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }



    /**
     * Načte všechny role, které jsou pro uživatele a vybranou školu dostupné.
     * Poté se zobrazí ve spinneru.
     * @param schoolID id školy
     */
    private void loadRoles(int schoolID)
    {
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                Informations.roleIds.clear();
                Informations.roles.clear();
                for (int j = 0; j < response.length(); j++)
                {
//                    roleIDs.add(Integer.parseInt(response.getJSONObject(j).getString("IDRole")));
//                    roles.add(response.getJSONObject(j).getString("Nazev"));
                    Informations.roleIds.add(Integer.parseInt(response.getJSONObject(j).getString("IDRole")));
                    Informations.roles.add(response.getJSONObject(j).getString("Nazev"));
                }
                ArrayAdapter<String> adapter = new ArrayAdapter(getApplication(), R.layout.spinner_item, Informations.roles);
                roleSelect.setAdapter(adapter);
                progressDialog.dismiss();
            }

            public void onError(VolleyError error)
            {
                progressDialog.dismiss();
                super.onError(error);
            }

            @Override
            public void onException(JSONException exception)
            {
                progressDialog.dismiss();
                super.onException(exception);
            }
        };
        Request.get(URL.Role.GetRoles(User.id, schoolID), arrayResponse);
    }


    /**
     * Načtou se dostupné roky pro daného uživatele, vybranou školu a vybranou roli.
     */
    private void getYears()
    {
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                if (response.length() == 0)
                {
                    progressDialog.dismiss();
                    SnackbarManager.show(Snackbar.with(LoginRoleSelectActivity.this).text("Nemáte dostupné žádné roky."));
                    return;
                }

                Informations.yearIDs.clear();
                Informations.years.clear();

                for (int j = response.length() - 1; j >= 0; j--)
                {
                    Informations.yearIDs.add(response.getJSONObject(j).getInt("IDRok"));
                    Informations.years.add(response.getJSONObject(j).getString("Rok"));
                }

                getCourses(0);
            }

            public void onError(VolleyError error)
            {
                progressDialog.dismiss();
                super.onError(error);
            }

            @Override
            public void onException(JSONException exception)
            {
                progressDialog.dismiss();
                super.onException(exception);
            }
        };
        Request.get(URL.Year.YearByUser(User.id, User.schoolID, User.roleID), arrayResponse);
    }


    /**
     * Načte se schoolInfoID.
     */
    static public void getSchoolInfoID()
    {
        Response res = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                User.schoolInfoID = response.getInt("IDInfo");
            }
        };
        Request.get(URL.School.schoolinfobyyear(User.yearID, User.schoolID), res);
    }



    /**
     * Načte všechny dostupné kurzy pro zvolený rok.
     * @param year index v poli Informations.yearIDs
     */
    private void getCourses(final int year)
    {
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                if (response.length() > 0)
                {
                    User.yearID = Informations.yearIDs.get(year);
                    User.courseInfoId = response.getJSONObject(0).getInt("IDKurzInfo");
                    getSchoolInfoID();
                }
                else
                {
                    if (year < Informations.yearIDs.size() - 1) getCourses(year + 1);
                    else
                    {
                        progressDialog.dismiss();
                        SnackbarManager.show(Snackbar.with(LoginRoleSelectActivity.this).text(R.string.no_available_course));
                    }
                    return;
                }

                Informations.courseInfoIds.clear();
                Informations.courseIds.clear();
                Informations.courseInfoNames.clear();

                for (int j = 0; j < response.length(); j++)
                {
                    Informations.courseInfoIds.add(response.getJSONObject(j).getInt("IDKurzInfo"));
                    Informations.courseIds.add(response.getJSONObject(j).getInt("IDKurz"));
                    Informations.courseInfoNames.add(response.getJSONObject(j).getString("KurzSemestr"));
                }

                progressDialog.dismiss();
                progressDialog2.dismiss();

                Intent intent = new Intent(LoginRoleSelectActivity.this, ContainerActivity.class);
                if(roleSelect.getSelectedItem().toString().equals("Garant")){
                    intent = new Intent(LoginRoleSelectActivity.this, ContainerActivityGarant.class);
                }
                else if(roleSelect.getSelectedItem().toString().equals("Tutor")){
                    intent = new Intent(LoginRoleSelectActivity.this, ContainerActivityTutor.class);
                }
                startActivity(intent);
                finish();
            }

            public void onError(VolleyError error)
            {
                progressDialog.dismiss();
                super.onError(error);
            }

            @Override
            public void onException(JSONException exception)
            {
                progressDialog.dismiss();
                super.onException(exception);
            }
        };
        if (User.roleID == User.STUDENT) Request.get(URL.Course.CourseByStudent(Informations.yearIDs.get(year), User.id), arrayResponse);
        else if (User.roleID == User.TUTOR) Request.get(URL.Course.CourseByTutor(Informations.yearIDs.get(year), User.id), arrayResponse);
        else if (User.roleID == User.GARANT) Request.get(URL.Course.CourseByGarant(Informations.yearIDs.get(year), User.id), arrayResponse);
    }



    /**
     * Metoda zavoláná přímo z layoutu login_role_select_layout.
     */
    public void enter(View view){
        enter();
    }
    public void enter()
    {
        progressDialog.setMessage(getString(R.string.loading_available_years_and_courses));
        progressDialog.show();

        User.schoolID = Informations.schoolIds.get(this.schoolSelect.getSelectedItemPosition());
        User.school = Informations.schools.get(this.schoolSelect.getSelectedItemPosition());
        User.roleID = Informations.roleIds.get(this.roleSelect.getSelectedItemPosition());
        User.role = Informations.roles.get(this.roleSelect.getSelectedItemPosition());

        getYears();
    }
}
