package cz.vsb.elogika.ui;

import android.app.ActionBar;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.MenuItem;

import cz.vsb.elogika.R;
import cz.vsb.elogika.app.AppController;
import cz.vsb.elogika.sqliteDB.MyDB;


public abstract class BaseActivity extends FragmentActivity {

    private static MyDB DATABAZE; //databaze používana pro offline testy

    public static MyDB getDB(){
        if (DATABAZE == null)
            DATABAZE = new MyDB(AppController.getActivity());;
        return DATABAZE;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar = getActionBar();
        DATABAZE = new MyDB(this);;
        if (actionBar != null) {
            // show-up navigation
            actionBar.setDisplayHomeAsUpEnabled(true);

            // TODO set-up custom view to ActionBar according to UI design

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            default:
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
}
