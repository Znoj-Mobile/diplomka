package cz.vsb.elogika.communication;

import com.android.volley.toolbox.Volley;

import cz.vsb.elogika.app.AppController;

public class RequestQueue
{
    private static com.android.volley.RequestQueue requestQueue;


    /**
     * Vrací frontu požadavků pro přidání nového požadavku. Pokud fronta ještě neexistuje, je vytvořena.
     * @return
     */
    public static com.android.volley.RequestQueue get()
    {
        if (requestQueue == null) requestQueue = Volley.newRequestQueue(AppController.getActivity());
        return requestQueue;
    }
}