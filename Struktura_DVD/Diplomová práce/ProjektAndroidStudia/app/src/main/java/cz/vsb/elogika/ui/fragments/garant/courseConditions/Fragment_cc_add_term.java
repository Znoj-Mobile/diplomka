package cz.vsb.elogika.ui.fragments.garant.courseConditions;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.NumberPicker;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.DatePickerDialogOnButton;
import cz.vsb.elogika.utils.Logging;
import cz.vsb.elogika.utils.TimePickerDialogOnButton;

public class Fragment_cc_add_term extends Fragment {

    private View rootView;

    String idSkupinaAktivit;
    String idAktivita;
    String idTest;
    String aktivitaNazev;

    Map<String, String> cc_term_list;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_course_conditions_term_detail, container, false);

        aktivitaNazev = getArguments().getString("aktivitaNazev");
        String id = "0";
        String typ = "";
        try{
            idSkupinaAktivit = getArguments().getString("idSkupinaAktivit");
            id = idSkupinaAktivit;
            typ = "SA";
        } catch(Exception e){        }
        try{
            idAktivita = getArguments().getString("idAktivita");
            id = idAktivita;
            typ = "A";
        } catch(Exception e){        }
        try{
            idTest = getArguments().getString("idTest");
            id = idTest;
            typ = "T";
        } catch(Exception e){        }

        try {
            cc_term_list = (Map<String, String>) getArguments().getSerializable("list");
        } catch (Exception e){   }
        if(cc_term_list == null){
            Actionbar.addSubcategory(R.string.add_term, this);
            Logging.logAccess("/Pages/Other/Termin.aspx?ID_SATA=" + typ + "_" + id);
        }
        else{
            Actionbar.addSubcategory(R.string.edit_term, this);
            Logging.logAccess("/Pages/Other/Termin.aspx?ID=" + id);
        }

        getDataFromLayout();

        return rootView;
    }

    private void getDataFromLayout() {
        final Map<String, String> item = new HashMap<String, String>();
        //aktivni od
        final DatePickerDialogOnButton cc_td_date_active_from = new DatePickerDialogOnButton((Button) rootView.findViewById(R.id.cc_td_date_active_from));
        final TimePickerDialogOnButton cc_td_time_active_from = new TimePickerDialogOnButton((Button) rootView.findViewById(R.id.cc_td_time_active_from));

        //aktivni do
        final DatePickerDialogOnButton cc_td_date_active_to = new DatePickerDialogOnButton((Button) rootView.findViewById(R.id.cc_td_date_active_to));
        final TimePickerDialogOnButton cc_td_time_active_to = new TimePickerDialogOnButton((Button) rootView.findViewById(R.id.cc_td_time_active_to));

        //datum moznosti prihlaseni
        final DatePickerDialogOnButton cc_td_date_login_from = new DatePickerDialogOnButton((Button) rootView.findViewById(R.id.cc_td_date_login_from));
        final TimePickerDialogOnButton cc_td_time_login_from = new TimePickerDialogOnButton((Button) rootView.findViewById(R.id.cc_td_time_login_from));

        //konec moznosti prihlaseni
        final DatePickerDialogOnButton cc_td_date_login_to = new DatePickerDialogOnButton((Button) rootView.findViewById(R.id.cc_td_date_login_to));
        final TimePickerDialogOnButton cc_td_time_login_to = new TimePickerDialogOnButton((Button) rootView.findViewById(R.id.cc_td_time_login_to));

        //datum moznosti odhlaseni
        final DatePickerDialogOnButton cc_td_date_logout = new DatePickerDialogOnButton((Button) rootView.findViewById(R.id.cc_td_date_logout));
        final TimePickerDialogOnButton cc_td_time_logout = new TimePickerDialogOnButton((Button) rootView.findViewById(R.id.cc_td_time_logout));

        final EditText cc_td_room = (EditText) rootView.findViewById(R.id.cc_td_room);

        final CheckBox cc_td_need_login = (CheckBox) rootView.findViewById(R.id.cc_td_need_login);

        final NumberPicker cc_td_max_students1 = (NumberPicker) rootView.findViewById(R.id.cc_td_max_students1);
        cc_td_max_students1.setMaxValue(9);
        cc_td_max_students1.setMinValue(0);
        final NumberPicker cc_td_max_students2 = (NumberPicker) rootView.findViewById(R.id.cc_td_max_students2);
        cc_td_max_students2.setMaxValue(9);
        cc_td_max_students2.setMinValue(0);
        final NumberPicker cc_td_max_students3 = (NumberPicker) rootView.findViewById(R.id.cc_td_max_students3);
        cc_td_max_students3.setMaxValue(9);
        cc_td_max_students3.setMinValue(0);

        final NumberPicker cc_td_number_of_efforts1 = (NumberPicker) rootView.findViewById(R.id.cc_td_number_of_efforts1);
        cc_td_number_of_efforts1.setMinValue(0);
        cc_td_number_of_efforts1.setMaxValue(9);
        final NumberPicker cc_td_number_of_efforts2 = (NumberPicker) rootView.findViewById(R.id.cc_td_number_of_efforts2);
        cc_td_number_of_efforts2.setMinValue(0);
        cc_td_number_of_efforts2.setMaxValue(9);
        final NumberPicker cc_td_number_of_efforts3 = (NumberPicker) rootView.findViewById(R.id.cc_td_number_of_efforts3);
        cc_td_number_of_efforts3.setMinValue(0);
        cc_td_number_of_efforts3.setMaxValue(9);


        Button btn_send = (Button) rootView.findViewById(R.id.btn_send);


        if(cc_term_list != null){
            it_is_update();
        }
        else {
            btn_send.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle(R.string.term_insert);
                    builder.setCancelable(false);
                    builder.setMessage(R.string.term_insert_message);
                    builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog2, int which) {
                            //aktualizace hodnot
                            //TODO - nejaka brutalni kontrola jake datum smi a nesmi byt zadano - takze urcite ne prepisovani kolekce

                            EditText cc_td_title = (EditText) rootView.findViewById(R.id.cc_td_title);
                            item.put("Nazev", cc_td_title.getText().toString());
                            item.put("AktivniOD", cc_td_date_active_from.getDate(cc_td_time_active_from));

                            item.put("AktivniDO", cc_td_date_active_to.getDate(cc_td_time_active_to));

                            item.put("DatumPrihlaseni", cc_td_date_login_from.getDate(cc_td_time_login_from));

                            item.put("PrihlaseniDO", cc_td_date_login_to.getDate(cc_td_time_login_to));

                            item.put("DatumOdhlaseni", cc_td_date_logout.getDate(cc_td_time_logout));

                            item.put("Ucebna", cc_td_room.getText().toString());
                            if (cc_td_need_login.isChecked()) {
                                item.put("NutnePrihlaseni", "true");
                            } else {
                                item.put("NutnePrihlaseni", "false");
                            }

                            int pocet = cc_td_max_students1.getValue() * 100 + cc_td_max_students2.getValue() * 10 + cc_td_max_students3.getValue();
                            item.put("PocetStudentu", pocet + "");
                            pocet = cc_td_number_of_efforts1.getValue() * 100 + cc_td_number_of_efforts2.getValue() * 10 + cc_td_number_of_efforts3.getValue();
                            item.put("PocetPokusu", pocet + "");

                            insert(item);

                        }
                    });
                    builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog2, int which) {
                            ;
                        }
                    }).setIcon(android.R.drawable.ic_dialog_alert);
                    Dialog d = builder.create();
                    d.show();
                }
            });

        }
        Button btn_cancel = (Button) rootView.findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                if(cc_term_list == null){
                    builder.setTitle(R.string.term_cancel);
                    builder.setMessage(R.string.term_cancel_message);
                }
                else{
                    builder.setTitle(R.string.term_cancel_update);
                    builder.setMessage(R.string.term_cancel_update_message);
                }
                builder.setCancelable(false);
                builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog2, int which) {
                        //Bundle bundle = new Bundle();
                        Actionbar.jumpBack();
                        /*
                        //zpet na patricnou zalozku
                        bundle.putInt("tab", Integer.valueOf(getArguments().getString("tab")));
                        Fragment_CourseConditions fragment_cc = new Fragment_CourseConditions();
                        fragment_cc.setArguments(bundle);
                        getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment_cc).commit();
                        */
                    }
                });

                builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog2, int which) {
                        ;
                    }
                }).setIcon(android.R.drawable.ic_dialog_alert);
                Dialog d = builder.create();
                d.show();
            }
        });
    }

    private void it_is_update() {

        final EditText cc_td_title = (EditText) rootView.findViewById(R.id.cc_td_title);
        cc_td_title.setText(cc_term_list.get("nazev"));

        //aktivni od
        final DatePickerDialogOnButton cc_td_date_active_from = new DatePickerDialogOnButton((Button) rootView.findViewById(R.id.cc_td_date_active_from));
        final TimePickerDialogOnButton cc_td_time_active_from = new TimePickerDialogOnButton((Button) rootView.findViewById(R.id.cc_td_time_active_from));
        cc_td_date_active_from.setDateTime(cc_td_time_active_from, cc_term_list.get("aktivniOD"));
                /*
                String s = cc_term_list.get("aktivniOD");
                Log.d("cc_term aktivniOD", s);
                int[] date_active_from = TimeLogika.getSeparatedTime(s);
                cc_td_date_active_from.updateDate(date_active_from[0], date_active_from[1], date_active_from[2]);
                cc_td_time_active_from.setCurrentHour(date_active_from[3]);
                cc_td_time_active_from.setCurrentMinute(date_active_from[4]);
                */


        //aktivni do
        final DatePickerDialogOnButton cc_td_date_active_to = new DatePickerDialogOnButton((Button) rootView.findViewById(R.id.cc_td_date_active_to));
        final TimePickerDialogOnButton cc_td_time_active_to = new TimePickerDialogOnButton((Button) rootView.findViewById(R.id.cc_td_time_active_to));
        cc_td_date_active_to.setDateTime(cc_td_time_active_to, cc_term_list.get("aktivniDO"));

                /*
                int[] date_active_to = TimeLogika.getSeparatedTime(cc_term_list.get("aktivniDO"));
                cc_td_date_active_to.updateDate(date_active_to[0], date_active_to[1], date_active_to[2]);
                cc_td_time_active_to.setCurrentHour(date_active_to[3]);
                cc_td_time_active_to.setCurrentMinute(date_active_to[4]);
                */

        //datum moznosti prihlaseni
        final DatePickerDialogOnButton cc_td_date_login_from = new DatePickerDialogOnButton((Button) rootView.findViewById(R.id.cc_td_date_login_from));
        final TimePickerDialogOnButton cc_td_time_login_from = new TimePickerDialogOnButton((Button) rootView.findViewById(R.id.cc_td_time_login_from));
        cc_td_date_login_from.setDateTime(cc_td_time_login_from, cc_term_list.get("datumPrihlaseni"));

                /*
                int[] date_login_from = TimeLogika.getSeparatedTime(cc_term_list.get("datumPrihlaseni"));
                cc_td_date_login_from.updateDate(date_login_from[0], date_login_from[1], date_login_from[2]);
                cc_td_time_login_from.setCurrentHour(date_login_from[3]);
                cc_td_time_login_from.setCurrentMinute(date_login_from[4]);
                */

        //konec moznosti prihlaseni
        final DatePickerDialogOnButton cc_td_date_login_to = new DatePickerDialogOnButton((Button) rootView.findViewById(R.id.cc_td_date_login_to));
        final TimePickerDialogOnButton cc_td_time_login_to = new TimePickerDialogOnButton((Button) rootView.findViewById(R.id.cc_td_time_login_to));
        cc_td_date_login_to.setDateTime(cc_td_time_login_to, cc_term_list.get("prihlaseniDO"));

                /*
                int[] date_login_to = TimeLogika.getSeparatedTime(cc_term_list.get("prihlaseniDO"));
                cc_td_date_login_to.updateDate(date_login_to[0], date_login_to[1], date_login_to[2]);
                cc_td_time_login_to.setCurrentHour(date_login_to[3]);
                cc_td_time_login_to.setCurrentMinute(date_login_to[4]);
                */

        //datum moznosti odhlaseni
        final DatePickerDialogOnButton cc_td_date_logout = new DatePickerDialogOnButton((Button) rootView.findViewById(R.id.cc_td_date_logout));
        final TimePickerDialogOnButton cc_td_time_logout = new TimePickerDialogOnButton((Button) rootView.findViewById(R.id.cc_td_time_logout));
        cc_td_date_logout.setDateTime(cc_td_time_logout, cc_term_list.get("datumOdhlaseni"));

                /*
                int[] td_date_logout = TimeLogika.getSeparatedTime(cc_term_list.get("datumOdhlaseni"));
                cc_td_date_logout.updateDate(td_date_logout[0], td_date_logout[1], td_date_logout[2]);
                cc_td_time_logout.setCurrentHour(td_date_logout[3]);
                cc_td_time_logout.setCurrentMinute(td_date_logout[4]);
                */

        final EditText cc_td_room = (EditText) rootView.findViewById(R.id.cc_td_room);
        cc_td_room.setText(cc_term_list.get("ucebna"));

        final CheckBox cc_td_need_login = (CheckBox) rootView.findViewById(R.id.cc_td_need_login);
        if(cc_term_list.get("nutnePrihlaseni").equals("true")){
            cc_td_need_login.setChecked(true);
        }
        else{
            cc_td_need_login.setChecked(false);
        }

        final NumberPicker cc_td_max_students1 = (NumberPicker) rootView.findViewById(R.id.cc_td_max_students1);
        cc_td_max_students1.setMaxValue(9);
        cc_td_max_students1.setMinValue(0);
        final NumberPicker cc_td_max_students2 = (NumberPicker) rootView.findViewById(R.id.cc_td_max_students2);
        cc_td_max_students2.setMaxValue(9);
        cc_td_max_students2.setMinValue(0);
        final NumberPicker cc_td_max_students3 = (NumberPicker) rootView.findViewById(R.id.cc_td_max_students3);
        cc_td_max_students3.setMaxValue(9);
        cc_td_max_students3.setMinValue(0);
        int max_studentu = Integer.valueOf(cc_term_list.get("pocetStudentu"));
        cc_td_max_students1.setValue(max_studentu/100);
        max_studentu %= 100;
        cc_td_max_students2.setValue(max_studentu/10);
        max_studentu %= 10;
        cc_td_max_students3.setValue(max_studentu);

        final NumberPicker cc_td_number_of_efforts1 = (NumberPicker) rootView.findViewById(R.id.cc_td_number_of_efforts1);
        cc_td_number_of_efforts1.setMinValue(0);
        cc_td_number_of_efforts1.setMaxValue(9);
        final NumberPicker cc_td_number_of_efforts2 = (NumberPicker) rootView.findViewById(R.id.cc_td_number_of_efforts2);
        cc_td_number_of_efforts2.setMinValue(0);
        cc_td_number_of_efforts2.setMaxValue(9);
        final NumberPicker cc_td_number_of_efforts3 = (NumberPicker) rootView.findViewById(R.id.cc_td_number_of_efforts3);
        cc_td_number_of_efforts3.setMinValue(0);
        cc_td_number_of_efforts3.setMaxValue(9);

        int pokusu = Integer.valueOf(cc_term_list.get("pocetPokusu"));
        cc_td_number_of_efforts1.setValue(pokusu/100);
        pokusu %= 100;
        cc_td_number_of_efforts2.setValue(pokusu/10);
        pokusu %= 10;
        cc_td_number_of_efforts3.setValue(pokusu);

        Button btn_send = (Button) rootView.findViewById(R.id.btn_send);
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(R.string.term_update);
                builder.setCancelable(false);
                builder.setMessage(R.string.term_update_message);
                builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog2, int which) {
                        //aktualizace hodnot
                        //TODO - nejaka brutalni kontrola jake datum smi a nesmi byt zadano - takze urcite ne prepisovani kolekce
                        cc_term_list.put("nazev", cc_td_title.getText().toString());
                        cc_term_list.put("aktivniOD", cc_td_date_active_from.getDate(cc_td_time_active_from));


                        cc_term_list.put("aktivniDO", cc_td_date_active_to.getDate(cc_td_time_active_to));

                        cc_term_list.put("datumPrihlaseni", cc_td_date_login_from.getDate(cc_td_time_login_from));

                        cc_term_list.put("prihlaseniDO", cc_td_date_login_to.getDate(cc_td_time_login_to));

                        cc_term_list.put("datumOdhlaseni", cc_td_date_logout.getDate(cc_td_time_logout));

                        cc_term_list.put("ucebna", cc_td_room.getText().toString());
                        if (cc_td_need_login.isChecked()) {
                            cc_term_list.put("nutnePrihlaseni", "true");
                        } else {
                            cc_term_list.put("nutnePrihlaseni", "false");
                        }
                        int pocet = cc_td_max_students1.getValue() * 100 + cc_td_max_students2.getValue() * 10 + cc_td_max_students3.getValue();
                        cc_term_list.put("pocetStudentu", pocet + "");
                        pocet = cc_td_number_of_efforts1.getValue() * 100 + cc_td_number_of_efforts2.getValue() * 10 + cc_td_number_of_efforts3.getValue();
                        cc_term_list.put("pocetPokusu", pocet + "");


                        update(cc_term_list);
                    }
                });
                builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog2, int which) {
                        ;
                    }
                }).setIcon(android.R.drawable.ic_dialog_alert);
                Dialog d = builder.create();
                d.show();
            }
        });
    }

    private void update(final Map<String, String> item) {
        Logging.logAccess("/Pages/Other/TerminDetail/TerminSummary.aspx");
        Response response = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                Log.d("typ response: ", response.getString("Success"));
                if (response.getString("Success").equals("true")) {
                    //novej toast
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.term) + " " + item.get("nazev") + " " + getActivity().getString(R.string.term_was_updated)));
                    Actionbar.jumpBack();
                    /*
                    Bundle bundle = new Bundle();
                    if(idSkupinaAktivit != null){
                        bundle.putString("idSkupinaAktivit", idSkupinaAktivit);
                    }
                    else if(idAktivita != null){
                        bundle.putString("idAktivita", idAktivita);

                    }
                    else if(idTest != null){
                        bundle.putString("idTest", idTest);

                    }
                    Fragment_cc_term fragment_cc_term = new Fragment_cc_term();
                    fragment_cc_term.setArguments(bundle);
                    getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment_cc_term).commit();
                    */
                }
                else{ //nepovedlo se
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.term) + " " + item.get("nazev") + " " + getActivity().getString(R.string.term_was_not_updated)));
                }
            }
        };
        JSONObject parameters = new JSONObject();
        try
        {
            parameters.put("IDTermin", item.get("idTermin"));
            parameters.put("IDSkupinaAktivit", item.get("idSkupinaAktivit"));
            parameters.put("IDTest", item.get("idTest"));
            parameters.put("IDAktivita", item.get("idAktivita"));
            parameters.put("Nazev", item.get("nazev"));
            parameters.put("Ucebna", item.get("ucebna"));
            parameters.put("NazevTyp", item.get("nazevTyp"));
            parameters.put("AktivniOD", item.get("aktivniOD"));
            parameters.put("AktivniDO", item.get("aktivniDO"));
            parameters.put("PrihlaseniDO", item.get("prihlaseniDO"));
            parameters.put("OdevzdaniDO", item.get("prihlaseniDO"));
            parameters.put("IDAutor", item.get("idAutor"));
            parameters.put("DatumPrihlaseni", item.get("datumPrihlaseni"));
            parameters.put("DatumOdhlaseni", item.get("datumOdhlaseni"));
            parameters.put("NutnePrihlaseni", item.get("nutnePrihlaseni"));
            parameters.put("PocetStudentu", item.get("pocetStudentu"));
            parameters.put("Smazany", item.get("smazany"));
            parameters.put("PocetZapsanych", item.get("pocetZapsanych"));
            parameters.put("AktivitaNazev", item.get("aktivitaNazev"));
            parameters.put("PocetPokusu", item.get("pocetPokusu"));
            parameters.put("Zapsan", item.get("zapsan"));
            Log.d("URL.Date.Update()", "" + parameters);
        }
        catch (JSONException e) {}
        Request.post(URL.Date.Update(), parameters, response);
    }

    private void insert(final Map<String, String> item) {
        Logging.logAccess("/Pages/Other/TerminDetail/TerminSummary.aspx");
        Response response = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException {
                Log.d("typ response: ", response.getString("Success"));
                if (response.getString("Success").equals("true")) {
                    //novej toast
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.term) + " " + item.get("Nazev") + " " + getActivity().getString(R.string.term_was_inserted)));

                    //Bundle bundle = new Bundle();
                    Actionbar.jumpBack();
                    /*
                    //zpet na patricnou zalozku
                    bundle.putInt("tab", Integer.valueOf(getArguments().getString("tab")));
                    Fragment_CourseConditions fragment_cc = new Fragment_CourseConditions();
                    fragment_cc.setArguments(bundle);
                    getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment_cc).commit();
                    */
                } else { //nepovedlo se
                    Log.d("Fragment_cc_add_term", "insert(): " + response.toString());
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.term) + " " + item.get("Nazev") + " " + getActivity().getString(R.string.term_was_not_inserted)));
                }
            }
        };
        JSONObject parameters = new JSONObject();
        try {
            if(idSkupinaAktivit != null) {
                parameters.put("IDSkupinaAktivit", idSkupinaAktivit);
                parameters.put("IDAktivita", "-1");
                parameters.put("IDTest", "-1");
            }
            else if(idAktivita != null){
                parameters.put("IDSkupinaAktivit", "-1");
                parameters.put("IDAktivita", idAktivita);
                parameters.put("IDTest", "-1");
            }
            else if(idTest != null){
                parameters.put("IDSkupinaAktivit", "-1");
                parameters.put("IDAktivita", "-1");
                parameters.put("IDTest", idTest);
            }

            parameters.put("Nazev", item.get("Nazev"));//
            parameters.put("Ucebna", item.get("Ucebna"));//
            //parameters.put("NazevTyp", item.get("Nazev") + "[ Skupina Aktivit ]");

            parameters.put("AktivniOD", item.get("AktivniOD"));//
            parameters.put("AktivniDO", item.get("AktivniDO"));//
            parameters.put("PrihlaseniDO", item.get("PrihlaseniDO"));//
            parameters.put("OdevzdaniDO", item.get("PrihlaseniDO"));//
            parameters.put("IDAutor", User.id);//
            parameters.put("DatumPrihlaseni", item.get("DatumPrihlaseni"));//
            parameters.put("DatumOdhlaseni", item.get("DatumOdhlaseni"));//
            parameters.put("NutnePrihlaseni", item.get("NutnePrihlaseni"));//
            parameters.put("PocetStudentu", item.get("PocetStudentu"));//
            //parameters.put("Smazany", "false");
            //parameters.put("PocetZapsanych", "0");
            parameters.put("AktivitaNazev", aktivitaNazev);
            parameters.put("PocetPokusu", item.get("PocetPokusu"));//
            //parameters.put("Zapsan", "false");
            Log.d("", parameters + "");
        } catch (JSONException e) {
            Log.d("Fragment_cc_add_term", "insert(): " + e.getMessage());
        }
        Request.post(URL.Date.Insert(), parameters, response);
    }

}