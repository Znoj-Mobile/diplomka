package cz.vsb.elogika.utils;

import cz.vsb.elogika.R;
import cz.vsb.elogika.app.AppController;

/**
 * Třída pro zpracování výčtových typů pro zpracování dat ze servru/posílání dat na server - ke konverzi id na správné názvy a naopak
 */
public class EnumLogika {
    /**
     * metoda podle zadaného id vrací, jestli se jedná o "Nevybírat podle zařazení", "Ukázkový", "Cvičný" nebo "Hlavní"
     * @param idZarazeni id reprezentující Ukázkový, Cvičný, Hlavní nebo libovolný test
     * @return řetězec "Nevybírat podle zařazení", "Ukázkový (á)", "Cvičný(á)" nebo "Hlavní"
     */
    public static String zarazeni(String idZarazeni){
        int id = Integer.valueOf(idZarazeni);
        if(id == 3){
            return AppController.getContext().getResources().getString(R.string.example_adj);
        }
        else if(id == 2){
            return AppController.getContext().getResources().getString(R.string.practise);
        }
        else if(id == 1){
            return AppController.getContext().getResources().getString(R.string.main);
        }
        else if(id == -1){
            AppController.getContext().getResources().getString(R.string.not_by_inclusion);
        }
        //pokud nenajdu, tak vzdy vracim puvodni string
        return idZarazeni;
    }

    /**
     * metoda podle toho, jestli se jedná o "Nevybírat podle zařazení", "Ukázkový (á)", "Cvičný(á)" nebo "Hlavní" test, vrací patřičné id
     * @param zarazeni řetězec "Nevybírat podle zařazení", "Ukázkový (á)", "Cvičný(á)" nebo "Hlavní"
     * @return id reprezentující Ukázkový, Cvičný, Hlavní nebo libovolný test
     */
    public static int idZarazeni(String zarazeni){
        zarazeni = zarazeni.toLowerCase();
        if(zarazeni.equals(AppController.getContext().getResources().getString(R.string.main).toLowerCase())){
            return 1;
        }
        else if(zarazeni.equals(AppController.getContext().getResources().getString(R.string.practise).toLowerCase())){
            return 2;
        }
        else if(zarazeni.equals(AppController.getContext().getResources().getString(R.string.example_adj).toLowerCase())){
            return 3;
        }
        else if(zarazeni.equals(AppController.getContext().getResources().getString(R.string.not_by_inclusion).toLowerCase())){
            return -1;
        }
        //pokud nenajdu, tak vracim 0
        return 0;
    }

    /**
     * Pro zadané id reprezentující formu studia vrací slovní reprezentaci "prezenční" nebo "kombinovaná"
     * @param formaStudia "0" nebo "1"
     * @return "prezenční" nebo "kombinovaná"
     */
    public static String formaStudia(String formaStudia){
        int id = Integer.valueOf(formaStudia);
        if(id == 0){
            return AppController.getContext().getResources().getString(R.string.study_form_present);
        }
        else if(id == 1){
            return AppController.getContext().getResources().getString(R.string.study_form_combinated);
        }
        return formaStudia;
    }

    /**
     * Pro zadanou formu studia vrací id, který formu reprezentuje pro zaslání na server
     * @param formaStudia "prezenční" nebo "kombinovaná"
     * @return id reprezentující formu studia
     */
    public static int idFormaStudia(String formaStudia){
        formaStudia = formaStudia.toLowerCase();
        if(formaStudia.equals(AppController.getContext().getResources().getString(R.string.study_form_present).toLowerCase())){
            return 0;
        }
        //kombinovana - snad jsou jen tyto 2
        return 1;
    }

    /**
     * metoda převede "true" na "ano "a "false" na "ne"
     * @param splnil "true" nebo "false"
     * @return "ano" nebo "ne" ve vybraném jazyce
     */
    public static String splnilAnoNe(String splnil){
        if(splnil.equals("true")){
            return AppController.getContext().getResources().getString(R.string.yes);
        }
        return AppController.getContext().getResources().getString(R.string.no);
    }

    /**
     * metoda převede "true" na "ano "a "false" na "ne"
     * @param povinny "true" nebo "false"
     * @return "ano" nebo "ne" ve vybraném jazyce
     */
    public static String povinnyAnoNe(String povinny){
        if(povinny.equals("true")){
            return AppController.getContext().getResources().getString(R.string.yes);
        }
        return AppController.getContext().getResources().getString(R.string.no);
    }

    /**
     * pro id role vrací jeho slovní reprezentaci
     * @param idRole přirozené číslo <1, 5>
     * @return "student", "garant", "tutor", "tajemnik", "superadmin"
     */
    public static String role(String idRole){
        int id = Integer.valueOf(idRole);
        if(id == 1){
            return AppController.getContext().getResources().getString(R.string.superadmin);
        }else if(id == 2){
            return AppController.getContext().getResources().getString(R.string.secretary);
        }else if(id == 3){
            return AppController.getContext().getResources().getString(R.string.garant);
        }else if(id == 4){
            return AppController.getContext().getResources().getString(R.string.tutor);
        }else if(id == 5){
            return AppController.getContext().getResources().getString(R.string.student);
        }
        return AppController.getContext().getResources().getString(R.string.unknown);
    }

    /**
     * pro roli zadanou slovně vrací její id
     * @param role "student", "garant", "tutor", "tajemnik", "superadmin"
     * @return přirozené číslo <0, 5>
     */
    public static int idRole(String role){
        role = role.toLowerCase();
        if(role.equals(AppController.getContext().getResources().getString(R.string.superadmin).toLowerCase())){
            return 1;
        }
        else if(role.equals(AppController.getContext().getResources().getString(R.string.secretary).toLowerCase())){
            return 2;
        }
        else if(role.equals(AppController.getContext().getResources().getString(R.string.garant).toLowerCase())){
            return 3;
        }
        else if(role.equals(AppController.getContext().getResources().getString(R.string.tutor).toLowerCase())){
            return 4;
        }
        else if(role.equals(AppController.getContext().getResources().getString(R.string.student).toLowerCase())){
            return 5;
        }
        return 0;
    }

    /**
     * metoda převede "true" na "ano "a "false" na "ne"
     * @param akce "true" nebo "false"
     * @return "ano" nebo "ne" ve vybraném jazyce
     */
    public static String akce(String akce) {
        if(akce.equals("true")){
            return AppController.getContext().getResources().getString(R.string.login_title);
        }
        return AppController.getContext().getResources().getString(R.string.logout_title);
    }

    /**
     * metoda vrací id reprezentující, jestli se jedná o omezení "na třídu" nebo "na kurz"
     * @param typOmezeni "na třídu", "na kurz"
     * @return 0 nebo 1
     */
    public static int idTypOmezeni(String typOmezeni) {
        typOmezeni = typOmezeni.toLowerCase();
        if(typOmezeni.equals(AppController.getContext().getResources().getString(R.string.to_class).toLowerCase())){
            return 0;
        }
        //R.string.to_course
        else{
            return 1;
        }
    }

    /**
     * Pro zadané id reprezentující typ třídy vrací slovní reprezentaci "Přednáška" nebo "Cvičení"
     * @param typ "0" nebo "1"
     * @return "Přednáška" nebo "Cvičení"
     */
    public static String typTridy(String typ) {
        int id = Integer.valueOf(typ);
        if(id == 0){
            return AppController.getContext().getResources().getString(R.string.lecture);
        }
        else if(id == 1){
            return AppController.getContext().getResources().getString(R.string.exercise);
        }
        return typ;
    }

    /**
     * Metoda vrací id reprezentující, jestli se jedná o typ "Přednáška" nebo "Cvičení"
     * @param typ "Přednáška" nebo "Cvičení"
     * @return "0" nebo "1"
     */
    public static int idTypTridy(String typ) {
        typ = typ.toLowerCase();
        if(typ.equals(AppController.getContext().getResources().getString(R.string.lecture).toLowerCase())){
            return 0;
        }
        //R.string.exercise
        else{
            return 1;
        }
    }
}
