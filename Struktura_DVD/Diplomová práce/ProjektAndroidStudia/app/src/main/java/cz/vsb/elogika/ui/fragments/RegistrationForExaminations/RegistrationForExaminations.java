package cz.vsb.elogika.ui.fragments.RegistrationForExaminations;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import cz.vsb.elogika.R;
import cz.vsb.elogika.ui.fragments.Fragment_RegistrationForExaminations;
import cz.vsb.elogika.utils.Logging;
import cz.vsb.elogika.utils.SpecialAdapter;

public class RegistrationForExaminations extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    static final String LOG_TAG = "Fragment_RegistrationForExaminations";

    private View rootView;

    public SwipeRefreshLayout swipeLayout;
    LinearLayout nothinglayout;
    Fragment_RegistrationForExaminations registration_data;

    public ListView activities_group_view;
    public ListView tests_view;
    public ListView activities_view;

    private int type;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.list_view_swipe, container, false);

        swipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_update);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeResources(R.color.darker_cyan, R.color.material_blue_grey_800);

        nothinglayout = (LinearLayout) rootView.findViewById(R.id.nothing_to_show);
        nothinglayout.setVisibility(View.GONE);

        registration_data = Fragment_RegistrationForExaminations.getInstance();
        Bundle bundle=getArguments();
        type = bundle.getInt("type");

        initialize();
        return rootView;
    }

    public void initialize() {
        if(type == 0) {
            Logging.logAccess("/Pages/Zakladni/Profil.aspx", "GroupActivity");
            //title.setText(getResources().getString(R.string.registration_for_examination));
            activities_group_view = (ListView) rootView.findViewById(R.id.listView);
            registration_data.adapter_1 = new SpecialAdapter(getActivity(), registration_data.activities_group_list, R.layout.fragment_registration_list, registration_data.from, registration_data.to);

            activities_group_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    registration_data.fill_detail(view, position, registration_data.activities_group_list);
                }
            });
            activities_group_view.setOnScrollListener(new AbsListView.OnScrollListener() {

                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {
                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem,
                                     int visibleItemCount, int totalItemCount) {
                    boolean enable = false;
                    if (activities_group_view != null) {
                        if (activities_group_view.getChildCount() == 0) {
                            enable = true;
                        } else {
                            // check if the first item of the list is visible
                            boolean firstItemVisible = activities_group_view.getFirstVisiblePosition() == 0;
                            // check if the top of the first item is visible
                            boolean topOfFirstItemVisible = activities_group_view.getChildAt(0).getTop() == 0;
                            // enabling or disabling the refresh layout
                            enable = firstItemVisible && topOfFirstItemVisible;
                        }
                    }
                    swipeLayout.setEnabled(enable);
                }
            });
            registration_data.adapter_1.notifyDataSetChanged();
            activities_group_view.setAdapter(registration_data.adapter_1);
            //staci jednou stahnout vsechna data
            update();
        } else if(type == 1){
            Logging.logAccess("/Pages/Zakladni/Profil.aspx", "Test");
            //title.setText(getResources().getString(R.string.registration_for_examination) + "1");
            tests_view = (ListView) rootView.findViewById(R.id.listView);
            registration_data.adapter_2 = new SpecialAdapter(getActivity(), registration_data.tests_list, R.layout.fragment_registration_list, registration_data.from, registration_data.to);
            tests_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    registration_data.fill_detail(view, position, registration_data.tests_list);
                }
            });
            tests_view.setOnScrollListener(new AbsListView.OnScrollListener() {

                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {
                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem,
                                     int visibleItemCount, int totalItemCount) {
                    boolean enable = false;
                    if (tests_view != null) {
                        if (tests_view.getChildCount() == 0) {
                            enable = true;
                        } else {
                            // check if the first item of the list is visible
                            boolean firstItemVisible = tests_view.getFirstVisiblePosition() == 0;
                            // check if the top of the first item is visible
                            boolean topOfFirstItemVisible = tests_view.getChildAt(0).getTop() == 0;
                            // enabling or disabling the refresh layout
                            enable = firstItemVisible && topOfFirstItemVisible;
                        }
                    }
                    swipeLayout.setEnabled(enable);
                }
            });
            registration_data.adapter_2.notifyDataSetChanged();
            tests_view.setAdapter(registration_data.adapter_2);
        } else {
            Logging.logAccess("/Pages/Zakladni/Profil.aspx", "Activity");
            // title.setText(getResources().getString(R.string.registration_for_examination) + "2");
            activities_view = (ListView) rootView.findViewById(R.id.listView);
            registration_data.adapter_3 = new SpecialAdapter(getActivity(), registration_data.activities_list, R.layout.fragment_registration_list, registration_data.from, registration_data.to);
            activities_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    registration_data.fill_detail(view, position, registration_data.activities_list);
                }
            });
            activities_view.setOnScrollListener(new AbsListView.OnScrollListener() {

                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {
                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem,
                                     int visibleItemCount, int totalItemCount) {
                    boolean enable = false;
                    if (activities_view != null) {
                        if (activities_view.getChildCount() == 0) {
                            enable = true;
                        } else {
                            // check if the first item of the list is visible
                            boolean firstItemVisible = activities_view.getFirstVisiblePosition() == 0;
                            // check if the top of the first item is visible
                            boolean topOfFirstItemVisible = activities_view.getChildAt(0).getTop() == 0;
                            // enabling or disabling the refresh layout
                            enable = firstItemVisible && topOfFirstItemVisible;
                        }
                    }
                    swipeLayout.setEnabled(enable);
                }
            });
            registration_data.adapter_3.notifyDataSetChanged();
            activities_view.setAdapter(registration_data.adapter_3);
        }
    }

    public void showNothingLayout(){
        nothinglayout.setVisibility(View.VISIBLE);
    }
    public void hideNothingLayout(){
        nothinglayout.setVisibility(View.GONE);
    }


    @Override
    public void onRefresh() {
        update();
    }

    private void update() {
        registration_data.update(swipeLayout);
    }
}
