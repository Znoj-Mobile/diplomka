package cz.vsb.elogika.ui.fragments.garant.testsAndAcvtivities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.DatePickerDialogOnButton;
import cz.vsb.elogika.utils.EnumLogika;
import cz.vsb.elogika.utils.Logging;
import cz.vsb.elogika.utils.TimePickerDialogOnButton;

public class Fragment_taa_add_test extends Fragment {
    View rootView;

    TextView taa_add_t_title;
    TextView taa_add_t_ip_address_range;

    List<String> toSpin_inclusion;
    Spinner taa_add_t_inclusion;
    ArrayAdapter<String> adapter_inclusion;

    Spinner taa_add_t_template;
    ArrayAdapter<String> adapter_template;

    NumberPicker taa_add_t_time1;
    NumberPicker taa_add_t_time2;
    NumberPicker taa_add_t_time3;

    NumberPicker taa_add_t_min1;
    NumberPicker taa_add_t_min2;
    NumberPicker taa_add_t_min3;

    NumberPicker taa_add_t_max1;
    NumberPicker taa_add_t_max2;
    NumberPicker taa_add_t_max3;

    NumberPicker taa_add_t_efforts1;
    NumberPicker taa_add_t_efforts2;
    NumberPicker taa_add_t_efforts3;

    CheckBox taa_add_t_show_test;
    CheckBox taa_add_t_send_results;
    CheckBox taa_add_t_paper_test;
    CheckBox taa_add_t_offline_test;
    CheckBox taa_add_t_mandatory;

    DatePickerDialogOnButton taa_add_t_date_to_date;
    TimePickerDialogOnButton taa_add_t_time_to_date;

    Map<String, String> list = null;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_taa_add_t, container, false);

        try {
            list = (Map<String, String>) getArguments().getSerializable("list");
        } catch (Exception e){   }

        if(list == null){
            Actionbar.addSubcategory(R.string.add_test, this);
            Logging.logAccess("/Pages/Garant/TestyAktivityDetail/TestForm.aspx");
        }
        else{
            Actionbar.addSubcategory(R.string.edit_test, this);
            Logging.logAccess("/Pages/Garant/TestyAktivityDetail/TestForm.aspx?ID_T=" + list.get("idTest"));
        }
        setComponents();
        return rootView;
    }

    private void setComponents() {

        taa_add_t_title = (TextView) rootView.findViewById(R.id.taa_add_t_title);
        taa_add_t_ip_address_range = (TextView) rootView.findViewById(R.id.taa_add_t_ip_address_range);
        ImageButton aa_add_t_range_image = (ImageButton) rootView.findViewById(R.id.taa_add_t_range_image);
        aa_add_t_range_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(R.string.ip_address_range);
                builder.setCancelable(false);
                builder.setMessage(getResources().getString(R.string.ip_address_range_message) + "\n\n" + getResources().getString(R.string.ip_address_range_example));
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog2, int which) {
                        dialog2.dismiss();
                    }
                }).setIcon(android.R.drawable.ic_dialog_info);
                Dialog d = builder.create();
                d.show();
            }
        });

        //nastaveni spinneru
        toSpin_inclusion = new ArrayList<String>();
        toSpin_inclusion.add(getResources().getString(R.string.main));
        toSpin_inclusion.add(getResources().getString(R.string.practise));
        //toSpin_inclusion.add(getResources().getString(R.string.exemplary));

        taa_add_t_inclusion = (Spinner) rootView.findViewById(R.id.taa_add_t_inclusion);
        adapter_inclusion = new ArrayAdapter<String>(getActivity(),R.layout.spinner_item,toSpin_inclusion);
        taa_add_t_inclusion.setAdapter( adapter_inclusion );


        taa_add_t_template = (Spinner) rootView.findViewById(R.id.taa_add_t_template);
        adapter_template = new ArrayAdapter<String>(getActivity(),R.layout.spinner_item,Fragment_TestsAndActivity.template_list_names);
        taa_add_t_template.setAdapter( adapter_template );

        //nastaveni komponent NumberPicker
        taa_add_t_time1 = (NumberPicker) rootView.findViewById(R.id.taa_add_t_time1);
        taa_add_t_time1.setMaxValue(9);
        taa_add_t_time1.setMinValue(0);
        taa_add_t_time2 = (NumberPicker) rootView.findViewById(R.id.taa_add_t_time2);
        taa_add_t_time2.setMaxValue(9);
        taa_add_t_time2.setMinValue(0);
        taa_add_t_time3 = (NumberPicker) rootView.findViewById(R.id.taa_add_t_time3);
        taa_add_t_time3.setMaxValue(9);
        taa_add_t_time3.setMinValue(0);

        taa_add_t_min1 = (NumberPicker) rootView.findViewById(R.id.taa_add_t_min1);
        taa_add_t_min1.setMaxValue(9);
        taa_add_t_min1.setMinValue(0);
        taa_add_t_min2 = (NumberPicker) rootView.findViewById(R.id.taa_add_t_min2);
        taa_add_t_min2.setMaxValue(9);
        taa_add_t_min2.setMinValue(0);
        taa_add_t_min3 = (NumberPicker) rootView.findViewById(R.id.taa_add_t_min3);
        taa_add_t_min3.setMaxValue(9);
        taa_add_t_min3.setMinValue(0);

        taa_add_t_max1 = (NumberPicker) rootView.findViewById(R.id.taa_add_t_max1);
        taa_add_t_max1.setMaxValue(9);
        taa_add_t_max1.setMinValue(0);
        taa_add_t_max2 = (NumberPicker) rootView.findViewById(R.id.taa_add_t_max2);
        taa_add_t_max2.setMaxValue(9);
        taa_add_t_max2.setMinValue(0);
        taa_add_t_max3 = (NumberPicker) rootView.findViewById(R.id.taa_add_t_max3);
        taa_add_t_max3.setMaxValue(9);
        taa_add_t_max3.setMinValue(0);

        taa_add_t_efforts1 = (NumberPicker) rootView.findViewById(R.id.taa_add_t_efforts1);
        taa_add_t_efforts1.setMaxValue(9);
        taa_add_t_efforts1.setMinValue(0);
        taa_add_t_efforts2 = (NumberPicker) rootView.findViewById(R.id.taa_add_t_efforts2);
        taa_add_t_efforts2.setMaxValue(9);
        taa_add_t_efforts2.setMinValue(0);
        taa_add_t_efforts3 = (NumberPicker) rootView.findViewById(R.id.taa_add_t_efforts3);
        taa_add_t_efforts3.setMaxValue(9);
        taa_add_t_efforts3.setMinValue(0);

        //checkboxy
        taa_add_t_show_test = (CheckBox) rootView.findViewById(R.id.taa_add_t_show_test);
        taa_add_t_send_results = (CheckBox) rootView.findViewById(R.id.taa_add_t_send_results);
        taa_add_t_paper_test = (CheckBox) rootView.findViewById(R.id.taa_add_t_paper_test);
        taa_add_t_offline_test = (CheckBox) rootView.findViewById(R.id.taa_add_t_offline_test);
        taa_add_t_mandatory = (CheckBox) rootView.findViewById(R.id.taa_add_t_mandatory);
/*
        taa_add_t_date_to_date = new DatePickerDialogOnButton((Button) rootView.findViewById(R.id.taa_add_t_date_to_date));
        taa_add_t_time_to_date =  new TimePickerDialogOnButton((Button) rootView.findViewById(R.id.taa_add_t_time_to_date));
*/

        Button taa_add_t_btn_send = (Button) rootView.findViewById(R.id.taa_add_t_btn_send);

        Button taa_add_t_btn_remove = (Button) rootView.findViewById(R.id.taa_add_t_btn_remove);
        taa_add_t_btn_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(R.string.test_cancel);
                builder.setCancelable(false);
                builder.setMessage(R.string.test_cancel_message);
                builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog2, int which) {
                        Actionbar.jumpBack();
                    }
                });

                builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog2, int which) {
                        ;
                    }
                }).setIcon(android.R.drawable.ic_dialog_alert);
                Dialog d = builder.create();
                d.show();
            }
        });

        if (list != null) {
            it_is_update();
            taa_add_t_btn_send.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle(R.string.test_update);
                    builder.setCancelable(false);
                    builder.setMessage(R.string.test_update_message);
                    builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog2, int which) {
                            update(list.get("idTest"));
                        }
                    });
                    builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog2, int which) {
                            ;
                        }
                    }).setIcon(android.R.drawable.ic_dialog_alert);
                    Dialog d = builder.create();
                    d.show();
                }
            });
        }
        else{
            taa_add_t_btn_send.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle(R.string.test_insert);
                    builder.setCancelable(false);
                    builder.setMessage(R.string.test_insert_message);
                    builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog2, int which) {
                            insert();
                        }
                    });
                    builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog2, int which) {
                            ;
                        }
                    }).setIcon(android.R.drawable.ic_dialog_alert);
                    Dialog d = builder.create();
                    d.show();
                }
            });
        }
    }

    private void it_is_update() {
        taa_add_t_title.setText(list.get("nazev"));
        taa_add_t_ip_address_range.setText(list.get("rozsahIP"));

        //todo nastaveni spinneru
        int position = toSpin_inclusion.indexOf(EnumLogika.zarazeni(list.get("idZarazeni")));
        taa_add_t_inclusion.setSelection(position);
        position = Fragment_TestsAndActivity.template_list_ids.indexOf(list.get("idSablona"));
        taa_add_t_template.setSelection(position);

        //nastaveni komponent NumberPicker
        int value = Integer.valueOf(list.get("cas"));
        taa_add_t_time1.setValue(value / 100);
        value %= 100;
        taa_add_t_time2.setValue(value / 10);
        value %= 10;
        taa_add_t_time3.setValue(value);

        value = Integer.valueOf(list.get("maxBody"));
        taa_add_t_min1.setValue(value / 100);
        value %= 100;
        taa_add_t_min2.setValue(value / 10);
        value %= 10;
        taa_add_t_min3.setValue(value);

        value = Integer.valueOf(list.get("minBody"));
        taa_add_t_max1.setValue(value / 100);
        value %= 100;
        taa_add_t_max2.setValue(value / 10);
        value %= 10;
        taa_add_t_max3.setValue(value);

        value = Integer.valueOf(list.get("pocetPokusu"));
        taa_add_t_efforts1.setValue(value / 100);
        value %= 100;
        taa_add_t_efforts2.setValue(value / 10);
        value %= 10;
        taa_add_t_efforts3.setValue(value);

        //checkboxy
        if(list.get("zobrazHotovTest").equals("true")){
            taa_add_t_show_test.setChecked(true);
        }
        else{
            taa_add_t_show_test.setChecked(false);
        }

        if(list.get("odeslaniVysl").equals("true")){
            taa_add_t_send_results.setChecked(true);
        }
        else{
            taa_add_t_send_results.setChecked(false);
        }

        if(list.get("papirovyTest").equals("true")){
            taa_add_t_paper_test.setChecked(true);
        }
        else{
            taa_add_t_paper_test.setChecked(false);
        }

        if(list.get("offline").equals("true")){
            taa_add_t_offline_test.setChecked(true);
        }
        else{
            taa_add_t_offline_test.setChecked(false);
        }

        if(list.get("povinny").equals("true")){
            taa_add_t_mandatory.setChecked(true);
        }
        else{
            taa_add_t_mandatory.setChecked(false);
        }
        /*
        if(list.get("odevzdaniDo") != "") {
            taa_add_t_date_to_date.setDateTime(taa_add_t_time_to_date, list.get("odevzdaniDo"));
        }
        */
    }

    private void insert() {
        final String name = taa_add_t_title.getText().toString();
        Logging.logAccess("/Pages/Garant/TestyAktivityDetail/TestSummary.aspx");
        Response response = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException {
                Log.d("typ response: ", response.getString("Success"));
                if (response.getString("Success").equals("true")) {
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .type(SnackbarType.MULTI_LINE)
                                    .text(getActivity().getString(R.string.test) + " " + name + " " + getActivity().getString(R.string.test_was_inserted)));
                    Actionbar.jumpBack();
                } else { //nepovedlo se
                    Log.d("Fragment_cc_add_ag", "insert(): " + response.toString());
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .type(SnackbarType.MULTI_LINE)
                                    .text(getActivity().getString(R.string.test) + " " + name + " " + getActivity().getString(R.string.test_was_not_inserted)));
                }
            }
        };
        JSONObject parameters = new JSONObject();
        try {
            parameters.put("IDTest", "-1");
            //parameters.put("OdevzdaniDO", taa_add_t_date_to_date.getDate(taa_add_t_time_to_date));
            int pocet = taa_add_t_efforts1.getValue() * 100 + taa_add_t_efforts2.getValue() * 10 + taa_add_t_efforts3.getValue();
            parameters.put("PocetPokusu", pocet);

            parameters.put("IDSablona", Fragment_TestsAndActivity.template_list_ids.get(taa_add_t_template.getSelectedItemPosition()));
            parameters.put("IDZarazeni", EnumLogika.idZarazeni(toSpin_inclusion.get(taa_add_t_inclusion.getSelectedItemPosition())));


            parameters.put("Nazev", taa_add_t_title.getText());
            pocet = taa_add_t_time1.getValue() * 100 + taa_add_t_time2.getValue() * 10 + taa_add_t_time3.getValue();
            parameters.put("Cas", pocet);
            pocet = taa_add_t_max1.getValue() * 100 + taa_add_t_max2.getValue() * 10 + taa_add_t_max3.getValue();
            parameters.put("MaxBody", pocet);
            pocet = taa_add_t_min1.getValue() * 100 + taa_add_t_min2.getValue() * 10 + taa_add_t_min3.getValue();
            parameters.put("MinBody", pocet);
            parameters.put("RozsahIP", taa_add_t_ip_address_range.getText());
            if(taa_add_t_show_test.isChecked()) {
                parameters.put("ZobrazHotovTest", true);
            }
            else{
                parameters.put("ZobrazHotovTest", false);
            }
            if(taa_add_t_send_results.isChecked()) {
                parameters.put("OdeslaniVysl", true);
            }
            else{
                parameters.put("OdeslaniVysl", false);
            }
            if(taa_add_t_paper_test.isChecked()) {
                parameters.put("PapirovyTest", true);
            }
            else{
                parameters.put("PapirovyTest", false);
            }
            if(taa_add_t_offline_test.isChecked()) {
                parameters.put("Offline", true);
            }
            else{
                parameters.put("Offline", false);
            }
            if(taa_add_t_mandatory.isChecked()) {
                parameters.put("Povinny", true);
            }
            else{
                parameters.put("Povinny", false);
            }



            //parameters.put("Smazany", false);
            //parameters.put("IDTermin", "");
            //parameters.put("NazevTermin", "");
            //parameters.put("AktivniOD", TimeLogika.getDateFromPickers(
            //        taa_add_t_date_to_date,
            //        taa_add_t_date_to_time));

            //parameters.put("AktivniDO", TimeLogika.getDateFromPickers(
            //        taa_add_t_date_to_date,
            //        taa_add_t_date_to_time));

            parameters.put("NazevSA", Fragment_TestsAndActivity.nazevSA);
            parameters.put("IsEditable", true);
            Log.d("", parameters + "");
        } catch (JSONException e) {
            Log.d("Fragment_taa_add_test", "insert() param: " + e.getMessage());
        }
        Request.post(URL.Test.Insert(Fragment_TestsAndActivity.groupActivityId, User.id), parameters, response);
    }

    private void update(String idTest){
        Logging.logAccess("/Pages/Garant/TestyAktivityDetail/TestSummary.aspx");
        final String name = taa_add_t_title.getText().toString();
        Response response = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException {
                Log.d("typ response: ", response.getString("Success"));
                if (response.getString("Success").equals("true")) {
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .type(SnackbarType.MULTI_LINE)
                                    .text(getActivity().getString(R.string.test) + " " + name + " " + getActivity().getString(R.string.test_was_updated)));
                    Actionbar.jumpBack();
                } else { //nepovedlo se
                    Log.d("Fragment_cc_add_ag", "insert(): " + response.toString());
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .type(SnackbarType.MULTI_LINE)
                                    .text(getActivity().getString(R.string.test) + " " + name + " " + getActivity().getString(R.string.test_was_not_updated)));
                }
            }
        };
        JSONObject parameters = new JSONObject();
        try {
            parameters.put("IDTest", idTest);
            //parameters.put("OdevzdaniDO", taa_add_t_date_to_date.getDate(taa_add_t_time_to_date));
            int pocet = taa_add_t_efforts1.getValue() * 100 + taa_add_t_efforts2.getValue() * 10 + taa_add_t_efforts3.getValue();
            parameters.put("PocetPokusu", pocet);

            parameters.put("IDSablona", Fragment_TestsAndActivity.template_list_ids.get(taa_add_t_template.getSelectedItemPosition()));
            parameters.put("IDZarazeni", EnumLogika.idZarazeni(toSpin_inclusion.get(taa_add_t_inclusion.getSelectedItemPosition())));


            parameters.put("Nazev", taa_add_t_title.getText());
            pocet = taa_add_t_time1.getValue() * 100 + taa_add_t_time2.getValue() * 10 + taa_add_t_time3.getValue();
            parameters.put("Cas", pocet);
            pocet = taa_add_t_max1.getValue() * 100 + taa_add_t_max2.getValue() * 10 + taa_add_t_max3.getValue();
            parameters.put("MaxBody", pocet);
            pocet = taa_add_t_min1.getValue() * 100 + taa_add_t_min2.getValue() * 10 + taa_add_t_min3.getValue();
            parameters.put("MinBody", pocet);
            parameters.put("RozsahIP", taa_add_t_ip_address_range.getText());
            if(taa_add_t_show_test.isChecked()) {
                parameters.put("ZobrazHotovTest", true);
            }
            else{
                parameters.put("ZobrazHotovTest", false);
            }
            if(taa_add_t_send_results.isChecked()) {
                parameters.put("OdeslaniVysl", true);
            }
            else{
                parameters.put("OdeslaniVysl", false);
            }
            if(taa_add_t_paper_test.isChecked()) {
                parameters.put("PapirovyTest", true);
            }
            else{
                parameters.put("PapirovyTest", false);
            }
            if(taa_add_t_offline_test.isChecked()) {
                parameters.put("Offline", true);
            }
            else{
                parameters.put("Offline", false);
            }
            if(taa_add_t_mandatory.isChecked()) {
                parameters.put("Povinny", true);
            }
            else{
                parameters.put("Povinny", false);
            }



            //parameters.put("Smazany", false);
            //parameters.put("IDTermin", "");
            //parameters.put("NazevTermin", "");
            //parameters.put("AktivniOD", TimeLogika.getDateFromPickers(
            //        taa_add_t_date_to_date,
            //        taa_add_t_date_to_time));

            //parameters.put("AktivniDO", TimeLogika.getDateFromPickers(
            //        taa_add_t_date_to_date,
            //        taa_add_t_date_to_time));

            parameters.put("NazevSA", Fragment_TestsAndActivity.nazevSA);
            parameters.put("IsEditable", true);
            Log.d("", parameters + "");
        } catch (JSONException e) {
            Log.d("Fragment_taa_add_test", "update() param: " + e.getMessage());
        }
        Request.post(URL.Test.Update(), parameters, response);
    }
}
