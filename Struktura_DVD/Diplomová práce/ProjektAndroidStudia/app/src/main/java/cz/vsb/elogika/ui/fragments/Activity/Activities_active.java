package cz.vsb.elogika.ui.fragments.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.ui.fragments.Fragment_Activities;
import cz.vsb.elogika.utils.DownloadFile;
import cz.vsb.elogika.utils.EnumLogika;
import cz.vsb.elogika.utils.Logging;
import cz.vsb.elogika.utils.SelectFileToUpload;
import cz.vsb.elogika.utils.SpecialAdapter;
import cz.vsb.elogika.utils.TimeLogika;

public class Activities_active extends Activities {
    static ArrayList<Map<String, String>> list;

    @Override
    protected void initialize() {
        Logging.logAccess("/Pages/Student/Aktivity.aspx", "Actual");
        list = new ArrayList<>();
        list_detail();
        String[] from = {"a_name", "a_group", "minmax", "mandatory", "a_term_name", "a_active_since"};
        int[] to = {R.id.a_name, R.id.a_group, R.id.a_minmax, R.id.a_mandatory, R.id.a_term_name, R.id.a_active_since};
        adapter = new SpecialAdapter(getActivity(), list, R.layout.fragment_activities_list, from, to);
    }

    @Override
    protected void list_detail() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                Logging.logAccess("/Pages/Student/AktivitaVyreseneDetail/AktivitaVyreseneDetail.aspx");
                //Toast.makeText(getActivity(), list.get(position).toString(), Toast.LENGTH_SHORT).show();
                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.fragment_activities_detail);

                TextView a_group_item = (TextView) dialog.findViewById(R.id.a_group_item);
                a_group_item.setText(list.get(position).get("a_group"));
                TextView a_name_item = (TextView) dialog.findViewById(R.id.a_name_item);
                a_name_item.setText(list.get(position).get("a_name"));
                TextView a_max_item = (TextView) dialog.findViewById(R.id.a_max_item);
                a_max_item.setText(list.get(position).get("minimum"));
                TextView a_min_item = (TextView) dialog.findViewById(R.id.a_min_item);
                a_min_item.setText(list.get(position).get("a_max"));
                TextView pocetPokusu = (TextView) dialog.findViewById(R.id.a_number_of_efforts);
                pocetPokusu.setText(list.get(position).get("pocetPokusu"));
                TextView a_mandatory_item = (TextView) dialog.findViewById(R.id.a_mandatory_item);
                a_mandatory_item.setText(list.get(position).get("mandatory"));
                TextView a_term_name_item = (TextView) dialog.findViewById(R.id.a_term_name_item);
                a_term_name_item.setText(list.get(position).get("a_term_name"));
                TextView a_active_since_item = (TextView) dialog.findViewById(R.id.a_active_since_item);
                a_active_since_item.setText(list.get(position).get("a_active_since"));
                TextView a_active_to_item = (TextView) dialog.findViewById(R.id.a_active_to_item);
                a_active_to_item.setText(list.get(position).get("a_active_to"));
                TextView a_description_item = (TextView) dialog.findViewById(R.id.a_description_item);
                a_description_item.setText(list.get(position).get("a_description"));
                Button btn_download_description = (Button) dialog.findViewById(R.id.btn_download_description);
                if (list.get(position).get("a_description_file_name").equals("")) {
                    btn_download_description.setVisibility(View.GONE);
                    btn_download_description.setEnabled(false);
                } else {
                    btn_download_description.setVisibility(View.VISIBLE);
                    btn_download_description.setEnabled(true);
                    btn_download_description.setText(getString(R.string.download) + ": " + list.get(position).get("a_description_file_name"));
                    btn_download_description.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            SnackbarManager.show(
                                    Snackbar.with(getActivity())
                                            .type(SnackbarType.MULTI_LINE)
                                            .duration(Snackbar.SnackbarDuration.LENGTH_INDEFINITE)
                                            .text(getResources().getString(R.string.file_is_downloaded)));
                            downloadInfoFile(list.get(position).get("a_description_file_name"), list.get(position).get("a_id_aktivita"));
                            dialog.dismiss();
                        }
                    });
                }

                TextView a_pattern_item = (TextView) dialog.findViewById(R.id.a_pattern_item);
                a_pattern_item.setText(list.get(position).get("a_pattern"));
                Button btn_download_pattern = (Button) dialog.findViewById(R.id.btn_download_pattern);
                if (list.get(position).get("a_pattern_file_name").equals("")) {
                    btn_download_pattern.setVisibility(View.GONE);
                    btn_download_pattern.setEnabled(false);
                } else {
                    btn_download_pattern.setVisibility(View.VISIBLE);
                    btn_download_pattern.setEnabled(true);
                    btn_download_pattern.setText(getString(R.string.download) + ": " + list.get(position).get("a_pattern_file_name"));
                    btn_download_pattern.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            SnackbarManager.show(
                                    Snackbar.with(getActivity())
                                            .type(SnackbarType.MULTI_LINE)
                                            .duration(Snackbar.SnackbarDuration.LENGTH_INDEFINITE)
                                            .text(getResources().getString(R.string.file_is_downloaded)));
                            downloadSampleFile(list.get(position).get("a_pattern_file_name"), list.get(position).get("a_id_aktivita"));
                            dialog.dismiss();
                        }
                    });
                }
                TextView a_file_name = (TextView) dialog.findViewById(R.id.a_file_name);
                TextView a_file_name_desc = (TextView) dialog.findViewById(R.id.a_file_name_desc);
                Button btn_upload_activity = (Button) dialog.findViewById(R.id.btn_upload_activity);
                Button btn_download_activity = (Button) dialog.findViewById(R.id.btn_download_activity);
                String file_name = exists_file(list.get(position).get("a_id_aktivita"));
                //tlacitko bude aktivni od casu "Aktivni OD" do "Aktivni DO" a to pouze, pokud aktivita jeste nebyla vlozena
                if (TimeLogika.isActive(list.get(position).get("a_active_since"), list.get(position).get("a_active_to"))) {
                    exists(list.get(position).get("a_id_aktivita"), list.get(position).get("a_id_termin"), btn_upload_activity, btn_download_activity, a_file_name);
                }
                if (file_name == null || file_name.equals("")) {
                    btn_download_activity.setVisibility(View.GONE);
                    btn_download_activity.setEnabled(false);
                    a_file_name_desc.setVisibility(View.GONE);
                    a_file_name.setVisibility(View.GONE);
                } else {
                    btn_download_activity.setEnabled(true);
                    a_file_name.setText(file_name);
                    a_file_name_desc.setVisibility(View.VISIBLE);
                    a_file_name.setVisibility(View.VISIBLE);
                }
                btn_download_activity.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SnackbarManager.show(
                                Snackbar.with(getActivity())
                                        .type(SnackbarType.MULTI_LINE)
                                        .duration(Snackbar.SnackbarDuration.LENGTH_INDEFINITE)
                                        .text(getResources().getString(R.string.file_is_downloaded)));
                        download(list.get(position).get("a_id_aktivita"));
                        dialog.dismiss();
                    }
                });
                btn_upload_activity.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Dialog dialog_solution = new Dialog(getActivity());
                        dialog_solution.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog_solution.setContentView(R.layout.fragment_activities_solution);
                        final EditText sol_solution = (EditText) dialog_solution.findViewById(R.id.sol_solution);
                        final EditText sol_note = (EditText) dialog_solution.findViewById(R.id.sol_note);
                        //komponenta pro zobrazeni nazvu souboru po jeho vybrani...
                        sol_solution_chosen_file = (TextView) dialog_solution.findViewById(R.id.sol_solution_chosen_file);
                        sol_file_name = (TextView) dialog_solution.findViewById(R.id.sol_file_name);
                        Button btn_choose_file_activity = (Button) dialog_solution.findViewById(R.id.btn_choose_file_activity);
                        final Button btn_upload_activity = (Button) dialog_solution.findViewById(R.id.btn_upload_activity);
                        btn_choose_file_activity.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                new SelectFileToUpload(getActivity()) {
                                    @Override
                                    public void selectedFile(String FileName_2, String FileType_2, String FileinBase64_2) {

                                        FileName = FileName_2;
                                        FileType = FileType_2;
                                        byteArray = FileinBase64_2;
                                        btn_upload_activity.setEnabled(true);

                                        //pridani nazvu vybraneho souboru do dialogu...
                                        if (sol_file_name != null) {
                                            sol_file_name.setText(FileName);
                                            sol_file_name.setVisibility(View.VISIBLE);
                                            sol_solution_chosen_file.setVisibility(View.VISIBLE);
                                        }
                                    }
                                };
                                //choose_file(btn_upload_activity);
                            }
                        });
                        btn_upload_activity.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                SnackbarManager.show(
                                        Snackbar.with(getActivity())
                                                .type(SnackbarType.MULTI_LINE)
                                                .duration(Snackbar.SnackbarDuration.LENGTH_INDEFINITE)
                                                .text(getResources().getString(R.string.file_is_uploaded)));
                                upload(list.get(position), sol_solution.getText().toString(), sol_note.getText().toString());
                                dialog_solution.dismiss();
                                dialog.dismiss();
                            }
                        });
                        dialog_solution.show();
                    }
                });
                dialog.show();
            }
        });
    }

    @Override
    protected void data_into_list()
    {
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                if (response.length() > 0)
                {
                    for (int j = 0; j < response.length(); j++)
                    {
                        //Log.d("-", "" + response.getJSONObject(j).getString("NazevSkupinaAktivit"));
                        if(!response.getJSONObject(j).getString("Smazana").equals("true")) {
                            list.add(put_data(
                                    response.getJSONObject(j).getString("NazevSA"),
                                    response.getJSONObject(j).getString("Nazev"),
                                    response.getJSONObject(j).getString("Maximum"),
                                    response.getJSONObject(j).getString("Povinny"),
                                    response.getJSONObject(j).getString("NazevTermin"),
                                    TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("AktivniOD")),
                                    TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("AktivniDO")),
                                    response.getJSONObject(j).getString("Popis"),
                                    response.getJSONObject(j).getString("VzorVyslSoubor"),
                                    response.getJSONObject(j).getString("PopisSoubor"),
                                    response.getJSONObject(j).getString("VzorVyslSouborNazev"),
                                    response.getJSONObject(j).getString("PopisSouborNazev"),
                                    response.getJSONObject(j).getString("VzorVyslSouborContType"),
                                    response.getJSONObject(j).getString("PopisSouborContType"),
                                    TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("VyberDoData")),
                                    response.getJSONObject(j).getString("Vybiratelnost"),
                                    response.getJSONObject(j).getString("IsEditable"),
                                    response.getJSONObject(j).getString("IDAktivita"),
                                    response.getJSONObject(j).getString("IDTermin"),
                                    response.getJSONObject(j).getString("PocetPokusu"),
                                    response.getJSONObject(j).getString("VzorVysl"),
                                    response.getJSONObject(j).getString("DoporHodnoceni"),
                                    response.getJSONObject(j).getString("Minimum"),
                                    response.getJSONObject(j).getString("OmezeniStudentu"),
                                    response.getJSONObject(j).getString("TypOmezeni")));
                        }
                    }
                }
                else{
                    Log.d("Chyba", "Fragment_Activities Data_into_list");
                    nothinglayout.setVisibility(View.VISIBLE);
                }
                swipeLayout.setRefreshing(false);
                adapter.notifyDataSetChanged();
            }
        };
        Request.get(URL.CourseConditions.CurrentAktivita(User.courseInfoId, User.id), arrayResponse);
    }

    @Override
    protected void clear_list() {
        list.clear();
    }

    private Map<String, String> put_data(String nazevSA, String nazev, String maximum, String povinny, String nazevTermin, String aktivniOD,
                                           String aktivniDO, String popis, String vzorVyslSoubor, String popisSoubor, String vzorVyslSouborNazev,
                                           String popisSouborNazev, String vzorVyslSouborContType, String popisSouborContType, String vyberDoData,
                                           String vybiratelnost, String isEditable, String IDAktivita, String IDTermin, String pocetPokusu,
                                           String vzorVysl, String doporHodnoceni, String minimum, String omezeniStudentu, String typOmezeni) {
        HashMap<String, String> item = new HashMap<>();
        item.put("a_group", nazevSA);
        item.put("a_name", nazev);
        item.put("a_max",maximum);
        item.put("mandatory", EnumLogika.povinnyAnoNe(povinny));
        item.put("a_term_name",nazevTermin);
        item.put("a_active_since",aktivniOD);
        item.put("a_active_to",aktivniDO);
        item.put("a_description",popis);
        item.put("a_pattern_file",vzorVyslSoubor);
        item.put("a_description_file",popisSoubor);
        item.put("a_pattern_file_name",vzorVyslSouborNazev);
        item.put("a_description_file_name",popisSouborNazev);
        item.put("a_pattern_file_name_type",vzorVyslSouborContType);
        item.put("a_description_file_name_type",popisSouborContType);
        item.put("a_choose_until",vyberDoData);
        item.put("vybiratelnost",vybiratelnost);
        item.put("a_is_editable",isEditable);
        item.put("a_id_aktivita",IDAktivita);
        item.put("a_id_termin",IDTermin);
        item.put("pocetPokusu",pocetPokusu);
        item.put("vzorVysl",vzorVysl);
        item.put("doporHodnoceni",doporHodnoceni);
        item.put("minimum",minimum);
        item.put("omezeniStudentu",omezeniStudentu);
        item.put("typOmezeni",typOmezeni);
        item.put("minmax", minimum + " / " + maximum);
        return item;
    }

    private void exists(String activityId, String dateId, final Button button, final Button but_download, final TextView a_file_name) {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                if (response.length() > 0)
                {
                    Log.d("exists response ",response.getString("Success"));
                    if(response.getString("Success") == "true"){
                        button.setEnabled(false);
                        but_download.setEnabled(true);
                        a_file_name.setVisibility(View.VISIBLE);
                        //zmena textu v buttonu...
                        button.setText(getResources().getString(R.string.activity_is_in_system));
                    }
                    else {
                        button.setEnabled(true);
                        but_download.setEnabled(false);
                        a_file_name.setVisibility(View.GONE);
                    }
                }
                else{
                    Log.d("Chyba", "Fragment_Activities exists()");
                }
            }
        };
        Request.get(URL.SolvedActivities.Exists(Integer.valueOf(activityId), Integer.valueOf(dateId), User.id), response);
    }

    private void upload(Map<String, String> item, String solution, String note) {
        Logging.logAccess("/Pages/Student/AktivitaVyreseneDetail/AktivitaVyreseneSummary.aspx");
        Response response = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException {
                if (response.getBoolean("Result")) {
                    Log.d("typ response: ", response.getString("Result"));
                    if (response.getString("Result").equals("true")) {
                        SnackbarManager.show(
                                Snackbar.with(getActivity())
                                        .type(SnackbarType.MULTI_LINE)
                                        .text(getActivity().getString(R.string.file) + " " + FileName + " " + getActivity().getString(R.string.file_was_uploaded)));
                    }
                    else{
                        SnackbarManager.show(
                                Snackbar.with(getActivity())
                                        .type(SnackbarType.MULTI_LINE)
                                        .text(getActivity().getString(R.string.file_was_not_uploaded) + " " + FileName));
                    }
                }
                else{
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .type(SnackbarType.MULTI_LINE)
                                    .text(getActivity().getString(R.string.file_was_not_uploaded) + " " + FileName));
                }
                //aktualizace dat
                update();
            }
        };
        JSONObject parameters = new JSONObject();
        try
        {
            parameters.put("IDAktivitaVyresene", "-1");
            parameters.put("IDVlozil", User.id);
            parameters.put("IDUzivatel", User.id);
            parameters.put("IDTermin", item.get("a_id_termin"));
            parameters.put("IDAktivita", item.get("a_id_aktivita"));
            parameters.put("Hodnoceni", "");
            parameters.put("HodnoceniDatum", "");
            parameters.put("DatumVlozeni", TimeLogika.currentServerTime());
            parameters.put("LoginSkola", User.school);
            parameters.put("LoginSystem", "");
            parameters.put("Jmeno", User.name);
            parameters.put("Prijmeni", User.surname);
            parameters.put("TitulPred", User.degreeBeforeName);
            parameters.put("TitulZa", User.degreeAfterName);
            parameters.put("NazevAktivita", item.get("a_name"));
            parameters.put("DoporHodnoceni", item.get("a_max"));
            parameters.put("NazevTermin", item.get("a_term_name"));
            parameters.put("NazevSkupinaAktivit", item.get("a_group"));


            parameters.put("Reseni", solution);
            parameters.put("Poznamka", note);

            parameters.put("ReseniSouborNazev", FileName);
            parameters.put("ReseniSouborContent", FileType);

            //Log.d("file", Base64.encodeToString(byteArray, Base64.DEFAULT));
            parameters.put("ReseniSoubor", byteArray);
            //parameters.put("ReseniSoubor", item.get("ReseniSoubor"));
        }
        catch (JSONException e) {}

        swipeLayout.post(new Runnable() {
            @Override
            public void run() {
                nothinglayout.setVisibility(View.GONE);
                swipeLayout.setRefreshing(true);
            }
        });
        Request.post(URL.SolvedActivities.Post(), parameters, response);
    }

    private String exists_file(String activityId){
        if(Fragment_Activities.fragment_activities_finished == null){
            return null;
        }
        if(Activities_finished.list == null){
            return null;
        }
        if(Activities_finished.list.size() == 0){
            return null;
        }
        for(int i = 0; i < Activities_finished.list.size(); i++){
            if(Activities_finished.list.get(i).get("af_idAktivita").equals(activityId)){
                if(Activities_finished.list.get(i).get("af_description") == ""){
                    //nebyl odevzdán žádný soubor
                    return null;
                }
                return Activities_finished.list.get(i).get("af_description");
            }
        }
        //jina chyba
        return null;
    }

    private void download(String activityId) {
        if(Activities_finished.list != null){
            for(int i = 0; i < Activities_finished.list.size(); i++){
                if(Activities_finished.list.get(i).get("af_idAktivita").equals(activityId)){
                    download(Activities_finished.list.get(i).get("af_description"), Activities_finished.list.get(i).get("af_idAktivitaVyresene"));
                    return;
                }
            }
        }
    }

    private void downloadSampleFile(final String fileName, String activityId) {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                if (response.length() > 0)
                {
                    Log.d("download odpoved", response.toString());
                }
                else{
                    Log.d("Chyba", "Fragment_Activities downloadSampleFile");
                }
                Intent intent = DownloadFile.download(fileName, response.getString("VzorVyslSoubor"));
                getActivity().startActivity(intent);
                SnackbarManager.show(
                        Snackbar.with(getActivity())
                                .type(SnackbarType.MULTI_LINE)
                                .duration(Snackbar.SnackbarDuration.LENGTH_INDEFINITE)
                                .text(getResources().getString(R.string.file) + " '" + fileName + "' " + getResources().getString(R.string.is_downloaded_in_download_folder))
                                .attachToAbsListView(listView), getActivity());
            }
        };

        Request.get(URL.Activity.GetActivitySampleFile(Integer.valueOf(activityId)), response);
    }

    private void downloadInfoFile(final String fileName, String activityId) {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                if (response.length() > 0)
                {
                    Log.d("download odpoved", response.toString());
                }
                else{
                    Log.d("Chyba", "Fragment_Activities downloadInfoFile");
                }
                Intent intent = DownloadFile.download(fileName, response.getString("PopisSoubor"));
                getActivity().startActivity(intent);
                SnackbarManager.show(
                        Snackbar.with(getActivity())
                                .type(SnackbarType.MULTI_LINE)
                                .duration(Snackbar.SnackbarDuration.LENGTH_INDEFINITE)
                                .text(getResources().getString(R.string.file) + " '" + fileName + "' " + getResources().getString(R.string.is_downloaded_in_download_folder))
                                .attachToAbsListView(listView), getActivity());
            }
        };

        Request.get(URL.Activity.GetActivityInfoFile(Integer.valueOf(activityId)), response);
    }
}
