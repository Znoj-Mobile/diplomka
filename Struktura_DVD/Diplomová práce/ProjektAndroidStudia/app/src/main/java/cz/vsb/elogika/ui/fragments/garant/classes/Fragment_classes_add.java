package cz.vsb.elogika.ui.fragments.garant.classes;

import android.app.Dialog;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.DatePickerDialogOnButton;
import cz.vsb.elogika.utils.DividerItemDecoration;
import cz.vsb.elogika.utils.EnumLogika;
import cz.vsb.elogika.utils.Logging;
import cz.vsb.elogika.utils.TimeLogika;
import cz.vsb.elogika.utils.TimePickerDialogOnButton;

public class Fragment_classes_add extends Fragment {
    View rootView;
    LinearLayout rootLayout;

    Map<String, String> list = null;
    ArrayList<Map<String, String>> combinated_list = null;

    private TextView tutor;
    private TextView lessonName;
    private EditText lessonShortcut;
    private EditText room;
    private NumberPicker limit_number_of_students1;
    private NumberPicker limit_number_of_students2;
    private NumberPicker limit_number_of_students3;
    private Spinner day_spinner;
    private CheckBox oddWeek;
    private CheckBox evenWeek;
    private RadioButton combinate;
    private RadioButton present;
    private RadioButton lecture;
    private RadioButton exercise;
    private Button roundButton;
    private LinearLayout ll_combinate;
    TimePickerDialogOnButton c_l_add_lesson_start;
    TimePickerDialogOnButton c_l_add_lesson_end;
    DatePickerDialogOnButton c_l_add_specific_date;
    Button c_l_add_specific_date_add;
    Button c_l_add_show_classes;


    RecyclerView recyclerView_dialog;
    ProgressBar loading_progress_dialog;
    LinearLayout nothinglayout_dialog;
    ArrayList<Map<String, String>> tutors_list_dialog;
    ArrayList<Map<String, String>> checkbox_tutor_list;
    RecyclerView.Adapter adapter_dialog;

    RecyclerView recyclerView_classes;
    ProgressBar loading_progress_classes;
    LinearLayout nothinglayout_classes;
    ArrayList<Map<String, String>> lesson_list;
    RecyclerView.Adapter adapter_classes;
    private int counter = 0;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_classes_add, container, false);
        rootLayout = (LinearLayout) rootView.findViewById(R.id.c_l_add_rootLayout);

        try {
            list = (Map<String, String>) getArguments().getSerializable("list");
            combinated_list = (ArrayList<Map<String, String>>) getArguments().getSerializable("combinated_list");

        } catch (Exception e){   }

        if(list == null){
            Actionbar.addSubcategory(R.string.add_class, this);
            Logging.logAccess("/Pages/Tutor/TridaDetail/TridaForm.aspx");
        }
        else{
            Actionbar.addSubcategory(R.string.edit_class, this);
            Logging.logAccess("/Pages/Tutor/TridaDetail/TridaForm.aspx?id=" + list.get("idTrida"));
        }

        setComponents();
        return rootView;
    }

    private void setComponents() {
        lesson_list = new ArrayList<>();
        ll_combinate = (LinearLayout) rootLayout.findViewById(R.id.c_l_add_ll_combinate);
        tutor = (TextView) rootLayout.findViewById(R.id.c_l_add_chosen_tutor);
        lessonName = (TextView) rootLayout.findViewById(R.id.c_l_add_lesson_name);
        lessonShortcut = (EditText) rootLayout.findViewById(R.id.c_l_add_lesson_shortcut);
        room = (EditText) rootLayout.findViewById(R.id.c_l_add_room);
        oddWeek = (CheckBox) rootLayout.findViewById(R.id.c_l_add_odd_week);
        evenWeek = (CheckBox) rootLayout.findViewById(R.id.c_l_add_even_week);
        combinate = (RadioButton) rootLayout.findViewById(R.id.c_l_add_combinate);
        present = (RadioButton) rootLayout.findViewById(R.id.c_l_add_present);
        lecture = (RadioButton) rootLayout.findViewById(R.id.c_l_add_lecture);
        exercise = (RadioButton) rootLayout.findViewById(R.id.c_l_add_exercise);

        limit_number_of_students1 = (NumberPicker) rootLayout.findViewById(R.id.c_l_add_limit_number_of_students1);
        limit_number_of_students1.setMaxValue(9);
        limit_number_of_students1.setMinValue(0);
        limit_number_of_students2 = (NumberPicker) rootLayout.findViewById(R.id.c_l_add_limit_number_of_students2);
        limit_number_of_students2.setMaxValue(9);
        limit_number_of_students2.setMinValue(0);
        limit_number_of_students3 = (NumberPicker) rootLayout.findViewById(R.id.c_l_add_limit_number_of_students3);
        limit_number_of_students3.setMaxValue(9);
        limit_number_of_students3.setMinValue(0);

        day_spinner = (Spinner) rootLayout.findViewById(R.id.c_l_add_day);
        day_spinner.setAdapter(new ArrayAdapter(getActivity(), R.layout.schedule_spinner, new String[]{getResources().getString(R.string.monday), getResources().getString(R.string.tuesday), getResources().getString(R.string.wednesday), getResources().getString(R.string.thursday), getResources().getString(R.string.friday), getResources().getString(R.string.saturday)}));
        oddWeek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!oddWeek.isChecked() && !evenWeek.isChecked()) evenWeek.setChecked(true);
            }
        });
        evenWeek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!evenWeek.isChecked() && !oddWeek.isChecked()) oddWeek.setChecked(true);
            }
        });

        checkbox_tutor_list = new ArrayList<>();
        Button btn_choose_tutor = (Button) rootLayout.findViewById(R.id.c_l_add_btn_choose_tutor);
        btn_choose_tutor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.fragment_classes_add_dialog);

                set_dialog(dialog.findViewById(R.id.recycler_view));

                Button save = (Button) dialog.findViewById(R.id.btn_c_l_t_dialog_save);
                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String names = "";
                        for (int i = 0; i < checkbox_tutor_list.size(); i++) {
                            if (i != 0)
                                names = names + ", ";
                            //na serveru nefunguje
                            //names = names + checkbox_tutor_list.get(i).get("jmeno") + " " + checkbox_tutor_list.get(i).get("prijmeni") + " [ " + checkbox_tutor_list.get(i).get("login") + " ]";
                            names = checkbox_tutor_list.get(i).get("jmeno") + " " + checkbox_tutor_list.get(i).get("prijmeni") + " [ " + checkbox_tutor_list.get(i).get("login") + " ]";
                        }
                        tutor.setText(names);
                        dialog.dismiss();

                    }
                });
                Button cancel = (Button) dialog.findViewById(R.id.btn_c_l_t_dialog_remove);
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
        c_l_add_lesson_start = new TimePickerDialogOnButton((Button) rootView.findViewById(R.id.c_l_add_lesson_start));
        c_l_add_lesson_end = new TimePickerDialogOnButton((Button) rootView.findViewById(R.id.c_l_add_lesson_end));
        c_l_add_specific_date = new DatePickerDialogOnButton((Button) rootView.findViewById(R.id.c_l_add_specific_date));
        c_l_add_specific_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                c_l_add_specific_date_add.setVisibility(View.VISIBLE);
            }
        });
        c_l_add_specific_date.setText(getResources().getString(R.string.set_specific_date));
        c_l_add_specific_date_add = (Button) rootLayout.findViewById(R.id.c_l_add_specific_date_add);
        c_l_add_specific_date_add.setVisibility(View.GONE);
        c_l_add_specific_date_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counter++;
                String date = TimeLogika.getFormatedDate(c_l_add_specific_date.getDate());
                for(int i = 0; i < lesson_list.size(); i++){
                    if(lesson_list.get(i).get("Datum").equals(date)){
                        return;
                    }
                }
                HashMap<String, String> item = new HashMap<>();

                if (checkbox_tutor_list.size() == 0) {
                    item.put("IDTutor", tutor.getText().toString());
                } else {
                    //stejne jako na serveru se vybere jen posledni vybrany
                    item.put("IDTutor", checkbox_tutor_list.get(checkbox_tutor_list.size()-1).get("idUzivatel"));
                }
                item.put("Nazev", generateClassName() + counter);
                item.put("ZkratkaPredmet", lessonShortcut.getText().toString());
                item.put("Ucebna", room.getText().toString());
                item.put("Den", getResources().getString(
                        TimeLogika.getNameOfDayInWeek(
                                TimeLogika.getDayInWeek(c_l_add_specific_date.getDate())
                        )));
                item.put("HodinaOD", c_l_add_lesson_start.getTime());
                item.put("HodinaDO", c_l_add_lesson_end.getTime());
                String week;
                if (oddWeek.isChecked() && evenWeek.isChecked()) week = "Každý";
                else week = oddWeek.isChecked() ? "Lichý" : "Sudý";
                item.put("Tyden", week);
                item.put("Typ", lecture.isChecked() ? "0" : "1");
                int pocet = limit_number_of_students1.getValue() * 100 + limit_number_of_students2.getValue() * 10 + limit_number_of_students3.getValue();
                item.put("Omezeni", String.valueOf(pocet));
                if (c_l_add_specific_date.isAlreadySet()) {
                    item.put("Datum", date);
                }
                else{
                    return;
                }

                lesson_list.add(item);
                c_l_add_show_classes.setVisibility(View.VISIBLE);
                //adapter_classes.notifyDataSetChanged();
            }
        });
        c_l_add_show_classes = (Button) rootLayout.findViewById(R.id.c_l_add_show_classes);
        c_l_add_show_classes.setVisibility(View.GONE);
        c_l_add_show_classes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AppCompatDialog dialog = new AppCompatDialog(getActivity());
//                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                //todo
                dialog.setContentView(R.layout.fragment_classes_add_dialog_classes);

                set_dialog_classes(dialog.findViewById(R.id.recycler_view));

                Button cancel = (Button) dialog.findViewById(R.id.btn_c_l_t_dialog_remove);
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

        roundButton = (Button) rootView.findViewById(R.id.roundButton);
        roundButton.setText("✔");


        combinate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (combinate.isChecked()){
                    day_spinner.setVisibility(View.GONE);

                    ll_combinate.setVisibility(View.VISIBLE);
                    if(lesson_list.size() > 0){
                        c_l_add_show_classes.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
        present.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (present.isChecked()) {
                    ll_combinate.setVisibility(View.GONE);
                    c_l_add_show_classes.setVisibility(View.GONE);
                    day_spinner.setVisibility(View.VISIBLE);
                }
            }
        });
        present.performClick();



        if(list != null){
            it_is_update();
        }
        this.roundButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                roundButton.setEnabled(false);
                boolean everythingOK = true;
                if (lessonShortcut.length() == 0) {
                    SnackbarManager.show(Snackbar.with(getActivity()).text(R.string.schedule_error_lesson_shortcut_empty));
                    everythingOK = false;
                }
                if (tutor.length() == 0) {
                    SnackbarManager.show(Snackbar.with(getActivity()).text(R.string.schedule_error_lesson_tutor_empty));
                    everythingOK = false;
                }
                if (room.length() == 0) {
                    SnackbarManager.show(Snackbar.with(getActivity()).text(R.string.schedule_error_lesson_room_empty));
                    everythingOK = false;
                }
                if (!everythingOK) {
                    roundButton.setEnabled(true);
                    return;
                } else {
                    String generateName = generateClassName();
                    lessonName.setText(generateName);
                    insert_update();
                }
            }
        });
    }

    private String generateClassName() {
        String generateName = lessonShortcut.getText() + "_" + room.getText() + "_";
        if(present.isChecked())
            generateName += day_spinner.getSelectedItem().toString()
                    .toUpperCase()
                    .substring(0, 2)
                    .replace('Ú', 'U')
                    .replace('Č', 'C')
                    .replace('Á', 'A');
        else
            generateName += getResources().getString(
                    TimeLogika.getNameOfDayInWeek(
                            TimeLogika.getDayInWeek(c_l_add_specific_date.getDate())
                    )
            ).toUpperCase()
                    .substring(0, 2)
                    .replace('Ú', 'U')
                    .replace('Č', 'C')
                    .replace('Á', 'A');

        generateName += "_" + String.valueOf(c_l_add_lesson_start.getHour()) + String.valueOf(c_l_add_lesson_end.getHour()) + "_";
        if(present.isChecked())
            generateName += "P";
        else

            generateName += "K_";
        return generateName;
    }

    private void insert_update() {
        Logging.logAccess("/Pages/Tutor/TridaDetail/TridaSummary.aspx");
        Response response = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException {
                if (response.getString("Success").equals("true")) {
                    //update
                    if(list != null){
                        SnackbarManager.show(
                                Snackbar.with(getActivity())
                                        .text(getActivity().getString(R.string.leasson) + " " + lessonName.getText().toString() + " " + getActivity().getString(R.string.leasson_was_updated)));
                    }
                    //insert
                    else{
                        SnackbarManager.show(
                                Snackbar.with(getActivity())
                                        .text(getActivity().getString(R.string.leasson) + " " + lessonName.getText().toString() + " " + getActivity().getString(R.string.leasson_was_inserted)));
                    }

                    Actionbar.jumpBack();
                } else { //nepovedlo se
                    Log.d("Fragment_classes_add", "insert(): " + response.toString());
                    //update
                    if(list != null){
                        SnackbarManager.show(
                                Snackbar.with(getActivity())
                                        .text(getActivity().getString(R.string.leasson) + " " + lessonName.getText().toString() + " " + getActivity().getString(R.string.leasson_was_not_updated)));
                    }
                    //insert
                    else{
                        SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.leasson) + " " + lessonName.getText().toString() + " " + getActivity().getString(R.string.leasson_was_not_inserted)));
                    }
                }
            }
        };
        JSONObject parameters = new JSONObject();
        try {
            //pokud se jedna o update
            if(list != null){
                parameters.put("IDTrida", list.get("idTrida"));
            }
            if(checkbox_tutor_list.size() == 0){
                parameters.put("IDTutor", list.get("idTutor"));
            }
            else{
                //funguje jako na serveru
                parameters.put("IDTutor", checkbox_tutor_list.get(checkbox_tutor_list.size()-1).get("idUzivatel"));
            }
            parameters.put("Nazev", lessonName.getText().toString());
            parameters.put("ZkratkaPredmet", lessonShortcut.getText().toString());
            parameters.put("Ucebna", room.getText().toString());
            parameters.put("HodinaOD", c_l_add_lesson_start.getTime());
            parameters.put("HodinaDO", c_l_add_lesson_end.getTime());
            String week;
            if (this.oddWeek.isChecked() && this.evenWeek.isChecked()) week = "Každý";
            else week = this.oddWeek.isChecked() ? "Lichý" : "Sudý";
            parameters.put("Tyden", week);
            parameters.put("Typ", lecture.isChecked() ? 0 : 1);
            int pocet = limit_number_of_students1.getValue() * 100 + limit_number_of_students2.getValue() * 10 + limit_number_of_students3.getValue();
            parameters.put("Omezeni", String.valueOf(pocet));
//            if (c_l_add_specific_date.isAlreadySet()) {
//                parameters.put("Datum", c_l_add_specific_date.getDate());
//            }

            if(present.isChecked()){
                parameters.put("FormaStudia", EnumLogika.idFormaStudia(present.getText().toString()));
                parameters.put("Den", day_spinner.getSelectedItem().toString());
            }
            else{
                parameters.put("FormaStudia", EnumLogika.idFormaStudia(combinate.getText().toString()));
                JSONArray comb_classes_array = new JSONArray();
                for(int i = 0; i < lesson_list.size(); i++) {
                    if(i == 0){
                        parameters.put("Den", lesson_list.get(0).get("Den"));
                    }
                    JSONObject comb_class = new JSONObject();

                    comb_class.put("IDTutor", lesson_list.get(i).get("IDTutor"));
                    comb_class.put("TutorName", lesson_list.get(i).get("TutorName"));
                    comb_class.put("TutorSurname", lesson_list.get(i).get("TutorSurname"));
                    comb_class.put("Nazev", lesson_list.get(i).get("Nazev"));
                    comb_class.put("ZkratkaPredmet", lesson_list.get(i).get("ZkratkaPredmet"));
                    comb_class.put("Ucebna", lesson_list.get(i).get("Ucebna"));
                    comb_class.put("Den", lesson_list.get(i).get("Den"));
                    comb_class.put("HodinaOD", lesson_list.get(i).get("HodinaOD"));
                    comb_class.put("HodinaDO", lesson_list.get(i).get("HodinaDO"));
                    comb_class.put("Tyden", lesson_list.get(i).get("Tyden"));
                    comb_class.put("Typ", lesson_list.get(i).get("Typ"));
                    comb_class.put("Omezeni", lesson_list.get(i).get("Omezeni"));
                    comb_class.put("FormaStudia", EnumLogika.idFormaStudia(combinate.getText().toString()));
                    comb_class.put("Datum", TimeLogika.getServerTimeFromFormated(lesson_list.get(i).get("Datum")));

                    comb_classes_array.put(comb_class);
                }
                parameters.put("CombinedClasses", comb_classes_array);
            }

        } catch (JSONException e) {
            Log.d("Fragment_cc_add_term", "insert(): " + e.getMessage());
        }
        //pokud jde o update
        if(list != null){
            Request.post(URL.SchoolClass.updateschoolclass(), parameters, response);
        }
        //vklada se nova trida
        else{
            Request.post(URL.SchoolClass.insertschoolclass(User.courseInfoId), parameters, response);
        }
    }

    private void set_dialog(View view) {
        recyclerView_dialog = (RecyclerView) view.findViewById(R.id.recyclerView);
        nothinglayout_dialog = (LinearLayout) view.findViewById(R.id.nothing_to_show);
        nothinglayout_dialog.setVisibility(View.GONE);
        loading_progress_dialog = (ProgressBar) view.findViewById(R.id.loading_progress);

        tutors_list_dialog = new ArrayList<>();

        adapter_dialog = new RecyclerView.Adapter() {
            class ViewHolderHeader extends RecyclerView.ViewHolder {
                public ViewHolderHeader(View itemView) {
                    super(itemView);
                }
            }

            class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
                public CheckBox checkBox;
                public TextView login;
                public TextView surname;
                public TextView name;
                public TextView email;

                public ViewHolder(View itemView) {
                    super(itemView);
                    itemView.setOnClickListener(this);
                    this.checkBox = (CheckBox) itemView.findViewById(R.id.c_l_t_checkbox);
                    this.login = (TextView) itemView.findViewById(R.id.c_l_t_login);
                    this.surname = (TextView) itemView.findViewById(R.id.c_l_t_surname);
                    this.name = (TextView) itemView.findViewById(R.id.c_l_t_name);
                    this.email = (TextView) itemView.findViewById(R.id.c_l_t_email);
                }

                @Override
                public void onClick(View view) {

                }

            }

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_list_of_tutors, parent, false);
                return new ViewHolder(view);
            }
            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
                final ViewHolder viewHolder = (ViewHolder) holder;

                viewHolder.login.setText(tutors_list_dialog.get(position).get("login"));
                viewHolder.surname.setText(tutors_list_dialog.get(position).get("prijmeni"));
                viewHolder.name.setText(tutors_list_dialog.get(position).get("jmeno"));
                viewHolder.email.setText(tutors_list_dialog.get(position).get("email"));

                final int finalPosition = position;
                //objekt neni v seznamu k exportu → nezaskrtly checkbox
                if(checkbox_tutor_list.indexOf(tutors_list_dialog.get(finalPosition)) == -1) {
                    viewHolder.checkBox.setChecked(false);
                } else {
                    viewHolder.checkBox.setChecked(true);
                }
                viewHolder.checkBox.setOnClickListener(new View.OnClickListener() {
                                                           @Override
                                                           public void onClick(View v) {
                                                               if (viewHolder.checkBox.isChecked()) {
                                                                   //pridat k exportu
                                                                   checkbox_tutor_list.add(tutors_list_dialog.get(finalPosition));
                                                               } else {
                                                                   //odebrat z exportu
                                                                   checkbox_tutor_list.remove(tutors_list_dialog.get(finalPosition));
                                                               }
                                                           }
                                                       }
                );
            }

            @Override
            public int getItemViewType(int position)
            {
                return 1;
            }

            @Override
            public int getItemCount() {
                return tutors_list_dialog.size();
            }
        };
        recyclerView_dialog.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView_dialog.addItemDecoration(new DividerItemDecoration(getActivity()));
        recyclerView_dialog.setHasFixedSize(true);
        recyclerView_dialog.setAdapter(adapter_dialog);
        get_tutors();

    }

    private void set_dialog_classes(final View view){

        recyclerView_classes = (RecyclerView) view.findViewById(R.id.recyclerView);
        nothinglayout_classes = (LinearLayout) view.findViewById(R.id.nothing_to_show);
        nothinglayout_classes.setVisibility(View.GONE);

        adapter_classes = new RecyclerView.Adapter() {
            class ViewHolderHeader extends RecyclerView.ViewHolder {
                public ViewHolderHeader(View itemView) {
                    super(itemView);
                }
            }

            class ViewHolder_combinate extends RecyclerView.ViewHolder implements View.OnClickListener{
                public TextView name;
                public TextView leasson;
                public TextView day;
                public TextView lecture_room;
                public TextView date;
                public Button button;
                public LinearLayout button_layout;
                public LinearLayout ll_right_half;
                public LinearLayout ll_black;

                public ViewHolder_combinate(View itemView) {
                    super(itemView);
                    itemView.setOnClickListener(this);
                    this.name = (TextView) itemView.findViewById(R.id.c_l_c_name);
                    this.leasson = (TextView) itemView.findViewById(R.id.c_l_c_leasson);
                    this.day = (TextView) itemView.findViewById(R.id.c_l_c_day);
                    this.lecture_room = (TextView) itemView.findViewById(R.id.c_l_c_lecture_room);
                    this.date = (TextView) itemView.findViewById(R.id.c_l_c_date);
                    this.button = (Button) itemView.findViewById(R.id.c_l_c_button);
                    this.button_layout = (LinearLayout) itemView.findViewById(R.id.c_l_c_ll_button);
                    this.ll_right_half = (LinearLayout) itemView.findViewById(R.id.c_l_c_ll_right_half);
                    this.ll_black = (LinearLayout) itemView.findViewById(R.id.c_l_c_ll_black);
                }

                @Override
                public void onClick(View view) {
                }
            }

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_classes_list_combinate_list, parent, false);
                if(lesson_list.size() == 0){
                    nothinglayout_classes.setVisibility(View.VISIBLE);
                }
                else{
                    nothinglayout_classes.setVisibility(View.GONE);
                }
                return new ViewHolder_combinate(view);
            }

            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
                if(holder.getItemViewType() == 3){
                    final ViewHolder_combinate viewHolder = (ViewHolder_combinate) holder;
                    viewHolder.name.setText(lesson_list.get(position).get("Nazev"));
                    viewHolder.leasson.setText(lesson_list.get(position).get("HodinaOD") + " - " + lesson_list.get(position).get("HodinaDO"));
                    viewHolder.day.setText(lesson_list.get(position).get("Den"));
                    viewHolder.lecture_room.setText(lesson_list.get(position).get("Ucebna"));
                    viewHolder.date.setText(lesson_list.get(position).get("Datum"));
                    //todo
                    viewHolder.button_layout.setVisibility(View.VISIBLE);
                    viewHolder.button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            lesson_list.remove(position);
                            adapter_classes.notifyItemRemoved(position);
                            notifyItemRangeChanged(position, lesson_list.size());
                        }
                    });
                    viewHolder.ll_right_half.setVisibility(View.GONE);
                    viewHolder.ll_black.setBackgroundResource(R.drawable.ab_transparent_white);
                }
            }

            @Override
            public int getItemViewType(int position)
            {
                return 3;
            }

            @Override
            public int getItemCount() {
                return lesson_list.size();
            }
        };
        recyclerView_classes.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView_classes.addItemDecoration(new DividerItemDecoration(getActivity()));
        recyclerView_classes.setHasFixedSize(true);
        recyclerView_classes.setAdapter(adapter_classes);
    }
    private void get_tutors() {
        nothinglayout_dialog.setVisibility(View.GONE);
        loading_progress_dialog.setVisibility(View.VISIBLE);
        tutors_list_dialog.clear();
        adapter_dialog.notifyDataSetChanged();

        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                if (response.length() > 0)
                {
                    for (int j = 0; j < response.length(); j++)
                    {
                        tutors_list_dialog.add(put_tutor_data(
                                response.getJSONObject(j).getString("IdUzivatel"),
                                response.getJSONObject(j).getString("Login"),
                                response.getJSONObject(j).getString("Jmeno"),
                                response.getJSONObject(j).getString("Prijmeni"),
                                response.getJSONObject(j).getString("PrijmeniRodne"),
                                response.getJSONObject(j).getString("Email"),
                                response.getJSONObject(j).getString("TitulPred"),
                                response.getJSONObject(j).getString("TitulZa"),
                                response.getJSONObject(j).getString("JmenoCele")));
                    }
                }
                else{
                    Log.d("Chyba", "classes_add get_tutors");
                    nothinglayout_dialog.setVisibility(View.VISIBLE);
                }
                loading_progress_dialog.setVisibility(View.GONE);
                adapter_dialog.notifyDataSetChanged();
            }
        };
        //-1, protoze Kuba rekl dej tam -1 a funguje to
        Request.get(URL.User.getTutorsByIdKurz(User.courseInfoId, -1), arrayResponse);
    }

    private Map<String, String> put_tutor_data(String idUzivatel, String login, String jmeno, String prijmeni,
                                               String prijmeniRodne, String email, String titulPred, String titulZa, String jmenoCele) {
        HashMap<String, String> item = new HashMap<String, String>();

        item.put("idUzivatel",idUzivatel);
        item.put("login",login);
        item.put("jmeno",jmeno);
        item.put("prijmeni",prijmeni);
        item.put("prijmeniRodne",prijmeniRodne);
        item.put("email",email);
        item.put("titulPred",titulPred);
        item.put("titulZa",titulZa);
        item.put("jmenoCele",jmenoCele);

        return item;
    }

    private void it_is_update() {
        //todo combined
        tutor.setText(list.get("tutorName") + " " + list.get("tutorSurname"));// + " [ " + checkbox_tutor_list.get(i).get("login") + " ]";
        lessonName.setText(list.get("nazev"));
        lessonShortcut.setText(list.get("zkratkaPredmet"));
        oddWeek.setChecked(list.get("tyden").contentEquals("Lichý") || list.get("tyden").contentEquals("Každý"));
        evenWeek.setChecked(list.get("tyden").contentEquals("Sudý") || list.get("tyden").contentEquals("Každý"));
        room.setText(list.get("ucebna"));

        c_l_add_lesson_start.setTime(list.get("hodinaOD"));
        c_l_add_lesson_end.setTime(list.get("hodinaDO"));
        if(!list.get("datum").equals("null")) {
            c_l_add_specific_date.setDate(list.get("datum"));
        }
        if (list.get("den").compareTo("Pondělí") == 0) day_spinner.setSelection(0);
        else if (list.get("den").compareTo("Úterý") == 0) day_spinner.setSelection(1);
        else if (list.get("den").compareTo("Středa") == 0) day_spinner.setSelection(2);
        else if (list.get("den").compareTo("Čtvrtek") == 0) day_spinner.setSelection(3);
        else if (list.get("den").compareTo("Pátek") == 0) day_spinner.setSelection(4);
        else if (list.get("den").compareTo("Sobota") == 0) day_spinner.setSelection(5);
        else day_spinner.setSelection(6);
        if (list.get("typ").equals("1")) exercise.setChecked(true);

        int omezeni_studentu = Integer.valueOf(list.get("omezeni"));
        limit_number_of_students1.setValue(omezeni_studentu / 100);
        omezeni_studentu %= 100;
        limit_number_of_students2.setValue(omezeni_studentu / 10);
        omezeni_studentu %= 10;
        limit_number_of_students3.setValue(omezeni_studentu);

        String date = TimeLogika.getFormatedDate(c_l_add_specific_date.getDate());
        for(int i = 0; i < lesson_list.size(); i++){
            if(lesson_list.get(i).get("Datum").equals(date)){
                return;
            }
        }

        if(combinated_list != null){
            for (Map<String, String> item : combinated_list) {
                Map<String, String> lesson = new HashMap<>();
                lesson.put("IDTutor", item.get("idTutor"));
                lesson.put("TutorName", item.get("tutorName"));
                lesson.put("TutorSurname", item.get("tutorSurname"));
                lesson.put("Nazev", item.get("nazev"));
                lesson.put("ZkratkaPredmet", item.get("zkratkaPredmet"));
                lesson.put("Ucebna", item.get("ucebna"));
                lesson.put("Den", item.get("den"));
                lesson.put("HodinaOD", item.get("hodinaOD"));
                lesson.put("HodinaDO", item.get("hodinaDO"));
                lesson.put("Tyden", item.get("tyden"));
                lesson.put("Typ", item.get("typ"));
                lesson.put("Omezeni", item.get("omezeni"));
                lesson.put("Datum", item.get("datum"));
                lesson_list.add(lesson);
            }
            counter = combinated_list.size();
        }
        if(list.get("formaStudia").equals("1")){
            combinate.performClick();
        }

    }

}
