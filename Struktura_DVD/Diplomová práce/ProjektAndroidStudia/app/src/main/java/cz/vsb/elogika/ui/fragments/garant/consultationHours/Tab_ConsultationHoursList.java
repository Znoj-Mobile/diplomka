package cz.vsb.elogika.ui.fragments.garant.consultationHours;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.dataTypes.ConsultationHour;
import cz.vsb.elogika.utils.DividerItemDecoration;
import cz.vsb.elogika.utils.TimeLogika;

public class Tab_ConsultationHoursList extends Fragment
{
    private RecyclerView.Adapter adapter;
    private View loadingProgress;
    private View nothingToShow;
    public ArrayList<ConsultationHour> consultationHours;
    public Boolean areDataReady;
    public Object lock;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        final View rootView = inflater.inflate(R.layout.recycler_view_with_button, container, false);
        // Přidání hlavičky
        ((ViewGroup) rootView.findViewById(R.id.recyclerViewLayout)).addView(LayoutInflater.from(getActivity()).inflate(R.layout.fragment_consultation_hours_list_header, container, false), 0);
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        this.loadingProgress = rootView.findViewById(R.id.loading_progress);
        this.nothingToShow = rootView.findViewById(R.id.nothing_to_show);
        this.loadingProgress.setVisibility(View.VISIBLE);

        // Vytvoření adaptéru
        this.adapter = new RecyclerView.Adapter()
        {
            class ViewHolder extends RecyclerView.ViewHolder
            {
                public TextView range;
                public TextView time;
                public TextView termsCount;
                public TextView repeating;
                public TextView maximumOfStudents;
                public TextView description;
                public Button edit;
                public Button delete;

                public ViewHolder(View itemView)
                {
                    super(itemView);
                    this.range = (TextView) itemView.findViewById(R.id.range);
                    this.time = (TextView) itemView.findViewById(R.id.time);
                    this.termsCount = (TextView) itemView.findViewById(R.id.terms_count);
                    this.repeating = (TextView) itemView.findViewById(R.id.repeating);
                    this.maximumOfStudents = (TextView) itemView.findViewById(R.id.maximum_of_students);
                    this.description = (TextView) itemView.findViewById(R.id.description);
                    this.edit = (Button) itemView.findViewById(R.id.edit);
                    this.delete = (Button) itemView.findViewById(R.id.delete);
                }
            }

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
            {
                View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_consultation_hours_list_item, parent, false);
                return new ViewHolder(view);
            }

            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position)
            {
                final ViewHolder holder = (ViewHolder) viewHolder;
                final ConsultationHour consultationHour = consultationHours.get(position);

                // Nastavení listenerů na tlačítka
                holder.edit.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        Bundle bundle = new Bundle();
                        bundle.putInt("id", consultationHour.id);
                        bundle.putString("description", consultationHour.description);
                        bundle.putString("room", consultationHour.room);
                        bundle.putInt("repeatableType", consultationHour.repeatableType);
                        bundle.putBoolean("notifyViaEmail", consultationHour.notifyViaEmail);
                        bundle.putInt("maximumOfStudents", consultationHour.maximumOfStudents);
                        bundle.putString("from", consultationHour.from);
                        bundle.putString("to", consultationHour.to);
                        bundle.putString("fromHours", consultationHour.fromHours);
                        bundle.putString("toHours", consultationHour.toHours);
                        Fragment_ConsultationHoursCreationAndModification fragment = new Fragment_ConsultationHoursCreationAndModification();
                        fragment.setArguments(bundle);
                        getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment).commit();
                    }
                });
                holder.delete.findViewById(R.id.delete).setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        new AlertDialog.Builder(getActivity()).setTitle(R.string.are_you_sure).setMessage(R.string.delete_consultation_hour_question).setPositiveButton(R.string.yes, new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int which)
                            {
                                removeConsultationHour(position);
                            }
                        }).setNegativeButton(R.string.no, null).setIcon(android.R.drawable.ic_dialog_alert).show();
                    }
                });

                holder.range.setText(TimeLogika.getFormatedDate(consultationHour.from) + " - " + TimeLogika.getFormatedDate(consultationHour.to));
                holder.time.setText(consultationHour.fromHours + " - " + consultationHour.toHours);
                holder.termsCount.setText(String.valueOf(String.valueOf(consultationHour.termsCount)));
                int repeating = 0;
                switch(consultationHour.repeatableType)
                {
                    case 0:
                        repeating = R.string.no;
                        break;

                    case 1:
                        repeating = R.string.weekly;
                        break;

                    case 2:
                        repeating = R.string.fortnightly;
                        break;

                    case 3:
                        repeating = R.string.monthly;
                        break;
                }
                holder.repeating.setText(getResources().getString(repeating));
                holder.maximumOfStudents.setText(String.valueOf(consultationHour.maximumOfStudents));
                holder.description.setText(consultationHour.description);
            }

            @Override
            public int getItemCount()
            {
                return consultationHours.size();
            }
        };
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(this.adapter);

        Button roundButton = (Button) rootView.findViewById(R.id.roundButton);
        roundButton.setText("+");
        roundButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, new Fragment_ConsultationHoursCreationAndModification()).commit();
            }
        });

        synchronized (this.lock)
        {
            if (this.areDataReady) notifyDataSetChanged();
        }

        return rootView;
    }



    public void notifyDataSetChanged()
    {
        this.loadingProgress.setVisibility(View.GONE);
        this.adapter.notifyDataSetChanged();
        if (consultationHours.size() == 0)
        {
            nothingToShow.findViewById(R.id.nothing_to_show).setVisibility(View.VISIBLE);
        }
    }



    /**
     * Vymaže tutora.
     * @param position index v poli consultationHours
     */
    private void removeConsultationHour(final int position)
    {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                consultationHours.remove(position);
                adapter.notifyItemRemoved(position);
                SnackbarManager.show(Snackbar.with(getActivity()).type(SnackbarType.MULTI_LINE).text(R.string.consultation_hour_deleted_successfully));
            }
        };
        Request.get(URL.ConsultationHours.Remove(this.consultationHours.get(position).id), response);
    }
}
