package cz.vsb.elogika.dataTypes;

public class Lesson
{
    public int timetableID;
    public String name;
    public String shortcut;
    public int type;
    public String from;
    public String to;
    public String room;
    public int day;
    public String week;
    public String date;
    public String teacherDegreeBeforeName;
    public String teacherName;
    public String teacherSurname;
    public String teacherDegreeAfterName;
    public boolean isLogged;
    public int loggedCount;
    public int spaces;
}
