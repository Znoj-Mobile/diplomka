package cz.vsb.elogika.dataTypes;

public class Template
{
    public int id;
    public String name;
    public String description;
    public int blockCount;
    public boolean blockMix;
    public boolean allMix;
}
