package cz.vsb.elogika.ui.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cz.vsb.elogika.R;
import cz.vsb.elogika.common.view.SlidingTabLayout;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.ui.fragments.RegistrationForExaminations.RegistrationForExaminations;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.EnumLogika;
import cz.vsb.elogika.utils.SpecialAdapter;
import cz.vsb.elogika.utils.TimeLogika;


public class Fragment_RegistrationForExaminations extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    static final String LOG_TAG = "Fragment_Activities";

    private View rootView;

    public static RegistrationForExaminations activity;
    public static RegistrationForExaminations groupActivity;
    public static RegistrationForExaminations test;

    LinearLayout nothinglayout;

    public String[] from = {"name", "activity_name", "sign_in_from", "sign_in_to", "room", "max", "number_of_signed", "date_id"};
    public int[] to = { R.id.reg_name, R.id.reg_activity_name, R.id.reg_sign_in_from, R.id.reg_sign_in_to,  R.id.reg_room,  R.id.reg_max, R.id.reg_number_of_signed};

    public ArrayList<Map<String, String>> activities_group_list;
    public SpecialAdapter adapter_1;

    public ArrayList<Map<String, String>> tests_list;
    public SpecialAdapter adapter_2;

    public ArrayList<Map<String, String>> activities_list;
    public SpecialAdapter adapter_3;

    private static Fragment_RegistrationForExaminations instance;

    public static Fragment_RegistrationForExaminations getInstance() {
        if(instance == null){
            instance = new Fragment_RegistrationForExaminations();
        }
        return instance;
    }

    public Fragment_RegistrationForExaminations(){
        instance = this;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.tabs_container, container, false);
        Actionbar.newCategory(R.string.registration_for_examination, this);

        nothinglayout = (LinearLayout) rootView.findViewById(R.id.nothing_to_show);
        nothinglayout.setVisibility(View.GONE);

        activities_group_list = new ArrayList<>();
        tests_list = new ArrayList<>();
        activities_list = new ArrayList<>();

        ViewPager mViewPager = (ViewPager) rootView.findViewById(R.id.viewpager);
        mViewPager.setAdapter(new FragmentStatePagerAdapter(getFragmentManager()) {
                                  String[] tabNames = new String[]{
                                          getString(R.string.activityGroup),
                                          getString(R.string.tests),
                                          getString(R.string.activity)};

                                  @Override
                                  public int getCount() {
                                      return tabNames.length;
                                  }

                                  @Override
                                  public CharSequence getPageTitle(int position) {
                                      return tabNames[position];
                                  }

                                  @Override
                                  public Fragment getItem(int position) {

                                      Bundle bundle = new Bundle();
                                      bundle.putInt("type", position);
                                      switch (position) {
                                          case 0:
                                              //update se vola uvnitr tridy RegistrationForExaminations() pro groupActivity
                                              groupActivity = new RegistrationForExaminations();
                                              groupActivity.setArguments(bundle);
                                              return groupActivity;
                                          case 1:
                                              test = new RegistrationForExaminations();
                                              test.setArguments(bundle);
                                              return test;
                                          case 2:
                                              activity = new RegistrationForExaminations();
                                              activity.setArguments(bundle);
                                              return activity;
                                          default:
                                              return null;
                                      }
                                  }
                              }
        );
        SlidingTabLayout mSlidingTabLayout = (SlidingTabLayout) rootView.findViewById(R.id.sliding_tabs);
        mSlidingTabLayout.setViewPager(mViewPager);
        return rootView;
    }

    private void data_into_list(final SwipeRefreshLayout swipeLayout)
    {
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            protected int typ;
            Map<String, String> pomItem = new HashMap<>();
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                if (response.length() > 0)
                {
                    for (int j = 0; j < response.length(); j++)
                    {
                        typ = Integer.parseInt(response.getJSONObject(j).getString("ActivityType"));
                        //Log.d("typ response: ",response.getJSONObject(j).getString("ActivityType"));
                        //Log.d("Vypis: ","" + response.getJSONObject(j).getJSONArray("Dates").getJSONObject(0).getString("DateId"));
                        //predelat na ArrayResponse...
                        JSONObject o = response.getJSONObject(j);
                        JSONArray ja = o.getJSONArray("Dates");

                        if (ja.length() > 0)
                        {
                            for (int h = 0; h < ja.length(); h++)
                            {

                                pomItem = put_data(
                                        ja.getJSONObject(h).getString("DateId"),
                                        ja.getJSONObject(h).getString("Name"),
                                        response.getJSONObject(j).getString("ActivityName"),
                                        TimeLogika.getFormatedDateTime(ja.getJSONObject(h).getString("LoginFrom")),
                                        TimeLogika.getFormatedDateTime(ja.getJSONObject(h).getString("LoginTo")),
                                        TimeLogika.getFormatedDateTime(ja.getJSONObject(h).getString("LogoutTo")),
                                        TimeLogika.getFormatedDateTime(ja.getJSONObject(h).getString("AvailableFrom")),
                                        TimeLogika.getFormatedDateTime(ja.getJSONObject(h).getString("AvailableTo")),
                                        ja.getJSONObject(h).getString("Place"),
                                        ja.getJSONObject(h).getString("LoginIsRequired"),
                                        ja.getJSONObject(h).getString("MaxStudents"),
                                        ja.getJSONObject(h).getString("LoginStudents"),
                                        ja.getJSONObject(h).getString("IsLogged"));

                                //typ == 0 → skupina aktivit
                                if(typ == 0){//Log.d("Vypis: ","" + response.getJSONObject(j).getJSONArray("Dates").getJSONObject(0).getString("DateId"));
                                    activities_group_list.add(pomItem);
                                }
                                //typ == 1 → test
                                else if(typ == 1){
                                    tests_list.add(pomItem);
                                }
                                //typ == 2 → aktivita
                                else{
                                    activities_list.add(pomItem);
                                }
                            }
                        }
                    }
                }
//                else{
//                    Log.d("Chyba", "");
//                    nothinglayout.setVisibility(View.VISIBLE);
//                }
                if(activities_group_list.size() == 0){
                    groupActivity.showNothingLayout();
                }
                else{
                    groupActivity.hideNothingLayout();
                }
                if(tests_list.size() == 0){
                    test.showNothingLayout();
                }
                else{
                    test.hideNothingLayout();
                }
                if(activities_list.size() == 0){
                    if(activity != null)
                        activity.showNothingLayout();
                }
                else{
                    if(activity != null)
                        activity.hideNothingLayout();
                }

                swipeLayout.setRefreshing(false);

                adapter_1.notifyDataSetChanged();
                adapter_2.notifyDataSetChanged();
                if(adapter_3 != null)
                    adapter_3.notifyDataSetChanged();
            }
        };
        Request.get(URL.CourseConditions.ActivityDates(User.courseInfoId, User.id), arrayResponse);
    }

    private Map<String,String> put_data(String date_id, String name, String activity_name, String sign_in_from, String sign_in_to, String sign_off_to, String active_from, String active_to, String room, String loginIsRequired, String max, String number_of_signed, String signed) {
        HashMap<String, String> item = new HashMap<>();
        item.put("date_id", date_id);
        item.put("name", name);
        item.put("activity_name", activity_name);
        item.put("sign_in_from", sign_in_from);
        item.put("sign_in_to", sign_in_to);
        item.put("sign_off_to", sign_off_to);
        item.put("active_from", active_from);
        item.put("active_to", active_to);
        item.put("room", room);
        item.put("loginIsRequired", loginIsRequired);
        item.put("max", max);
        item.put("number_of_signed", number_of_signed);
        item.put("signed", signed);
        return item;
    }
    //naplneni dialogu detaily - plati pro activities_group, tests i activities
    public void fill_detail(View view, final int position, final ArrayList<Map<String, String>> list){
        final Dialog dialog_f = new Dialog(getActivity());
        dialog_f.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_f.setContentView(R.layout.fragment_registration_detail);

        TextView reg_name = (TextView) dialog_f.findViewById(R.id.reg_name);
        reg_name.setText(list.get(position).get("name"));
        final TextView reg_activity_name = (TextView) dialog_f.findViewById(R.id.reg_activity_name);
        reg_activity_name.setText(list.get(position).get("activity_name"));
        TextView reg_sign_in_from = (TextView) dialog_f.findViewById(R.id.reg_sign_in_from);
        reg_sign_in_from.setText(list.get(position).get("sign_in_from"));
        TextView reg_sign_in_to = (TextView) dialog_f.findViewById(R.id.reg_sign_in_to);
        reg_sign_in_to.setText(list.get(position).get("sign_in_to"));
        TextView reg_sign_off_to = (TextView) dialog_f.findViewById(R.id.reg_sign_off_to);
        reg_sign_off_to.setText(list.get(position).get("sign_off_to"));
        TextView reg_active_from = (TextView) dialog_f.findViewById(R.id.reg_active_from);
        reg_active_from.setText(list.get(position).get("active_from"));
        TextView reg_active_to = (TextView) dialog_f.findViewById(R.id.reg_active_to);
        reg_active_to.setText(list.get(position).get("active_to"));
        TextView reg_room = (TextView) dialog_f.findViewById(R.id.reg_room);
        reg_room.setText(list.get(position).get("room"));
        TextView reg_loginIsRequired = (TextView) dialog_f.findViewById(R.id.reg_loginIsRequired);
        reg_loginIsRequired.setText(EnumLogika.povinnyAnoNe(list.get(position).get("loginIsRequired")));
        TextView reg_max = (TextView) dialog_f.findViewById(R.id.reg_max);
        reg_max.setText(list.get(position).get("max"));
        TextView reg_number_of_signed = (TextView) dialog_f.findViewById(R.id.reg_number_of_signed);
        reg_number_of_signed.setText(list.get(position).get("number_of_signed"));
        final Button btn_sign = (Button) dialog_f.findViewById(R.id.btn_sign);
        btn_sign.setVisibility(View.VISIBLE);
        if(list.get(position).get("signed") == "true"){
            btn_sign.setText("" + getString(R.string.logout));
            btn_sign.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    logout(getActivity(), list.get(position).get("date_id"), btn_sign);
                    dialog_f.dismiss();
                }
            });
        }
        else{
            btn_sign.setText("" + getString(R.string.login));
            btn_sign.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    login(getActivity(), list.get(position).get("date_id"), btn_sign);
                    dialog_f.dismiss();
                }
            });
        }
        /*
        Calendar rightNow = Calendar.getInstance();
        Calendar c_active_from = Calendar.getInstance();
        Calendar c_active_to = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.US);
        try {
            //je potreba nahradit T
            Log.d("TIME FROM", list.get(position).get("active_from"));
            Log.d("TIME TO", list.get(position).get("active_to"));
            c_active_from.setTime(sdf.parse(list.get(position).get("active_from")));
            c_active_to.setTime(sdf.parse(list.get(position).get("active_to")));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //Log.d("TIME FROM", sdf.format(c_active_from.getTime()));
        //Log.d("TIME NOW", sdf.format(rightNow.getTime()));
        //Log.d("TIME TO", sdf.format(c_active_to.getTime()));
        */
        if(TimeLogika.isActive(list.get(position).get("active_from"), list.get(position).get("active_to"))){
            btn_sign.setEnabled(true);
        }
        else{
            btn_sign.setEnabled(false);
        }
        dialog_f.show();
    }

    public void login(final Context context, final String dateId, final String name, final CheckBox cb){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.login_title);
        builder.setCancelable(false);
        builder.setMessage(R.string.login_question);
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Response response = new Response() {
                    @Override
                    public void onResponse(JSONObject response) throws JSONException {
                        Log.d("login odpoved", response.toString());
                        Log.d("typ response: ", response.getString("ResultType"));
                        if (response.getString("ResultType").equals("0")) {
                            //Toast.makeText(context, R.string.login_success, Toast.LENGTH_SHORT).show();
                            SnackbarManager.show(
                                    Snackbar.with(context)
                                            .text(context.getResources().getString(R.string.login_success)));
                            //cb.setChecked(true);
                        } else {
                            //Toast.makeText(context, R.string.login_failure, Toast.LENGTH_SHORT).show();
                            SnackbarManager.show(
                                    Snackbar.with(context)
                                            .text(context.getResources().getString(R.string.login_failure)));
                            //cb.setChecked(false);
                        }
                        update();
                    }
                };
                JSONObject parameters = new JSONObject();
                Request.post(URL.CourseConditions.LoginToDate(User.id, Integer.parseInt(dateId)), parameters, response);
            }
        });
        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which) {
                cb.setChecked(false);
            }
        }).setIcon(android.R.drawable.ic_dialog_alert);
        Dialog d = builder.create();
        d.show();
    }

    public void login(final Context context, String dateId, final Button btn_sign) {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                Log.d("login odpoved", response.toString());
                Log.d("typ response: ",response.getString("ResultType"));
                if(response.getString("ResultType").equals("0")){
                    //Toast.makeText(context, R.string.login_success, Toast.LENGTH_SHORT).show();
                    SnackbarManager.show(
                            Snackbar.with(context)
                                    .text(context.getResources().getString(R.string.login_success)));
                }
                else{
                    //Toast.makeText(context, R.string.login_failure, Toast.LENGTH_SHORT).show();
                    SnackbarManager.show(
                            Snackbar.with(context)
                                    .text(context.getResources().getString(R.string.login_failure)));
                }
                update();
            }
        };
        JSONObject parameters = new JSONObject();
        Request.post(URL.CourseConditions.LoginToDate(User.id, Integer.parseInt(dateId)), parameters, response);
    }


    public void logout(final Context context, final String dateId, final String name, final CheckBox cb){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.logout_title);
        builder.setCancelable(false);
        builder.setMessage(R.string.logout_question);
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which) {
                Response response = new Response()
                {
                    @Override
                    public void onResponse(JSONObject response) throws JSONException
                    {
                        Log.d("logout odpoved", response.toString());
                        Log.d("typ response: ",response.getString("ResultType"));
                        if(response.getString("ResultType").equals("0")){
                            //Toast.makeText(context, R.string.logout_success, Toast.LENGTH_SHORT).show();
                            SnackbarManager.show(
                                    Snackbar.with(context)
                                            .text(context.getResources().getString(R.string.logout_success)));
                            //cb.setChecked(false);
                        }
                        else{
                            SnackbarManager.show(
                                    Snackbar.with(context)
                                            .text(context.getResources().getString(R.string.logout_failure)));
                        }
                        update();
                    }
                };
                JSONObject parameters = new JSONObject();
                Request.post(URL.CourseConditions.LogoutFromDate(User.id, Integer.parseInt(dateId)), parameters, response);
            }
        });
        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which) {
                cb.setChecked(true);
            }
        }).setIcon(android.R.drawable.ic_dialog_alert);
        Dialog d = builder.create();
        d.show();
    }

    public void logout(final Context context, String dateId, final Button btn_sign) {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                Log.d("logout odpoved", response.toString());
                Log.d("typ response: ",response.getString("ResultType"));
                if(response.getString("ResultType").equals("0")){
                    //Toast.makeText(context, R.string.logout_success, Toast.LENGTH_SHORT).show();
                    SnackbarManager.show(
                            Snackbar.with(context)
                                    .text(context.getResources().getString(R.string.logout_success)));
                    //cb.setChecked(false);
                }
                else{
                    SnackbarManager.show(
                            Snackbar.with(context)
                                    .text(context.getResources().getString(R.string.logout_failure)));
                }
                update();
            }
        };
        JSONObject parameters = new JSONObject();
        Request.post(URL.CourseConditions.LogoutFromDate(User.id, Integer.parseInt(dateId)), parameters, response);
    }

    @Override
    public void onRefresh() {
        update();
    }

    private void update() {
        while(true) {
            if (groupActivity != null) {
                update(groupActivity.swipeLayout);
                return;
            } else if (test != null) {
                update(test.swipeLayout);
                return;
            } else if (activity != null) {
                update(activity.swipeLayout);
                return;
            } else {
                try {
                    wait(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void update(final SwipeRefreshLayout swipeLayout) {
        nothinglayout.setVisibility(View.GONE);
        clear_list();
        if (adapter_1 != null) adapter_1.notifyDataSetChanged();
        if (adapter_2 != null) adapter_2.notifyDataSetChanged();
        if (adapter_3 != null) adapter_3.notifyDataSetChanged();
        data_into_list(swipeLayout);
        if(swipeLayout != null) {
            swipeLayout.post(new Runnable() {
                @Override
                public void run() {
                    swipeLayout.setRefreshing(true);
                }
            });
        }
    }

    private void clear_list() {
        activities_group_list.clear();
        tests_list.clear();
        activities_list.clear();
    }

}