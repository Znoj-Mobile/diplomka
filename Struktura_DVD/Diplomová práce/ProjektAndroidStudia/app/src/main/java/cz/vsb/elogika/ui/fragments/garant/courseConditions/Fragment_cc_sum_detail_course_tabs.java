package cz.vsb.elogika.ui.fragments.garant.courseConditions;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import org.json.JSONArray;
import org.json.JSONException;

import cz.vsb.elogika.R;
import cz.vsb.elogika.common.view.SlidingTabLayout;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.Actionbar;

public class Fragment_cc_sum_detail_course_tabs extends Fragment{
    static Fragment_cc_sum_detail fragment;

    private View rootView;

    private ProgressBar progressDialog;
    LinearLayout nothinglayout;

    String userId;
    String studyForm;

    int max_tabs = 100;
    String[] tabNames = new String[100];
    String[] list = new String[100];
    int tabLength = 0;

    ViewPager mViewPager;
    MyFragmentStatePagerAdapter adapter;
    SlidingTabLayout mSlidingTabLayout;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.tabs_container, container, false);

        progressDialog = (ProgressBar) rootView.findViewById(R.id.loading_progress);
        nothinglayout = (LinearLayout) rootView.findViewById(R.id.nothing_to_show);
        nothinglayout.setVisibility(View.GONE);

        userId = getArguments().getString("userId");
        studyForm = getArguments().getString("studyForm");

        Actionbar.addSubcategory(R.string.sum_detail_of_result_activity_group, this);
        //setActionBar();

        get_activity_groups();
        mViewPager = (ViewPager) rootView.findViewById(R.id.viewpager);
        adapter = new MyFragmentStatePagerAdapter(getFragmentManager());
        mViewPager.setAdapter(adapter);

        mSlidingTabLayout = (SlidingTabLayout) rootView.findViewById(R.id.sliding_tabs);
        mSlidingTabLayout.setViewPager(mViewPager);

        return rootView;
    }

    public void get_activity_groups() {
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                if (response.length() > 0)
                {
                    for (int j = 0; j < response.length(); j++)
                    {
                        if(response.getJSONObject(j).getString("ZapocitatDoVysledku").equals("true")){
                            if(response.getJSONObject(j).getString("FormaStudia").equals(studyForm)) {
                                list[tabLength] = response.getJSONObject(j).getString("IDSkupinaAktivit");
                                tabNames[tabLength++] = response.getJSONObject(j).getString("Nazev");
                            }
                        }
                    }

                    //todo presunout do tabu
                    if(tabLength == 0){
                        progressDialog.setVisibility(View.GONE);
                        nothinglayout.setVisibility(View.VISIBLE);
                    }

                    adapter.notifyDataSetChanged();
                    mSlidingTabLayout.setViewPager(mViewPager);
                }
                else{
                    Log.d("Chyba", "Fragment_cc_sum_detail get_activity_groups");
                    progressDialog.setVisibility(View.GONE);
                    nothinglayout.setVisibility(View.VISIBLE);
                }
            }
        };
        Request.get(URL.GroupActivities.getgroupactivitiesbycourse(User.courseInfoId, User.id, User.roleID), arrayResponse);
    }

    private class MyFragmentStatePagerAdapter extends FragmentStatePagerAdapter {
        MyFragmentStatePagerAdapter(FragmentManager fm){
            super(fm);
        }
        @Override
        public int getCount() {
            return tabLength;
        }
        @Override
        public CharSequence getPageTitle(int position) {
            return tabNames[position];
        }

        @Override
        public Fragment getItem(int position) {

            Bundle params = new Bundle();
            switch (position)
            {
                default :
                    params.putString("userId", userId);
                    params.putString("studyForm", studyForm);
                    params.putString("idSkupinaAktivit", list[position]);
                    params.putString("back", "course_tabs");
                    fragment = new Fragment_cc_sum_detail();
                    fragment.setArguments(params);
                    return fragment;
            }
        }
    }
}
