package cz.vsb.elogika.ui.fragments;


import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.DividerItemDecoration;
import cz.vsb.elogika.utils.Logging;
/**
 *
 * Načtou se všechny veřejné kurzy a pokud se jedná o přihlášeného
 * uživatele, tak je hned jasné, jestli je uživatel přihlášen nebo
 * ne. Pokud je tento fragment volaný přímo z loginu přes aktivitu
 * AnonymousPublicCoursesActivity, tak je při loginu přesměrován
 * na aktivitu RegisterForPublicCoursesActivity, kde je formulář
 * k registraci. Teprve po registraci je možné se přihlásit.
*/
public class Fragment_PublicCourses extends Fragment
{
    private RecyclerView.Adapter adapter;
    protected ArrayList<PublicCourse> publicCourses;

    protected class PublicCourse
    {
        public int id;
        public int courseInfoId;
        public String name;
        public String school;
        public String shortcut;
        public String semester;
        public String year;
        public int loggedCount;
        public int spaces;
        public boolean isLogged;
    }



    /**
     * Konstruktor
     */
    public Fragment_PublicCourses()
    {
        this.publicCourses = new ArrayList();
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        if (User.id == 0) Actionbar.hideMenu();
        Actionbar.newCategory(R.string.public_courses, this);

        View rootView = inflater.inflate(R.layout.recycler_view, container, false);
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        rootView.findViewById(R.id.loading_progress).setVisibility(View.VISIBLE);

        // Vytvoření adaptéru
        this.adapter = new RecyclerView.Adapter()
        {
            class ViewHolder extends RecyclerView.ViewHolder
            {
                public CheckBox checkBox;
                public ProgressBar progressBar;
                public TextView name;
                public TextView spaces;

                public ViewHolder(View itemView)
                {
                    super(itemView);
                    this.checkBox = (CheckBox) itemView.findViewById(R.id.checkbox);
                    this.progressBar = (ProgressBar) itemView.findViewById(R.id.checkbox_progress_bar);
                    this.name = (TextView) itemView.findViewById(R.id.name);
                    this.spaces = (TextView) itemView.findViewById(R.id.spaces);
                }
            }

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
            {
                View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_public_courses_item, parent, false);
                return new ViewHolder(view);
            }

            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position)
            {
                final ViewHolder holder = (ViewHolder) viewHolder;
                final PublicCourse publicCourse = publicCourses.get(position);

                // Zobrazení detailu
                holder.itemView.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        Dialog dialog = new Dialog(getActivity());
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.fragment_public_courses_detail);

                        ((TextView) dialog.findViewById(R.id.school)).setText(publicCourse.school);
                        ((TextView) dialog.findViewById(R.id.name)).setText(publicCourse.name);
                        ((TextView) dialog.findViewById(R.id.shortcut)).setText(publicCourse.shortcut);
                        ((TextView) dialog.findViewById(R.id.semester)).setText(publicCourse.semester);
                        ((TextView) dialog.findViewById(R.id.year)).setText(publicCourse.year);
                        ((TextView) dialog.findViewById(R.id.spaces)).setText(String.valueOf(publicCourse.loggedCount) + "/" + String.valueOf(publicCourse.spaces));

                        dialog.show();
                    }
                });

                // Nastavení listeneru na checkbox pro přihlášení nebo odhlášení
                holder.checkBox.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        final CheckBox checkbox = (CheckBox) view;
                        checkbox.setChecked(!checkbox.isChecked());

                        if (User.id == 0) // Uživatel není přihlášen
                        {
                            Bundle bundle = new Bundle();
                            bundle.putInt("courseInfoId", publicCourses.get(position).courseInfoId);
                            Fragment_PublicCoursesRegistration fragment = new Fragment_PublicCoursesRegistration();
                            fragment.setArguments(bundle);
                            getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment).commit();
                            return;
                        }

                        if (!checkbox.isChecked()) // přihlášení
                        {
                            new AlertDialog.Builder(getActivity()).setTitle(R.string.login_title).setMessage(R.string.login_question).setPositiveButton(R.string.yes, new DialogInterface.OnClickListener()
                            {
                                public void onClick(DialogInterface dialog, int which)
                                {
                                    holder.progressBar.setLayoutParams(new LinearLayout.LayoutParams(holder.checkBox.getWidth(), holder.checkBox.getHeight()));
                                    holder.checkBox.setVisibility(View.GONE);
                                    holder.progressBar.setVisibility(View.VISIBLE);
                                    login(position);
                                }
                            }).setNegativeButton(R.string.no, null).setIcon(android.R.drawable.ic_dialog_info).show();
                        }
                        else // odhlášení
                        {
                            new AlertDialog.Builder(getActivity()).setTitle(R.string.logout_title).setMessage(R.string.logout_question).setPositiveButton(R.string.yes, new DialogInterface.OnClickListener()
                            {
                                public void onClick(DialogInterface dialog, int which)
                                {
                                    holder.progressBar.setLayoutParams(new LinearLayout.LayoutParams(holder.checkBox.getWidth(), holder.checkBox.getHeight()));
                                    holder.checkBox.setVisibility(View.GONE);
                                    holder.progressBar.setVisibility(View.VISIBLE);
                                    logout(position);
                                }
                            }).setNegativeButton(R.string.no, null).setIcon(android.R.drawable.ic_dialog_info).show();
                        }
                    }
                });
                if (!publicCourse.isLogged && publicCourse.spaces <= publicCourse.loggedCount) holder.checkBox.setEnabled(false);
                else holder.checkBox.setEnabled(true);
                holder.checkBox.setVisibility(View.VISIBLE);
                holder.checkBox.setChecked(publicCourse.isLogged);
                holder.progressBar.setVisibility(View.GONE);
                holder.name.setText(publicCourse.name);
                holder.spaces.setText(publicCourse.loggedCount + "/" + publicCourse.spaces);
            }

            @Override
            public int getItemCount()
            {
                return publicCourses.size();
            }
        };
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);


        /*ArrayList<PublicCourse> savedPublicCourses = (ArrayList<PublicCourse>) getActivity().getIntent().getSerializableExtra("publicCourses");
        if (savedPublicCourses != null)
        {
            rootView.findViewById(R.id.loading_progress).setVisibility(View.GONE);
            this.publicCourses = savedPublicCourses;
            for (int j = 0; j < this.publicCourses.size(); j++)
            {
                addItem(j);
            }
            adapter.notifyDataSetChanged();
        }
        else */ loadPublicCourses();

        Logging.logAccess("/Pages/Anonymous/Registrace.aspx");

        return rootView;
    }



    /**
     * Načte všechny veřejné kurzy a zobrazí je v recyclerView.
     */
    private void loadPublicCourses()
    {
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                getActivity().findViewById(R.id.loading_progress).setVisibility(View.GONE);

                JSONObject o;
                PublicCourse publicCourse;

                for (int j = 0; j < response.length(); j++)
                {
                    o = response.getJSONObject(j);
                    if (o.getBoolean("Smazany")) continue;
                    publicCourse = new PublicCourse();
                    publicCourse.id = o.getInt("IDKurz");
                    publicCourse.courseInfoId = o.getInt("IDKurzInfo");
                    publicCourse.isLogged = o.getBoolean("Zapsan");
                    publicCourse.name = o.getString("Nazev");
                    publicCourse.school = o.getString("Skola");
                    publicCourse.shortcut = o.getString("Zkratka");
                    publicCourse.semester = o.getString("Semestr");
                    publicCourse.year = o.getString("Rok");
                    publicCourse.loggedCount = o.getInt("PocetPrihlasenych");
                    publicCourse.spaces = o.getInt("MaxStudentu");
                    publicCourses.add(publicCourse);
                }
                adapter.notifyDataSetChanged();
                /*ArrayList<PublicCourse> savedPublicCourses = (ArrayList<PublicCourse>) getActivity().getIntent().getSerializableExtra("publicCourses");
                if (savedPublicCourses == null)
                {
                    getActivity().getIntent().putExtra("publicCourses", publicCourses);
                }*/
                // Zobrazení prázdného layoutu
                if (publicCourses.size() == 0)
                {
                    getActivity().findViewById(R.id.nothing_to_show).setVisibility(View.VISIBLE);
                }
            }
        };
        Request.get(URL.Course.PublicCourses(User.id), arrayResponse);
    }



    /**
     * Přihlásí studenta na vybraný veřejný kurz.
     * @param position index v poli publicCurses
     */
    protected void login(final int position)
    {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                if (response.getBoolean("Success") == true)
                {
                    publicCourses.get(position).loggedCount++;
                    publicCourses.get(position).isLogged = true;
                    adapter.notifyItemChanged(position);
                }
                else
                {
                    publicCourses.get(position).loggedCount = publicCourses.get(position).spaces;
                    adapter.notifyItemChanged(position);
                    SnackbarManager.show(Snackbar.with(getActivity()).type(SnackbarType.MULTI_LINE).text(R.string.error_public_course_is_full));
                }
            }

            @Override
            public void onError(VolleyError error)
            {
                super.onError(error);
                adapter.notifyItemChanged(position);
            }

            @Override
            public void onException(JSONException e)
            {
                adapter.notifyItemChanged(position);
                // TODO Tuto chybu bude možná dobré vypisovat přímo z Response
                SnackbarManager.show(Snackbar.with(getActivity()).text(R.string.error_reports__server_error));
            }
        };
        Request.post(URL.User.RegistrationExistUser(User.id, publicCourses.get(position).courseInfoId), response);
    }



    /**
     * Odhlásí studenta z vybraného veřejného kurzu.
     * @param position index v poli publicCurses
     */
    private void logout(final int position)
    {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                if (response.getBoolean("Success") == true)
                {
                    publicCourses.get(position).loggedCount--;
                    publicCourses.get(position).isLogged = false;
                    adapter.notifyItemChanged(position);
                }
                else onException(null);
            }

            @Override
            public void onError(VolleyError error)
            {
                super.onError(error);
                adapter.notifyItemChanged(position);
            }

            @Override
            public void onException(JSONException e)
            {
                adapter.notifyItemChanged(position);
                // TODO Tuto chybu bude možná dobré vypisovat přímo z Response
                SnackbarManager.show(Snackbar.with(getActivity()).text(R.string.error_reports__server_error));
            }
        };
        Request.post(URL.User.UnregistrationExistUser(User.id, publicCourses.get(position).courseInfoId), response);
    }
}
