package cz.vsb.elogika.dataTypes;

public class Category {

    public int IDKategorie;
    public String Nazev;
    public String Popis;
    public String Rovnice;
    public int Hodnota;
    public boolean Smazana;
    public int IDKategoriePuv;

    @Override
    public String toString() {
        return Nazev;
    }
}
