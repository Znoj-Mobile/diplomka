package cz.vsb.elogika.ui.fragments;

import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cz.vsb.elogika.R;
import cz.vsb.elogika.common.view.SlidingTabLayout;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.ui.fragments.takeAtest.Fragment_OnlineTest_list;
import cz.vsb.elogika.ui.fragments.takeAtest.Taking_a_test;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.Logging;
import cz.vsb.elogika.utils.Matematika;
import uk.co.senab.photoview.PhotoViewAttacher;

/**
 *
 * Fragment pro zobrazení vypracovaných testů
 */
public class Fragment_TestDrawn extends Fragment {


    public ArrayList<Map<String, String>> itemList = new ArrayList<Map<String, String>>();;
    public SimpleAdapter adapter;
    private int readycounter = 0;


    /**
     * Inicializace UI
     */
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_test_drawn, container, false);
        Actionbar.newCategory(R.string.tests_drawn, this);
        Logging.logAccess("/Pages/Student/VypracovaneTesty.aspx");
        ListView listView = (ListView) rootView.findViewById(R.id.worked_out_tests_listView);
        String[] from = {"test", "activitiesGroup", "points", "minPoints", "maxPoints"};
        int[] to = {R.id.take_a_test_name, R.id.take_a_test_activities_group,
                R.id.take_a_test_points_earned_value, R.id.take_a_test_minpoints, R.id.take_a_test_maxpoints};

        this.adapter = new SimpleAdapter(getActivity(), this.itemList, R.layout.fragment_take_a_test_item, from, to){
            /**
             * Zvýrazní neuspešné testy červenou barvou
             */
            @Override
            public View getView( final int position, View convertView, ViewGroup parent) {
              //  Log.d("pozice" , "" + position);
                View view = super.getView(position, convertView, parent);
                TextView text = (TextView) view.findViewById(R.id.take_a_test_points_earned_value);
                view.findViewById(R.id.take_a_test_points_earned).setVisibility(View.VISIBLE);
                view.findViewById(R.id.take_a_test_points_earned_value).setVisibility(View.VISIBLE);
                view.findViewById(R.id.take_a_test_active).setVisibility(View.GONE);
                view.findViewById(R.id.take_a_test_active_name).setVisibility(View.GONE);

                try{
                    float minPoints = Float.parseFloat(((TextView) view.findViewById(R.id.take_a_test_minpoints)).getText().toString());
                    if(Float.parseFloat(text.getText().toString())  < minPoints )
                    {
                        text.setTextColor(getResources().getColor(R.color.elogika_mandatory));
                    }
                    else text.setTextColor(Color.WHITE);
                }
                catch(Exception e){}
                return view;
            }
        };

        listView.setAdapter(this.adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int position, long id)
            {
                Logging.logAccess("/Pages/Other/TestDetail/GeneratedTestHodnoceni.aspx");
                final TextView name = (TextView) view.findViewById(R.id.take_a_test_name);
                TextView activites_group = (TextView) view.findViewById(R.id.take_a_test_activities_group);
                TextView points = (TextView) view.findViewById(R.id.take_a_test_points_earned_value);
                TextView minpoints = (TextView) view.findViewById(R.id.take_a_test_minpoints);
                TextView maxpoints = (TextView) view.findViewById(R.id.take_a_test_maxpoints);

                //Toast.makeText(getActivity(), activity_finished_list.get(position).toString(), Toast.LENGTH_SHORT).show();
                final Dialog myDialog = new Dialog(getActivity());
                myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                myDialog.setContentView(R.layout.fragment_take_a_test_detail);

                LinearLayout Llayout = (LinearLayout) myDialog.findViewById(R.id.take_a_test_detail_layout);



                String data[];

                String[] labels = {getString(R.string.name), getString(R.string.activity_group), getString(R.string.points_earned), getString(R.string.minmaxpoints),
                        getString(R.string.percentage), getString(R.string.minutes)};

                data = new String[]{
                        (String) name.getText(),
                        (String) activites_group.getText(),
                        (String) points.getText(),
                        minpoints.getText() + " / " + maxpoints.getText(),
                        Matematika.zaokrouhleni(String.valueOf((Float.parseFloat(points.getText().toString()) / Float.parseFloat(maxpoints.getText().toString()))*100))   +"%",
                        //    (String) place.getText(),
                        //    (String) spaces.getText(),
                };


                TextView labelText;
                TextView dataText;
                for (int j = 0; j < data.length && j < labels.length; j++) {
                    labelText = new TextView(getActivity());
                    labelText.setText(labels[j]);
                    if (Build.VERSION.SDK_INT < 23) {
                        labelText.setTextAppearance(view.getContext(), R.style.itemHint);
                    } else {
                        labelText.setTextAppearance(R.style.itemHint);
                    }
                    Llayout.addView(labelText);
                    dataText = new TextView(getActivity());
                    dataText.setText(data[j]);
                    if (Build.VERSION.SDK_INT < 23) {
                        dataText.setTextAppearance(view.getContext(), R.style.itemValue);
                    } else {
                        dataText.setTextAppearance(R.style.itemValue);
                    }
                    dataText.setGravity(Gravity.RIGHT);// | Gravity.CENTER);
                    Llayout.addView(dataText);
                }
                final LinearLayout progressBar = (LinearLayout) inflater.inflate(R.layout.loadingwithpercentage, null);
                final TextView progressPercentage = (TextView) progressBar.findViewById(R.id.percentageprogresstext);



                Llayout.addView(progressBar);

                try {
                    if (respons.getJSONObject(position).getBoolean("ZobrazHotovTest") && respons.getJSONObject(position).getBoolean("PaperTest"))
                    {  // ZOBRAZ SCAN A MOŽNOST OPRAVY
                        final Button b = new Button(getActivity());
                        b.setText(getString(R.string.show));
                        b.setOnClickListener(new View.OnClickListener() {
                                                 @Override
                                                 public void onClick(View v) {


                                                     myDialog.findViewById(R.id.testdrawn_progress).setVisibility(View.VISIBLE);
                                                     b.setVisibility(View.GONE);
                                                     final ArrayResponse res = new ArrayResponse() {
                                                         @Override
                                                         public void onResponse(JSONArray response) throws JSONException
                                                         {

                                                             myDialog.findViewById(R.id.testdrawn_progress).setVisibility(View.GONE);
                                                             b.setVisibility(View.VISIBLE);
                                                             Log.w("obrazek", response.toString());
                                                             final Dialog dialogimages = new Dialog(getActivity());
                                                             dialogimages.requestWindowFeature(Window.FEATURE_NO_TITLE);


                                                             dialogimages.setContentView(inflater.inflate(R.layout.fragment_test_drawn_image, null));
                                                             ViewPager mViewPager = (ViewPager) dialogimages.findViewById(R.id.viewpager);
                                                             mViewPager.setAdapter(new SamplePagerAdapter(getActivity(), response));

                                                             SlidingTabLayout mSlidingTabLayout = (SlidingTabLayout) dialogimages.findViewById(R.id.sliding_tabs);
                                                             mSlidingTabLayout.setViewPager(mViewPager);

                                                             final Button badrecognitionbutton  = (Button) dialogimages.findViewById(R.id.badrecognizedbutton);
                                                             badrecognitionbutton.setOnClickListener(new View.OnClickListener() {
                                                                 @Override
                                                                 public void onClick(View v) {
                                                                     dialogimages.findViewById(R.id.badrecognizedbutton).setVisibility(View.GONE);
                                                                     Response response = new Response() {
                                                                         @Override
                                                                         public void onResponse(JSONObject response) throws JSONException {
                                                                             dialogimages.findViewById(R.id.badrecognizedtext).setVisibility(View.VISIBLE);

                                                                         }
                                                                     };
                                                                     try {
                                                                         Request.post(URL.GeneratedTest.setvygenerovanytestuzivatelcorrect(respons.getJSONObject(position).getInt("IDVygenerovanyTestUzivatel"), false), new JSONObject(), response);
                                                                     } catch (JSONException e) {
                                                                         e.printStackTrace();
                                                                     }
                                                                 }
                                                             });

                                                             WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                                             lp.copyFrom(dialogimages.getWindow().getAttributes());
                                                             lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                                                             lp.height = WindowManager.LayoutParams.MATCH_PARENT;
                                                             dialogimages.show();
                                                             dialogimages.getWindow().setAttributes(lp);


                                                         }
                                                     };
                                                     try {
                                                         Request.get(URL.GeneratedTest.papirovytestobrazek(respons.getJSONObject(position).getInt("IDVygenerovanyTestUzivatel")), res);
                                                     } catch (JSONException e) {
                                                         e.printStackTrace();
                                                     }
                                                 }
                                             }
                        );
                        Llayout.addView(b);
                    }
                    else if (respons.getJSONObject(position).getBoolean("OdeslaniVysledku") && !respons.getJSONObject(position).getBoolean("ZobrazHotovTest"))
                    {   // ZOBRAZ JEN VYSLEDEK
                    }
                    else if (respons.getJSONObject(position).getBoolean("ZobrazHotovTest"))
                    {   // ZOBRAZ VYSLEDEK I ODPOVEDI  */
                        final Button b = new Button(getActivity());
                        b.setText(getString(R.string.show));
                        b.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                myDialog.setCancelable(false);
                                progressBar.setVisibility(View.VISIBLE);
                                b.setVisibility(View.GONE);

                                ArrayResponse res = new ArrayResponse() {
                                    @Override
                                    public void onResponse(JSONArray response) throws JSONException {
                                        progressPercentage.setText("0.1 %");
                                        Taking_a_test.testDrawnResponse = response; //IMPORTANT zjistuju zde body pro otazky a podle otazek pak zjistiju ktere vyplnil
                                        Taking_a_test.testName = (String) name.getText();

                                        Log.w("Test Drawn Response", response.toString());
                                        try {

                                            if (response.length() > 0) {
                                                Taking_a_test.testDrawnQuestionAnswers = new JSONArray[response.length()];
                                                for (int j = 0; j < response.length(); j++) {
                                                    int IDVygenerovanyTestUzivatel = response.getJSONObject(j).getInt("IDVygenerovanyTestUzivatel");
                                                    int IDVygenerovanaOtazka = response.getJSONObject(j).getInt("IDVygenerovanaOtazka");
                                                    readycounter = 0;
                                                    progressPercentage.setText("0. " + (2 + readycounter) + " %");
                                                    ArrayResponse couzivatelodpovedel = new ArrayResponse() {
                                                        @Override
                                                        public void onResponse(JSONArray response) throws JSONException {
                                                            Taking_a_test.testDrawnQuestionAnswers[readycounter] = response;
                                                            readycounter++;
                                                            if (readycounter == Taking_a_test.testDrawnQuestionAnswers.length) {
                                                                new Fragment_OnlineTest_list().showTestDrawn(
                                                                        getActivity(),
                                                                        respons.getJSONObject(position).getInt("IDTest"),
                                                                        respons.getJSONObject(position).getInt("IDVygenerovanyTest"),
                                                                        myDialog,
                                                                        progressBar,
                                                                        progressPercentage
                                                                );
                                                            }
                                                        }
                                                    };
                                                    Request.get(URL.GeneratedTest.answersbygeneratedquestion(IDVygenerovanyTestUzivatel, IDVygenerovanaOtazka), couzivatelodpovedel);
                                                }
                                            } else {
                                                myDialog.cancel();
                                                SnackbarManager.show(
                                                        Snackbar.with(getActivity())
                                                                .text(Taking_a_test.testName + " " + getActivity().getString(R.string.is_empty))
                                                );
                                            }

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                };
                                try {
                                    Request.get(URL.EvaluationQuestion.GetEvaluationByGeneratedTest(respons.getJSONObject(position).getInt("IDVygenerovanyTestUzivatel")), res);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                        Llayout.addView(b);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                myDialog.show();
            }
        });


        return rootView;
    }


    /**
     * přidá záznam do tabulky
     *
     * @param test název testu
     * @param activitiesGroup název skupiny aktivit
     * @param points získané body
     * @param minPoints minimální počet bodů pro uspěch
     * @param maxPoints celkový počet bodů v testu
     *
     */
    private void addItem(String test, String activitiesGroup, String points, String minPoints, String maxPoints)
    {
        HashMap<String, String> item = new HashMap<String, String>();
        item.put("test", test);
        item.put("activitiesGroup", activitiesGroup);
        item.put("points", Matematika.zaokrouhleni(points));
        item.put("minPoints",Matematika.zaokrouhleni(minPoints));
        item.put("maxPoints", Matematika.zaokrouhleni(maxPoints));
        this.itemList.add(item);
        this.adapter.notifyDataSetChanged();
    }


    /**
     * Pošle požadavek na poslání dat ze serveru
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        getActivity().findViewById(R.id.testdrawn_progress).setVisibility(View.VISIBLE);
        getTestDrawnFromServer();
    }

    JSONArray respons;

    /**
     * Metoda pro získání dat ze serveru
     */
    public void getTestDrawnFromServer()
    {
        ArrayResponse r = new ArrayResponse() {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                //getActivity().findViewById(R.id.testdrawn_progress).setVisibility(View.GONE);
                respons = response;
                if (response.length() > 0)
                {
                    for (int j = 0; j < response.length(); j++)
                    {
                        addItem(response.getJSONObject(j).getString("TestNazev"),
                                response.getJSONObject(j).getString("SkupinaAktNazev"),
                                response.getJSONObject(j).getString("BodyTest"),
                                response.getJSONObject(j).getString("MinBodyTest"),
                                response.getJSONObject(j).getString("HodnoceniMaxBodyTest"));
                    }
                }
                else getActivity().findViewById(R.id.test_drawn_nothing_to_show).setVisibility(View.VISIBLE);

                getActivity().findViewById(R.id.testdrawn_progress).setVisibility(View.GONE);
            }

            @Override
            public void onError(VolleyError error){
                Log.d("elogika", "testy nenačteny"+ error);

            };
        };
        Request.get(URL.TestDrawn.requestInformation(User.id, User.courseInfoId), r);
    }


    /**
     * Třída pro zobrazení více oken vedle sebe pro zobrazení scanů z testů
     */
    class SamplePagerAdapter extends PagerAdapter {
        Context mContext;
        LayoutInflater mLayoutInflater;
        JSONArray imageResponse;
        public SamplePagerAdapter(Context context,JSONArray imageResponse) {
            this.imageResponse = imageResponse;
            mContext = context;
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        @Override
        public CharSequence getPageTitle(int position) {
            return String.valueOf(position+1);
        }

        @Override
        public int getCount() {
            return imageResponse.length();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((LinearLayout) object);
        }

        /**
         * Zce se inicializuje stránka se scanem
         */
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            String s = null;
            try {
                s = imageResponse.getJSONObject(position).getString("Soubor");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            byte[] decodedString = Base64.decode(s, Base64.DEFAULT);
            Bitmap bbb  = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

            ImageView  imv = new ImageView(mContext);
            imv.setImageBitmap(bbb);
            new PhotoViewAttacher(imv);

            LinearLayout ll = new LinearLayout(mContext);
            ll.addView(imv);

            container.addView(ll);
            Log.i("ell", "instantiateItem() [position: " + position + "]");
            return ll;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

    }
}


