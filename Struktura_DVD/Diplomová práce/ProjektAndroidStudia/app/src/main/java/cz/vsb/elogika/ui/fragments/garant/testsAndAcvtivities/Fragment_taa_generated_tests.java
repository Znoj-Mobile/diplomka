package cz.vsb.elogika.ui.fragments.garant.testsAndAcvtivities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.DividerItemDecoration;
import cz.vsb.elogika.utils.DownloadFile;
import cz.vsb.elogika.utils.Logging;
import cz.vsb.elogika.utils.TimeLogika;

public class Fragment_taa_generated_tests extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private View mainView;
    private View rootView;
    private RecyclerView recyclerView;

    LinearLayout nothinglayout;
    SwipeRefreshLayout swipeLayout;

    ArrayList<Map<String, String>> term_list;
    ArrayList<Map<String, String>> gt_list;
    protected RecyclerView.Adapter adapter;

    private String idTest;
    private String nameTest;

    Spinner taa_gt_spinner = null;
    private int spinner_position;

    ArrayAdapter<String> adapter_terms;
    List<String> toSpin_name;
    List<String> toSpin_id_term;
    HashMap<Integer, String> users_list;
    Map<String, String> testList;

    JSONArray avaiableTestsJSONArray;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_taa_generated_tests, container, false);
        rootView = (View) mainView.findViewById(R.id.recycler_view_swipe);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);

        swipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_update);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeResources(R.color.darker_cyan, R.color.material_blue_grey_800);

        try {
            testList = (Map<String, String>) getArguments().getSerializable("testList");
            idTest = testList.get("idTest");
        } catch (Exception e){   }
        nameTest = getArguments().getString("nameTest");

        Actionbar.addSubcategory(R.string.generated_tests, this);
        Logging.logAccess("/Pages/Other/TestDetail/GeneratedTestDetail.aspx?ID_T=" + idTest);

        nothinglayout = (LinearLayout) rootView.findViewById(R.id.nothing_to_show);
        nothinglayout.setVisibility(View.GONE);

        term_list = new ArrayList<Map<String,String>>();
        gt_list = new ArrayList<Map<String,String>>();
        toSpin_name = new ArrayList<String>();
        toSpin_id_term = new ArrayList<String>();

        users_list = new HashMap<Integer, String>();

        Button taa_gt_btn_generate_test = (Button) mainView.findViewById(R.id.taa_gt_btn_generate_test);
        taa_gt_btn_generate_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generate_test();
            }
        });
        Button taa_gt_btn_generate_more = (Button) mainView.findViewById(R.id.taa_gt_btn_generate_more);
        taa_gt_btn_generate_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("testList", (Serializable) testList);
                bundle.putString("idTest", idTest);
                bundle.putString("idTermin", term_list.get(spinner_position).get("idTermin"));
                Fragment_taa_generate_more_tests fragment_taa_generate_more_tests = new Fragment_taa_generate_more_tests();
                fragment_taa_generate_more_tests.setArguments(bundle);
                getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment_taa_generate_more_tests).commit();

            }
        });
        Button taa_gt_btn_print_to_name = (Button) mainView.findViewById(R.id.taa_gt_btn_print_to_name);
        taa_gt_btn_print_to_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        Button taa_gt_btn_print_all = (Button) mainView.findViewById(R.id.taa_gt_btn_print_all);
        taa_gt_btn_print_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });
        Button taa_gt_btn_export_results = (Button) mainView.findViewById(R.id.taa_gt_btn_export_results);
        taa_gt_btn_export_results.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getExport();
            }
        });
        Button taa_gt_btn_term_statistics = (Button) mainView.findViewById(R.id.taa_gt_btn_term_statistics);
        taa_gt_btn_term_statistics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });
        Button taa_gt_btn_switch_term = (Button) mainView.findViewById(R.id.taa_gt_btn_switch_term);
        taa_gt_btn_switch_term.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        this.adapter = new RecyclerView.Adapter() {
            class ViewHolderHeader extends RecyclerView.ViewHolder {
                public ViewHolderHeader(View itemView) {
                    super(itemView);
                }
            }

            class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
                public TextView autor;
                public TextView group;
                public Button solvers;
                public Button preview;
                public Button print;
                public Button delete;

                public ViewHolder(View itemView) {
                    super(itemView);
                    itemView.setOnClickListener(this);
                    /*this.btn = (Button) itemView.findViewById(R.id.taa_a_button);*/
                    this.autor = (TextView) itemView.findViewById(R.id.taa_gt_autor);
                    this.group = (TextView) itemView.findViewById(R.id.taa_gt_group);
                    this.solvers = (Button) itemView.findViewById(R.id.taa_gt_solvers);
                    this.preview = (Button) itemView.findViewById(R.id.taa_gt_preview);
                    this.print = (Button) itemView.findViewById(R.id.taa_gt_print);
                    this.delete = (Button) itemView.findViewById(R.id.taa_gt_delete);
                }

                @Override
                public void onClick(View view) {
                }
            }

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view;
                switch (viewType) {
                    case 0:
                        view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_taa_generated_tests_list_header, parent, false);
                        return new ViewHolderHeader(view);
                    case 1:
                        view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_taa_generated_tests_list, parent, false);
                        return new ViewHolder(view);
                    default:
                        return null;
                }
            }
            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
                if (holder.getItemViewType() == 1) {
                    final ViewHolder viewHolder = (ViewHolder) holder;

                    viewHolder.autor.setText(gt_list.get(position).get("autor"));
                    viewHolder.group.setText(gt_list.get(position).get("skupina"));

                    viewHolder.solvers.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {


                        }
                    });
                    viewHolder.preview.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {


                        }
                    });
                    viewHolder.print.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {


                        }
                    });
                    viewHolder.delete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            //builder.setTitle(R.string.save_activity);
                            builder.setCancelable(false);
                            //builder.setMessage(R.string.activity_save_message);
                            builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog2, int which) {
                                    //save(list.get(position));
                                }
                            });

                            builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog2, int which) {
                                    ;
                                }
                            }).setIcon(android.R.drawable.ic_dialog_alert);
                            Dialog d = builder.create();
                            d.show();
                        }
                    });
                }
            }

            @Override
            public int getItemViewType(int position)
            {
                /*
                //hlavicka tabulky
                if (position == 0)
                    return 0;
                    //tabulka
                else
                */
                return 1;
            }

            @Override
            public int getItemCount() {
                return gt_list.size();
            }
        };

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        recyclerView.setHasFixedSize(true);
        //zaruci, ze se bude seznam aktualizovat jen pri swipu uplne nahore...
        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy <= 0) {
                    swipeLayout.setEnabled(true);
                } else {
                    swipeLayout.setEnabled(false);
                }
            }
        });
        recyclerView.setAdapter(adapter);

        spinner_position = 0;
        taa_gt_spinner = (Spinner) mainView.findViewById(R.id.taa_gt_taa_spinner);
        taa_gt_spinner.setSelection(spinner_position);
        adapter_terms = new ArrayAdapter<String>(getActivity(),R.layout.spinner_item,toSpin_name);
        taa_gt_spinner.setAdapter(adapter_terms);

        assert taa_gt_spinner != null;
        taa_gt_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view2, int position, long id) {
                if (position != spinner_position) {
                    spinner_position = position;
                    update_gt(term_list.get(position).get("idTermin"));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                    ;
            }

        });
        update();

        TextView taa_gt_activity_group = (TextView) mainView.findViewById(R.id.taa_gt_activity_group);
        taa_gt_activity_group.setText(Fragment_TestsAndActivity.nazevSA);
        TextView taa_gt_test = (TextView) mainView.findViewById(R.id.taa_gt_test);
        taa_gt_test.setText(nameTest);

        return mainView;
    }

    public void update() {
        swipeLayout.post(new Runnable() {
            @Override
            public void run() {
                nothinglayout.setVisibility(View.GONE);
                swipeLayout.setRefreshing(true);
                term_list.clear();
                toSpin_name.clear();
                toSpin_id_term.clear();
                adapter.notifyDataSetChanged();
                get_terms();
            }
        });
    }

    public void update_gt(String dateId) {
        nothinglayout.setVisibility(View.GONE);
        swipeLayout.post(new Runnable() {
            @Override
            public void run() {
                //android bug - progress se neukaze
                swipeLayout.setEnabled(true);
                swipeLayout.setRefreshing(true);
            }
        });
        gt_list.clear();
        adapter.notifyDataSetChanged();
        get_gt(dateId);
    }

    public void get_terms() {

        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                if (response.length() > 0)
                {
                    for (int j = 0; j < response.length(); j++)
                    {
                        term_list.add(put_data(
                                response.getJSONObject(j).getString("IDTermin"),
                                response.getJSONObject(j).getString("IDSkupinaAktivit"),
                                response.getJSONObject(j).getString("IDTest"),
                                response.getJSONObject(j).getString("IDAktivita"),
                                response.getJSONObject(j).getString("Nazev"),
                                response.getJSONObject(j).getString("Ucebna"),
                                response.getJSONObject(j).getString("NazevTyp"),
                                TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("AktivniOD")),
                                TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("AktivniDO")),
                                TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("PrihlaseniDO")),
                                response.getJSONObject(j).getString("IDAutor"),
                                TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("DatumPrihlaseni")),
                                TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("DatumOdhlaseni")),
                                response.getJSONObject(j).getString("NutnePrihlaseni"),
                                response.getJSONObject(j).getString("PocetStudentu"),
                                response.getJSONObject(j).getString("Smazany"),
                                response.getJSONObject(j).getString("PocetZapsanych"),
                                response.getJSONObject(j).getString("AktivitaNazev"),
                                response.getJSONObject(j).getString("PocetPokusu"),
                                response.getJSONObject(j).getString("Zapsan")));

                        toSpin_name.add(response.getJSONObject(j).getString("Nazev"));
                        toSpin_id_term.add(response.getJSONObject(j).getString("IDTermin"));
                    }
                }
                else{
                    Log.d("Chyba", "Taa get_generatedtests");
                    nothinglayout.setVisibility(View.VISIBLE);
                }
                adapter.notifyDataSetChanged();
                adapter_terms.notifyDataSetChanged();

                update_gt(term_list.get(spinner_position).get("idTermin"));
            }
        };
        Request.get(URL.Date.GetDates(Fragment_TestsAndActivity.groupActivityId, idTest, "-1"), arrayResponse);
    }

    private HashMap<String, String> put_data(String idTermin, String idSkupinaAktivit, String idTest, String idAktivita, String nazev, String ucebna, String nazevTyp,
                                         String aktivniOD, String aktivniDO, String prihlaseniDO, String idAutor, String datumPrihlaseni, String datumOdhlaseni,
                                         String nutnePrihlaseni, String pocetStudentu, String smazany, String pocetZapsanych, String aktivitaNazev, String pocetPokusu, String zapsan) {
        HashMap<String, String> item = new HashMap<String, String>();

        item.put("idTermin",idTermin);
        item.put("idSkupinaAktivit",idSkupinaAktivit);
        item.put("idTest",idTest);
        item.put("idAktivita",idAktivita);
        item.put("nazev",nazev);
        item.put("ucebna",ucebna);
        item.put("nazevTyp",nazevTyp);
        item.put("aktivniOD",aktivniOD);
        item.put("aktivniDO",aktivniDO);
        item.put("prihlaseniDO",prihlaseniDO);
        item.put("idAutor", idAutor);
        item.put("datumPrihlaseni",datumPrihlaseni);
        item.put("datumOdhlaseni",datumOdhlaseni);
        item.put("nutnePrihlaseni",nutnePrihlaseni);
        item.put("pocetStudentu",pocetStudentu);
        item.put("smazany",smazany);
        item.put("pocetZapsanych",pocetZapsanych);
        item.put("aktivitaNazev",aktivitaNazev);
        item.put("pocetPokusu",pocetPokusu);
        item.put("zapsan", zapsan);

        return item;

    }

    private void put_it_into_term_list(String s, HashMap<String, String> item) {
        item.put("autor", s);
        gt_list.add(item);
        adapter.notifyDataSetChanged();
    }


    public void get_gt(String dateId) {

        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                if (response.length() > 0)
                {
                    for (int j = 0; j < response.length(); j++)
                    {
                        if(j == response.length()-1){
                            put_data_gt(response.getJSONObject(j).getString("IDVygenerovanyTest"),
                                    response.getJSONObject(j).getString("IDTest"),
                                    response.getJSONObject(j).getString("CasSpusteni"),
                                    response.getJSONObject(j).getString("IP"),
                                    response.getJSONObject(j).getString("IDAutor"),
                                    response.getJSONObject(j).getString("Skupina"),
                                    response.getJSONObject(j).getString("IDTermin"),
                                    TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("DatumSpusteni")),
                                    response.getJSONObject(j).getString("Shlednuto"),
                                    response.getJSONObject(j).getString("QuestionsInfo"),
                                    response.getJSONObject(j).getString("GeneratedStatistic"), true);
                        }
                        else {
                            put_data_gt(response.getJSONObject(j).getString("IDVygenerovanyTest"),
                                    response.getJSONObject(j).getString("IDTest"),
                                    response.getJSONObject(j).getString("CasSpusteni"),
                                    response.getJSONObject(j).getString("IP"),
                                    response.getJSONObject(j).getString("IDAutor"),
                                    response.getJSONObject(j).getString("Skupina"),
                                    response.getJSONObject(j).getString("IDTermin"),
                                    TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("DatumSpusteni")),
                                    response.getJSONObject(j).getString("Shlednuto"),
                                    response.getJSONObject(j).getString("QuestionsInfo"),
                                    response.getJSONObject(j).getString("GeneratedStatistic"), false);
                        }
                    }
                }
                else{
                    Log.d("Chyba", "Taa get_gt");
                    nothinglayout.setVisibility(View.VISIBLE);
                    swipeLayout.setRefreshing(false);
                }
                //loading_progress.setVisibility(View.GONE); - resi se az po nacteni jmen
                adapter.notifyDataSetChanged();
            }
        };
        Request.get(URL.GeneratedTest.getgeneratedtestbytestid(idTest, dateId), arrayResponse);
    }

    private void put_data_gt(String idVygenerovanyTest, String idTest, String casSpusteni, String ip, String idAutor, String skupina,
                                            String idTermin, String datumSpusteni, String shlednuto, String questionsInfo, String generatedStatistic, boolean last) {
        HashMap<String, String> item = new HashMap<String, String>();

        item.put("idVygenerovanyTest",idVygenerovanyTest);
        item.put("idTest",idTest);
        item.put("casSpusteni",casSpusteni);
        item.put("ip",ip);
        item.put("idAutor",idAutor);
        item.put("skupina",skupina);
        item.put("idTermin",idTermin);
        item.put("datumSpusteni",datumSpusteni);
        item.put("shlednuto",shlednuto);
        item.put("questionsInfo",questionsInfo);
        item.put("generatedStatistic",generatedStatistic);
        //todo
        getAutorName(item, last);
    }



    private void getAutorName(final HashMap<String, String> item, final boolean last) {
        final int idAutor = Integer.valueOf(item.get("idAutor"));
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                if (response.length() > 0)
                {
                    users_list.put(idAutor, response.getString("Jmeno") + " " + response.getString("Prijmeni") + " [ " + response.getString("Login") + " ] ");
                    put_it_into_term_list(response.getString("Jmeno") + " " + response.getString("Prijmeni") + " [ " + response.getString("Login") + " ] ", item);

                }
                else{
                    put_it_into_term_list("", item);
                }
                if(last){
                    swipeLayout.setRefreshing(false);
                }
            }
        };

        if(idAutor <= 0){
            put_it_into_term_list("", item);
            swipeLayout.setRefreshing(false);
        }
        else if(users_list.containsKey(idAutor)){
            put_it_into_term_list(users_list.get(idAutor), item);
            swipeLayout.setRefreshing(false);
        }
        else{
            Request.get(URL.User.Get(idAutor), response);
        }
    }

    private void getAvaiableTest(final int testID, final int cas)
    { //dostanu seznam dostupných testů
        ArrayResponse res = new ArrayResponse() {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {

                if (adapter != null) adapter.notifyDataSetChanged();
                avaiableTestsJSONArray = response;
                if (response.length() > 0) {
                    for (int j = 0; j < response.length(); j++) {
                        String remaintogenerate = "0";
                        try{
                            remaintogenerate= response.getJSONObject(j).getString("RemainToGenerate");
                        }
                        catch (JSONException e){}

                        addItem(response.getJSONObject(j).getInt("TestId"),
                                response.getJSONObject(j).getString("Name"),
                                response.getJSONObject(j).getString("GroupActivityName"),
                                response.getJSONObject(j).getString("Min"),
                                response.getJSONObject(j).getString("Max"),
                                response.getJSONObject(j).getString("DateAvailableFrom"),
                                response.getJSONObject(j).getString("DateAvailableTo"),
                                // response.getJSONObject(j).getString("Attempts"),
                                remaintogenerate,
                                response.getJSONObject(j).getBoolean("Required"),
                                response.getJSONObject(j).getInt("TimeMinutes")
                        );
                    }
                }

                else{ nothinglayout.setVisibility(View.VISIBLE);}
                if (testID !=-1)
                    generate_test();
            }
        };
        Request.get(URL.GeneratedTest.getAvaiableTests(User.id, User.courseInfoId), res);
    }

    private void addItem(int testID, String name, String activitiesGroup, String minPoints, String maxPoints,
                         String activeFrom, String activeTo, String trialsRemaining, boolean mandatory, int timeMinutes) {

        HashMap<String, String> item = new HashMap<String, String>();
        item.put("name", name);
        item.put("activitiesGroup", activitiesGroup);
        item.put("minPoints", minPoints);
        item.put("maxPoints", maxPoints);
        item.put("active", TimeLogika.getFormatedDateTime(activeFrom) + " - " + TimeLogika.getFormatedDateTime(activeTo));
        item.put("trialsRemaining", trialsRemaining);
        if (mandatory)
            item.put("mandatory", "T" + testID);
        else item.put("mandatory", "F" + testID);
        item.put("timeMinutes", String.valueOf(timeMinutes));
//        this.itemList.add(item);
        if (this.adapter != null) this.adapter.notifyDataSetChanged();
    }



    private void generate_test() {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                if (response.getString("Result").equals("true")) {
                    update();
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .type(SnackbarType.MULTI_LINE)
                                    .duration(Snackbar.SnackbarDuration.LENGTH_INDEFINITE)
                                    .text(getResources().getString(R.string.test) + getResources().getString(R.string.test_was_generated)));
                }
                else{ //nepovedlo se
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .type(SnackbarType.MULTI_LINE)
                                    .duration(Snackbar.SnackbarDuration.LENGTH_INDEFINITE)
                                    .text(getResources().getString(R.string.test) + getResources().getString(R.string.test_was_not_generated)));
                }
            }
        };
        JSONObject parameters = new JSONObject();
        try
        {
            parameters.put("IDTest", testList.get("idTest"));
            parameters.put("CasSpusteni", TimeLogika.currentServerTimeWithoutDate() );
            parameters.put("IP", URL.getLocalIpAddress());
            parameters.put("IDTermin", toSpin_id_term.get(spinner_position));
            //parameters.put("IDTermin", testList.get("idTermin"));
            parameters.put("DatumSpusteni", TimeLogika.currentServerTime());
        } catch (JSONException e) {Log.d("elogika","exception at fragment_taa_generated_tests" + e.toString());}

        Log.d("generate test request",parameters.toString());
        String hlavni = "false";
        if(testList.get("idZarazeni").equals("1")){
            hlavni = "true";
        }
        Request.post(URL.GeneratedTest.generatetestsprava(User.id, testList.get("idSablona"), hlavni), parameters, response);
    }


    private void getExport() {

        SnackbarManager.show(
                Snackbar.with(getActivity())
                        .type(SnackbarType.MULTI_LINE)
                        .duration(Snackbar.SnackbarDuration.LENGTH_INDEFINITE)
                        .text(getResources().getString(R.string.file) + " 'ExportCelkovy.txt' " + getResources().getString(R.string.file_is_downloaded)));

        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                Log.d("generate test response", response.toString());
                Intent intent = DownloadFile.downloadFromText("ExportCelkovy.txt", response.toString());
                getActivity().startActivity(intent);
                SnackbarManager.show(
                        Snackbar.with(getActivity())
                                .type(SnackbarType.MULTI_LINE)
                                .duration(Snackbar.SnackbarDuration.LENGTH_INDEFINITE)
                                .text(getResources().getString(R.string.file) + " 'ExportCelkovy.txt' " + getResources().getString(R.string.is_downloaded_in_download_folder)));

            }

            @Override
            public void onError(VolleyError error) {
                //Toast.makeText(getActivity(), getResources().getString(R.string.export_results_failed), Toast.LENGTH_LONG).show();
                SnackbarManager.show(
                        Snackbar.with(getActivity())
                                .type(SnackbarType.MULTI_LINE)
                                .text(getResources().getString(R.string.export_results_failed) + " 'ExportCelkovy.txt' "));
            }
        };
        JSONObject parameters = new JSONObject();

        Log.d("Fragment_taa_generated_tests getExport()",parameters.toString());
        Request.post(URL.ExportResult.ExportResult(idTest, term_list.get(spinner_position).get("idTermin"),User.schoolInfoID), parameters, response);
    }

    @Override
    public void onRefresh() {
        update();
    }
}
