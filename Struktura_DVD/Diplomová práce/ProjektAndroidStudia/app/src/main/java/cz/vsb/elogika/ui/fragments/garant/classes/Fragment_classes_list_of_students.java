package cz.vsb.elogika.ui.fragments.garant.classes;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cz.vsb.elogika.R;
import cz.vsb.elogika.common.view.SlidingTabLayout;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.ui.fragments.garant.emails.Fragment_email_message;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.Logging;

public class Fragment_classes_list_of_students extends Fragment {
    private View mainView;
    private View rootView;

    public static LinearLayout nothinglayout;
    public static ArrayList<Map<String, String>> list_of_students;

    protected RecyclerView.Adapter adapter;

    static int classId = 0;
    String className;
    static boolean email = false;

    public static Fragment_classes_list_of_students_list fragment_classes_list_of_students_list;
    public static ArrayList<Map<String, String>> list_of_found_students;
    public static Fragment_email_message send_message;


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mainView = inflater.inflate(R.layout.fragment_classes_list_of_students, container, false);
        rootView = mainView.findViewById(R.id.tabs_container);

        className = getArguments().getString("nazev");
        classId = Integer.valueOf(getArguments().getString("classId"));
        email = getArguments().getBoolean("email", false);

        if(email){
            Actionbar.addSubcategory(R.string.send_email_to_chosen_students, this);
            Logging.logAccess("/Pages/Other/EmailMessages/SendToStudent.aspx");
        }
        else{
            Actionbar.addSubcategory(R.string.list_of_students, this);
            Logging.logAccess("/Pages/Other/Student.aspx?id=" + classId);
        }

        nothinglayout = (LinearLayout) mainView.findViewById(R.id.nothing_to_show);
        nothinglayout.setVisibility(View.GONE);

        list_of_students = new ArrayList<>();
        list_of_found_students = new ArrayList<>();

        ViewPager mViewPager = (ViewPager) rootView.findViewById(R.id.viewpager);
        mViewPager.setAdapter(new FragmentStatePagerAdapter(getFragmentManager()) {
                                  String[] tabNames = new String[]{
                                          getString(R.string.list_of_students),
                                          getString(R.string.send_message)};

                                  Bundle bundle = new Bundle();

                                  @Override
                                  public int getCount() {
                                      return tabNames.length;
                                  }

                                  @Override
                                  public CharSequence getPageTitle(int position) {
                                      return tabNames[position];
                                  }

                                  @Override
                                  public Fragment getItem(int position) {

                                      switch (position) {
                                          case 0:
                                              bundle.putInt("classId", classId);
                                              bundle.putBoolean("email", email);
                                              fragment_classes_list_of_students_list = new Fragment_classes_list_of_students_list();
                                              fragment_classes_list_of_students_list.setArguments(bundle);
                                              return fragment_classes_list_of_students_list;
                                          case 1:
                                              bundle.putBoolean("listing", true);
                                              send_message = new Fragment_email_message();
                                              send_message.setArguments(bundle);
                                              send_message.set_listing();
                                              return send_message;
                                          default:
                                              return null;
                                      }
                                  }
                              }


        );
        SlidingTabLayout mSlidingTabLayout = (SlidingTabLayout) rootView.findViewById(R.id.sliding_tabs);
        mSlidingTabLayout.setViewPager(mViewPager);


        TextView tv_className = (TextView) mainView.findViewById(R.id.c_l_s_class);
        tv_className.setText(this.className);

        return mainView;
    }

    public static void update(final RecyclerView.Adapter adapter, final SwipeRefreshLayout swipeLayout) {
        swipeLayout.post(new Runnable() {
            @Override
            public void run() {
                nothinglayout.setVisibility(View.GONE);
                swipeLayout.setRefreshing(true);
                list_of_students.clear();
                adapter.notifyDataSetChanged();
                data_into_list_of_students(adapter, swipeLayout);
            }
        });
    }

    private static void data_into_list_of_students(final RecyclerView.Adapter adapter, final SwipeRefreshLayout swipeLayout) {
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                if (response.length() > 0)
                {
                    for (int j = 0; j < response.length(); j++)
                    {
                        list_of_students.add(put_data(
                                response.getJSONObject(j).getString("IdUzivatel"),
                                response.getJSONObject(j).getString("Login"),
                                response.getJSONObject(j).getString("Jmeno"),
                                response.getJSONObject(j).getString("Prijmeni"),
                                response.getJSONObject(j).getString("PrijmeniRodne"),
                                response.getJSONObject(j).getString("Heslo"),
                                response.getJSONObject(j).getString("Email"),
                                response.getJSONObject(j).getString("Online"),
                                response.getJSONObject(j).getString("Fotka"),
                                response.getJSONObject(j).getString("Jazyk"),
                                response.getJSONObject(j).getString("Ulice"),
                                response.getJSONObject(j).getString("PSC"),
                                response.getJSONObject(j).getString("CisloPop"),
                                response.getJSONObject(j).getString("Mesto"),
                                response.getJSONObject(j).getString("TitulPred"),
                                response.getJSONObject(j).getString("TitulZa"),
                                response.getJSONObject(j).getString("Salt"),
                                response.getJSONObject(j).getString("Smazany"),
                                response.getJSONObject(j).getString("PocetKaret"),
                                response.getJSONObject(j).getString("JmenoCele"),
                                response.getJSONObject(j).getString("Adresa"),
                                response.getJSONObject(j).getString("HesloResetovano")));
                    }
                }
                else{
                    Log.d("Chyba", "classes_list_of_students data_into_list_of_students");
                    nothinglayout.setVisibility(View.VISIBLE);
                }
                swipeLayout.setRefreshing(false);
                adapter.notifyDataSetChanged();
            }
        };
        Request.get(URL.User.GetStudentOfTrida(classId), arrayResponse);
    }

    private static Map<String, String> put_data(String idUzivatel, String login, String jmeno, String prijmeni,
                                                String prijmeniRodne, String heslo, String emailParam, String online,
                                                String fotka, String jazyk, String ulice, String psc, String cisloPop,
                                                String mesto, String titulPred, String titulZa, String salt, String smazany,
                                                String pocetKaret, String jmenoCele, String adresa, String hesloResetovano) {
        HashMap<String, String> item = new HashMap<String, String>();

        item.put("idUzivatel",idUzivatel);
        item.put("login",login);
        item.put("jmeno",jmeno);
        item.put("prijmeni",prijmeni);
        item.put("prijmeniRodne",prijmeniRodne);
        item.put("heslo",heslo);
        item.put("email",emailParam);
        item.put("online",online);
        item.put("fotka",fotka);
        item.put("jazyk",jazyk);
        item.put("ulice",ulice);
        item.put("psc",psc);
        item.put("cisloPop",cisloPop);
        item.put("mesto",mesto);
        item.put("titulPred",titulPred);
        item.put("titulZa", titulZa);
        item.put("salt",salt);
        item.put("smazany",smazany);
        item.put("pocetKaret",pocetKaret);
        item.put("jmenoCele",jmenoCele);
        item.put("adresa",adresa);
        item.put("hesloResetovano",hesloResetovano);
        //pridano pro potreby aplikace
        if(!email){
            item.put("checked", "true");
        }
        else{
            item.put("checked", "false");
        }

        return item;
    }

}
