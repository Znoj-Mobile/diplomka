package cz.vsb.elogika.ui.fragments.garant.classes;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;

import cz.vsb.elogika.R;

public class Fragment_classes_timetable extends Fragment {
    private View rootView;
    ArrayList<Map<String, String>> monday_list = null;
    ArrayList<Map<String, String>> tuesday_list = null;
    ArrayList<Map<String, String>> wednesday_list = null;
    ArrayList<Map<String, String>> thursday_list = null;
    ArrayList<Map<String, String>> friday_list = null;
    ArrayList<Map<String, String>> saturday_list = null;

    TableLayout tl;
    LayoutInflater inflater;
    ViewGroup container;

    int actual_row;

    ArrayList<LinearLayout> inflater_subjects_list;
    ArrayList<TableRow> inflater_rows_list;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        this.inflater = inflater;
        this.container = container;

        rootView = inflater.inflate(R.layout.fragment_classes_table, container, false);
        tl = (TableLayout) rootView.findViewById(R.id.table_layout);

        monday_list = new ArrayList<>();
        tuesday_list = new ArrayList<>();
        wednesday_list = new ArrayList<>();
        thursday_list = new ArrayList<>();
        friday_list = new ArrayList<>();
        saturday_list = new ArrayList<>();

        inflater_subjects_list = new ArrayList<>();
        inflater_rows_list = new ArrayList<>();

        return rootView;
    }

    public void setTimetable() {

        for(TableRow rl : inflater_rows_list){
            ((TableRow) rl.getParent()).removeView(rl);
        }

        for(LinearLayout rl : inflater_subjects_list){
            ((LinearLayout) rl.getParent()).removeView(rl);
        }

        inflater_subjects_list.clear();
        inflater_rows_list.clear();

        //rozdeleni po dnech
        monday_list.clear();
        tuesday_list.clear();
        wednesday_list.clear();
        thursday_list.clear();
        friday_list.clear();
        saturday_list.clear();
        for(int i = 0; i < Fragment_Classes.lesson_list.size(); i++){
            //vynecham zaznamy ktery byly pridany jen jako indikatory hlavicky vlozenych tabulek
            //vynecham "zastreseni" kombinovanych studentu
            //pokud jsou chybny casy, tak predmety taky ignoruju
            if(Fragment_Classes.lesson_list.get(i) != null &&
                    Fragment_Classes.lesson_list.get(i).get("combinedClasses").equals("false") &&
                    !Fragment_Classes.lesson_list.get(i).get("poradiCelkem").equals("-1") ) {

                if (Fragment_Classes.lesson_list.get(i).get("den").compareTo("Pondělí") == 0) {
                    monday_list.add(Fragment_Classes.lesson_list.get(i));
                } else if (Fragment_Classes.lesson_list.get(i).get("den").compareTo("Úterý") == 0) {
                    tuesday_list.add(Fragment_Classes.lesson_list.get(i));
                } else if (Fragment_Classes.lesson_list.get(i).get("den").compareTo("Středa") == 0) {
                    wednesday_list.add(Fragment_Classes.lesson_list.get(i));
                } else if (Fragment_Classes.lesson_list.get(i).get("den").compareTo("Čtvrtek") == 0) {
                    thursday_list.add(Fragment_Classes.lesson_list.get(i));
                } else if (Fragment_Classes.lesson_list.get(i).get("den").compareTo("Pátek") == 0) {
                    friday_list.add(Fragment_Classes.lesson_list.get(i));
                } else if (Fragment_Classes.lesson_list.get(i).get("den").compareTo("Sobota") == 0) {
                    saturday_list.add(Fragment_Classes.lesson_list.get(i));
                }
            }
        }

        //serazeni podle casu v rozvrhu
        Comparator comparator = new Comparator<Map<String, String>>() {
            @Override
            public int compare(Map<String, String> lhs, Map<String, String> rhs) {
                return lhs.get("poradiOD").compareTo(rhs.get("poradiOD"));
            }
        };
        Collections.sort(monday_list, comparator);
        Collections.sort(tuesday_list, comparator);
        Collections.sort(wednesday_list, comparator);
        Collections.sort(thursday_list, comparator);
        Collections.sort(friday_list, comparator);
        Collections.sort(saturday_list, comparator);




        ArrayList<ArrayList<Map<String, String>>> done_list = new ArrayList<ArrayList<Map<String, String>>>();
        ArrayList<LinearLayout> rows = new ArrayList<>();
        actual_row = 1;

        LinearLayout llmonday = (LinearLayout) rootView.findViewById(R.id.ll_monday);
        rows.add(llmonday);
        actual_row++;
        process_day(monday_list, done_list, rows);
        done_list.clear();
        rows.clear();

        LinearLayout lltuesday = (LinearLayout) rootView.findViewById(R.id.ll_tuesday);
        rows.add(lltuesday);
        actual_row++;
        process_day(tuesday_list, done_list, rows);
        done_list.clear();
        rows.clear();

        LinearLayout llwednesday = (LinearLayout) rootView.findViewById(R.id.ll_wednesday);
        rows.add(llwednesday);
        actual_row++;
        process_day(wednesday_list, done_list, rows);
        done_list.clear();
        rows.clear();


        LinearLayout llthursday = (LinearLayout) rootView.findViewById(R.id.ll_thursday);
        rows.add(llthursday);
        actual_row++;
        process_day(thursday_list, done_list, rows);
        done_list.clear();
        rows.clear();


        LinearLayout llfriday = (LinearLayout) rootView.findViewById(R.id.ll_friday);
        rows.add(llfriday);
        actual_row++;
        process_day(friday_list, done_list, rows);
        done_list.clear();
        rows.clear();


        LinearLayout llsaturday = (LinearLayout) rootView.findViewById(R.id.ll_saturday);
        rows.add(llsaturday);
        actual_row++;
        process_day(saturday_list, done_list, rows);
        done_list.clear();
        rows.clear();

    }

    private void process_day(ArrayList<Map<String, String>> day_list, ArrayList<ArrayList<Map<String, String>>> done_list, ArrayList<LinearLayout> rows) {
        int weight = 0;
        int poradi_min = 0;
        int poradi_max = 0;

        for (int i = 0; i < day_list.size(); i++) {
            weight = Integer.valueOf(day_list.get(i).get("poradiCelkem"));
            if(i == 0) {
                poradi_min = Integer.valueOf(day_list.get(i).get("poradiOD"));
                //pokud je potreba pocatecni mezera, tak se vytvori...
                if(poradi_min != 0){
                    empty_layout(rows.get(0), poradi_min);
                }
                print_subject(rows.get(0), weight, day_list.get(i));
                ArrayList<Map<String, String>> help_list = new ArrayList<>();
                done_list.add(help_list);
                done_list.get(0).add(day_list.get(i));
            }

            else {
                for (int j = 0; j < done_list.size(); j++) {
                    int fits = is_fitting(day_list.get(i), done_list.get(j));
                    if (fits >= 0) {

                        //jen se dopise datum
                        if(fits > 0){
                            print_date_to_subject(day_list.get(i).get("datum"), done_list.get(j).get(fits-1));
                        }
                        else {
                            //vytvoreni pripadne mezery
                            if (i != 0) {
                                poradi_min = Integer.valueOf(day_list.get(i).get("poradiOD")) - Integer.valueOf(done_list.get(j).get(done_list.get(j).size() - 1).get("poradiDO")) - 1;
                                if (poradi_min != 0) {
                                    empty_layout(rows.get(j), poradi_min);
                                }
                            }
                            print_subject(rows.get(j), weight, day_list.get(i));
                            done_list.get(j).add(day_list.get(i));
                        }
                        break;
                    }
                    if (j == done_list.size() - 1) {
                        //je potreba vytvorit novej radek
                        TableRow new_row = (TableRow) inflater.inflate(R.layout.leasson_one_row, null, false);
                        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, 1);
                        tl.addView(new_row, actual_row++, param);
                        inflater_rows_list.add(new_row);

                        LinearLayout llday = (LinearLayout) new_row.findViewById(R.id.ll_day);
                        rows.add(llday);

                        poradi_min = Integer.valueOf(day_list.get(i).get("poradiOD"));
                        //pokud je potreba pocatecni mezera, tak se vytvori...
                        if(poradi_min != 0){
                            empty_layout(rows.get(j + 1), poradi_min);
                        }
                        print_subject(rows.get(j + 1), weight, day_list.get(i));

                        //vytvoreni noveho pole pro novy vytvoreny radek...
                        ArrayList<Map<String, String>> help_list = new ArrayList<>();
                        done_list.add(help_list);
                        done_list.get(j + 1).add(day_list.get(i));
                        break;
                    }
                }
            }


            if(i == day_list.size() - 1){
                //vezmu posledni zaznam z kazdeho radku pro dany den a radek "uzavru"
                for(int j = 0; j < done_list.size(); j++){
                    poradi_max = Integer.valueOf(done_list.get(j).get(done_list.get(j).size() - 1).get("poradiDO"));
                    weight = 13 - poradi_max;
                    empty_layout(rows.get(j), weight);
                }
            }
        }
    }

    //POZOR vrací index + 1 nebo 0 pokud se predmety nekryji, -1 pokud se kryji
    private int is_fitting(Map<String, String> list, ArrayList<Map<String, String>> done) {
        int poradiOD = Integer.valueOf(list.get("poradiOD"));
        for(int i = 0; i < done.size(); i++){
            //kombinovane studium - pouze jinej cas...
            if(poradiOD == Integer.valueOf(done.get(i).get("poradiOD"))
                    && list.get("poradiDO").equals(done.get(i).get("poradiDO"))
                    && list.get("idTutor").equals(done.get(i).get("idTutor"))
                    && list.get("zkratkaPredmet").equals(done.get(i).get("zkratkaPredmet"))
                    && list.get("ucebna").equals(done.get(i).get("ucebna"))) {
                return i+1;
            }
            if(poradiOD <= Integer.valueOf(done.get(i).get("poradiDO"))){
                return -1;
            }
        }
        return 0;
    }

    private void print_subject(LinearLayout llday, int weight, Map<String, String> list) {
        LinearLayout leasson_one_class = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.leasson_one_class, null);
        //pro pozdejsi smazani
        inflater_subjects_list.add(leasson_one_class);

        TextView title = (TextView) leasson_one_class.findViewById(R.id.one_class_title);
        title.setText(list.get("zkratkaPredmet"));
        TextView room = (TextView) leasson_one_class.findViewById(R.id.one_class_room);
        room.setText(list.get("ucebna"));
        //todo
        TextView tutor = (TextView) leasson_one_class.findViewById(R.id.one_class_tutor);
        tutor.setText(list.get("tutorName") + ' ' + list.get("tutorSurname"));
        //tutor.setText(list.get("idTutor"));
        TextView type = (TextView) leasson_one_class.findViewById(R.id.one_class_type);
        TextView week = (TextView) leasson_one_class.findViewById(R.id.one_class_week);
        week.setText(list.get("tyden"));
        TextView time = (TextView) leasson_one_class.findViewById(R.id.one_class_time);
        //mario cas nema - spatne se zobrazuje pro velmi kratke hodiny (45 minut)
        if(list.get("poradiCelkem").equals("1")){
            time.setText("");
        }
        else{
            time.setText(list.get("hodinaOD") + " - " + list.get("hodinaDO"));
        }


        if(!list.get("datum").equals("null")){
            LinearLayout ll_date = (LinearLayout) leasson_one_class.findViewById(R.id.one_class_date_ll);
            ll_date.setVisibility(View.VISIBLE);
            TextView date_tv = (TextView) leasson_one_class.findViewById(R.id.one_class_date_tv);
            date_tv.setText(list.get("datum"));
        }

        if(list.get("typ").equals("0"))
            type.setText(getResources().getString(R.string.lecture_shortcut));
            //prebarvit na barvu cviceni
        else {
            type.setText(getResources().getString(R.string.exercise_shortcut));

            LinearLayout ll_top = (LinearLayout) leasson_one_class.findViewById(R.id.one_class_top_layout);
            ll_top.setBackgroundColor(Color.RED);

            LinearLayout ll_middle = (LinearLayout) leasson_one_class.findViewById(R.id.one_class_middle_layout);
            ll_middle.setBackgroundColor(Color.RED);

            LinearLayout ll_bottom = (LinearLayout) leasson_one_class.findViewById(R.id.one_class_bottom_layout);
            ll_bottom.setBackgroundColor(Color.RED);

            LinearLayout ll_date = (LinearLayout) leasson_one_class.findViewById(R.id.one_class_date_ll);
            ll_date.setBackgroundColor(Color.RED);
        }

        LinearLayout.LayoutParams param;
        param = new LinearLayout.LayoutParams(0,LinearLayout.LayoutParams.MATCH_PARENT, weight);
        llday.addView(leasson_one_class, param);
    }


    private void print_date_to_subject(String datum, Map<String, String> stringStringMap) {
        //urcite to musel byt ten posledni, protoze mam vsechno serazeno
        LinearLayout leasson_one_class = inflater_subjects_list.get(inflater_subjects_list.size() - 1);

        TextView date_tv = (TextView) leasson_one_class.findViewById(R.id.one_class_date_tv);
        date_tv.setText(date_tv.getText() + ", " + datum);
    }

    private void empty_layout(LinearLayout llday, int weight) {
        LinearLayout.LayoutParams param;
        param = new LinearLayout.LayoutParams(0,LinearLayout.LayoutParams.MATCH_PARENT, weight);
        LinearLayout ll_one_class = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.leasson_one_empty_class, null);
        llday.addView(ll_one_class, param);
        //pro pozdejsi smazani
        inflater_subjects_list.add(ll_one_class);
    }

}
