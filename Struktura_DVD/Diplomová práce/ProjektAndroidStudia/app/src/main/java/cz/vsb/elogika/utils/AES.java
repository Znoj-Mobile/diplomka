package cz.vsb.elogika.utils;



import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
/**
 * Statická třída pro symetrické šifrování 128bitovým klíčem
 *
 */ //from http://www.java2s.com/Code/Android/Security/AESEncryption.htm

public class  AES{
    private SecretKeySpec skeySpec;
    private Cipher cipher;
    private static final byte[] keyraw = { (byte) 0xA4, (byte) 0x0B, (byte) 0xC8,
            (byte) 0x34, (byte) 0xD6, (byte) 0x95, (byte) 0xF3, (byte) 0x13,
            (byte) 0xA4, (byte) 0x0B, (byte) 0xC8,
            (byte) 0x34, (byte) 0xD6, (byte) 0x95, (byte) 0xF3, (byte) 0x13};


    /**
     * Metoda pro zašifrování textu pomocí 128bitové šifry
     * @param input text pro zašifrování
     * @return zašifrovaný text
     */
        public static String zasifrovat(String input)
        {
            AES encrypter = null;
            byte[] encrypted = null;
            try {
                encrypter = new AES(new byte[16]);
            encrypted = encrypter.encrypt(input.getBytes("UTF-8"));

            } catch (Exception e) {
                e.printStackTrace();
            }

           return Base64.encodeToString(encrypted, Base64.DEFAULT);
        }

    /**
     * Metoda pro odšifrování
     * @param encrypted zašifrovaný text
     * @return původní (odšifrovaný) text
     */
        public static String odsifrovat(String encrypted)
        {
            AES encrypter = null;
            byte[] decrypted = null;
            try {
                encrypter = new AES(new byte[16]);

                decrypted = encrypter.decrypt(Base64.decode(encrypted,Base64.DEFAULT));

            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                return new String (decrypted, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return "Database Failure";
            }
        }

    /**
     * kontruktor pro šifrovíní s bitovým klíčem
     * @param keyraw klíč v bitové formě
     * @throws Exception InvalidKeyException
     */
        private AES(byte [] keyraw) throws Exception{
            if(keyraw == null){
                byte[] bytesOfMessage = "".getBytes("UTF-8");
                MessageDigest md = MessageDigest.getInstance("MD5");
                byte[] bytes = md.digest(bytesOfMessage);

                skeySpec = new SecretKeySpec(bytes, "AES");
                cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            }
            else{

                skeySpec = new SecretKeySpec(keyraw, "AES");
                cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");

            }
        }

        /**
         * konstrukor pro šiforvání pomocí textového klíče
         *
         * @param passphrase heslo v textové podobě
         * @throws Exception InvalidKeyException
         */
        private AES(String passphrase) throws Exception{
            byte[] bytesOfMessage = passphrase.getBytes("UTF-8");
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] thedigest = md.digest(bytesOfMessage);
            skeySpec = new SecretKeySpec(thedigest, "AES");


            cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        }

        /**
         * Konstruktor pro Vepsaný klíč do aplikace
         * @throws Exception InvalidKeyException
         */
        private AES() throws Exception{
            byte[] bytesOfMessage = "".getBytes("UTF-8");
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] thedigest = md.digest(bytesOfMessage);
            skeySpec = new SecretKeySpec(thedigest, "AES");

            skeySpec = new SecretKeySpec(new byte[16], "AES");
            cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        }

        private byte[] encrypt (byte[] plaintext) throws Exception{
            //returns byte array encrypted with key

            cipher.init(Cipher.ENCRYPT_MODE, skeySpec);

            byte[] ciphertext =  cipher.doFinal(plaintext);

            return ciphertext;
        }

        private byte[] decrypt (byte[] ciphertext) throws Exception{
            //returns byte array decrypted with key
            cipher.init(Cipher.DECRYPT_MODE, skeySpec);

            byte[] plaintext = cipher.doFinal(ciphertext);

            return plaintext;
        }
}