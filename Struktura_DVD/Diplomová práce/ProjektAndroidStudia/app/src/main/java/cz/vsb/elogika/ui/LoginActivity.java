package cz.vsb.elogika.ui;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;

import cz.vsb.elogika.R;
import cz.vsb.elogika.app.AppController;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.Hash;
import cz.vsb.elogika.utils.Logging;

public class LoginActivity extends Activity
{
    private ArrayList<Integer> schoolIDs;
    private ArrayList<Integer> roleIDs;
    private ArrayList<String> schools;
    private ArrayList<String> roles;
    private ProgressDialog progressDialog;

    private String usernameSalt = "gb2o?8zailxy";
    private String passwordSalt = "55u!zz!opq0?";


    /**
     * Konstruktor
     */
    public LoginActivity()
    {
        this.schoolIDs = new ArrayList();
        this.roleIDs = new ArrayList();
        this.schools = new ArrayList();
        this.roles = new ArrayList();

        User.id = 0;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);
        AppController.setActivity(this);
        Logging.logAccess("/LoginPage.aspx");

        progressDialog = new ProgressDialog(new ContextThemeWrapper(this, android.R.style.Theme_Holo_Light_Dialog));
        progressDialog.setIndeterminate(true);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener()
        {
            @Override
            public void onCancel(DialogInterface dialogInterface)
            {
                Request.cancelAllRequests();
            }
        });
    }



    /**
     * Ověří uživatele při přihlášení offline.
     * Provede se kontrola údajů s tím, co je uloženo v databázi.
     * @param username uživatelské jméno, které uživatel zadal
     * @param password heslo, které uživatel zadal
     * @return
     */
    private boolean authorizeOffline(String username, String password)
    {
        SharedPreferences datastore = getSharedPreferences("ELogika", Context.MODE_PRIVATE);
        String emptyString = new String();
        String[] usernames = datastore.getString("usernames", emptyString).split(",");
        String[] passwords = datastore.getString("passwords", emptyString).split(",");
        if (usernames[0].isEmpty() && passwords[0].isEmpty()) return false;

        String[] ids = datastore.getString("ids", emptyString).split(",");
        String[] names = datastore.getString("names", emptyString).split(",");
        String[] surnames = datastore.getString("surnames", emptyString).split(",");

        for (int j = 0; j < usernames.length; j++)
        {
            if (username.compareTo(usernames[j]) == 0 && password.compareTo(passwords[j]) == 0)
            {
                User.id = Integer.valueOf(ids[j]);
                User.name = names[j];
                User.surname = surnames[j];
                User.isOnline = false;
                return true;
            }
        }

        return false;
    }



    /**
     * Pokud uživatel ještě není v systému, tak se přidá.
     * Tato metoda se volá jen v případě, že se jedná o přihlášení online.
     * Proběhne prohledání databáze a pokud není nalezen záznam odpovídající
     * tomuto uživatelskému jménu, tak je vytvořen nový záznam.
     * @param username uživatelské jméno, které uživatel zadal
     * @param password heslo, které uživatel zadal
     * @param id číslo, které má uživatel v eLogice
     * @param name jméno uživatele
     * @param surname příjmení uživatele
     */
    private void addUser(String username, String password, int id, String name, String surname)
    {
        SharedPreferences datastore = getSharedPreferences("ELogika", Context.MODE_PRIVATE);
        String emptyString = new String();
        String[] usernames = datastore.getString("usernames", emptyString).split(",");
        emptyString = new String();
        String[] passwords = datastore.getString("passwords", emptyString).split(",");
        for (int j = 0; j < usernames.length; j++)
        {
            if (usernames[j].compareTo(username) == 0)
            {
                if (passwords[j].compareTo(password) != 0) passwords[j] = password;
                return;
            }
        }
        String[] ids;
        String[] names;
        String[] surnames;
        if (usernames[0].isEmpty() && passwords[0].isEmpty())
        {
            usernames = new String[0];
            passwords = new String[0];
            ids = new String[0];
            names = new String[0];
            surnames = new String[0];
        }
        else
        {
            emptyString = new String();
            ids = datastore.getString("ids", emptyString).split(",");
            emptyString = new String();
            names = datastore.getString("names", emptyString).split(",");
            emptyString = new String();
            surnames = datastore.getString("surnames", emptyString).split(",");
        }

        SharedPreferences.Editor editor = datastore.edit();
        editor.clear();

        StringBuilder stringBuilder = new StringBuilder();
        for (String u: usernames)
        {
            stringBuilder.append(u).append(",");
        }
        stringBuilder.append(username);
        editor.putString("usernames", stringBuilder.toString());

        stringBuilder = new StringBuilder();
        for (String p: passwords)
        {
            stringBuilder.append(p).append(",");
        }
        stringBuilder.append(password);
        editor.putString("passwords", stringBuilder.toString());

        stringBuilder = new StringBuilder();
        for (String i: ids)
        {
            stringBuilder.append(i).append(",");
        }
        stringBuilder.append(String.valueOf(id));
        editor.putString("ids", stringBuilder.toString());

        stringBuilder = new StringBuilder();
        for (String n: names)
        {
            stringBuilder.append(n).append(",");
        }
        stringBuilder.append(name);
        editor.putString("names", stringBuilder.toString());

        stringBuilder = new StringBuilder();
        for (String s: surnames)
        {
            stringBuilder.append(s).append(",");
        }
        stringBuilder.append(surname);
        editor.putString("surnames", stringBuilder.toString());

        editor.commit();
    }


    /**
     * Přihlásí uživatele do systému, ať už online, tak offline.
     * Nejdříve zjištění solí, pokud jich je víc, tak se postupně zkusí
     * ověřit uživatelské jméno a heslo se solí.
     * Metoda zavoláná přímo z layoutu login_layout.
     * @param view
     */
    public void login(View view)
    {
        final TextView username = (TextView) findViewById(R.id.uzivatelske_jmeno);
        final TextView password = (TextView) findViewById(R.id.uzivatelske_heslo);
        final CheckBox checkboxLoginOffline = (CheckBox) findViewById(R.id.checkboxLoginOffline);

        // Přihlásit se offline
        if (checkboxLoginOffline.isChecked())
        {
            if (authorizeOffline(Hash.sha1(this.usernameSalt + username.getText().toString()), Hash.sha1(this.passwordSalt + password.getText().toString() + username.getText().toString())))
            {
                Intent intent = new Intent(LoginActivity.this, OfflineTestsActivity.class);
                startActivity(intent);
                finish();
            }
            else
            {
                SnackbarManager.show(Snackbar.with(this).text(R.string.name_and_password_not_found));
            }
            return;
        }

        progressDialog.setMessage(getString(R.string.elogikaLoginDialog));
        progressDialog.show();

        ArrayResponse saltResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                if (response.length() == 0)
                {
                    progressDialog.dismiss();
                    SnackbarManager.show(Snackbar.with(LoginActivity.this).text(R.string.login_doesnt_exist));
                    return;
                }

                ArrayList<String> s = new ArrayList();
                for (int j = 0; j < response.length(); j++) s.add(response.getString(j));
                Enumeration<String> salts = Collections.enumeration(s);
                tryLogin(username.getText().toString(), password.getText().toString(), salts);
            }


            @Override
            public void onError(VolleyError error)
            {
                progressDialog.dismiss();
                super.onError(error);
            }

            @Override
            public void onException(JSONException exception)
            {
                progressDialog.dismiss();
                super.onException(exception);
            }
        };
        Request.get(URL.User.GetSaltByLogin(username.getText().toString()), saltResponse);
    }


    /**
     * Postupně pro celou množinu solí se zkusí přihlášení.
     * @param username uživatelské jméno, které uživatel zadal
     * @param password heslo, které uživatel zadal
     * @param salts pole solí
     */
    private void tryLogin(final String username, final String password, final Enumeration<String> salts)
    {
        if (!salts.hasMoreElements())
        {
            progressDialog.dismiss();
            SnackbarManager.show(Snackbar.with(this).text(R.string.wrong_name_or_password));
            return;
        }

        final Response loginResponse = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                if (response.has("Success") && !response.getBoolean("Success"))
                {
                    tryLogin(username, password, salts);
                    return;
                }

                User.id = Integer.parseInt(response.getString("IdUzivatel"));
                User.name = response.getString("Jmeno");
                User.surname = response.getString("Prijmeni");
                User.degreeBeforeName = response.getString("TitulPred");
                User.degreeAfterName = response.getString("TitulZa");
                User.email = response.getString("Email");
                User.loginSchool = response.getString("LoginSchool");
                User.isOnline = true;

                addUser(Hash.sha1(usernameSalt + username), Hash.sha1(passwordSalt + password + username), User.id, User.name, User.surname);

                loadSchools();
            }

            public void onError(VolleyError error)
            {
                progressDialog.dismiss();
                super.onError(error);
            }

            @Override
            public void onException(JSONException exception)
            {
                progressDialog.dismiss();
                super.onException(exception);
            }
        };
        JSONObject parameters = new JSONObject();
        try
        {
            parameters.put("Login", username);
            parameters.put("Password", Hash.getHashedPassword(password, salts.nextElement()));
        }
        catch (JSONException e) {}
        Request.post(URL.User.LoginSecureObject(), parameters, loginResponse);
    }


    /**
     * Načte všechny školy, které jsou pro uživatele dostupné.
     * Tato data jsou přednačítána pro další obrazovku.
     */
    private void loadSchools()
    {
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                if (response.length() == 0)
                {
                    progressDialog.dismiss();
                    SnackbarManager.show(Snackbar.with(LoginActivity.this).text(R.string.no_available_school));
                    return;
                }

                schoolIDs.clear();
                schools.clear();

                for (int j = 0; j < response.length(); j++)
                {
                    schoolIDs.add(Integer.parseInt(response.getJSONObject(j).getString("IDSkola")));
                    schools.add(response.getJSONObject(j).getString("Nazev"));
                }

                loadRoles(schoolIDs.get(0));
            }

            public void onError(VolleyError error)
            {
                progressDialog.dismiss();
                super.onError(error);
            }

            @Override
            public void onException(JSONException exception)
            {
                progressDialog.dismiss();
                super.onException(exception);
            }
        };
        Request.get(URL.School.SchoolsByUser(User.id), arrayResponse);
    }


    /**
     * Načte všechny role, které jsou pro uživatele a první školu dostupné.
     * Tato data jsou přednačítána pro další obrazovku.
     * @param schoolID první dostupná škola
     */
    private void loadRoles(int schoolID)
    {
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                if (response.length() == 0)
                {
                    progressDialog.dismiss();
                    SnackbarManager.show(Snackbar.with(LoginActivity.this).text(R.string.no_available_role));
                    return;
                }

                roleIDs.clear();
                roles.clear();

                for (int j = 0; j < response.length(); j++)
                {
                    roleIDs.add(Integer.parseInt(response.getJSONObject(j).getString("IDRole")));
                    roles.add(response.getJSONObject(j).getString("Nazev"));
                }

                Intent intent = new Intent(LoginActivity.this, LoginRoleSelectActivity.class);
                intent.putIntegerArrayListExtra("schoolIDs", schoolIDs);
                intent.putIntegerArrayListExtra("roleIDs", roleIDs);
                intent.putStringArrayListExtra("schools", schools);
                intent.putStringArrayListExtra("roles", roles);
                progressDialog.dismiss();
                startActivity(intent);
                finish();
            }

            public void onError(VolleyError error)
            {
                progressDialog.dismiss();
                super.onError(error);
            }

            @Override
            public void onException(JSONException exception)
            {
                progressDialog.dismiss();
                super.onException(exception);
            }
        };
        Request.get(URL.Role.GetRoles(User.id, schoolID), arrayResponse);
    }


    /**
     * Přechod to veřejných kurzů.
     * @param view
     */
    public void publicCourses(View view)
    {
        Intent intent = new Intent(this, AnonymousPublicCoursesActivity.class);
        startActivity(intent);
    }
}
