package cz.vsb.elogika.dataTypes;

import cz.vsb.elogika.adapters.HierarchicalAdapter;

public class ConsultationHourTerm extends HierarchicalAdapter.Item
{
    public String date;
    public int loggedCount;
}
