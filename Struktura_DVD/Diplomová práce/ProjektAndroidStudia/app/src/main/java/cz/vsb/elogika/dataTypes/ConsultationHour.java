package cz.vsb.elogika.dataTypes;

import cz.vsb.elogika.adapters.HierarchicalAdapter;

public class ConsultationHour extends HierarchicalAdapter.Item
{
    public int id;
    public String from;
    public String to;
    public String fromHours;
    public String toHours;
    public String room;
    public int termsCount;
    public ConsultationHourTerm[] terms;
    public int repeatableType;
    public int maximumOfStudents;
    public String description;
    public boolean notifyViaEmail;
}
