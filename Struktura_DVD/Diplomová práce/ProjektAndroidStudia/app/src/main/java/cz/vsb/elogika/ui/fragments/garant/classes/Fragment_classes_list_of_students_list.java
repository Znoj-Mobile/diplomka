package cz.vsb.elogika.ui.fragments.garant.classes;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.DividerItemDecoration;

public class Fragment_classes_list_of_students_list extends Fragment implements SwipeRefreshLayout.OnRefreshListener  {
    private View mainView;
    private View rootView;
    private RecyclerView recyclerView;

    protected RecyclerView.Adapter adapter;

    int classId = 0;
    boolean email = false;
    boolean search_clicked = false;

    EditText search_bar;
    Button search;

    SwipeRefreshLayout swipeLayout;


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mainView = inflater.inflate(R.layout.fragment_classes_list_of_students_list_layout, container, false);
        rootView = mainView.findViewById(R.id.recycler_view_with_button_swipe);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);

        swipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_update);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeResources(R.color.darker_cyan, R.color.material_blue_grey_800);

        classId = getArguments().getInt("classId");
        email = getArguments().getBoolean("email", false);

        Fragment_classes_list_of_students.nothinglayout = (LinearLayout) rootView.findViewById(R.id.nothing_to_show);
        Fragment_classes_list_of_students.nothinglayout.setVisibility(View.GONE);

        this.adapter = new RecyclerView.Adapter() {
            class ViewHolderHeader extends RecyclerView.ViewHolder {
                public ViewHolderHeader(View itemView) {
                    super(itemView);
                }
            }

            class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
                public TextView login;
                public TextView name;
                public TextView surname;
                public TextView email;
                public Button delete;
                public CheckBox checkbox;

                public ViewHolder(View itemView) {
                    super(itemView);
                    itemView.setOnClickListener(this);
                    this.login = (TextView) itemView.findViewById(R.id.c_l_s_login);
                    this.name = (TextView) itemView.findViewById(R.id.c_l_s_name);
                    this.surname = (TextView) itemView.findViewById(R.id.c_l_s_surname);
                    this.email = (TextView) itemView.findViewById(R.id.c_l_s_email);
                    this.delete = (Button)  itemView.findViewById(R.id.c_l_s_delete);
                    this.checkbox = (CheckBox) itemView.findViewById(R.id.c_l_s_checkbox);
                }

                @Override
                public void onClick(View view) {
                    checkbox.setChecked(!checkbox.isChecked());
                }
            }

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view;
                switch (viewType) {
                    case 1:
                        view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_classes_list_of_students_list, parent, false);
                        return new ViewHolder(view);
                    default:
                        return null;
                }
            }
            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
                if (holder.getItemViewType() == 1) {
                    final ViewHolder viewHolder = (ViewHolder) holder;

                    final ArrayList<Map<String, String>> list_of_students;
                    if(search_clicked){
                        list_of_students = Fragment_classes_list_of_students.list_of_found_students;
                    }
                    else{
                        list_of_students = Fragment_classes_list_of_students.list_of_students;
                    }

                    viewHolder.login.setText(list_of_students.get(position).get("login"));
                    viewHolder.name.setText(list_of_students.get(position).get("jmeno"));
                    viewHolder.surname.setText(list_of_students.get(position).get("prijmeni"));
                    viewHolder.email.setText(list_of_students.get(position).get("email"));
                    final int finalPosition = position;
                    if(email){
                        viewHolder.delete.setVisibility(View.GONE);
                        viewHolder.checkbox.setVisibility(View.VISIBLE);
                        if(list_of_students.get(finalPosition).get("checked").equals("true")){
                            viewHolder.checkbox.setChecked(true);
                        }
                        else{
                            viewHolder.checkbox.setChecked(false);
                        }
                        viewHolder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                if(viewHolder.checkbox.isChecked()){
                                    list_of_students.get(finalPosition).put("checked", "true");
                                }
                                else{
                                    list_of_students.get(finalPosition).put("checked", "false");
                                }
                            }
                        });
                    }
                    else{
                        viewHolder.delete.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setTitle(R.string.student_delete);
                                builder.setCancelable(false);
                                builder.setMessage(R.string.student_delete_message);
                                builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog2, int which) {
                                        delete(list_of_students.get(finalPosition).get("idUzivatel"), list_of_students.get(finalPosition).get("jmeno") + " " + list_of_students.get(finalPosition).get("prijmeni") + " [ " + list_of_students.get(finalPosition).get("login") + " ] ");
                                    }
                                });

                                builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog2, int which) {
                                        ;
                                    }
                                }).setIcon(android.R.drawable.ic_dialog_alert);
                                Dialog d = builder.create();
                                d.show();
                            }
                        });
                    }

                }
            }

            @Override
            public int getItemViewType(int position)
            {
                return 1;
            }

            @Override
            public int getItemCount() {
                if(search_clicked){
                    return Fragment_classes_list_of_students.list_of_found_students.size();
                }
                return Fragment_classes_list_of_students.list_of_students.size();
            }
        };

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        recyclerView.setHasFixedSize(true);
        //zaruci, ze se bude seznam aktualizovat jen pri swipu uplne nahore...
        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy <= 0) {
                    swipeLayout.setEnabled(true);
                } else {
                    swipeLayout.setEnabled(false);
                }
            }
        });
        recyclerView.setAdapter(adapter);

        Fragment_classes_list_of_students.update(adapter, swipeLayout);

        Button round_button = (Button) rootView.findViewById(R.id.roundButton);
        LinearLayout ll = (LinearLayout)mainView.findViewById(R.id.email_list_layout);
        if(email){
            round_button.setVisibility(View.GONE);
            ll.setVisibility(View.VISIBLE);
            Fragment_classes_list_of_students.list_of_found_students = new ArrayList<>();
            setSearchingFilter();

        }
        else{
            ll.setVisibility(View.GONE);
            round_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    add_student();
                }
            });
        }
        return mainView;
    }

    private void setSearchingFilter() {
        search_bar = (EditText) mainView.findViewById(R.id.email_search_bar);
        search = (Button) mainView.findViewById(R.id.email_search);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String searchText = search_bar.getText().toString();
                Fragment_classes_list_of_students.list_of_found_students.clear();
                for(Map<String, String> item : Fragment_classes_list_of_students.list_of_students){
                    if(item.get("jmeno").contains(searchText)){
                        Fragment_classes_list_of_students.list_of_found_students.add(item);
                    }
                    else  if(item.get("login").contains(searchText)){
                        Fragment_classes_list_of_students.list_of_found_students.add(item);
                    }
                    else  if(item.get("prijmeni").contains(searchText)){
                        Fragment_classes_list_of_students.list_of_found_students.add(item);
                    }
                }
                search_clicked = true;
                adapter.notifyDataSetChanged();
            }
        });
    }

    private void add_student() {
        Bundle bundle = new Bundle();
        bundle.putInt("classId", classId);
        Fragment_classes_add_student fragment_classes_add_student = new Fragment_classes_add_student();
        fragment_classes_add_student.setArguments(bundle);
        getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment_classes_add_student).commit();
    }


    private void delete(String studentId, final String name) {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                if (response.getString("Success").equals("true")) {
                    //novej toast
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.student) + " " + name + " " + getActivity().getString(R.string.student_was_deleted)));
                    Fragment_classes_list_of_students.update(adapter, swipeLayout);
                }
                else{ //nepovedlo se
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.student_4p) + " " + name + " " + getActivity().getString(R.string.student_was_not_deleted)));
                }
            }
        };

        Request.get(URL.User.RemoveStudent(studentId, classId, User.schoolID), response);
    }

    @Override
    public void onRefresh() {
        Fragment_classes_list_of_students.update(adapter, swipeLayout);
    }
}
