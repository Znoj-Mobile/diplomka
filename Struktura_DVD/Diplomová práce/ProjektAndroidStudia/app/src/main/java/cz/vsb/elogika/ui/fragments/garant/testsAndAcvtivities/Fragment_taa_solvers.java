package cz.vsb.elogika.ui.fragments.garant.testsAndAcvtivities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.DividerItemDecoration;
import cz.vsb.elogika.utils.DownloadFile;
import cz.vsb.elogika.utils.Logging;
import cz.vsb.elogika.utils.TimeLogika;

public class Fragment_taa_solvers extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private View rootView;
    private View mainView;
    private RecyclerView recyclerView;

    LinearLayout nothinglayout;
    SwipeRefreshLayout swipeLayout;

    String idActivity;
    String nameActivity;

    ArrayList<Map<String, String>> term_list;
    ArrayList<Map<String, String>> sol_list;
    ArrayList<Map<String, String>> list;

    List<String> toSpin_name;
    List<String> toSpin_id_term;

    private int spinner_position;
    ArrayAdapter<String> adapter_terms;
    protected RecyclerView.Adapter adapter;

    String current_term = "0";

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_taa_solvers, container, false);
        rootView = (View) mainView.findViewById(R.id.recycler_view_swipe);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);

        swipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_update);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeResources(R.color.darker_cyan, R.color.material_blue_grey_800);

        idActivity = getArguments().getString("idActivity");
        nameActivity = getArguments().getString("nameActivity");

        Actionbar.addSubcategory(R.string.solvers, this);
        Logging.logAccess("/Pages/Garant/AktivityVyreseneHodnoceni/AktivityVyreseneHodnoceni.aspx?IDA=" + idActivity);

        nothinglayout = (LinearLayout) rootView.findViewById(R.id.nothing_to_show);
        nothinglayout.setVisibility(View.GONE);


        term_list = new ArrayList<Map<String,String>>();
        sol_list = new ArrayList<Map<String,String>>();
        toSpin_name = new ArrayList<String>();
        toSpin_id_term = new ArrayList<String>();
        list = new ArrayList<Map<String,String>>();


        this.adapter = new RecyclerView.Adapter() {
            class ViewHolderHeader extends RecyclerView.ViewHolder {
                public ViewHolderHeader(View itemView) {
                    super(itemView);
                }
            }

            class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
                public TextView school_login;
                public TextView system_login;
                public TextView surname;
                public TextView person_name;
                public TextView solution;
                public Button attachment;
                public TextView insert_date;
                public EditText evaluation;
                public TextView evaluation_date;
                public Button save;

                public ViewHolder(View itemView) {
                    super(itemView);
                    itemView.setOnClickListener(this);
                    /*this.btn = (Button) itemView.findViewById(R.id.taa_a_button);*/
                    this.school_login = (TextView) itemView.findViewById(R.id.taa_sol_school_login);
                    this.system_login = (TextView) itemView.findViewById(R.id.taa_sol_system_login);
                    this.surname = (TextView) itemView.findViewById(R.id.taa_sol_surname);
                    this.person_name = (TextView) itemView.findViewById(R.id.taa_sol_person_name);
                    this.solution = (TextView) itemView.findViewById(R.id.taa_sol_solution);
                    this.attachment = (Button) itemView.findViewById(R.id.taa_sol_attachment);
                    this.insert_date = (TextView) itemView.findViewById(R.id.taa_sol_insert_date);
                    this.evaluation = (EditText) itemView.findViewById(R.id.taa_sol_evaluation);
                    this.evaluation_date = (TextView) itemView.findViewById(R.id.taa_sol_evaluation_date);
                    this.save = (Button) itemView.findViewById(R.id.taa_sol_save);
                }


                @Override
                public void onClick(View view) {

                }


            }

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view;
                switch (viewType) {
                    case 0:
                        view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_taa_solvers_list_header, parent, false);
                        return new ViewHolderHeader(view);
                    case 1:
                        view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_taa_solvers_list, parent, false);
                        return new ViewHolder(view);
                    default:
                        return null;
                }
            }
            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
                if (holder.getItemViewType() == 1) {
                    final ViewHolder viewHolder = (ViewHolder) holder;

                    viewHolder.school_login.setText(list.get(position).get("skolaLogin"));
                    viewHolder.system_login.setText(list.get(position).get("systemLogin"));
                    viewHolder.surname.setText(list.get(position).get("prijmeni"));
                    viewHolder.person_name.setText(list.get(position).get("jmeno"));
                    viewHolder.solution.setText(list.get(position).get("af_solution"));
                    viewHolder.attachment.setText(list.get(position).get("af_description"));
                    viewHolder.insert_date.setText(list.get(position).get("af_inserted"));
                    viewHolder.evaluation.setText(list.get(position).get("af_result"));
                    viewHolder.evaluation_date.setText(list.get(position).get("af_result_date"));

                    viewHolder.attachment.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(list.get(position).get("af_description") != null && list.get(position).get("af_description") != "" )
                                download(list.get(position).get("af_description"), list.get(position).get("af_idAktivitaVyresene"));
                        }
                    });
                    viewHolder.save.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setTitle(R.string.evaluation);
                            builder.setCancelable(false);
                            builder.setMessage(R.string.evaluation_save_message);
                            builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog2, int which) {
                                    change(list.get(position), viewHolder.evaluation.getText().toString());
                                }
                            });

                            builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog2, int which) {
                                    ;
                                }
                            }).setIcon(android.R.drawable.ic_dialog_alert);
                            Dialog d = builder.create();
                            d.show();
                        }
                    });
                }
            }

            @Override
            public int getItemViewType(int position)
            {
                    return 1;
            }

            @Override
            public int getItemCount() {
                return list.size();
            }
        };

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        recyclerView.setHasFixedSize(true);
        //zaruci, ze se bude seznam aktualizovat jen pri swipu uplne nahore...
        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy <= 0) {
                    swipeLayout.setEnabled(true);
                } else {
                    swipeLayout.setEnabled(false);
                }
            }
        });
        recyclerView.setAdapter(adapter);
        update();

        spinner_position = 0;
        final Spinner taa_sol_spinner = (Spinner) mainView.findViewById(R.id.taa_sol_spinner);
        taa_sol_spinner.setSelection(spinner_position);
        adapter_terms = new ArrayAdapter<String>(getActivity(),R.layout.spinner_item,toSpin_name);
        taa_sol_spinner.setAdapter(adapter_terms);

        taa_sol_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view2, int position, long id) {
                if (position != spinner_position) {
                    spinner_position = position;
                    current_term = term_list.get(position).get("idTermin");
                    update_sol(current_term);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                ;
            }

        });

        TextView taa_sol_activity_group = (TextView) mainView.findViewById(R.id.taa_sol_activity_group);
        taa_sol_activity_group.setText(Fragment_TestsAndActivity.nazevSA);
        TextView taa_sol_test = (TextView) mainView.findViewById(R.id.taa_sol_one_activity);
        taa_sol_test.setText(nameActivity);
        return mainView;
    }

    private void update() {
        swipeLayout.post(new Runnable() {
            @Override
            public void run() {
                nothinglayout.setVisibility(View.GONE);
                swipeLayout.setRefreshing(true);
                term_list.clear();
                toSpin_name.clear();
                toSpin_id_term.clear();
                adapter.notifyDataSetChanged();
                get_terms();
            }
        });
    }

    public void update_sol(final String dateId) {
        nothinglayout.setVisibility(View.GONE);
        swipeLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeLayout.setEnabled(true);
                swipeLayout.setRefreshing(true);
                sol_list.clear();
                list.clear();
                adapter.notifyDataSetChanged();
                get_sol(dateId);
            }
        });
    }

    public void get_terms() {
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                if (response.length() > 0)
                {
                    for (int j = 0; j < response.length(); j++)
                    {
                        term_list.add(put_data(
                                response.getJSONObject(j).getString("IDTermin"),
                                response.getJSONObject(j).getString("IDSkupinaAktivit"),
                                response.getJSONObject(j).getString("IDTest"),
                                response.getJSONObject(j).getString("IDAktivita"),
                                response.getJSONObject(j).getString("Nazev"),
                                response.getJSONObject(j).getString("Ucebna"),
                                response.getJSONObject(j).getString("NazevTyp"),
                                TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("AktivniOD")),
                                TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("AktivniDO")),
                                TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("PrihlaseniDO")),
                                response.getJSONObject(j).getString("IDAutor"),
                                TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("DatumPrihlaseni")),
                                TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("DatumOdhlaseni")),
                                response.getJSONObject(j).getString("NutnePrihlaseni"),
                                response.getJSONObject(j).getString("PocetStudentu"),
                                response.getJSONObject(j).getString("Smazany"),
                                response.getJSONObject(j).getString("PocetZapsanych"),
                                response.getJSONObject(j).getString("AktivitaNazev"),
                                response.getJSONObject(j).getString("PocetPokusu"),
                                response.getJSONObject(j).getString("Zapsan")));

                        toSpin_name.add(response.getJSONObject(j).getString("NazevTyp"));
                        toSpin_id_term.add(response.getJSONObject(j).getString("IDTermin"));
                    }
                }
                else{
                    Log.d("Chyba", "Taa get_terms");
                    nothinglayout.setVisibility(View.VISIBLE);
                }
                swipeLayout.setRefreshing(false);
                adapter.notifyDataSetChanged();
                adapter_terms.notifyDataSetChanged();

                current_term = term_list.get(0).get("idTermin");
                update_sol(current_term);
            }
        };
        Request.get(URL.Date.GetDates(Fragment_TestsAndActivity.groupActivityId, "-1", idActivity), arrayResponse);
    }

    private HashMap<String, String> put_data(String idTermin, String idSkupinaAktivit, String idTest, String idAktivita, String nazev, String ucebna, String nazevTyp,
                                             String aktivniOD, String aktivniDO, String prihlaseniDO, String idAutor, String datumPrihlaseni, String datumOdhlaseni,
                                             String nutnePrihlaseni, String pocetStudentu, String smazany, String pocetZapsanych, String aktivitaNazev, String pocetPokusu, String zapsan) {
        HashMap<String, String> item = new HashMap<String, String>();

        item.put("idTermin",idTermin);
        item.put("idSkupinaAktivit",idSkupinaAktivit);
        item.put("idTest",idTest);
        item.put("idAktivita",idAktivita);
        item.put("nazev",nazev);
        item.put("ucebna",ucebna);
        item.put("nazevTyp",nazevTyp);
        item.put("aktivniOD",aktivniOD);
        item.put("aktivniDO",aktivniDO);
        item.put("prihlaseniDO",prihlaseniDO);
        item.put("idAutor", idAutor);
        item.put("datumPrihlaseni",datumPrihlaseni);
        item.put("datumOdhlaseni",datumOdhlaseni);
        item.put("nutnePrihlaseni",nutnePrihlaseni);
        item.put("pocetStudentu",pocetStudentu);
        item.put("smazany",smazany);
        item.put("pocetZapsanych",pocetZapsanych);
        item.put("aktivitaNazev",aktivitaNazev);
        item.put("pocetPokusu",pocetPokusu);
        item.put("zapsan", zapsan);

        return item;

    }

    public void get_sol(final String dateId) {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException {
                if(response.length() > 0){
                    JSONArray ja = response.getJSONArray("List");
                    if(ja.length() > 0) {
                        nothinglayout.setVisibility(View.VISIBLE);
                        for (int j = 0; j < ja.length(); j++) {
                            put_data_gt(ja.getJSONObject(j).getString("IDUzivatel"),
                                    ja.getJSONObject(j).getString("Jmeno"),
                                    ja.getJSONObject(j).getString("Prijmeni"),
                                    ja.getJSONObject(j).getString("TitulPred"),
                                    ja.getJSONObject(j).getString("TitulZa"),
                                    ja.getJSONObject(j).getString("Email"),
                                    ja.getJSONObject(j).getString("SystemLogin"),
                                    ja.getJSONObject(j).getString("SkolaLogin"),
                                    TimeLogika.getFormatedDateTime(ja.getJSONObject(j).getString("DatumPrihlaseni")),
                                    ja.getJSONObject(j).getString("Pokus"), dateId);
                        }
                    }
                    else{
                        nothinglayout.setVisibility(View.VISIBLE);
                        swipeLayout.setRefreshing(false);
                    }
                }
                else{
                    Log.d("Chyba", "Taa get_sol");
                    nothinglayout.setVisibility(View.VISIBLE);
                    swipeLayout.setRefreshing(false);
                }
                adapter.notifyDataSetChanged();
            }
        };
        Request.get(URL.Date.GetStudents(dateId, User.schoolInfoID), response);
    }

    //posledni parametr je dateId!!!
    private void put_data_gt(String idUzivatel, String jmeno, String prijmeni, String titulPred, String titulZa,
                                                 String email, String systemLogin, String skolaLogin, String datumPrihlaseni, String pokus, String dateId) {
        HashMap<String, String> item = new HashMap<String, String>();

        item.put("idUzivatel",idUzivatel);
        item.put("jmeno",jmeno);
        item.put("prijmeni",prijmeni);
        item.put("titulPred",titulPred);
        item.put("titulZa",titulZa);
        item.put("email",email);
        item.put("systemLogin",systemLogin);
        item.put("skolaLogin",skolaLogin);
        item.put("datumPrihlaseni",datumPrihlaseni);
        item.put("pokus",pokus);

        Data_into_activity_finished_list(idUzivatel, dateId, item);
    }

    private void Data_into_activity_finished_list(String userId, final String dateId, final HashMap<String, String> item)
    {
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                if (response.length() > 0)
                {
                    for (int j = 0; j < response.length(); j++)
                    {
                        if(response.getJSONObject(j).getString("IDTermin").equals(dateId) && response.getJSONObject(j).getString("IDAktivita").equals(idActivity)) {
                            nothinglayout.setVisibility(View.GONE);
                            list.add(item);
                            put_finished_data(
                                    response.getJSONObject(j).getString("NazevSkupinaAktivit"),
                                    response.getJSONObject(j).getString("NazevAktivita"),
                                    response.getJSONObject(j).getString("DoporHodnoceni"),
                                    response.getJSONObject(j).getString("NazevTermin"),
                                    TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("DatumVlozeni")),
                                    response.getJSONObject(j).getString("Hodnoceni"),
                                    TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("HodnoceniDatum")),
                                    response.getJSONObject(j).getString("ReseniSouborNazev"),
                                    response.getJSONObject(j).getString("ReseniSouborContent"),
                                    response.getJSONObject(j).getString("Reseni"),
                                    response.getJSONObject(j).getString("Poznamka"),
                                    response.getJSONObject(j).getString("IDAktivitaVyresene"),
                                    response.getJSONObject(j).getString("IDVlozil"),
                                    response.getJSONObject(j).getString("IDTermin"),
                                    response.getJSONObject(j).getString("IDAktivita"),
                                    response.getJSONObject(j).getString("LoginSkola"),
                                    response.getJSONObject(j).getString("LoginSystem"),
                                    response.getJSONObject(j).getString("ReseniSoubor"),
                                    response.getJSONObject(j).getString("Jmeno"),
                                    response.getJSONObject(j).getString("Prijmeni"),
                                    response.getJSONObject(j).getString("TitulPred"),
                                    response.getJSONObject(j).getString("TitulZa"), item);
                            break;
                        }
                    }
                }
                else{
                    Log.d("Chyba", "Fragment_Ataa_solvers Data_into_activity_finished_list");
                }
                swipeLayout.setRefreshing(false);
                adapter.notifyDataSetChanged();
            }
        };
        Request.get(URL.SolvedActivities.GetAll(Integer.valueOf(userId), User.courseInfoId), arrayResponse);
    }

    private void put_finished_data(String nazevSkupinaAktivit, String nazevAktivita, String doporHodnoceni, String nazevTermin,
                                                  String datumVlozeni, String hodnoceni, String hodnoceniDatum, String reseniSouborNazev,
                                                  String reseniSouborContent, String reseni, String poznamka, String idAktivitaVyresene,
                                                  String idVlozil, String idTermin, String idAktivita, String loginSkola, String loginSystem,
                                                  String reseniSoubor, String jmeno, String prijmeni, String titulPred, String titulZa, HashMap<String, String> item) {
        item.put("af_group",nazevSkupinaAktivit);
        item.put("af_name",nazevAktivita);
        item.put("af_max",doporHodnoceni);
        item.put("af_term_name",nazevTermin);
        item.put("af_inserted",datumVlozeni);
        item.put("af_result",hodnoceni);
        item.put("af_result_date",hodnoceniDatum);
        item.put("af_description",reseniSouborNazev);
        item.put("af_description_type",reseniSouborContent);
        item.put("af_solution",reseni);
        item.put("af_note",poznamka);
        item.put("af_idAktivitaVyresene",idAktivitaVyresene);
        item.put("af_idVlozil",idVlozil);
        item.put("af_idTermin",idTermin);
        item.put("af_idAktivita",idAktivita);
        item.put("af_loginSkola",loginSkola);
        item.put("af_loginSystem",loginSystem);
        item.put("af_reseniSoubor",reseniSoubor);
        item.put("af_jmeno",jmeno);
        item.put("af_prijmeni",prijmeni);
        item.put("af_titulPred", titulPred);
        item.put("af_titulZa", titulZa);
    }


    private void change(final Map<String, String> item, String result) {
        Response response = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                swipeLayout.setRefreshing(false);
                Log.d("typ response: ", response.getString("Result"));
                if (response.getString("Result").equals("true")) {
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.evaluation) + " " + item.get("af_jmeno") + " (" + item.get("jmeno") + " " + item.get("prijmeni") + " [" + item.get("skolaLogin") + "]) " + getActivity().getString(R.string.evaluation_was_send)));
                }
                //aktualizace dat
                onRefresh();

            }
        };
        JSONObject parameters = new JSONObject();
        try
        {
            parameters.put("IDAktivitaVyresene", item.get("af_idAktivitaVyresene"));
            parameters.put("IDVlozil", User.id);
            parameters.put("Hodnoceni", result);
            parameters.put("HodnoceniDatum", TimeLogika.currentServerTime());

        }
        catch (JSONException e) {}

        //ukazani progressBaru - ze se neco deje
        swipeLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeLayout.setEnabled(true);
                swipeLayout.setRefreshing(true);
            }
        });
        Request.post(URL.SolvedActivities.postEvaluation(), parameters, response);
    }

    private void download(final String fileName, String activityId) {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                swipeLayout.setRefreshing(false);
                if (response.length() > 0)
                {
                    Log.d("download odpoved", response.toString());
                }
                else{
                    Log.d("Chyba", "Fragment_taa_solvers download");
                }
                Intent intent = DownloadFile.download(fileName, response.getString("ReseniSoubor"));
                getActivity().startActivity(intent);
                SnackbarManager.show(
                        Snackbar.with(getActivity())
                                .type(SnackbarType.MULTI_LINE)
                                .duration(Snackbar.SnackbarDuration.LENGTH_INDEFINITE)
                                .text(getResources().getString(R.string.file) + " '" + fileName + "' " + getResources().getString(R.string.is_downloaded_in_download_folder))
                                .attachToRecyclerView(recyclerView), getActivity());
            }
        };

        //ukazani progressBaru - ze se neco deje
        swipeLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeLayout.setEnabled(true);
                swipeLayout.setRefreshing(true);
            }
        });
        Request.get(URL.SolvedActivities.getsolvedactivityfile(Integer.valueOf(activityId)), response);
    }


    @Override
    public void onRefresh() {
        update_sol(current_term);
    }
}
