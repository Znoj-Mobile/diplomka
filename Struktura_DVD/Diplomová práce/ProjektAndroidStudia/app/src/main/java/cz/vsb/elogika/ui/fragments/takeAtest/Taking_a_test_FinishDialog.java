package cz.vsb.elogika.ui.fragments.takeAtest;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.View;

import cz.vsb.elogika.R;

public abstract class Taking_a_test_FinishDialog  {

    AlertDialog blockingDialog;

    public abstract void onRetryClicked();

    final View textLayout;
    public Taking_a_test_FinishDialog(Activity activity){
        textLayout = activity.getLayoutInflater().inflate(R.layout.dialog_sending_test_answers, null);
        blockingDialog = new AlertDialog.Builder(activity)
                .setTitle(R.string.sending_answers)
                .setView(textLayout)
                .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        blockingDialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                        textLayout.findViewById(R.id.dialog_error).setVisibility(View.GONE);
                        textLayout.findViewById(R.id.dialog_server_error).setVisibility(View.GONE);
                        textLayout.findViewById(R.id.dialog_server_upload).setVisibility(View.VISIBLE);

                        onRetryClicked();
                    }
                })
                .setIcon(android.R.drawable.ic_menu_upload)
                .setCancelable(false)
                .show();

        blockingDialog.getButton(AlertDialog.BUTTON_POSITIVE).setVisibility(View.GONE);
        blockingDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                blockingDialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                textLayout.findViewById(R.id.dialog_error).setVisibility(View.GONE);
                textLayout.findViewById(R.id.dialog_server_error).setVisibility(View.GONE);
                textLayout.findViewById(R.id.dialog_server_upload).setVisibility(View.VISIBLE);

                onRetryClicked();
            }
        });
    }


    public void setServerError(){
        blockingDialog.setCancelable(true);
        textLayout.findViewById(R.id.dialog_error).setVisibility(View.GONE);
        textLayout.findViewById(R.id.dialog_server_error).setVisibility(View.VISIBLE);
        textLayout.findViewById(R.id.dialog_server_upload).setVisibility(View.GONE);
        blockingDialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
        blockingDialog.getButton(AlertDialog.BUTTON_POSITIVE).setVisibility(View.VISIBLE);
    }
    public void setError(){
        textLayout.findViewById(R.id.dialog_error).setVisibility(View.VISIBLE);
        textLayout.findViewById(R.id.dialog_server_error).setVisibility(View.GONE);
        textLayout.findViewById(R.id.dialog_server_upload).setVisibility(View.GONE);
        blockingDialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
        blockingDialog.getButton(AlertDialog.BUTTON_POSITIVE).setVisibility(View.VISIBLE);
    }

    public void cancel(){
        blockingDialog.cancel();
    }


}
