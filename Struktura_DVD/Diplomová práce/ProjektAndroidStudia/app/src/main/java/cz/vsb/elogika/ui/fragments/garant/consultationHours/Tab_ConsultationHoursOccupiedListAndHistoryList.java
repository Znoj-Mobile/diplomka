package cz.vsb.elogika.ui.fragments.garant.consultationHours;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import cz.vsb.elogika.R;
import cz.vsb.elogika.adapters.HierarchicalAdapter;
import cz.vsb.elogika.dataTypes.ConsultationHour;
import cz.vsb.elogika.utils.DividerItemDecoration;
import cz.vsb.elogika.utils.TimeLogika;

public class Tab_ConsultationHoursOccupiedListAndHistoryList extends Fragment
{
    public HierarchicalAdapter adapter;
    private View loadingProgress;
    private View nothingToShow;
    public ArrayList<ConsultationHour> consultationHours;
    public Boolean areDataReady;
    public Object lock;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        final ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.recycler_view, container, false);
        // Přidání hlavičky
        ViewGroup header = (ViewGroup) LayoutInflater.from(getActivity()).inflate(R.layout.tab_consultation_hours_occupied_and_history_list_header, container, false);
        header.addView(HierarchicalAdapter.getGroupingAndExpandingButton(header), 0);
        rootView.addView(header, 0);
        final RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        this.loadingProgress = rootView.findViewById(R.id.loading_progress);
        this.nothingToShow = rootView.findViewById(R.id.nothing_to_show);
        this.loadingProgress.setVisibility(View.VISIBLE);

        // Vytvoření adaptéru
        this.adapter = new HierarchicalAdapter()
        {
            class ConsultationHourHolder extends HierarchicalAdapter.Holder
            {
                public TextView period;
                public TextView soonestOccupiedTerm;
                public TextView time;
                public TextView venue;
                public TextView description;
                public TextView space;
                public TextView repeating;

                public ConsultationHourHolder(View itemView)
                {
                    super(itemView);
                    this.period = (TextView) itemView.findViewById(R.id.period);
                    this.soonestOccupiedTerm = (TextView) itemView.findViewById(R.id.soonest_occupied_term);
                    this.time = (TextView) itemView.findViewById(R.id.time);
                    this.venue = (TextView) itemView.findViewById(R.id.venue);
                    this.description = (TextView) itemView.findViewById(R.id.description);
                    this.space = (TextView) itemView.findViewById(R.id.space);
                    this.repeating = (TextView) itemView.findViewById(R.id.repeating);
                }
            }


            class TermHolder extends HierarchicalAdapter.Holder
            {
                public TextView when;
                public TextView time;
                public TextView maximumOfStudents;
                public TextView loggedCount;

                public TermHolder(View itemView)
                {
                    super(itemView);
                    this.when = (TextView) itemView.findViewById(R.id.when);
                    this.time = (TextView) itemView.findViewById(R.id.time);
                    this.maximumOfStudents = (TextView) itemView.findViewById(R.id.maximum_of_students);
                    this.loggedCount = (TextView) itemView.findViewById(R.id.logged_count);
                }
            }



            @Override
            public Holder onCreateViewHolder(ViewGroup parent, int viewType)
            {
                switch (viewType)
                {
                    case Fragment_ConsultationHoursTabs.CONSULTATION_HOUR:
                        return new ConsultationHourHolder(wrapLayout(parent, R.layout.tab_consultation_hours_occupied_and_history_list_item));

                    case Fragment_ConsultationHoursTabs.TERM_HEADER:
                        return new HierarchicalAdapter.Holder(wrapLayout(parent, R.layout.dialog_consultation_hours_terms_header));

                    case Fragment_ConsultationHoursTabs.TERM:
                        return new TermHolder(wrapLayout(parent, R.layout.dialog_consultation_hours_terms_item));
                }

                return null;
            }



            @Override
            public void onBindViewHolder(Holder viewHolder, final int position, Item item)
            {
                final ConsultationHour consultationHour;

                switch(viewHolder.getItemViewType())
                {
                    case Fragment_ConsultationHoursTabs.CONSULTATION_HOUR:
                        final ConsultationHourHolder consultationHourHolder = (ConsultationHourHolder) viewHolder;
                        consultationHour = (ConsultationHour) item;

                        int soonestOccupiedTerm = 0;
                        for (int j = 0; j < consultationHour.terms.length; j++)
                        {
                            if (consultationHour.terms[j].loggedCount > 0)
                            {
                                soonestOccupiedTerm = j;
                                break;
                            }
                        }

                        if (consultationHour.terms.length > 0)
                        {
                            final int finalSoonestOccupiedTerm = soonestOccupiedTerm;

                            // Zobrazení seznamu studentů přihlášených na nejbližším termínu (pokud nějaký je)
                            consultationHourHolder.itemView.setOnClickListener(new View.OnClickListener()
                            {
                                @Override
                                public void onClick(View view)
                                {
                                    Dialog_ConsultationHoursStudents dialog = new Dialog_ConsultationHoursStudents();
                                    dialog.id = consultationHour.id;
                                    dialog.date = consultationHour.terms[finalSoonestOccupiedTerm].date;
                                    dialog.show(getFragmentManager(), "dialog");
                                }
                            });

                            consultationHourHolder.soonestOccupiedTerm.setText(TimeLogika.getFormatedDate(consultationHour.terms[soonestOccupiedTerm].date));
                            consultationHourHolder.space.setText(consultationHour.terms[soonestOccupiedTerm].loggedCount + " / " + String.valueOf(consultationHour.maximumOfStudents));
                        }
                        else
                        {
                            consultationHourHolder.itemView.setOnClickListener(null);
                            consultationHourHolder.soonestOccupiedTerm.setText("");
                            consultationHourHolder.space.setText("0 / " + String.valueOf(consultationHour.maximumOfStudents));
                        }

                        consultationHourHolder.period.setText(TimeLogika.getFormatedDate(consultationHour.from) + " - " + TimeLogika.getFormatedDate(consultationHour.to));
                        consultationHourHolder.time.setText(consultationHour.fromHours + " - " + consultationHour.toHours);
                        consultationHourHolder.venue.setText(consultationHour.room);
                        consultationHourHolder.description.setText(consultationHour.description);

                        int repeating = 0;
                        switch(consultationHour.repeatableType)
                        {
                            case 0:
                                repeating = R.string.no;
                                break;

                            case 1:
                                repeating = R.string.weekly;
                                break;

                            case 2:
                                repeating = R.string.fortnightly;
                                break;

                            case 3:
                                repeating = R.string.monthly;
                                break;
                        }
                        consultationHourHolder.repeating.setText(getResources().getString(repeating));
                        break;

                    case Fragment_ConsultationHoursTabs.TERM:
                        final TermHolder termHolder = (TermHolder) viewHolder;
                        final Fragment_ConsultationHoursTabs.ConsultationHourTermItem consultationHourTermItem = (Fragment_ConsultationHoursTabs.ConsultationHourTermItem) item;
                        consultationHour = consultationHourTermItem.consultationHour;

                        // Zobrazení seznamu studentů přihlášených na termínu
                        termHolder.itemView.setOnClickListener(new View.OnClickListener()
                        {
                            @Override
                            public void onClick(View view)
                            {
                                Dialog_ConsultationHoursStudents dialog = new Dialog_ConsultationHoursStudents();
                                dialog.id = consultationHour.id;
                                dialog.date = consultationHour.terms[consultationHourTermItem.position].date;
                                dialog.show(getFragmentManager(), "dialog");
                            }
                        });

                        termHolder.when.setText(TimeLogika.getFormatedDate(consultationHour.terms[consultationHourTermItem.position].date));
                        termHolder.time.setText(consultationHour.fromHours + " - " + consultationHour.toHours);
                        termHolder.maximumOfStudents.setText(String.valueOf(consultationHour.maximumOfStudents));
                        termHolder.loggedCount.setText(String.valueOf(consultationHour.terms[consultationHourTermItem.position].loggedCount));
                }

            }
        };

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        synchronized (this.lock)
        {
            if (this.areDataReady) notifyDataSetChanged();
        }

        return rootView;
    }



    public void notifyDataSetChanged()
    {
        this.loadingProgress.setVisibility(View.GONE);
        for (ConsultationHour consultationHour: this.consultationHours) this.adapter.addCategory(consultationHour, Fragment_ConsultationHoursTabs.CONSULTATION_HOUR);
        this.adapter.notifyDataSetChanged();
        if (consultationHours.size() == 0)
        {
            nothingToShow.findViewById(R.id.nothing_to_show).setVisibility(View.VISIBLE);
        }
    }
}
