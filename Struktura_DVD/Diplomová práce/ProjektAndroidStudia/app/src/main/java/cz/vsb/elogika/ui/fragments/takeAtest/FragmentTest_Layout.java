package cz.vsb.elogika.ui.fragments.takeAtest;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cz.vsb.elogika.R;
import cz.vsb.elogika.common.view.SlidingTabLayout;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.Actionbar;

/**
 * Fragment pro zobrazení Offline testů, a vypracovaných Offline testů na dvou kartách vedle sebe
 */
public class FragmentTest_Layout extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    SwipeRefreshLayout swipeLayout;

    /**
     * Inicializace UI
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_evaluation, container, false);
        swipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_update);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeResources(R.color.darker_cyan, R.color.material_blue_grey_800);
        swipeLayout.setRefreshing(false);

        if(User.isOnline)
            Actionbar.newCategory(R.string.offline_tests, this);
        else
            Actionbar.newCategory(R.string.logged_offline, this);
        ViewPager mViewPager = (ViewPager) rootView.findViewById(R.id.evaluation_viewpager);
        mViewPager.setAdapter(new FragmentStatePagerAdapter(getFragmentManager()) {
            String[] tabNames = new String[] {
                        getString(R.string.downloaded_offline_tests),
                        getString(R.string.offline_tests_drawn)};

            @Override
            public int getCount() {
                return tabNames.length;
            }
            @Override
            public CharSequence getPageTitle(int position) {
                return tabNames[position];
            }


            /**
            * Inicializuje testy na jednotlivých kartách
            */
            @Override
            public Fragment getItem(int position) {

                switch (position)
                {
                    case 0:
                        offlineTestlist = new Fragment_OfflineTest_list() {
                            @Override
                            public void onDataChanged() {
                                doneofflinetests.getAllDoneTestNames();
                            }
                        };
                        return offlineTestlist;//offlinetest;
                    case 1:
                        doneofflinetests =new Fragment_Offline_Done() {
                            @Override
                            public void onDataChanged() {
                                offlineTestlist.getAllStoredTests();
                            }
                        };
                        return doneofflinetests;
                    default : return null;
                }
            }
        }
        );
        SlidingTabLayout mSlidingTabLayout = (SlidingTabLayout) rootView.findViewById(R.id.evaluation_sliding_tabs);
        mSlidingTabLayout.setViewPager(mViewPager);

        return rootView;
    }

    static Fragment_OfflineTest_list offlineTestlist;
    static Fragment_Offline_Done doneofflinetests;

    /**
     * Po vypracování testu je třeba aktualizovat informace na kartách
     */
    public static void refreshaftertest()
    {
        offlineTestlist.getAllStoredTests();
        doneofflinetests.getAllDoneTestNames();
    }

    @Override
    public void onRefresh() {
        if(swipeLayout != null) {
            swipeLayout.setRefreshing(false);
        }

    }
}
