package cz.vsb.elogika.ui.fragments.Schedule;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.google.gson.Gson;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.dataTypes.Lesson;
import cz.vsb.elogika.utils.DividerItemDecoration;
import cz.vsb.elogika.utils.TimeLogika;

public class Fragment_ScheduleDay extends Fragment
{
    private RecyclerView.Adapter adapter;
    private ArrayList<Map<String, String>> itemList;
    public ArrayList<Lesson> lessons;



    public Fragment_ScheduleDay()
    {
        this.itemList = new ArrayList();
    }



    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.recycler_view, container, false);
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);

        // Vytvoření adaptéru
        this.adapter = new RecyclerView.Adapter()
        {
            class ViewHolder extends RecyclerView.ViewHolder
            {
                public View line;
                public TextView from;
                public TextView to;
                public TextView firstRow;
                public TextView secondRow;

                public ViewHolder(View itemView)
                {
                    super(itemView);
                    this.line = itemView.findViewById(R.id.line);
                    this.from = (TextView) itemView.findViewById(R.id.from);
                    this.to = (TextView) itemView.findViewById(R.id.to);
                    this.firstRow = (TextView) itemView.findViewById(R.id.first_row);
                    this.secondRow = (TextView) itemView.findViewById(R.id.second_row);
                }
            }

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
            {
                View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_schedule_item, parent, false);
                return new ViewHolder(view);
            }

            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position)
            {
                ViewHolder holder = (ViewHolder) viewHolder;

                // Detail
                holder.itemView.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        final Dialog dialog = new Dialog(getActivity());
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.fragment_schedule_lesson_detail);

                        ((TextView) dialog.findViewById(R.id.day)).setText(TimeLogika.getNameOfDayInWeek(lessons.get(position).day));
                        if (lessons.get(position).date == null)
                        {
                            dialog.findViewById(R.id.label_date).setVisibility(View.GONE);
                            dialog.findViewById(R.id.date).setVisibility(View.GONE);
                        }
                        else ((TextView) dialog.findViewById(R.id.date)).setText(TimeLogika.getFormatedDate(lessons.get(position).date));
                        ((TextView) dialog.findViewById(R.id.lesson_start)).setText(lessons.get(position).from);
                        ((TextView) dialog.findViewById(R.id.lesson_end)).setText(lessons.get(position).to);
                        ((TextView) dialog.findViewById(R.id.lesson_name)).setText(lessons.get(position).name);
                        ((TextView) dialog.findViewById(R.id.lesson_shortcut)).setText(lessons.get(position).shortcut);
                        ((TextView) dialog.findViewById(R.id.week)).setText(lessons.get(position).week);
                        ((TextView) dialog.findViewById(R.id.room)).setText(lessons.get(position).room);
                        ((TextView) dialog.findViewById(R.id.tutor)).setText(lessons.get(position).teacherDegreeBeforeName + " " + lessons.get(position).teacherName + " " + lessons.get(position).teacherSurname + " " + lessons.get(position).teacherDegreeAfterName);
                        ((TextView) dialog.findViewById(R.id.lesson_type)).setText((lessons.get(position).type == 0 ? R.string.lecture : R.string.exercise));

                        if (lessons.get(position).timetableID == -1)
                        {
                            dialog.findViewById(R.id.label_action).setVisibility(View.GONE);
                            dialog.findViewById(R.id.action_buttons).setVisibility(View.GONE);
                        }
                        else
                        {
                            dialog.findViewById(R.id.edit).setOnClickListener(new View.OnClickListener()
                            {
                                @Override
                                public void onClick(View view)
                                {
                                    dialog.dismiss();
                                    Bundle bundle = new Bundle();
                                    bundle.putString("lesson", new Gson().toJson(lessons.get(position)).toString());
                                    Fragment_ScheduleLessonCreationAndModification fragment = new Fragment_ScheduleLessonCreationAndModification();
                                    fragment.setArguments(bundle);
                                    getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment).commit();

                                }
                            });

                            dialog.findViewById(R.id.delete).setOnClickListener(new View.OnClickListener()
                            {
                                @Override
                                public void onClick(View view)
                                {
                                    new AlertDialog.Builder(getActivity()).setTitle(R.string.are_you_sure).setMessage(R.string.delete_lesson_question).setPositiveButton(R.string.yes, new DialogInterface.OnClickListener()
                                    {
                                        public void onClick(DialogInterface dialogy, int which)
                                        {
                                            dialogy.dismiss();
                                            dialog.dismiss();
                                            removeOwnLesson(position);
                                        }
                                    }).setNegativeButton(R.string.no, null).setIcon(android.R.drawable.ic_dialog_alert).show();
                                }
                            });
                        }

                        dialog.show();
                    }
                });
                if (lessons.get(position).type == 0) holder.line.setBackgroundColor(getResources().getColor(android.R.color.holo_red_dark));
                else holder.line.setBackgroundColor(getResources().getColor(android.R.color.holo_blue_dark));

                holder.from.setText(lessons.get(position).from);
                holder.to.setText(lessons.get(position).to);
                holder.firstRow.setText(lessons.get(position).name + " (" + lessons.get(position).shortcut + ")");
                holder.secondRow.setText((lessons.get(position).date == null ? lessons.get(position).week : TimeLogika.getFormatedDate(lessons.get(position).date)) + " - " + lessons.get(position).room + ", " + (lessons.get(position).teacherDegreeBeforeName.isEmpty() ? "" : (lessons.get(position).teacherDegreeBeforeName + " ")) + lessons.get(position).teacherName + " " + lessons.get(position).teacherSurname + " " + lessons.get(position).teacherDegreeAfterName);
                if (lessons.get(position).timetableID != -1)
                {
                    holder.itemView.setBackgroundColor(Color.argb(20, 181, 230, 29));
                }
            }

            @Override
            public int getItemCount()
            {
                return lessons.size();
            }
        };
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        return rootView;
    }


    /**
     * Vymaže studentem vytvořenou hodinu.
     * @param position index v poli lessons
     */
    private void removeOwnLesson(final int position)
    {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                lessons.remove(position);
                adapter.notifyItemRemoved(position);
                SnackbarManager.show(Snackbar.with(getActivity()).type(SnackbarType.MULTI_LINE).text(R.string.lesson_was_successfully_deleted));
            }
        };
        Request.get(URL.Timetable.RemoveTimetable(this.lessons.get(position).timetableID), response);
    }
}
