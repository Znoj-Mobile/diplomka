package cz.vsb.elogika.dataTypes;

public class Tutor
{
    public int id;
    public String name;
    public String surname;
    public String login;
    public String email;
}
