package cz.vsb.elogika.utils;

import android.app.DatePickerDialog;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class DatePickerDialogButton extends Button
{
    public int year;
    public int month;
    public int day;
    private Calendar currentDate;

    public DatePickerDialogButton(Context context)
    {
        super(context);
        init();
    }

    public DatePickerDialogButton(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }

    public DatePickerDialogButton(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        init();
    }
//
//    public DatePickerDialogButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes)
//    {
//        super(context, attrs, defStyleAttr, defStyleRes);
//        init();
//    }



    /**
     * Nastaví aktuální čas.
     * Nastaví na tlačítko listener, který vyvolá dialog pro nastavení času.
     * Nastaví aktuální hodinu.
     */
    private void init()
    {
        currentDate = Calendar.getInstance();
        currentDate.setTimeZone(TimeZone.getDefault());
        setCurrentDate();

        this.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {
                DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener()
                {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day)
                    {
                        DatePickerDialogButton.this.year = year;
                        DatePickerDialogButton.this.month = month + 1;
                        DatePickerDialogButton.this.day = day;
                        drawDate();
                    }
                };
                new DatePickerDialog(DatePickerDialogButton.this.getContext(), listener, year, month - 1, day).show();
            }
        });
    }



    /**
     * Nastaví rok, měsíc i den.
     * @param date datum musí být ve formátu yyyy-MM-dd'T'kk:mm:ss, kde hodiny, minuty a sekundy nejsou podstatné
     * @throws ParseException pokud je datum ve špatném formátu
     */
    public void setDate(String date) throws ParseException
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'kk:mm:ss");
        Date d = dateFormat.parse(date);
        Calendar c = new GregorianCalendar();
        c.setTime(d);
        this.year = c.get(Calendar.YEAR);
        this.month = c.get(Calendar.MONTH);
        this.day = c.get(Calendar.DAY_OF_MONTH);
        drawDate();
    }



    /**
     * Nastaví rok, měsíc i den.
     * @param year rok
     * @param month měsíc
     * @param day den
     */
    public void setDate(int year, int month, int day)
    {
        this.year = year;
        this.month = month;
        this.day = day;
        drawDate();
    }



    /**
     * Nastaví aktuální rok, měsíc i den.
     */
    public void setCurrentDate()
    {
        this.year = currentDate.get(Calendar.YEAR);
        this.month = currentDate.get(Calendar.MONTH) + 1;
        this.day = currentDate.get(Calendar.DAY_OF_MONTH);
        drawDate();
    }


    /**
     * Zobrazí na tlačítku datum podle dat.
     */
    public void drawDate()
    {
        this.setText(getDate(this.year, this.month, this.day));
    }


    /**
     * @return vrátí datum ve formátu yyyy-MM-dd'T'kk:mm:ss
     */
    public String getDate()
    {
        return createDate(this.year, this.month, this.day);
    }



    /**
     * Vrací kompletni datum s časem.
     * @return vrátí datum ve formátu yyyy-MM-dd'T'kk:mm:ss
     */
    public String getDate(TimePickerDialogButton time)
    {
        return getDateTime(this.year, this.month, this.day, time.hour, time.minute);
    }



    private String getDate(int years, int months, int days)
    {
        return (days + ". " + months + ". " + years);
    }



    private String createDate(int year, int month, int day)
    {
        return year + "-" + (month < 10 ? "0" + month : month) + "-" + (day < 10 ? "0" + day : day) + "T00:00:00";
    }



    public static String getDateTime(int year, int month, int day, int hours, int minutes)
    {
        return year + "-" + (month < 10 ? "0" + month : month) + "-" + (day < 10 ? "0" + day : day) + "T" + (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes);
    }
}
