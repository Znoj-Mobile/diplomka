package cz.vsb.elogika.ui.fragments.takeAtest;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.ui.BaseActivity;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.Logging;
import cz.vsb.elogika.utils.TimeLogika;

/**
 * Fragment pro Zobrazení dostupných testů k vypracování,
 * je zde možno zahájit vypracovávání nebo stáhnout test offline
 */
public class Fragment_OnlineTest_list extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private ArrayList<Map<String, String>> itemList = new ArrayList<Map<String, String>>();
    private SimpleAdapter adapter;
    SwipeRefreshLayout swipeLayout;
    ListView listView;
    Activity aktivita;
    Button buttonGenerateTest, buttonDownloadTestOffline;
    ProgressBar progressloadingtests;
    TextView progressPercentage;
    LinearLayout progressBar;
    Dialog myDialog;
    LinearLayout nothinglayout;
    //ArrayList<JSONObject> jsonObjectList = new ArrayList<JSONObject>();
    private JSONArray avaiableTestsJSONArray;


    /**
     * metoda se volá z karty vypracované testy, v tomto objektu si stáhne test a zobrazí ho
     *
     * @param aktivita odkaz na aktivitu
     * @param testID id vypraovaného testu
     * @param GeneratedTestId id vygenerovaného testu
     * @param myDialog odkaz na dialog, aby se po stažení testu zavřel
     * @param progressBar odkaz na načítacá kolečko
     * @param progressPercentage odkaz na text s procentama
     *
     */
    public void showTestDrawn(Activity aktivita, int testID, int GeneratedTestId, Dialog myDialog, LinearLayout progressBar, TextView progressPercentage) {
        Taking_a_test.mode = Taking_a_test.TESTDRAWN;
        this.aktivita = aktivita;
        this.myDialog = myDialog;
        this.progressBar = progressBar;
        this.progressPercentage = progressPercentage;
        getTest(testID, GeneratedTestId, false, -1);
    }

    /**
     * metoda se volá z karty teorie, v tomto objektu si stáhne test a zobrazí ho
     *
     * @param aktivita odkaz na aktivitu
     * @param testID id vypraovaného testu
     * @param cas doba pro vypracování testu
     * @param myDialog odkaz na dialog, aby se po stažení testu zavřel
     * @param progressBar odkaz na načítacá kolečko
     * @param progressPercentage odkaz na text s procentama
     *
     */
    public void practiseTest(Activity aktivita, int testID, int cas, Dialog myDialog, LinearLayout progressBar, TextView progressPercentage) {
        Taking_a_test.mode = Taking_a_test.TEST;
        this.aktivita = aktivita;
        this.myDialog = myDialog;
        this.progressBar = progressBar;
        this.progressPercentage = progressPercentage;
        getAvaiableTest(testID, cas);
    }


    /**
     * Inicializace UI
     * zobrazi všechny dostupné testy
     */
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.aktivita = getActivity();
        Actionbar.newCategory(R.string.take_a_test, this);
        Logging.logAccess("/Pages/Student/Vypracovani.aspx");
        Taking_a_test.mode = Taking_a_test.TEST;
        View rootView = inflater.inflate(R.layout.fragment_take_a_test, container, false);
        swipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_update);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeResources(R.color.darker_cyan, R.color.material_blue_grey_800);

        rootView.findViewById(R.id.take_a_test_title).setVisibility(View.VISIBLE);
        listView = (ListView) rootView.findViewById(R.id.take_a_test_listView);
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                boolean enable = false;
                if (listView != null) {
                    if (listView.getChildCount() == 0) {
                        enable = true;
                    } else {
                        // check if the first item of the list is visible
                        boolean firstItemVisible = listView.getFirstVisiblePosition() == 0;
                        // check if the top of the first item is visible
                        boolean topOfFirstItemVisible = listView.getChildAt(0).getTop() == 0;
                        // enabling or disabling the refresh layout
                        enable = firstItemVisible && topOfFirstItemVisible;
                    }
                }
                swipeLayout.setEnabled(enable);
            }
        });
        progressloadingtests = (ProgressBar) rootView.findViewById(R.id.take_a_Test_progress);
        nothinglayout = (LinearLayout) rootView.findViewById(R.id.take_a_test_nothing_to_show);
        String[] from = {"name", "activitiesGroup", "minPoints", "maxPoints", "active", "trialsRemaining", "timeMinutes"};
        int[] to = {R.id.take_a_test_name, R.id.take_a_test_activities_group, R.id.take_a_test_minpoints, R.id.take_a_test_maxpoints,
                R.id.take_a_test_active, R.id.take_a_test_trialsRemaining, R.id.take_a_test_timeminutes
        };

        this.adapter = new SimpleAdapter(getActivity(), this.itemList, R.layout.fragment_take_a_test_item, from, to) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView nadpis = (TextView) view.findViewById(R.id.take_a_test_name);
                String mandatory = itemList.get(position).get("mandatory");
                if (mandatory.charAt(0) == 'T') {
                    nadpis.setTextColor(getResources().getColor(R.color.elogika_mandatory));
                } else nadpis.setTextColor(Color.WHITE);
                view.setId(Integer.parseInt(mandatory.substring(1)));

                return view;
            }
        };


        listView.setAdapter(this.adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                myDialog = new Dialog(getActivity());
                myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                myDialog.setContentView(R.layout.fragment_take_a_test_detail);
                LinearLayout Llayout = (LinearLayout) myDialog.findViewById(R.id.take_a_test_detail_layout);
                TextView name = (TextView) view.findViewById(R.id.take_a_test_name);
                TextView activites_group = (TextView) view.findViewById(R.id.take_a_test_activities_group);
                //  TextView points = (TextView) view.findViewById(R.id.take_a_test_p);
                TextView minpoints = (TextView) view.findViewById(R.id.take_a_test_minpoints);
                TextView maxpoints = (TextView) view.findViewById(R.id.take_a_test_maxpoints);
                TextView active = (TextView) view.findViewById(R.id.take_a_test_active);
                final TextView trialsRemaining = (TextView) view.findViewById(R.id.take_a_test_trialsRemaining);
                final TextView timeMinutes = (TextView) view.findViewById(R.id.take_a_test_timeminutes);
                Taking_a_test.testName = (String) name.getText();
                String data[];
                String[] labels = {getString(R.string.name), getString(R.string.activity_group), getString(R.string.minmaxpoints),
                        getString(R.string.trialsremaining), getString(R.string.minutes)};

                data = new String[]{
                        (String) name.getText(),
                        (String) activites_group.getText(),
                        //    (String) points.getText(),
                        minpoints.getText() + " / " + maxpoints.getText(),
                        (String) trialsRemaining.getText(),
                        (String) timeMinutes.getText()
                };


                TextView labelText;
                TextView dataText;
                for (int j = 0; j < data.length && j < labels.length; j++) {
                    labelText = new TextView(getActivity());
                    labelText.setText(labels[j]);
                    if (Build.VERSION.SDK_INT < 23) {
                        labelText.setTextAppearance(view.getContext(), R.style.itemHint);
                    } else {
                        labelText.setTextAppearance(R.style.itemHint);
                    }
                    Llayout.addView(labelText);
                    dataText = new TextView(getActivity());
                    dataText.setText(data[j]);
                    if (Build.VERSION.SDK_INT < 23) {
                        dataText.setTextAppearance(view.getContext(), R.style.itemValue);
                    } else {
                        dataText.setTextAppearance(R.style.itemValue);
                    }
                    dataText.setGravity(Gravity.RIGHT);// | Gravity.CENTER);
                    Llayout.addView(dataText);
                }
                progressBar = (LinearLayout) inflater.inflate(R.layout.loadingwithpercentage, null);
                progressPercentage = (TextView) progressBar.findViewById(R.id.percentageprogresstext);
                Llayout.addView(progressBar);
                buttonGenerateTest = new Button(getActivity());
                buttonGenerateTest.setText(R.string.generate_and_run_test);
                final int testID = view.getId();
                buttonGenerateTest.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        myDialog.setCancelable(false);
                        progressBar.setVisibility(View.VISIBLE);
                        buttonDownloadTestOffline.setVisibility(View.GONE);
                        buttonGenerateTest.setVisibility(View.GONE);
                        try {
                            canGenerate(
                                    true,
                                    false,
                                    Integer.valueOf((String) timeMinutes.getText()),
                                    testID,
                                    avaiableTestsJSONArray.getJSONObject(position).getInt("DateId"),
                                    trialsRemaining,
                                    buttonGenerateTest,
                                    buttonDownloadTestOffline
                            );
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
                buttonGenerateTest.setEnabled(true); //TODO false
                Llayout.addView(buttonGenerateTest);
                buttonDownloadTestOffline = new Button(getActivity());
                try {
                    if(avaiableTestsJSONArray.getJSONObject(position).getBoolean("Offline"))
                        buttonDownloadTestOffline.setVisibility(View.VISIBLE);
                    else
                        buttonDownloadTestOffline.setVisibility(View.GONE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                buttonDownloadTestOffline.setText(R.string.download_test_offline);
                buttonDownloadTestOffline.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        myDialog.setCancelable(false);
                        progressBar.setVisibility(View.VISIBLE);
                        buttonDownloadTestOffline.setVisibility(View.GONE);
                        buttonGenerateTest.setVisibility(View.GONE);
                        try {
                            Taking_a_test.DateSendTo =  avaiableTestsJSONArray.getJSONObject(position).getString("DateSendTo");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            canGenerate(
                                    true,
                                    true,
                                    Integer.valueOf((String) timeMinutes.getText()),
                                    testID,
                                    avaiableTestsJSONArray.getJSONObject(position).getInt("DateId"),
                                    trialsRemaining,
                                    buttonGenerateTest,
                                    buttonDownloadTestOffline
                            );
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                buttonDownloadTestOffline.setEnabled(false);

                try {
                    canGenerate(
                            false, //generatetest
                            false, //offline
                            0,     //minutes
                            testID,
                            avaiableTestsJSONArray.getJSONObject(position).getInt("DateId"),
                            trialsRemaining,
                            buttonGenerateTest,
                            buttonDownloadTestOffline
                    );
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Llayout.addView(buttonDownloadTestOffline);
                myDialog.show();
            }
        });

        listView.setAdapter(adapter);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        //progressloadingtests.setVisibility(View.VISIBLE);
        getAvaiableTest(-1, -1);
    }


    /**
     * Tato metoda zařadí test do seznamu
     *
     * @param testID id testu
     * @param name název testu
     * @param activitiesGroup název skupiny aktivit
     * @param minPoints minimální počet bodu pro uspěch
     * @param maxPoints celkový počet bodů
     * @param activeFrom aktivní od
     * @param activeTo aktivní do
     * @param trialsRemaining zbývá pokusů
     * @param mandatory povinný?
     * @param timeMinutes čas na vypracování v minutách
     *
     */
    private void addItem(int testID, String name, String activitiesGroup, String minPoints, String maxPoints,
                         String activeFrom, String activeTo, String trialsRemaining, boolean mandatory, int timeMinutes) {

        HashMap<String, String> item = new HashMap<String, String>();
        item.put("name", name);
        item.put("activitiesGroup", activitiesGroup);
        item.put("minPoints", minPoints);
        item.put("maxPoints", maxPoints);
        item.put("active", TimeLogika.getFormatedDateTime(activeFrom) + " - " + TimeLogika.getFormatedDateTime(activeTo));
        item.put("trialsRemaining", trialsRemaining);
        if (mandatory)
            item.put("mandatory", "T" + testID);
        else item.put("mandatory", "F" + testID);
        item.put("timeMinutes", String.valueOf(timeMinutes));
        this.itemList.add(item);
        if (this.adapter != null) this.adapter.notifyDataSetChanged();
    }

    /**
     * Získá všechny dostupné testy ze serveru, atributy se vyplní -1,-1
     * pokud je tato metoda volana z vypracovaných nebo cvičnách testů tak se musí zadat ID a čas
     * a následně se test stáhne a rovnou zobrazí
     *
     * @param testID klasicky -1, je li voláno z vypracovaných nebo cvičných testů je třeba zadat ID
     * @param cas čas na vypracování testu v minutách
     *
     */
    private void getAvaiableTest(final int testID, final int cas)
    { //dostanu seznam dostupných testů
        ArrayResponse res = new ArrayResponse() {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                itemList.clear();
                if (adapter != null) adapter.notifyDataSetChanged();
                avaiableTestsJSONArray = response;
                if (response.length() > 0) {
                    for (int j = 0; j < response.length(); j++) {
                        String remaintogenerate = "0";
                        try{
                            remaintogenerate= response.getJSONObject(j).getString("RemainToGenerate");
                        }
                        catch (JSONException e){}

                        addItem(response.getJSONObject(j).getInt("TestId"),
                                response.getJSONObject(j).getString("Name"),
                                response.getJSONObject(j).getString("GroupActivityName"),
                                response.getJSONObject(j).getString("Min"),
                                response.getJSONObject(j).getString("Max"),
                                response.getJSONObject(j).getString("DateAvailableFrom"),
                                response.getJSONObject(j).getString("DateAvailableTo"),
                             // response.getJSONObject(j).getString("Attempts"),
                                remaintogenerate,
                                response.getJSONObject(j).getBoolean("Required"),
                                response.getJSONObject(j).getInt("TimeMinutes")
                                );
                    }
                }

                else{ nothinglayout.setVisibility(View.VISIBLE);}
                if (progressloadingtests != null) {
                    progressloadingtests.setVisibility(View.GONE);
                    swipeLayout.setRefreshing(false);
                }
                if (testID !=-1)
                    generateTest(testID,false,cas);
            }
        };
        Request.get(URL.GeneratedTest.getAvaiableTests(User.id, User.courseInfoId), res);
    }


    /**
     * Metoda, která zkontroluje na serveru jestli lze test vygenerovat
     *
     * @param generate boolean má-li být test rovnou spuštěn
     * @param offline boolean jedná-li se o offline test
     * @param timeMinutes čas na vypracování v minutách
     * @param testId id testu pro vygenerování
     * @param terminId id termínu pro test
     * @param trialstext odkaz na text s počtem pokusů
     * @param b odkaz na tlačítko pro spustit test
     * @param c odkaz na tlačítko pro stáhnutí testu offline
     *
     */
    private void canGenerate(final boolean generate, final boolean offline, final int timeMinutes, final int testId, int terminId, final TextView trialstext, final Button b, final Button c){
        Response res = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException {
                Log.d("cangenerate", response.toString());
                b.setEnabled(response.getBoolean("CanGenerate"));
                c.setEnabled(response.getBoolean("CanGenerate"));
                trialstext.setText((String.valueOf(response.getInt("RemainToGenerate"))));
                if(response.getInt("RemainToGenerate") == 0)
                    trialstext.setTextColor(Color.RED);

                if (generate)
                    generateTest(testId, offline, timeMinutes);
            }
        };
        Request.get(URL.GeneratedTest.canGenerate(testId, User.id, terminId), res);
    }


    /**
     * metoda pro vygenerování id testu
     *
     * @param testID id testu
     * @param Offline boolean má-li být test stažen offline nebo rovnou spuštěn
     * @param timeMinutes čas na vypracování v minutách
     *
     */
    private void generateTest(final int testID, final boolean Offline, final int timeMinutes)
    {
        //TODO tato metoda posila obrazky ktere jsou zbytečné a navic špatně
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                progressPercentage.setText(1 + " %");
                Log.d("generate test response", response.toString());
                int GeneratedTestId = response.getJSONObject("Result").getInt("GeneratedTestId");
                Log.d("generate test", "žádám o test generatedTestId " +GeneratedTestId);
                Taking_a_test.startAt = response.getJSONObject("Result").getString("StartAt");

                Log.w("generatedTest", response.toString());
                getTest(testID,GeneratedTestId, Offline,timeMinutes);

            }

            @Override
            public void onError(VolleyError error) {
                //Toast.makeText(aktivita,"Chyba v testu,", Toast.LENGTH_LONG).show();
                Log.e("Fragment_OnlineTest_list", "" + error);
                SnackbarManager.show(
                        Snackbar.with(aktivita)
                                .text(aktivita.getString(R.string.test_failed)));
                myDialog.setCancelable(true);
            }
        };
        JSONObject parameters = new JSONObject();
        try
        {
            if (avaiableTestsJSONArray.length() > 0) {
                for (int j = 0; j < avaiableTestsJSONArray.length(); j++) {
                    if(testID == avaiableTestsJSONArray.getJSONObject(j).getInt("TestId"))
                    {
                        parameters.put("Test", (Object) avaiableTestsJSONArray.getJSONObject(j));
                        Log.d("elogika","byl nalezen testID " + testID);
                    }
                }
            }
            // parameters
            parameters.put("StartAt", TimeLogika.currentServerTime() );
            parameters.put("IPAddress", URL.getLocalIpAddress());
            parameters.put("StudentId", User.id);
            parameters.put("CourseInfoId", User.courseInfoId);
        }
        catch (JSONException e) {Log.d("elogika","exception at fragmenttakeatest/generatetest" + e.toString());}

        Log.d("generate test request",parameters.toString());
        Request.post(URL.GeneratedTest.generateTest(), parameters, response);
    }


    int readyCounter = 0;
    int readyCounterLenght = 0;

    /**
     * metoda pro získání ID otázek a odpovídajících ID odpovědí
     *
     * @param testID id testu
     * @param generatedTestId id vygenerovaného testu z předcházející metody
     * @param Offline boolean jedná-li se o offline test
     * @param timeMinutes
     *
     */
    private void getTest(final int testID, final int generatedTestId, final boolean Offline, final int timeMinutes) {
        //zjistim čisla otazek a k nim čisla odpovědi
        ArrayResponse r = new ArrayResponse() {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                progressPercentage.setText(2 + " %");
                Log.w("generatedquestions", response.toString());
                Taking_a_test.testInfo = response;
                Taking_a_test.jsonQuestions = new JSONObject[response.length()];
                Taking_a_test.jsonQuestionOptions = new JSONObject[response.length()][];
                Taking_a_test.timeMinutes = timeMinutes;
                if (response.length() > 0) {
                    readyCounter = 2*response.length();
                    readyCounterLenght = 2*response.length();
                    for (int poradi = 0; poradi < response.length(); poradi++) {
                        int IDVygenerovanaOtazka = Integer.parseInt(response.getJSONObject(poradi).getString("IDVygenerovanaOtazka"));
                        int IDOtazka = Integer.parseInt(response.getJSONObject(poradi).getString("IDOtazka"));
                        Log.d("elogika", "čisla otazek: " + IDVygenerovanaOtazka);
                        getQuestionText(generatedTestId,poradi, IDVygenerovanaOtazka,Offline);
                        getQuestionOptions(generatedTestId,poradi, IDVygenerovanaOtazka,Offline);
                    }
                }else
                {
                    Log.d("Fragment_OnlineTest_list", "prazdny test");
                    SnackbarManager.show(
                            Snackbar.with(aktivita)
                                    .text(aktivita.getString(R.string.this_test_has_no_questions)));
                    //Toast.makeText(aktivita, aktivita.getString(R.string.this_test_has_no_questions), Toast.LENGTH_SHORT).show();
                    myDialog.setCancelable(true);
                }

                if(Offline) // jenom najdu odpovídající informace ze všech testů jako jsou jmeno a čas
                    for (int j = 0; j < avaiableTestsJSONArray.length(); j++) {
                        if(testID == avaiableTestsJSONArray.getJSONObject(j).getInt("TestId"))
                        {
                            Taking_a_test.testName = avaiableTestsJSONArray.getJSONObject(j).getString("Name");
                            BaseActivity.getDB().insertTest(
                                    generatedTestId,
                                    testID,
                                    timeMinutes,
                                    avaiableTestsJSONArray.getJSONObject(j).getString("Name"),
                                    avaiableTestsJSONArray.getJSONObject(j).getString("GroupActivityName"),
                                    avaiableTestsJSONArray.getJSONObject(j).getString("DateAvailableFrom"),
                                    avaiableTestsJSONArray.getJSONObject(j).getString("DateAvailableTo"),
                                    avaiableTestsJSONArray.getJSONObject(j).getString("DateSendTo"),
                                    avaiableTestsJSONArray.getJSONObject(j).getString("Min"),
                                    avaiableTestsJSONArray.getJSONObject(j).getString("Max"),
                                    avaiableTestsJSONArray.getJSONObject(j).getBoolean("Required"),
                                    response
                            );
                        }
                    }
            }

        };
        Request.get(URL.GeneratedTest.generatedquestions(generatedTestId), r);
    }


    /**
     * metoda pro získání textu otázek podle zadaných IDček
     *
     * @param generatedTestId id vygenerovaného testu z generateTest()
     * @param poradi pořadí otázky
     * @param IDVygenerovanaOtazka id vygenerované otázky z getTest()
     * @param offline boolean jedná-li se o offline test
     *
     */
    private void getQuestionText(final int generatedTestId ,final int poradi,final int IDVygenerovanaOtazka, final boolean offline)
    {       //stahnu zneni otazek
        Response res = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                Log.w("getQuestionText " + IDVygenerovanaOtazka, response.toString());
                if (response.length() > 0) {
                    Taking_a_test.jsonQuestions[poradi] = response;
                    if (offline)
                    {
                        BaseActivity.getDB().insertQuestion(generatedTestId, poradi, response);
                    }
                }
                else{
                    SnackbarManager.show(
                            Snackbar.with(aktivita)
                                    .text(aktivita.getString(R.string.no_questionText) + " idVygenerovaneOtazky: " + IDVygenerovanaOtazka));
                }
                checkReady(offline);
            }


        };
        Request.get(URL.GeneratedTest.getgeneratedquestionbyid(IDVygenerovanaOtazka), res);
    }


    /**
     * metoda pro získání textu odpovědí podle zadaných IDček
     *
     * @param generatedTestId id vygenerovaného testu z generateTest()
     * @param poradi pořadí otázky
     * @param IDVygenerovanaOtazka id vygenerované otázky z getTest()
     * @param offline boolean jedná-li se o offline test
     *
     */
    private void getQuestionOptions(final int generatedTestId,final int poradi, final int IDVygenerovanaOtazka, final boolean offline) {
        //stahnu možnosti pro otazky
        ArrayResponse r = new ArrayResponse() {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                Log.w("getQuestionOptions", response.toString());
                Taking_a_test.jsonQuestionOptions[poradi]= new JSONObject[response.length()];
                if (response.length() > 0) {
                    for (int poradiodpovedi = 0; poradiodpovedi < response.length(); poradiodpovedi++) {
                        Taking_a_test.jsonQuestionOptions[poradi][poradiodpovedi] = response.getJSONObject(poradiodpovedi);
                        if (offline)
                        {
                            BaseActivity.getDB().insertOption(generatedTestId, poradi, poradiodpovedi, response.getJSONObject(poradiodpovedi) );
                        }
                    }
                }
                else{
                    SnackbarManager.show(
                            Snackbar.with(aktivita)
                                    .text(aktivita.getString(R.string.no_questionOptions) + " idVygenerovaneOtazky: " + IDVygenerovanaOtazka));
                }
                checkReady(offline);
            }
        };
        Request.get(URL.GeneratedTest.generatedanswers(IDVygenerovanaOtazka), r);
    }

    /**
     * kontroluje jestli přišli všechny odpovědi a poté umožní spustit test
     *
     * @param offline boolean jedná-li se o offline test
     */
    private void checkReady(boolean offline) {
        readyCounter--;

        float procenta = ((float)(readyCounterLenght-readyCounter)/readyCounterLenght)*100;
        progressPercentage.setText(procenta + " %");

        if (readyCounter ==0){
            myDialog.setCancelable(true);
            Log.d("checkready", "ready");
            progressBar.setVisibility(View.GONE);

          //  buttonStartTest.setVisibility(View.VISIBLE);
            myDialog.cancel();
            if(offline)
            {
                SnackbarManager.show(
                        Snackbar.with(getActivity())
                                .text(Taking_a_test.testName + " " + aktivita.getString(R.string.can_be_taken_offline))
                );
            }
            else{
            Intent intent = new Intent(aktivita, Taking_a_test.class);
            aktivita.startActivity(intent);
            }

        }
    }

    @Override
    public void onRefresh() {
        nothinglayout.setVisibility(View.GONE);
        getAvaiableTest(-1, -1);
        if(swipeLayout != null) {
            swipeLayout.post(new Runnable() {
                @Override
                public void run() {
                    swipeLayout.setRefreshing(true);
                }
            });
        }
    }
}



