package cz.vsb.elogika.dataTypes;

public class Question {

    public int IDOtazka;
    public int IDVygenerovanyTest;
    public int TypOdpovedi;
    public int IDVygenerovanaOtazka;
    public int IDZarazeni;
    public int IDKategorie;
    public int IDUzivatel;
    public int IDRole;
    public String Zadani;
    public int PredpCasPrecteni;
    public boolean Smazana;
    public int IDBlok;
    public String DatumEvidence;
    public int IDUzivatelKontrola;
    public String DatumKontrola;
    public boolean Prekontrolovano;
    public boolean Vlastnik;
    public String KategorieNazev;
    public String KapitolaNazev;
    public int Variant;

    public int obtiznost = -1;


    public String createdText;
    public String checkedText;

    @Override
    public String toString() {
        return Zadani;
    }

}
