package cz.vsb.elogika.ui.fragments.garant.courseConditions;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.ui.fragments.garant.testsAndAcvtivities.Fragment_TestsAndActivity;
import cz.vsb.elogika.utils.DividerItemDecoration;

public class Fragment_cc_pc extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private View rootView;
    private RecyclerView recyclerView;

    LinearLayout nothinglayout;
    SwipeRefreshLayout swipeLayout;

    private int form;
    protected RecyclerView.Adapter adapter;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.recycler_view_with_button_swipe, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);

        swipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_update);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeResources(R.color.darker_cyan, R.color.material_blue_grey_800);

        nothinglayout = (LinearLayout) rootView.findViewById(R.id.nothing_to_show);


        Bundle bundle=getArguments();
        form = bundle.getInt("study_form");


        this.adapter = new RecyclerView.Adapter() {
            class ViewHolderHeader extends RecyclerView.ViewHolder {
                public ViewHolderHeader(View itemView) {
                    super(itemView);
                }
            }

            class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
                public TextView name;
                public TextView min;
                public TextView max;
                public TextView mandatory;
                public TextView include_to_result_detail;
                //public Button btn;
                ArrayList<Map<String, String>> list;
                //public TextView study_form_detail;

                public ViewHolder(View itemView) {
                    super(itemView);
                    itemView.setOnClickListener(this);
                    /*this.btn = (Button) itemView.findViewById(R.id.cc_button);*/
                    this.name = (TextView) itemView.findViewById(R.id.cc_name);
                    this.min = (TextView) itemView.findViewById(R.id.cc_min);
                    this.max = (TextView) itemView.findViewById(R.id.cc_max);
                    this.mandatory = (TextView) itemView.findViewById(R.id.cc_mandatory);
                    this.include_to_result_detail = (TextView) itemView.findViewById(R.id.cc_include_to_result_detail);
                    //this.study_form_detail = (TextView) itemView.findViewById(R.id.cc_study_form_detail);
                }


                @Override
                public void onClick(View view) {

                    final int position = getAdapterPosition()-1;
                    if(User.isTutor){
                        if (form == 0) {
                            toFragment_cc_summary(Fragment_CourseConditions.course_conditions_list.get(position), form + 1);
                            return;
                        }
                        else {
                            toFragment_cc_summary(Fragment_CourseConditions.course_conditions_combined_list.get(position), form + 1);
                            return;
                        }
                    }
                    else if(!User.isGarant){
                        return;
                    }

                    final Dialog dialog = new Dialog(getActivity());
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.fragment_course_conditions_detail);
                    /*
                        TextView cc_title = (TextView) dialog.findViewById(R.id.cc_title);
                        cc_title.setText(course_conditions_list.get(position).get("nazev"));
                        TextView cc_minimum = (TextView) dialog.findViewById(R.id.cc_minimum);
                        cc_minimum.setText(course_conditions_list.get(position).get("minimum"));
                        TextView cc_maximum = (TextView) dialog.findViewById(R.id.cc_maximum);
                        cc_maximum.setText(course_conditions_list.get(position).get("maximum"));
                        TextView cc_mandatory = (TextView) dialog.findViewById(R.id.cc_mandatory);
                        cc_mandatory.setText(course_conditions_list.get(position).get("mandatory"));
                        TextView cc_include_to_result = (TextView) dialog.findViewById(R.id.cc_include_to_result);
                        cc_include_to_result.setText(course_conditions_list.get(position).get("zapocitatDoVysledku"));
                        TextView cc_study_form = (TextView) dialog.findViewById(R.id.cc_study_form);
                        cc_study_form.setText(course_conditions_list.get(position).get("formaStudia"));
                    */
                    Button btn_term_list = (Button) dialog.findViewById(R.id.btn_cc_term_list);
                    btn_term_list.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //pokud se jedna o prezencni formu...
                            if (form == 0)
                                toFragment_cc_term(Fragment_CourseConditions.course_conditions_list.get(position), form + 1);
                            else
                                toFragment_cc_term(Fragment_CourseConditions.course_conditions_combined_list.get(position), form + 1);
                            dialog.dismiss();
                        }
                    });

                    Button btn_cc_add_term = (Button) dialog.findViewById(R.id.btn_cc_add_term);
                    btn_cc_add_term.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (form == 0)
                                toFragment_cc_add_term(Fragment_CourseConditions.course_conditions_list.get(position), form + 1);
                            else
                                toFragment_cc_add_term(Fragment_CourseConditions.course_conditions_combined_list.get(position), form + 1);
                            dialog.dismiss();
                        }
                    });

                    Button btn_cc_summary = (Button) dialog.findViewById(R.id.btn_cc_summary);
                    btn_cc_summary.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (form == 0)
                                toFragment_cc_summary(Fragment_CourseConditions.course_conditions_list.get(position), form + 1);
                            else
                                toFragment_cc_summary(Fragment_CourseConditions.course_conditions_combined_list.get(position), form + 1);
                            dialog.dismiss();
                        }
                    });

                    Button btn_cc_tests_and_activity = (Button) dialog.findViewById(R.id.btn_cc_tests_and_activity);
                    btn_cc_tests_and_activity.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (form == 0)
                                toFragment_cc_tests_and_activity(Fragment_CourseConditions.course_conditions_list.get(position), form + 1);
                            else
                                toFragment_cc_tests_and_activity(Fragment_CourseConditions.course_conditions_combined_list.get(position), form + 1);
                            dialog.dismiss();
                        }
                    });

                    Button btn_cc_edit = (Button) dialog.findViewById(R.id.btn_cc_edit);
                    btn_cc_edit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (form == 0)
                                toFragment_cc_edit(Fragment_CourseConditions.course_conditions_list.get(position), form + 1);
                            else
                                toFragment_cc_edit(Fragment_CourseConditions.course_conditions_combined_list.get(position), form + 1);
                            dialog.dismiss();
                        }
                    });

                    Button btn_cc_delete = (Button) dialog.findViewById(R.id.btn_cc_delete);
                    btn_cc_delete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setTitle(R.string.activity_group_delete);
                            builder.setCancelable(false);
                            builder.setMessage(R.string.activity_group_delete_message);
                            builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog2, int which) {
                                    if (form == 0)
                                        delete(Fragment_CourseConditions.course_conditions_list.get(position));
                                    else
                                        delete(Fragment_CourseConditions.course_conditions_combined_list.get(position));

                                    dialog.dismiss();
                                }
                            });

                            builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog2, int which) {
                                    ;
                                }
                            }).setIcon(android.R.drawable.ic_dialog_alert);
                            Dialog d = builder.create();
                            d.show();
                        }
                    });
                    dialog.show();
                }


            }

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view;
                switch (viewType) {
                    case 0:
                        view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_course_conditions_list_header, parent, false);
                        return new ViewHolderHeader(view);
                    case 1:
                        view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_course_conditions_list, parent, false);
                        return new ViewHolder(view);
                    default:
                        return null;
                }
            }
            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
                if (holder.getItemViewType() == 1) {
                    //kvuli hlavicce
                    position--;
                    final ViewHolder viewHolder = (ViewHolder) holder;

                    //pokud se jedna o prezencni formu...
                    if(form == 0) {
                        viewHolder.list = Fragment_CourseConditions.course_conditions_list;
                    }
                    else {
                        viewHolder.list = Fragment_CourseConditions.course_conditions_combined_list;
                    }
                    viewHolder.name.setText(viewHolder.list.get(position).get("nazev"));
                    viewHolder.min.setText(viewHolder.list.get(position).get("minimum"));
                    viewHolder.max.setText(viewHolder.list.get(position).get("maximum"));
                    if(viewHolder.list.get(position).get("mandatory").equals("true")){
                        viewHolder.mandatory.setText(getString(R.string.yes));
                    }
                    else{
                        viewHolder.mandatory.setText(getString(R.string.no));
                    }
                    if(viewHolder.list.get(position).get("zapocitatDoVysledku").equals("true")){
                        viewHolder.include_to_result_detail.setText(getString(R.string.yes));
                    }
                    else{
                        viewHolder.include_to_result_detail.setText(getString(R.string.no));
                    }
                    //viewHolder.study_form_detail.setText(list.get(position).get("formaStudia"));
                   /* viewHolder.btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                        }
                    });*/
                }
            }

            @Override
            public int getItemViewType(int position)
            {
                //hlavicka tabulky
                if (position == 0)
                    return 0;
                    //tabulka
                else
                    return 1;
            }

            @Override
            public int getItemCount() {
                //velikost listu pro vyplneni tabulky + 1 pro nadpis tabulky
                if(form == 0) {
                    return Fragment_CourseConditions.course_conditions_list.size() + 1;
                }
                else {
                    return Fragment_CourseConditions.course_conditions_combined_list.size() + 1;
                }
            }
        };

        Button roundButton = (Button) rootView.findViewById(R.id.roundButton);
        roundButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle params = new Bundle();
                //0 == prezencni, 1 == kombinovana forma
                params.putInt("study_form", form);
                Fragment_cc_add_ag fragment_cc_add_ag = new Fragment_cc_add_ag();
                fragment_cc_add_ag.setArguments(params);
                getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment_cc_add_ag).commit();
            }
        });


        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        recyclerView.setHasFixedSize(true);
        //zaruci, ze se bude seznam aktualizovat jen pri swipu uplne nahore...
        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy <= 0) {
                    swipeLayout.setEnabled(true);
                } else {
                    swipeLayout.setEnabled(false);
                }
            }
        });
        recyclerView.setAdapter(adapter);
        return rootView;
    }


    public void start_update(){
        swipeLayout.post(new Runnable() {
            @Override
            public void run() {
                nothinglayout.setVisibility(View.GONE);
                swipeLayout.setRefreshing(true);
            }
        });
    }
    public void update() {
        if(form == 0){
            if(Fragment_CourseConditions.course_conditions_list.isEmpty()) {
                nothinglayout.setVisibility(View.VISIBLE);
            }
        }
        else{
            if(Fragment_CourseConditions.course_conditions_combined_list.isEmpty()) {
                nothinglayout.setVisibility(View.VISIBLE);
            }
        }
        adapter.notifyDataSetChanged();
        swipeLayout.setRefreshing(false);
    }


    private void toFragment_cc_tests_and_activity(Map<String, String> list, int tab) {
        Bundle bundle = new Bundle();
        bundle.putString("idSkupinaAktivit", list.get("idSkupinaAktivit"));
        bundle.putString("nazevSA", list.get("nazev"));
        //pak se vrati sem...
        bundle.putString("tab", tab + "");
        Fragment_TestsAndActivity fragment__tests_and_activity = new Fragment_TestsAndActivity();
        fragment__tests_and_activity.setArguments(bundle);
        getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment__tests_and_activity).commit();
    }

    private void toFragment_cc_term(Map<String, String> list, int tab) {
        Bundle bundle = new Bundle();
        bundle.putString("idSkupinaAktivit", list.get("idSkupinaAktivit"));
        bundle.putString("aktivitaNazev", list.get("nazev"));
        //pak se vrati sem...
        bundle.putString("tab", tab + "");
        Fragment_cc_term fragment_cc_term = new Fragment_cc_term();
        fragment_cc_term.setArguments(bundle);
        getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment_cc_term).commit();
    }

    private void toFragment_cc_add_term(Map<String, String> list, int tab) {
        Bundle bundle = new Bundle();
        bundle.putString("idSkupinaAktivit", list.get("idSkupinaAktivit"));
        bundle.putString("aktivitaNazev", list.get("nazev"));
        //pak se vrati sem...
        bundle.putString("tab", tab + "");
        Fragment_cc_add_term fragment_cc_add_term = new Fragment_cc_add_term();
        fragment_cc_add_term.setArguments(bundle);
        getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment_cc_add_term).commit();
    }

    private void toFragment_cc_summary(Map<String, String> list, int tab) {
        Bundle bundle = new Bundle();
        bundle.putString("idSkupinaAktivit", list.get("idSkupinaAktivit"));
        //pak se vrati sem...
        bundle.putString("tab", tab + "");
        Fragment_cc_summary fragment_cc_summary = new Fragment_cc_summary();
        fragment_cc_summary.setArguments(bundle);
        getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment_cc_summary).commit();
    }

    private void toFragment_cc_edit(Map<String, String> list, int tab) {
        Bundle bundle = new Bundle();
        bundle.putString("idSkupinaAktivit", list.get("idSkupinaAktivit"));
        bundle.putString("pocetPokusu", list.get("pocetPokusu"));
        bundle.putString("nazev", list.get("nazev"));
        bundle.putString("idRole", list.get("idRole"));
        bundle.putString("idKurzInfo", list.get("idKurzInfo"));
        bundle.putString("minimum", list.get("minimum"));
        bundle.putString("maximum", list.get("maximum"));
        bundle.putString("mandatory", list.get("mandatory"));
        bundle.putString("zapocitatDoVysledku", list.get("zapocitatDoVysledku"));
        bundle.putString("formaStudia", list.get("formaStudia"));
        bundle.putString("nulovat", list.get("nulovat"));
        //pak se vrati sem...
        bundle.putString("tab", tab + "");
        Fragment_cc_update_ag fragment_cc_update_ag = new Fragment_cc_update_ag();
        fragment_cc_update_ag.setArguments(bundle);
        getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment_cc_update_ag).commit();
    }


    private void delete(final Map<String, String> list) {
        Response response = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException {
                Log.d("typ response: ", response.getString("Success"));
                if (response.getString("Success").equals("true")) {
                    //novej toast
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.activity_group) + " " + list.get("nazev") + " " + getActivity().getString(R.string.activity_group_was_deleted)));

                } else { //nepovedlo se
                    Log.d("Fragment_cc_pc", "delete(): " + response.toString());
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.activity_group) + " " + list.get("nazev") + " " + getActivity().getString(R.string.activity_group_was_not_deleted)));
                }
                Fragment_CourseConditions.reference.Data_into_course_list();
            }
        };
        Request.get(URL.GroupActivities.Remove(list.get("idSkupinaAktivit")), response);
    }

    @Override
    public void onRefresh() {
        Fragment_CourseConditions.reference.Data_into_course_list();
    }
}
