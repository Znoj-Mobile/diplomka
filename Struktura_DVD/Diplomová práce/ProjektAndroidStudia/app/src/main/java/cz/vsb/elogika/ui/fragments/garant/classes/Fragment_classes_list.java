package cz.vsb.elogika.ui.fragments.garant.classes;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.DatePickerDialogOnButton;
import cz.vsb.elogika.utils.DividerItemDecoration;
import cz.vsb.elogika.utils.EnumLogika;
import cz.vsb.elogika.utils.TimePickerDialogOnButton;

public class Fragment_classes_list extends Fragment implements SwipeRefreshLayout.OnRefreshListener{
    private View mainView;
    private View rootView;
    private RecyclerView recyclerView;


    SwipeRefreshLayout swipeLayout;


    protected RecyclerView.Adapter adapter;


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mainView = inflater.inflate(R.layout.fragment_classes_list, container, false);
        rootView = mainView.findViewById(R.id.recycler_view_with_button_swipe);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);

        swipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_update);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeResources(R.color.darker_cyan, R.color.material_blue_grey_800);

        this.adapter = new RecyclerView.Adapter() {
            class ViewHolderHeader extends RecyclerView.ViewHolder {
                public ViewHolderHeader(View itemView) {
                    super(itemView);
                }
            }

            class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
                public TextView plus;
                public TextView teacher;
                public TextView lecture_room;
                public TextView lesson;
                public TextView day;
                public TextView week;
                public TextView study_form;
                public TextView class_type;
                public Button btn_students;
                public Button btn_import;
                public Button btn_export;
                public Button btn_edit;
                public Button btn_delete;

                public ViewHolder(View itemView) {
                    super(itemView);
                    itemView.setOnClickListener(this);
                    this.plus = (TextView) itemView.findViewById(R.id.c_l_plus);
                    this.teacher = (TextView) itemView.findViewById(R.id.c_l_teacher);
                    this.lecture_room = (TextView) itemView.findViewById(R.id.c_l_lecture_room);
                    this.lesson = (TextView) itemView.findViewById(R.id.c_l_leasson);
                    this.day = (TextView) itemView.findViewById(R.id.c_l_day);
                    this.week = (TextView) itemView.findViewById(R.id.c_l_week);
                    this.study_form = (TextView) itemView.findViewById(R.id.c_l_study_form);
                    this.class_type = (TextView) itemView.findViewById(R.id.c_l_class_type);
                    this.btn_students = (Button)  itemView.findViewById(R.id.c_l_students);
                    this.btn_import = (Button) itemView.findViewById(R.id.c_l_import);
                    this.btn_export = (Button) itemView.findViewById(R.id.c_l_export);
                    this.btn_edit = (Button) itemView.findViewById(R.id.c_l_edit);
                    this.btn_delete = (Button) itemView.findViewById(R.id.c_l_delete);
                }


                @Override
                public void onClick(View view) {
                    int position = getLayoutPosition();
                    if(Fragment_Classes.lesson_list.get(position).get("combinedClasses").equals("true")){
                        //preskoceni hlavicky
                        position+=2;
                        while(position != Fragment_Classes.lesson_list.size()){
                            if(Fragment_Classes.lesson_list.get(position).get("idTrida").equals("")){
                                Fragment_Classes.lesson_list.get(position).put("idTrida", "visible");
                            }
                            else if(Fragment_Classes.lesson_list.get(position).get("idTrida").equals("visible")){
                                Fragment_Classes.lesson_list.get(position).put("idTrida", "");
                            }
                            else{
                                break;
                            }
                            position++;
                        }
                        adapter.notifyDataSetChanged();
                    }
                }
            }

            class ViewHolder_combinate extends RecyclerView.ViewHolder implements View.OnClickListener{
                public TextView name;
                public TextView leasson;
                public TextView day;
                public TextView lecture_room;
                public TextView date;

                public ViewHolder_combinate(View itemView) {
                    super(itemView);
                    itemView.setOnClickListener(this);
                    this.name = (TextView) itemView.findViewById(R.id.c_l_c_name);
                    this.leasson = (TextView) itemView.findViewById(R.id.c_l_c_leasson);
                    this.day = (TextView) itemView.findViewById(R.id.c_l_c_day);
                    this.lecture_room = (TextView) itemView.findViewById(R.id.c_l_c_lecture_room);
                    this.date = (TextView) itemView.findViewById(R.id.c_l_c_date);
                }


                @Override
                public void onClick(View view) {
                }
            }

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view;
                switch (viewType) {
                    case 0:
                        view = LayoutInflater.from(getActivity()).inflate(R.layout.invisible_layout, parent, false);
                        return new ViewHolder(view);
                    case 1:
                        view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_classes_list_list, parent, false);
                        return new ViewHolder(view);
                    case 2:
                        view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_classes_list_combinate_head, parent, false);
                        return new ViewHolder_combinate(view);
                    case 3:
                        view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_classes_list_combinate_list, parent, false);
                        return new ViewHolder_combinate(view);
                    default:
                        return null;
                }
            }
            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
                if (holder.getItemViewType() == 1) {
                    final ViewHolder viewHolder = (ViewHolder) holder;

                    if(Fragment_Classes.lesson_list.get(position).get("combinedClasses").equals("true")){
                        viewHolder.plus.setText("+");
                    }
                    else{
                        viewHolder.plus.setText(" ");
                    }
                    String tutor = Fragment_Classes.lesson_list.get(position).get("tutorName") + " " + Fragment_Classes.lesson_list.get(position).get("tutorSurname");
                    viewHolder.teacher.setText(tutor);
                    viewHolder.lecture_room.setText(Fragment_Classes.lesson_list.get(position).get("ucebna"));
                    String lesson = Fragment_Classes.lesson_list.get(position).get("hodinaOD") + " - " + Fragment_Classes.lesson_list.get(position).get("hodinaDO");
                    viewHolder.lesson.setText(lesson);
                    viewHolder.day.setText(Fragment_Classes.lesson_list.get(position).get("den"));
                    viewHolder.week.setText(Fragment_Classes.lesson_list.get(position).get("tyden"));
                    viewHolder.study_form.setText(EnumLogika.formaStudia(Fragment_Classes.lesson_list.get(position).get("formaStudia")));
                    viewHolder.class_type.setText(EnumLogika.typTridy(Fragment_Classes.lesson_list.get(position).get("typ")));

                    final int finalPosition = position;

                    if(User.isTutor){
                        if(!Fragment_Classes.lesson_list.get(position).get("idTutor").equals(String.valueOf(User.id))){
                            viewHolder.btn_students.setEnabled(false);
                            viewHolder.btn_import.setEnabled(false);
                            viewHolder.btn_export.setEnabled(false);
                            viewHolder.btn_edit.setEnabled(false);
                            viewHolder.btn_delete.setEnabled(false);
                        }
                    }

                    viewHolder.btn_students.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Bundle bundle = new Bundle();
                            bundle.putString("nazev", Fragment_Classes.lesson_list.get(finalPosition).get("nazev"));
                            bundle.putString("classId", Fragment_Classes.lesson_list.get(finalPosition).get("idTrida"));
                            Fragment_classes_list_of_students fragment_classes_list_of_students = new Fragment_classes_list_of_students();
                            fragment_classes_list_of_students.setArguments(bundle);
                            getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment_classes_list_of_students).commit();
                        }
                    });

                    viewHolder.btn_edit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Bundle bundle = new Bundle();

                            ArrayList<Map<String, String>> combinated_list = new ArrayList<>();
                            if(Fragment_Classes.lesson_list.get(finalPosition).get("combinedClasses").equals("true")){
                                int positionWhile = finalPosition + 2;
                                while(positionWhile != Fragment_Classes.lesson_list.size()){
                                    if(Fragment_Classes.lesson_list.get(positionWhile).get("idTrida").equals("") || Fragment_Classes.lesson_list.get(positionWhile).get("idTrida").equals("visible")){
                                        combinated_list.add(Fragment_Classes.lesson_list.get(positionWhile));
                                    }
                                    else{
                                        break;
                                    }
                                    positionWhile++;
                                }
                            }
                            bundle.putSerializable("list", (Serializable) Fragment_Classes.lesson_list.get(finalPosition));
                            bundle.putSerializable("combinated_list", combinated_list);
                            Fragment_classes_add fragment_classes_add = new Fragment_classes_add();
                            fragment_classes_add.setArguments(bundle);
                            getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment_classes_add).commit();
                        }
                    });

                    viewHolder.btn_delete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setTitle(R.string.leasson_delete);
                            builder.setCancelable(false);
                            builder.setMessage(R.string.leasson_delete_message);
                            builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog2, int which) {
                                    delete(Fragment_Classes.lesson_list.get(finalPosition).get("idTrida"), Fragment_Classes.lesson_list.get(finalPosition).get("nazev"));
                                }
                            });

                            builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog2, int which) {
                                    ;
                                }
                            }).setIcon(android.R.drawable.ic_dialog_alert);
                            Dialog d = builder.create();
                            d.show();
                        }
                    });
                }
                else if(holder.getItemViewType() == 3){
                    final ViewHolder_combinate viewHolder = (ViewHolder_combinate) holder;
                    viewHolder.name.setText(Fragment_Classes.lesson_list.get(position).get("nazev"));
                    viewHolder.leasson.setText(Fragment_Classes.lesson_list.get(position).get("hodinaOD") + " - " + Fragment_Classes.lesson_list.get(position).get("hodinaDO"));
                    viewHolder.day.setText(Fragment_Classes.lesson_list.get(position).get("den"));
                    viewHolder.lecture_room.setText(Fragment_Classes.lesson_list.get(position).get("ucebna"));
                    viewHolder.date.setText(Fragment_Classes.lesson_list.get(position).get("datum"));
                }
            }

            @Override
            public int getItemViewType(int position)
            {
                if (Fragment_Classes.lesson_list.get(position) == null){
                    if(position+1 != Fragment_Classes.lesson_list.size()){
                        if(Fragment_Classes.lesson_list.get(position+1).get("idTrida").equals("visible")){
                            //ukaz
                            return 2;
                        }
                    }
                    //schovej
                    return 0;
                }
                else if(Fragment_Classes.lesson_list.get(position).get("idTrida").equals("")){
                    //schovej
                    return 0;
                }
                else if(Fragment_Classes.lesson_list.get(position).get("idTrida").equals("visible")){
                    //ukaz
                    return 3;
                }
                return 1;
            }

            @Override
            public int getItemCount() {
                return Fragment_Classes.lesson_list.size();
            }
        };

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        recyclerView.setHasFixedSize(true);
        //zaruci, ze se bude seznam aktualizovat jen pri swipu uplne nahore...
        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy <= 0) {
                    swipeLayout.setEnabled(true);
                } else {
                    swipeLayout.setEnabled(false);
                }
            }
        });
        recyclerView.setAdapter(adapter);

        Fragment_Classes.update(adapter, swipeLayout);

        //todo predelat id a tlacitko
        Button time_limit_for_changing_class = (Button) mainView.findViewById(R.id.button);
        time_limit_for_changing_class.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                change_time_limit_for_changing_class();
            }
        });

        Button round_button = (Button) rootView.findViewById(R.id.roundButton);
        round_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                insert_class();
            }
        });
        return mainView;
    }

    private void change_time_limit_for_changing_class() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setCancelable(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.fragment_classes_time_for_change);

        LinearLayout nothinglayout_dialog = (LinearLayout) dialog.findViewById(R.id.nothing_to_show);
        nothinglayout_dialog.setVisibility(View.GONE);

        ProgressBar progressDialog = (ProgressBar) dialog.findViewById(R.id.loading_progress);
        progressDialog.setVisibility(View.VISIBLE);

        //zapis od
        final DatePickerDialogOnButton start_login_to_classes_date = new DatePickerDialogOnButton((Button) dialog.findViewById(R.id.tfc_start_login_to_classes_date));
        final TimePickerDialogOnButton start_login_to_classes_time = new TimePickerDialogOnButton((Button) dialog.findViewById(R.id.tfc_start_login_to_classes_time));
        //start_login_to_classes_date.setDateTime(start_login_to_classes_time, cc_term_list.get("zapisOD"));

        //zapis do
        final DatePickerDialogOnButton end_login_to_classes_date = new DatePickerDialogOnButton((Button) dialog.findViewById(R.id.tfc_end_login_to_classes_date));
        final TimePickerDialogOnButton end_login_to_classes_time = new TimePickerDialogOnButton((Button) dialog.findViewById(R.id.tfc_end_login_to_classes_time));
        //end_login_to_classes_date.setDateTime(end_login_to_classes_time, cc_term_list.get("zapisOD"));

        get_limits(start_login_to_classes_date, start_login_to_classes_time, end_login_to_classes_date, end_login_to_classes_time, nothinglayout_dialog, progressDialog);

        Button set_limits = (Button) dialog.findViewById(R.id.tfc_set_limits);
        set_limits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set_limits(start_login_to_classes_date, start_login_to_classes_time, end_login_to_classes_date, end_login_to_classes_time);
                dialog.dismiss();
            }
        });

        Button back = (Button) dialog.findViewById(R.id.tfc_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void get_limits(final DatePickerDialogOnButton start_login_to_classes_date, final TimePickerDialogOnButton start_login_to_classes_time, final DatePickerDialogOnButton end_login_to_classes_date, final TimePickerDialogOnButton end_login_to_classes_time, final LinearLayout nothinglayout_dialog, final ProgressBar progressDialog) {
        final Response response = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException {
                if (response.length() > 0) {
                    if(!response.getString("FromDate").equals("")) {
                        start_login_to_classes_date.setDateTime(start_login_to_classes_time, response.getString("FromDate"));
                    }
                    else{
                        SnackbarManager.show(
                                Snackbar.with(getActivity())
                                        .text(getActivity().getString(R.string.time_limit_for_changing_class) + " " + getActivity().getString(R.string.was_not_set_3_person)));
                    }
                    if(!response.getString("ToDate").equals("")) {
                        end_login_to_classes_date.setDateTime(end_login_to_classes_time, response.getString("ToDate"));
                    }
                    else{
                        SnackbarManager.show(
                                Snackbar.with(getActivity())
                                        .text(getActivity().getString(R.string.time_limit_for_changing_class) + " " + getActivity().getString(R.string.was_not_set_3_person)));
                    }
                } else {
                    Log.d("Chyba", "Taa get_generatedtests");
                    nothinglayout_dialog.setVisibility(View.VISIBLE);
                }
                progressDialog.setVisibility(View.GONE);
            }
        };
        Request.get(URL.Course.GetClassTimeLimitsFromTo(User.courseInfoId), response);
    }

    private void set_limits(DatePickerDialogOnButton start_login_to_classes_date, TimePickerDialogOnButton start_login_to_classes_time, DatePickerDialogOnButton end_login_to_classes_date, TimePickerDialogOnButton end_login_to_classes_time) {
        Response response = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException {
                if (response.getString("Success").equals("true")) {
                    //novej toast
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.time_limit_for_changing_class) + " " + getActivity().getString(R.string.time_limit_was_updated)));

                } else { //nepovedlo se
                    Log.d("Fragment_cc", "set_limits(): " + response.toString());
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.time_limit_for_changing_class) + " " + getActivity().getString(R.string.time_limit_was_not_updated)));
                }
            }
        };

        JSONObject parameters = new JSONObject();
        try {

            parameters.put("zapisOD", start_login_to_classes_date.getDate(start_login_to_classes_time));
            parameters.put("zapisDO", end_login_to_classes_date.getDate(end_login_to_classes_time));

        } catch (JSONException e) {
            Log.d("Fragment_classes_list", "set_limits() param: " + e.getMessage());
        }

        Request.post(URL.Course.SetClassTimeLimits(User.courseInfoId), parameters, response);
    }

    private void insert_class() {
        Fragment_classes_add fragment_classes_add = new Fragment_classes_add();
        getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment_classes_add).commit();
    }


    private void delete(String id, final String name) {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                if (response.getString("Success").equals("true")) {
                    //novej toast
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.leasson) + " " + name + " " + getActivity().getString(R.string.leasson_was_deleted)));
                    Fragment_Classes.update(adapter, swipeLayout);
                }
                else{ //nepovedlo se
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.leasson_4p) + " " + name + " " + getActivity().getString(R.string.leasson_was_not_deleted)));
                }
            }
        };

        Request.get(URL.SchoolClass.Remove(id), response);
    }

    @Override
    public void onRefresh() {
        Fragment_Classes.update(adapter, swipeLayout);
    }
}
