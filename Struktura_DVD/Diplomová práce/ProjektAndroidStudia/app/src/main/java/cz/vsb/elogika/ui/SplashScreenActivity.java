package cz.vsb.elogika.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.ShortBufferException;

import cz.vsb.elogika.R;
import cz.vsb.elogika.utils.AES;
import cz.vsb.elogika.utils.SelectFileToUpload;

public class SplashScreenActivity extends BaseActivity {

    private static final long SCREEN_TIMEOUT = 1500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        setContentView(R.layout.activity_splash_screen);

        //findViewById(R.id.splashbox).startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.boxslide));

        // go to login page after specified delay
        showLoginScreen(SCREEN_TIMEOUT);


    }



    /**
     * Shows Login Screen.
     */
    private void showLoginScreen(final long delay) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            }
        }, delay);
    }
}
