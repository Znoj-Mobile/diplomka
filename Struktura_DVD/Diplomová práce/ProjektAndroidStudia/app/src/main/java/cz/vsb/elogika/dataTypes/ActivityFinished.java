package cz.vsb.elogika.dataTypes;

public class ActivityFinished {
    public String nazevSkupinaAktivit;
    public String nazevAktivita;
    public String doporHodnoceni;
    public String nazevTermin;
    public String datumVlozeni;
    public String hodnoceni;
    public String hodnoceniDatum;
    public String reseniSouborNazev;
    public String reseniSouborContent;
    public String reseni;
    public String poznamka;
    public String idAktivitaVyresene;
    public String idVlozil;
    public String idTermin;
    public String idAktivita;
    public String loginSkola;
    public String loginSystem;
    public String reseniSoubor;
    public String jmeno;
    public String prijmeni;
    public String titulPred;
    public String titulZa;
}
