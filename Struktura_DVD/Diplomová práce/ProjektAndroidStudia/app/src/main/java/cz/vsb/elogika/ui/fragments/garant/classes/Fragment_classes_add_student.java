package cz.vsb.elogika.ui.fragments.garant.classes;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import org.json.JSONException;
import org.json.JSONObject;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.Logging;

public class Fragment_classes_add_student extends Fragment {
    View rootView;

    EditText system_login;
    EditText school_login;
    EditText name;
    EditText surname;
    EditText email;
    EditText degree_front;
    EditText degree_behind;

    Button roundButton;

    int classId;
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_classes_add_student, container, false);

        classId = getArguments().getInt("classId");

        Actionbar.addSubcategory(R.string.add_student, this);
        Logging.logAccess("/Pages/Garant/StudentDetail/StudentForm.aspx");

        setComponents();
        return rootView;
    }

    private void setComponents() {
        system_login = (EditText) rootView.findViewById(R.id.c_l_s_add_system_login);
        school_login = (EditText) rootView.findViewById(R.id.c_l_s_add_school_login);
        name = (EditText) rootView.findViewById(R.id.c_l_s_add_name);
        surname = (EditText) rootView.findViewById(R.id.c_l_s_add_surname);
        email = (EditText) rootView.findViewById(R.id.c_l_s_add_email);
        degree_front = (EditText) rootView.findViewById(R.id.c_l_s_add_degree_front);
        degree_behind = (EditText) rootView.findViewById(R.id.c_l_s_add_degree_behind);



        Button add_student_btn = (Button) rootView.findViewById(R.id.c_l_s_add_student);
        add_student_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (school_login.length() == 0) {
                    SnackbarManager.show(Snackbar.with(getActivity()).text(R.string.login_is_mandatory));

                } else {
                    add_student();
                }
            }
        });

        /*
        roundButton = (Button) rootView.findViewById(R.id.roundButton);
        roundButton.setText("✔");

        this.roundButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (school_login.length() == 0) {
                    SnackbarManager.show(Snackbar.with(getActivity()).text(R.string.login_is_mandatory));

                } else {
                    add_student();
                }
            }
        });
        */
    }

    private void add_student() {
        Response arrayResponse = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                if (response.length() > 0)
                {
                    if (response.getInt("IdUzivatel") == -1)
                        create_student();
                    else
                        insert_student(response.getInt("IdUzivatel"));
                }
                else {
                    Log.d("Chyba", "classes_add_student add_student");
                }
            }
        };
        Request.get(URL.User.GetUserIdBySchoolsLogin(User.schoolID, school_login.getText().toString()), arrayResponse);
    }

    private void create_student() {
        Response response = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                if (response.getString("UserId").length() != 0) {
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.student) + " " + name.getText().toString() + " " + surname.getText().toString() + " [" + school_login.getText().toString() + "] " + getActivity().getString(R.string.student_was_created)));
                    insert_student(response.getInt("UserId"));

                }
                else{ //nepovedlo se
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.student_4p) + " " + name.getText().toString() + " " + surname.getText().toString() + " [" + school_login.getText().toString() + "] " + getActivity().getString(R.string.student_was_not_inserted)));
                }
            }
        };
        JSONObject parameters = new JSONObject();
        try
        {
            parameters.put("IdUzivatel", "");
            parameters.put("Login", system_login.getText().toString());
            parameters.put("LoginSchool", school_login.getText().toString());
            parameters.put("Jmeno", name.getText().toString());
            parameters.put("Prijmeni", surname.getText().toString());
            parameters.put("Email", email.getText().toString());
            parameters.put("TitulPred", degree_front.getText().toString());
            parameters.put("TitulZa", degree_behind.getText().toString());
        }
        catch (JSONException e) {}
        Request.post(URL.User.insertUser(), parameters, response);
    }

    private void insert_student(int studentId){
        Logging.logAccess("/Pages/Garant/StudentDetail/StudentSummary.aspx");
        Response response = new Response() {
            @SuppressLint("LongLogTag")
            @Override
            public void onResponse(JSONObject response) throws JSONException {
                if (response.getString("Success").equals("true")) {
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.student) + " " + school_login.getText().toString() + " " + getActivity().getString(R.string.student_was_inserted)));


                    Actionbar.jumpBack();
                } else { //nepovedlo se
                    Log.d("Fragment_classes_add_student", "insert_student(): " + response.toString());
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.student_4p) + " " + school_login.getText().toString() + " " + getActivity().getString(R.string.student_was_not_inserted)));

                }
            }
        };
        Request.post(URL.User.InsertStudent(studentId, User.schoolInfoID, classId, school_login.getText().toString()), response);
    }
}
