package cz.vsb.elogika.ui.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.ui.fragments.takeAtest.Fragment_OnlineTest_list;
import cz.vsb.elogika.ui.fragments.takeAtest.Taking_a_test;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.DownloadFile;
import cz.vsb.elogika.utils.Logging;
import cz.vsb.elogika.utils.SelectFileToUpload;
import cz.vsb.elogika.utils.SeparatedListAdapter;
import cz.vsb.elogika.utils.TimeLogika;

/**
 * Fragment pro teorii u studenta a Správu kapitol u garanta
 * Obsahuje:
 *  +kapitoly
 *  +materiály
 *  +cvičné testy
 */
public class Fragment_Theory extends Fragment {
    private View rootView;

    private ProgressBar progressDialog;
    LinearLayout nothinglayout;
    private ListView listView;

    final String[] from = {"Nazev"};
    final int[] to = {R.id.theory_name};


    private ArrayList<Map<String, String>> chapterList = new ArrayList<Map<String, String>>();
    private SimpleAdapter adapter;
    private ArrayList<Map<String, String>> file_list = new ArrayList<Map<String, String>>();
    private SimpleAdapter adapterFiles;


    SeparatedListAdapter separatedListAdapter;

    /**
     * Inicializace UI
     */
    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_theory, container, false);
        Logging.logAccess("/Pages/Student/Teorie.aspx");
        if(User.isGarant) {
            Actionbar.newCategory(R.string.chapters_managing, this);
        }
        else {
            Actionbar.newCategory(R.string.theory, this);
        }
        progressDialog = (ProgressBar) rootView.findViewById(R.id.theory_progress);
        nothinglayout = (LinearLayout) rootView.findViewById(R.id.theory_nothing_to_show);
        nothinglayout.setVisibility(View.GONE);

        listView = (ListView) rootView.findViewById(R.id.theory_listView);

        separatedListAdapter = new SeparatedListAdapter(getActivity());
        adapter = new SimpleAdapter(getActivity(), chapterList, R.layout.fragment_theory_list_item, from, to) {
            @Override
            public View getView(final int position, View convertView, ViewGroup parent) {
                final View view = super.getView(position, convertView, parent);
                //  if (view.getTag() == null) {
                if (User.isGarant) {
                    ImageButton buttonedit = (ImageButton) view.findViewById(R.id.theory_edit_button);
                    buttonedit.setVisibility(View.VISIBLE);
                    buttonedit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String nazev = chapterList.get(position).get("Nazev");
                            int chapterid = Integer.valueOf(chapterList.get(position).get("IDKapitola"));
                            int parentid = getParentIDfromChapterID(chapterid);
                            createDialog(true, nazev, chapterid, parentid);
                        }
                    });
                }
                //    }
                //    view.setTag("already asociated");
                return view;
            }
        };
        separatedListAdapter.addSection(getResources().getString(R.string.list_of_chapters), adapter);
        adapterFiles = new SimpleAdapter(getActivity(), file_list, R.layout.fragment_theory_list_item, from, to) {
            @Override
            public View getView(final int position, View convertView, ViewGroup parent) {
                final View view = super.getView(position, convertView, parent);
                if (User.isGarant || User.isTutor) {
                    ImageButton buttondelete = (ImageButton) view.findViewById(R.id.theory_delete_button);
                    buttondelete.setVisibility(View.VISIBLE);
                    buttondelete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int IDMaterial = Integer.parseInt(file_list.get(position).get("IDMaterial"));
                            int IDKapitola = Integer.parseInt(file_list.get(position).get("IDKapitola"));
                            deleteMaterial(IDKapitola,IDMaterial);
                        }
                    });
                }
                return view;
            }
        };


        separatedListAdapter.addSection(getString(R.string.list_of_materials), adapterFiles);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                if(position < chapterList.size()+1)
                {
                    onChapterClick(position-1);
                }
                else if(position < chapterList.size() + file_list.size()+2)
                {
                    onFileClick(position-chapterList.size()-2);
                }
                else
                    onPractiseTestClick(position - chapterList.size() - file_list.size() - 3, view);
            }


        });

        getChaptersFromServer(-1);
        initTestlist();
        initizeAddButton(rootView);

        listView.setAdapter(separatedListAdapter);
        return rootView;

    }


    /**
     * Zpracování kliknutí na cvičný test,
     * zobrazí se okno s podrobným popisem a možností spustit test
     * Při volbě spustit test se inicializuje třída pro testy: "Fragment_OnlineTest_list" a zavolá se metoda "practiseTest"
     *
     * @param position na jakou pozici bylo kliknuto
     * @param view na co bylo kliknuto
     *
     */
    private void onPractiseTestClick(int position, View view) {
         Logging.logAccess("/Pages/Student/TeorieDetail/TeorieForm.aspx?ID=", "TrainTest");
         final Dialog myDialog = new Dialog(getActivity());
         myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
         myDialog.setContentView(R.layout.fragment_take_a_test_detail);
         LinearLayout Llayout = (LinearLayout) myDialog.findViewById(R.id.take_a_test_detail_layout);
         TextView name = (TextView) view.findViewById(R.id.take_a_test_name);
         TextView activites_group = (TextView) view.findViewById(R.id.take_a_test_activities_group);
         //  TextView points = (TextView) view.findViewById(R.id.take_a_test_p);
         TextView minpoints = (TextView) view.findViewById(R.id.take_a_test_minpoints);
         TextView maxpoints = (TextView) view.findViewById(R.id.take_a_test_maxpoints);
         TextView active = (TextView) view.findViewById(R.id.take_a_test_active);
         final TextView trialsRemaining = (TextView) view.findViewById(R.id.take_a_test_trialsRemaining);
         final TextView timeMinutes = (TextView) view.findViewById(R.id.take_a_test_timeminutes);
         Taking_a_test.testName = (String) name.getText();
         String data[];
         String[] labels = {getString(R.string.name), getString(R.string.activity_group), getString(R.string.minmaxpoints),
                 getString(R.string.trialsremaining), getString(R.string.minutes)};

         data = new String[]{
                 (String) name.getText(),
                 (String) activites_group.getText(),
                 //    (String) points.getText(),
                 minpoints.getText() + " / " + maxpoints.getText(),
                 (String) trialsRemaining.getText(),
                 (String) timeMinutes.getText()
         };


         TextView labelText;
         TextView dataText;
         for (int j = 0; j < data.length && j < labels.length; j++) {
             labelText = new TextView(getActivity());
             labelText.setText(labels[j]);
             if (Build.VERSION.SDK_INT < 23) {
                 labelText.setTextAppearance(view.getContext(), R.style.itemHint);
             } else {
                 labelText.setTextAppearance(R.style.itemHint);
             }
             Llayout.addView(labelText);
             dataText = new TextView(getActivity());
             dataText.setText(data[j]);
             if (Build.VERSION.SDK_INT < 23) {
                 dataText.setTextAppearance(view.getContext(), R.style.itemValue);
             } else {
                 dataText.setTextAppearance(R.style.itemValue);
             }
             dataText.setGravity(Gravity.RIGHT);// | Gravity.CENTER);
             Llayout.addView(dataText);
         }
         final LinearLayout progressBar = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.loadingwithpercentage, null);
         final TextView progressPercentage = (TextView) progressBar.findViewById(R.id.percentageprogresstext);
         Llayout.addView(progressBar);
         final Button buttonGenerateTest = new Button(getActivity());
         buttonGenerateTest.setText(R.string.generate_and_run_test);
         final int testID = view.getId();
         buttonGenerateTest.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 myDialog.setCancelable(false);
                 progressBar.setVisibility(View.VISIBLE);
                 buttonGenerateTest.setVisibility(View.GONE);

                 Log.d("spoustím cvičný test", "start");
                 new Fragment_OnlineTest_list().practiseTest(
                         getActivity(),
                         testID, //testID
                         Integer.valueOf((String) timeMinutes.getText()), //dateTime minutes
                         myDialog,
                         progressBar,
                         progressPercentage
                 );

             }
         });

         if (Integer.valueOf((String)trialsRemaining.getText()) < 1) buttonGenerateTest.setEnabled(false);
         Llayout.addView(buttonGenerateTest);

         canGenerate(testID, Integer.parseInt(testsitemList.get(position).get("IDTermin").toString()), trialsRemaining, buttonGenerateTest);
         myDialog.show();

    }

    /**
     * metoda pro zjistění zda-li test může být vygenerován
     *
     * @param testId id testu
     * @param terminId id termínu pro test
     * @param trialstext odkaz na text "počet pokusu"
     * @param b tlačítko "sputit test" bude deaktivováno jestli test nelze spustit
     *
     */
    private void canGenerate(final int testId, final int terminId, final TextView trialstext, final Button b){
        Response res = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException {
                Log.d("cangenerate, terminId: " + terminId, response.toString());
                b.setEnabled(response.getBoolean("CanGenerate"));
                trialstext.setText((String.valueOf(response.getInt("RemainToGenerate"))));
                if(response.getInt("RemainToGenerate") == 0)
                    trialstext.setTextColor(Color.RED);
            }
        };
        Request.get(URL.GeneratedTest.canGenerate(testId, User.id, terminId), res);
    }


    /**
     * @param position pozice na kterou bylo kliknuto
     *
     * Metoda pro zpracování kliknutí na soubor, soubor se stáhne do paměti zařízení
     */
    private void onFileClick(int position) {
         //content-type nas asi nezajima, protoze pripona je obsazena v nazvu souboru
         Logging.logAccess("/Pages/Student/TeorieDetail/TeorieForm.aspx?ID=", "Material");
         Intent intent = DownloadFile.download(file_list.get(position).get("name"), file_list.get(position).get("Soubor"));
         Toast.makeText(getActivity(), getResources().getString(R.string.file) + " '" + file_list.get(position).get("name") + "' " + getResources().getString(R.string.is_downloaded_in_download_folder), Toast.LENGTH_LONG).show();
         getActivity().startActivity(intent);
    }

    /**
     * Metoda pro zpracování kliknutí na kapitolu, dojde k zanoření do kapitoly a zobrazí se aktuální podkapitoly
     *
     * @param position pozice na kterou bylo kliknuto
     *
     */
    private void onChapterClick(int position) {
        final int chapterID = Integer.valueOf(chapterList.get(position).get("IDKapitola"));
        Logging.logAccess("/Pages/Student/TeorieDetail/TeorieForm.aspx?ID="+chapterID, "Theory");
        Actionbar.Action  action = new Actionbar.Action() {
            @Override
            public void onClick(Activity activity) {
                int chapterID1 = chapterID;
                fillChapterList(chapterID1);
            }
        };
        String nazev = chapterList.get(position).get("Nazev");
        Actionbar.addSection(nazev, action);
        fillChapterList(chapterID);
    }

    /**
     * Vytvoří dialog pro přidání nebo editaci kapitol nebo souborů (nedostupné pro roli student)
     *
     * @param edit parametr jestli se jedná o editaci nebo vytvoření nové kapitoly
     * @param name název kapitoly
     * @param chapterID id kapitoly
     * @param parentID id rodiče
     *
     */
    private void createDialog(final boolean edit,String name, final int chapterID, final int parentID) {
        selectedFileInBase64 = null;
        selectedFileType = null;
        final Dialog d = new Dialog(getActivity());
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView( R.layout.fragment_theory_chapter_edit );
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        final EditText edittextname = (EditText)d.findViewById(R.id.chapter_edit_name);


        if(edit || currentChapterID == -1)
        {
            d.findViewById(R.id.gap).setVisibility(View.GONE);
            d.findViewById(R.id.add_material).setVisibility(View.GONE);
        }
        if (edit){
            d.findViewById(R.id.add_chapter_icon).setVisibility(View.GONE);
            Button delete = (Button)d.findViewById(R.id.deletechapterbutton);
            delete.setVisibility(View.VISIBLE);
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteChapter(parentID ,chapterID, d);
                }
            });

            edittextname.append(name);
        }
        else {
            d.findViewById(R.id.edit_chapter_icon).setVisibility(View.GONE);

            ((Button)d.findViewById(R.id.choose_material)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    new SelectFileToUpload(getActivity()) {
                        @Override
                        public void selectedFile(String FileName, String FileType, String FileinBase64) {
                            ((EditText)d.findViewById(R.id.material_name)).setText(FileName);
                            ((Button)d.findViewById(R.id.choose_material)).setText(FileName);
                            selectedFileType = FileType;
                            selectedFileInBase64 = FileinBase64;
                        }
                    };
                }});

            ((Button)d.findViewById(R.id.upload_material)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String filename = ((EditText) d.findViewById(R.id.material_name)).getText().toString();
                    if(selectedFileInBase64 == null)
                        Toast.makeText(getActivity(),getActivity().getString(R.string.no_file_was_selected), Toast.LENGTH_SHORT).show();
                    else if (filename.equals(""))
                        Toast.makeText(getActivity(),getActivity().getString(R.string.attachment_name_cant_be_empty), Toast.LENGTH_SHORT).show();
                    else
                    {
                        v.setEnabled(false);
                        d.findViewById(R.id.chapter_insert_progrees).setVisibility(View.VISIBLE);
                        insertMaterial(d, chapterID, filename, selectedFileType, selectedFileInBase64);
                    }
                }});


        }
        ((Button)d.findViewById(R.id.chapter_confrim_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nazev =  edittextname.getText().toString();
                if (edit){
                    updateChapter(d,nazev, chapterID,parentID );
                }
                else
                {
                    insertChapter(d,nazev, currentChapterID);
                }
            }
        });
        d.show();
    }

    String selectedFileType;
    String selectedFileInBase64;

    JSONArray savedResponse;

    /**
     * metoda pro zanořování, zobrazí se pouze aktuální kapitola a její obsah
     *
     * @param chapterID id aktuální kapitoly, root = -1
     *
     */
    private void fillChapterList(int chapterID)
    {
        currentChapterID = chapterID;

        if (chapterID == -1) {
            currentParentID = -1;
        }
        else
            for (int i = 0; i<savedResponse.length();i++) {
                try {
                    if (savedResponse.getJSONObject(i).getInt("IDKapitola") == currentChapterID)
                    {
                        if (savedResponse.getJSONObject(i).getString("ParentID") == "null") currentParentID = -1;
                        else currentParentID = savedResponse.getJSONObject(i).getInt("ParentID");
                        break;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


        chapterList.clear();
        try {
            for (int i = 0;i<savedResponse.length();i++) {
                if (chapterID == -1) {
                    if (savedResponse.getJSONObject(i).getString("ParentID") == "null") {
                        HashMap<String, String> item = new HashMap<String, String>();
                        item.put("Nazev", savedResponse.getJSONObject(i).getString("Nazev"));
                        item.put("IDKapitola", savedResponse.getJSONObject(i).getString("IDKapitola"));
                        item.put("ParentID", savedResponse.getJSONObject(i).getString("ParentID"));
                        this.chapterList.add(item);
                    }
                } else
                if (savedResponse.getJSONObject(i).getString("ParentID") != "null")
                    if (savedResponse.getJSONObject(i).getInt("ParentID") == chapterID) {
                        HashMap<String, String> item = new HashMap<String, String>();
                        item.put("Nazev", savedResponse.getJSONObject(i).getString("Nazev"));
                        item.put("IDKapitola", savedResponse.getJSONObject(i).getString("IDKapitola") );
                        item.put("ParentID", savedResponse.getJSONObject(i).getString("ParentID"));
                        this.chapterList.add(item);
                    }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        notifyAdapters();

        currentChapterID = chapterID;
        getpractiseTests(chapterID);
        getChapterFiles(chapterID);
    }


    /**
     * Získá id rodiče
     *
     * @param chapterid id kapitly
     * @return id rodiče zadané kapitoly
     *
     */
    private int getParentIDfromChapterID(int chapterid)
    {
        try{
            for(int i = 0; i<savedResponse.length();i++)
            {
                if(chapterid == savedResponse.getJSONObject(i).getInt("IDKapitola"))
                {
                    return savedResponse.getJSONObject(i).getInt("ParentID");
                }
            }
        }
        catch(JSONException e){}
        return -1;
    }

    /**
     * získá kapitoly ze serveru
     *
     * @param selectedChapterID id vybrané kapitoly
     */
    private void getChaptersFromServer(final int selectedChapterID) {

        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                savedResponse = response;
                fillChapterList(selectedChapterID);
                if (response.length() == 0)
                {
                    nothinglayout.setVisibility(View.VISIBLE);
                }
                progressDialog.setVisibility(View.GONE);

                notifyAdapters();
            }
        };
        Request.get(URL.Chapter.chaptersbycourse(User.courseInfoId), arrayResponse);
    }


    /**
     * Zobrazí materiály(soubory) ze serveru pro zvolenou kapitolu
     *
     * @param chapterId id vybrané kapitoly
     */
    private void getChapterFiles(final int chapterId) {
        file_list.clear();
        progressDialog.setVisibility(View.VISIBLE);
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                if (response.length() > 0)
                {
                    for (int j = 0; j < response.length(); j++)
                    {
                        Log.d("byl pridan soubor: ", chapterId + " - " + response.getJSONObject(j).getString("IDKapitola"));
                        add_file(
                                response.getJSONObject(j).getString("IDMaterial"),
                                response.getJSONObject(j).getString("IDKapitola"),
                                response.getJSONObject(j).getString("Nazev"),
                                response.getJSONObject(j).getString("Soubor"),
                                response.getJSONObject(j).getString("SouborNazev"),
                                response.getJSONObject(j).getString("SouborContent"));
                    }
                }
                else{
                    Log.d("kapitola " + chapterId, "nema zadne soubory");
                }
                progressDialog.setVisibility(View.GONE);

                notifyAdapters();
            }
        };
        Request.get(URL.Chapter.materials(chapterId), arrayResponse);
    }

    /**
     * přidá soubor do seznamu
     *
     * @param IDMaterial id souboru
     * @param idKapitola id kapitoly
     * @param nazev název materiálu
     * @param soubor název souboru
     * @param souborContent data souboru
     *
     */
    private void add_file(String IDMaterial,String idKapitola, String nazev, String soubor, String souborNazev, String souborContent) {
        HashMap<String, String> item = new HashMap<String, String>();
        item.put("IDMaterial",IDMaterial);
        item.put("IDKapitola", idKapitola);
        item.put("Nazev", nazev);
        item.put("Soubor", soubor);
        item.put("name", souborNazev);
        item.put("SouborContent", souborContent);

        file_list.add(item);
    }


    /**
     * Nastaví seznam pro testy
     */
    private void initTestlist() {
        String[] from = {"name", "activitiesGroup", "minPoints", "maxPoints", "active", "trialsRemaining", "timeMinutes"};
        int[] to = {R.id.take_a_test_name, R.id.take_a_test_activities_group, R.id.take_a_test_minpoints, R.id.take_a_test_maxpoints,
                R.id.take_a_test_active, R.id.take_a_test_trialsRemaining, R.id.take_a_test_timeminutes
        };

        this.testsadapter = new SimpleAdapter(getActivity(), this.testsitemList, R.layout.fragment_take_a_test_item, from, to) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView nadpis = (TextView) view.findViewById(R.id.take_a_test_name);
                String mandatory = testsitemList.get(position).get("mandatory");
                if (mandatory.charAt(0) == 'T') {
                    nadpis.setTextColor(getResources().getColor(R.color.elogika_mandatory));
                } else nadpis.setTextColor(Color.WHITE);
                view.setId(Integer.parseInt(mandatory.substring(1)));

                return view;
            }
        };
        separatedListAdapter.addSection(getResources().getString(R.string.list_of_practise_tests),this.testsadapter);
    }

    /**
     * Získá cvičné testy ze serveru
     *
     * @param chapterId id zvolené kapitoly
     *
     */
    private void getpractiseTests (final int chapterId)
    {
        testsitemList.clear();
        notifyAdapters();
        ArrayResponse r = new ArrayResponse() {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {

                Log.d("getpractiseTests", response.toString());
                if (response.length() > 0)
                {
                    for (int j = 0; j < response.length(); j++) {

                        addItemIntoTestsList(response.getJSONObject(j).getInt("IDTest"),
                                response.getJSONObject(j).getString("Nazev"),
                                response.getJSONObject(j).getString("NazevSA"),
                                response.getJSONObject(j).getString("MinBody"),
                                response.getJSONObject(j).getString("MaxBody"),
                                response.getJSONObject(j).getString("AktivniOD"),
                                response.getJSONObject(j).getString("AktivniDO"),
                                response.getJSONObject(j).getString("PocetPokusu"), //tady server neposílá remain to generate
                                response.getJSONObject(j).getBoolean("Povinny"),
                                response.getJSONObject(j).getInt("Cas"),
                                response.getJSONObject(j).getInt("IDTermin")
                        );
                    }
                }
                //else
             //  if(chapterId != -1) //TODO delete getActivity().findViewById(R.id.theory_tests_nothing_to_show).setVisibility(View.VISIBLE);

                notifyAdapters();
            }
        };

        Log.d("getpractiseTests ", "currenttestbychapterrt:" + chapterId);
        Request.get(URL.CourseConditions.currenttestbychapterrt(chapterId, User.id), r);
    }
    private ArrayList<Map<String, String>> testsitemList = new ArrayList<Map<String, String>>();
    private SimpleAdapter testsadapter;

    /**
     * zařadí test do seznamu
     *
     * @param testID id testu
     * @param name název testu
     * @param activitiesGroup názv skupiny aktivity
     * @param minPoints minimální počet bodů pro úspěch
     * @param maxPoints cekový počet bodů
     * @param activeFrom aktivní od
     * @param activeTo aktivní do
     * @param trialsRemaining zbývá pokusů
     * @param mandatory povinný?
     * @param timeMinutes počet minut na vypracování
     *
     */
    private void addItemIntoTestsList(int testID, String name, String activitiesGroup, String minPoints, String maxPoints,
                                      String activeFrom, String activeTo, String trialsRemaining, boolean mandatory, int timeMinutes,int IDTermin) {

        HashMap<String, String> item = new HashMap<String, String>();
        item.put("name", name);
        item.put("activitiesGroup", activitiesGroup);
        item.put("minPoints", minPoints);
        item.put("maxPoints", maxPoints);
        item.put("active", TimeLogika.getFormatedDateTime(activeFrom) + " - " + TimeLogika.getFormatedDateTime(activeTo));
        item.put("trialsRemaining", trialsRemaining);
        if (mandatory)
            item.put("mandatory", "T" + testID);
        else item.put("mandatory", "F" + testID);
        item.put("timeMinutes", String.valueOf(timeMinutes));
        item.put("IDTermin", String.valueOf(timeMinutes));
        this.testsitemList.add(item);
        if (this.testsadapter != null)
            notifyAdapters();
    }

    int currentChapterID = -1;
    int currentParentID = -1;


    private void initizeAddButton(View rootView) {
        if(User.isGarant){
            rootView.findViewById(R.id.theory_add_new_button).setVisibility(View.VISIBLE);
            ((Button) rootView.findViewById(R.id.theory_add_new_button)).setOnClickListener(new View.OnClickListener() {
                                                                                                @Override
                                                                                                public void onClick(View v) {
              createDialog(false, "", currentChapterID, currentParentID);
                                                                                                }
                                                                                            }
            );
        }
        else{
            rootView.findViewById(R.id.theory_add_new_button).setVisibility(View.GONE);
        }
    }

    /**
     * vloží novou kapitolu na serveru
     *
     * @param d oteřený dialog
     * @param nazev název kapitoly
     * @param IDkapitola id kapitoly do které je kapitola vkládána, vložit do rootu = -1
     *
     */
    private void insertChapter(final Dialog d,String nazev, final int IDkapitola)
    {
        Response res = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException {
                d.cancel();
                getChaptersFromServer(IDkapitola);
                Log.d("insertChapter response", response.toString());
            }
        };
        JSONObject parameters = new JSONObject();
        if (IDkapitola == -1){
            try {
                parameters.put("nazev", nazev);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else {
            try {
                parameters.put("ParentID", IDkapitola);
                parameters.put("nazev", nazev);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        Log.d("insertChapter", parameters.toString());
        Request.post(URL.Chapter.insertchapter(User.courseInfoId), parameters, res);
    }


    /**
     * smaže zadanou kapitolu
     *
     * @param ParentID id rodiče
     * @param IDkapitola id kapitoly
     * @param d otevřený dialog
     *
     */
    private void deleteChapter(final int ParentID,int IDkapitola, final Dialog d)
    {
        Response res = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException {
                Log.d("DeleteChapter response", response.toString());
                d.cancel();
                getChaptersFromServer(ParentID);
            }
        };
        JSONObject parameters = new JSONObject();
        try {
            parameters.put("id", IDkapitola);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("deleteChapter", parameters.toString());
        Request.post(URL.Chapter.deletechapter(IDkapitola), parameters, res);
    }

    /**
     * upraví kapitolu
     *
     * @param d otevřený dialog
     * @param nazev nový název
     * @param IDkapitola id kapitoly
     * @param ParentID id rodiče
     *
     */
    private void updateChapter(final  Dialog d,String nazev,int IDkapitola, final int ParentID)
    {
        Response res = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException {
                d.cancel();
                getChaptersFromServer(ParentID);
                Log.d("updateChapter response", response.toString());
            }
        };
        JSONObject parameters = new JSONObject();
        if (IDkapitola == -1){ //pokud jsme v rootu
            try {
                parameters.put("ParentID", null);
                parameters.put("Nazev", nazev);
                parameters.put("IDKapitola", IDkapitola);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else {
            try {
                parameters.put("IDKapitola", IDkapitola);
                parameters.put("ParentID", ParentID);
                parameters.put("Nazev", nazev);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        Log.d("updateChapter", parameters.toString());
        Request.post(URL.Chapter.updatechapter(), parameters, res);
    }

    /**
     * vloží materiál na server
     *
     * @param d otevřený dialog
     * @param chapterID id kapitoly do které se materiál vloží
     * @param nazev název materiálu
     * @param fileType typ materiálu
     * @param fileInBase64 data souboru zakodované v base64
     *
     */
    private void insertMaterial(final Dialog d,final int chapterID, String nazev,String fileType, final String fileInBase64)
    {

        Response r = new Response() {

            @Override
            public void onResponse(JSONObject response) throws JSONException {
                d.cancel();
                getChapterFiles(chapterID);
                selectedFileInBase64 = null;
                selectedFileType = null;
            }

        };
        try {

            JSONObject parameters = new JSONObject();
            parameters.put("IDKapitola", chapterID);
            parameters.put("Nazev", nazev);
            parameters.put("SouborNazev", nazev);
            parameters.put("Soubor",fileInBase64);
            parameters.put("SouborContent", fileType);


            Log.d("insert material request", parameters.toString());
            Request.post(URL.Chapter.insertmaterial(), parameters, r);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    /**
     * smaže materiál na serveru
     *
     * @param chapterid id kapitoly ve které se materiál nachází
     * @param IDmaterial id materiálu, který má být smazán
     *
     */
    private void deleteMaterial(final int chapterid,int IDmaterial)
    {
        Response res = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException {
                Log.d("DeleteChapter response", response.toString());
                getChapterFiles(chapterid);
            }
        };

        Request.get(URL.Chapter.removeMaterial(IDmaterial), res);
    }

    /**
     * při donačtení dat musí být UI upozorněno aby se překreslilo
     */
    private void notifyAdapters(){
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(adapter != null) adapter.notifyDataSetChanged();
                if(adapterFiles != null) adapterFiles.notifyDataSetChanged();
                if(testsadapter != null) testsadapter.notifyDataSetChanged();
                if(separatedListAdapter != null) separatedListAdapter.notifyDataSetChanged();
            }
        });
    }
}
