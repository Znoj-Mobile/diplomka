package cz.vsb.elogika.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import cz.vsb.elogika.R;

public class ListViewHierarchicalAdapter extends BaseAdapter
{
    private Context context;
    private ArrayList<Item> categories;
    private int remain;

    final private int space = 10;



    public static class Item
    {
        protected int id;
        protected View view;
        protected Item parent;
        protected int level;
        protected View groupingButton;
        protected ArrayList<Item> children;
        protected int allChildrenCount;
        protected boolean isGrouped;
        protected ListViewHierarchicalAdapter adapter;


        public Item()
        {
            this.id = 0;
            this.view = null;
            this.parent = null;
            this.level = 0;
            this.groupingButton = null;
            this.children = new ArrayList();
            this.allChildrenCount = 0;
            this.isGrouped = false;
            this.adapter = null;
        }



        public Item addChild(View view)
        {
            Item child = new Item();
            child.parent = this;
            child.adapter = this.adapter;
            child.level = this.level + 1;
            this.children.add(child);

            //LinearLayout layout = (LinearLayout) LayoutInflater.from(this.adapter.context).inflate(R.layout.hierarchical_adapter_item, null, false);
            //layout.setPadding((int) (child.level * this.adapter.space * this.adapter.context.getResources().getDisplayMetrics().density), 0, 0, 0);
            //this.groupingButton.setVisibility(View.VISIBLE);
            /*this.groupingButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    Item item = Item.this;
                    int count;
                    if (Item.this.isGrouped) count = item.allChildrenCount;
                    else count = -item.allChildrenCount;
                    item = item.parent;
                    while (item != null)
                    {
                        item.allChildrenCount += count;
                        item = item.parent;
                    }

                    Item.this.isGrouped = !Item.this.isGrouped;
                    adapter.notifyDataSetChanged();
                }
            });*/
            //layout.addView(view);
            //child.groupingButton = layout.findViewById(R.id.grouping_button);
            child.view = view;

            increaseChildCountInAllHigherLevels();

            return child;
        }



        public void removeChild(Item item)
        {
            if (item.parent == null) this.adapter.categories.remove(item);
            else
            {
                if (item.parent.children.size() == 1) item.parent.groupingButton.setVisibility(View.INVISIBLE);
                item.parent.children.remove(item);
            }
        }



        private void increaseChildCountInAllHigherLevels()
        {
            Item item = this;
            while(item != null)
            {
                item.allChildrenCount++;
                item = item.parent;
            }
        }
    }



    public ListViewHierarchicalAdapter(Context context)
    {
        this.context = context;
        this.categories = new ArrayList();
    }



    public Item addCategory(View view)
    {
        Item category = new Item();
        category.adapter = this;
        category.level = 0;
        this.categories.add(category);

        //LinearLayout layout = (LinearLayout) LayoutInflater.from(this.context).inflate(R.layout.hierarchical_adapter_item, null, false);
        //layout.addView(view);
        //category.groupingButton = layout.findViewById(R.id.grouping_button);
        category.view = view;

        return category;
    }



    public void removeCategory(Item item)
    {
        this.categories.remove(item);
    }



    private Item findItem(ArrayList<Item> children)
    {
        Item temp;
        for (Item child: children)
        {
            if (remain == 0) return child;
            this.remain--;
            if (child.isGrouped) continue;
            if (child.children.size() != 0)
            {
                temp = findItem(child.children);
                if (temp != null) return temp;
            }
        }
        return null;
    }

    public int getItemViewType(int position)
    {
        return 0;
    }

    public int getViewTypeCount()
    {
        return 1;
    }

    @Override
    public int getCount()
    {
        int sum = 0;
        for (Item category: this.categories)
        {
            sum++;
            if (category.isGrouped) continue;
            sum += category.allChildrenCount;
        }
        return sum;
    }



    @Override
    public Object getItem(int i)
    {
        return findItem(this.categories);
    }



    @Override
    public long getItemId(int i)
    {
        return i;
    }



    @Override
    public View getView(int i, View view, ViewGroup viewGroup)
    {
        // http://android.amberfog.com/?p=296
        Log.d("list view item", String.valueOf(i));

        this.remain = i;
        final Item item = findItem(this.categories);
        if (item.view.getParent() != null && view != null)
        {
            if (item.view.getParent() == view)
            {
                return view;
            }
            else
            {
                try
                {
                    ((ViewGroup) view).removeViewAt(1);
                }
                catch (NullPointerException e) {}
                ((ViewGroup) item.view.getParent()).removeView(item.view);
                ((ViewGroup) view).addView(item.view);
                Log.d("chyba", "to by se nemělo stát");
                return view;
            }
        }
        //View v;
        //if (view == null)
        //{
            view = (LinearLayout) LayoutInflater.from(this.context).inflate(R.layout.hierarchical_adapter_item, viewGroup, false);

            view.setPadding((int) (item.level * this.space * this.context.getResources().getDisplayMetrics().density), 0, 0, 0);
            item.groupingButton = view.findViewById(R.id.grouping_button);
            if (item.parent != null)
            {
                item.parent.groupingButton.setVisibility(View.VISIBLE);
                item.parent.groupingButton.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        Item parent = item.parent;
                        int count;
                        if (parent.isGrouped) count = parent.allChildrenCount;
                        else count = -parent.allChildrenCount;
                        parent = parent.parent;
                        while (parent != null)
                        {
                            parent.allChildrenCount += count;
                            parent = parent.parent;
                        }

                        item.parent.isGrouped = !item.parent.isGrouped;
                        notifyDataSetChanged();
                    }
                });
            }


            if (item.view.getParent() != null)
            {
                ((ViewGroup) item.view.getParent()).removeView(item.view);
                return (View) item.view.getParent();
            }
            TextView textView = new TextView(this.context);
            textView.setText(String.valueOf(i));
            ((LinearLayout) view).addView(textView);
            ((LinearLayout) view).addView(item.view);
        /*}
        else
        {
            //if (view != null) ((ViewGroup) view).removeViewAt(0);
            v = findItem(this.categories).view;
            if (v.getParent() != null) ((ViewGroup) v.getParent()).removeView(v);
            //((ViewGroup) view).removeAllViews();
            ((ViewGroup) view).addView(v);
        }*/
        return view;
    }
}
