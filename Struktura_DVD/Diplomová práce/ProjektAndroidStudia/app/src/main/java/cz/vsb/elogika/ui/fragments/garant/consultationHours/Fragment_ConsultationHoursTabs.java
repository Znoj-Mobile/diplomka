package cz.vsb.elogika.ui.fragments.garant.consultationHours;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.vsb.elogika.R;
import cz.vsb.elogika.adapters.HierarchicalAdapter;
import cz.vsb.elogika.common.view.SlidingTabLayout;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.dataTypes.ConsultationHour;
import cz.vsb.elogika.dataTypes.ConsultationHourTerm;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.Logging;

public class Fragment_ConsultationHoursTabs extends Fragment
{
    private Tab_ConsultationHoursList tab_consultationHoursList;
    private Tab_ConsultationHoursOccupiedList tab_consultationHoursOccupiedList;
    private Tab_ConsultationHoursHistoryList tab_consultationHoursHistoryList;

    private ArrayList<ConsultationHour> consultationHours;
    private ArrayList<ConsultationHour> occupiedConsultationHours;
    private Boolean areConsultationHoursReady;
    private Boolean areOccupiedConsultationHoursReady;
    private Object lock;

    final private static int CONSULTATION_HOURS = 0;
    final private static int OCCUPIED_CONSULTATION_HOURS = 1;

    protected final static int CONSULTATION_HOUR = 0;
    protected final static int TERM_HEADER = 1;
    protected final static int TERM = 2;



    protected class ConsultationHourTermItem extends HierarchicalAdapter.Item
    {
        public ConsultationHour consultationHour;
        public int position;
    }



    public Fragment_ConsultationHoursTabs()
    {
        this.consultationHours = new ArrayList();
        this.occupiedConsultationHours = new ArrayList();
        this.areConsultationHoursReady = new Boolean(false);
        this.areOccupiedConsultationHoursReady = new Boolean(false);
        this.lock = new Object();
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        Actionbar.newCategory(R.string.consultation_hours, this);

        View rootView = inflater.inflate(R.layout.tabs_layout, container, false);
        ViewPager pager = (ViewPager) rootView.findViewById(R.id.pager);
        pager.setAdapter(new FragmentStatePagerAdapter(getFragmentManager())
        {
            @Override
            public Fragment getItem(int position)
            {
                switch (position)
                {
                    case 0:
                        tab_consultationHoursList = new Tab_ConsultationHoursList();
                        tab_consultationHoursList.consultationHours = consultationHours;
                        tab_consultationHoursList.areDataReady = areConsultationHoursReady;
                        tab_consultationHoursList.lock = lock;
                        return tab_consultationHoursList;

                    case 1:
                        tab_consultationHoursOccupiedList = new Tab_ConsultationHoursOccupiedList();
                        tab_consultationHoursOccupiedList.consultationHours = occupiedConsultationHours;
                        tab_consultationHoursOccupiedList.areDataReady = areOccupiedConsultationHoursReady;
                        tab_consultationHoursOccupiedList.lock = lock;
                        return tab_consultationHoursOccupiedList;

                    case 2:
                        tab_consultationHoursHistoryList = new Tab_ConsultationHoursHistoryList();
                        tab_consultationHoursHistoryList.consultationHours = consultationHours;
                        tab_consultationHoursHistoryList.areDataReady = areConsultationHoursReady;
                        tab_consultationHoursHistoryList.lock = lock;
                        return tab_consultationHoursHistoryList;
                }

                return null;
            }

            @Override
            public void destroyItem(ViewGroup container, int position, Object object)
            {
                super.destroyItem(container, position, object);

                switch (position)
                {
                    case 0:
                        tab_consultationHoursList = null;

                    case 1:
                        tab_consultationHoursOccupiedList = null;

                    case 2:
                        tab_consultationHoursHistoryList = null;
                }
            }

            @Override
            public CharSequence getPageTitle(int position)
            {
                switch (position)
                {
                    case 0:
                        return getString(R.string.list_of_consultation_hours);

                    case 1:
                        return getString(R.string.soonest_occupied_consultation_hours);

                    case 2:
                        return getString(R.string.history_of_consultation_hours);
                }

                return "";
            }

            @Override
            public int getCount()
            {
                return 3;
            }
        });
        ((SlidingTabLayout) rootView.findViewById(R.id.sliding_tabs)).setViewPager(pager);

        loadConsultationHours();
        loadOccupiedConsultationHours();

        Logging.logAccess("/Pages/Other/ConsultationHours/ConsultationHours.aspx");

        return rootView;
    }



    /**
     * Tato data jsou potřebná pro 1. a 3. tab
     */
    private void loadConsultationHours()
    {
        Request.get(URL.ConsultationHours.GetConsultationHoursDates(User.courseInfoId, User.id), getArrayResponse(CONSULTATION_HOURS));
    }



    /**
     * Tato data jsou potřebná pro 2. tab
     */
    private void loadOccupiedConsultationHours()
    {
        Request.get(URL.ConsultationHours.GetOccupiedConsultationHoursDates(User.courseInfoId, User.id), getArrayResponse(OCCUPIED_CONSULTATION_HOURS));
    }



    /**
     * Načte data.
     * Jelikož je zpracování přijatých dat z loadConsultationHours a loadOccupiedConsultationHours téměř shodné,
     * je to řešeno touto cestou...
     * @param mode to jestli se zpracujou data jako loadOccupiedConsultationHours nebo jako loadConsultationHours rozhoduje mode
     * @return
     */
    protected ArrayResponse getArrayResponse(final int mode)
    {
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                JSONObject o;
                ConsultationHour consultationHour;
                ConsultationHourTermItem consultationHourTermItem;

                for (int j = 0; j < response.length(); j++)
                {
                    o = response.getJSONObject(j);
                    consultationHour = new ConsultationHour();
                    consultationHour.id = o.getInt("Id");
                    consultationHour.from = o.getString("From");
                    consultationHour.to = o.getString("To");
                    consultationHour.fromHours = o.getString("FromHours");
                    consultationHour.toHours = o.getString("ToHours");
                    consultationHour.room = o.getString("Place");
                    consultationHour.terms = new ConsultationHourTerm[o.getJSONArray("Dates").length()];
                    if (consultationHour.terms.length > 0)
                    {
                        consultationHour.addChild(new HierarchicalAdapter.Item(), TERM_HEADER);
                    }
                    for (int i = 0; i < consultationHour.terms.length; i++)
                    {
                        consultationHour.terms[i] = new ConsultationHourTerm();
                        consultationHour.terms[i].date = o.getJSONArray("Dates").getJSONObject(i).getString("Date");
                        consultationHour.terms[i].loggedCount = o.getJSONArray("Dates").getJSONObject(i).getInt("LoggedCount");
                        consultationHourTermItem = new ConsultationHourTermItem();
                        consultationHourTermItem.consultationHour = consultationHour;
                        consultationHourTermItem.position = i;
                        consultationHour.addChild(consultationHourTermItem, TERM);
                    }
                    consultationHour.repeatableType = o.getInt("RepeatableType");
                    consultationHour.maximumOfStudents = o.getInt("MaxStudents");
                    consultationHour.description = o.getString("Description");
                    consultationHour.notifyViaEmail = o.getBoolean("AlertByEmail");
                    consultationHour.groupItems();

                    if (mode == CONSULTATION_HOURS) consultationHours.add(consultationHour);
                    else occupiedConsultationHours.add(consultationHour);
                }

                synchronized (lock)
                {
                    if (mode == CONSULTATION_HOURS && tab_consultationHoursList != null) tab_consultationHoursList.notifyDataSetChanged();
                    if (mode == OCCUPIED_CONSULTATION_HOURS && tab_consultationHoursOccupiedList != null) tab_consultationHoursOccupiedList.notifyDataSetChanged();
                    if (mode == CONSULTATION_HOURS && tab_consultationHoursHistoryList != null) tab_consultationHoursHistoryList.notifyDataSetChanged();

                    if (mode == CONSULTATION_HOURS) areConsultationHoursReady = true;
                    else areOccupiedConsultationHoursReady = true;
                }
            }
        };

        return arrayResponse;
    }
}
