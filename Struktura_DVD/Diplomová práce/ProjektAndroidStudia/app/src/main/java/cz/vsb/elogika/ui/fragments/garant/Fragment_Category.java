package cz.vsb.elogika.ui.fragments.garant;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.dataTypes.Category;
import cz.vsb.elogika.dataTypes.Chapter;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.Logging;

public class Fragment_Category extends Fragment {

    ProgressBar progressBar;
    int selectedChapterPosition;

    /**
     * Inicializace UI
     */
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_category, container, false);
        Actionbar.newCategory(R.string.category, this);
        Logging.logAccess("/Pages/Other/Kategorie.aspx");
        progressBar = (ProgressBar) rootView.findViewById(R.id.category_progress);
        initizeAddButton(rootView);
        return rootView;
    }
    Spinner spinner;
    /**
     * PostInicializace UI
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        spinner = (Spinner) getActivity().findViewById(R.id.category_spinner);
        chapteradapter = new ArrayAdapter<Chapter>(getActivity(),
                R.layout.spinner_item, chapter_list);
        chapteradapter.setDropDownViewResource(R.layout.spinner_item);
        spinner.setAdapter(chapteradapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getKategoryFromServer(chapter_list.get(position).IDKapitola);
                selectedChapterPosition = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ListView listView = (ListView) getActivity().findViewById(R.id.category_listView);
        categoryadapter = new ArrayAdapter<Category>(getActivity(),R.layout.fragment_category_list_item, R.id.category_name, category_list) {
            @Override
            public View getView(final int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                //   Log.d("pozice", position + "");

                //  ((TextView) view.findViewById(R.id.category_name)).setText(category_list.get(position).Nazev);
                if (category_list.get(position).Popis.isEmpty())
                    view.findViewById(R.id.category_description).setVisibility(View.GONE);
                else{
                    view.findViewById(R.id.category_description).setVisibility(View.VISIBLE);
                    ((TextView) view.findViewById(R.id.category_description)).setText(category_list.get(position).Popis);
                }
                ((TextView) view.findViewById(R.id.category_equasion)).setText(category_list.get(position).Rovnice);
                ((TextView) view.findViewById(R.id.category_value)).setText(String.valueOf(category_list.get(position).Hodnota));

                if (view.getTag() == null){
                    ImageButton deletebutton = (ImageButton) view.findViewById(R.id.category_delete_button);
                    if(User.isGarant) {
                        deletebutton.setVisibility(View.VISIBLE);
                        deletebutton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                deleteCategory(category_list.get(position).IDKategorie, position);
                            }
                        });
                    }
                    view.setTag("already asociated");
                }
                return view;
            }
        };
        listView.setAdapter(categoryadapter);

        if(User.isGarant) {
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Logging.logAccess("/Pages/Other/KategorieDetail/KategorieForm.aspx", "edit");
                    getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, new Fragment_Category_Create().editCategory(chapteradapter, chapter_list, category_list.get(position), selectedChapterPosition)).commit();

                }
            });
        }


        getChaptersFromServer();

    }

    /**
     * přidá tlačítko po přidání nové kategorie
     *
     * @param rootView layout kam se tlačítko přidá
     */
    private void initizeAddButton(View rootView) {

        if(User.isGarant) {
            rootView.findViewById(R.id.category_add_new_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Logging.logAccess("/Pages/Other/KategorieDetail/KategorieForm.aspx", "add");
                    getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, new Fragment_Category_Create().addNewCategory(chapteradapter, chapter_list, selectedChapterPosition)).commit();
                }
            });
        }
        else{
            rootView.findViewById(R.id.category_add_new_button).setVisibility(View.GONE);
        }
    }

    ArrayList<Chapter> chapter_list = new ArrayList<Chapter>();
    ArrayAdapter<Chapter> chapteradapter;

    ArrayList<Category> category_list = new ArrayList<Category>();
    ArrayAdapter<Category> categoryadapter;
    int selectedChapterID = 0;


    /**
     * metoda pro získání kapitol ze serveru
     */
    private void getChaptersFromServer() {
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                //   savedResponse = response;
                //   fillChapterList(selectedChapterID);
                Log.d("getChaptersFromServer response", response.toString());
                if (response.length() > 0)
                {
                    for(int i = 0;i<response.length();i++)
                    {
                        Chapter c = new Chapter();
                        c.IDKapitola = response.getJSONObject(i).getInt("IDKapitola");
                        c.Nazev = response.getJSONObject(i).getString("Nazev");
                        c.Smazana = response.getJSONObject(i).getBoolean("Smazana");
                        chapter_list.add(c);
                    }
                    // nothinglayout.setVisibility(View.VISIBLE);
                }
                else{
                    Log.d("response", "is empty");
                    // progressDialog.setVisibility(View.GONE);
                }
                chapteradapter.notifyDataSetChanged();
                //TODO když se vtatím zpět na kategorie tak semi resetne výber
                // spinner.setSelection(selectedChapterPosition);
            }
        };
        //     Log.d("course", "id" + User.courseInfoId);
        Request.get(URL.Chapter.chaptersbycourse(User.courseInfoId), arrayResponse);
    }

    /**
     * metoda pro získání kategorii v zadané kapitole ze serveru
     *
     * @param IDKapitola id kapitoly
     *
     */
    private void getKategoryFromServer(int IDKapitola) {
        progressBar.setVisibility(View.VISIBLE);
        category_list.clear();
        selectedChapterID = IDKapitola;
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {

                Log.d("getKategoryFromServer", response.toString());
                if (response.length() > 0)
                {
                    for(int i = 0;i<response.length();i++)
                    {
                        Category c = new Category();
                        c.IDKategorie = response.getJSONObject(i).getInt("IDKategorie");
                        c.Nazev = response.getJSONObject(i).getString("Nazev");
                        c.Popis = response.getJSONObject(i).getString("Popis");
                        c.Rovnice = response.getJSONObject(i).getString("Rovnice");
                        c.Hodnota = response.getJSONObject(i).getInt("Hodnota");
                        c.Smazana = response.getJSONObject(i).getBoolean("Smazana");
                        c.IDKategoriePuv = response.getJSONObject(i).getInt("IDKategoriePuv");
                        category_list.add(c);

                    }
                    // nothinglayout.setVisibility(View.VISIBLE);
                }
                else
                    Log.d("response", "is empty");

                categoryadapter.notifyDataSetChanged();
                progressBar.setVisibility(View.GONE);
            }
        };
        Request.get(URL.Category.getCategories(IDKapitola), arrayResponse);
    }

    private void deleteCategory(final int IdCategory, final int position) {
        Response r = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException {
                categoryadapter.remove(category_list.get(position));
                categoryadapter.notifyDataSetChanged();
            }
        };
        Log.d("category remove request", "idkategorie: "+ IdCategory + ", pozice: " + position);
        Request.post(URL.Category.remove(IdCategory), r);
    }

}
