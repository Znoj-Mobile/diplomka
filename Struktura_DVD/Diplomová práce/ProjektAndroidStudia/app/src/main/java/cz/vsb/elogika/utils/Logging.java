package cz.vsb.elogika.utils;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;

/**
 * Třída se používá pro sledování uživatelů,
 * neboli na které obrazovky přistoupili.
 */
public class Logging
{
    /**
     * Zaloguje přístup.
     * @param url pro popis, kde se uživatel nachází, se používá url z webového rozhraní
     */
    public static void logAccess(String url)
    {
        sendLogAccess(url, "");
    }



    /**
     * Zaloguje přístup.
     * @param url pro popis, kde se uživatel nachází, se používá url z webového rozhraní
     * @param flag v případě, že se na webu něco nachází na jedné stránce a v této aplikaci je to rozsekané do více obrazovek, se pro dodatečné rozlišení dá použít tento string
     */
    public static void logAccess(String url, String flag)
    {
        sendLogAccess(url, flag);
    }



    /**
     * Odešle žádost na server.
     * @param url pro popis, kde se uživatel nachází, se používá url z webového rozhraní
     * @param flag v případě, že se na webu něco nachází na jedné stránce a v této aplikaci je to rozsekané do více obrazovek, se pro dodatečné rozlišení dá použít tento string
     */
    private static void sendLogAccess(String url, String flag)
    {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response)
            {}

            @Override
            public void onError(VolleyError error)
            {
                // POZOR! ZMĚNĚNO DEFAULTNÍ CHOVÁNÍ
                // NIC SE NESTANE
            }

            @Override
            public void onException(JSONException exception)
            {
                // POZOR! ZMĚNĚNO DEFAULTNÍ CHOVÁNÍ
                // NIC SE NESTANE
            }


        };
        JSONObject parameters = new JSONObject();
        try
        {
            parameters.put("UserId", User.id);
            parameters.put("RoleId", User.roleID);
            parameters.put("SkolaInfoId", User.schoolInfoID);
            parameters.put("KurzInfoId", User.courseInfoId);
            parameters.put("Cas", TimeLogika.currentServerTime());
            parameters.put("URL", url);
            parameters.put("IP", URL.getLocalIpAddress());
            parameters.put("Flag", flag);
            parameters.put("OS", "Android");
        }
        catch (JSONException e) {}

        Request.post(URL.User.LogAccess(), parameters, response);
    }
}
