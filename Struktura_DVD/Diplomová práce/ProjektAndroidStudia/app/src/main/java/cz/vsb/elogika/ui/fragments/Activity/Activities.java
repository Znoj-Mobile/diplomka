package cz.vsb.elogika.ui.fragments.Activity;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;

import org.json.JSONException;
import org.json.JSONObject;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.utils.DownloadFile;

public abstract class Activities extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private View mainView;

    LinearLayout nothinglayout;
    SwipeRefreshLayout swipeLayout;
    SimpleAdapter adapter;
    protected ListView listView;

    TextView sol_file_name;
    TextView sol_solution_chosen_file;

    protected String FileName = "";
    protected String FileType = "";
    protected String byteArray;


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_activities_pager, container, false);
        listView = (ListView) mainView.findViewById(R.id.activities_listView);

        swipeLayout = (SwipeRefreshLayout) mainView.findViewById(R.id.swipe_update);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeResources(R.color.darker_cyan, R.color.material_blue_grey_800);

        //swipeToRefresh pouze pokud je uplne viditelna prvni polozka seznamu
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                boolean enable = false;
                if (listView != null) {
                    if(listView.getChildCount() == 0){
                        enable = true;
                    }
                    else {
                        // check if the first item of the list is visible
                        boolean firstItemVisible = listView.getFirstVisiblePosition() == 0;
                        // check if the top of the first item is visible
                        boolean topOfFirstItemVisible = listView.getChildAt(0).getTop() == 0;
                        // enabling or disabling the refresh layout
                        enable = firstItemVisible && topOfFirstItemVisible;
                    }
                }
                swipeLayout.setEnabled(enable);
            }
        });

        nothinglayout = (LinearLayout) mainView.findViewById(R.id.nothing_to_show);
        nothinglayout.setVisibility(View.GONE);

        initialize();
        update();

        listView.setAdapter(adapter);

        return mainView;
    }

    protected abstract void initialize();
    protected abstract void list_detail();
    protected abstract void data_into_list();
    protected abstract void clear_list();

    protected void download(final String fileName, String activityId) {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                if (response.length() > 0)
                {
                    Log.d("download odpoved", response.toString());
                }
                else{
                    Log.d("Chyba", "Fragment_Activities download");
                }
                Intent intent = DownloadFile.download(fileName, response.getString("ReseniSoubor"));
                getActivity().startActivity(intent);
                SnackbarManager.show(
                        Snackbar.with(getActivity())
                                .type(SnackbarType.MULTI_LINE)
                                .duration(Snackbar.SnackbarDuration.LENGTH_INDEFINITE)
                                .text(getResources().getString(R.string.file) + " '" + fileName + "' " + getResources().getString(R.string.is_downloaded_in_download_folder))
                                .attachToAbsListView(listView), getActivity());
            }
        };

        Request.get(URL.SolvedActivities.getsolvedactivityfile(Integer.valueOf(activityId)), response);
    }

    public void update() {
        swipeLayout.post(new Runnable() {
            @Override
            public void run() {
                nothinglayout.setVisibility(View.GONE);
                swipeLayout.setRefreshing(true);
                clear_list();
                adapter.notifyDataSetChanged();
                data_into_list();
            }
        });
    }

    @Override
    public void onRefresh() {
        update();
    }
}
