package cz.vsb.elogika.utils;

import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public class Hash
{
    /**
     * Na základě hesla a soli vytvoří hash.
     * @param password
     * @param salt
     * @return
     */
    public static String getHashedPassword(String password, String salt)
    {
        String hashWithNewLine = Base64.encodeToString(md5InBytes(password + salt), Base64.DEFAULT);
        return hashWithNewLine.substring(0, hashWithNewLine.length() - 1);
    }


    /**
     * Zahashuje text md5 hashem.
     * @param text
     * @return pole bytů
     */
    public static byte[] md5InBytes(String text)
    {
        try
        {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.update(text.getBytes("UTF-8"), 0, text.length()); // Použito UTF-8, v případě např. ruských znaků by mohlo dělat potíže
            return messageDigest.digest();
        }
        catch (NoSuchAlgorithmException e) {}
        catch (UnsupportedEncodingException e) {}
        return null;
    }


    /**
     * Vytvoří náhodný řetězec.
     * @return
     */
    public static String getSalt()
    {
        Random random = new Random();
        random.setSeed(System.currentTimeMillis());
        byte[] buffer = new byte[6];
        random.nextBytes(buffer);
        String salt = Base64.encodeToString(buffer, Base64.DEFAULT);
        return salt.substring(0, salt.length() - 1);
    }


    /**
     *
     * @param text
     * @return
     */
    public static String sha1(String text)
    {
        MessageDigest messageDigest = null;
        try
        {
            messageDigest = MessageDigest.getInstance("SHA-1");
            messageDigest.update(text.getBytes("UTF-8"), 0, text.length()); // Použito UTF-8, v případě např. ruských znaků by mohlo dělat potíže
            byte[] data = messageDigest.digest();

            StringBuilder buffer = new StringBuilder();
            for (byte b : data)
            {
                int halfByte = (b >>> 4) & 0x0F;
                int twoHalfs = 0;
                do
                {
                    buffer.append((0 <= halfByte) && (halfByte <= 9) ? (char) ('0' + halfByte) : (char) ('a' + (halfByte - 10)));
                    halfByte = b & 0x0F;
                } while (twoHalfs++ < 1);
            }

            return buffer.toString();
        }
        catch (NoSuchAlgorithmException e) {}
        catch (UnsupportedEncodingException e) {}

        return "";
    }
}
