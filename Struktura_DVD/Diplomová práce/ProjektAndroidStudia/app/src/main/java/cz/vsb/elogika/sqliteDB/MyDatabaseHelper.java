package cz.vsb.elogika.sqliteDB;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Třída pro komunikaci se systémovým rozhraním k databázi
 */
public class MyDatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "ElogikaOfflineTests";

    private static final int DATABASE_VERSION = 6;

    // Database creation sql statement
    private static final String TEST_TABLE = "create table  "+ MyDB.TEST_TABLE_NAME+"( "+
            "_id integer primary key," +
            MyDB.TEST_ID + " integer, " +
            MyDB.TEST_USER_ID + " integer, " +
            MyDB.TEST_GENERATED_ID + " integer, " +
            MyDB.TEST_TIME_MINUTES + " integer, " +
            MyDB.TEST_NAME + " text, " +
            MyDB.TEST_ACTIVITIESGROUP + " text, " +
            MyDB.TEST_TIME_FROM + " text, " +
            MyDB.TEST_TIME_TO + " text, " +
            MyDB.TEST_DATE_SEND_TO + " text, " +
            MyDB.TEST_MINPOINTS + " text, " +
            MyDB.TEST_MAXPOINTS + " text, " +
            MyDB.TEST_MANDATORY + " boolean, " +
            MyDB.SCHOOL_INFO_ID + " integer, " +
            MyDB.TEST_INFO + " text " +
            ");";

    private static final String QUESTION_TABLE = "create table "+ MyDB.QUESTION_TABLE_NAME+"( " +
            "_id integer primary key," +
            MyDB.TEST_GENERATED_ID + " integer, " +
            MyDB.QUESTION_ID + " integer, " +
            MyDB.QUESTION_JSON + " text " +
            ");";
    private static final String OPTION_TABLE = "create table "+ MyDB.OPTION_TABLE_NAME+"( " +
            "_id integer primary key," +
            MyDB.TEST_GENERATED_ID + " integer, " +
            MyDB.QUESTION_ID + " integer, " +
            MyDB.OPTION_ID + " integer, " +
            MyDB.OPTION_JSON + " text " +
            ");";
    private static final String UNSAVED_TABLE = "create table "+ MyDB.UNSAVED_TABLE_NAME+"( " +
            "_id integer primary key," +
            MyDB.TEST_GENERATED_ID + " integer, " +
            MyDB.UNSAVED_NAME + " text, " +
            MyDB.UNSAVED_SAVE + " text, " +
            MyDB.UNSAVED_OTAZKA + " text, " +
            MyDB.UNSAVED_ODPOVED + " text, " +
            MyDB.TEST_DATE_SEND_TO + " text, " +
            MyDB.UNSAVED_LOG + " text " +
            ");";


    public MyDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Metoda volaná při první vytvoření databáze v telefonu
     * vytvoří odpovídající tabulky
     * @param database objekt databaze
     */
    @Override
    public void onCreate(SQLiteDatabase database) {
        Log.d("elogika databaze", " vytvarim tabulky");
        database.execSQL(TEST_TABLE);
        database.execSQL(QUESTION_TABLE);
        database.execSQL(OPTION_TABLE);
        database.execSQL(UNSAVED_TABLE);
        Log.d("elogika databaze", " tabulky vytvořeny");
    }

    /**
     * metoda volaná při změně databáze v telefonu na novější verzi
     * @param database objekt databaze
     * @param oldVersion id staré verze
     * @param newVersion id nové verze
     */
    @Override
    public void onUpgrade(SQLiteDatabase database,int oldVersion,int newVersion){
        Log.d(MyDatabaseHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        dropALLtables(database);
        onCreate(database);
    }


    /**
     * Metoda pro kompletní vymazání databáze
     * @param database objekt databaze
     */
    private void dropALLtables(SQLiteDatabase database)
    {
        database.execSQL("DROP TABLE IF EXISTS " + MyDB.TEST_TABLE_NAME);
        database.execSQL("DROP TABLE IF EXISTS " + MyDB.QUESTION_TABLE_NAME);
        database.execSQL("DROP TABLE IF EXISTS " + MyDB.OPTION_TABLE_NAME);
        database.execSQL("DROP TABLE IF EXISTS " + MyDB.UNSAVED_TABLE_NAME);
    }
}
