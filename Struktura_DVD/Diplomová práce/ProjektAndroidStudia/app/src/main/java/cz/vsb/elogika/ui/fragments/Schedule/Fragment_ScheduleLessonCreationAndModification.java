package cz.vsb.elogika.ui.fragments.Schedule;

import android.app.Fragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TimePicker;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import org.json.JSONException;
import org.json.JSONObject;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.dataTypes.Lesson;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.Logging;

public class Fragment_ScheduleLessonCreationAndModification extends Fragment
{
    private int timetableID;
    private EditText lessonName;
    private EditText lessonShortcut;
    private EditText tutorOrTeacher;
    private EditText room;
    private Spinner day;
    private CheckBox oddWeek;
    private CheckBox evenWeek;
    private RadioButton lecture;
    private RadioButton exercise;
    private Button roundButton;
    private int fromHours;
    private int fromMinutes;
    private int toHours;
    private int toMinutes;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        Actionbar.addSubcategory(R.string.create_own_lesson, this);
        Logging.logAccess("/Pages/Student/RozvrhDetail/RozvrhZapisVlastni.aspx");

        View rootView = inflater.inflate(R.layout.fragment_schedule_lesson_creation_and_modification, container, false);
        LinearLayout rootLayout = (LinearLayout) rootView.findViewById(R.id.rootLayout);

        final Bundle bundle = getArguments();
        Lesson lesson = null;
        if (bundle != null) lesson = (new Gson()).fromJson(bundle.getString("lesson"), Lesson.class);

        this.lessonName = (EditText) rootLayout.findViewById(R.id.lesson_name);
        this.lessonShortcut = (EditText) rootLayout.findViewById(R.id.lesson_shortcut);
        this.tutorOrTeacher = (EditText) rootLayout.findViewById(R.id.tutor_or_teacher);
        this.room = (EditText) rootLayout.findViewById(R.id.room);
        final Button startButton = (Button) rootLayout.findViewById(R.id.lesson_start);
        final Button endButton = (Button) rootLayout.findViewById(R.id.lesson_end);
        this.day = (Spinner) rootLayout.findViewById(R.id.day);
        this.oddWeek = (CheckBox) rootLayout.findViewById(R.id.odd_week);
        this.evenWeek = (CheckBox) rootLayout.findViewById(R.id.even_week);
        this.lecture = (RadioButton) rootLayout.findViewById(R.id.lecture);
        this.exercise = (RadioButton) rootLayout.findViewById(R.id.exercise);

        day.setAdapter(new ArrayAdapter(getActivity(), R.layout.schedule_spinner, new String[]{getResources().getString(R.string.monday), getResources().getString(R.string.tuesday), getResources().getString(R.string.wednesday), getResources().getString(R.string.thursday), getResources().getString(R.string.friday), getResources().getString(R.string.saturday)}));

        oddWeek.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (!oddWeek.isChecked() && !evenWeek.isChecked()) evenWeek.setChecked(true);
            }
        });
        evenWeek.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (!evenWeek.isChecked() && !oddWeek.isChecked()) oddWeek.setChecked(true);
            }
        });

        if (lesson != null)
        {
            this.timetableID = lesson.timetableID;
            this.lessonName.setText(lesson.name);
            this.lessonShortcut.setText(lesson.shortcut);
            this.tutorOrTeacher.setText(lesson.teacherName);
            this.room.setText(lesson.room);
            day.setSelection(lesson.day);
            oddWeek.setChecked(lesson.week.contentEquals("Lichý") || lesson.week.contentEquals("Každý"));
            evenWeek.setChecked(lesson.week.contentEquals("Sudý") || lesson.week.contentEquals("Každý"));
            if (lesson.type == 1) exercise.setChecked(true);

            String[] parts = lesson.from.split(":");
            fromHours = Integer.parseInt(parts[0]);
            fromMinutes = Integer.parseInt(parts[1]);
            startButton.setText(getTime(fromHours, fromMinutes));

            parts = lesson.to.split(":");
            toHours = Integer.parseInt(parts[0]);
            toMinutes = Integer.parseInt(parts[1]);
            endButton.setText(getTime(toHours, toMinutes));
        }

        startButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                TimePickerDialog.OnTimeSetListener listener = new TimePickerDialog.OnTimeSetListener()
                {
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute)
                    {
                        fromHours = hourOfDay;
                        fromMinutes = minute;
                        startButton.setText(getTime(fromHours, fromMinutes));
                    }
                };
                new TimePickerDialog(getActivity(), listener, fromHours, fromMinutes, true).show();
            }
        });

        endButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                TimePickerDialog.OnTimeSetListener listener = new TimePickerDialog.OnTimeSetListener()
                {
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute)
                    {
                        toHours = hourOfDay;
                        toMinutes = minute;
                        endButton.setText(getTime(toHours, toMinutes));
                    }
                };
                new TimePickerDialog(getActivity(), listener, toHours, toMinutes, true).show();
            }
        });

        this.roundButton = (Button) rootView.findViewById(R.id.roundButton);
        this.roundButton.setText("✔");
        this.roundButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                roundButton.setEnabled(false);
                boolean everythingOK = true;
                if (lessonName.length() == 0)
                {
                    SnackbarManager.show(Snackbar.with(getActivity()).text(R.string.schedule_error_lesson_name_empty));
                    everythingOK = false;
                }
                if (lessonShortcut.length() == 0)
                {
                    SnackbarManager.show(Snackbar.with(getActivity()).text(R.string.schedule_error_lesson_shortcut_empty));
                    everythingOK = false;
                }
                if (tutorOrTeacher.length() == 0)
                {
                    SnackbarManager.show(Snackbar.with(getActivity()).text(R.string.schedule_error_lesson_tutor_empty));
                    everythingOK = false;
                }
                if (room.length() == 0)
                {
                    SnackbarManager.show(Snackbar.with(getActivity()).text(R.string.schedule_error_lesson_room_empty));
                    everythingOK = false;
                }
                if (!everythingOK)
                {
                    roundButton.setEnabled(true);
                    return;
                }
                else
                {
                    Response response = new Response()
                    {
                        @Override
                        public void onResponse(JSONObject response) throws JSONException
                        {
                            // bundle == null -> jedná se o vytvoření nové hodiny
                            // bundle != null -> jedná se o editaci
                            createOrUpdateLesson(bundle == null, response.getInt("IDSemestr"));
                        }

                        @Override
                        public void onError(VolleyError error)
                        {
                            super.onError(error);
                            roundButton.setEnabled(true);
                        }

                        @Override
                        public void onException(JSONException exception)
                        {
                            super.onException(exception);
                            roundButton.setEnabled(true);
                        }
                    };
                    Request.get(URL.Semester.SemestersByCourseInfo(User.courseInfoId), response);
                }
            }
        });

        return rootView;
    }


    /**
     * Vytvoří nebo upraví hodinu.
     * @param isCreation vytvořit? (nebo upravit)
     * @param semesterID
     */
    private void createOrUpdateLesson(final boolean isCreation, int semesterID)
    {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                if (response.getBoolean("Success"))
                {
                    Logging.logAccess("/Pages/Student/RozvrhDetail/RozvrhSummary.aspx");
                    SnackbarManager.show(Snackbar.with(getActivity()).text(isCreation ? R.string.lesson_was_successfully_created : R.string.lesson_was_successfully_edited));
                    Actionbar.jumpBack();
                }
                else
                {
                    SnackbarManager.show(Snackbar.with(getActivity()).text(R.string.lesson_was_not_created));
                    roundButton.setEnabled(true);
                }
            }

            @Override
            public void onError(VolleyError error)
            {
                super.onError(error);
                roundButton.setEnabled(true);
            }

            @Override
            public void onException(JSONException exception)
            {
                super.onException(exception);
                roundButton.setEnabled(true);
            }
        };
        JSONObject parameters = new JSONObject();
        try
        {
            if (!isCreation) parameters.put("IDRozvrh", this.timetableID);
            parameters.put("Predmet", this.lessonName.getText().toString());
            parameters.put("Zkratka", this.lessonShortcut.getText().toString());
            parameters.put("Jmeno", this.tutorOrTeacher.getText().toString());
            parameters.put("Ucebna", this.room.getText().toString());
            parameters.put("Den", this.day.getSelectedItem().toString());
            parameters.put("HodinaOD", getTime(fromHours, fromMinutes));
            parameters.put("HodinaDO", getTime(toHours, toMinutes));
            String week;
            if (this.oddWeek.isChecked() && this.evenWeek.isChecked()) week = "Každý";
            else week = this.oddWeek.isChecked() ? "Lichý" : "Sudý";
            parameters.put("Tyden", week);
            parameters.put("Typ", this.lecture.isChecked() ? 0 : 1);
        }
        catch (JSONException e) {}
        if (isCreation) Request.post(URL.Timetable.TimetableInsert(User.id, semesterID), parameters, response);
        else Request.post(URL.Timetable.TimetableUpdate(), parameters, response);
    }



    private String getTime(int hours, int minutes)
    {
        return ((hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes));
    }
}
