package cz.vsb.elogika.ui.fragments.garant.emails;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import cz.vsb.elogika.R;
import cz.vsb.elogika.ui.fragments.garant.classes.Fragment_classes_list_of_students;
import cz.vsb.elogika.utils.DividerItemDecoration;
import cz.vsb.elogika.utils.EnumLogika;

public class Fragment_email_classes_list extends Fragment implements SwipeRefreshLayout.OnRefreshListener{
    private View mainView;
    private View rootView;
    private RecyclerView recyclerView;

    SwipeRefreshLayout swipeLayout;

    protected RecyclerView.Adapter adapter;


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mainView = inflater.inflate(R.layout.fragment_email_classes_list, container, false);
        rootView = mainView.findViewById(R.id.recycler_view_swipe);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);

        Fragment_email_add.nothinglayout = (LinearLayout) rootView.findViewById(R.id.nothing_to_show);
        Fragment_email_add.nothinglayout.setVisibility(View.GONE);

        swipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_update);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeResources(R.color.darker_cyan, R.color.material_blue_grey_800);

        this.adapter = new RecyclerView.Adapter() {
            class ViewHolderHeader extends RecyclerView.ViewHolder {
                public ViewHolderHeader(View itemView) {
                    super(itemView);
                }
            }

            class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
                public CheckBox checkbox;
                public TextView name;
                public TextView shortcut;
                public TextView lecture_room;
                public TextView leasson;
                public TextView day;
                public TextView week;
                public TextView study_form;
                public TextView class_type;
                public Button btn_students;

                public ViewHolder(View itemView) {
                    super(itemView);
                    itemView.setOnClickListener(this);
                    this.checkbox = (CheckBox) itemView.findViewById(R.id.email_list_checkbox);
                    this.name = (TextView) itemView.findViewById(R.id.email_list_name);
                    this.shortcut = (TextView) itemView.findViewById(R.id.email_list_shortcut);
                    this.lecture_room = (TextView) itemView.findViewById(R.id.email_list_lecture_room);
                    this.leasson = (TextView) itemView.findViewById(R.id.email_list_leasson);
                    this.day = (TextView) itemView.findViewById(R.id.email_list_day);
                    this.week = (TextView) itemView.findViewById(R.id.email_list_week);
                    this.study_form = (TextView) itemView.findViewById(R.id.email_list_study_form);
                    this.class_type = (TextView) itemView.findViewById(R.id.email_list_class_type);
                    this.btn_students = (Button)  itemView.findViewById(R.id.email_list_students);
                }


                @Override
                public void onClick(View view) {
                    //int position = getLayoutPosition();
                    checkbox.setChecked(!checkbox.isChecked());
                }
            }

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view;
                switch (viewType) {
                    case 1:
                        view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_email_classes_list_list, parent, false);
                        return new ViewHolder(view);
                    default:
                        return null;
                }
            }
            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
                if (holder.getItemViewType() == 1) {
                    final ViewHolder viewHolder = (ViewHolder) holder;

                    viewHolder.name.setText(Fragment_email_add.lesson_list.get(position).get("nazev"));
                    viewHolder.shortcut.setText(Fragment_email_add.lesson_list.get(position).get("zkratkaPredmet"));
                    viewHolder.lecture_room.setText(Fragment_email_add.lesson_list.get(position).get("ucebna"));
                    viewHolder.leasson.setText(Fragment_email_add.lesson_list.get(position).get("hodinaOD") + " - " + Fragment_email_add.lesson_list.get(position).get("hodinaDO"));
                    viewHolder.day.setText(Fragment_email_add.lesson_list.get(position).get("den"));
                    viewHolder.week.setText(Fragment_email_add.lesson_list.get(position).get("tyden"));
                    viewHolder.study_form.setText(EnumLogika.formaStudia(Fragment_email_add.lesson_list.get(position).get("formaStudia")));
                    viewHolder.class_type.setText(EnumLogika.typTridy(Fragment_email_add.lesson_list.get(position).get("typ")));

                    final int finalPosition = position;
                    if(Fragment_email_add.lesson_list.get(finalPosition).get("checked").equals("true")){
                        viewHolder.checkbox.setChecked(true);
                    }
                    else{
                        viewHolder.checkbox.setChecked(false);
                    }
                    viewHolder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            if(viewHolder.checkbox.isChecked()){
                                Fragment_email_add.lesson_list.get(finalPosition).put("checked", "true");
                            }
                            else{
                                Fragment_email_add.lesson_list.get(finalPosition).put("checked", "false");
                            }
                        }
                    });
                    //todo
                    viewHolder.btn_students.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            Bundle bundle = new Bundle();
                            bundle.putString("nazev", Fragment_email_add.lesson_list.get(finalPosition).get("nazev"));
                            bundle.putString("classId", Fragment_email_add.lesson_list.get(finalPosition).get("idTrida"));
                            bundle.putBoolean("email", true);
                            Fragment_classes_list_of_students fragment_classes_list_of_students = new Fragment_classes_list_of_students();
                            fragment_classes_list_of_students.setArguments(bundle);
                            getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment_classes_list_of_students).commit();

                        }
                    });
                }
            }

            @Override
            public int getItemViewType(int position)
            {
                return 1;
            }

            @Override
            public int getItemCount() {
                return Fragment_email_add.lesson_list.size();
            }
        };

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        recyclerView.setHasFixedSize(true);
        //zaruci, ze se bude seznam aktualizovat jen pri swipu uplne nahore...
        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy <= 0) {
                    swipeLayout.setEnabled(true);
                } else {
                    swipeLayout.setEnabled(false);
                }
            }
        });
        recyclerView.setAdapter(adapter);

        Fragment_email_add.update(adapter, swipeLayout);

        return mainView;
    }

    @Override
    public void onRefresh() {
        Fragment_email_add.update(adapter, swipeLayout);
    }
}
