package cz.vsb.elogika.ui.fragments;

import android.app.Dialog;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.DatePickerDialogOnButton;
import cz.vsb.elogika.utils.DividerItemDecoration;
import cz.vsb.elogika.utils.DownloadFile;
import cz.vsb.elogika.utils.Logging;
import cz.vsb.elogika.utils.SelectFileToUpload;
import cz.vsb.elogika.utils.TimeLogika;
import cz.vsb.elogika.utils.TimePickerDialogOnButton;

/**
 * Fragment pro zobrazení Aktualit a také jejich editaci u garanta
 */
public class Fragment_News extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private View mainView;
    private View rootView;
    public ArrayList<Map<String, String>> itemList = new ArrayList<>();
    public RecyclerView.Adapter adapter;
    //private ListView mylist;
    private RecyclerView recyclerView;
    LinearLayout nothinglayout;
    private SwipeRefreshLayout swipeLayout;

    /**
     * Inicializace UI
     */
        @Override
        public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                                 Bundle savedInstanceState) {
            mainView = inflater.inflate(R.layout.fragment_news, container, false);

            Actionbar.newCategory(R.string.news, this);
            if (User.isGarant) Logging.logAccess("/Pages/Other/News.aspx");
            else Logging.logAccess("/Pages/Other/NewsLonger.aspx", "Actuality");

            rootView = mainView.findViewById(R.id.recycler_view_with_button_swipe);
            recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
            //mylist = (ListView) mainView.findViewById(R.id.fragment_news_listView);

            swipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_update);
            swipeLayout.setOnRefreshListener(this);
            swipeLayout.setColorSchemeResources(R.color.darker_cyan, R.color.material_blue_grey_800);

            nothinglayout = (LinearLayout) rootView.findViewById(R.id.nothing_to_show);
            nothinglayout.setVisibility(View.GONE);

            this.adapter = new RecyclerView.Adapter() {

                class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
                    public TextView date;
                    public TextView name;
                    public TextView text;
                    public Button editButton;
                    public LinearLayout llAttachment;

                    public ViewHolder(View itemView) {
                        super(itemView);
                        itemView.setOnClickListener(this);
                        this.date = (TextView) itemView.findViewById(R.id.news_time_view);
                        this.name = (TextView) itemView.findViewById(R.id.news_name_text_view);
                        this.text = (TextView) itemView.findViewById(R.id.news_text_text_view);
                        this.editButton = (Button) itemView.findViewById(R.id.news_edit_button);
                        this.llAttachment = (LinearLayout) itemView.findViewById(R.id.news_attachment);
                    }


                    @Override
                    public void onClick(View view) {
                        //final int position = getAdapterPosition();
                    }
                }

                @Override
                public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                    View view;
                    switch (viewType) {
                        case 0:
                            view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_news_item, parent, false);
                            return new ViewHolder(view);
                        default:
                            return null;
                    }
                }
                @Override
                public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
                    if (holder.getItemViewType() == 0) {
                        final int finalPosition = holder.getAdapterPosition();
                        final ViewHolder viewHolder = (ViewHolder) holder;

                        viewHolder.date.setText(itemList.get(position).get("date"));
                        viewHolder.name.setText(itemList.get(position).get("name"));
                        viewHolder.text.setText(itemList.get(position).get("text"));
                        //todo
                        viewHolder.llAttachment.removeAllViewsInLayout();
                        LinearLayout attachmentLayout = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.fragment_news_attachmenttext, null);
                        try {
                            if (savedResponse.getJSONObject(finalPosition).getJSONArray("Attachments").length() > 0) {
                                viewHolder.llAttachment.setMinimumHeight((savedResponse.getJSONObject(finalPosition).getJSONArray("Attachments").length() - 1) * attachmentLayout.getHeight());
                            }
                            else{
                                viewHolder.llAttachment.setMinimumHeight(0);
                            }
                            showAttachments(null, viewHolder.llAttachment, viewHolder.getAdapterPosition(), false);
                            if (User.isGarant || User.isTutor) {
                                viewHolder.editButton.setVisibility(View.VISIBLE);
                                viewHolder.editButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        //edituje novinku
                                        createDialog(true, viewHolder.getAdapterPosition());
                                    }
                                });
                            } else {
                                viewHolder.editButton.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public int getItemViewType(int position)
                {
                    return 0;
                }

                @Override
                public int getItemCount() {
                    //velikost listu pro vyplneni tabulky
                    return itemList.size();
                }
            };


            if(User.isGarant || User.isTutor){  //vytvoří dialog pro přidání nové novinky
                rootView.findViewById(R.id.roundButton).setVisibility(View.VISIBLE);
                 ((Button) rootView.findViewById(R.id.roundButton)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        createDialog(false, -1);
                    }
                });
            }
            else{
                rootView.findViewById(R.id.roundButton).setVisibility(View.GONE);
            }

            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
            recyclerView.setHasFixedSize(true);
            //zaruci, ze se bude seznam aktualizovat jen pri swipu uplne nahore...
            recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    if (dy <= 0) {
                        swipeLayout.setEnabled(true);
                    } else {
                        swipeLayout.setEnabled(false);
                    }
                }
            });
            recyclerView.setAdapter(adapter);

            update();
            return mainView;
        }

    /**
     * vytvoří dialog pro přidání nebo upravu aktuality včetně přidávání příloh
     *
     * @param edit rozhoduje jestli je přidáváno nebo upravováno
     * @param position je-li upravováno pak je třeba poslat pozici na kterou bylo kliknuto
     *
     */
    private void createDialog(final boolean edit, final int position){
        if (edit)
            Logging.logAccess("/Pages/Other/NewsDetail/NewsForm.aspx", "edit");
        else
            Logging.logAccess("/Pages/Other/NewsDetail/NewsForm.aspx", "add");
        //position -1 = nová novinka
        final Dialog d = new Dialog(getActivity());

        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView( R.layout.fragment_news_item_edit );

        final Button datePickerViewFrom = (Button) d.findViewById(R.id.news_datepicker_activefrom);
        final Button datePickerViewTo = (Button) d.findViewById(R.id.news_datepicker_activeto);
        final Button timePickerViewFrom = (Button) d.findViewById(R.id.news_timepicker_activefrom);
        final Button timePickerViewTo = (Button) d.findViewById(R.id.news_timepicker_activeto);


        DatePickerDialogOnButton datePickerFrom;
        DatePickerDialogOnButton datePickerTo;
        TimePickerDialogOnButton timePickerFrom;
        TimePickerDialogOnButton timePickerTo;

        final TextView title = (TextView)d.findViewById(R.id.news_name_edit_text);
        final TextView text = (TextView)d.findViewById(R.id.news_text_edit_text);

        datePickerFrom = new DatePickerDialogOnButton(datePickerViewFrom);
        timePickerFrom = new TimePickerDialogOnButton(timePickerViewFrom);
        datePickerTo= new DatePickerDialogOnButton(datePickerViewTo);
        timePickerTo = new TimePickerDialogOnButton(timePickerViewTo);

        if(edit){
            try {
                title.setText(savedResponse.getJSONObject(position).getString("Name"));
                text.setText(savedResponse.getJSONObject(position).getString("Text"));

                int[] dateInserteds = TimeLogika.getSeparatedTime(
                        TimeLogika.getFormatedDateTime(
                                savedResponse.getJSONObject(position).getString("ValidFrom")));
                datePickerFrom = new DatePickerDialogOnButton(datePickerViewFrom,dateInserteds[0],dateInserteds[1],dateInserteds[2]);
                timePickerFrom = new TimePickerDialogOnButton(timePickerViewFrom,dateInserteds[3],dateInserteds[4]);

                dateInserteds = TimeLogika.getSeparatedTime(
                        TimeLogika.getFormatedDateTime(
                                savedResponse.getJSONObject(position).getString("ValidTo")));
                datePickerTo = new DatePickerDialogOnButton(datePickerViewTo,dateInserteds[0],dateInserteds[1],dateInserteds[2]);
                timePickerTo = new TimePickerDialogOnButton(timePickerViewTo,dateInserteds[3],dateInserteds[4]);

             } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        final DatePickerDialogOnButton finalDatePickerFrom = datePickerFrom;
        final TimePickerDialogOnButton finalTimePickerFrom = timePickerFrom;
        final DatePickerDialogOnButton finalDatePickerTo = datePickerTo;
        final TimePickerDialogOnButton finalTimePickerTo = timePickerTo;

        d.findViewById(R.id.news_confrim_new_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                swipeLayout.post(new Runnable() {
                    @Override
                    public void run() {
                        swipeLayout.setRefreshing(true);
                    }
                });

                Response r = new Response() {

                    @Override
                    public void onResponse(JSONObject response) throws JSONException
                    {
                        Log.d("update new request", response.toString());
                        if (response.getString("Success").equals("true")) {
                            //update
                            if(edit){
                                SnackbarManager.show(
                                        Snackbar.with(getActivity())
                                                .text(getActivity().getString(R.string.one_news) + " " + title.getText().toString() + " " + getActivity().getString(R.string.one_news_was_updated)));
                            }
                            //insert
                            else{
                                SnackbarManager.show(
                                        Snackbar.with(getActivity())
                                                .text(getActivity().getString(R.string.one_news) + " " + title.getText().toString() + " " + getActivity().getString(R.string.one_news_was_inserted)));
                            }

                        } else { //nepovedlo se
                            Log.d("Fragment_news", "update / insert news: " + response.toString());
                            //update
                            if(edit){
                                SnackbarManager.show(
                                        Snackbar.with(getActivity())
                                                .text(getActivity().getString(R.string.one_news_4_p) + " " + title.getText().toString() + " " + getActivity().getString(R.string.one_news_was_not_updated)));
                            }
                            //insert
                            else{
                                SnackbarManager.show(
                                        Snackbar.with(getActivity())
                                                .text(getActivity().getString(R.string.one_news_4_p) + " " + title.getText().toString() + " " + getActivity().getString(R.string.one_news_was_not_inserted)));
                            }
                        }
                        //TODO loading a pak close ((LinearLayout)attachmentLayout.getParent()).removeView(attachmentLayout);
                        getNewsFromServer();
                        d.cancel();
                        swipeLayout.setRefreshing(false);
                    }

                };
                try {

                    JSONObject parameters = new JSONObject();
                    if(edit){
                        parameters.put("IDNews",savedResponse.getJSONObject(position).getInt("IDNews"));
                        parameters.put("IDUserInserted",savedResponse.getJSONObject(position).getInt("IDUserInserted"));
                        parameters.put("DateInserted",savedResponse.getJSONObject(position).getString("DateInserted"));
                    }
                    else
                    {
                        parameters.put("IDNews",null);
                        parameters.put("IDUserInserted",User.id);
                        parameters.put("DateInserted",TimeLogika.currentServerTime());
                    }


                    parameters.put("ValidFrom",finalDatePickerFrom.getDate(finalTimePickerFrom));
                    parameters.put("ValidTo",finalDatePickerTo.getDate(finalTimePickerTo));
                    parameters.put("Name",title.getText());
                    parameters.put("Text",text.getText());
                    parameters.put("IDUserModified",User.id);
                    parameters.put("DateModified",TimeLogika.currentServerTime());
                    parameters.put("Attachments",new JSONArray());
                    parameters.put("IDSchoolInfos",new JSONArray());


                    Log.d("update new request", parameters.toString());

                    if(edit)
                        Request.post(URL.News.update(), parameters, r);
                    else
                        Request.post(URL.News.insert(User.schoolInfoID, User.courseInfoId), parameters, r);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        if(edit){
            ImageButton deletenew = (ImageButton) d.findViewById(R.id.deletenewbutton);
            deletenew.setVisibility(View.VISIBLE);
            deletenew.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    swipeLayout.post(new Runnable() {
                        @Override
                        public void run() {
                            swipeLayout.setRefreshing(true);
                        }
                    });

                    Response r = new Response() {
                        @Override
                        public void onResponse(JSONObject response) throws JSONException {
                            Log.d("remove new response", response.toString());
                            if (response.getString("Success").equals("true")) {
                                    SnackbarManager.show(
                                            Snackbar.with(getActivity())
                                                    .text(getActivity().getString(R.string.one_news) + " " + title.getText().toString() + " " + getActivity().getString(R.string.one_news_was_deleted)));

                            } else { //nepovedlo se
                                Log.d("Fragment_news", "delete news: " + response.toString());
                                    SnackbarManager.show(
                                            Snackbar.with(getActivity())
                                                    .text(getActivity().getString(R.string.one_news_4_p) + " " + title.getText().toString() + " " + getActivity().getString(R.string.one_news_was_not_deleted)));
                            }
                            //TODO loading a pak close ((LinearLayout)attachmentLayout.getParent()).removeView(attachmentLayout);
                            update();
                            d.cancel();
                            swipeLayout.setRefreshing(false);
                        }

                    };

                    try {
                        JSONObject parameters = new JSONObject();
                        parameters.put("newsId",savedResponse.getJSONObject(position).getInt("IDNews"));
                        parameters.put("schoolInfoId", User.schoolInfoID);
                        Log.d("delete new", parameters.toString());
                        Request.get(URL.News.remove(savedResponse.getJSONObject(position).getInt("IDNews"), User.courseInfoId), r);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
        });
        }
        showAttachments(d,(LinearLayout) d.findViewById(R.id.news_edit_linearlayout), position, edit);
        d.show();
    }

    /**
     * Ke každé aktualitě připojí seznam dostupných přiloh
     *
     * @param d je-li otevřen dialog s aktualitou, musí zde být poslan odkaz pro vložení nové
     * @param itemlayout layout přílohy
     * @param position pozice aktiality v seznamu
     * @param edit  jedná-li se o editaci nebo ne
     *
     */
    private void showAttachments( final Dialog d,LinearLayout itemlayout, final int position, boolean edit) {
        try {
            if (position != -1 && savedResponse.getJSONObject(position).getJSONArray("Attachments").length() > 0)
            {
                    for (int j = 0; j < savedResponse.getJSONObject(position).getJSONArray("Attachments").length(); j++)
                    {
                        final int k = j;
                        final LinearLayout attachmentLayout = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.fragment_news_attachmenttext,null);
                        ((TextView)attachmentLayout.findViewById(R.id.attachmenttext)).setText(savedResponse.getJSONObject(position).getJSONArray("Attachments").getJSONObject(j).getString("FileName"));

                        if (edit){ //přidá ikonu delete a přiřadí ji funkčnost
                            View delete = attachmentLayout.findViewById(R.id.attachment_delete);
                            delete.setVisibility(View.VISIBLE);
                            delete.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Response r = new Response() {

                                        @Override
                                        public void onResponse(JSONObject response) throws JSONException
                                        {
                                            Log.d("attachment delete", response.toString());
                                            ((LinearLayout)attachmentLayout.getParent()).removeView(attachmentLayout);
                                        }

                                    };
                                    try {
                                        JSONObject parameters = new JSONObject();
                                        parameters.put("attachmentId",savedResponse.getJSONObject(position).getJSONArray("Attachments").getJSONObject(k).getInt("IDNewsAttachment") );
                                        Request.post(URL.News.deleteattachment(savedResponse.getJSONObject(position).getJSONArray("Attachments").getJSONObject(k).getInt("IDNewsAttachment")), parameters, r);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }

                        attachmentLayout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                attachmentLayout.setClickable(false);
                                attachmentLayout.findViewById(R.id.progressbar).setVisibility(View.VISIBLE);
                                Response r = new Response() {

                                    @Override
                                    public void onResponse(JSONObject response) throws JSONException
                                    {
                                        Intent intent = DownloadFile.download(response.getString("FileName"), response.getString("File"));
                                        getActivity().startActivity(intent);
                                        attachmentLayout.setClickable(true);
                                        attachmentLayout.findViewById(R.id.progressbar).setVisibility(View.GONE);
                                    }

                                };
                                try {
                                    Request.get(URL.News.getAttachment(savedResponse.getJSONObject(position).getInt("IDNews"), savedResponse.getJSONObject(position).getJSONArray("Attachments").getJSONObject(k).getInt("IDNewsAttachment")), r);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                        itemlayout.addView(attachmentLayout);
                    }
            }
            if(edit){
                LinearLayout addattachmentbutton = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.circle_button_to_inflate,null);
                itemlayout.removeView(addattachmentbutton);
                itemlayout.addView(addattachmentbutton);
                ((Button)addattachmentbutton.findViewById(R.id.inflated_circle_button)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new SelectFileToUpload(getActivity()) {
                            @Override
                            public void selectedFile(String FileName, String FileType, String FileinBase64) {
                                try {
                                    insertAttachment(d,savedResponse.getJSONObject(position).getInt("IDNews"), FileName, FileType, FileinBase64);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        };
                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    JSONArray savedResponse;

    /**
     * metoda pro získání dat ze serveru
     */
    public void getNewsFromServer()
    {
       ArrayResponse r = new ArrayResponse() {
           @Override
           public void onResponse(JSONArray response) throws JSONException
           {
               itemList.clear();
               Log.d("getNewsFromServer", response.toString());
               savedResponse = response;
               swipeLayout.setRefreshing(false);
               if (response.length() > 0)
               {
                   for (int j = 0; j < response.length(); j++)
                   {
                       addItem( TimeLogika.getFormatedDate(response.getJSONObject(j).getString("DateInserted")),
                               response.getJSONObject(j).getString("Name"),
                               response.getJSONObject(j).getString("Text"));
                   }
               } else nothinglayout.setVisibility(View.VISIBLE);
           }
       };
        if(User.isGarant || User.isTutor)
            Request.get(URL.News.NewsByCourse_Garant(User.courseInfoId, User.id), r);
        else
            Request.get(URL.News.NewsByCourse(User.courseInfoId, User.id), r);

    }


    /**
     * přidá záznam do tabulky, přilohy jsou přidány později
     *
     * @param date datum aktuality
     * @param name nadpis aktuality
     * @param text text aktuality
     *
     */
    private void addItem(String date, String name, String text)
    {
        HashMap<String, String> item = new HashMap<String, String>();
        item.put("date", date);
        item.put("name", name);
        item.put("text", text);
        this.itemList.add(item);
        this.adapter.notifyDataSetChanged();
    }

    /**
     * metoda pro vložení přílohy na server
     *
     * @param d dialog ze kterého je přidáváno, po dokončení nahrávání se zavře
     * @param newsId ID aktuality
     * @param fileName Jméno přilohy
     * @param fileType Typ přílohy
     * @param fileinBase64 data souboru v kodování base64
     *
     */
    private void insertAttachment(final Dialog d, int newsId, final String fileName, String fileType, String fileinBase64)
    {

        swipeLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeLayout.setRefreshing(true);
            }
        });

        Response r = new Response() {

            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                Log.d("update new request", response.toString());
                if (response.getString("Success").equals("true")) {
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.attachment) + " " + fileName + " " + getActivity().getString(R.string.one_news_was_inserted)));

                } else { //nepovedlo se
                    Log.d("Fragment_news", "insertAttachment news: " + response.toString());
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.one_news_4_p) + " " + fileName + " " + getActivity().getString(R.string.one_news_was_not_inserted)));
                }
                //TODO
                update();
                d.cancel();
            }

        };
        try {
            JSONArray parameters = new JSONArray();
            JSONObject attachmenObject = new JSONObject();
            attachmenObject.put("IDNewsAttachment", newsId);
            attachmenObject.put("File", fileinBase64);
            attachmenObject.put("FileName", fileName);
            attachmenObject.put("FileContent",fileType);

            parameters.put(attachmenObject);
            Log.d("update new request", parameters.toString());

            Request.post(URL.News.insertattachments(newsId), parameters, r);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRefresh() {
        update();
    }

    private void update() {
        swipeLayout.post(new Runnable() {
            @Override
            public void run() {
                nothinglayout.setVisibility(View.GONE);
                swipeLayout.setRefreshing(true);
                //too clear list
                adapter.notifyDataSetChanged();
                getNewsFromServer();
            }
        });
    }
}
