package cz.vsb.elogika.ui.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cz.vsb.elogika.R;
import cz.vsb.elogika.common.view.SlidingTabLayout;
import cz.vsb.elogika.ui.fragments.Activity.Activities_active;
import cz.vsb.elogika.ui.fragments.Activity.Activities_finished;
import cz.vsb.elogika.utils.Actionbar;

public class Fragment_Activities extends Fragment {
    static final String LOG_TAG = "Fragment_Activities";

    private View rootView;

    public static Activities_active fragment_activities;
    public static Activities_finished fragment_activities_finished;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.tabs_container, container, false);
        Actionbar.newCategory(R.string.activities, this);


        ViewPager mViewPager = (ViewPager) rootView.findViewById(R.id.viewpager);
        mViewPager.setAdapter(new FragmentStatePagerAdapter(getFragmentManager()) {
                                  String[] tabNames = new String[] {
                                          getString(R.string.activities_for_current_curse_tab),
                                          getString(R.string.activities_finished_tab)};

                                  @Override
                                  public int getCount() {
                                      return tabNames.length;
                                  }
                                  @Override
                                  public CharSequence getPageTitle(int position) {
                                      return tabNames[position];
                                  }

                                  @Override
                                  public Fragment getItem(int position) {

                                      switch (position)
                                      {
                                          case 0:
                                              fragment_activities = new Activities_active();
                                              return fragment_activities;
                                          case 1:
                                              fragment_activities_finished = new Activities_finished();
                                              return fragment_activities_finished;
                                          default : return null;
                                      }
                                  }
                              }


        );
        SlidingTabLayout mSlidingTabLayout = (SlidingTabLayout) rootView.findViewById(R.id.sliding_tabs);
        mSlidingTabLayout.setViewPager(mViewPager);

        return rootView;
    }
}
