package cz.vsb.elogika.dataTypes;

public class Chapter {

    public int IDKapitola;
    public String Nazev;
    public int ParentID;
    public boolean Smazana;

    @Override
    public String toString() {
        return Nazev;
    }
}
