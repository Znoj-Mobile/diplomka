package cz.vsb.elogika.communication;

import android.util.Log;

import com.android.volley.VolleyError;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;

import org.json.JSONException;
import org.json.JSONObject;

import cz.vsb.elogika.R;
import cz.vsb.elogika.app.AppController;

public abstract class Response
{
    /**
     * Po příchodu odpovědi od serveru je zavolána tato metoda.
     * @param response Odpověď je obsažena v této proměnné
     * @throws JSONException Je vyhozena, pokud dojde k chybnému zacházení s proměnnou response (např. požadování parametrů, které neobsahuje). Ovšem je zachycena a vyvolána metoda onException, kterou je možné překrýt.
     */
    public abstract void onResponse(JSONObject response) throws JSONException;



    /**
     * Je zavolána, pokud dojde k výpadku internetového připojení.
     * @param error
     */
    public void onError(VolleyError error)
    {
        //Toast.makeText(AppController.getActivity(), R.string.error_reports__connection_failed, Toast.LENGTH_SHORT).show();
        SnackbarManager.show(Snackbar.with(AppController.getActivity()).type(SnackbarType.MULTI_LINE).text(R.string.error_reports__connection_failed));

        // Jen pro debug, poté smazat
        if(error.getMessage() != null)
        {
            Log.d("Response: ", error.getMessage());
        }
    }


    /**
     * Je zavolána v případě, že došlo k chybě v metodě onResponse. Překrytím se dá implementovat jiné chování.
     * @param exception
     */
    public void onException(JSONException exception)
    {
        //Toast.makeText(AppController.getActivity(), R.string.error_reports__parsing_error, Toast.LENGTH_SHORT).show();
        SnackbarManager.show(Snackbar.with(AppController.getActivity()).type(SnackbarType.MULTI_LINE).text(R.string.error_reports__parsing_error));

        // Jen pro debug, poté smazat
        if(exception.getMessage() != null)
        {
            Log.d("Response: ", exception.getMessage());
        }
    }
}