package cz.vsb.elogika.model;

import java.util.ArrayList;
import java.util.List;

public class Informations
{
    public static List<Integer> yearIDs = new ArrayList<>();
    public static List<Integer> courseInfoIds = new ArrayList<>();
    public static List<Integer> courseIds = new ArrayList<>();
    public static List<Integer> schoolIds = new ArrayList<>();
    public static List<Integer> roleIds = new ArrayList<>();

    public static List<String> years = new ArrayList<>();
    public static List<String> courseInfoNames = new ArrayList<>();
    public static List<String> schools = new ArrayList<>();
    public static List<String> roles = new ArrayList<>();
}
