package cz.vsb.elogika.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;

/**
    Tato statická třída slouží pro ovládání hierarchie fragmentů.
    Umožňuje přechody do heirarchicky vyšších fragmentů, buď
    klikem uživatele nebo programově (viz dále).
    Skupina metod newCategory začne nový strom. Typicky první
    fragment kliknutelný z levého menu. Pro další fragmenty,
    ve kterých se zanořujeme níž a níž, je určena skupina metod
    addSubcategory. Čili zvětšujeme hloubku stromu.
    Poslední skupinou je addSection, která slouží pro navigaci v rámci
    jednoho fragmentu. Jejím arguementem je i akce (podtřída Action, jenž je
    součástí této třídy), která se má provést po aktivaci uživatelem nebo
    programově. Poslední dvě skupiny metod vracejí index úrovně zanoření,
    což se může hodit pro přechody (jumpy) viz dále. U první skupiny by
    to nemělo smysl => je vždy 0.
    Programově se dají přechody aktivovat několika metodami.
    Metodou jumpBack() se vrátíme o 1 úroveň. V metodě jumpBack(int count)
    se zadává o kolik úrovní chceme zpět. Pokud je count < 0, je
    nastaven na 0 a pokud je count větší jak počet úrovní, tak je
    nastaven na aktuální úroveň (což vede k aktualizaci fragmentu nebo
    aktivaci podtřídy Action). Obdobně funguje metoda jumpTo(int index),
    kde index bud známe přímo nebo můžeme použít hodnoty vrácené metodami
    addSubcategory i AddSection. Metoda back(int index) jen smaže všechny
    nižší úrovně zanoření (s vyšším indexem). Což se hodí pro situaci,
    kdy se chceme vrátit a poté jít po jiné větvi v jednom kroku.
    Práce s menu funguje následovně. Nastavuje se pro každý fragment.
    Při změně v sekcích se menu nemění. Menu je možné skrýt pro celou
    následující kategorii metodou hideMenu. Je tedy nutné volat tuto
    metodu před metodou newCategory. Po dalším zavoláním metody
    newCategory se menu už zobrazí. To je vše, co tato třída nabízí :)
 */
public class Actionbar
{
    private static Activity activity;
    private static ArrayList<Section> sections = new ArrayList();
    private static ArrayList<MenuItem> menuItems = new ArrayList();
    private static int[] menuItemNames;
    private static boolean isPainted = false;
    private static boolean isMenuLocked = false;
    private static boolean isMenuHid = false;
    private static boolean isMenuHidForNextCategory = false;

    private static View logoMenu;
    private static View arrowBack;
    private static HorizontalScrollView scrollView;
    private static LinearLayout categoriesLayout;
    private static View buttonMenu;
    private static PopupMenu popupMenu;

    public interface Action
    {
        void onClick(Activity activity);
    }

    public interface OnMenuStringResourceClickListener
    {
        boolean onMenuStringResourceClick(int stringResource, int position);
    }

    private static class Section
    {
        String title;
        Class<? extends Fragment> link;
        Bundle data;
        Action action;
    }

    

    /**
     * Vymaže stávající větev a začne se od znova.
     * K dispozici nebude žádné menu (krom natvrdo nadefinových položek).
     * @param title název kategorie
     * @param link fragment, ve kterém se tato metoda volá
     */
    public static void newCategory(String title, Fragment link)
    {
        Actionbar.sections.clear();
        if (Actionbar.isMenuHidForNextCategory) Actionbar.isMenuHidForNextCategory = false;
        else Actionbar.isMenuHid = false;

        addSubcategory(title, link);
    }


    
    /**
     * Vymaže stávající větev a začne se od znova.
     * K dispozici nebude žádné menu (krom natvrdo nadefinových položek).
     * @param title název kategorie
     * @param link fragment, ve kterém se tato metoda volá
     */
    public static void newCategory(int title, Fragment link)
    {
        newCategory(link.getActivity().getResources().getString(title), link);
    }



    /**
     * Vymaže stávající větev a začne se od znova.
     * Možnost definice vlastního menu.
     * @param title název kategorie
     * @param link fragment, ve kterém se tato metoda volá
     * @param menuItems pole s idéčkama položek v menu
     * @param menuListener co se má stát po kliknutí na x-tou položku v menu
     */
    public static void newCategory(String title, Fragment link, int[] menuItems, OnMenuStringResourceClickListener menuListener)
    {
        boolean isMenuAlreadyLocked = Actionbar.isMenuLocked;
        Actionbar.isMenuLocked = true;
        newCategory(title, link);
        if (!isMenuAlreadyLocked) Actionbar.isMenuLocked = false;
        createMenu(menuItems, menuListener);
    }

    

    /**
     * Vymaže stávající větev a začne se od znova.
     * Možnost definice vlastního menu.
     * @param title název kategorie
     * @param link fragment, ve kterém se tato metoda volá
     * @param menuItems pole s idéčkama položek v menu
     * @param menuListener co se má stát po kliknutí na x-tou položku v menu
     */
    public static void newCategory(int title, Fragment link, int[] menuItems, OnMenuStringResourceClickListener menuListener)
    {
        newCategory(link.getActivity().getResources().getString(title), link, menuItems, menuListener);
    }


    /**
     * Vymaže stávající větev a začne se od znova.
     * Možnost definice vlastního menu.
     * @param title název kategorie
     * @param link fragment, ve kterém se tato metoda volá
     * @param menu id menu (R.menu.něco)
     * @param menuListener co se má stát po kliknutí na x-tou položku v menu
     */
    public static void newCategory(String title, Fragment link, int menu, PopupMenu.OnMenuItemClickListener menuListener)
    {
        boolean isMenuAlreadyLocked = Actionbar.isMenuLocked;
        Actionbar.isMenuLocked = true;
        newCategory(title, link);
        if (!isMenuAlreadyLocked) Actionbar.isMenuLocked = false;
        createMenu(menu, menuListener);
    }



    /**
     * Vymaže stávající větev a začne se od znova.
     * Možnost definice vlastního menu.
     * @param title název kategorie
     * @param link fragment, ve kterém se tato metoda volá
     * @param menu id menu (R.menu.něco)
     * @param menuListener co se má stát po kliknutí na x-tou položku v menu
     */
    public static void newCategory(int title, Fragment link, int menu, PopupMenu.OnMenuItemClickListener menuListener)
    {
        newCategory(link.getActivity().getResources().getString(title), link, menu, menuListener);
    }


    /**
     * Přidá do větve další úroveň.
     * Předpokládá se, že je tato metoda volána z jiného fragmentu,
     * než z jakého byla volána naposledy.
     * K dispozici nebude žádné menu (krom natvrdo nadefinových položek).
     * @param title název subkategorie
     * @param link fragment, ve kterém se tato metoda volá
     * @return počet úrovní ve větvi (včetně této)
     */
    public static int addSubcategory(String title, Fragment link)
    {
        Section section = new Section();
        section.title = title;
        section.link = link.getClass();
        section.data = link.getArguments();
        section.action = null;

        if (Actionbar.activity != link.getActivity())
        {
            Actionbar.activity = link.getActivity();
            Actionbar.isPainted = false;
        }

        Actionbar.sections.add(section);

        paintActionbar();

        if (!Actionbar.isMenuLocked) Actionbar.isMenuLocked = false;
        createMenu(new int[]{}, null);

        return Actionbar.sections.size() - 1;
    }



    /**
     * Přidá do větve další úroveň.
     * Předpokládá se, že je tato metoda volána z jiného fragmentu,
     * než z jakého byla volána naposledy.
     * K dispozici nebude žádné menu (krom natvrdo nadefinových položek).
     * @param title název subkategorie
     * @param link fragment, ve kterém se tato metoda volá
     * @return počet úrovní ve větvi (včetně této)
     */
    public static int addSubcategory(int title, Fragment link)
    {
        return addSubcategory(link.getActivity().getResources().getString(title), link);
    }



    /**
     * Přidá do větve další úroveň.
     * Předpokládá se, že je tato metoda volána z jiného fragmentu,
     * než z jakého byla volána naposledy.
     * Možnost definice vlastního menu.
     * @param title název subkategorie
     * @param link fragment, ve kterém se tato metoda volá
     * @param menuItems pole s idéčkama položek v menu
     * @param menuListener co se má stát po kliknutí na x-tou položku v menu
     * @return počet úrovní ve větvi (včetně této)
     */
    public static int addSubcategory(String title, Fragment link, int[] menuItems, OnMenuStringResourceClickListener menuListener)
    {
        boolean isMenuAlreadyLocked = Actionbar.isMenuLocked;
        Actionbar.isMenuLocked = true;
        int position = addSubcategory(title, link);
        if (!isMenuAlreadyLocked) Actionbar.isMenuLocked = false;
        createMenu(menuItems, menuListener);
        return position;
    }



    /**
     * Přidá do větve další úroveň.
     * Předpokládá se, že je tato metoda volána z jiného fragmentu,
     * než z jakého byla volána naposledy.
     * Možnost definice vlastního menu.
     * @param title název subkategorie
     * @param link fragment, ve kterém se tato metoda volá
     * @param menuItems pole s idéčkama položek v menu
     * @param menuListener co se má stát po kliknutí na x-tou položku v menu
     * @return počet úrovní ve větvi (včetně této)
     */
    public static int addSubcategory(int title, Fragment link, int[] menuItems, OnMenuStringResourceClickListener menuListener)
    {
        return addSubcategory(link.getActivity().getResources().getString(title), link, menuItems, menuListener);
    }


    /**
     * Přidá do větve další úroveň.
     * Předpokládá se, že je tato metoda volána z jiného fragmentu,
     * než z jakého byla volána naposledy.
     * Možnost definice vlastního menu.
     * @param title název subkategorie
     * @param link fragment, ve kterém se tato metoda volá
     * @param menu id menu (R.menu.něco)
     * @param menuListener co se má stát po kliknutí na x-tou položku v menu
     * @return
     */
    public static int addSubcategory(String title, Fragment link, int menu, PopupMenu.OnMenuItemClickListener menuListener)
    {
        boolean isMenuAlreadyLocked = Actionbar.isMenuLocked;
        Actionbar.isMenuLocked = true;
        int position = addSubcategory(title, link);
        if (!isMenuAlreadyLocked) Actionbar.isMenuLocked = false;
        createMenu(menu, menuListener);
        return position;
    }



    /**
     * Přidá do větve další úroveň.
     * Předpokládá se, že je tato metoda volána z jiného fragmentu,
     * než z jakého byla volána naposledy.
     * Možnost definice vlastního menu.
     * @param title název subkategorie
     * @param link fragment, ve kterém se tato metoda volá
     * @param menu id menu (R.menu.něco)
     * @param menuListener co se má stát po kliknutí na x-tou položku v menu
     * @return
     */
    public static int addSubcategory(int title, Fragment link, int menu, PopupMenu.OnMenuItemClickListener menuListener)
    {
        return addSubcategory(link.getActivity().getResources().getString(title), link, menu, menuListener);
    }


    /**
     * Přidá do větve další úroveň.
     * Předpokládá se, že zůstaneme na stejném fragmentu,
     * na které děláme nějaké úpravy.
     * Menu se zachovává z poslední (sub)kategorie.
     * @param title název sekce
     * @param action co se má po kliknutí na tlačítko vykonat
     * @return
     */
    public static int addSection(String title, Action action)
    {
        if (Actionbar.sections.size() <= 0) return -1;

        Section section = new Section();
        section.title = title;
        section.link = Actionbar.sections.get(Actionbar.sections.size() - 1).link;
        section.data = Actionbar.sections.get(Actionbar.sections.size() - 1).data;
        section.action = action;

        Actionbar.sections.add(section);

        paintActionbar();

        return Actionbar.sections.size() - 1;
    }



    /**
     * Přidá do větve další úroveň.
     * Předpokládá se, že zůstaneme na stejném fragmentu,
     * na které děláme nějaké úpravy.
     * Menu se zachovává z poslední (sub)kategorie.
     * @param title název sekce
     * @param action co se má po kliknutí na tlačítko vykonat
     * @return
     */
    public static int addSection(int title, Action action)
    {
        return addSection(Actionbar.activity.getResources().getString(title), action);
    }


    /**
     *
     * @return Má tato větev jen jednu úroveň? Neboli jsme v kořeni?
     */
    public static boolean isInRoot()
    {
        if (Actionbar.sections.size() <= 1) return true;
        else return false;
    }


    /**
     * Všechny vyšší úrovně než je v parametru se vymažou,
     * ale stále zůstaneme na stejném fragmentu.
     * @param index od které úrovně výš se má mazat (počítáme od kořene)
     */
    public static void back(int index)
    {
        while (index < Actionbar.sections.size()) Actionbar.sections.remove(Actionbar.sections.size() - 1);
    }


    /**
     * Vrátíme se na úroveň podle parametru s tím,
     * že je realizován přechod na příslušný fragment.
     * @param index úroveň stromu (počítáme od kořene)
     */
    public static void jumpTo(int index)
    {
        if (index < 0) index = 0;
        else if (index >= Actionbar.sections.size()) index = Actionbar.sections.size() - 1;

        Section section = Actionbar.sections.get(index);

        if (Actionbar.sections.get(Actionbar.sections.size() - 1).link != section.link || index == 0 || Actionbar.sections.get(index - 1).link != section.link)
        {
            try
            {
                Fragment fragment = section.link.newInstance();
                fragment.setArguments(section.data);

                back(index);

                Actionbar.activity.getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment).commit();
            }
            catch (Exception e) {}
        }
        else back(index + 1);

        paintActionbar();

        if (section.action != null) section.action.onClick(Actionbar.activity);
    }



    /**
     * Vrátíme se o jednu úroveň zpět s tím,
     * že je realizován přechod na příslušný fragment.
     */
    public static void jumpBack()
    {
        jumpTo(Actionbar.sections.size() - 2);
    }



    /**
     * Vrátíme se o několik úrovení zpět podle parametru s tím,
     * že je realizován přechod na příslušný fragment.
     */
    public static void jumpBack(int count)
    {
        jumpTo(Actionbar.sections.size() - count - 1);
    }



    /**
     * Pro danou (sub)kategorii úplně skryjeme menu.
     * Tuto metodu voláme před newCategory nebo addSubcategory.
     */
    public static void hideMenu()
    {
        Actionbar.isMenuHid = true;
        Actionbar.isMenuHidForNextCategory = true;
    }


    /**
     * Do actionbaru u příslušného fragmentu přidáme menu.
     * Součástí jsou i natvrdo definované položky.
     * @param menu id menu (R.menu.něco)
     * @param menuListener co se má stát po kliknutí na x-tou položku v menu
     */
    private static void createMenu(int menu, final PopupMenu.OnMenuItemClickListener menuListener)
    {
        if (Actionbar.isMenuLocked) return;
        if (!Actionbar.isMenuHidForNextCategory && Actionbar.isMenuHid) return;

        Actionbar.popupMenu.getMenu().clear();
        Actionbar.popupMenu.inflate(menu);
        Actionbar.popupMenu.getMenu().add(Actionbar.activity.getString(R.string.bug_report));
        Actionbar.popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener()
        {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem)
            {
                if (menuItem.getItemId() == 0 && Actionbar.activity.getString(R.string.bug_report).contentEquals(menuItem.getTitle()))
                {
                    reportBug();
                    return true;
                }

                return menuListener.onMenuItemClick(menuItem);
            }
        });
    }


    /**
     * Do actionbaru u příslušného fragmentu přidáme menu.
     * Součástí jsou i natvrdo definované položky.
     * @param menuItems pole s idéčkama položek v menu
     * @param menuListener co se má stát po kliknutí na x-tou položku v menu
     */
    private static void createMenu(int[] menuItems, final OnMenuStringResourceClickListener menuListener)
    {
        if (Actionbar.isMenuLocked) return;
        if (!Actionbar.isMenuHidForNextCategory && Actionbar.isMenuHid) return;

        Actionbar.menuItemNames = menuItems;
        Menu menu = Actionbar.popupMenu.getMenu();
        menu.clear();
        Actionbar.menuItems.clear();
        for (int menuItem: menuItems)
        {
            Actionbar.menuItems.add(menu.add(menuItem));
        }
        Actionbar.menuItems.add(menu.add(Actionbar.activity.getString(R.string.bug_report)));

        Actionbar.popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener()
        {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem)
            {
                int index = Actionbar.menuItems.indexOf(menuItem);
                if (index < 0 || index > Actionbar.menuItems.size()) return false;
                if (index >= Actionbar.menuItemNames.length)
                {
                    reportBug();
                    return true;
                }

                return menuListener.onMenuStringResourceClick(menuItemNames[index], index);
            }
        });
    }


    /**
     * Vytvoří dialog, jehož prostřednictvím zjistíme od uživatele potřebné informace.
     */
    private static void reportBug()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(Actionbar.activity);
        builder.setTitle(R.string.bug_report);
        builder.setMessage(R.string.bug_description);
        final EditText input = new EditText(Actionbar.activity);
        builder.setView(input);

        builder.setPositiveButton(R.string.send, new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which)
            {
                sendBugReport(input.getText().toString());
            }
        });
        builder.setNegativeButton(R.string.cancel, null);
        builder.setIcon(android.R.drawable.ic_dialog_alert);
        builder.create().show();
    }


    /**
     * Odešle na server žádost o nahlášení chyby.
     * @param report
     */
    private static void sendBugReport(String report)
    {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                if (response.has("Result"))
                {
                    SnackbarManager.show(Snackbar.with(Actionbar.activity).text(R.string.bug_report_was_send));
                }
                else
                {
                    SnackbarManager.show(Snackbar.with(Actionbar.activity).text(R.string.bug_report_was_not_send));
                }
            }
        };
        JSONObject parameters = new JSONObject();
        try
        {
            parameters.put("OSName", "Android");
            parameters.put("OSVersion", android.os.Build.VERSION.SDK_INT);
            parameters.put("Device", android.os.Build.MODEL);
            parameters.put("IdUser", User.id);
            parameters.put("IdCourse", User.courseInfoId);
            parameters.put("IdRole", User.roleID);
            parameters.put("IdInfoSkola", User.schoolInfoID);
            parameters.put("BugText", report);
        }
        catch (JSONException e) {}

        Request.post(URL.User.SendBugReport(), parameters, response);
    }


    /**
     * Některé části se vykreslí znovu. Zdaleka ne všechny.
     */
    private static void repaintActionbar()
    {
        // Zobrazení buď loga nebo šipky zpět
        if (Actionbar.sections.size() > 1)
        {
            Actionbar.logoMenu.setVisibility(View.GONE);
            Actionbar.arrowBack.setVisibility(View.VISIBLE);
        }
        else
        {
            Actionbar.arrowBack.setVisibility(View.GONE);
            Actionbar.logoMenu.setVisibility(View.VISIBLE);
        }

        // Vypsání kategorií
        Actionbar.categoriesLayout.removeAllViews();
        Button button;
        TextView textView;
        int j = 0;
        for (final Section section: Actionbar.sections)
        {
            if (j != 0)
            {
                textView = new TextView(Actionbar.activity);
                textView.setTextAppearance(Actionbar.activity, R.style.textViewActionBar);
                textView.setText(" > ");
                Actionbar.categoriesLayout.addView(textView);
            }

            button = new Button(Actionbar.activity);
            button.setText(section.title);
            button.setTextAppearance(Actionbar.activity, R.style.actionbarItem);
            button.setBackgroundResource(R.drawable.actionbar_item);

            final int id = j;
            button.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    jumpTo(id);
                }
            });
            Actionbar.categoriesLayout.addView(button);
            j++;
        }

        new Handler().postDelayed(new Runnable()
        {
            public void run()
            {
                Actionbar.scrollView.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
            }
        }, 100L);

        // Menu
        if (Actionbar.isMenuHid) Actionbar.buttonMenu.setVisibility(View.GONE);
        else Actionbar.buttonMenu.setVisibility(View.VISIBLE);
    }


    /**
     * Vykreslí se celý actionbar
     */
    private static void paintActionbar()
    {
        if (Actionbar.isPainted)
        {
            repaintActionbar();
            return;
        }

        RelativeLayout newActionBar = (RelativeLayout) LayoutInflater.from(Actionbar.activity).inflate(R.layout.actionbar, null);

        Actionbar.logoMenu = newActionBar.findViewById(R.id.buttonLeftMenu);
        Actionbar.arrowBack = newActionBar.findViewById(R.id.buttonBack);
        Actionbar.scrollView = (HorizontalScrollView) newActionBar.findViewById(R.id.scroll_bar);
        Actionbar.categoriesLayout = (LinearLayout) newActionBar.findViewById(R.id.categories);
        Actionbar.buttonMenu = newActionBar.findViewById(R.id.buttonMenu);

        Actionbar.popupMenu = new PopupMenu(Actionbar.activity, Actionbar.buttonMenu);

        Actionbar.arrowBack.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                jumpBack();
            }
        });

        Actionbar.logoMenu.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Actionbar.activity.onBackPressed();
            }
        });

        Actionbar.buttonMenu.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Actionbar.popupMenu.show();
            }
        });

        ActionBar oldActionBar = ((ActionBarActivity) Actionbar.activity).getSupportActionBar();
        oldActionBar.setDisplayShowHomeEnabled(false);
        oldActionBar.setDisplayShowTitleEnabled(false);
        oldActionBar.setDisplayShowCustomEnabled(true);
        oldActionBar.setCustomView(newActionBar);

        Actionbar.isPainted = true;

        repaintActionbar();
    }
}
