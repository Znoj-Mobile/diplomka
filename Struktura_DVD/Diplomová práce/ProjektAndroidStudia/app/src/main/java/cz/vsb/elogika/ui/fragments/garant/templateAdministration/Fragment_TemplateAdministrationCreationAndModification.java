package cz.vsb.elogika.ui.fragments.garant.templateAdministration;

import android.app.Dialog;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import cz.vsb.elogika.R;
import cz.vsb.elogika.adapters.HierarchicalAdapter;
import cz.vsb.elogika.common.view.SlidingTabLayout;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.Actionbar;

public class Fragment_TemplateAdministrationCreationAndModification extends Fragment
{
    private boolean isCreation;
    private int templateId;
    private NumberPicker templateBlockCount;
    private AtomicInteger activeRequests;
    private volatile boolean areHeadersLoaded;
    private volatile boolean isDataReady;
    private final Object lock = new Object();

    private ViewPager pager;
    private Dialog dialog;
    private ArrayList<Chapter> chapters;
    private ArrayList<Chapter> rootChapters;
    private ArrayList<Category> categories;
    private ArrayList<Step> steps;
    private ArrayList<Question> questions;
    private Block blocks[];
    private AdapterAndHisState[] adapters;
    private HierarchicalAdapter.Item loading[];
    private Button roundButton;
    private Button roundButtonInBlocks[];
    private volatile boolean isButtonEnabled;

    // Slouží pro uchování informace, ve kterém stavu se nacházíme při načítání výběru otázek
    private class AdapterAndHisState
    {
        public final static int INITIATION = 0;
        public final static int LOADING_ADDED = 1;
        public final static int HEADER_ADDED = 2;
        public final static int DATA_ADDED = 3;

        public HierarchicalAdapter adapter;
        public int state;
    }

    // Konstanty
    private final static int HEADER = 0;
    private final static int CHAPTER = 1;
    private final static int CATEGORY = 2;
    private final static int STEPS = 3;
    private final static int STEP_HEADER = 4;
    private final static int STEP = 5;
    private final static int QUESTIONS = 6;
    private final static int QUESTION = 7;
    private final static int LOADING = 8;


    // Struktury potřebné pro HierarchicalAdapter (proto dědí od HierarchicalAdapter.Item)
    // Jsou to typy položek v recycler view
    private class ISteps extends HierarchicalAdapter.Item
    {
        public IStepHeader istepHeader;

        public ISteps()
        {
            this.type = STEPS;
        }
    }

    private class IStepHeader extends HierarchicalAdapter.Item
    {
        int condition;

        public IStepHeader()
        {
            this.type = STEP_HEADER;
        }
    }

    private class Step
    {
        public int id;
        public String name;
        public Category category;
        public IStep isteps[];

        public Step()
        {
            this.isteps = new IStep[20];
        }
    }

    private class IStep extends HierarchicalAdapter.Item
    {
        public Step step;
        public boolean isChecked;

        public IStep()
        {
            this.type = STEP;
        }
    }


    private class IQuestions extends HierarchicalAdapter.Item
    {
        public IQuestions()
        {
            this.type = QUESTIONS;
        }
    }

    private class Question
    {
        public int id;
        public String name;
        public Category category;
        public IQuestion iquestions[];
    }

    private class IQuestion extends HierarchicalAdapter.Item
    {
        public int id;
        public String name;
        public Question question;
        public boolean isChecked;

        public IQuestion()
        {
            this.type = QUESTION;
        }
    }

    public class Chapter
    {
        public int id;
        public String name;
        public IChapter ichapters[];
        public AtomicInteger childCount;

        public Chapter()
        {
            this.childCount = new AtomicInteger(0);
        }
    }

    public class IChapter extends HierarchicalAdapter.Item
    {
        public Chapter chapter;
        public boolean isChecked;
        public int childCheckedCount;

        public IChapter()
        {
            this.type = CHAPTER;
        }
    }

    protected class Category
    {
        public int id;
        public String name;
        public ICategory icategory[];
        public AtomicInteger childCount;

        public Category()
        {
            this.childCount = new AtomicInteger(0);
        }
    }

    protected class ICategory extends HierarchicalAdapter.Item
    {
        public boolean isChecked;
        public Category category;
        public int questionCount;
        public TextWatcher textWatcher;
        public ISteps isteps;
        public IQuestions iquestions;
        public int childCheckedCount;

        public ICategory()
        {
            this.type = CATEGORY;
        }
    }

    protected class Block extends HierarchicalAdapter.Item
    {
        int id;
        String name;
        boolean displayName = true;
        int complexityFrom;
        int complexityTo;
        double blockWeight;
        int questionCount;
        int answerCount;
        int answerType;
        int evaluationType;
        int percentagesWhichWillBeSubtractedForWrongAnswer;
        boolean mixInBlock = true;
        boolean isCreated;

        boolean wasCalled = false;

        public Block()
        {
            this.type = HEADER;
            this.name = new String();
            this.isCreated = true;
        }
    }



    public Fragment_TemplateAdministrationCreationAndModification()
    {
        this.activeRequests = new AtomicInteger();
        this.areHeadersLoaded = false;
        this.isDataReady = false;
        this.chapters = new ArrayList();
        this.rootChapters = new ArrayList();
        this.categories = new ArrayList();
        this.steps = new ArrayList();
        this.questions = new ArrayList();
        this.blocks = new Block[20];
        this.loading = new HierarchicalAdapter.Item[20];
        this.adapters = new AdapterAndHisState[20];
        for (int j = 0; j < 20; j++) this.blocks[j] = new Block();
        this.roundButtonInBlocks = new Button[20];
        this.isButtonEnabled = true;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        if (getArguments() == null) this.isCreation = true;
        else this.isCreation = false;

        Actionbar.addSubcategory((this.isCreation) ? R.string.template_administration_creation : R.string.template_administration_editation, this);

        final View rootView = inflater.inflate(R.layout.fragment_template_administration_creation_and_modification, container, false);

        this.roundButton = (Button) rootView.findViewById(R.id.roundButton);
        this.roundButton.setText("✔");
        this.roundButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                setAllButtons(false);
                SnackbarManager.show(Snackbar.with(getActivity()).type(SnackbarType.MULTI_LINE).duration(Snackbar.SnackbarDuration.LENGTH_INDEFINITE).text(getResources().getString(R.string.send_data)));
                if (isCreation) insertTemplate();
                else updateTemplate();
            }
        });

        this.templateBlockCount = ((NumberPicker) rootView.findViewById(R.id.template_block_count));
        this.templateBlockCount.setMinValue(1);
        this.templateBlockCount.setMaxValue(20);

        if (!this.isCreation)
        {
            this.templateId = getArguments().getInt("id");
            ((EditText) rootView.findViewById(R.id.name)).setText(getArguments().getString("name"));
            ((EditText) rootView.findViewById(R.id.description)).setText(getArguments().getString("description"));
            this.templateBlockCount.setValue(getArguments().getInt("blockCount"));
            ((CheckBox) rootView.findViewById(R.id.all_mix)).setChecked(getArguments().getBoolean("allMix"));
            ((CheckBox) rootView.findViewById(R.id.block_mix)).setChecked(getArguments().getBoolean("blockMix"));
        }

        // Dialog s bloky
        this.dialog = new Dialog(getActivity());
        this.dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.dialog.setContentView(R.layout.tabs_layout);

        this.pager = (ViewPager) dialog.findViewById(R.id.pager);
        final PagerAdapter pagerAdapter = new PagerAdapter()
        {
            @Override
            public CharSequence getPageTitle(int position)
            {
                return String.valueOf(position + 1);
            }

            @Override
            public int getCount()
            {
                return templateBlockCount.getValue();
            }

            @Override
            public boolean isViewFromObject(View view, Object object)
            {
                return view == object;
            }

            @Override
            public Object instantiateItem(ViewGroup container, final int tabPosition)
            {
                final View tab = LayoutInflater.from(getActivity()).inflate(R.layout.recycler_view_with_button, container, false);
                roundButtonInBlocks[tabPosition] = ((Button) tab.findViewById(R.id.roundButton));
                roundButtonInBlocks[tabPosition].setText("✔");
                roundButtonInBlocks[tabPosition].setEnabled(isButtonEnabled);
                roundButtonInBlocks[tabPosition].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        setAllButtons(false);
                        SnackbarManager.show(Snackbar.with(getActivity()).type(SnackbarType.MULTI_LINE).duration(Snackbar.SnackbarDuration.LENGTH_INDEFINITE).text(getResources().getString(R.string.send_data)));
                        if (isCreation) insertTemplate();
                        else updateTemplate();
                        dialog.dismiss();
                    }
                });

                RecyclerView recyclerView = (RecyclerView) tab.findViewById(R.id.recyclerView);
                synchronized (lock)
                {
                    if (adapters[tabPosition] == null)
                    {
                        adapters[tabPosition] = new AdapterAndHisState();
                        adapters[tabPosition].adapter = new HierarchicalAdapter()
                        {
                            // Definice všech holderů, které musí dědit od HierarchicalAdapter.Holder
                            class ViewHolderLoading extends HierarchicalAdapter.Holder
                            {
                                public ViewHolderLoading(View itemView)
                                {
                                    super(itemView);
                                }
                            }

                            class ViewHolderHeader extends HierarchicalAdapter.Holder
                            {
                                public EditText name;
                                public CheckBox displayName;
                                public EditText difficultyFrom;
                                public EditText difficultyTo;
                                public EditText blockWeight;
                                public EditText questionCount;
                                public EditText answerCount;
                                public Spinner answerType;
                                public RadioGroup radioGroup;
                                public EditText percentagesWhichWillBeSubtractedForWrongAnswer;
                                public CheckBox mixInBlock;

                                public ViewHolderHeader(View itemView)
                                {
                                    super(itemView);
                                    this.name = (EditText) itemView.findViewById(R.id.name);
                                    this.displayName = (CheckBox) itemView.findViewById(R.id.displayName);
                                    this.difficultyFrom = (EditText) itemView.findViewById(R.id.difficultyFrom);
                                    this.difficultyTo = (EditText) itemView.findViewById(R.id.difficultyTo);
                                    this.blockWeight = (EditText) itemView.findViewById(R.id.blockWeight);
                                    this.questionCount = (EditText) itemView.findViewById(R.id.questionCount);
                                    this.answerCount = (EditText) itemView.findViewById(R.id.answerCount);
                                    this.answerType = (Spinner) itemView.findViewById(R.id.answerType);
                                    this.radioGroup = (RadioGroup) itemView.findViewById(R.id.evaluationType);
                                    this.percentagesWhichWillBeSubtractedForWrongAnswer = (EditText) itemView.findViewById(R.id.percentagesWhichWillBeSubtractedForWrongAnswer);
                                    this.mixInBlock = ((CheckBox) itemView.findViewById(R.id.mixInBlock));
                                }
                            }

                            class ViewHolderCheckBox extends HierarchicalAdapter.Holder
                            {
                                public CheckBox checkBox;

                                public ViewHolderCheckBox(View itemView)
                                {
                                    super(itemView);
                                    this.checkBox = (CheckBox) itemView.findViewById(R.id.checkbox);
                                }
                            }

                            class ViewHolderCategory extends HierarchicalAdapter.Holder
                            {
                                public CheckBox checkBox;
                                public EditText questionCount;

                                public ViewHolderCategory(View itemView)
                                {
                                    super(itemView);
                                    this.checkBox = (CheckBox) itemView.findViewById(R.id.checkbox);
                                    this.questionCount = (EditText) itemView.findViewById(R.id.questionCount);
                                }
                            }

                            class ViewHolderTitle extends HierarchicalAdapter.Holder
                            {
                                public TextView title;

                                public ViewHolderTitle(View itemView)
                                {
                                    super(itemView);
                                    this.title = (TextView) itemView.findViewById(R.id.title);
                                }
                            }

                            class ViewHolderStepHeader extends HierarchicalAdapter.Holder
                            {
                                public RadioGroup radioGroup;

                                public ViewHolderStepHeader(View itemView)
                                {
                                    super(itemView);
                                    this.radioGroup = (RadioGroup) itemView.findViewById(R.id.radioGroup);
                                }
                            }


                            @Override
                            public HierarchicalAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType)
                            {
                                switch (viewType)
                                {
                                    case HEADER:
                                        blocks[tabPosition].wasCalled = false;
                                        return new ViewHolderHeader(LayoutInflater.from(getActivity()).inflate(R.layout.fragment_template_administration_block_header, parent, false));

                                    case CHAPTER:
                                        return new ViewHolderCheckBox(wrapLayout(parent, R.layout.fragment_template_administration_block_item));

                                    case CATEGORY:
                                        return new ViewHolderCategory(wrapLayout(parent, R.layout.fragment_template_administration_block_category_item));

                                    case STEPS:
                                        return new ViewHolderTitle(wrapLayout(parent, R.layout.fragment_template_administration_block_title));

                                    case STEP_HEADER:
                                        return new ViewHolderStepHeader(wrapLayout(parent, R.layout.fragment_template_administration_block_step_header));

                                    case STEP:
                                        return new ViewHolderCheckBox(wrapLayout(parent, R.layout.fragment_template_administration_block_item));

                                    case QUESTIONS:
                                        return new ViewHolderTitle(wrapLayout(parent, R.layout.fragment_template_administration_block_title));

                                    case QUESTION:
                                        return new ViewHolderCheckBox(wrapLayout(parent, R.layout.fragment_template_administration_block_item));

                                    case LOADING:
                                        return new ViewHolderLoading(LayoutInflater.from(getActivity()).inflate(R.layout.progress_bar, parent, false));
                                }
                                return null;
                            }

                            @Override
                            public void onBindViewHolder(final HierarchicalAdapter.Holder holder, int position, Item item)
                            {
                                switch (holder.getItemViewType())
                                {
                                    case HEADER:
                                        if (blocks[tabPosition].wasCalled) return;
                                        blocks[tabPosition].wasCalled = true;

                                        final ViewHolderHeader holderHeader = (ViewHolderHeader) holder;

                                        holderHeader.name.setText(blocks[tabPosition].name);
                                        holderHeader.name.addTextChangedListener(new TextWatcher() {
                                            @Override
                                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                                            }

                                            @Override
                                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                                blocks[tabPosition].name = s.toString();
                                            }

                                            @Override
                                            public void afterTextChanged(Editable s) {
                                            }
                                        });

                                        holderHeader.displayName.setChecked(blocks[tabPosition].displayName);
                                        holderHeader.displayName.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                blocks[tabPosition].displayName = ((CheckBox) view).isChecked();
                                            }
                                        });

                                        holderHeader.difficultyFrom.setText(String.valueOf(blocks[tabPosition].complexityFrom));
                                        holderHeader.difficultyFrom.addTextChangedListener(new TextWatcher() {
                                            @Override
                                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                                            }

                                            @Override
                                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                                try {
                                                    blocks[tabPosition].complexityFrom = Integer.parseInt(s.toString());
                                                } catch (NumberFormatException exception) {
                                                    blocks[tabPosition].complexityFrom = 0;
                                                }
                                            }

                                            @Override
                                            public void afterTextChanged(Editable s) {
                                            }
                                        });

                                        holderHeader.difficultyTo.setText(String.valueOf(blocks[tabPosition].complexityTo));
                                        holderHeader.difficultyTo.addTextChangedListener(new TextWatcher() {
                                            @Override
                                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                                            }

                                            @Override
                                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                                try {
                                                    blocks[tabPosition].complexityTo = Integer.parseInt(s.toString());
                                                } catch (NumberFormatException exception) {
                                                    blocks[tabPosition].complexityTo = 0;
                                                }
                                            }

                                            @Override
                                            public void afterTextChanged(Editable s) {
                                            }
                                        });

                                        holderHeader.blockWeight.setText(String.valueOf(blocks[tabPosition].blockWeight));
                                        holderHeader.blockWeight.addTextChangedListener(new TextWatcher() {
                                            @Override
                                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                                            }

                                            @Override
                                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                                try {
                                                    blocks[tabPosition].blockWeight = Double.parseDouble(s.toString());
                                                } catch (NumberFormatException exception) {
                                                    blocks[tabPosition].blockWeight = (double) 0;
                                                }
                                            }

                                            @Override
                                            public void afterTextChanged(Editable s) {
                                            }
                                        });

                                        holderHeader.questionCount.setText(String.valueOf(blocks[tabPosition].questionCount));
                                        holderHeader.questionCount.addTextChangedListener(new TextWatcher() {
                                            @Override
                                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                                            }

                                            @Override
                                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                                try {
                                                    blocks[tabPosition].questionCount = Integer.parseInt(s.toString());
                                                } catch (NumberFormatException exception) {
                                                    blocks[tabPosition].questionCount = 0;
                                                }
                                            }

                                            @Override
                                            public void afterTextChanged(Editable s) {
                                            }
                                        });

                                        holderHeader.answerCount.setText(String.valueOf(blocks[tabPosition].answerCount));
                                        holderHeader.answerCount.addTextChangedListener(new TextWatcher() {
                                            @Override
                                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                                            }

                                            @Override
                                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                                try {
                                                    blocks[tabPosition].answerCount = Integer.parseInt(s.toString());
                                                } catch (NumberFormatException exception) {
                                                    blocks[tabPosition].answerCount = 0;
                                                }
                                            }

                                            @Override
                                            public void afterTextChanged(Editable s) {
                                            }
                                        });

                                        holderHeader.answerType.setSelection(blocks[tabPosition].answerType);
                                        holderHeader.answerType.setAdapter(new ArrayAdapter<>(getActivity(), R.layout.spinner_item, getResources().getStringArray(R.array.answer_type)));
                                        holderHeader.answerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                            @Override
                                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                                blocks[tabPosition].answerType = i;
                                            }

                                            @Override
                                            public void onNothingSelected(AdapterView<?> adapterView) {
                                            }
                                        });

                                        holderHeader.radioGroup.check(holderHeader.radioGroup.getChildAt(blocks[tabPosition].evaluationType).getId());
                                        holderHeader.radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                                            @Override
                                            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                                                blocks[tabPosition].evaluationType = radioGroup.indexOfChild(radioGroup.findViewById(radioGroup.getCheckedRadioButtonId()));
                                            }
                                        });

                                        holderHeader.percentagesWhichWillBeSubtractedForWrongAnswer.setText(String.valueOf(blocks[tabPosition].percentagesWhichWillBeSubtractedForWrongAnswer));
                                        holderHeader.percentagesWhichWillBeSubtractedForWrongAnswer.addTextChangedListener(new TextWatcher() {
                                            @Override
                                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                                            }

                                            @Override
                                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                                try {
                                                    blocks[tabPosition].percentagesWhichWillBeSubtractedForWrongAnswer = Integer.parseInt(s.toString());
                                                } catch (NumberFormatException exception) {
                                                    blocks[tabPosition].percentagesWhichWillBeSubtractedForWrongAnswer = 0;
                                                }
                                            }

                                            @Override
                                            public void afterTextChanged(Editable s) {
                                            }
                                        });

                                        holderHeader.mixInBlock.setChecked(blocks[tabPosition].mixInBlock);
                                        holderHeader.mixInBlock.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                blocks[tabPosition].mixInBlock = ((CheckBox) view).isChecked();
                                            }
                                        });
                                        break;

                                    case CHAPTER:
                                        final IChapter ichapter = (IChapter) item;
                                        ((ViewHolderCheckBox) holder).checkBox.setText(ichapter.chapter.name);
                                        ((ViewHolderCheckBox) holder).checkBox.setChecked(ichapter.isChecked);
                                        ((ViewHolderCheckBox) holder).checkBox.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                AtomicBoolean isNotifyRequired = new AtomicBoolean(false);
                                                checkItAndCheckParentUp(ichapter, isNotifyRequired);
                                                if (ichapter.isChecked)
                                                    ichapter.childCheckedCount = ichapter.chapter.childCount.get();
                                                else ichapter.childCheckedCount = 0;
                                                checkAllChildren(ichapter, ichapter.isChecked);
                                                if (isNotifyRequired.get() || ichapter.chapter.childCount.get() > 0)
                                                    notifyDataSetChanged();
                                            }
                                        });
                                        break;

                                    case CATEGORY:
                                        final ICategory icategory = (ICategory) item;
                                        ((ViewHolderCategory) holder).checkBox.setText(icategory.category.name);
                                        ((ViewHolderCategory) holder).checkBox.setChecked(icategory.isChecked);
                                        ((ViewHolderCategory) holder).checkBox.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                AtomicBoolean isNotifyRequired = new AtomicBoolean(false);
                                                checkItAndCheckParentUp(icategory, isNotifyRequired);
                                                if (icategory.isChecked)
                                                    icategory.childCheckedCount = icategory.category.childCount.get();
                                                else icategory.childCheckedCount = 0;
                                                checkAllChildren(icategory, icategory.isChecked);
                                                if (isNotifyRequired.get() || icategory.category.childCount.get() > 0)
                                                    notifyDataSetChanged();
                                            }
                                        });
                                        for (Category category : categories)
                                        {
                                            if (category.icategory[tabPosition].textWatcher != null)
                                            {
                                                ((ViewHolderCategory) holder).questionCount.removeTextChangedListener(category.icategory[tabPosition].textWatcher);
                                            }
                                        }
                                        ((ViewHolderCategory) holder).questionCount.setText(String.valueOf(icategory.questionCount));
                                        if (icategory.textWatcher == null) icategory.textWatcher = new TextWatcher()
                                        {
                                            @Override
                                            public void beforeTextChanged(CharSequence s, int start, int count, int after){ }

                                            @Override
                                            public void onTextChanged(CharSequence s, int start, int before, int count)
                                            {
                                                try
                                                {
                                                    icategory.questionCount = Integer.parseInt(s.toString());
                                                }
                                                catch (NumberFormatException exception)
                                                {
                                                    icategory.questionCount = 0;
                                                }
                                            }

                                            @Override
                                            public void afterTextChanged(Editable s) { }
                                        };
                                        ((ViewHolderCategory) holder).questionCount.addTextChangedListener(icategory.textWatcher);
                                        break;

                                    case STEPS:
                                        ((ViewHolderTitle) holder).title.setText(R.string.steps);
                                        break;

                                    case STEP_HEADER:
                                        final IStepHeader istepHeader = (IStepHeader) item;
                                        ((ViewHolderStepHeader) holder).radioGroup.check(((ViewHolderStepHeader) holder).radioGroup.getChildAt(istepHeader.condition).getId());
                                        ((ViewHolderStepHeader) holder).radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
                                        {
                                            @Override
                                            public void onCheckedChanged(RadioGroup radioGroup, int checkedId)
                                            {
                                                istepHeader.condition = radioGroup.indexOfChild(radioGroup.findViewById(radioGroup.getCheckedRadioButtonId()));
                                            }
                                        });
                                        break;

                                    case STEP:
                                        final IStep istep = (IStep) item;
                                        ((ViewHolderCheckBox) holder).checkBox.setText(istep.step.name);
                                        ((ViewHolderCheckBox) holder).checkBox.setChecked(istep.isChecked);
                                        ((ViewHolderCheckBox) holder).checkBox.setOnClickListener(new View.OnClickListener()
                                        {
                                            @Override
                                            public void onClick(View view)
                                            {
                                                AtomicBoolean isNotifyRequired = new AtomicBoolean(false);
                                                checkItAndCheckParentUp(istep, isNotifyRequired);
                                                if (isNotifyRequired.get()) notifyDataSetChanged();
                                            }
                                        });
                                        break;

                                    case QUESTIONS:
                                        ((ViewHolderTitle) holder).title.setText(R.string.questions);
                                        break;

                                    case QUESTION:
                                        final IQuestion iquestion = (IQuestion) item;
                                        ((ViewHolderCheckBox) holder).checkBox.setText(iquestion.question.name);
                                        ((ViewHolderCheckBox) holder).checkBox.setChecked(iquestion.isChecked);
                                        ((ViewHolderCheckBox) holder).checkBox.setOnClickListener(new View.OnClickListener()
                                        {
                                            @Override
                                            public void onClick(View view)
                                            {
                                                AtomicBoolean isNotifyRequired = new AtomicBoolean(false);
                                                checkItAndCheckParentUp(iquestion, isNotifyRequired);
                                                if (isNotifyRequired.get()) notifyDataSetChanged();
                                            }
                                        });
                                        break;
                                }
                            }
                        };
                    }
                    recyclerView.setAdapter(adapters[tabPosition].adapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                    recyclerView.setHasFixedSize(true);

                    if (adapters[tabPosition].state != AdapterAndHisState.DATA_ADDED) fillAdapter(tabPosition);
                    adapters[tabPosition].adapter.notifyDataSetChanged();
                }

                container.addView(tab);

                return tab;
            }

            @Override
            public void destroyItem(ViewGroup container, int position, Object object)
            {
                container.removeView((View) object);
            }
        };

        final SlidingTabLayout slidingTabLayout = (SlidingTabLayout) dialog.findViewById(R.id.sliding_tabs);

        rootView.findViewById(R.id.show_blocks).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dialog.show();
                dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                pager.setAdapter(pagerAdapter);
                slidingTabLayout.setViewPager(pager);
            }
        });

        setAllButtons(false);
        if (!isCreation) loadBlockHeaders(this.templateId);
        else
        {
            this.areHeadersLoaded = true;
            loadDataStructure();
        }

        return rootView;
    }



    /**
     * Načte do proměnné informace z plněného formuláře (ten před dialogovým oknem).
     * Rozlišuje, jestli se jedná o vytvoření nové šablony nebo o editaci.
     * @return Naplněný objekt aktuálními daty, připraven pro odeslání na server
     */
    private JSONObject fillTemplate()
    {
        JSONObject template = new JSONObject();
        try
        {
            if (!this.isCreation) template.put("IDSablona", this.templateId);
            template.put("Nazev", ((EditText) getActivity().findViewById(R.id.name)).getText());
            template.put("Popis", ((EditText) getActivity().findViewById(R.id.description)).getText());
            template.put("MichaniBloku", ((CheckBox) getActivity().findViewById(R.id.block_mix)).isChecked());
            template.put("MichatCele", ((CheckBox) getActivity().findViewById(R.id.all_mix)).isChecked());
            template.put("PocetBloku", ((NumberPicker) getActivity().findViewById(R.id.template_block_count)).getValue());
            template.put("IDKurzInfo", User.courseInfoId);
        }
        catch (JSONException e) {}

        return template;
    }



    /**
     * Načte do proměnné informace z celého formuláře v dialogovém okně.
     * Rozlišuje, jestli se jedná o vytvoření nové šablony nebo o editaci.
     * @return Naplněný objekt aktuálními daty, připraven pro odeslání na server
     */
    private JSONArray fillBlocks()
    {
        JSONArray blocks = new JSONArray();
        try
        {
            JSONObject block;
            JSONArray blockStructures;
            JSONObject blockStructure;
            for (int j = 0; j < this.templateBlockCount.getValue(); j++)
            {
                block = new JSONObject();
                block.put("IDBlok", this.blocks[j].isCreated ? j : this.blocks[j].id);
                block.put("TypHodnoceni", this.blocks[j].evaluationType);
                block.put("VahaBloku", this.blocks[j].blockWeight);
                block.put("SlozitostOD", this.blocks[j].complexityFrom);
                block.put("SlozitostDO", this.blocks[j].complexityTo);
                block.put("PocetOtazek", this.blocks[j].questionCount);
                block.put("Minimum", 0);
                block.put("TypOdpovedi", this.blocks[j].answerType);
                block.put("PocetOdpovedi", this.blocks[j].answerCount);
                block.put("MinusProcento", this.blocks[j].percentagesWhichWillBeSubtractedForWrongAnswer);
                block.put("Nazev", this.blocks[j].name);
                block.put("MichaniVBloku", this.blocks[j].mixInBlock);
                block.put("NazevZapnout", this.blocks[j].displayName);
                block.put("Smazany", false);

                blockStructures = new JSONArray();
                for (Category category: this.categories)
                {
                    if (!category.icategory[j].isChecked) continue;

                    blockStructure = new JSONObject();
                    blockStructure.put("IDBlok", this.blocks[j].isCreated ? j : this.blocks[j].id);
                    blockStructure.put("IDKategorie", category.id);
                    blockStructure.put("PocetOtazekKategorie", category.icategory[j].questionCount);
                    blockStructure.put("IDKrok", null);
                    blockStructure.put("Podminka", null);
                    blockStructure.put("IDOtazka", null);
                    blockStructures.put(blockStructure);
                }

                for (Step step: this.steps)
                {
                    if (!step.isteps[j].isChecked) continue;

                    blockStructure = new JSONObject();
                    blockStructure.put("IDBlok", this.blocks[j].isCreated ? j : this.blocks[j].id);
                    blockStructure.put("IDKategorie", step.category.id);
                    blockStructure.put("PocetOtazekKategorie", null);
                    blockStructure.put("IDKrok", step.id);
                    blockStructure.put("Podminka", step.category.icategory[j].isteps.istepHeader.condition);
                    blockStructure.put("IDOtazka", null);
                    blockStructures.put(blockStructure);
                }

                for (Question question: this.questions)
                {
                    if (!question.iquestions[j].isChecked) continue;

                    blockStructure = new JSONObject();
                    blockStructure.put("IDBlok", this.blocks[j].isCreated ? j : this.blocks[j].id);
                    blockStructure.put("IDKategorie", question.category.id);
                    blockStructure.put("PocetOtazekKategorie", null);
                    blockStructure.put("IDKrok", null);
                    blockStructure.put("Podminka", null);
                    blockStructure.put("IDOtazka", question.id);
                    blockStructures.put(blockStructure);
                }

                JSONObject object = new JSONObject();
                object.put("Block", block);
                object.put("BlockStructures", blockStructures);
                blocks.put(object);
            }
        }
        catch (JSONException e) {}

        return blocks;
    }



    /**
     * Vlož novou šablonu.
     */
    private void insertTemplate()
    {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                dialog.dismiss();
                Actionbar.jumpBack();
                SnackbarManager.show(Snackbar.with(getActivity()).type(SnackbarType.MULTI_LINE).text(R.string.template_created));
            }

            @Override
            public void onError(VolleyError error)
            {
                super.onError(error);
                setAllButtons(true);
            }

            @Override
            public void onException(JSONException exception)
            {
                super.onException(exception);
                setAllButtons(true);
            }
        };
        JSONObject parameters = new JSONObject();
        try
        {
            parameters.put("Template", fillTemplate());
            parameters.put("Blocks", fillBlocks());
            parameters.put("CourseInfoId", User.courseInfoId);
            parameters.put("UserId", User.id);
        }
        catch (JSONException e) {}
        Request.post(URL.Block.InsertBlocks(), parameters, response);
    }



    /**
     * Ediduj šablonu.
     */
    private void updateTemplate()
    {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                updateBlocks();
            }

            @Override
            public void onError(VolleyError error)
            {
                super.onError(error);
                setAllButtons(true);
            }

            @Override
            public void onException(JSONException exception)
            {
                super.onException(exception);
                setAllButtons(true);
            }
        };
        Request.post(URL.Template.Update(), fillTemplate(), response);
    }



    /**
     * Edituj bloky.
     */
    private void updateBlocks()
    {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                dialog.dismiss();
                Actionbar.jumpBack();
                SnackbarManager.show(Snackbar.with(getActivity()).type(SnackbarType.MULTI_LINE).text(R.string.template_edited));
            }

            @Override
            public void onError(VolleyError error)
            {
                super.onError(error);
                setAllButtons(true);
            }

            @Override
            public void onException(JSONException exception)
            {
                super.onException(exception);
                setAllButtons(true);
            }
        };
        Request.post(URL.Block.UpdateBlocks(this.templateId), fillBlocks(), response);
    }



    /**
     * Načte hlavičky v dialogovém okně pro všechny bloky a spustí načítání dalšího obsahu.
     * Používá se jen pokud se edituje.
     * @param templateId id šablony
     */
    private void loadBlockHeaders(int templateId)
    {
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                JSONObject o;

                for (int j = 0; j < response.length(); j++)
                {
                    o = response.getJSONObject(j);
                    blocks[j].id = o.getInt("IDBlok");
                    blocks[j].name = o.getString("Nazev");
                    blocks[j].displayName = o.getBoolean("NazevZapnout");
                    blocks[j].complexityFrom = o.getInt("SlozitostOD");
                    blocks[j].complexityTo = o.getInt("SlozitostDO");
                    blocks[j].blockWeight = o.getDouble("VahaBloku");
                    blocks[j].questionCount = o.getInt("PocetOtazek");
                    blocks[j].answerCount = o.getInt("PocetOdpovedi");
                    blocks[j].answerType = o.getInt("TypOdpovedi");
                    blocks[j].evaluationType = o.getInt("TypHodnoceni");
                    blocks[j].percentagesWhichWillBeSubtractedForWrongAnswer = o.getInt("MinusProcento");
                    blocks[j].mixInBlock = o.getBoolean("MichaniVBloku");
                    blocks[j].isCreated = false;
                }

                blockHeadersLoaded();
                loadDataStructure();
            }
        };
        Request.get(URL.Block.GetBlocks(templateId), arrayResponse);
    }



    /**
     * Načte informace o vyplnění formuláře pro vybraný blok.
     * Co má být zaškrtnuto, případně vyplněno.
     * @param blockId id bloku
     * @param position pozice tabu v dialogovém okně
     */
    private void loadBlockStructure(int blockId, final int position)
    {
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                int id;
                JSONObject o;

                for (int j = 0; j < response.length(); j++)
                {
                    o = response.getJSONObject(j);
                    if (!o.isNull("IDKrok"))
                    {
                        id = o.getInt("IDKrok");
                        for (Step step: steps)
                        {
                            if (step.id == id)
                            {
                                if (!step.isteps[position].isChecked) checkItAndCheckParentUp(step.isteps[position], new AtomicBoolean());
                            }
                        }
                    }
                    else if (!o.isNull("IDOtazka"))
                    {
                        id = o.getInt("IDOtazka");
                        for (Question question: questions)
                        {
                            if (question.id == id)
                            {
                                if (!question.iquestions[position].isChecked) checkItAndCheckParentUp(question.iquestions[position], new AtomicBoolean());
                            }
                        }
                    }
                    else if (!o.isNull("IDKategorie"))
                    {
                        id = o.getInt("IDKategorie");
                        for (Category category: categories)
                        {
                            if (category.id == id)
                            {
                                if (!category.icategory[position].isChecked)
                                {
                                    checkItAndCheckParentUp(category.icategory[position], new AtomicBoolean());
                                    if (category.icategory[position].isChecked) category.icategory[position].childCheckedCount = category.icategory[position].category.childCount.get();
                                    else category.icategory[position].childCheckedCount = 0;
                                    checkAllChildren(category.icategory[position], category.icategory[position].isChecked);
                                }

                                if (o.has("IDKategorie")) category.icategory[position].questionCount = o.optInt("PocetOtazekKategorie");
                            }
                        }
                    }
                }

                if (activeRequests.decrementAndGet() == 0) blocksLoaded();
            }
        };
        Request.get(URL.Block.GetBlockStructure(blockId), arrayResponse);
    }



    /**
     * Načte všechny kapitoly a přidá ty v nejvyšší úrovni do struktur.
     * Ostatní přijatá data se posílají do další metody.
     */
    private void loadDataStructure()
    {
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                Chapter chapter;
                IChapter ichapter;
                JSONObject o;

                for (int j = 0; j < response.length(); j++)
                {
                    o = response.getJSONObject(j);
                    if (o.getString("ParentID").contentEquals("null"))
                    {
                        chapter = new Chapter();
                        chapter.id = o.getInt("IDKapitola");
                        chapter.name = o.getString("Nazev") + " [ " + getResources().getString(R.string.chapter) + " ]";
                        chapter.ichapters = new IChapter[20];

                        for (int i = 0; i < 20; i++)
                        {
                            ichapter = new IChapter();
                            ichapter.chapter = chapter;
                            chapter.ichapters[i] = ichapter;
                        }
                        chapters.add(chapter);
                        rootChapters.add(chapter);
                        loadCategories(chapter);
                        loadChildChapters(response, chapter);
                    }
                }

                if (activeRequests.decrementAndGet() == 0) dataStructureLoaded();
            }
        };
        this.activeRequests.incrementAndGet();
        Request.get(URL.Chapter.chaptersbycourse(User.courseInfoId), arrayResponse);
    }



    /**
     * Z načtených kapitol přidá do struktur ke zvolené kapitole i podkapitoly.
     * @param response data
     * @param parent kapitola (na nejvyšší úrovni)
     * @throws JSONException
     */
    private void loadChildChapters(JSONArray response, Chapter parent) throws JSONException
    {
        Chapter chapter;
        IChapter ichapter;
        JSONObject o;

        for (int j = 0; j < response.length(); j++)
        {
            o = response.getJSONObject(j);
            if (o.getString("ParentID").contentEquals("null")) continue;
            else if (o.getInt("ParentID") == parent.id)
            {
                chapter = new Chapter();
                chapter.id = o.getInt("IDKapitola");
                chapter.name = o.getString("Nazev") + " [ " + getResources().getString(R.string.subchapter) + " ]";
                chapter.ichapters = new IChapter[20];

                parent.childCount.incrementAndGet();

                for (int i = 0; i < 20; i++)
                {
                    ichapter = new IChapter();
                    ichapter.chapter = chapter;
                    chapter.ichapters[i] = ichapter;
                    parent.ichapters[i].addChild(ichapter);
                }
                chapters.add(chapter);
                loadCategories(chapter);
                loadChildChapters(response, chapter);
            }
        }
    }



    /**
     * Načte a přidá do sktruktur všechny kategorie dostupné v dané kapitole / podkapitole
     * @param chapter kapitola nebo podkapitola
     */
    private void loadCategories(final Chapter chapter)
    {
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                Category category;
                ICategory icategory;
                JSONObject o;

                chapter.childCount.addAndGet(response.length());

                for (int j = 0; j < response.length(); j++)
                {
                    o = response.getJSONObject(j);

                    category = new Category();
                    category.id = o.getInt("IDKategorie");
                    category.name = o.getString("Nazev") + " [ " + getResources().getString(R.string.category) + " ]";
                    category.icategory = new ICategory[20];

                    for (int i = 0; i < 20; i++)
                    {
                        icategory = new ICategory();
                        icategory.category = category;
                        category.icategory[i] = icategory;
                        chapter.ichapters[i].addChild(icategory);
                    }
                    loadSteps(category);
                    loadQuestions(category);

                    synchronized (categories)
                    {
                        categories.add(category);
                    }
                }

                if (activeRequests.decrementAndGet() == 0) dataStructureLoaded();
            }
        };
        this.activeRequests.incrementAndGet();
        Request.get(URL.Category.getCategories(chapter.id), arrayResponse);
    }



    /**
     * Načte a přidá do sktruktur všechny kroky dostupné v dané kategori
     * @param category kategorie
     */
    private void loadSteps(final Category category)
    {
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                ISteps isteps;
                IStep istep;
                IStepHeader istepHeader;
                Step step;
                JSONObject o;

                if (response.length() > 0)
                {
                    // Hlavička
                    for (int i = 0; i < 20; i++)
                    {
                        isteps = new ISteps();
                        category.icategory[i].isteps = isteps;
                        category.icategory[i].addChild(isteps); // Titulek
                        istepHeader = new IStepHeader();
                        isteps.addChild(istepHeader); // Radio buttony
                        isteps.istepHeader = istepHeader;
                    }
                }

                category.childCount.addAndGet(response.length());

                for (int j = 0; j < response.length(); j++)
                {
                    o = response.getJSONObject(j);
                    step = new Step();
                    step.id = o.getInt("IDKrok");
                    step.name = o.getString("Nazev") + " [ " + getResources().getString(R.string.step) + " ]";
                    step.category = category;
                    step.isteps = new IStep[20];

                    for (int i = 0; i < 20; i++)
                    {
                        istep = new IStep();
                        istep.step = step;
                        step.isteps[i] = istep;
                        category.icategory[i].isteps.addChild(istep);
                    }

                    synchronized (steps)
                    {
                        Fragment_TemplateAdministrationCreationAndModification.this.steps.add(step);
                    }
                }

                if (activeRequests.decrementAndGet() == 0) dataStructureLoaded();
            }
        };
        this.activeRequests.incrementAndGet();
        Request.get(URL.Step.GetSteps(category.id), arrayResponse);
    }



    /**
     * Načte a přidá do sktruktur všechny otázky dostupné v dané kategori
     * @param category
     */
    private void loadQuestions(final Category category)
    {
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                IQuestions iquestions;
                Question question;
                IQuestion iquestion;
                JSONObject o;

                if (response.length() > 0)
                {
                    // Hlavička
                    for (int i = 0; i < 20; i++)
                    {
                        iquestions = new IQuestions();
                        category.icategory[i].iquestions = iquestions;
                        category.icategory[i].addChild(iquestions); // Titulek
                    }
                }

                category.childCount.addAndGet(response.length());

                for (int j = 0; j < response.length(); j++)
                {
                    o = response.getJSONObject(j);

                    question = new Question();
                    question.id = o.getInt("IDOtazka");
                    question.name = o.getString("Zadani") + " [ Otázka ]";
                    question.category = category;
                    question.iquestions = new IQuestion[20];

                    for (int i = 0; i < 20; i++)
                    {
                        iquestion = new IQuestion();
                        iquestion.question = question;
                        question.iquestions[i] = iquestion;
                        category.icategory[i].iquestions.addChild(iquestion);
                    }

                    synchronized (questions)
                    {
                        Fragment_TemplateAdministrationCreationAndModification.this.questions.add(question);
                    }
                }

                if (activeRequests.decrementAndGet() == 0) dataStructureLoaded();
            }
        };
        this.activeRequests.incrementAndGet();
        Request.get(URL.Question.GetQuestions(category.id, -1, -1, true, User.id, true), arrayResponse);
    }



    /**
     * Hlavičky všech bloků jsou načteny.
     */
    private void blockHeadersLoaded()
    {
        synchronized (lock)
        {
            this.areHeadersLoaded = true;

            for (int j = 0; j < this.templateBlockCount.getValue(); j++)
            {
                if (this.adapters[j] == null) continue;
                fillAdapter(j);
                this.adapters[j].adapter.notifyDataSetChanged();
                Log.d("data", "notifikovana");
            }
        }
    }



    /**
     * Struktura bloků je načtena. Pokud se jedná o vytvoření nové šablony, hned
     * se volá blocksLoaded(), pokud ne, tak se začnou načítat informace pro vyplnění
     * (např. co se má zaškrnout)
     */
    private void dataStructureLoaded()
    {
        if (this.isCreation) blocksLoaded();
        else
        {
            this.activeRequests.addAndGet(this.templateBlockCount.getValue());
            for (int j = 0; j < this.templateBlockCount.getValue(); j++) loadBlockStructure(blocks[j].id, j);
        }
    }



    /**
     * Celé bloky jsou načteny, čili úplně všechno...
     */
    private void blocksLoaded()
    {
        synchronized (this.lock)
        {
            this.isDataReady = true;

            for (int j = 0; j < this.templateBlockCount.getValue(); j++)
            {
                if (this.adapters[j] == null) continue;
                fillAdapter(j);
                this.adapters[j].adapter.notifyDataSetChanged();
            }

            setAllButtons(true);
        }
    }



    /**
     * Zobrazí data na obrazovku.
     * Záleží na tom v jakém stavu adaptér je při zavolání této metody.
     * Pak je vykresleno jen to, co je do tohoto okamžiku načteno.
     * Tato metoda teda zajišťuje postupné vykreslování obsahu. Tedy
     * zabranuje tomu, že by se data zobrazila, až poté co budou všechna
     * načtena, což by u složitější šablony mohlo trvat dlouho.
     * @param tabPosition pozice tabu
     */
    private void fillAdapter(int tabPosition)
    {
        HierarchicalAdapter adapter = this.adapters[tabPosition].adapter;
        int state = this.adapters[tabPosition].state;
        int newState = this.adapters[tabPosition].state;

        // Zjištění aktuálního stavu adaptéru
        switch (state)
        {
            case AdapterAndHisState.INITIATION:
                if (this.isDataReady) newState = AdapterAndHisState.DATA_ADDED;
                else if (this.areHeadersLoaded) newState = AdapterAndHisState.HEADER_ADDED;
                else newState = AdapterAndHisState.LOADING_ADDED;
                break;

            case AdapterAndHisState.LOADING_ADDED:
                if (this.isDataReady) newState = AdapterAndHisState.DATA_ADDED;
                else if (this.areHeadersLoaded) newState = AdapterAndHisState.HEADER_ADDED;
                break;

            case AdapterAndHisState.HEADER_ADDED:
                if (this.isDataReady) newState = AdapterAndHisState.DATA_ADDED;
                break;
        }

        if (state == newState) return;

        // Do jakého stavu se chceme dostat, to jsme zjistili v podmínkách nad tímto řádkem podle toho, jaká data už máme načtena
        switch (newState)
        {
            case AdapterAndHisState.LOADING_ADDED:
                this.loading[tabPosition] = new HierarchicalAdapter.Item();
                adapter.addCategory(this.loading[tabPosition], LOADING);
                break;

            case AdapterAndHisState.HEADER_ADDED:
                adapter.addCategory(this.blocks[tabPosition]);
                if (this.loading[tabPosition] != null)
                {
                    Log.d("HEADER_ADDED", "mazu");
                    //adapter.notifyItemRemoved();
                    adapter.removeItem(this.loading[tabPosition]);
                }
                this.loading[tabPosition] = new HierarchicalAdapter.Item();
                adapter.addCategory(this.loading[tabPosition], LOADING);
                break;

            case AdapterAndHisState.DATA_ADDED:
                if (this.loading[tabPosition] != null)
                {
                    Log.d("DATA_ADDED", "mazu");
                    //adapter.notifyItemRemoved();
                    adapter.removeItem(this.loading[tabPosition]);
                }
                if (state < AdapterAndHisState.HEADER_ADDED) adapter.addCategory(this.blocks[tabPosition]);
                for (Chapter rootChapter: this.rootChapters) adapter.addCategory(rootChapter.ichapters[tabPosition]);
                break;
        }

        this.adapters[tabPosition].state = newState;
    }


    /**
     * Zaškrtne nebo odškrtne všechny potomky itemu.
     * @param item
     * @param isChecked
     */
    private void checkAllChildren(HierarchicalAdapter.Item item, boolean isChecked)
    {
        for (HierarchicalAdapter.Item i: item.getChildren())
        {
            switch(i.getType())
            {
                case CHAPTER:
                    ((IChapter) i).isChecked = isChecked;
                    if (isChecked) ((IChapter) i).childCheckedCount = ((IChapter) i).chapter.childCount.get();
                    else ((IChapter) i).childCheckedCount = 0;
                    checkAllChildren(i, isChecked);
                    break;

                case CATEGORY:
                    ((ICategory) i).isChecked = isChecked;
                    if (isChecked) ((ICategory) i).childCheckedCount = ((ICategory) i).category.childCount.get();
                    else ((ICategory) i).childCheckedCount = 0;
                    if (((ICategory) i).iquestions != null) checkAllChildren(((ICategory) i).iquestions, isChecked);
                    if (((ICategory) i).isteps != null) checkAllChildren(((ICategory) i).isteps, isChecked);
                    break;

                case QUESTIONS:
                case STEPS:
                    checkAllChildren(i, isChecked);
                    break;

                case QUESTION:
                    ((IQuestion) i).isChecked = isChecked;
                    break;

                case STEP:
                    ((IStep) i).isChecked = isChecked;
                    break;
            }
        }
    }


    /**
     * Prochází rodiče prvku a zkouší, jestli by se neměli zaškrnout.
     * @param item
     * @param isNotifyRequired bude potom potřeba zavolat na adaptér notify?
     */
    private void checkItAndCheckParentUp(HierarchicalAdapter.Item item, AtomicBoolean isNotifyRequired)
    {
        switch(item.getType())
        {
            case CHAPTER:
                ((IChapter) item).isChecked = !((IChapter) item).isChecked;
                if (item.getParent() == null) return;
                if (((IChapter) item).isChecked) ((IChapter) item.getParent()).childCheckedCount++;
                else ((IChapter) item.getParent()).childCheckedCount--;

                if (((IChapter) item.getParent()).childCheckedCount == ((IChapter) item.getParent()).chapter.childCount.get() || (((IChapter) item.getParent()).childCheckedCount == ((IChapter) item.getParent()).chapter.childCount.get() - 1 && !((IChapter) item).isChecked))
                {
                    checkItAndCheckParentUp(item.getParent(), isNotifyRequired);
                    isNotifyRequired.set(true);
                }
                break;

            case CATEGORY:
                ((ICategory) item).isChecked = !((ICategory) item).isChecked;
                if (((ICategory) item).isChecked) ((IChapter) item.getParent()).childCheckedCount++;
                else ((IChapter) item.getParent()).childCheckedCount--;

                if (((IChapter) item.getParent()).childCheckedCount == ((IChapter) item.getParent()).chapter.childCount.get() || (((IChapter) item.getParent()).childCheckedCount == ((IChapter) item.getParent()).chapter.childCount.get() - 1 && !((ICategory) item).isChecked))
                {
                    checkItAndCheckParentUp(item.getParent(), isNotifyRequired);
                    isNotifyRequired.set(true);
                }
                break;

            case QUESTION:
                ((IQuestion) item).isChecked = !((IQuestion) item).isChecked;
                if (((IQuestion) item).isChecked) ((ICategory) item.getParent().getParent()).childCheckedCount++;
                else ((ICategory) item.getParent().getParent()).childCheckedCount--;

                if (((ICategory) item.getParent().getParent()).childCheckedCount == ((IQuestion) item).question.category.childCount.get() || (((ICategory) item.getParent().getParent()).childCheckedCount == ((IQuestion) item).question.category.childCount.get() - 1 && !((IQuestion) item).isChecked))
                {
                    checkItAndCheckParentUp(item.getParent().getParent(), isNotifyRequired);
                    isNotifyRequired.set(true);
                }
                break;

            case STEP:
                ((IStep) item).isChecked = !((IStep) item).isChecked;
                if (((IStep) item).isChecked) ((ICategory) item.getParent().getParent()).childCheckedCount++;
                else ((ICategory) item.getParent().getParent()).childCheckedCount--;

                if (((ICategory) item.getParent().getParent()).childCheckedCount == ((IStep) item).step.category.childCount.get() || (((ICategory) item.getParent().getParent()).childCheckedCount == ((IStep) item).step.category.childCount.get() - 1 && !((IStep) item).isChecked))
                {
                    checkItAndCheckParentUp(item.getParent().getParent(), isNotifyRequired);
                    isNotifyRequired.set(true);
                }
                break;
        }
    }



    private void setAllButtons(boolean enabled)
    {
        this.isButtonEnabled = enabled;
        this.roundButton.setEnabled(enabled);
        for (int j = 0; j < 20; j++)
        {
            if (this.roundButtonInBlocks[j] != null) this.roundButtonInBlocks[j].setEnabled(enabled);
        }
    }
}