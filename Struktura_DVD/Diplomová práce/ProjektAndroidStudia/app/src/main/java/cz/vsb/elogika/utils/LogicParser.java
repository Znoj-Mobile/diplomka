package cz.vsb.elogika.utils;

import java.util.ArrayList;

public class LogicParser

{

    public static boolean IsFormulaValid(String formula)

    {

        String[] tmp = formula.split("##");

        return ((tmp.length % 2) == 1);

    }

    public static String replaceSpecialChars(String sb){

        sb = sb.replace("∀","##!##");
        sb = sb.replace("∃","##1##");
        sb = sb.replace("≡","##<>##");
        sb = sb.replace("≥","##>=##");//vetsi rovno
        sb = sb.replace("≤","##=<##");//mensi rovno
        sb = sb.replace("⊃","##>##");
        sb = sb.replace("¬","##-##");
        sb = sb.replace("∧","##*##");
        sb = sb.replace("∨","##+##");
        sb = sb.replace("⊆","##<=##");
        sb = sb.replace("⊂","##<##");
        sb = sb.replace("∩","##$in##");
        sb = sb.replace("∩","##$In##");
        sb = sb.replace("∩","##$iN##");
        sb = sb.replace("∩","##$IN##");
        sb = sb.replace("∪","##$un##");
        sb = sb.replace("∪","##$Un##");
        sb = sb.replace("∪","##$uN##");
        sb = sb.replace("∪","##$UN##");
        return sb;
    }


    public static String StringToFormula(String s/*, String dirname*/)

    {
        String dirname= "";
        String[] tmp = s.split("##");

        String vystup = "";

        for (int i = 0; i < tmp.length; i++)

        {

            if (i % 2 == 1)

            { //formule musi byt mezi dvema oddelovaci, tedy v poli bude vzdy na liche pozici

                //   StringBuilder sb = new StringBuilder(tmp[i]);

                String sb = tmp[i];

                sb = sb.replace("!", "∀");
                sb = sb.replace("1", "∃");
                sb = sb.replace("<>", "≡");
                sb = sb.replace(">=", "≥");//vetsi rovno
                sb = sb.replace("=<", "≤");//mensi rovno
                sb = sb.replace(">", "⊃");
                sb = sb.replace("-", "¬");
                sb = sb.replace("*", "∧");
                sb = sb.replace("+", "∨");
                sb = sb.replace("<=", "⊆");
                sb = sb.replace("<", "⊂");
                sb = sb.replace("$in", "∩");
                sb = sb.replace("$In", "∩");
                sb = sb.replace("$iN", "∩");
                sb = sb.replace("$IN", "∩");
                sb = sb.replace("$un", "∪");
                sb = sb.replace("$Un", "∪");
                sb = sb.replace("$uN", "∪");
                sb = sb.replace("$UN", "∪");

                String logicString = sb;
                logicString = ParseSuperscripts(logicString);

                logicString = ParseSubscripts(logicString);

                vystup += logicString;

            }

            else

            {

                if (tmp[i].contains("<elogikatex>"))

                    vystup += GetHtmlFromTex(dirname, tmp[i]);

                else

                    vystup += tmp[i];

            }

        }

        vystup = vystup.replace("\n", "<br>");
        return vystup;

    }



    private static String GetHtmlFromTex(String dirname, String s)

    {
        return "";

    }



    private static ArrayList<Boolean> GetSuperscriptParenthesisList(String text)

    {

        ArrayList<Boolean> parenthesisList = new ArrayList<Boolean>();

        char[] charArray = text.toCharArray();

        int countOfLeftParenthesis = 0;

        Boolean hasIndex = false;

        for (int i = 0; i < charArray.length; i++)

        {

            char ch = charArray[i];

            if (ch == '^')

            {

                hasIndex = true;

                if (charArray[i + 1] == '{')

                {   // HORNI INDEX JE VYRAZ

                    countOfLeftParenthesis++;

                    parenthesisList.add(true);

                    i++;

                }

                else

                {   // HORNI INDEX NENI VYRAZ

                    i++;

                }

            }

            else

            {

                if (countOfLeftParenthesis > 0 && hasIndex)

                {

                    if (ch == '{')

                    {

                        countOfLeftParenthesis++;

                        parenthesisList.add(false);

                    }

                    else if (ch == '}') countOfLeftParenthesis--;

                }

            }

        }

        return parenthesisList;

    }



    private static ArrayList<Boolean> GetSubscriptParenthesisList(String text)

    {

        ArrayList<Boolean> parenthesisList = new ArrayList<Boolean>();

        char[] charArray = text.toCharArray();

        int countOfLeftParenthesis = 0;

        Boolean hasIndex = false;

        for (int i = 0; i < charArray.length; i++)

        {

            char ch = charArray[i];

            if (ch == '_')

            {

                hasIndex = true;

                if (charArray[i + 1] == '{')

                {   // HORNI INDEX JE VYRAZ

                    countOfLeftParenthesis++;

                    parenthesisList.add(true);

                    i++;

                }

                else

                {   // HORNI INDEX NENI VYRAZ

                    i++;

                }

            }

            else

            {

                if (countOfLeftParenthesis > 0 && hasIndex)

                {

                    if (ch == '{')

                    {

                        countOfLeftParenthesis++;

                        parenthesisList.add(false);

                    }

                    else if (ch == '}') countOfLeftParenthesis--;

                }

            }

        }

        return parenthesisList;

    }



    private static String ParseSuperscripts(String text)

    {

        char[] charArray = text.toCharArray();

        String newText = "";

        int countOfLeftParenthesis = 0;

        int countOfLeftParenthesis4Pairing = 0;

        int countOfRightParenthesis = 0;

        Boolean hasIndex = false;

        ArrayList<Boolean> parenthesisList = GetSuperscriptParenthesisList(text);

        for (int i = 0; i < charArray.length; i++)

        {

            char ch = charArray[i];

            if (ch == '^')

            {

                hasIndex = true;

                if (charArray[i + 1] == '{')

                {   // HORNI INDEX JE VYRAZ

                    newText += "<sup>";

                    countOfLeftParenthesis++;

                    countOfLeftParenthesis4Pairing++;

                    i++;

                }

                else

                {   // HORNI INDEX NENI VYRAZ

                    newText += "<sup>" + charArray[i + 1] + "</sup>";

                    i++;

                }

            }

            else

            {

                if (countOfLeftParenthesis > 0 && hasIndex)

                {

                    if (ch == '{')

                    {

                        countOfLeftParenthesis++;

                        countOfLeftParenthesis4Pairing++;

                    }

                    else if (ch == '}')

                    {

                        countOfRightParenthesis++;

                        countOfLeftParenthesis--;

                    }



                    if (ch == '}' && parenthesisList.get(countOfLeftParenthesis4Pairing - countOfRightParenthesis)) newText += "</sup>";

                    else newText += ch;



                    if (ch == '}')

                        parenthesisList.remove(countOfLeftParenthesis4Pairing - countOfRightParenthesis);

                }

                else

                    newText += ch;

            }

        }

        return newText;

    }



    private static String ParseSubscripts(String text)

    {

        char[] charArray = text.toCharArray();

        String newText = "";

        int countOfLeftParenthesis = 0;

        int countOfLeftParenthesis4Pairing = 0;

        int countOfRightParenthesis = 0;

        Boolean hasIndex = false;

        ArrayList<Boolean> parenthesisList = GetSubscriptParenthesisList(text);

        for (int i = 0; i < charArray.length; i++)

        {

            char ch = charArray[i];

            if (ch == '_')

            {

                hasIndex = true;

                if (charArray[i + 1] == '{')

                {   // HORNI INDEX JE VYRAZ

                    newText += "<sub>";

                    countOfLeftParenthesis++;

                    countOfLeftParenthesis4Pairing++;

                    i++;

                }

                else

                {   // HORNI INDEX NENI VYRAZ

                    newText += "<sub>" + charArray[i + 1] + "</sub>";

                    i++;

                }

            }

            else

            {

                if (countOfLeftParenthesis > 0 && hasIndex)

                {

                    if (ch == '{')

                    {

                        countOfLeftParenthesis++;

                        countOfLeftParenthesis4Pairing++;

                    }

                    else if (ch == '}')

                    {

                        countOfRightParenthesis++;

                        countOfLeftParenthesis--;

                    }



                    if (ch == '}' && parenthesisList.get(countOfLeftParenthesis4Pairing - countOfRightParenthesis)) newText += "</sub>";

                    else newText += ch;



                    if (ch == '}')

                        parenthesisList.remove(countOfLeftParenthesis4Pairing - countOfRightParenthesis);

                }

                else

                    newText += ch;

            }

        }

        return newText;

    }

}