package cz.vsb.elogika.ui.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.VolleyError;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.Hash;
import cz.vsb.elogika.utils.Image;

public class ProfileEditation extends Fragment
{
    private JSONObject data;

    private EditText birthSurname;
    private EditText email;
    private EditText language;
    private EditText street;
    private EditText landRegistryNumber;
    private EditText postcode;
    private EditText city;
    private EditText degreeInFrontOfName;
    private EditText degreeBehindName;
    private ImageView avatar;
    private Button roundButton;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        Actionbar.addSubcategory(R.string.profile_editation, this, new int[] {R.string.change_avatar, R.string.change_password, R.string.save}, new Actionbar.OnMenuStringResourceClickListener()
        {
            @Override
            public boolean onMenuStringResourceClick(int stringResource, int position)
            {
                switch (stringResource)
                {
                    case R.string.change_avatar:
                        changeAvatar();
                        break;

                    case R.string.change_password:
                        changePassword();
                        break;

                    case R.string.save:
                        save();
                        break;

                    default:
                        return false;
                }
                return true;
            }
        });

        final View rootView = inflater.inflate(R.layout.fragment_profile_editation, container, false);

        this.roundButton = (Button) rootView.findViewById(R.id.roundButton);
        this.roundButton.setText("✔");
        this.roundButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                roundButton.setEnabled(false);
                save();
            }
        });

        this.birthSurname = (EditText) rootView.findViewById(R.id.user_birth_surname);
        this.email = (EditText) rootView.findViewById(R.id.user_email);
        this.language = (EditText) rootView.findViewById(R.id.user_language);
        this.street = (EditText) rootView.findViewById(R.id.user_street);
        this.landRegistryNumber = (EditText) rootView.findViewById(R.id.user_land_registry_number);
        this.postcode = (EditText) rootView.findViewById(R.id.user_postcode);
        this.city = (EditText) rootView.findViewById(R.id.user_city);
        this.degreeInFrontOfName = (EditText) rootView.findViewById(R.id.user_degree_in_front_of_name);
        this.degreeBehindName = (EditText) rootView.findViewById(R.id.user_degree_behind_name);
        this.avatar = (ImageView) rootView.findViewById(R.id.avatar);

        Bundle bundle = getArguments();
        try
        {
            this.data = new JSONObject(bundle.getString("data"));

            this.birthSurname.setText(this.data.getString("PrijmeniRodne"));
            this.email.setText(this.data.getString("Email"));
            this.language.setText(this.data.getString("Jazyk"));
            this.street.setText(this.data.getString("Ulice"));
            this.landRegistryNumber.setText(this.data.getString("CisloPop"));
            this.postcode.setText(this.data.getString("PSC"));
            this.city.setText(this.data.getString("Mesto"));
            this.degreeInFrontOfName.setText(this.data.getString("TitulPred"));
            this.degreeBehindName.setText(this.data.getString("TitulZa"));

            if (this.data.getString("Fotka").isEmpty())
            {
                rootView.findViewById(R.id.label_avatar).setVisibility(View.GONE);
                rootView.findViewById(R.id.avatar).setVisibility(View.GONE);
            }
            else
            {
                byte[] decodedString = Base64.decode(this.data.getString("Fotka"), Base64.DEFAULT);
                Bitmap bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                Display display = getActivity().getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int width = size.x - (32 * (int) getActivity().getResources().getDisplayMetrics().density);
                int height = (size.x - (32 * (int) getActivity().getResources().getDisplayMetrics().density)) * bitmap.getHeight() / bitmap.getWidth();
                if (bitmap.getWidth() < width)
                {
                    width = bitmap.getWidth();
                    height = bitmap.getHeight();
                }
                this.avatar.setImageBitmap(Bitmap.createScaledBitmap(bitmap, width, height, false));
            }
        }
        catch (JSONException e) {}

        return rootView;
    }


    /**
     * Změní avatara, ale neodešle změnu na server.
     */
    private void changeAvatar()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(intent, 1);
    }


    /**
     * Ověří platnost údajů, poté na serveru změní heslo.
     */
    private void changePassword()
    {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        dialogBuilder.setIcon(android.R.drawable.ic_dialog_info);
        dialogBuilder.setTitle(R.string.password_change);
        dialogBuilder.setPositiveButton(R.string.change, null);
        dialogBuilder.setNegativeButton(R.string.cancel, null);

        LinearLayout layout = (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.fragment_profile_editation_change_password, null);
        final EditText inputCurrentPassword = (EditText) layout.findViewById(R.id.current_password);
        final EditText inputNewPassword = (EditText) layout.findViewById(R.id.new_password);
        final EditText inputNewPassword2 = (EditText) layout.findViewById(R.id.new_password_again);
        dialogBuilder.setView(layout);

        final AlertDialog dialog = dialogBuilder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener()
        {
            @Override
            public void onShow(DialogInterface dialogInterface)
            {
                final Button button = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        final String currentPassword = inputCurrentPassword.getText().toString();
                        final String newPassword = inputNewPassword.getText().toString();
                        final String newPassword2 = inputNewPassword2.getText().toString();
                        String hashedCurrentPassword = null;
                        String password = null;
                        String salt = null;
                        try
                        {
                            hashedCurrentPassword = Hash.getHashedPassword(currentPassword, data.getString("Salt"));
                            password = data.getString("Heslo");
                            salt = data.getString("Salt");
                        }
                        catch (JSONException e) {}

                        if (currentPassword.isEmpty() || newPassword.isEmpty() || newPassword2.isEmpty())
                        {
                            SnackbarManager.show(Snackbar.with(getActivity()).text(R.string.error_input_empty));
                        }
                        else if (!newPassword.contentEquals(newPassword2))
                        {
                            SnackbarManager.show(Snackbar.with(getActivity()).text(R.string.error_new_passwords_mismatch));
                        }
                        else if (!hashedCurrentPassword.contentEquals(password))
                        {
                            Log.d("heslo zíkáno", password);
                            Log.d("heslo zadáno", hashedCurrentPassword);
                            SnackbarManager.show(Snackbar.with(getActivity()).text(R.string.error_current_password_mismatch));
                        }
                        else
                        {
                            final String hashedNewPassword = Hash.getHashedPassword(newPassword, salt);
                            Response response = new Response()
                            {
                                @Override
                                public void onResponse(JSONObject response) throws JSONException
                                {
                                    data.put("Heslo", hashedNewPassword);
                                    dialog.dismiss();
                                    SnackbarManager.show(Snackbar.with(getActivity()).text(R.string.password_change_successful));
                                }
                            };
                            JSONObject parameters = new JSONObject();
                            try
                            {
                                parameters.put("IDUser", User.id);
                                parameters.put("PasswordNew", hashedNewPassword);
                                parameters.put("PasswordOld", hashedCurrentPassword);
                            }
                            catch (JSONException e) {}
                            Request.post(URL.User.UpdatePassword(), parameters, response);
                        }
                    }
                });
            }
        });
        dialog.show();
    }


    /**
     * Ulož všechny údaje.
     */
    private void save()
    {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                SnackbarManager.show(Snackbar.with(getActivity()).text(R.string.data_saved_successful));
                Actionbar.jumpBack();
            }

            @Override
            public void onError(VolleyError error)
            {
                super.onError(error);
                roundButton.setEnabled(true);
            }

            @Override
            public void onException(JSONException exception)
            {
                super.onException(exception);
                roundButton.setEnabled(true);
            }
        };
        try
        {
            this.data.put("PrijmeniRodne", birthSurname.getText());
            this.data.put("Email", email.getText());
            this.data.put("Jazyk", language.getText());
            this.data.put("Ulice", street.getText());
            this.data.put("CisloPop", landRegistryNumber.getText());
            this.data.put("PSC", postcode.getText());
            this.data.put("Mesto", city.getText());
            this.data.put("TitulPred", degreeInFrontOfName.getText());
            this.data.put("TitulZa", degreeBehindName.getText());
        }
        catch (JSONException e) {}
        Request.post(URL.User.Post(), this.data, response);
    }



    /**
     * Využití pro načtení fotky z galerie.
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == 1 && resultCode == Activity.RESULT_OK)
        {
            try
            {
                InputStream inputStream = getActivity().getContentResolver().openInputStream(data.getData());
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                inputStream.close();

                byte[] bytes = Image.scale(bitmap, 500000);
                bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                Display display = getActivity().getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int width = size.x - (32 * (int) getActivity().getResources().getDisplayMetrics().density);
                int height = (size.x - (32 * (int) getActivity().getResources().getDisplayMetrics().density)) * bitmap.getHeight() / bitmap.getWidth();
                if (bitmap.getWidth() < width)
                {
                    width = bitmap.getWidth();
                    height = bitmap.getHeight();
                }
                this.avatar.setImageBitmap(Bitmap.createScaledBitmap(bitmap, width, height, false));
                this.data.put("Fotka", Base64.encodeToString(bytes, Base64.DEFAULT));
            }
            catch (FileNotFoundException e) {}
            catch (IOException e) {}
            catch (JSONException e) {}
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
