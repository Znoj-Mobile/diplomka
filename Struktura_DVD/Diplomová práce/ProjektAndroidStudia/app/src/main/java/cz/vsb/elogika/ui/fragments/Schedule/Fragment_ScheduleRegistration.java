package cz.vsb.elogika.ui.fragments.Schedule;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.dataTypes.Lesson;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.DividerItemDecoration;
import cz.vsb.elogika.utils.Logging;
import cz.vsb.elogika.utils.TimeLogika;

public class Fragment_ScheduleRegistration extends Fragment
{
    private ArrayList<Lesson> lessons;
    private RecyclerView.Adapter adapter;



    public Fragment_ScheduleRegistration()
    {
        this.lessons = new ArrayList();
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        Actionbar.addSubcategory(R.string.schedule_registration, this);

        View rootView = inflater.inflate(R.layout.recycler_view, container, false);
        rootView.findViewById(R.id.loading_progress).setVisibility(View.VISIBLE);
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);

        // Vytvoření adaptéru
        this.adapter = new RecyclerView.Adapter()
        {
            class ViewHolder extends RecyclerView.ViewHolder
            {
                public CheckBox checkBox;
                public ProgressBar progressBar;
                public View line;
                public TextView from;
                public TextView to;
                public TextView firstRow;
                public TextView secondRow;
                public TextView thirdRow;
                public TextView fourthRow;

                public ViewHolder(View itemView)
                {
                    super(itemView);
                    this.checkBox = (CheckBox) itemView.findViewById(R.id.checkbox);
                    this.progressBar = (ProgressBar) itemView.findViewById(R.id.checkbox_progress_bar);
                    this.line = itemView.findViewById(R.id.line);
                    this.from = (TextView) itemView.findViewById(R.id.from);
                    this.to = (TextView) itemView.findViewById(R.id.to);
                    this.firstRow = (TextView) itemView.findViewById(R.id.first_row);
                    this.secondRow = (TextView) itemView.findViewById(R.id.second_row);
                    this.thirdRow = (TextView) itemView.findViewById(R.id.third_row);
                    this.fourthRow = (TextView) itemView.findViewById(R.id.fourth_row);
                }
            }

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
            {
                View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_schedule_registration_item, parent, false);
                return new ViewHolder(view);
            }

            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position)
            {
                final ViewHolder holder = (ViewHolder) viewHolder;
                final Lesson lesson = lessons.get(position);

                holder.progressBar.setVisibility(View.GONE);
                holder.checkBox.setVisibility(View.VISIBLE);
                holder.checkBox.setChecked(lesson.isLogged);
                boolean isEnabled = true;
                if (lesson.isLogged && lesson.type == 0) isEnabled = false;
                else if (!lesson.isLogged && lesson.spaces <= lesson.loggedCount) isEnabled = false;
                holder.checkBox.setEnabled(isEnabled);

                // Nastavení listeneru na checkbox pro přihlášení nebo odhlášení
                holder.checkBox.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        final CheckBox checkbox = (CheckBox) view;
                        checkbox.setChecked(!checkbox.isChecked());

                        if (!checkbox.isChecked()) // přihlášení
                        {
                            new AlertDialog.Builder(getActivity()).setTitle(R.string.login_title).setMessage(R.string.login_question).setPositiveButton(R.string.yes, new DialogInterface.OnClickListener()
                            {
                                public void onClick(DialogInterface dialog, int which)
                                {
                                    holder.progressBar.setLayoutParams(new LinearLayout.LayoutParams(holder.checkBox.getWidth(), holder.checkBox.getHeight()));
                                    holder.checkBox.setVisibility(View.GONE);
                                    holder.progressBar.setVisibility(View.VISIBLE);
                                    login(position);
                                }
                            }).setNegativeButton(R.string.no, null).setIcon(android.R.drawable.ic_dialog_info).show();
                        }
                        else // odhlášení
                        {
                            new AlertDialog.Builder(getActivity()).setTitle(R.string.logout_title).setMessage(R.string.logout_question).setPositiveButton(R.string.yes, new DialogInterface.OnClickListener()
                            {
                                public void onClick(DialogInterface dialog, int which)
                                {
                                    holder.progressBar.setLayoutParams(new LinearLayout.LayoutParams(holder.checkBox.getWidth(), holder.checkBox.getHeight()));
                                    holder.checkBox.setVisibility(View.GONE);
                                    holder.progressBar.setVisibility(View.VISIBLE);
                                    logout(position);
                                }
                            }).setNegativeButton(R.string.no, null).setIcon(android.R.drawable.ic_dialog_info).show();
                        }
                    }
                });
                if (lesson.type == 0) holder.line.setBackgroundColor(getResources().getColor(android.R.color.holo_red_dark));
                else holder.line.setBackgroundColor(getResources().getColor(android.R.color.holo_blue_dark));

                int[] days = {R.string.monday, R.string.tuesday, R.string.wednesday, R.string.thursday, R.string.friday, R.string.saturday};
                holder.from.setText(lesson.from);
                holder.to.setText(lesson.to);
                holder.firstRow.setText(lesson.name + " (" + lesson.shortcut + ")");
                holder.secondRow.setText(lesson.date == null ? (lesson.week + " " + getResources().getString(days[lesson.day])) + " " : TimeLogika.getFormatedDate(lesson.date) + " - " + lesson.room + ", " + (lesson.teacherDegreeBeforeName.isEmpty() ? "" : (lesson.teacherDegreeBeforeName + " ")) + lesson.teacherName + " " + lesson.teacherSurname + " " + lesson.teacherDegreeAfterName);
                holder.thirdRow.setText(getActivity().getResources().getString(R.string.maximum_of_students) + ": " + lesson.spaces);
                holder.fourthRow.setText(getActivity().getResources().getString(R.string.count_of_logged_students) + ": " + lesson.loggedCount);

            }

            @Override
            public int getItemCount()
            {
                return lessons.size();
            }
        };
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        loadAvailableLessons();

        Logging.logAccess("/Pages/Student/RozvrhDetail/RozvrhZapis.aspx");

        return rootView;
    }



    /**
     * Načte všechny dostupné hodiny a zobrazí je v recyclerView.
     */
    private void loadAvailableLessons()
    {
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                getActivity().findViewById(R.id.loading_progress).setVisibility(View.GONE);

                JSONObject o;
                Lesson lesson;

                for (int j = 0; j < response.length(); j++)
                {
                    o = response.getJSONObject(j);
                    lesson = new Lesson();
                    lesson.timetableID = o.getInt("IDTrida");
                    lesson.name = o.getString("Predmet");
                    lesson.shortcut = o.getString("Zkratka");
                    lesson.type = o.getInt("Typ");
                    lesson.from = o.getString("HodinaOD");
                    lesson.to = o.getString("HodinaDO");
                    lesson.room = o.getString("Ucebna");
                    lesson.week = o.getString("Tyden");
                    lesson.date = (o.isNull("Datum") ? null : o.getString("Datum"));
                    lesson.teacherDegreeBeforeName = o.getString("TitulPred");
                    lesson.teacherName = o.getString("Jmeno");
                    lesson.teacherSurname = o.getString("Prijmeni");
                    lesson.teacherDegreeAfterName = o.getString("TitulZa");
                    lesson.isLogged = o.getBoolean("Zapsan");
                    lesson.loggedCount = o.getInt("PocetZapsanych");
                    lesson.spaces = o.getInt("Omezeni");
                    if (o.getString("Den").compareTo("Pondeli") == 0) lesson.day = 0;
                    else if (o.getString("Den").compareTo("Úterý") == 0) lesson.day = 1;
                    else if (o.getString("Den").compareTo("Streda") == 0) lesson.day = 2;
                    else if (o.getString("Den").compareTo("Ctvrtek") == 0) lesson.day = 3;
                    else if (o.getString("Den").compareTo("Pátek") == 0) lesson.day = 4;

                    lessons.add(lesson);
                }
                adapter.notifyDataSetChanged();

                // Zobrazení prázdného layoutu
                if (lessons.size() == 0)
                {
                    getActivity().findViewById(R.id.nothing_to_show).setVisibility(View.VISIBLE);
                }
            }
        };
        Request.get(URL.Timetable.TimetableEntry(User.courseInfoId, User.id), arrayResponse);
    }



    /**
     * Přihlásí studenta na zvolený předmět.
     * @param position index v poli lessons
     */
    private void login(final int position)
    {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                if (response.getBoolean("Success") == true)
                {
                    lessons.get(position).loggedCount++;
                    lessons.get(position).isLogged = true;
                    adapter.notifyItemChanged(position);
                }
                else
                {
                    lessons.get(position).loggedCount = lessons.get(position).spaces;
                    adapter.notifyItemChanged(position);
                    SnackbarManager.show(Snackbar.with(getActivity()).text(R.string.error_lesson_is_full));
                }
            }

            @Override
            public void onError(VolleyError error)
            {
                adapter.notifyItemChanged(position);
                super.onError(error);
            }

            @Override
            public void onException(JSONException e)
            {
                adapter.notifyItemChanged(position);
                // TODO Tuto chybu bude možná dobré vypisovat přímo z Response
                SnackbarManager.show(Snackbar.with(getActivity()).text(R.string.error_reports__server_error));
            }
        };
        JSONObject parameters = new JSONObject();
        Request.post(URL.Timetable.TimetableLogin(User.id, this.lessons.get(position).timetableID), parameters, response);
    }



    /**
     * Odhlásí studenta ze zvoleného předmětu.
     * @param position index v poli lessons
     */
    private void logout(final int position)
    {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                if (response.getBoolean("Success") == true)
                {
                    lessons.get(position).loggedCount--;
                    lessons.get(position).isLogged = false;
                    adapter.notifyItemChanged(position);
                }
                else onException(null);
            }

            @Override
            public void onError(VolleyError error)
            {
                adapter.notifyItemChanged(position);
                super.onError(error);
            }

            @Override
            public void onException(JSONException e)
            {
                adapter.notifyItemChanged(position);
                // TODO Tuto chybu bude možná dobré vypisovat přímo z Response
                SnackbarManager.show(Snackbar.with(getActivity()).text(R.string.error_reports__server_error));
            }
        };
        JSONObject parameters = new JSONObject();
        Request.post(URL.Timetable.TimetableLogout(User.id, this.lessons.get(position).timetableID), parameters, response);
    }
}
