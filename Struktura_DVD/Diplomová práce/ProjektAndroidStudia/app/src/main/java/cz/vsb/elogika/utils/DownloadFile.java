package cz.vsb.elogika.utils;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.webkit.MimeTypeMap;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;

/**
 * <pre>
 * Třída slouží ke stažení souboru
 *
 * Příklad použití:
 * Intent intent = DownloadFile.download(fileName, response.getString("ReseniSoubor"));
 * getActivity().startActivity(intent);
 * SnackbarManager.show(
 * Snackbar.with(getActivity())
 * .type(SnackbarType.MULTI_LINE)
 * .duration(Snackbar.SnackbarDuration.LENGTH_INDEFINITE)
 * .text(getResources().getString(R.string.file) + " '" + fileName + "' " + getResources().getString(R.string.is_downloaded_in_download_folder)));
 * nebo po skončení metody zobrazuji následující toast
 * Toast.makeText(getActivity(), getResources().getString(R.string.file) + " '" + fileName + "' " + getResources().getString(R.string.is_downloaded_in_download_folder), Toast.LENGTH_LONG).show();
 * </pre>
 */
public class DownloadFile {
    /**
     * Metoda po vložení názvu souboru a zakódovaného pole bytů vytvoří soubor a uloží jej do složky Download
     * @return intent pro zobrazení staženého souboru
     * @param filename název souboru
     * @param field pole bytů v base64
     */
    public static Intent download(String filename, String field) {
        /*
        String out = null;
        try {
            //pro vypis textoveho souboru
            out = new String(Base64.decode(file_list.get(position).get("Soubor"), 0), "ISO-8859-2" );
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        */
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        File file = new File(path, filename);
        try {
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
            bos.write(Base64.decode(field, 0));
            bos.flush();
            bos.close();
                    /*
                    //pokud bychom chteli pracovat se stringy a ne polem bytu
                    FileWriter fff = new FileWriter(file);
                    BufferedWriter outX = new BufferedWriter(fff);
                    outX.write(out);
                    outX.close();
                    */
            file.createNewFile();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }

        Uri uri =  Uri.fromFile(file);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        String mime = "*/*";
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        if (mimeTypeMap.hasExtension(
                mimeTypeMap.getFileExtensionFromUrl(uri.toString())))
            mime = mimeTypeMap.getMimeTypeFromExtension(
                    mimeTypeMap.getFileExtensionFromUrl(uri.toString()));
        intent.setDataAndType(uri,mime);
        return intent;

        //
        //

        ///priklad...
        /*
        Intent intent = DownloadFile.download(fileName, response.getString("ReseniSoubor"));
        getActivity().startActivity(intent);
        SnackbarManager.show(
                Snackbar.with(getActivity())
                        .type(SnackbarType.MULTI_LINE)
                        .duration(Snackbar.SnackbarDuration.LENGTH_INDEFINITE)
                        .text(getResources().getString(R.string.file) + " '" + fileName + "' " + getResources().getString(R.string.is_downloaded_in_download_folder)));
        */

        //pro otevreni spust vracenej intent: getActivity().startActivity(intent);
    }

    public static Intent downloadFromText(String filename, String text) {
        /*
        String out = null;
        try {
            //pro vypis textoveho souboru
            out = new String(Base64.decode(file_list.get(position).get("Soubor"), 0), "ISO-8859-2" );
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        */
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        File file = new File(path, filename);
        try {
            //pokud bychom chteli pracovat se stringy a ne polem bytu
            FileWriter fff = new FileWriter(file);
            BufferedWriter outX = new BufferedWriter(fff);
            outX.write(text);
            outX.close();
            file.createNewFile();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }

        Uri uri =  Uri.fromFile(file);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        String mime = "*/*";
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        if (mimeTypeMap.hasExtension(
                mimeTypeMap.getFileExtensionFromUrl(uri.toString())))
            mime = mimeTypeMap.getMimeTypeFromExtension(
                    mimeTypeMap.getFileExtensionFromUrl(uri.toString()));
        intent.setDataAndType(uri,mime);
        return intent;

        //
        //

        ///priklad...
        /*
        Intent intent = DownloadFile.downloadFromText(fileName, text);
        getActivity().startActivity(intent);
        SnackbarManager.show(
                Snackbar.with(getActivity())
                        .type(SnackbarType.MULTI_LINE)
                        .duration(Snackbar.SnackbarDuration.LENGTH_INDEFINITE)
                        .text(getResources().getString(R.string.file) + " '" + fileName + "' " + getResources().getString(R.string.is_downloaded_in_download_folder)));
        */

        //pro otevreni spust vracenej intent: getActivity().startActivity(intent);
    }
}
