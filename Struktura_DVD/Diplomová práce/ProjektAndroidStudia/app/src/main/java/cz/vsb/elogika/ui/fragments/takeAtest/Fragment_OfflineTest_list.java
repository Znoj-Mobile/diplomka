package cz.vsb.elogika.ui.fragments.takeAtest;

import android.app.Dialog;
import android.app.Fragment;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.sqliteDB.MyDB;
import cz.vsb.elogika.ui.BaseActivity;
import cz.vsb.elogika.utils.AES;
import cz.vsb.elogika.utils.Logging;
import cz.vsb.elogika.utils.TimeLogika;

/**
 * Fragment, který zobrazuje stažené offline testy a umožňuje jejich spuštění
 */
public abstract class Fragment_OfflineTest_list extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    /**
     * interface, který je aktivován když se zde změní data
     */
    public abstract void onDataChanged();

    private ArrayList<Map<String, String>> itemList = new ArrayList<Map<String, String>>();;
    private SimpleAdapter adapter;
    SwipeRefreshLayout swipeLayout;
    Dialog myDialog;
    LinearLayout nothinglayout;

    /**
     * Inicializace UI
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Logging.logAccess("OfflineTest", "DownloadedTests");
        Taking_a_test.mode = Taking_a_test.TEST;
        View rootView = inflater.inflate(R.layout.fragment_take_a_test, container, false);
        swipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_update);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeResources(R.color.darker_cyan, R.color.material_blue_grey_800);
        swipeLayout.setRefreshing(false);

        ListView listView = (ListView) rootView.findViewById(R.id.take_a_test_listView);
        if(User.isOnline) rootView.findViewById(R.id.linear_info).setVisibility(View.VISIBLE);
        nothinglayout = (LinearLayout) rootView.findViewById(R.id.take_a_test_nothing_to_show);
        rootView.findViewById(R.id.take_a_Test_progress).setVisibility(View.GONE);

        String[] from = {"name", "activitiesGroup", "minPoints", "maxPoints", "active", "trialsRemaining","mandatory"};
        int[] to = {R.id.take_a_test_name, R.id.take_a_test_activities_group, R.id.take_a_test_minpoints, R.id.take_a_test_maxpoints,
                R.id.take_a_test_active, R.id.take_a_test_trialsRemaining,
        };
        this.adapter = new SimpleAdapter(getActivity(), this.itemList, R.layout.fragment_take_a_test_item, from, to){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView nadpis = (TextView) view.findViewById(R.id.take_a_test_name);
                String mandatory = itemList.get(position).get("mandatory");
                if(mandatory.charAt(0) == 'T' )
                {
                    nadpis.setTextColor(Color.parseColor("#ff4444"));
                }
                else nadpis.setTextColor(Color.WHITE);
                view.setId(Integer.parseInt(mandatory.substring(1)));

                return view;
            }
        };

        listView.setAdapter(this.adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(getActivity(), activity_finished_list.get(position).toString(), Toast.LENGTH_SHORT).show();
                myDialog = new Dialog(getActivity());
                myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                myDialog.setContentView(R.layout.fragment_take_a_test_detail);

                LinearLayout Llayout = (LinearLayout) myDialog.findViewById(R.id.take_a_test_detail_layout);
                final TextView name = (TextView) view.findViewById(R.id.take_a_test_name);
                TextView activites_group = (TextView) view.findViewById(R.id.take_a_test_activities_group);
                //  TextView points = (TextView) view.findViewById(R.id.take_a_test_p);
                TextView minpoints = (TextView) view.findViewById(R.id.take_a_test_minpoints);
                TextView maxpoints = (TextView) view.findViewById(R.id.take_a_test_maxpoints);
                TextView active = (TextView) view.findViewById(R.id.take_a_test_active);
                TextView trialsRemaining = (TextView) view.findViewById(R.id.take_a_test_trialsRemaining);




                String data[];
                String[] labels = {getString(R.string.name), getString(R.string.activity_group), getString(R.string.minmaxpoints), };

                data = new String[]{
                        (String) name.getText(),
                        (String) activites_group.getText(),
                        //    (String) points.getText(),
                        minpoints.getText() + " / " + maxpoints.getText(),
                        (String) trialsRemaining.getText()};


                TextView labelText;
                TextView dataText;
                for (int j = 0; j < data.length && j < labels.length; j++) {
                    labelText = new TextView(getActivity());
                    labelText.setText(labels[j]);
                    if (Build.VERSION.SDK_INT < 23) {
                        labelText.setTextAppearance(view.getContext(), R.style.itemHint);
                    } else {
                        labelText.setTextAppearance(R.style.itemHint);
                    }
                    Llayout.addView(labelText);
                    dataText = new TextView(getActivity());
                    dataText.setText(data[j]);
                    if (Build.VERSION.SDK_INT < 23) {
                        dataText.setTextAppearance(view.getContext(), R.style.itemValue);
                    } else {
                        dataText.setTextAppearance(R.style.itemValue);
                    }
                    dataText.setGravity(Gravity.RIGHT);// | Gravity.CENTER);
                    Llayout.addView(dataText);
                }

                Button buttonStartTest = new Button(getActivity());
                if (User.isOnline)
                {
                    buttonStartTest.setText(R.string.returntry);
                }
                else
                {
                    buttonStartTest.setText(R.string.run_test);
                    String[] isactive = active.getText().toString().split(" - ");

                    buttonStartTest.setEnabled(TimeLogika.isActive(isactive[0],isactive[1]));
                }
                Log.d("elogika", "zobrazen test id " + view.getId());
                final int GeneratedtestID = view.getId();
                buttonStartTest.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (User.isOnline)
                        {
                           deleteUndoneTest(GeneratedtestID, myDialog,(String) name.getText());
                        }
                        else

                        getTestFromDBAndStart(GeneratedtestID);
                    }
                });
                Llayout.addView(buttonStartTest);

                myDialog.show();
            }
        });



        listView.setAdapter(adapter);
        try{
        getAllStoredTests();
        }catch (Exception e){}
        return rootView;
    }

    /**
     * Smaže Offline test a vrátí pokus za nevypracovaný test
     */
    private void deleteUndoneTest(final int generatedtestID, final Dialog myDialog, final String name) {
            Response response = new Response()
            {
                @Override
                public void onResponse(JSONObject response) throws JSONException
                {
                    Log.d("deleteUndoneTest", response.toString());
                    BaseActivity.getDB().deleteTest(generatedtestID);
                    getAllStoredTests();
                    myDialog.cancel();

                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(name + ": " + getActivity().getString(R.string.try_was_returned))
                    );
                }
            };
        Request.post(URL.GeneratedTest.viewgeneratedtest(generatedtestID, false), new JSONObject(), response);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        if(itemList.size() == 0){
            nothinglayout.setVisibility(View.VISIBLE);
            Log.d("databaze", "prazdna");
        }
        else nothinglayout.setVisibility(View.GONE);
    }


    /**
     * Tato metoda načte testy z databáze
     */
    public void getAllStoredTests()
    {
        //první vyzmaže pro znovu načtení
        itemList.clear();
        adapter.notifyDataSetChanged();

        Cursor cursor = BaseActivity.getDB().selectTests();
        if (cursor == null) nothinglayout.setVisibility(View.VISIBLE);
        else{
            while (cursor.moveToNext()) {
                boolean checkmandatory =false;
                if(cursor.getInt(cursor.getColumnIndex(MyDB.TEST_MANDATORY)) == 1)
                    checkmandatory = true;

                addItem(
                        cursor.getInt(cursor.getColumnIndex(MyDB.TEST_GENERATED_ID)) ,
                        cursor.getString(cursor.getColumnIndex(MyDB.TEST_NAME)),
                        cursor.getString(cursor.getColumnIndex(MyDB.TEST_ACTIVITIESGROUP)),
                        cursor.getString(cursor.getColumnIndex(MyDB.TEST_MINPOINTS)) ,
                        cursor.getString(cursor.getColumnIndex(MyDB.TEST_MAXPOINTS)),
                        cursor.getString(cursor.getColumnIndex(MyDB.TEST_TIME_FROM)) ,
                        cursor.getString(cursor.getColumnIndex(MyDB.TEST_TIME_TO)) ,
                        cursor.getString(cursor.getColumnIndex(MyDB.TEST_DATE_SEND_TO)) ,
                        checkmandatory
                        );
                       // cursor.getString(cursor.getColumnIndex(MyDB.TEST_INFO)));
            }
            cursor.close();
        }

    }


    /**
     * Přidá test do seznamu
     *
     * @param testID id testu
     * @param name název testu
     * @param activitiesGroup název skupiny aktivit
     * @param minPoints minimální počet pro úspěch
     * @param maxPoints celkový počet bodů
     * @param activeFrom aktivní od
     * @param activeTo aktivní do
     * @param dateSendTo datum do kdy je možno offline test odevzdat
     * @param mandatory povinny
     */
    private void addItem(int testID, String name, String activitiesGroup, String minPoints, String maxPoints,
                         String activeFrom, String activeTo,String dateSendTo, /*String trialsRemaining,*/ boolean mandatory) {

        HashMap<String, String> item = new HashMap<String, String>();
        item.put("name", name);
        item.put("activitiesGroup", activitiesGroup);
        item.put("minPoints", minPoints);
        item.put("maxPoints", maxPoints);
        item.put("active" , TimeLogika.getFormatedDateTime(activeFrom) + " - " + TimeLogika.getFormatedDateTime(activeTo));
        item.put("dateSendTo", dateSendTo);
        if (mandatory)
            item.put("mandatory", "T"+testID);
        else item.put("mandatory", "F"+testID);
        this.itemList.add(item);
        this.adapter.notifyDataSetChanged();
        nothinglayout.setVisibility(View.GONE);
    }

    /**
     * Po zvolení testu uživatelem se test načte a spustí
     *
     * @param GeneratedtestID id vybraného testu
     */
    private void getTestFromDBAndStart(int GeneratedtestID) {
        try {
            Taking_a_test.generatedTestId = GeneratedtestID;
            Taking_a_test.startAt = TimeLogika.currentServerTime();
            Cursor cursor = BaseActivity.getDB().selectTest(GeneratedtestID); //najdu testinfo pro získání počtu otazek
            while (cursor.moveToNext()) {
                String string = AES.odsifrovat(cursor.getString(cursor.getColumnIndex(MyDB.TEST_INFO)));
                Taking_a_test.timeMinutes = cursor.getInt(cursor.getColumnIndex(MyDB.TEST_TIME_MINUTES));;
                Taking_a_test.DateSendTo = cursor.getString(cursor.getColumnIndex(MyDB.TEST_DATE_SEND_TO));;
                JSONArray jsonArray = new JSONArray(string);
                Taking_a_test.testName = cursor.getString(cursor.getColumnIndex(MyDB.TEST_NAME));
                Taking_a_test.testInfo = jsonArray;
                Taking_a_test.jsonQuestions = new JSONObject[jsonArray.length()];
                Taking_a_test.jsonQuestionOptions = new JSONObject[jsonArray.length()][];
                Taking_a_test.startAt = TimeLogika.currentServerTime();
                User.schoolInfoID = cursor.getInt(cursor.getColumnIndex(MyDB.SCHOOL_INFO_ID));
            }
            cursor.close();
            cursor = BaseActivity.getDB().selectQuestions(GeneratedtestID);
            int poradi=0;
            while (cursor.moveToNext()) {
                int questionid = cursor.getInt(cursor.getColumnIndex(MyDB.QUESTION_ID));

                Taking_a_test.jsonQuestions[poradi] = new JSONObject(AES.odsifrovat(cursor.getString(cursor.getColumnIndex(MyDB.QUESTION_JSON))));

                Cursor cursor2 = BaseActivity.getDB().selectOptions(GeneratedtestID, questionid);
                int poradiodpovedi = 0;

                Taking_a_test.jsonQuestionOptions[poradi] = new JSONObject[cursor2.getCount()];
                while (cursor2.moveToNext()) {
                    Taking_a_test.jsonQuestionOptions[poradi][poradiodpovedi] = new JSONObject(AES.odsifrovat(cursor2.getString(cursor2.getColumnIndex(MyDB.OPTION_JSON))));
                    poradiodpovedi++;
                }
                poradi++;
                cursor2.close();
            }



        } catch (JSONException e) {
            e.printStackTrace();
        }
        myDialog.cancel();
        Intent intent = new Intent(getActivity(), Taking_a_test.class);
        getActivity().startActivity(intent);

    }

    @Override
    public void onRefresh() {
        if(swipeLayout != null) {
            swipeLayout.setRefreshing(false);
        }

    }
}
