package cz.vsb.elogika.dataTypes;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import cz.vsb.elogika.R;

public class Category_step extends RecyclerView.ViewHolder{

    public int IDKrok;
    public int Poradi;
    public String Nazev_kroku;
    public int Slozitost;

    public EditText category_new_step_name;
    public EditText category_new_complexity;
    public TextView category_new_step_position;

    public Category_step(View itemView) {
        super(itemView);
        category_new_step_name = (EditText) itemView.findViewById(R.id.category_new_step_name);
        category_new_complexity = (EditText) itemView.findViewById(R.id.category_new_complexity);
        category_new_step_position = (TextView) itemView.findViewById(R.id.category_new_step_position);
    }

    @Override
    public String toString() {
        return Nazev_kroku;
    }
}
