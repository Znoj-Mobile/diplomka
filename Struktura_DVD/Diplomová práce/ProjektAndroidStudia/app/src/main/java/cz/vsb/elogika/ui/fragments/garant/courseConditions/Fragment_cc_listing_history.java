package cz.vsb.elogika.ui.fragments.garant.courseConditions;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.DividerItemDecoration;
import cz.vsb.elogika.utils.EnumLogika;
import cz.vsb.elogika.utils.TimeLogika;

public class Fragment_cc_listing_history extends Fragment implements SwipeRefreshLayout.OnRefreshListener{
    private View mainView;
    private View rootView;
    private RecyclerView recyclerView;

    SwipeRefreshLayout swipeLayout;
    LinearLayout nothinglayout;

    private RecyclerView.Adapter adapter;

    ArrayList<Map<String, String>> list;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mainView = inflater.inflate(R.layout.fragment_cc_listing_history, container, false);
        rootView = mainView.findViewById(R.id.recycler_view_swipe);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);

        swipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_update);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeResources(R.color.darker_cyan, R.color.material_blue_grey_800);

        nothinglayout = (LinearLayout) rootView.findViewById(R.id.nothing_to_show);
        nothinglayout.setVisibility(View.GONE);

        list = new ArrayList<Map<String,String>>();


        Actionbar.addSubcategory(R.string.sum_detail_of_result_activity_group_history, this);

        this.adapter = new RecyclerView.Adapter() {
            class ViewHolderHeader extends RecyclerView.ViewHolder {
                public ViewHolderHeader(View itemView) {
                    super(itemView);
                }
            }

            class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
                public TextView surname;
                public TextView name;
                public TextView date;
                public TextView action;

                public ViewHolder(View itemView) {
                    super(itemView);
                    itemView.setOnClickListener(this);
                    this.surname = (TextView) itemView.findViewById(R.id.cc_listing_history_surname);
                    this.name = (TextView) itemView.findViewById(R.id.cc_listing_history_name);
                    this.date = (TextView) itemView.findViewById(R.id.cc_listing_history_date);
                    this.action = (TextView) itemView.findViewById(R.id.cc_listing_history_action);
                }

                @Override
                public void onClick(View view) {

                }
            }

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view;
                switch (viewType) {
                    case 1:
                        view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_cc_listing_history_list, parent, false);
                        return new ViewHolder(view);
                    default:
                        return null;
                }
            }
            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
                if (holder.getItemViewType() == 1) {
                    final ViewHolder viewHolder = (ViewHolder) holder;

                    viewHolder.surname.setText(list.get(position).get("prijmeni"));
                    viewHolder.name.setText(list.get(position).get("jmeno"));
                    viewHolder.date.setText(list.get(position).get("datum"));
                    viewHolder.action.setText(EnumLogika.akce(list.get(position).get("akce")));
                }
            }

            @Override
            public int getItemViewType(int position)
            {
                return 1;
            }

            @Override
            public int getItemCount() {
                return list.size();
            }
        };

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        update();
        return mainView;
    }

    public void update() {
        swipeLayout.post(new Runnable() {
            @Override
            public void run() {
                nothinglayout.setVisibility(View.GONE);
                swipeLayout.setEnabled(true);
                swipeLayout.setRefreshing(true);
                list.clear();
                adapter.notifyDataSetChanged();
                data_into_list();
            }
        });
    }

    private void data_into_list() {
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                if (response.length() > 0)
                {
                    for (int j = 0; j < response.length(); j++)
                    {
                        list.add(put_data(
                                response.getJSONObject(j).getString("SkolaLogin"),
                                response.getJSONObject(j).getString("Akce"),
                                TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("Datum")),
                                response.getJSONObject(j).getString("Prijmeni"),
                                response.getJSONObject(j).getString("IDUzivatel"),
                                response.getJSONObject(j).getString("TitulZa"),
                                response.getJSONObject(j).getString("Jmeno"),
                                response.getJSONObject(j).getString("TitulPred")));
                    }
                }
                else{
                    Log.d("Chyba", "Fragment_cc_listing_history");
                    nothinglayout.setVisibility(View.VISIBLE);
                }
                swipeLayout.setRefreshing(false);
                adapter.notifyDataSetChanged();
            }
        };

        Request.get(URL.Student.GetStudentHistoryByIDTermin(Fragment_cc_listing_students.idTermin), arrayResponse);
    }

    private Map<String, String> put_data(String skolaLogin, String akce, String datum, String prijmeni, String idUzivatel, String titulZa, String jmeno, String titulPred) {
        HashMap<String, String> item = new HashMap<String, String>();

        item.put("skolaLogin", skolaLogin);
        item.put("akce", akce);
        item.put("datum", datum);
        item.put("prijmeni", prijmeni);
        item.put("idUzivatel", idUzivatel);
        item.put("titulZa", titulZa);
        item.put("jmeno", jmeno);
        item.put("titulPred", titulPred);

        return item;
    }

    @Override
    public void onRefresh() {
        update();
    }

}
