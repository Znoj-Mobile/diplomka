package cz.vsb.elogika.ui.fragments.garant.tutorAdministration;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.dataTypes.Tutor;
import cz.vsb.elogika.model.Informations;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.DividerItemDecoration;

public class Fragment_TutorAdministrationList extends Fragment
{
    private RecyclerView.Adapter adapter;
    private ArrayList<Tutor> tutors;



    public Fragment_TutorAdministrationList()
    {
        this.tutors = new ArrayList();
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        Actionbar.newCategory(R.string.tutor_managing, this, new int[]{R.string.createNewTutor, R.string.findTutor}, new Actionbar.OnMenuStringResourceClickListener()
        {
            @Override
            public boolean onMenuStringResourceClick(int stringResource, int position)
            {
                switch (stringResource)
                {
                    case R.string.createNewTutor:
                        getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, new Fragment_TutorAdministrationCreationAndModification()).commit();
                        break;

                    case R.string.findTutor:
                        getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, new Fragment_TutorAdministrationAssigning()).commit();
                        break;

                    default:
                        return false;
                }
                return true;
            }
        });

        final View rootView = inflater.inflate(R.layout.recycler_view, container, false);
        // Přidání hlavičky
        ((ViewGroup) rootView).addView(LayoutInflater.from(getActivity()).inflate(R.layout.fragment_tutor_administration_header, container, false), 0);
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        rootView.findViewById(R.id.loading_progress).setVisibility(View.VISIBLE);

        // Vytvoření adaptéru
        this.adapter = new RecyclerView.Adapter()
        {
            class ViewHolderHeader extends RecyclerView.ViewHolder
            {

                public ViewHolderHeader(View itemView)
                {
                    super(itemView);
                }
            }

            class ViewHolder extends RecyclerView.ViewHolder
            {
                public TextView login;
                public TextView name;
                public TextView surname;
                public TextView email;
                public TextView course;
                public Button edit;
                public Button delete;

                public ViewHolder(View itemView)
                {
                    super(itemView);
                    this.login = (TextView) itemView.findViewById(R.id.login);
                    this.name = (TextView) itemView.findViewById(R.id.name);
                    this.surname = (TextView) itemView.findViewById(R.id.surname);
                    this.email = (TextView) itemView.findViewById(R.id.email);
                    this.course = (TextView) itemView.findViewById(R.id.course);
                    this.edit = (Button) itemView.findViewById(R.id.edit);
                    this.delete = (Button) itemView.findViewById(R.id.delete);
                }
            }

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
            {
                View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_tutor_administration_item, parent, false);
                return new ViewHolder(view);
            }

            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position)
            {
                final ViewHolder holder = (ViewHolder) viewHolder;
                final Tutor tutor = tutors.get(position);

                // Nastavení listenerů na tlačítka
                holder.edit.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        Bundle bundle = new Bundle();
                        bundle.putInt("userID", tutors.get(position).id);
                        Fragment_TutorAdministrationCreationAndModification fragment = new Fragment_TutorAdministrationCreationAndModification();
                        fragment.setArguments(bundle);
                        getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment).commit();
                    }
                });
                holder.delete.findViewById(R.id.delete).setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        new AlertDialog.Builder(getActivity()).setTitle(R.string.are_you_sure).setMessage(R.string.delete_tutor_question).setPositiveButton(R.string.yes, new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int which)
                            {
                                removeTutor(position);
                            }
                        }).setNegativeButton(R.string.no, null).setIcon(android.R.drawable.ic_dialog_alert).show();
                    }
                });

                holder.login.setText(tutor.login);
                holder.name.setText(tutor.name);
                holder.surname.setText(tutor.surname);
                holder.email.setText(tutor.email);
                holder.course.setText(Informations.courseInfoNames.get(Informations.courseInfoIds.indexOf(User.courseInfoId)));
            }

            @Override
            public int getItemCount()
            {
                return tutors.size();
            }
        };
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        loadTutors();

        return rootView;
    }



    /**
     * Načte všechny tutory.
     */
    private void loadTutors()
    {
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                getActivity().findViewById(R.id.loading_progress).setVisibility(View.GONE);

                JSONObject o;
                Tutor tutor;

                for (int j = 0; j < response.length(); j++)
                {
                    o = response.getJSONObject(j);
                    tutor = new Tutor();
                    tutor.id = o.getInt("IdUzivatel");
                    tutor.name = o.getString("Jmeno");
                    tutor.surname = o.getString("Prijmeni");
                    tutor.login = o.getString("Login");
                    tutor.email = o.getString("Email");

                    tutors.add(tutor);
                }
                adapter.notifyDataSetChanged();

                // Zobrazení prázdného layoutu
                if (tutors.size() == 0)
                {
                    getActivity().findViewById(R.id.nothing_to_show).setVisibility(View.VISIBLE);
                }
            }
        };

        Request.get(URL.User.getTutorsByIdKurz(User.courseInfoId, User.id), arrayResponse);
    }



    /**
     * Vymaže tutora.
     * @param position index v poli tutors
     */
    private void removeTutor(final int position)
    {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                tutors.remove(position);
                adapter.notifyItemRemoved(position);
                SnackbarManager.show(Snackbar.with(getActivity()).type(SnackbarType.MULTI_LINE).text(R.string.tutor_deleted_successfully));
            }
        };
        Request.get(URL.User.RemoveTutor(this.tutors.get(position).id, User.courseInfoId, User.schoolInfoID), response);
    }
}
