package cz.vsb.elogika.ui.fragments.garant.emails;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.ui.fragments.garant.classes.Fragment_classes_list_of_students;
import cz.vsb.elogika.ui.fragments.garant.consultationHours.LoggedStudent;
import cz.vsb.elogika.ui.fragments.garant.courseConditions.Fragment_cc_listing_students;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.DividerItemDecoration;
import cz.vsb.elogika.utils.DownloadFile;
import cz.vsb.elogika.utils.SelectFileToUpload;
import cz.vsb.elogika.utils.TimeLogika;

public class Fragment_email_message extends Fragment {
    View rootView;
    View mainView;

    EditText subject;
    EditText message;
    Button add_attachment;
    Button send_message;
    ArrayList<String> emails;
    ArrayList<HashMap<String, String>> attachment_list;

    ArrayList<LoggedStudent> loggedStudents;
    Boolean class_style;
    Boolean course;
    Boolean logged;
    Boolean listing;

    protected RecyclerView.Adapter adapter;
    private RecyclerView recyclerView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.send_message, container, false);
        mainView = rootView.findViewById(R.id.recycler_view);
        recyclerView = (RecyclerView) mainView.findViewById(R.id.recyclerView);

        subject = (EditText) rootView.findViewById(R.id.ccl_subject);
        message = (EditText) rootView.findViewById(R.id.ccl_message);
        add_attachment = (Button) rootView.findViewById(R.id.ccl_add_attachment);
        send_message = (Button) rootView.findViewById(R.id.ccl_send_message);

        logged = getArguments().getBoolean("logged", false);
        if(logged){
            try {
                loggedStudents = (ArrayList<LoggedStudent>) getArguments().getSerializable("loggedStudents");
                Actionbar.addSubcategory(R.string.send_message, this);
            } catch (Exception e){}
        }
        listing = getArguments().getBoolean("listing", false);
        class_style = getArguments().getBoolean("class", false);
        course = getArguments().getBoolean("course", false);

        emails = new ArrayList<>();
        attachment_list = new ArrayList<>();


        adapter = new RecyclerView.Adapter() {
            class ViewHolderHeader extends RecyclerView.ViewHolder {
                public ViewHolderHeader(View itemView) {
                    super(itemView);
                }
            }

            class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
                public TextView fileName;
                public Button delete;

                public ViewHolder(View itemView) {
                    super(itemView);
                    itemView.setOnClickListener(this);
                    this.fileName = (TextView) itemView.findViewById(R.id.emails_att_name);
                    this.delete = (Button) itemView.findViewById(R.id.emails_att_delete);
                }


                @Override
                public void onClick(View view) {
                    DownloadFile.download(fileName.getText().toString(), attachment_list.get(getLayoutPosition()).get("byteArray"));
                }
            }

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view;
                switch (viewType) {
                    case 1:
                        view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_email_att_list, parent, false);
                        return new ViewHolder(view);
                    default:
                        return null;
                }
            }

            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
                if (holder.getItemViewType() == 1) {
                    final ViewHolder viewHolder = (ViewHolder) holder;

                    viewHolder.fileName.setText(attachment_list.get(position).get("fileName"));

                    viewHolder.delete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setTitle(R.string.attachment_delete);
                            builder.setCancelable(false);
                            builder.setMessage(R.string.attachment_delete_message);
                            builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog2, int which) {
                                    attachment_list.remove(position);
                                    notifyItemRemoved(position);
                                    notifyItemRangeChanged(position, getItemCount());
                                }
                            });

                            builder.setNegativeButton(R.string.no, null).setIcon(R.drawable.ic_info_outline_white_24dp);
                            Dialog d = builder.create();
                            d.show();
                        }
                    });
                }
            }

            @Override
            public int getItemViewType(int position) {
                return 1;
            }

            @Override
            public int getItemCount() {
                //velikost listu pro vyplneni tabulky
                return attachment_list.size();
            }
        };

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        recyclerView.setHasFixedSize(true);

        recyclerView.setAdapter(adapter);

        send_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(logged){
                    if(loggedStudents != null){
                        for (LoggedStudent ls : loggedStudents) {
                            if(ls.isChecked) {
                                emails.add(ls.email);
                            }
                        }
                    }
                    else if(Fragment_cc_listing_students.logged != null) {
                        for (Map<String, String> user : Fragment_cc_listing_students.logged) {
                            emails.add(user.get("email"));
                        }
                    }
                }

                else if(listing && Fragment_classes_list_of_students.list_of_found_students != null){
                    if(Fragment_classes_list_of_students.list_of_found_students.size() == 0){
                        for(Map<String, String> user : Fragment_classes_list_of_students.list_of_students){
                            if(user.get("checked").equals("true")){
                                emails.add(user.get("email"));
                            }
                        }
                    }
                    else{
                        for(Map<String, String> user : Fragment_classes_list_of_students.list_of_found_students){
                            if(user.get("checked").equals("true")){
                                emails.add(user.get("email"));
                            }
                        }
                    }
                }

                else if(class_style && Fragment_email_add.lesson_list != null){
                    for(Map<String, String> one_class : Fragment_email_add.lesson_list){
                        if(one_class.get("checked").equals("true")){
                            emails.add(one_class.get("idTrida"));
                        }
                    }
                }

                else if(course){
                    emails_from_course();
                    return;
                }
                send_message_multiple();
            }
        });

        add_attachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SelectFileToUpload(getActivity()) {
                    @Override
                    public void selectedFile(String FileName_2, String FileType_2, String FileinBase64_2) {
                        HashMap<String, String> file = new HashMap<>();
                        file.put("fileName", FileName_2);
                        file.put("fileType", FileType_2);
                        file.put("byteArray", FileinBase64_2);
                        attachment_list.add(file);
                        adapter.notifyItemInserted(attachment_list.size()-1);

                    }
                };
                //choose_file(btn_upload_activity);
            }
        });

        return rootView;
    }

    private void emails_from_course() {
        email_is_sending();
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                if (response.length() > 0)
                {
                    for (int j = 0; j < response.length(); j++)
                    {
                        emails.add(response.getJSONObject(j).getString("Email"));
                    }
                    send_message_multiple();
                }
                else{
                    email_failed();
                }
            }
        };

        Request.get(URL.User.GetStudentOfKurz(User.courseInfoId), arrayResponse);
    }

    private void email_is_sending(){
        SnackbarManager.show(
                Snackbar.with(getActivity())
                        .type(SnackbarType.MULTI_LINE)
                        .duration(Snackbar.SnackbarDuration.LENGTH_INDEFINITE)
                        .text(getResources().getString(R.string.email_message) + " " + getResources().getString(R.string.is_sending)));
        send_message.setEnabled(false);
    }
    private void email_sent(){
        SnackbarManager.show(
                Snackbar.with(getActivity())
                        .text(getActivity().getString(R.string.email_message) + " " + getActivity().getString(R.string.email_was_send)));
        send_message.setEnabled(true);
    }
    private void email_failed(){
        SnackbarManager.show(
                Snackbar.with(getActivity())
                        .text(getActivity().getString(R.string.email_message4p) + " " + getActivity().getString(R.string.email_was_not_send)));
        send_message.setEnabled(true);
    }


    public void set_logged(){
        logged = true;
        listing = false;
        class_style = false;
        course = false;
    }

    public void set_listing(){
        logged = false;
        listing = true;
        class_style = false;
        course = false;

    }

    public void set_class(){
        logged = false;
        listing = false;
        class_style = true;
        course = false;

    }

    public void set_course(){
        logged = false;
        listing = false;
        class_style = false;
        course = true;

    }
    //not using
    private void send_message() {
        Response response = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                if (response.getString("Result").equals("true")) {
                    email_sent();
                }
                else{ //nepovedlo se
                    email_failed();
                }
            }
        };
        JSONObject parameters = new JSONObject();
        try
        {
            parameters.put("From", User.email);
            parameters.put("To", User.email);
            parameters.put("Subject", subject.getText().toString());
            parameters.put("Body", message.getText().toString());

            JSONObject attachment = new JSONObject();


            JSONArray attachmentsArray = new JSONArray();
            //attachmentsArray.put("attachment");

            //todo
            parameters.put("Attachments", attachmentsArray);
        }
        catch (JSONException e) {}
        Request.post(URL.EmailMessage.Send(), parameters, response);
    }


    private void send_message_multiple() {
        //Message - I am sending...
        email_is_sending();

        Response response = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                if (response.getString("Result").equals("true")) {
                    email_sent();
                    Fragment_Emails fragment_emails = new Fragment_Emails();
                    getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment_emails).commit();

                }
                else{ //nepovedlo se
                    email_failed();
                }
            }
        };
        JSONObject parameters = new JSONObject();
        try
        {
            parameters.put("Sender", User.email);
            //User.email;User.email
            String email_addresses;
            if(emails.size() > 0) {
                if(class_style){
                    JSONArray classesArray = new JSONArray();
                    for(String item : emails) {
                        classesArray.put(item);
                    }
                    parameters.put("IdClasses", classesArray);
                }
                else {
                        email_addresses = emails.get(0);
                        for (int i = 1; i < emails.size(); i++) {
                            email_addresses += ";" + emails.get(i);
                        }
                        parameters.put("Receiver", email_addresses);
                    }
            }
            else{
                SnackbarManager.show(
                        Snackbar.with(getActivity())
                                .text(getActivity().getString(R.string.email_message4p) + " "  + getActivity().getString(R.string.email_was_not_send) +
                                        " - " + getActivity().getString(R.string.addressees) + " " + getActivity().getString(R.string.was_not_set_plural) ));
                return;
            }
            parameters.put("UserId", User.id);
            parameters.put("CourseInfoId", User.courseInfoId);
            //parameters.put("State", 0);
            parameters.put("SentAt", TimeLogika.getTime());
            //parameters.put("ErrorMessage", "");
            parameters.put("Subject", subject.getText().toString());
            parameters.put("Body", message.getText().toString());


            JSONArray attachmentsArray = new JSONArray();


            for(HashMap<String, String> item : attachment_list) {
                JSONObject attachments = new JSONObject();

                attachments.put("Id", -1);
                attachments.put("Name", item.get("fileName"));
                attachments.put("Content", item.get("byteArray"));
                attachments.put("ContentType", item.get("fileType"));

                attachmentsArray.put(attachments);
            }

            parameters.put("Attachments", attachmentsArray);

            //parameters.put("Attachments", "[]");
        }
        catch (JSONException e) {}
        if(class_style){
            Request.post(URL.EmailMessage.SentToMultipleClasses(), parameters, response);
        }
        else {
            Request.post(URL.EmailMessage.SendToMultipleUsers(), parameters, response);
        }
    }
}
