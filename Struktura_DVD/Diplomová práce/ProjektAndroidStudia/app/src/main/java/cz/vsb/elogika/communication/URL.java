package cz.vsb.elogika.communication;

import android.text.format.Formatter;
import android.util.Log;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

public class URL
{
    public static String getLocalIpAddress()
    {
        try
        {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); )
            {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); )
                {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress())
                    {
                        String ip = Formatter.formatIpAddress(inetAddress.hashCode());
                        Log.i("elogika", "***** IP=" + ip);
                        return ip; //TODO opravdu ukazuje spravnou ip?
                    }
                }
            }
        }
        catch (SocketException ex)
        {
            Log.e("elogika", ex.toString());
        }
        return "0.0.0.0";
    }

    private static final String URLADDRESS = "https://elogika.vsb.cz";
    private static final String HOST = URLADDRESS + ":23590";

//    private static final String URLADDRESS = "http://158.196.141.100";
//    private static final String HOST = URLADDRESS + ":8090";

    private static final String API = "/api";


    public static class User
    {
        private static final String SECTION = "/User";


        public static String Get(int userID)
        {
            return (HOST + API + SECTION + "/Get/" + userID);
        }


        public static String LoginObject()
        {
            return (HOST + API + SECTION + "/loginobject");
        }


        public static String LoginSecureObject()
        {
            return (HOST + API + SECTION + "/loginsecureobject");
        }


        public static String GetSaltByLogin(String login)
        {
            return (HOST + API + SECTION + "/getsaltbylogin?login=" + login);
        }


        public static String Login(String user, String password)
        {
            return (HOST + API + SECTION + "/login?login=" + user + "&password=" + password);
        }


        public static String RegistrationExistUser(int userID, int courseInfoID)
        {
            return (HOST + API + SECTION + "/registrationexistuser?userId=" + userID + "&courseInfoId=" + courseInfoID);
        }


        public static String UnregistrationExistUser(int userID, int courseInfoID)
        {
            return (HOST + API + SECTION + "/unregistrationexistuser?userId=" + userID + "&courseInfoId=" + courseInfoID);
        }


        public static String RegistrationUser(int courseInfoID, String login)
        {
            return (HOST + API + SECTION + "/registrationuser?courseInfoId=" + courseInfoID + "&loginSkola=" + login);
        }


        public static String Post()
        {
            return (HOST + API + SECTION + "/Post");
        }


        public static String GenerateLogin(String surname)
        {
            return (HOST + API + SECTION + "/generatelogin?p_prijmeni=" + surname);
        }


        public static String UpdatePassword()
        {
            return (HOST + API + SECTION + "/updatepassword");
        }


        public static String getTutorsByIdKurz(int courseInfoID, int userID)
        {
            return (HOST + API + SECTION + "/gettutorsbyidkurz?courseInfoId=" + courseInfoID + "&userId=" + userID);
        }

        public static String RemoveTutor(int tutorID, int courseInfoID, int schoolInfoID)
        {
            return (HOST + API + SECTION + "/RemoveTutor?tutorId=" + tutorID + "&courseInfoId=" + courseInfoID + "&schoolInfoId=" + schoolInfoID);
        }


        public static String InsertTutor(int tutorID, int courseInfoID, int schoolInfoID, String schoolLogin)
        {
            return (HOST + API + SECTION + "/inserttutor?tutorId="+ tutorID + "&schoolInfoId=" + schoolInfoID + "&courseInfoId=" + courseInfoID + "&schoolLogin=" + schoolLogin);
        }


        public static String InsertTutors(int courseInfoID, int schoolInfoID, String schoolLogin)
        {
            return (HOST + API + SECTION + "/inserttutors?schoolInfoId=" + schoolInfoID + "&courseInfoId=" + courseInfoID + "&schoolLogin=" + schoolLogin);
        }


        public static String FindUserBySchool(String surname, int courseInfoID, int schoolID)
        {
            return (HOST + API + SECTION + "/FindUserBySchool?text=" + surname + "&schoolId=" + schoolID + "&courseInfoId=" + courseInfoID);
        }

        public static String SendBugReport() {
            return (HOST + API + SECTION + "/SendBugReport");
        }

        public static String LogAccess()
        {
            return (HOST + API + SECTION + "/LogAccess");
        }

        public static String GetStudentOfTrida(int classId) {
            return (HOST + API + SECTION + "/GetStudentOfTrida?classId=" + classId);
        }

        public static String RemoveStudent(String studentId, int classId, int schoolID) {
            return (HOST + API + SECTION + "/RemoveStudent?studentId=" + studentId + "&classId=" + classId + "&schoolID=" + schoolID);
        }

        public static String InsertStudent(int studentId, int schoolInfoID, int classId, String schoolLogin) {
            return (HOST + API + SECTION + "/InsertStudent?studentId=" + studentId + "&schoolInfoID=" + schoolInfoID + "&classId=" + classId + "&schoolLogin=" + schoolLogin);
        }

        public static String getlogaccessbyiduzivatel(String idUzivatel, String casOD, String casDO) {
            return (HOST + API + SECTION + "/getlogaccessbyiduzivatel?idUzivatel=" + idUzivatel + "&casOD=" + casOD + "&casDO=" + casDO);
        }

        public static String getlogactionsbyiduzivatel(String idUzivatel, String casOD, String casDO) {
            return (HOST + API + SECTION + "/getlogactionsbyiduzivatel?idUzivatel=" + idUzivatel + "&casOD=" + casOD + "&casDO=" + casDO);
        }

        public static String getlogonlinetestbyiduzivatel(String idUzivatel, String casOD, String casDO) {
            return (HOST + API + SECTION + "/getlogonlinetestbyiduzivatel?idUzivatel=" + idUzivatel + "&casOD=" + casOD + "&casDO=" + casDO);
        }

        public static String GetUserIdBySchoolsLogin(int schoolId, String schoolLogin) {
            return (HOST + API + SECTION + "/GetUserIdBySchoolsLogin?schoolId=" + schoolId + "&schoolLogin=" + schoolLogin);
        }

        public static String insertUser() {
            return (HOST + API + SECTION + "/insertuser");
        }

        public static String GetStudentOfKurz(int idCourseInfo) {
            return (HOST + API + SECTION + "/GetStudentOfKurz?idCourseInfo=" + idCourseInfo);
        }
    }


    public static class School
    {
        private static final String SECTION = "/School";


        public static String SchoolsByUser(int userID)
        {
            return (HOST + API + SECTION + "/schoolsbyuser?userId=" + userID);
        }

        public static String schoolinfobyyear(int yearId, int schoolId)
        {
            return (HOST + API + SECTION + "/schoolinfobyyear?yearId=" + yearId + "&schoolId=" + schoolId);
        }

    }


    public static class Role
    {
        private static final String SECTION = "/Role";


        public static String GetRoles(int userID, int schoolID)
        {
            return (HOST + API + SECTION + "/getroles?userId=" + userID + "&schoolId=" + schoolID);
        }
    }


    public static class Year
    {
        private static final String SECTION = "/Year";


        public static String YearByUser(int userID, int schoolID, int roleID)
        {
            return (HOST + API + SECTION + "/yearbyuser?userId=" + userID + "&schoolId=" + schoolID + "&roleId=" + roleID);
        }
    }


    public static class Semester
    {
        private static final String SECTION = "/Semester";


        public static String SemestersByCourseInfo(int courseInfoID)
        {
            return (HOST + API + SECTION + "/semestersbycourseinfo?courseInfoId=" + courseInfoID);
        }
    }


    public static class Timetable
    {
        private static final String SECTION = "/Timetable";

        public static String TimetableGet(int semesterID, int userID)
        {
            return (HOST + API + SECTION + "/timetableget?semesterId=" + semesterID + "&userId=" + userID);
        }

        public static String TimetableEntry(int courseInfoID, int userID)
        {
            return (HOST + API + SECTION + "/timetableentry?courseInfoId=" + courseInfoID + "&userId=" + userID);
        }

        public static String TimetableLogin(int studentID, int schoolClassID)
        {
            return (HOST + API + SECTION + "/timetablelogin?studentId=" + studentID + "&schoolClassId=" + schoolClassID);
        }

        public static String TimetableLogout(int studentID, int schoolClassID)
        {
            return (HOST + API + SECTION + "/timetablelogout?studentId=" + studentID + "&schoolClassId=" + schoolClassID);
        }

        public static String TimetableInsert(int userID, int semesterID)
        {
            return (HOST + API + SECTION + "/timetableinsert?userId=" + userID + "&semesterId=" + semesterID);
        }

        public static String TimetableUpdate()
        {
            return (HOST + API + SECTION + "/timetableupdate");
        }

        public static String RemoveTimetable(int timetableID)
        {
            return (HOST + API + SECTION + "/RemoveTimetable?timetableId=" + timetableID);
        }
    }


    public static class SolvedActivities
    {
        private static final String SECTION = "/SolvedActivities";

        public static String GetAll(int userId, int courseInfoId)
        {
            return (HOST + API + SECTION + "/getall?userId=" + userId + "&courseInfoId=" + courseInfoId);
        }

        public static String Exists(int activityId, int dateId, int studentId)
        {
            return (HOST + API + SECTION + "/Exists?activityId=" + activityId + "&dateId=" + dateId + "&studentId=" + studentId);
        }

        public static String getsolvedactivityfile(int activityId)
        {
            return (HOST + API + SECTION + "/getsolvedactivityfile?activityId=" + activityId);
        }

        public static String Post()
        {
            return (HOST + API + SECTION + "/Post");
        }

        public static String postEvaluation() {
            return (HOST + API + SECTION + "/postEvaluation");
        }
    }

    public static class CourseConditions
    {
        private static final String SECTION = "/CourseConditions";

        public static String ActivityDates(int courseInfoId, int studentId)
        {
            return (HOST + API + SECTION + "/activitydates?courseInfoId=" + courseInfoId + "&studentId=" + studentId);
        }

        public static String CurrentAktivita(int courseInfoId, int userId)
        {
            return (HOST + API + SECTION + "/currentaktivita?courseInfoId=" + courseInfoId + "&userId=" + userId);

        }

        public static String DatesByCourse(int courseInfoId, int userId)
        {
            return (HOST + API + SECTION + "/datesbycourse?courseInfoId=" + courseInfoId + "&userId=" + userId);

        }

        public static String LoginToDate(int studentId, int dateId)
        {
            return (HOST + API + SECTION + "/logintodate?studentId=" + studentId + "&dateId=" + dateId);

        }

        public static String LogoutFromDate(int studentId, int dateId)
        {
            return (HOST + API + SECTION + "/logoutfromdate?studentId=" + studentId + "&dateId=" + dateId);

        }

        public static String addloglist()
        {
            return (HOST + API + SECTION + "/addloglist");

        }

        public static String groupactivites(int courseInfoId, int id, int roleID)
        {
            return (HOST + API + SECTION + "/groupactivites?courseInfoId=" + courseInfoId + "&userId=" + id + "&roleID=" + roleID);
        }

        public static String currenttestbychapterrt(int chapterId, int studentId)
        {
            return (HOST + API + SECTION + "/currenttestbychapter?chapterId=" + chapterId + "&studentId=" + studentId);
        }

        public static String datesbycourse(int courseInfoId, int userId)
        {
            return (HOST + API + SECTION + "/datesbycourse?courseInfoId=" + courseInfoId + "&userId=" + userId);
        }
    }

    public static class ConsultationHours
    {
        private static final String SECTION = "/ConsultationHours";


        public static String ConsultationHoursWhichIsLogged(int courseInfoId, int userID)
        {
            return (HOST + API + SECTION + "/ConsultationHoursWhichIsLogged?courseInfoId=" + courseInfoId + "&studentId=" + userID);
        }


        public static String ConsultationHoursById(int consultationHoursID)
        {
            return (HOST + API + SECTION + "/ConsultationHoursById?consultationHoursId=" + consultationHoursID);
        }


        public static String ConsultationHoursDatesToLogin(int courseInfoId, int userID)
        {
            return (HOST + API + SECTION + "/ConsultationHoursDatesToLogin?courseInfoId=" + courseInfoId + "&studentId=" + userID);
        }

        public static String StudentsConsultationHours(int consultationHoursID, String exactDate)
        {
            return (HOST + API + SECTION + "/studentsconsultationhours?consultationHoursId=" + consultationHoursID + "&exactDate=" + exactDate);
        }

        public static String StudentsConsultationHoursCount(int consultationHoursID, String exactDate)
        {
            return (HOST + API + SECTION + "/studentsconsultationhourscount?consultationHoursId=" + consultationHoursID + "&exactDate=" + exactDate);
        }


        public static String login()
        {
            return (HOST + API + SECTION + "/login");
        }


        public static String logout()
        {
            return (HOST + API + SECTION + "/logout");
        }


        public static String GetConsultationHoursDates(int courseInfoId, int userID)
        {
            return (HOST + API + SECTION + "/getconsultationhoursdates?courseInfoId=" + courseInfoId + "&userId=" + userID);
        }


        public static String GetOccupiedConsultationHoursDates(int courseInfoId, int userID)
        {
            return (HOST + API + SECTION + "/getoccupiedconsultationhoursdates?courseInfoId=" + courseInfoId + "&userId=" + userID);
        }


        public static String Save()
        {
            return (HOST + API + SECTION + "/save");
        }


        public static String Remove(int consultationHoursId)
        {
            return (HOST + API + SECTION + "/Remove?consultationHoursId=" + consultationHoursId);
        }
    }

    public static class News
    {
        private static final String SECTION = "/News";

        public static String NewsByCourse(int courseInfoId, int ownerId, int studentId)
        {
            return (HOST + API + SECTION + "/newsbycourse?courseInfoId=" + courseInfoId + "&ownerId=" + ownerId + "&studentId=" + studentId);
        }

        public static String NewsByCourse(int courseInfoId, int studentId)
        {
            return (HOST + API + SECTION + "/newsbycourse?courseInfoId=" + courseInfoId + "&ownerId=" + "&studentId=" + studentId);
        }
        public static String NewsByCourse_Garant(int courseInfoId, int studentId)
        {
            return (HOST + API + SECTION + "/newsbycourse?courseInfoId=" + courseInfoId + "&ownerId=" + studentId + "&studentId=null");
        }
        public static String getAttachment(int newsId, int attachmentId)
        {
            return (HOST + API + SECTION + "/newsattachment?newsId=" + newsId + "&attachmentId=" + attachmentId);
        }

        public static String deleteattachment(int attachmentId)
        {
            return (HOST + API + SECTION + "/deleteattachment?attachmentId=" + attachmentId);
        }

        public static String update()
        {
            return (HOST + API + SECTION + "/update");
        }
        public static String insert(int schoolInfoId, int courseInfoId)
        {
            return (HOST + API + SECTION + "/insert?schoolInfoId=" + schoolInfoId + "&courseInfoId="+courseInfoId);
        }
        public static String insertattachments(int newsId)
        {
            return (HOST + API + SECTION + "/insertattachments?newsId=" + newsId);
        }
        @Deprecated
        public static String delete(int newsId, int schoolInfoId)
        {
            return (HOST + API + SECTION + "/delete?newsId=" + newsId+ "&schoolInfoId=" +schoolInfoId);
        }
        public static String remove(int newsId, int schoolInfoId)
        {
            return (HOST + API + SECTION + "/Remove?newsId=" + newsId+ "&schoolInfoId="  + schoolInfoId);
        }
    }

    public static class GeneratedTest
    {
        private static final String SECTION = "/GeneratedTest";

        public static String generatedquestions(int generatedTestID)
        {
            return (HOST + API + SECTION + "/generatedquestions?generatedTestId=" + generatedTestID);
        }

        public static String generatedanswers(int generatedQuestionID)
        {
            return (HOST + API + SECTION + "/generatedanswers?generatedQuestionId=" + generatedQuestionID);
        }

        public static String getQuestionText(int generatedQuestionID)
        {
            return (HOST + API + "/Question/Get/" + generatedQuestionID);
        }

        public static String getgeneratedquestionbyid(int generatedQuestionID)
        {
            return (HOST + API + SECTION + "/getgeneratedquestionbyid?generatedQuestionId=" + generatedQuestionID);
        }

        public static String getAvaiableTests(int studentId, int courseInfoId)
        {
            return (HOST + API + "/CourseConditions/availabletests?courseInfoId=" + courseInfoId + "&studentId=" + studentId);
        }

        public static String generateTest()
        {
            return (HOST + API + SECTION + "/generatetest");
        }

        public static String savegenerateTest()
        {
            return (HOST + API + SECTION + "/savegeneratetest");
        }

        public static String savegenerateTestkeyvalue()
        {
            return (HOST + API + SECTION + "/savegeneratetestkeyvalue");
        }

        //TEST DRAWN
        public static String viewgeneratedtest(int generatedtestID, boolean b)
        {
            if (b)
                return (HOST + API + SECTION + "/viewgeneratedtest?generatedTestId=" + generatedtestID + "&viewed=true");
            else
                return (HOST + API + SECTION + "/viewgeneratedtest?generatedTestId=" + generatedtestID + "&viewed=false");
        }

        public static String answersbygeneratedquestion(int IDVygenerovanyTestUzivatel, int IDVygenerovanaOtazka)
        {
            return (HOST + API + SECTION + "/answersbygeneratedquestion?userGeneratedTestId=" + IDVygenerovanyTestUzivatel + "&generatedQuestionId=" + IDVygenerovanaOtazka);
        }

        public static String papirovytestobrazek(int userGeneratedTestId)
        {
            return (HOST + API + SECTION + "/generatedtestimages?userGeneratedTestId=" + userGeneratedTestId);
        }

        public static String setvygenerovanytestuzivatelcorrect(int userGeneratedTestId, boolean correct)
        {
            if (correct)
                return (HOST + API + SECTION + "/setvygenerovanytestuzivatelcorrect?userGeneratedTestId=" + userGeneratedTestId + "&correct=true");
            else
                return (HOST + API + SECTION + "/setvygenerovanytestuzivatelcorrect?userGeneratedTestId=" + userGeneratedTestId + "&correct=false");
        }

        public static String canGenerate(int testId, int userId, int terminId)
        {
            return (HOST + API + SECTION + "/cangenerate?testId=" + testId + "&userId=" + userId + "&terminId=" + terminId);
        }

        public static String getgeneratedtestbytestid(String testId, String dateId) {
            return (HOST + API + SECTION + "/getgeneratedtestbytestid?testId=" + testId + "&dateId=" + dateId);
        }

        public static String generatetestsprava(int idUzivatel, String idSablona, String hlavni) {
            return (HOST + API + SECTION + "/generatetestsprava?idUzivatel=" + idUzivatel + "&idSablona=" + idSablona + "&idVlastnik=" + idUzivatel + "&hlavni=" + hlavni);
        }
    }

    public static class TestDrawn
    {
        private static final String SECTION = "/TestDrawn";

        public static String requestInformation(int studentId, int courseInfoId)
        {
            return (HOST + API + SECTION + "/completedtests?studentId=" + studentId + "&courseInfoId=" + courseInfoId);
        }

    }

    public static class Course
    {
        private static final String SECTION = "/Course";

        public static String PublicCourses(int userId)
        {
            return (HOST + API + SECTION + "/publiccourses?userId=" + userId);
        }

        public static String CourseByStudent(int yearId, int userId)
        {
            return (HOST + API + SECTION + "/coursebystudent?yearId=" + yearId + "&studentId=" + userId);
        }

        public static String CourseByGarant(int yearId, int userId)
        {
            return (HOST + API + SECTION + "/coursebygarant?yearId=" + yearId + "&garantId=" + userId);
        }

        public static String CourseByTutor(int yearId, int userId)
        {
            return (HOST + API + SECTION + "/coursebytutor?yearId=" + yearId + "&tutorId=" + userId);
        }

        public static String GetClassTimeLimitsFromTo(int courseInfoId) {
            return (HOST + API + SECTION + "/GetClassTimeLimitsFromTo?courseInfoId=" + courseInfoId);
        }

        public static String SetKurzLimits(int courseInfoId, int studyForm, int min, int max) {
            return (HOST + API + SECTION + "/SetKurzLimits?courseInfoId=" + courseInfoId + "&studyForm=" + studyForm + "&min=" + min + "&max=" + max);
        }

        public static String GetKurzLimits(int courseInfoId, int studyForm) {
            return (HOST + API + SECTION + "/GetKurzLimits?courseInfoId=" + courseInfoId + "&studyForm=" + studyForm);
        }

        public static String SetClassTimeLimits(int courseInfoId) {
            return (HOST + API + SECTION + "/SetClassTimeLimits?courseInfoId=" + courseInfoId);
        }
    }

    public static class StudentEvaluation
    {
        private static final String SECTION = "/StudentEvaluation";

        public static String getdata(int courseInfoId, int userId)
        {
            return (HOST + API + SECTION + "/getdata?courseInfoId=" + courseInfoId + "&userId=" + userId);
        }
    }

    public static class EvaluationQuestion
    {

        private static final String SECTION = "/EvaluationQuestion";

        public static String GetEvaluationByGeneratedTest(int userGeneratedTestId)
        {
            return (HOST + API + SECTION + "/GetEvaluationByGeneratedTest?userGeneratedTestId=" + userGeneratedTestId);
        }
    }



    public static class Block
    {
        private static final String SECTION = "/Block";

        public static String GetBlocks(int templateId)
        {
            return (HOST + API + SECTION + "/GetBlocks?templateId=" + templateId);
        }

        public static String GetBlockStructure(int blockId)
        {
            return (HOST + API + SECTION + "/GetBlockStructure?blockId=" + blockId);
        }

        public static String InsertBlocks()
        {
            return (HOST + API + SECTION + "/InsertBlocks");
        }

        public static String UpdateBlocks(int templateId)
        {
            return (HOST + API + SECTION + "/UpdateBlocks?templateId=" + templateId);
        }
    }



    public static class Chapter
    {
        private static final String SECTION = "/Chapter";

        public static String chaptersbycourse(int courseInfoId)
        {
            return (HOST + API + SECTION + "/chaptersbycourse?courseInfoId=" + courseInfoId);
        }

        public static String materials(int chapterId)
        {
            return (HOST + API + SECTION + "/materials?chapterId=" + chapterId);
        }

        public static String insertchapter(int courseInfoId)
        {
            return (HOST + API + SECTION + "/insertchapter?courseInfoId=" + courseInfoId);
        }
        public static String removeMaterial(int IDmaterial)
        {
            return (HOST + API + SECTION + "/RemoveMaterial/" + IDmaterial);
        }
        public static String updatechapter()
        {
            return (HOST + API + SECTION + "/updatechapter");
        }

        public static String deletechapter(int chapterID)
        {
            return (HOST + API + SECTION + "/deletechapter/" + chapterID);
        }
        public static String insertmaterial()
        {
            return (HOST + API + SECTION + "/insertmaterial");
        }
    }

    public static class OnlineStatistics
    {
        private static final String SECTION = "/OnlineStatistics";

        public static String InsertStatOtazkaUzivatelList()
        {
            return (HOST + API + SECTION + "/InsertStatOtazkaUzivatelList");
        }

        public static String InsertStatOdpovedUzivatelList()
        {
            return (HOST + API + SECTION + "/InsertStatOdpovedUzivatelList");
        }
    }

    public static class Date
    {
        private static final String SECTION = "/Date";


        public static String GetDates(String groupActivityId, String testId, String activityId)
        {
            return (HOST + API + SECTION + "/GetDates?groupActivityId=" + groupActivityId + "&testId=" + testId + "&activityId=" + activityId);
        }

        public static String Delete(String idTermin) {
            return (HOST + API + SECTION + "/Delete/" + idTermin);
        }

        public static String Remove(String idTermin) {
            return (HOST + API + SECTION + "/Remove/" + idTermin);
        }

        public static String Update() {
            return (HOST + API + SECTION + "/Update");
        }

        public static String Insert() {
            return (HOST + API + SECTION + "/Insert");
        }

        public static String LoginAllStudentsToDate(int courseInfoId, String dateId, String fulltime, String combined) {
            return (HOST + API + SECTION + "/LoginAllStudentsToDate?courseInfoId=" + courseInfoId + "&dateId=" + dateId + "&fulltime=" + fulltime + "&combined=" + combined);
        }

        public static String LogoutFromDateImmediately(String studentId, String dateId) {
            return (HOST + API + SECTION + "/LogoutFromDateImmediately?studentId=" + studentId + "&dateId=" + dateId);
        }

        public static String GetStudentsQRCodes(String dateId, String skolaInfoId) {
            return (HOST + API + SECTION + "/GetStudentsQRCodes?dateId=" + dateId + "&skolaInfoId=" + skolaInfoId);
        }

        public static String GetStudentspdf(String dateId, String skolaInfoId) {
            return (HOST + API + SECTION + "/GetStudentspdf?dateId=" + dateId + "&skolaInfoId=" + skolaInfoId);
        }

        public static String GetStudents(String dateId, int skolaInfoId) {
            return (HOST + API + SECTION + "/GetStudents?dateId=" + dateId + "&skolaInfoId=" + skolaInfoId);
        }
    }

    public static class StudentSummary {
        private static final String SECTION = "/StudentSummary";

        public static String GetSummaryByGroupActivity(String groupActivityId) {
            return (HOST + API + SECTION + "/GetSummaryByGroupActivity?groupActivityId=" + groupActivityId);
        }

        public static String GetSummaryByGroupActivityAndStudent(String groupActivityId, String userId) {
            return (HOST + API + SECTION + "/GetSummaryByGroupActivityAndStudent?groupActivityId=" + groupActivityId + "&userId=" + userId);
        }

        public static String GetSummaryByCourse(int courseInfoId) {
            return (HOST + API + SECTION + "/GetSummaryByCourse?courseInfoId=" + courseInfoId);
        }

        public static String GetSchoolClassesByStudent(int studentId, int courseInfoId) {
            return (HOST + API + SECTION + "/GetSchoolClassesByStudent?studentId=" + studentId + "&courseInfoId=" + courseInfoId);
        }

        public static String GetSummaryBySchoolClassFromTest(int schoolClassId, int testId) {
            return (HOST + API + SECTION + "/GetSummaryBySchoolClassFromTest?schoolClassId=" + schoolClassId + "&testId=" + testId);
        }
    }

    public static class EvaluationTest {
        private static final String SECTION = "/EvaluationTest";

        public static String PrioritizeEvaluationTest(String evaluationTestId){
            return (HOST + API + SECTION + "/PrioritizeEvaluationTest?evaluationTestId=" + evaluationTestId);
        }
    }

    public static class GroupActivities {
        private static final String SECTION = "/GroupActivities";
        public static String updategroupactivity(){
            return (HOST + API + SECTION + "/updategroupactivity");
        }

        public static String getgroupactivitiesbycourse(int courseInfoId, int id, int roleID) {
            return (HOST + API + SECTION + "/getgroupactivitiesbycourse?courseInfoId=" + courseInfoId + "&userId=" + id + "&roleID=" + roleID);

        }

        public static String insertgroupactivities(int userId) {
            return (HOST + API + SECTION + "/insertgroupactivities?userId=" + userId);
        }

        public static String Remove(String idSkupinaAktivit) {
            return (HOST + API + SECTION + "/Remove/" + idSkupinaAktivit);
        }
    }

    public static class Activity {
        private static final String SECTION = "/Activity";

        public static String GetActivities(String groupActivityId, int id, int roleID) {
            return (HOST + API + SECTION + "/GetActivities?groupActivityId=" + groupActivityId + "&userId=" + id + "&roleID=" + roleID);
        }
        public static String Remove(String idActivity) {
            return (HOST + API + SECTION + "/Remove/" + idActivity);
        }

        public static String Insert(String groupActivityId, int userId) {
            return (HOST + API + SECTION + "/Insert?groupActivityId=" + groupActivityId + "&userId=" + userId);
        }

        public static String GetActivitySampleFile(Integer activityId) {

            return (HOST + API + SECTION + "/GetActivitySampleFile?activityId=" + activityId);
        }

        public static String GetActivityInfoFile(Integer activityId) {

            return (HOST + API + SECTION + "/GetActivityInfoFile?activityId=" + activityId);
        }

        public static String Update() {
            return (HOST + API + SECTION + "/Update");
        }
    }

    public static class Test {
        private static final String SECTION = "/Test";

        public static String GetTests(String groupActivitiesId, int id, int roleID) {
            return (HOST + API + SECTION + "/GetTests?groupActivitiesId=" + groupActivitiesId + "&userId=" + id + "&roleID=" + roleID);
        }

        public static String Remove(String idActivity) {
            return (HOST + API + SECTION + "/Remove/" + idActivity);
        }

        public static String Insert(String groupActivityId, int userId) {
            return (HOST + API + SECTION + "/Insert?groupActivityId=" + groupActivityId + "&userId=" + userId);
        }

        public static String Update() {
            return (HOST + API + SECTION + "/Update");
        }
    }

    public static class Step
    {
        private static final String SECTION = "/Step";

        public static String GetSteps(int categoryId)
        {
            return (HOST + API + SECTION + "/GetSteps?categoryId=" + categoryId);
        }

        public static String insert(int categoryId)
        {
            return (HOST + API + SECTION + "/Insert?categoryId=" + categoryId);
        }

        public static String update() {
            return (HOST + API + SECTION + "/Update");
        }

        public static String DeleteFull() {
            return (HOST + API + SECTION + "/DeleteFull");
        }
    }

    public static class Question
    {
        private static final String SECTION = "/Question";

        public static String GetQuestions(int categoryId, int inclusionId, int chapterId, boolean confirmed, int userID, boolean onlyOwn)
        {
            return (HOST + API + SECTION + "/getquestions?categoryId=" + categoryId + "&inclusionId=" + inclusionId + "&chapterId=" + chapterId + "&confirmed=" + confirmed + "&userId=" + userID + "&onlyOwn=" + onlyOwn);
        }

        public static String GetQuestionsAll(int categoryId, int inclusionId, int chapterId, boolean confirmed, boolean unconfirmed, int userID, boolean onlyOwn)
        {
            return (HOST + API + SECTION + "/getquestionsall?categoryId=" + categoryId + "&inclusionId=" + inclusionId + "&chapterId=" + chapterId + "&confirmed=" + confirmed +"&unconfirmed="+ unconfirmed + "&userId=" + userID + "&onlyOwn=" + onlyOwn);
        }

        public static String GetQuestionsAllFull(int categoryId, int inclusionId, int chapterId, boolean confirmed, boolean unconfirmed, int userID, boolean onlyOwn)
        {
            return (HOST + API + SECTION + "/getquestionsallfull?categoryId=" + categoryId + "&inclusionId=" + inclusionId + "&chapterId=" + chapterId + "&confirmed=" + confirmed +"&unconfirmed="+ unconfirmed + "&userId=" + userID + "&onlyOwn=" + onlyOwn);
        }

        public static String getquestiondifficulty(int questionId)
        {
            return (HOST + API + SECTION + "/getquestiondifficulty?questionId=" + questionId);
        }
        public static String insertquestion()
        {
            return (HOST + API + SECTION + "/insertquestion");
        }
        public static String Remove(int id)
        {
            return (HOST + API + SECTION + "/Remove/" + id);
        }
    }

    public static class Template {
        private static final String SECTION = "/Template";

        public static String GetTemplates(int courseInfoId, int userId, int roleID) {
            return (HOST + API + SECTION + "/GetTemplates?courseInfoId=" + courseInfoId + "&userId=" + userId + "&roleID=" + roleID);
        }


        public static String GetTemplatesWithBlocks(int courseInfoId, int userID, int roleID)
        {
            return (HOST + API + SECTION + "/GetTemplatesWithBlocks?courseInfoId=" + courseInfoId + "&userId=" + userID + "&roleId=" + roleID);
        }


        public static String Insert(int courseInfoId, int userID)
        {
            return (HOST + API + SECTION + "/Insert?courseInfoId=" + courseInfoId + "&userId=" + userID);
        }


        public static String Update()
        {
            return (HOST + API + SECTION + "/Update");
        }


        public static String Remove(int id)
        {
            return (HOST + API + SECTION + "/Remove/" + id);
        }
    }

    public static class Student {
        private static final String SECTION = "/Student";

        public static String GetStudentSummaryByIDTermin(String dateId, int skolaInfoId, String minPoints) {
            return (HOST + API + SECTION + "/GetStudentSummaryByIDTermin?dateId=" + dateId + "&skolaInfoId=" + skolaInfoId + "&minPoints=" + minPoints);
        }

        public static String GetStudentHistoryByIDTermin(String dateId) {
            return (HOST + API + SECTION + "/GetStudentHistoryByIDTermin?dateId=" + dateId);
        }
    }

    public static class Category {
        private static final String SECTION = "/Category";

        public static String getCategories(int chapterId) {
            return (HOST + API + SECTION + "/GetCategories?chapterId=" + chapterId);
        }

        public static String insert(int chapterId) {
            return (HOST + API + SECTION + "/Insert?chapterId=" + chapterId);
        }

        public static String update(int chapterId) {
            return (HOST + API + SECTION + "/Update?chapterId=" + chapterId);
        }

        public static String remove(int categoryId) {
            return (HOST + API + SECTION + "/Remove/" + categoryId);
        }
    }

    public static class Answer {
        private static final String SECTION = "/Answer";

        public static String Insert() {
            return (HOST + API + SECTION + "/Insert");
        }
    }


    public static class SchoolClass {
        private static final String SECTION = "/SchoolClass";

        public static String insertactivitytoschoolclasses(String groupActivityId) {
            return (HOST + API + SECTION + "/insertactivitytoschoolclasses?groupActivityId=" + groupActivityId);
        }

        public static String inserttesttoschoolclasses(String groupActivityId) {
            return (HOST + API + SECTION + "/inserttesttoschoolclasses?groupActivityId=" + groupActivityId);
        }

        public static String getgarantclasses(int courseInfoId) {
            return (HOST + API + SECTION + "/getgarantclasses?courseInfoId=" + courseInfoId);
        }

        public static String insertschoolclass(int courseInfoId) {
            return (HOST + API + SECTION + "/insertschoolclass?courseInfoId=" + courseInfoId);
        }

        public static String updateschoolclass() {
            return (HOST + API + SECTION + "/updateschoolclass");
        }

        public static String Remove(String id) {
            return (HOST + API + SECTION + "/Remove/" + id);
        }

        public static String getbytest(int testId) {
            return (HOST + API + SECTION + "/getbytest?testId=" + testId);
        }
    }

    public static class Datamining {
        private static final String SECTION = "/datamining";

        public static String Statistics(int user, Integer course, String year) {
            return (URLADDRESS + SECTION + "/statistics.aspx?user=" + user + "&course=" + course + "&year=" + year);
        }
    }

    public static class EmailMessage {
        private static final String SECTION = "/EmailMessage";

        public static String Messages() {
            return (HOST + API + SECTION + "/Messages");
        }

        public static String Remove(String id) {
            return (HOST + API + SECTION + "/Remove?messageId=" + id);
        }

        public static String Send() {
            return (HOST + API + SECTION + "/Send");
        }

        public static String SendToMultipleUsers() {
            return (HOST + API + SECTION + "/SendToMultipleUsers");
        }

        public static String SentToMultipleClasses() {
            return (HOST + API + SECTION + "/SentToMultipleClasses");
        }
    }

    public static class ExportResult {
        private static final String SECTION = "/ExportResult";

        public static String ExportResult(String testId, String dateId, int schoolInfoId) {
            return (HOST + API + SECTION + "/ExportResult?testId=" + testId + "&dateId=" + dateId + "&schoolInfoId=" + schoolInfoId);
        }
    }
}
