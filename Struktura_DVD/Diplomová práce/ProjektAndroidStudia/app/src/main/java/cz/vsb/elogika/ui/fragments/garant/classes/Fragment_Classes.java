package cz.vsb.elogika.ui.fragments.garant.classes;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cz.vsb.elogika.R;
import cz.vsb.elogika.common.view.SlidingTabLayout;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.ui.fragments.garant.emails.Fragment_email_message;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.Logging;
import cz.vsb.elogika.utils.TimeLogika;

public class Fragment_Classes extends Fragment {
    private View rootView;

    static Fragment_classes_list fragment_classes_list;
    static Fragment_classes_timetable fragment_classes_timetable;
    static Fragment_email_message send_message;


    private static LinearLayout nothinglayout;
    public static ArrayList<Map<String, String>> lesson_list;
    public static Map<String, JSONObject> dates_list;
    public static Map<String, JSONObject> combinedClasses_list;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.tabs_container, container, false);
        Actionbar.newCategory(R.string.list_of_classes_and_students, this);
        Logging.logAccess("/Pages/Garant/Tridy.aspx");

        nothinglayout = (LinearLayout) rootView.findViewById(R.id.nothing_to_show);
        nothinglayout.setVisibility(View.GONE);

        lesson_list = new ArrayList<>();
        dates_list = new HashMap<>();
        combinedClasses_list = new HashMap<>();

        ViewPager mViewPager = (ViewPager) rootView.findViewById(R.id.viewpager);
        mViewPager.setAdapter(new FragmentStatePagerAdapter(getFragmentManager()) {
                                  String[] tabNames = new String[]{
                                          getString(R.string.class_management),
                                          getString(R.string.table_version_of_classes),
                                          getString(R.string.send_message)};

                                  @Override
                                  public int getCount() {
                                      return tabNames.length;
                                  }

                                  @Override
                                  public CharSequence getPageTitle(int position) {
                                      return tabNames[position];
                                  }


                                  @Override
                                  public Fragment getItem(int position) {

                                      switch (position) {
                                          case 0:
                                              fragment_classes_list = new Fragment_classes_list() {};
                                              return fragment_classes_list;
                                          case 1:
                                              fragment_classes_timetable = new Fragment_classes_timetable() {};
                                              return fragment_classes_timetable;
                                          case 2:
                                              Bundle bundle = new Bundle();
                                              bundle.putBoolean("course", true);
                                              send_message = new Fragment_email_message() {};
                                              send_message.setArguments(bundle);
                                              send_message.set_course();
                                              return send_message;
                                          default:
                                              return null;
                                      }
                                  }
                              }


        );
        SlidingTabLayout mSlidingTabLayout = (SlidingTabLayout) rootView.findViewById(R.id.sliding_tabs);
        mSlidingTabLayout.setViewPager(mViewPager);

        return rootView;
    }




    public static void update(final RecyclerView.Adapter adapter, final SwipeRefreshLayout swipeLayout) {
        swipeLayout.post(new Runnable() {
            @Override
            public void run() {
                nothinglayout.setVisibility(View.GONE);
                swipeLayout.setRefreshing(true);
                lesson_list.clear();
                adapter.notifyDataSetChanged();
                data_into_leasson_list(adapter, swipeLayout);
            }
        });
    }

    private static void data_into_leasson_list(final RecyclerView.Adapter adapter, final SwipeRefreshLayout swipeLayout) {
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                if (response.length() > 0)
                {
                    for (int j = 0; j < response.length(); j++)
                    {
                        lesson_list.add(put_data(
                                response.getJSONObject(j).getString("IDTrida"),
                                response.getJSONObject(j).getString("IDTutor"),
                                response.getJSONObject(j).getString("Nazev"),
                                response.getJSONObject(j).getString("ZkratkaPredmet"),
                                response.getJSONObject(j).getString("Ucebna"),
                                response.getJSONObject(j).getString("Den"),
                                response.getJSONObject(j).getString("HodinaOD"),
                                response.getJSONObject(j).getString("HodinaDO"),
                                response.getJSONObject(j).getString("Tyden"),
                                response.getJSONObject(j).getString("FormaStudia"),
                                response.getJSONObject(j).getString("Typ"),
                                response.getJSONObject(j).getString("Omezeni"),
                                response.getJSONObject(j).getString("Smazana"),
                                TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("Datum")),
                                response.getJSONObject(j).getJSONArray("Dates"),
                                response.getJSONObject(j).getJSONArray("CombinedClasses"),
                                response.getJSONObject(j).getString("TutorName"),
                                response.getJSONObject(j).getString("TutorSurname")));


                        //nevim jeste presne na co se dates pouziva...
                        /*
                        if(dates.length() != 0){
                            for(int i = 0; i < dates.length(); i++){
                                try {
                                    dates_list.put(idTrida, dates.getJSONObject(i));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        */
                        JSONArray combinedClasses = response.getJSONObject(j).getJSONArray("CombinedClasses");
                        if(combinedClasses.length() != 0){
                            //jeste nevim jestli budu pouzivat, ale ukladam podtridy jeste zvlast - ale ne pod IDTrida
                            //combinedClasses_list.put(response.getJSONObject(j).getString("IDTrida"), combinedClasses.getJSONObject(i));
                            //radek pro hlavicku
                            //HashMap<String, String> item_head = new HashMap<String, String>();
                            lesson_list.add(null);
                            for(int i = 0; i < combinedClasses.length(); i++){
                                try {
                                    lesson_list.add(put_data(
                                            "", //idTrida = "" pro seznam trid kombinovanych studentu
                                            combinedClasses.getJSONObject(i).getString("IDTutor"),
                                            combinedClasses.getJSONObject(i).getString("Nazev"),
                                            combinedClasses.getJSONObject(i).getString("ZkratkaPredmet"),
                                            combinedClasses.getJSONObject(i).getString("Ucebna"),
                                            combinedClasses.getJSONObject(i).getString("Den"),
                                            combinedClasses.getJSONObject(i).getString("HodinaOD"),
                                            combinedClasses.getJSONObject(i).getString("HodinaDO"),
                                            combinedClasses.getJSONObject(i).getString("Tyden"),
                                            combinedClasses.getJSONObject(i).getString("FormaStudia"),
                                            combinedClasses.getJSONObject(i).getString("Typ"),
                                            combinedClasses.getJSONObject(i).getString("Omezeni"),
                                            combinedClasses.getJSONObject(i).getString("Smazana"),
                                            TimeLogika.getFormatedDate(combinedClasses.getJSONObject(i).getString("Datum")),
                                            combinedClasses.getJSONObject(i).getJSONArray("Dates"),
                                            combinedClasses.getJSONObject(i).getJSONArray("CombinedClasses"),
                                            response.getJSONObject(j).getString("TutorName"),
                                            response.getJSONObject(j).getString("TutorSurname")));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                }
                else{
                    Log.d("Chyba", "classes data_into_leasson_list");
                    nothinglayout.setVisibility(View.VISIBLE);
                }
                swipeLayout.setRefreshing(false);
                adapter.notifyDataSetChanged();
                //nastaveni rozvrhu zde, protoze zde jsou data jiz k dispozici
                fragment_classes_timetable.setTimetable();
            }
        };
        Request.get(URL.SchoolClass.getgarantclasses(User.courseInfoId), arrayResponse);
    }

    private static Map<String, String> put_data(String idTrida, String idTutor, String nazev, String zkratkaPredmet, String ucebna,
                                                String den, String hodinaOD, String hodinaDO, String tyden, String formaStudia, String typ,
                                                String omezeni, String smazana, String datum, JSONArray dates, JSONArray combinedClasses, String tutorName, String tutorSurname) {
        HashMap<String, String> item = new HashMap<String, String>();

        item.put("idTrida",idTrida);
        item.put("idTutor",idTutor);
        item.put("nazev",nazev);
        item.put("zkratkaPredmet",zkratkaPredmet);
        item.put("ucebna",ucebna);
        item.put("den",den);
        item.put("hodinaOD",hodinaOD);
        item.put("hodinaDO", hodinaDO);
        //pridano poradiDO, poradiDO a poradiCelkem pro potreby aplikace
        counting_for_timetable(item);
        item.put("tyden",tyden);
        item.put("formaStudia",formaStudia);
        item.put("typ",typ);
        item.put("omezeni",omezeni);
        item.put("smazana",smazana);
        item.put("datum",datum);
        if(combinedClasses.length() > 0){
            item.put("combinedClasses","true");
        }
        else{
            item.put("combinedClasses","false");
        }
        item.put("tutorName",tutorName);
        item.put("tutorSurname",tutorSurname);

        return item;
    }

    private static void counting_for_timetable(HashMap<String, String> item) {
        int poradiOD = -1;
        int poradiDO = -1;
        int poradiCelkem = -1;
        String hodinaOD = item.get("hodinaOD");
        String hodinaDO = item.get("hodinaDO");


        ArrayList<String> leassonsFrom = new ArrayList<>();
        ArrayList<String> leassonsTo = new ArrayList<>();
        //7:00 je zde kvuli kombinovanym studentum
        leassonsFrom.add("7:00"); //0
        leassonsTo.add("8:00");
        leassonsFrom.add("8:00"); //1
        leassonsTo.add("8:45");
        leassonsFrom.add("9:00"); //2
        leassonsTo.add("9:45");
        leassonsFrom.add("9:45"); //3
        leassonsTo.add("10:30");
        leassonsFrom.add("10:45"); //4
        leassonsTo.add("11:30");
        leassonsFrom.add("11:30"); //5
        leassonsTo.add("12:15");
        leassonsFrom.add("12:30"); //6
        leassonsTo.add("13:15");
        leassonsFrom.add("13:15"); //7
        leassonsTo.add("14:00");
        leassonsFrom.add("14:15"); //8
        leassonsTo.add("15:00");
        leassonsFrom.add("15:00"); //9
        leassonsTo.add("15:45");
        leassonsFrom.add("16:00"); //10
        leassonsTo.add("16:45");
        leassonsFrom.add("16:45"); //11
        leassonsTo.add("17:30");
        leassonsFrom.add("17:45"); //12
        leassonsTo.add("18:30");
        leassonsFrom.add("18:30"); //13
        leassonsTo.add("19:15");

        int i = 0;
        for(i = 0; i < leassonsFrom.size(); i++){
            if(TimeLogika.compareTime( leassonsFrom.get(i), hodinaOD) <= 0){
                if(TimeLogika.compareTime(leassonsTo.get(i), hodinaOD) > 0){
                    poradiOD = i;
                    break;
                }
            }
        }
        for(; i < leassonsTo.size(); i++){
            if(TimeLogika.compareTime(leassonsFrom.get(i), hodinaDO) < 0){
                if(TimeLogika.compareTime(leassonsTo.get(i), hodinaDO) >= 0) {
                    poradiDO = i;
                    break;
                }
            }
        }

        if(poradiOD != -1 && poradiDO != -1) {
            poradiCelkem = poradiDO - poradiOD + 1;
        }
        item.put("poradiOD", String.valueOf(poradiOD));
        item.put("poradiDO", String.valueOf(poradiDO));
        //v pripade chybnych dat bude poradiDO = -1
        item.put("poradiCelkem", String.valueOf(poradiCelkem));
    }
}
