package cz.vsb.elogika.ui.fragments.takeAtest;

import android.app.Fragment;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.android.volley.VolleyError;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.sqliteDB.MyDB;
import cz.vsb.elogika.ui.BaseActivity;
import cz.vsb.elogika.utils.Logging;
import cz.vsb.elogika.utils.TimeLogika;

/**
 * Fragment pro zobrazení vypracovaných offline testů
 */
public abstract class Fragment_Offline_Done extends Fragment {

    private ArrayList<Map<String, String>> itemList = new ArrayList<Map<String, String>>();
    private SimpleAdapter adapter;

    /**
     * interface, který je aktivován když se zde změní data
     */
    public abstract void onDataChanged();
    View nothinglayout;
    Button sendtests;

    /**
     * Inicializace UI
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Logging.logAccess("OfflineTest", "PerformedTests");
        View rootView = inflater.inflate(R.layout.fragment_tests_settings, container, false);
        nothinglayout = rootView.findViewById(R.id.take_a_test_done_nothing_to_show);
        if(User.isOnline) rootView.findViewById(R.id.linear_info).setVisibility(View.GONE);
        sendtests  = (Button) rootView.findViewById(R.id.buttonsendalldoneofflinetests);
        sendtests.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                taking_a_test_finishDialog = new Taking_a_test_FinishDialog(getActivity()) {
                    @Override
                    public void onRetryClicked() {
                        sendDoneTests();
                    }
                };
                sendDoneTests();
                getAllDoneTestNames();
                onDataChanged();
            }
        });
        ListView list = (ListView) rootView.findViewById(R.id.doneofflinetests);
        String[] from = {"name","date"};
        int[] to = {R.id.offline_done_name, R.id.offline_done_date};

        this.adapter = new SimpleAdapter(getActivity(), this.itemList, R.layout.fragment_offline_done_item, from, to) {
            @Override
            public View getView(final int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                return view;
            }
        };
        list.setAdapter(this.adapter);
        return rootView;
    }


    /**
     * Metoda ktrá zažádá server aby poslal data
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getAllDoneTestNames();
    }

    /**
     * Metoda, která získá názvy všech uložených offline testů
     */
    public void getAllDoneTestNames() {
        nothinglayout.setVisibility(View.VISIBLE);
        sendtests.setEnabled(false);
        this.itemList.clear();
        this.adapter.notifyDataSetChanged();
        try {
            Cursor cursor = BaseActivity.getDB().selectOfflineDoneNames(); //TODO null pointer exception někdy
            while (cursor.moveToNext()) {
                addItem(cursor.getString(cursor.getColumnIndex(MyDB.UNSAVED_NAME)),
                        cursor.getString(cursor.getColumnIndex(MyDB.TEST_DATE_SEND_TO)));
                nothinglayout.setVisibility(View.GONE);
                sendtests.setEnabled(true);
            }
            cursor.close();
        } catch (NullPointerException e) {
            e.toString();
        }
    }

    /**
     * Přidá záznam do seznamu
     *
     * @param name název testu
     * @param date Datum do kdy se musí test odevzdat
     */
    private void addItem(String name, String date) {
        HashMap<String, String> item = new HashMap<String, String>();
        item.put("name", name);
        item.put("date", getActivity().getString(R.string.send_until) +": "+ TimeLogika.getFormatedDateTime(date));
        this.itemList.add(item);
        this.adapter.notifyDataSetChanged();
    }


    /**
     * Metoda, která načte offline testy z databáze
     */
    private void sendDoneTests() {
        Cursor cursor = BaseActivity.getDB().selectAllDoneTests();
        checkCounter = 4* cursor.getCount();
        while (cursor.moveToNext()) {
            try {
                saveGenerateTest(
                        new JSONObject(cursor.getString(cursor.getColumnIndex(MyDB.UNSAVED_SAVE))),
                        new JSONObject(cursor.getString(cursor.getColumnIndex(MyDB.UNSAVED_OTAZKA))),
                        new JSONObject(cursor.getString(cursor.getColumnIndex(MyDB.UNSAVED_ODPOVED))),
                        new JSONObject(cursor.getString(cursor.getColumnIndex(MyDB.UNSAVED_LOG)))
                );
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        cursor.close();
    }


    Taking_a_test_FinishDialog taking_a_test_finishDialog;
    /**
     * Metoda, která uloží offline testy
     *
     * @param parameters
     * @param otazka
     * @param odpoved
     * @param log
     */
    private void saveGenerateTest(JSONObject parameters, final JSONObject otazka, final JSONObject odpoved, final JSONObject log) {
        Response response = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException {
                Log.d("savegeneratetest response", response.toString());
                InsertStatOtazkaUzivatel(response.getInt("Result"), otazka);
                InsertStatOdpovedUzivatel(response.getInt("Result"), odpoved);
                addloglist(response.getInt("Result"), log);
                checkResponses();
            }

            @Override
            public void onError(VolleyError error) {
                taking_a_test_finishDialog.setServerError();
            }
        };
        Request.post(URL.GeneratedTest.savegenerateTestkeyvalue(), parameters, response);
    }

    /**
     * Vložení statistiky pro otázky na server
     *
     * @param IDVygenerovanyTestUzivatel id vygenerovaného testu
     * @param parameters Json objekt se statistikama
     */
    private void InsertStatOtazkaUzivatel(int IDVygenerovanyTestUzivatel, JSONObject parameters) {
        Response response = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException {
                Log.d("InsertStatOtazkaUzivatel uspešně uloženo na server", response.getString("Success"));
                checkResponses();
            }
            @Override
            public void onError(VolleyError error) {
                taking_a_test_finishDialog.setServerError();
            }
        };

        try {
            for(int i = 0; i<parameters.getJSONArray("QuestionStats").length();i++)
                parameters.getJSONArray("QuestionStats").getJSONObject(i).put("IDVygenerovanyTestUzivatel", IDVygenerovanyTestUzivatel);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("elogika odesilam InsertStatOtazkaUzivatel: " , parameters.toString());
        Request.post(URL.OnlineStatistics.InsertStatOtazkaUzivatelList(), parameters, response);
    }

    /**
     * Vložení statistiky pro odpovědi na server
     *
     * @param IDVygenerovanyTestUzivatel id vygenerovaného testu
     * @param parameters Json objekt se statistikama
     */
    private void InsertStatOdpovedUzivatel(int IDVygenerovanyTestUzivatel, JSONObject parameters) {
        Response response = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException {
                Log.d("InsertStatOdpovedUzivatel", response.toString());
                checkResponses();
            }
            @Override
            public void onError(VolleyError error) {
                taking_a_test_finishDialog.setServerError();
            }
        };
        try {
            for (int i = 0; i < parameters.getJSONArray("AnswerStats").length(); i++)
                parameters.getJSONArray("AnswerStats").getJSONObject(i).put("IDVygenerovanyTestUzivatel", IDVygenerovanyTestUzivatel);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("elogika odesilam InsertStatOdpovedUzivatel: ", parameters.toString());
        Request.post(URL.OnlineStatistics.InsertStatOdpovedUzivatelList(), parameters, response);
    }

    /**
     * Vložení logu z testu na server
     *
     * @param userGeneratedTestId id vygenerovaného testu
     * @param parameters Json objekt se statistikama
     */
    private void addloglist(int userGeneratedTestId, JSONObject parameters) {
        Response response = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException {
                Log.d("addloglist response", response.toString());
                checkResponses();
            }
            @Override
            public void onError(VolleyError error) {
                taking_a_test_finishDialog.setServerError();
            }
        };

        try {
            for (int i = 0; i < parameters.getJSONArray("TestLogs").length(); i++)
                parameters.getJSONArray("TestLogs").getJSONObject(i).put("UserGeneratedTestId", userGeneratedTestId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.w("addloglist request " , parameters.toString());
        Request.post(URL.CourseConditions.addloglist(), parameters, response);
    }

    private static int checkCounter;
    private void checkResponses() {
        checkCounter--;
        if (checkCounter == 0){
            SnackbarManager.show(
                    Snackbar.with(getActivity())
                            .text(getString(R.string.all_tests_successfully_uploaded_to_server))
            );
            BaseActivity.getDB().deleteALLDoneTests();
            itemList.clear();
            adapter.notifyDataSetChanged();
            sendtests.setEnabled(false);
            taking_a_test_finishDialog.cancel();
        }
    }
}