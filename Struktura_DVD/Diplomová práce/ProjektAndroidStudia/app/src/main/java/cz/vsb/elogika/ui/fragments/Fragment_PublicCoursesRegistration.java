package cz.vsb.elogika.ui.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Base64;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.Hash;
import cz.vsb.elogika.utils.Image;
import cz.vsb.elogika.utils.Logging;

public class Fragment_PublicCoursesRegistration extends Fragment
{
    private String avatarInString;

    private EditText name;
    private EditText surname;
    private EditText birthSurname;
    private EditText email;
    private EditText language;
    private EditText street;
    private EditText landRegistryNumber;
    private EditText postcode;
    private EditText city;
    private EditText degreeInFrontOfName;
    private EditText degreeBehindName;
    private ImageView avatar;
    private EditText password;
    private EditText passwordConfirmation;
    private Button roundButton;



    public Fragment_PublicCoursesRegistration()
    {
        this.avatarInString = new String();
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        Actionbar.addSubcategory(R.string.register_to_public_course, this);
        Logging.logAccess("/Pages/Anonymous/RegistraceForm.aspx");

        View rootView = inflater.inflate(R.layout.fragment_public_courses_registration, container, false);
        final int courseInfoId = getArguments().getInt("courseInfoId", 0);


        final TextView labelSchoolLogin = (TextView) rootView.findViewById(R.id.label_user_school_login);
        final TextView schoolLogin = (EditText) rootView.findViewById(R.id.user_school_login);
        final CheckBox iAmStudent = (CheckBox) rootView.findViewById(R.id.i_am_student);
        iAmStudent.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b)
            {
                if (b)
                {
                    labelSchoolLogin.setVisibility(View.VISIBLE);
                    schoolLogin.setVisibility(View.VISIBLE);
                    schoolLogin.requestFocus();
                }
                else
                {
                    labelSchoolLogin.setVisibility(View.GONE);
                    schoolLogin.setVisibility(View.GONE);
                }
            }
        });

        this.name = (EditText) rootView.findViewById(R.id.user_name);
        this.surname = (EditText) rootView.findViewById(R.id.user_surname);
        this.birthSurname = (EditText) rootView.findViewById(R.id.user_birth_surname);
        this.email = (EditText) rootView.findViewById(R.id.user_email);
        this.language = (EditText) rootView.findViewById(R.id.user_language);
        this.street = (EditText) rootView.findViewById(R.id.user_street);
        this.landRegistryNumber = (EditText) rootView.findViewById(R.id.user_land_registry_number);
        this.postcode = (EditText) rootView.findViewById(R.id.user_postcode);
        this.city = (EditText) rootView.findViewById(R.id.user_city);
        this.degreeInFrontOfName = (EditText) rootView.findViewById(R.id.user_degree_in_front_of_name);
        this.degreeBehindName = (EditText) rootView.findViewById(R.id.user_degree_behind_name);
        this.avatar = (ImageView) rootView.findViewById(R.id.avatar);
        this.password = (EditText) rootView.findViewById(R.id.user_password);
        this.passwordConfirmation = (EditText) rootView.findViewById(R.id.user_password_comfirmation);

        rootView.findViewById(R.id.select_avatar).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                startActivityForResult(intent, 1);
            }
        });

        this.roundButton = (Button) rootView.findViewById(R.id.roundButton);
        this.roundButton.setText("✔");
        this.roundButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                roundButton.setEnabled(false);
                boolean everythingOK = true;
                if (email.length() == 0)
                {
                    SnackbarManager.show(Snackbar.with(getActivity()).text(R.string.error_email_empty));
                    everythingOK = false;
                }
                if (name.length() == 0)
                {
                    SnackbarManager.show(Snackbar.with(getActivity()).text(R.string.error_name_empty));
                    everythingOK = false;
                }
                if (surname.length() == 0)
                {
                    SnackbarManager.show(Snackbar.with(getActivity()).text(R.string.error_surname_empty));
                    everythingOK = false;
                }
                if (!password.getText().toString().contentEquals(passwordConfirmation.getText().toString()))
                {
                    SnackbarManager.show(Snackbar.with(getActivity()).text(R.string.error_passwords_mismatch));
                    everythingOK = false;
                }
                if (!everythingOK)
                {
                    roundButton.setEnabled(true);
                    return;
                }

                if (iAmStudent.isChecked()) register(courseInfoId, schoolLogin.getText().toString());
                else // pokud login nemáme, musíme ho zjistit
                {
                    Response response = new Response()
                    {
                        @Override
                        public void onResponse(JSONObject response) throws JSONException
                        {
                            register(courseInfoId, response.getString("Login"));
                        }

                        @Override
                        public void onError(VolleyError error)
                        {
                            super.onError(error);
                            roundButton.setEnabled(true);
                        }

                        @Override
                        public void onException(JSONException exception)
                        {
                            super.onException(exception);
                            roundButton.setEnabled(true);
                        }
                    };
                    Request.get(URL.User.GenerateLogin(surname.getText().toString()), response);
                }
            }
        });

        return rootView;
    }


    /**
     * Vytvoří uživateli účet.
     * @param courseInfoId
     * @param login
     */
    private void register(int courseInfoId, final String login)
    {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                if (response.getBoolean("Success"))
                {
                    SnackbarManager.show(Snackbar.with(getActivity()).type(SnackbarType.MULTI_LINE).text(R.string.account_created));
                    SnackbarManager.show(Snackbar.with(getActivity()).type(SnackbarType.MULTI_LINE).text(R.string.logged_to_public_course));
                    Actionbar.jumpBack();
                }
                else SnackbarManager.show(Snackbar.with(getActivity()).text(R.string.registration_unsuccessful));
            }

            @Override
            public void onError(VolleyError error)
            {
                super.onError(error);
                roundButton.setEnabled(true);
            }

            @Override
            public void onException(JSONException exception)
            {
                super.onException(exception);
                roundButton.setEnabled(true);
            }
        };
        JSONObject parameters = new JSONObject();
        try
        {
            parameters.put("Jmeno", this.name.getText());
            parameters.put("Prijmeni", this.surname.getText());
            parameters.put("PrijmeniRodne", this.birthSurname.getText());
            parameters.put("Email", this.email.getText());
            parameters.put("Jazyk", this.language.getText());
            parameters.put("Ulice", this.street.getText());
            parameters.put("CisloPop", this.landRegistryNumber.getText());
            parameters.put("PSC", this.postcode.getText());
            parameters.put("Mesto", this.city.getText());
            parameters.put("TitulPred", this.degreeInFrontOfName.getText());
            parameters.put("TitulZa", this.degreeBehindName.getText());
            parameters.put("Fotka", this.avatarInString);
            String salt = Hash.getSalt();
            parameters.put("Salt", salt);
            parameters.put("Heslo", Hash.getHashedPassword(this.password.getText().toString(), salt));
        }
        catch (JSONException e) {}
        Request.post(URL.User.RegistrationUser(courseInfoId, login), parameters, response);
    }



    /**
     * Použito pro načtení fotky z galerie.
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == 1 && resultCode == Activity.RESULT_OK)
        {
            try
            {
                InputStream inputStream = getActivity().getContentResolver().openInputStream(data.getData());
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                inputStream.close();

                byte[] bytes = Image.scale(bitmap, 500000);
                bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);

                Display display = getActivity().getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int width = size.x - (32 * (int) getActivity().getResources().getDisplayMetrics().density);
                int height = (size.x - (32 * (int) getActivity().getResources().getDisplayMetrics().density)) * bitmap.getHeight() / bitmap.getWidth();
                if (bitmap.getWidth() < width)
                {
                    width = bitmap.getWidth();
                    height = bitmap.getHeight();
                }
                this.avatar.setImageBitmap(Bitmap.createScaledBitmap(bitmap, width, height, false));
                this.avatarInString = Base64.encodeToString(bytes, Base64.DEFAULT);
            }
            catch (IOException e) {}
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
