package cz.vsb.elogika.ui.fragments.Schedule;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.vsb.elogika.R;
import cz.vsb.elogika.common.view.SlidingTabLayout;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.dataTypes.Lesson;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.Logging;
import cz.vsb.elogika.utils.TimeLogika;

public class Fragment_Schedule extends Fragment
{
    private ArrayList<Lesson> lessons[];

    private ViewPager pager;
    private SlidingTabLayout slidingTabLayout;



    public Fragment_Schedule()
    {
        this.lessons = new ArrayList[6];
        for (int j = 0; j < 6; j++) this.lessons[j] = new ArrayList();
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        Actionbar.newCategory(R.string.schedule, this, new int[] {R.string.schedule_registration, R.string.create_own_lesson}, new Actionbar.OnMenuStringResourceClickListener()
        {
            @Override
            public boolean onMenuStringResourceClick(int stringResource, int position)
            {
                switch(stringResource)
                {
                    case R.string.schedule_registration:
                        getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, new Fragment_ScheduleRegistration()).commit();
                        return true;

                    case R.string.create_own_lesson:
                        getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, new Fragment_ScheduleLessonCreationAndModification()).commit();
                        return true;

                    default:
                        return false;
                }
            }
        });

        View rootView = inflater.inflate(R.layout.fragment_schedule, container, false);

        this.pager = (ViewPager) rootView.findViewById(R.id.pager);
        this.pager.setAdapter(new FragmentStatePagerAdapter(getFragmentManager())
        {
            int[] tabNames = {R.string.monday, R.string.tuesday, R.string.wednesday, R.string.thursday, R.string.friday, R.string.saturday};

            @Override
            public Fragment getItem(int position)
            {
                Fragment_ScheduleDay fragment = new Fragment_ScheduleDay();
                fragment.lessons = lessons[position];
                return fragment;
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return getResources().getString(tabNames[position]);
            }

            @Override
            public int getCount()
            {
                return tabNames.length;
            }
        });
        this.slidingTabLayout = (SlidingTabLayout) rootView.findViewById(R.id.sliding_tabs);
        this.slidingTabLayout.setViewPager(this.pager);

        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                loadSchedule(response.getInt("IDSemestr"));
            }
        };
        Request.get(URL.Semester.SemestersByCourseInfo(User.courseInfoId), response);

        Logging.logAccess("/Pages/Student/Rozvrh.aspx");

        return rootView;
    }



    /**
     * Načte všechny hodiny a zobrazí je v recyclerView.
     * @param semesterID
     */
    private void loadSchedule(int semesterID)
    {
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                getActivity().findViewById(R.id.loading_progress).setVisibility(View.GONE);

                JSONObject o;
                Lesson lesson;
                int day;

                for (int j = 0; j < response.length(); j++)
                {
                    o = response.getJSONObject(j);
                    lesson = new Lesson();
                    lesson.timetableID = o.getInt("IDRozvrh");
                    lesson.name = o.getString("Predmet");
                    lesson.shortcut = o.getString("Zkratka");
                    lesson.type = o.getInt("Typ");
                    lesson.from = o.getString("HodinaOD");
                    lesson.to = o.getString("HodinaDO");
                    lesson.room = o.getString("Ucebna");
                    lesson.week = o.getString("Tyden");
                    lesson.date = (o.isNull("Datum") ? null : o.getString("Datum"));
                    lesson.teacherDegreeBeforeName = o.getString("TitulPred");
                    lesson.teacherName = o.getString("Jmeno");
                    lesson.teacherSurname = o.getString("Prijmeni");
                    lesson.teacherDegreeAfterName = o.getString("TitulZa");
                    if (o.getString("Den").compareTo("Pondělí") == 0) lesson.day = 0;
                    else if (o.getString("Den").compareTo("Úterý") == 0) lesson.day = 1;
                    else if (o.getString("Den").compareTo("Středa") == 0) lesson.day = 2;
                    else if (o.getString("Den").compareTo("Čtvrtek") == 0) lesson.day = 3;
                    else if (o.getString("Den").compareTo("Pátek") == 0) lesson.day = 4;
                    else if (o.getString("Den").compareTo("Sobota") == 0) lesson.day = 5;
                    else{
                        lesson.day = 6;
                        Log.d("Fragment_Schedule", "Wrong day: " + o.getString("Den"));
                    }

                    if (lesson.date == null)
                    {
                        if (lesson.day <= 5) lessons[lesson.day].add(lesson);
                    }
                    else
                    {
                        day = TimeLogika.getDayInWeek(lesson.date);
                        if (day != -1 && day <= 5) lessons[day].add(lesson);
                    }
                }

                // Zobrazení prázdného layoutu
                int size = 0;
                for (int j = 0; j < 5; j++) size += lessons[j].size();
                if (size == 0)
                {
                    getActivity().findViewById(R.id.nothing_to_show).setVisibility(View.VISIBLE);
                }
                else
                {
                    pager.getAdapter().notifyDataSetChanged();
                    pager.setVisibility(View.VISIBLE);
                    slidingTabLayout.setVisibility(View.VISIBLE);
                }
            }
        };
        Request.get(URL.Timetable.TimetableGet(semesterID, User.id), arrayResponse);
    }
}
