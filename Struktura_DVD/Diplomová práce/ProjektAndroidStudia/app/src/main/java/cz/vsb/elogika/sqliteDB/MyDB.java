package cz.vsb.elogika.sqliteDB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.AES;

/**
 * Metoda pro rozhraní aplikace s databází
 */
public class MyDB{

    private MyDatabaseHelper dbHelper;

    private SQLiteDatabase database;

    protected static final String TEST_TABLE_NAME = "testtable";
    public static final String TEST_ID = "testid";
    public static final String TEST_USER_ID = "userid";
    public static final String TEST_GENERATED_ID = "generatedtestid";
    public static final String TEST_NAME = "name";
    public static final String TEST_ACTIVITIESGROUP = "activitiesGroup";
    public static final String TEST_TIME_FROM = "timefrom";
    public static final String TEST_TIME_TO = "timeto";
    public static final String TEST_DATE_SEND_TO = "datesendto";
    public static final String TEST_MINPOINTS = "minpoints";
    public static final String TEST_MAXPOINTS = "maxpoints";
    public static final String TEST_MANDATORY = "mandatory";
    public static final String TEST_INFO = "testinfo";
    public static final String SCHOOL_INFO_ID = "schoolinfoid";
    public static final String TEST_TIME_MINUTES = "timeminutes";

    protected static final String QUESTION_TABLE_NAME = "questiontable";
    public static final String QUESTION_ID = "questionid";
    public static final String QUESTION_JSON = "questionjson";

    protected static final String OPTION_TABLE_NAME = "optiontable";
    public static final String OPTION_ID = "optionid";
    public static final String OPTION_JSON = "optionjson";


    protected static final String UNSAVED_TABLE_NAME = "unsavedresult";
    public static final String UNSAVED_SAVE = "save";
    public static final String UNSAVED_NAME = "name";
    public static final String UNSAVED_OTAZKA = "statotazka";
    public static final String UNSAVED_ODPOVED = "statodpoved";
    public static final String UNSAVED_LOG = "log";

    /**
     * Konstruktor pro Databázové rozhraní
     * @param context kontext aplikace získaný z aktivity pomocí getContext()
     */
    public MyDB(Context context){
        dbHelper = new MyDatabaseHelper(context);
        database = dbHelper.getWritableDatabase();
    }


    /**
     * Metoda vyprázdnění databáze
     */
    public  void eraseDB()
    {
        dbHelper.onUpgrade(database, 1,2);
    }

    /**
     * vloží stažený test do databáze s parametry staženými ze serveru
     *
     * @param generatedtestid
     * @param testid
     * @param timeMinutes
     * @param name
     * @param activitiesgroup
     * @param timefrom
     * @param timeto
     * @param DateSendTo
     * @param minpoints
     * @param maxpoints
     * @param mandatory
     * @param testinfo
     * @return
     */
    public long insertTest(int generatedtestid,int testid,int timeMinutes, String name,String activitiesgroup, String timefrom, String timeto, String DateSendTo,String minpoints,String maxpoints, boolean mandatory, JSONArray testinfo){
        ContentValues values = new ContentValues();
        values.put(TEST_GENERATED_ID,generatedtestid);
        values.put(TEST_USER_ID,User.id);
        values.put(TEST_ID, testid);
        values.put(TEST_TIME_MINUTES, timeMinutes);
        values.put(TEST_NAME, name);
        values.put(TEST_ACTIVITIESGROUP, activitiesgroup);
        values.put(TEST_TIME_FROM, timefrom);
        values.put(TEST_TIME_TO, timeto);
        values.put(TEST_DATE_SEND_TO, DateSendTo);
        values.put(TEST_MINPOINTS, minpoints);
        values.put(TEST_MAXPOINTS, maxpoints);
        values.put(TEST_MANDATORY, mandatory);
        values.put(TEST_INFO, AES.zasifrovat(testinfo.toString()));
        values.put(SCHOOL_INFO_ID, User.schoolInfoID);

        //database.execSQL("INSERT INTO question(questionid,testid,questionjson) VALUES (1,2,\"abc\")" );
        Log.d("elogika databaze", "proběhl insert testid " + testid);
        return database.insert(TEST_TABLE_NAME, null, values);
    }

    /**
     * @return vrací cursor se všemi parametry všech testů
     */
    public Cursor selectTests() {
        String where = TEST_USER_ID+"="+ User.id;
        String[] cols = new String[] {TEST_GENERATED_ID, TEST_ID, TEST_NAME,TEST_ACTIVITIESGROUP,TEST_TIME_FROM,TEST_TIME_TO,TEST_DATE_SEND_TO,TEST_MINPOINTS,TEST_MAXPOINTS,TEST_MANDATORY};
        Cursor mCursor = database.query(true, TEST_TABLE_NAME,cols,where
                , null, null, null, null, null);
        return mCursor; // iterate to get each value.
    }

    /**
     * Vyzvedne data testu z databáze
     * @param generatedtestid zvolená test
     * @return data testu
     */
    public Cursor selectTest(int generatedtestid) {

        Log.d("elogika","hledam test generatedid: "+ generatedtestid);
       // String where = TEST_GENERATED_ID+"="+generatetestid;
        String where = "generatedtestid="+generatedtestid;
        // String[] cols = new String[] {TEST_GENERATED_ID, TEST_ID, TEST_NAME,TEST_ACTIVITIESGROUP,TEST_TIME_FROM,TEST_TIME_TO,TEST_MINPOINTS,TEST_MAXPOINTS,TEST_MANDATORY, TEST_INFO};
        Cursor mCursor = database.query(TEST_TABLE_NAME,null/*cols*/,where
                , null, null, null, null, null);
        return mCursor; // iterate to get each value.
    }


    /**
     * Vloží otázku do databáze
     *
     * @param generatedtestid
     * @param questionid
     * @param jsonquestion
     * @return
     */
    public long insertQuestion(int generatedtestid, int questionid, JSONObject jsonquestion){
        ContentValues values = new ContentValues();
        values.put(QUESTION_ID, questionid);
        values.put(TEST_GENERATED_ID, generatedtestid);
        values.put(QUESTION_JSON, AES.zasifrovat(jsonquestion.toString()));
        Log.d("elogika databaze", "proběhl insert questionid " + questionid);
        return database.insert(QUESTION_TABLE_NAME, null, values);
    }

    /**
     * Vyzvedne otázku z databáze
     *
     * @param generatetestid
     * @return
     */
    public Cursor selectQuestions(int generatetestid) {
       // String[] cols = new String[] {QUESTION_ID, TEST_GENERATED_ID, QUESTION_JSON};
        String where = TEST_GENERATED_ID+"="+generatetestid;
        Cursor mCursor = database.query(true, QUESTION_TABLE_NAME,null,where
                , null, null, null, null, null);

        return mCursor; // iterate to get each value.
    }


    /**
     * Vloží odpovědi k otázce do databáze
     *
     * @param generatedtestid
     * @param questionid
     * @param optionid
     * @param jsonquestion
     * @return
     */
    public long insertOption(int generatedtestid,int questionid, int optionid, JSONObject jsonquestion){
        ContentValues values = new ContentValues();
        values.put(OPTION_ID, optionid);
        values.put(QUESTION_ID, questionid);
        values.put(TEST_GENERATED_ID, generatedtestid);
        values.put(OPTION_JSON, AES.zasifrovat(jsonquestion.toString()));
        Log.d("elogika databaze", "proběhl insert optionid " + optionid);
        return database.insert(OPTION_TABLE_NAME, null, values);
    }

    /**
     * Vyzvedne odpovědi k otázce z databáze
     * @param generatetestid
     * @param questionid
     * @return
     */
    public Cursor selectOptions(int generatetestid, int questionid) {
     //   String[] cols = new String[] {OPTION_ID, QUESTION_ID, TEST_GENERATED_ID, OPTION_JSON};

        String where = TEST_GENERATED_ID+"="+generatetestid + " and "+ QUESTION_ID +"="+ questionid ;
        Cursor mCursor = database.query( true, OPTION_TABLE_NAME,null,where
                , null, null, null, null, null);

        return mCursor; // iterate to get each value.
    }

    /**
     * Smaže test z databáze podle id
     *
     * @param generatetestid id testu
     */
    public void deleteTest(int generatetestid) {
        database.execSQL("delete from " + TEST_TABLE_NAME + " where " + TEST_GENERATED_ID +" = " + generatetestid);
        database.execSQL("delete from " + QUESTION_TABLE_NAME + " where " + TEST_GENERATED_ID +" = " + generatetestid);
        database.execSQL("delete from " + OPTION_TABLE_NAME + " where " + TEST_GENERATED_ID +" = "+ generatetestid);
    }


    /**
     * Vloží výsledky vypracovaného offline testu do databáze
     *
     * @param generatedtestid
     * @param dateSendTo
     * @param name
     * @param save
     * @param StatOtazka
     * @param StatOdpoved
     * @param addlog
     * @return
     */
    public long insertOfflineTestResult(int generatedtestid,String dateSendTo,String name, JSONObject save ,JSONObject StatOtazka, JSONObject StatOdpoved,JSONObject addlog){
        ContentValues values = new ContentValues();
        values.put(TEST_GENERATED_ID, generatedtestid);
        values.put(TEST_DATE_SEND_TO, dateSendTo);
        values.put(UNSAVED_NAME, name);
        values.put(UNSAVED_SAVE, save.toString());
        values.put(UNSAVED_OTAZKA, StatOtazka.toString());
        values.put(UNSAVED_ODPOVED, StatOdpoved.toString());
        values.put(UNSAVED_LOG, addlog.toString());
        Log.d("elogika databaze", "proběhl insert generatedtestid " + generatedtestid);
        return database.insert(UNSAVED_TABLE_NAME, null, values);
    }

    /**
     * Zobrazí názvy vypracovaných offline testů s odpovídajícími datumy nejposlednější možnosti odevzdání
     * @return
     */
    public Cursor selectOfflineDoneNames() {
           String[] cols = new String[] {UNSAVED_NAME, TEST_DATE_SEND_TO};
        Cursor mCursor = database.query( false, UNSAVED_TABLE_NAME,cols, null
                , null, null, null, null, null);

        return mCursor; // iterate to get each value.
    }

    /**
     * Vyzvedne všechny výsledky vypracovaných offline testů
     */
    public Cursor selectAllDoneTests() {
        String[] cols = new String[] {UNSAVED_SAVE, UNSAVED_OTAZKA, UNSAVED_ODPOVED, UNSAVED_LOG};

        Cursor mCursor = database.query( false, UNSAVED_TABLE_NAME,cols, null
                , null, null, null, null, null);

        return mCursor; // iterate to get each value.
    }


    /**
     * Vymaže všechny uložené vypracované testy z databáze
     */
    public void deleteALLDoneTests() {
        database.execSQL("delete from " + UNSAVED_TABLE_NAME);
        Log.d("Databaze", "vymazany všechny vysledky offline testů");
    }


}