package cz.vsb.elogika.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import cz.vsb.elogika.R;

public class SpecialCharDialogButton extends Button {

    EditText e;
    public SpecialCharDialogButton(Context context, AttributeSet attrs) {
        super(context,attrs);
        init(context);
    }
    public SpecialCharDialogButton(final Context context){
        super(context);
        init(context);
    }

    public void linkWithEditText(EditText e){
        this.e = e;
    }

   private void add(String s){
        e.getText().insert(e.getSelectionStart(), s);
    }

    private void init(final Context context) {
        this.setOnClickListener(new View.OnClickListener() {
            AlertDialog keyboarddialog;
            @Override
            public void onClick(View v) {
                final View keyboard = inflate(context, R.layout.special_keyboard, null);
                ((Button) keyboard.findViewById(R.id.keyboardbutton1)).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        add((String)((Button)v).getText());
                        keyboarddialog.cancel();
                    }
                });
                ((Button) keyboard.findViewById(R.id.keyboardbutton2)).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        add((String)((Button)v).getText());
                        keyboarddialog.cancel();
                    }
                });
                ((Button) keyboard.findViewById(R.id.keyboardbutton3)).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        add((String)((Button)v).getText());
                        keyboarddialog.cancel();
                    }
                });
                ((Button) keyboard.findViewById(R.id.keyboardbutton4)).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        add((String)((Button)v).getText());
                        keyboarddialog.cancel();
                    }
                });
                ((Button) keyboard.findViewById(R.id.keyboardbutton5)).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        add((String)((Button)v).getText());
                        keyboarddialog.cancel();
                    }
                });
                ((Button) keyboard.findViewById(R.id.keyboardbutton6)).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        add((String)((Button)v).getText());
                        keyboarddialog.cancel();
                    }
                });
                ((Button) keyboard.findViewById(R.id.keyboardbutton7)).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        add((String)((Button)v).getText());
                        keyboarddialog.cancel();
                    }
                });
                ((Button) keyboard.findViewById(R.id.keyboardbutton8)).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        add((String)((Button)v).getText());
                        keyboarddialog.cancel();
                    }
                });
                ((Button) keyboard.findViewById(R.id.keyboardbutton9)).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        add((String)((Button)v).getText());
                        keyboarddialog.cancel();
                    }
                });
                ((Button) keyboard.findViewById(R.id.keyboardbutton10)).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        add((String)((Button)v).getText());
                        keyboarddialog.cancel();
                    }
                });
                ((Button) keyboard.findViewById(R.id.keyboardbutton11)).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        add((String)((Button)v).getText());
                        keyboarddialog.cancel();
                    }
                });
                ((Button) keyboard.findViewById(R.id.keyboardbutton12)).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        add((String)((Button)v).getText());
                        keyboarddialog.cancel();
                    }
                });
                ((Button) keyboard.findViewById(R.id.keyboardbutton13)).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        add((String)((Button)v).getText());
                        keyboarddialog.cancel();
                    }
                });

            keyboarddialog = new AlertDialog.Builder(context)
                        .setTitle(R.string.add_special_char)
                        .setView(keyboard)
                        .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                //  hide
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_dialer)
                        .show();
            }
        });
    }

}
