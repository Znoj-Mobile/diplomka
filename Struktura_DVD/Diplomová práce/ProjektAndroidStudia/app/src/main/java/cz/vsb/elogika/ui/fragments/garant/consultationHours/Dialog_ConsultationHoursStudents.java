package cz.vsb.elogika.ui.fragments.garant.consultationHours;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.ui.fragments.garant.emails.Fragment_email_message;
import cz.vsb.elogika.utils.DividerItemDecoration;
import cz.vsb.elogika.utils.Logging;
import cz.vsb.elogika.utils.TimeLogika;

public class Dialog_ConsultationHoursStudents extends DialogFragment
{
    public int id;
    public String date;
    private RecyclerView.Adapter adapter;
    private View nothingToShow;
    private View loadingProgress;
    private int count;
    private int sum;
    public ArrayList<LoggedStudent> loggedStudents;

    public Dialog_ConsultationHoursStudents()
    {
        this.loggedStudents = new ArrayList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        final View rootView = inflater.inflate(R.layout.recycler_view_with_button, container, false);
        // Přidání hlavičky
        final View header = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_consultation_hours_students_header, container, false);
        ((ViewGroup) rootView.findViewById(R.id.recyclerViewLayout)).addView(header, 0);
        final CheckBox topCheckbox = (CheckBox) header.findViewById(R.id.checkbox);
        this.loadingProgress = rootView.findViewById(R.id.loading_progress);
        this.nothingToShow = rootView.findViewById(R.id.nothing_to_show);
        this.loadingProgress.setVisibility(View.VISIBLE);
        final Button roundButton = (Button) rootView.findViewById(R.id.roundButton);
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);

        // Vytvoření adaptéru
        this.adapter = new RecyclerView.Adapter()
        {
            class ViewHolder extends RecyclerView.ViewHolder
            {
                public CheckBox checkBox;
                public TextView student;
                public TextView loginDate;
                public TextView reason;

                public ViewHolder(View itemView)
                {
                    super(itemView);
                    this.checkBox = (CheckBox) itemView.findViewById(R.id.checkbox);
                    this.student = (TextView) itemView.findViewById(R.id.student);
                    this.loginDate = (TextView) itemView.findViewById(R.id.login_date);
                    this.reason = (TextView) itemView.findViewById(R.id.reason);
                }
            }

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
            {
                return new ViewHolder(LayoutInflater.from(getActivity()).inflate(R.layout.dialog_consultation_hours_students_item, parent, false));
            }

            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position)
            {
                final ViewHolder holder = (ViewHolder) viewHolder;
                holder.checkBox.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        boolean b = ((CheckBox) view).isChecked();

                        loggedStudents.get(position).isChecked = b;

                        if (b) count++;
                        else count--;

                        if (count > 0) roundButton.setEnabled(true);
                        else roundButton.setEnabled(false);
                        if (count == sum) topCheckbox.setChecked(true);
                        else topCheckbox.setChecked(false);
                    }
                });

                holder.checkBox.setChecked(loggedStudents.get(position).isChecked);
                holder.student.setText(loggedStudents.get(position).student);
                holder.loginDate.setText(loggedStudents.get(position).loginDate);
                holder.reason.setText(loggedStudents.get(position).reason);
            }

            @Override
            public int getItemCount()
            {
                return loggedStudents.size();
            }
        };
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        roundButton.setEnabled(false);
        roundButton.setText("✔");
        roundButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Bundle bundle = new Bundle();
                //dialog.show(getFragmentManager(), "dialog");
                bundle.putBoolean("logged", true);
                bundle.putSerializable("loggedStudents", loggedStudents);
                Fragment_email_message fragment_email_message = new Fragment_email_message();
                fragment_email_message.setArguments(bundle);
                getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment_email_message).commit();
                dismiss();
            }
        });

        topCheckbox.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                boolean b = ((CheckBox) view).isChecked();
                for (LoggedStudent loggedStudent : loggedStudents) loggedStudent.isChecked = b;
                adapter.notifyDataSetChanged();

                if (b)
                {
                    count = sum;
                    roundButton.setEnabled(true);
                }
                else
                {
                    count = 0;
                    roundButton.setEnabled(false);
                }
            }
        });

        loadLoggedStudents();

        Logging.logAccess("/Pages/Other/ConsultationHours/ConsultationHoursStudents.aspx?Id=" + this.id + "&date=" + this.date);

        return rootView;
    }



    /**
     * Načte všechny studenty, kteří jsou přihlášení na dané konzultační hodině.
     */
    private void loadLoggedStudents()
    {
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                loadingProgress.setVisibility(View.GONE);

                JSONObject o;
                LoggedStudent loggedStudent;

                sum = response.length();

                for (int j = 0; j < response.length(); j++)
                {
                    o = response.getJSONObject(j);
                    loggedStudent = new LoggedStudent();
                    loggedStudent.id = o.getInt("StudentId");
                    loggedStudent.email = o.getString("Email");
                    loggedStudent.student = (o.getString("Before").isEmpty() ? "" : o.getString("Before") + " ") + o.getString("Name") + " " + o.getString("SurName") + (o.getString("After").isEmpty() ? "" : " " + o.getString("After"));
                    loggedStudent.loginDate = TimeLogika.getFormatedDateTime(o.getString("LoginAt"));
                    loggedStudent.reason = o.getString("Reason");

                    loggedStudents.add(loggedStudent);
                }
                adapter.notifyDataSetChanged();

                // Zobrazení prázdného layoutu
                if (loggedStudents.size() == 0)
                {
                    nothingToShow.setVisibility(View.VISIBLE);
                }
            }
        };
        Request.get(URL.ConsultationHours.StudentsConsultationHours(this.id, this.date), arrayResponse);
    }



    @Override
    public void onStart()
    {
        super.onStart();
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }
}
