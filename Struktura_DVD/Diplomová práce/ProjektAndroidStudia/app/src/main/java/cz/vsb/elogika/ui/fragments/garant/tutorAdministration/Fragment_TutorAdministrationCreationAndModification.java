package cz.vsb.elogika.ui.fragments.garant.tutorAdministration;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.android.volley.VolleyError;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import org.json.JSONException;
import org.json.JSONObject;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.Hash;

public class Fragment_TutorAdministrationCreationAndModification extends Fragment
{
    private int userID;
    private JSONObject data;
    private View progressBar;
    private Button roundButton;
    private LinearLayout rootLayout;

    private EditText login;
    private EditText name;
    private EditText surname;
    private EditText birthSurname;
    private EditText email;
    private EditText language;
    private EditText street;
    private EditText landRegistryNumber;
    private EditText postcode;
    private EditText city;
    private EditText degreeInFrontOfName;
    private EditText degreeBehindName;



    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_tutor_administration_creation_and_modification, container, false);
        this.rootLayout = (LinearLayout) rootView.findViewById(R.id.rootLayout);
        this.progressBar = rootView.findViewById(R.id.loading_progress);
        this.roundButton = (Button) rootView.findViewById(R.id.roundButton);
        this.roundButton.setText("✔");

        if (getArguments() == null) this.userID = 0;
        else this.userID = getArguments().getInt("userID");

        if (this.userID == 0) Actionbar.addSubcategory(R.string.create_new_tutor, this);
        else
        {
            Actionbar.addSubcategory(R.string.edit_tutor, this);
            loadTutor();
        }

        this.login = (EditText) rootView.findViewById(R.id.user_school_login);
        this.name = (EditText) rootView.findViewById(R.id.user_name);
        this.surname = (EditText) rootView.findViewById(R.id.user_surname);
        this.birthSurname = (EditText) rootView.findViewById(R.id.user_birth_surname);
        this.email = (EditText) rootView.findViewById(R.id.user_email);
        this.language = (EditText) rootView.findViewById(R.id.user_language);
        this.street = (EditText) rootView.findViewById(R.id.user_street);
        this.landRegistryNumber = (EditText) rootView.findViewById(R.id.user_land_registry_number);
        this.postcode = (EditText) rootView.findViewById(R.id.user_postcode);
        this.city = (EditText) rootView.findViewById(R.id.user_city);
        this.degreeInFrontOfName = (EditText) rootView.findViewById(R.id.user_degree_in_front_of_name);
        this.degreeBehindName = (EditText) rootView.findViewById(R.id.user_degree_behind_name);

        if (this.userID != 0)
        {
            this.progressBar.setVisibility(View.VISIBLE);
            this.roundButton.setVisibility(View.GONE);
            this.rootLayout.setVisibility(View.GONE);

            rootView.findViewById(R.id.label_user_school_login).setVisibility(View.GONE);
            this.login.setVisibility(View.GONE);
        }

        this.roundButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                roundButton.setEnabled(false);
                boolean everythingOK = true;

                if (login.length() == 0)
                {
                    SnackbarManager.show(Snackbar.with(getActivity()).text(R.string.tutor_login_empty));
                    everythingOK = false;
                }
                if (name.length() == 0)
                {
                    SnackbarManager.show(Snackbar.with(getActivity()).text(R.string.tutor_name_empty));
                    everythingOK = false;
                }
                if (surname.length() == 0)
                {
                    SnackbarManager.show(Snackbar.with(getActivity()).text(R.string.tutor_surname_empty));
                    everythingOK = false;
                }
                if (email.length() == 0)
                {
                    SnackbarManager.show(Snackbar.with(getActivity()).text(R.string.tutor_email_empty));
                    everythingOK = false;
                }
                if (street.length() == 0)
                {
                    SnackbarManager.show(Snackbar.with(getActivity()).text(R.string.tutor_street_empty));
                    everythingOK = false;
                }
                if (landRegistryNumber.length() == 0)
                {
                    SnackbarManager.show(Snackbar.with(getActivity()).text(R.string.tutor_land_registry_number_empty));
                    everythingOK = false;
                }
                if (city.length() == 0)
                {
                    SnackbarManager.show(Snackbar.with(getActivity()).text(R.string.tutor_city_empty));
                    everythingOK = false;
                }
                if (!everythingOK)
                {
                    roundButton.setEnabled(true);
                    return;
                }

                if (userID == 0) createTutor();
                else save();
            }
        });

        return rootView;
    }



    /**
     * Načte tutorův účet, pokud se jedná o editaci.
     */
    private void loadTutor()
    {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                data = response;

                login.setText(response.getString("Login"));
                name.setText(response.getString("Jmeno"));
                surname.setText(response.getString("Prijmeni"));
                birthSurname.setText(response.getString("PrijmeniRodne"));
                email.setText(response.getString("Email"));
                language.setText(response.getString("Jazyk"));
                street.setText(response.getString("Ulice"));
                landRegistryNumber.setText(response.getString("CisloPop"));
                postcode.setText(response.getString("PSC"));
                city.setText(response.getString("Mesto"));
                degreeInFrontOfName.setText(response.getString("TitulPred"));
                degreeBehindName.setText(response.getString("TitulZa"));

                progressBar.setVisibility(View.GONE);
                roundButton.setVisibility(View.VISIBLE);
                rootLayout.setVisibility(View.VISIBLE);
            }
        };
        Request.get(URL.User.Get(this.userID), response);
    }



    /**
     * Vytvoří tutorovi nový účet.
     */
    private void createTutor()
    {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                SnackbarManager.show(Snackbar.with(getActivity()).text(R.string.tutor_created_successfully));
                insertTutor(response.getInt("UserId"), login.getText().toString());
            }

            @Override
            public void onError(VolleyError error)
            {
                super.onError(error);
                roundButton.setEnabled(true);
            }

            @Override
            public void onException(JSONException exception)
            {
                super.onException(exception);
                roundButton.setEnabled(true);
            }
        };
        JSONObject parameters = new JSONObject();
        try
        {
            fillData(parameters);
            parameters.put("Salt", Hash.getSalt());
            parameters.put("Heslo", "");
            parameters.put("Fotka", "");
            parameters.put("Smazany", false);
        }
        catch (JSONException e) {}
        Request.post(URL.User.insertUser(), parameters, response);
    }



    /**
     * Přiřadí právě vytvořeného tutora ke kurzu, který má uživatel vybraný.
     * @param id zrovna vytvořeného tutora
     * @param login který mu byl právě vytvořen
     */
    private void insertTutor(int id, String login)
    {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                SnackbarManager.show(Snackbar.with(getActivity()).text(R.string.tutor_added_into_course));
                Actionbar.jumpBack();
            }

            @Override
            public void onError(VolleyError error)
            {
                super.onError(error);
                roundButton.setEnabled(true);
            }

            @Override
            public void onException(JSONException exception)
            {
                super.onException(exception);
                roundButton.setEnabled(true);
            }
        };
        Request.post(URL.User.InsertTutor(id, User.courseInfoId, User.schoolInfoID, login), response);
    }



    /**
     * Upraví tutorovi účet.
     */
    private void save()
    {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                SnackbarManager.show(Snackbar.with(getActivity()).text(R.string.tutor_edited_successfully));
                Actionbar.jumpBack();
            }

            @Override
            public void onError(VolleyError error)
            {
                super.onError(error);
                roundButton.setEnabled(true);
            }

            @Override
            public void onException(JSONException exception)
            {
                super.onException(exception);
                roundButton.setEnabled(true);
            }
        };
        try
        {
            fillData(this.data);
        }
        catch (JSONException e) {}
        Request.post(URL.User.Post(), this.data, response);
    }



    /**
     * Naháže data z formuláře do objektu parameters.
     * @param parameters
     * @throws JSONException
     */
    private void fillData(JSONObject parameters) throws JSONException
    {
        parameters.put("Login", this.login.getText());
        parameters.put("Jmeno", this.name.getText());
        parameters.put("Prijmeni", this.surname.getText());
        parameters.put("PrijmeniRodne", this.birthSurname.getText());
        parameters.put("Email", this.email.getText());
        parameters.put("Jazyk", this.language.getText());
        if(this.language.getText().length() == 0){
            parameters.put("Jazyk", "0");
        }
        parameters.put("Ulice", this.street.getText());
        parameters.put("CisloPop", this.landRegistryNumber.getText());
        parameters.put("PSC", this.postcode.getText());
        if(this.postcode.getText().length() == 0){
            parameters.put("PSC", "0");
        }
        parameters.put("Mesto", this.city.getText());
        parameters.put("TitulPred", this.degreeInFrontOfName.getText());
        parameters.put("TitulZa", this.degreeBehindName.getText());
    }
}
