package cz.vsb.elogika.ui.fragments.garant.courseConditions;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.DividerItemDecoration;
import cz.vsb.elogika.utils.EnumLogika;
import cz.vsb.elogika.utils.Logging;

public class Fragment_cc_summary extends Fragment implements SwipeRefreshLayout.OnRefreshListener{
    private View mainView;
    private View rootView;
    private RecyclerView recyclerView;

    SwipeRefreshLayout swipeLayout;
    LinearLayout nothinglayout;

    private RecyclerView.Adapter adapter;

    ArrayList<Map<String, String>> cc_summary_list;

    String idSkupinaAktivit = null;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mainView = inflater.inflate(R.layout.fragment_course_conditions_summary, container, false);
        rootView = mainView.findViewById(R.id.recycler_view_swipe);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);

        swipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_update);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeResources(R.color.darker_cyan, R.color.material_blue_grey_800);

        nothinglayout = (LinearLayout) rootView.findViewById(R.id.nothing_to_show);
        nothinglayout.setVisibility(View.GONE);

        cc_summary_list = new ArrayList<>();

        try {
            idSkupinaAktivit = getArguments().getString("idSkupinaAktivit");
        }
        catch(Exception e){

        }

        //celkovy souhrn vysledku kurzu
        if(idSkupinaAktivit == null){
            Actionbar.addSubcategory(R.string.summary_of_results_of_course, this);
            Logging.logAccess("Pages/Other/SouhrnCelkem.aspx");
            //mTitleTextView.setText(getResources().getString(R.string.summary_of_results_of_course));
        }
        //souhrn vysledku skupiny aktivit
        else{
            Actionbar.addSubcategory(R.string.summary_of_result_activity_group, this);
            Logging.logAccess("/Pages/Other/Souhrn.aspx?IDSA=" + idSkupinaAktivit);
            //mTitleTextView.setText(getResources().getString(R.string.summary_of_result_activity_group));
        }


        this.adapter = new RecyclerView.Adapter() {
            class ViewHolderHeader extends RecyclerView.ViewHolder {
                public ViewHolderHeader(View itemView) {
                    super(itemView);
                }
            }

            class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
                public TextView system_login;
                public TextView surname;
                public TextView name;
                public TextView school_login;
                public TextView study_form;
                public TextView result;
                public TextView fulfilled;

                public ViewHolder(View itemView) {
                    super(itemView);
                    itemView.setOnClickListener(this);
                    this.system_login = (TextView) itemView.findViewById(R.id.cc_sum_system_login);
                    this.surname = (TextView) itemView.findViewById(R.id.cc_sum_surname);
                    this.name = (TextView) itemView.findViewById(R.id.cc_sum_name);
                    this.school_login = (TextView) itemView.findViewById(R.id.cc_sum_school_login);
                    this.study_form = (TextView) itemView.findViewById(R.id.cc_sum_study_form);
                    this.result = (TextView) itemView.findViewById(R.id.cc_sum_result);
                    this.fulfilled = (TextView) itemView.findViewById(R.id.cc_sum_fulfilled);
                }

                @Override
                public void onClick(View view) {
                    int position = getLayoutPosition();
                    Bundle bundle = new Bundle();
                    bundle.putString("studyForm", cc_summary_list.get(position).get("formaStudia"));
                    bundle.putString("userId", cc_summary_list.get(position).get("idUzivatel"));
                    //souhrn kurzu
                    if(idSkupinaAktivit == null){
                        Fragment_cc_sum_detail_course_tabs fragment_cc_sum_detail_course_tabs = new Fragment_cc_sum_detail_course_tabs();
                        fragment_cc_sum_detail_course_tabs.setArguments(bundle);
                        getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment_cc_sum_detail_course_tabs).commit();
                    }
                    //souhrn (podle skupiny aktivit)
                    else{

                        Fragment_cc_sum_detail fragment_cc_sum_detail = new Fragment_cc_sum_detail();
                        //navrat na 1. zalozku
                        bundle.putString("tab", getArguments().getString("tab"));
                        bundle.putString("idSkupinaAktivit", idSkupinaAktivit);
                        fragment_cc_sum_detail.setArguments(bundle);
                        getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment_cc_sum_detail).commit();
                    }
                }
            }

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view;
                switch (viewType) {
                    case 1:
                        view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_course_conditions_summary_list, parent, false);
                        return new ViewHolder(view);
                    default:
                        return null;
                }
            }
            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
                if (holder.getItemViewType() == 1) {
                    final ViewHolder viewHolder = (ViewHolder) holder;

                    viewHolder.system_login.setText(cc_summary_list.get(position).get("systemLogin"));
                    viewHolder.surname.setText(cc_summary_list.get(position).get("prijmeni"));
                    viewHolder.name.setText(cc_summary_list.get(position).get("jmeno"));
                    viewHolder.school_login.setText(cc_summary_list.get(position).get("loginSkola"));
                    viewHolder.study_form.setText(EnumLogika.formaStudia(cc_summary_list.get(position).get("formaStudia")));
                    viewHolder.result.setText(cc_summary_list.get(position).get("vysledek"));
                    viewHolder.fulfilled.setText(EnumLogika.splnilAnoNe(cc_summary_list.get(position).get("splnil")));
                }
            }

            @Override
            public int getItemViewType(int position)
            {
                return 1;
            }

            @Override
            public int getItemCount() {
                return cc_summary_list.size();
            }
        };

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        recyclerView.setHasFixedSize(true);
        //zaruci, ze se bude seznam aktualizovat jen pri swipu uplne nahore...
        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy <= 0) {
                    swipeLayout.setEnabled(true);
                } else {
                    swipeLayout.setEnabled(false);
                }
            }
        });
        recyclerView.setAdapter(adapter);

        update();

        return mainView;
    }

    public void update() {
        swipeLayout.post(new Runnable() {
            @Override
            public void run() {
                nothinglayout.setVisibility(View.GONE);
                swipeLayout.setRefreshing(true);
                cc_summary_list.clear();
                adapter.notifyDataSetChanged();
                data_into_summary_list();
            }
        });
    }

    private void data_into_summary_list() {
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                if (response.length() > 0)
                {
                    for (int j = 0; j < response.length(); j++)
                    {
                        cc_summary_list.add(put_data(
                                response.getJSONObject(j).getString("IDUzivatel"),
                                response.getJSONObject(j).getString("SystemLogin"),
                                response.getJSONObject(j).getString("Jmeno"),
                                response.getJSONObject(j).getString("Prijmeni"),
                                response.getJSONObject(j).getString("LoginSkola"),
                                response.getJSONObject(j).getString("Vysledek"),
                                response.getJSONObject(j).getString("Splnil"),
                                response.getJSONObject(j).getString("FormaStudia")));
                    }
                }
                else{
                    Log.d("Chyba", "CourseConditions Data_into_summary_list");
                    nothinglayout.setVisibility(View.VISIBLE);
                }
                swipeLayout.setRefreshing(false);
                adapter.notifyDataSetChanged();
            }
        };
        //souhrn kurzu
        if(idSkupinaAktivit == null){
            Request.get(URL.StudentSummary.GetSummaryByCourse(User.courseInfoId), arrayResponse);
        }
        //souhrn (podle skupiny aktivit)
        else{
            Request.get(URL.StudentSummary.GetSummaryByGroupActivity(idSkupinaAktivit), arrayResponse);
        }
    }

    private Map<String, String> put_data(String idUzivatel, String systemLogin, String jmeno, String prijmeni, String loginSkola, String vysledek, String splnil, String formaStudia) {
        HashMap<String, String> item = new HashMap<String, String>();

        item.put("idUzivatel",idUzivatel);
        item.put("systemLogin",systemLogin);
        item.put("jmeno",jmeno);
        item.put("prijmeni",prijmeni);
        item.put("loginSkola",loginSkola);
        item.put("vysledek",vysledek);
        item.put("splnil",splnil);
        item.put("formaStudia",formaStudia);

        return item;
    }

    @Override
    public void onRefresh() {
        update();
    }
}