package cz.vsb.elogika.ui.fragments.garant.tutorAdministration;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.dataTypes.Tutor;
import cz.vsb.elogika.model.Informations;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.DividerItemDecoration;

public class Fragment_TutorAdministrationAssigning extends Fragment
{
    private RecyclerView recyclerView;
    private View loadingProgress;
    private View nothingToShow;
    private Button roundButton;
    private View writeSurnameAndFind;
    private RecyclerView.Adapter adapter;
    private ArrayList<Tutor> tutors;
    private TextView surname;
    private boolean[] selectedTutors;


    public Fragment_TutorAdministrationAssigning()
    {
        this.tutors = new ArrayList();
        this.selectedTutors = new boolean[0];
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        Actionbar.addSubcategory(R.string.tutor_searching, this);

        final View rootView = inflater.inflate(R.layout.fragment_tutor_administration_assigning, container, false);
        // Přidání hlavičky
        ((ViewGroup) rootView).addView(LayoutInflater.from(getActivity()).inflate(R.layout.fragment_tutor_administration_assigning_header, container, false), 1);
        this.recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        rootView.findViewById(R.id.loading_progress).setVisibility(View.VISIBLE);

        this.surname = (TextView) rootView.findViewById(R.id.surname);
        this.loadingProgress = rootView.findViewById(R.id.loading_progress);
        this.nothingToShow = rootView.findViewById(R.id.nothing_to_show);
        this.writeSurnameAndFind = rootView.findViewById(R.id.write_surname_and_find);
        this.roundButton = (Button) rootView.findViewById(R.id.roundButton);
        this.roundButton.setEnabled(false);
        rootView.findViewById(R.id.search).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                loadingProgress.setVisibility(View.VISIBLE);
                nothingToShow.setVisibility(View.GONE);
                writeSurnameAndFind.setVisibility(View.GONE);
                roundButton.setEnabled(false);
                searchTutors();
            }
        });

        // Vytvoření adaptéru
        this.adapter = new RecyclerView.Adapter()
        {
            class ViewHolderHeader extends RecyclerView.ViewHolder
            {

                public ViewHolderHeader(View itemView)
                {
                    super(itemView);
                }
            }

            class ViewHolder extends RecyclerView.ViewHolder
            {
                public CheckBox checkBox;
                public TextView login;
                public TextView name;
                public TextView surname;
                public TextView email;
                public TextView course;

                public ViewHolder(View itemView)
                {
                    super(itemView);
                    this.checkBox = (CheckBox) itemView.findViewById(R.id.checkbox);
                    this.login = (TextView) itemView.findViewById(R.id.login);
                    this.name = (TextView) itemView.findViewById(R.id.name);
                    this.surname = (TextView) itemView.findViewById(R.id.surname);
                    this.email = (TextView) itemView.findViewById(R.id.email);
                    this.course = (TextView) itemView.findViewById(R.id.course);
                }
            }

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
            {
                View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_tutor_administration_assigning_item, parent, false);
                return new ViewHolder(view);
            }

            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position)
            {
                final ViewHolder holder = (ViewHolder) viewHolder;
                final Tutor tutor = tutors.get(position);

                // Nastavení listeneru na celý řádek, aby se po kliknutí zaškrtnul / odškrtnul checkbox
                holder.itemView.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        holder.checkBox.setChecked(!holder.checkBox.isChecked());
                    }
                });

                // Nastavení listeneru na checkbox, který uloží do pole informaci o jeho zaškrtnutí
                holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
                {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b)
                    {
                        selectedTutors[position] = b;
                    }
                });

                holder.login.setText(tutor.login);
                holder.name.setText(tutor.name);
                holder.surname.setText(tutor.surname);
                holder.email.setText(tutor.email);
                holder.course.setText(Informations.courseInfoNames.get(Informations.courseInfoIds.indexOf(User.courseInfoId)));
            }

            @Override
            public int getItemCount()
            {
                return tutors.size();
            }
        };
        this.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        this.recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        this.recyclerView.setHasFixedSize(true);
        this.recyclerView.setAdapter(adapter);

        roundButton.setText("✔");
        roundButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                insertTutors();
            }
        });

        return rootView;
    }



    /**
     * Vyžádá si ze serveru seznam tutorů, kteří vyhovují zadanému příjmení.
     */
    private void searchTutors()
    {
        this.tutors.clear();

        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                getActivity().findViewById(R.id.loading_progress).setVisibility(View.GONE);

                JSONObject o;
                Tutor tutor;

                for (int j = 0; j < response.length(); j++)
                {
                    o = response.getJSONObject(j);
                    tutor = new Tutor();
                    tutor.id = o.getInt("IdUzivatel");
                    tutor.name = o.getString("Jmeno");
                    tutor.surname = o.getString("Prijmeni");
                    tutor.login = o.getString("Login");
                    tutor.email = o.getString("Email");

                    tutors.add(tutor);
                }
                selectedTutors = new boolean[response.length()];
                adapter.notifyDataSetChanged();
                roundButton.setEnabled(true);

                // Zobrazení prázdného layoutu
                if (tutors.size() == 0)
                {
                    getActivity().findViewById(R.id.nothing_to_show).setVisibility(View.VISIBLE);
                }
            }
        };
        Request.get(URL.User.FindUserBySchool(this.surname.getText().toString(), User.courseInfoId, User.schoolID), arrayResponse);
    }



    /**
     * Projde pole selectedTutors a pokud je nějaký prvek true, tak pošle na server
     * dotaz s idéčkama vybraných tutorů.
     */
    private void insertTutors()
    {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                if (!response.getBoolean("Success")) return;
                SnackbarManager.show(Snackbar.with(getActivity()).text(R.string.tutors_were_assigned_into_curse));
                getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, new Fragment_TutorAdministrationList()).commit();
            }
        };
        JSONArray parameters = new JSONArray();
        for(int j = 0; j < this.selectedTutors.length; j++)
        {
            if (this.selectedTutors[j]) parameters.put(this.tutors.get(j).id);
        }
        if (parameters.length() == 0) return;
        Request.post(URL.User.InsertTutors(User.courseInfoId, User.schoolInfoID, ""), parameters, response);
    }
}
