package cz.vsb.elogika.ui;

import android.app.ActionBar;
import android.app.Fragment;
import android.os.Bundle;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;

import cz.vsb.elogika.R;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.ui.fragments.Fragment_News;
import cz.vsb.elogika.ui.fragments.Fragment_Theory;
import cz.vsb.elogika.ui.fragments.garant.Fragment_Category;
import cz.vsb.elogika.ui.fragments.garant.classes.Fragment_Classes;
import cz.vsb.elogika.ui.fragments.garant.consultationHours.Fragment_ConsultationHoursTabs;
import cz.vsb.elogika.ui.fragments.garant.courseConditions.Fragment_CourseConditions;
import cz.vsb.elogika.ui.fragments.garant.emails.Fragment_Emails;
import cz.vsb.elogika.ui.fragments.garant.logs.Fragment_Logs;
import cz.vsb.elogika.ui.fragments.garant.question.Fragment_Question;
import cz.vsb.elogika.ui.fragments.garant.templateAdministration.Fragment_TemplateAdministration;
import cz.vsb.elogika.ui.fragments.garant.testsAndAcvtivities.Fragment_TestsAndActivity;


public class ContainerActivityTutor extends ContainerActivity {
    public ContainerActivityTutor() {
        User.isGarant = false;
        User.isTutor = true;
    }

    @Override
    public Fragment selectFragment(int position) {                //změna layoutu, které musí dědit od třídy fragment probíhá zde ve switchi

        //změna horni listy
        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            //todo - pokud budeme vsude pouzivat custom action bar, pak se tohle smaze
            actionBar.setDisplayShowCustomEnabled(false);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setDisplayUseLogoEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }

        Fragment mFragment = null;
        switch (position - 1) {
            case 0:
                mFragment = new Fragment_CourseConditions();
                break;
            case 1:
                Bundle bundle = new Bundle();
                bundle.putString("idSkupinaAktivit", null);
                mFragment = new Fragment_TestsAndActivity();
                mFragment.setArguments(bundle);
                break;
            case 2:
                mFragment = new Fragment_Question();
                break;
            case 3:
                mFragment = new Fragment_Classes();
                break;
            case 4:
                mFragment = new Fragment_CourseConditions();
                break;
            case 5:
                mFragment = new Fragment_Category();
                break;
            case 6:
                mFragment = new Fragment_TemplateAdministration();
                break;
            case 7:
                mFragment = new Fragment_Theory();
                break;
            case 8:
                mFragment = new Fragment_News();
                break;
            case 9:
                mFragment = new Fragment_Emails();
                break;
            case 10:
                mFragment = new Fragment_ConsultationHoursTabs();
                break;
            case 11:
                mFragment = new Fragment_Logs();
                break;
            default:
                mFragment = null;
                //Toast.makeText(getLayoutInflater().getContext(), "Not implemented yet!", Toast.LENGTH_SHORT).show();
                SnackbarManager.show(Snackbar.with(getLayoutInflater().getContext()).type(SnackbarType.MULTI_LINE).text("Not implemented yet!"));
                break;
        }
        return mFragment;
    }



        @Override
        protected String[] getNames(){
            User.isGarant = false;
            User.isTutor = true;
            return new String[]{getString(R.string.course_condition),
                    getString(R.string.tests_and_activity_managing),
                    getString(R.string.questions_managing),
                    getString(R.string.list_of_classes_and_asociation_with_students),
                    getString(R.string.activity_groups_managing),
                    getString(R.string.category_managing),
                    getString(R.string.template_administration),
                    getString(R.string.chapters_managing),
                    getString(R.string.news),
                    getString(R.string.email_messages),
                    getString(R.string.consultation_hours),
                    getString(R.string.logs)};
        }

}