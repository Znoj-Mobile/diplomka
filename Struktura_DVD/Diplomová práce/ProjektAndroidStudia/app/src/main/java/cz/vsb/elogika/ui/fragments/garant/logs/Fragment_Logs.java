package cz.vsb.elogika.ui.fragments.garant.logs;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.DatePickerDialogOnButton;
import cz.vsb.elogika.utils.DividerItemDecoration;
import cz.vsb.elogika.utils.EnumLogika;
import cz.vsb.elogika.utils.TimeLogika;
import cz.vsb.elogika.utils.TimePickerDialogOnButton;

public class Fragment_Logs extends Fragment {
    private View mainView;
    private View rootView;

    private View head0;
    private View head1;
    private View head2;
    private RecyclerView recyclerView;


    public static ArrayList<Map<String, String>> log_list;
    private static LinearLayout nothinglayout;
    SwipeRefreshLayout swipeLayout;

    protected RecyclerView.Adapter adapter;

    EditText login;
    DatePickerDialogOnButton sinceDate;
    TimePickerDialogOnButton sinceTime;
    DatePickerDialogOnButton toDate;
    TimePickerDialogOnButton toTime;
    Button show_log_access;
    Button show_log_online_tests;
    Button show_log_all_actions;

    int pressed_button;



    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mainView = inflater.inflate(R.layout.fragment_logs, container, false);
        rootView = mainView.findViewById(R.id.recycler_view);
        head0 = mainView.findViewById(R.id.log_head_0);
        head1 = mainView.findViewById(R.id.log_head_1);
        head2 = mainView.findViewById(R.id.log_head_2);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);

        nothinglayout = (LinearLayout) rootView.findViewById(R.id.nothing_to_show);
        nothinglayout.setVisibility(View.GONE);

        swipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_update);
        swipeLayout.setEnabled(false);
        swipeLayout.setColorSchemeResources(R.color.darker_cyan, R.color.material_blue_grey_800);

        Actionbar.newCategory(R.string.logs, this);
        log_list = new ArrayList<>();

        this.adapter = new RecyclerView.Adapter() {
            class ViewHolderHeader extends RecyclerView.ViewHolder {
                public ViewHolderHeader(View itemView) {
                    super(itemView);
                }
            }

            class ViewHolder_access extends RecyclerView.ViewHolder implements View.OnClickListener{
                public TextView time;
                public TextView id_course_info;
                public TextView role;
                public TextView id_school_info;
                public TextView url;
                public TextView ip;

                public ViewHolder_access(View itemView) {
                    super(itemView);
                    itemView.setOnClickListener(this);
                    this.time = (TextView) itemView.findViewById(R.id.logs_time);
                    this.id_course_info = (TextView) itemView.findViewById(R.id.logs_id_course_info);
                    this.role = (TextView) itemView.findViewById(R.id.logs_role);
                    this.id_school_info = (TextView) itemView.findViewById(R.id.logs_id_school_info);
                    this.url = (TextView) itemView.findViewById(R.id.logs_url);
                    this.ip = (TextView) itemView.findViewById(R.id.logs_ip);
                }

                @Override
                public void onClick(View view) {

                }
            }

            class ViewHolder_actions extends RecyclerView.ViewHolder implements View.OnClickListener{
                public TextView time;
                public TextView action;

                public ViewHolder_actions(View itemView) {
                    super(itemView);
                    itemView.setOnClickListener(this);
                    this.time = (TextView) itemView.findViewById(R.id.logs_time);
                    this.action = (TextView) itemView.findViewById(R.id.logs_actions);
                }

                @Override
                public void onClick(View view) {

                }
            }

            class ViewHolder_onlinetest extends RecyclerView.ViewHolder implements View.OnClickListener{
                public TextView time;
                public TextView test;
                public TextView questionNumber;
                public TextView responseNumber;
                public TextView idTest;
                public TextView idQuestion;
                public TextView idResponse;
                public TextView generatedTest;
                public TextView checked;
                public TextView changes;
                public TextView question_time;
                public TextView views;
                public TextView category;

                public ViewHolder_onlinetest(View itemView) {
                    super(itemView);
                    itemView.setOnClickListener(this);
                    this.time = (TextView) itemView.findViewById(R.id.logs_time);
                    this.test = (TextView) itemView.findViewById(R.id.logs_test);
                    this.questionNumber = (TextView) itemView.findViewById(R.id.logs_questionNumber);
                    this.responseNumber = (TextView) itemView.findViewById(R.id.logs_responseNumber);
                    this.idTest = (TextView) itemView.findViewById(R.id.logs_idTest);
                    this.idQuestion = (TextView) itemView.findViewById(R.id.logs_idQuestion);
                    this.idResponse = (TextView) itemView.findViewById(R.id.logs_idResponse);
                    this.generatedTest = (TextView) itemView.findViewById(R.id.logs_generatedTest);
                    this.checked = (TextView) itemView.findViewById(R.id.logs_checked);
                    this.changes = (TextView) itemView.findViewById(R.id.logs_changes);
                    this.question_time = (TextView) itemView.findViewById(R.id.logs_question_time);
                    this.views = (TextView) itemView.findViewById(R.id.logs_views);
                    this.category = (TextView) itemView.findViewById(R.id.logs_category);
                }

                @Override
                public void onClick(View view) {

                }
            }

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view;
                switch (viewType) {
                    case 0:
                        view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_logs_access_head, parent, false);
                        return new ViewHolder_access(view);
                    case 1:
                        view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_logs_access_list, parent, false);
                        return new ViewHolder_access(view);

                    case 2:
                        view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_logs_actions_head, parent, false);
                        return new ViewHolder_actions(view);
                    case 3:
                        view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_logs_actions_list, parent, false);
                        return new ViewHolder_actions(view);

                    case 4:
                        view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_logs_onlinetest_head, parent, false);
                        return new ViewHolder_onlinetest(view);
                    case 5:
                        view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_logs_onlinetest_list, parent, false);
                        return new ViewHolder_onlinetest(view);
                    default:
                        return null;
                }
            }

            @Override
            public int getItemViewType(int position)
            {
                if(pressed_button == 0) {
                    /*if(position == 0) {
                        return 0;
                    }*/
                    return 1;
                }
                else if(pressed_button == 1){
                    /*if(position == 0) {
                        return 2;
                    }*/
                    return 5;
                }
                else {
                    /*if(position == 0) {
                        return 4;
                    }*/
                    return 3;
                }
            }

            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
                if(holder.getItemViewType() == 1) {
                    final ViewHolder_access viewHolder = (ViewHolder_access) holder;
                    viewHolder.time.setText(log_list.get(position).get("cas"));
                    viewHolder.id_course_info.setText(log_list.get(position).get("kurzInfoId"));
                    viewHolder.role.setText(log_list.get(position).get("roleId"));
                    viewHolder.id_school_info.setText(log_list.get(position).get("skolaInfoId"));
                    viewHolder.url.setText(log_list.get(position).get("url"));
                    viewHolder.ip.setText(log_list.get(position).get("ip"));
                }
                else if(holder.getItemViewType() == 3){
                    final ViewHolder_actions viewHolder = (ViewHolder_actions) holder;
                    viewHolder.time.setText(log_list.get(position).get("cas"));
                    viewHolder.action.setText(log_list.get(position).get("akce"));
                }
                else if(holder.getItemViewType() == 5){
                    final ViewHolder_onlinetest viewHolder = (ViewHolder_onlinetest) holder;
                    viewHolder.time.setText(log_list.get(position).get("cas"));
                    viewHolder.test.setText(log_list.get(position).get("test"));
                    viewHolder.questionNumber.setText(log_list.get(position).get("otazkaNo"));
                    viewHolder.responseNumber.setText(log_list.get(position).get("odpovedNo"));
                    viewHolder.idTest.setText(log_list.get(position).get("testId"));
                    viewHolder.idQuestion.setText(log_list.get(position).get("otazkaId"));
                    viewHolder.idResponse.setText(log_list.get(position).get("odpovedId"));
                    viewHolder.generatedTest.setText(log_list.get(position).get("generatedTestId"));
                    viewHolder.checked.setText(log_list.get(position).get("checked"));
                    viewHolder.changes.setText(log_list.get(position).get("odpovedPocetVybrani"));
                    viewHolder.question_time.setText(log_list.get(position).get("otazkaCas"));
                    viewHolder.views.setText(log_list.get(position).get("otazkaPocetZobrazeni"));
                    viewHolder.category.setText(log_list.get(position).get("kategorie"));
                }
            }

            @Override
            public int getItemCount() {
                return log_list.size();
            }
        };

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        setComponents();

        return mainView;
    }

    private void setComponents() {
        head0.setVisibility(View.GONE);
        head1.setVisibility(View.GONE);
        head2.setVisibility(View.GONE);
        login = (EditText) mainView.findViewById(R.id.log_login);
        sinceDate = new DatePickerDialogOnButton((Button) mainView.findViewById(R.id.log_since_date));
        sinceTime = new TimePickerDialogOnButton((Button) mainView.findViewById(R.id.log_since_time));
        toDate = new DatePickerDialogOnButton((Button) mainView.findViewById(R.id.log_to_date));
        toTime = new TimePickerDialogOnButton((Button) mainView.findViewById(R.id.log_to_time));
        show_log_access = (Button) mainView.findViewById(R.id.log_show_log_access);
        show_log_online_tests = (Button) mainView.findViewById(R.id.log_show_log_online_tests);
        show_log_all_actions = (Button) mainView.findViewById(R.id.log_show_log_all_actions);

        show_log_access.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pressed_button = 0;
                head0.setVisibility(View.VISIBLE);
                head1.setVisibility(View.GONE);
                head2.setVisibility(View.GONE);
                log_access(sinceDate.getDate(sinceTime), toDate.getDate(toTime), 0);
            }
        });
        show_log_online_tests.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pressed_button = 1;
                head0.setVisibility(View.GONE);
                head1.setVisibility(View.VISIBLE);
                head2.setVisibility(View.GONE);
                log_access(sinceDate.getDate(sinceTime), toDate.getDate(toTime), 1);
            }
        });
        show_log_all_actions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pressed_button = 2;
                head0.setVisibility(View.GONE);
                head1.setVisibility(View.GONE);
                head2.setVisibility(View.VISIBLE);
                log_access(sinceDate.getDate(sinceTime), toDate.getDate(toTime), 2);
            }
        });

    }

    private void log_access(final String timeSince, final String timeTo, final int type) {
        if(login.getText().length() == 0){
            SnackbarManager.show(Snackbar.with(getActivity()).text(R.string.login_is_mandatory));
            return;
        }
        swipeLayout.post(new Runnable() {
            @Override
            public void run() {
                nothinglayout.setVisibility(View.GONE);
                swipeLayout.setRefreshing(true);
                log_list.clear();
                adapter.notifyDataSetChanged();
                data_into_log_list(timeSince, timeTo, type);
            }
        });
    }

    private void data_into_log_list(final String timeSince, final String timeTo, final int type) {
        Response arrayResponse = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                if (response.length() > 0)
                {
                    if (type == 0)
                        data_into_log_list_access(response.getString("IdUzivatel"), timeSince, timeTo);
                    else if (type == 1)
                        data_into_log_list_onlinetest(response.getString("IdUzivatel"), timeSince, timeTo);
                    else
                        data_into_log_list_actions(response.getString("IdUzivatel"), timeSince, timeTo);
                }
                else{
                    Log.d("Chyba", "logs data_into_log_list");
                    nothinglayout.setVisibility(View.VISIBLE);
                }
                swipeLayout.setRefreshing(false);
                adapter.notifyDataSetChanged();
            }
        };
        Request.get(URL.User.GetUserIdBySchoolsLogin(User.schoolID, login.getText().toString()), arrayResponse);

    }


    private void data_into_log_list_access(String idUzivatel, String timeSince, String timeTo) {
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                if (response.length() > 0)
                {
                    for (int j = 0; j < response.length(); j++)
                    {
                        log_list.add(put_data_access(
                                response.getJSONObject(j).getString("UserId"),
                                EnumLogika.role(response.getJSONObject(j).getString("RoleId")),
                                response.getJSONObject(j).getString("SkolaInfoId"),
                                response.getJSONObject(j).getString("KurzInfoId"),
                                TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("Cas")),
                                response.getJSONObject(j).getString("URL"),
                                response.getJSONObject(j).getString("IP"),
                                response.getJSONObject(j).getString("OS"),
                                response.getJSONObject(j).getString("Flag")));
                    }
                }
                else{
                    Log.d("Chyba", "logs data_into_log_list");
                    nothinglayout.setVisibility(View.VISIBLE);
                }
                swipeLayout.setRefreshing(false);
                adapter.notifyDataSetChanged();
            }
        };
        Request.get(URL.User.getlogaccessbyiduzivatel(idUzivatel, timeSince, timeTo), arrayResponse);
    }

    private Map<String, String> put_data_access(String userId, String roleId, String skolaInfoId, String kurzInfoId, String cas, String url, String ip, String os, String flag) {
        HashMap<String, String> item = new HashMap();

        item.put("userId", userId);
        item.put("roleId", roleId);
        item.put("skolaInfoId", skolaInfoId);
        item.put("kurzInfoId", kurzInfoId);
        item.put("cas", cas);
        item.put("url", url);
        item.put("ip", ip);
        item.put("os", os);
        item.put("flag", flag);

        return item;
    }


    private void data_into_log_list_actions(String idUzivatel, String timeSince, String timeTo) {
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                if (response.length() > 0)
                {
                    for (int j = 0; j < response.length(); j++)
                    {
                        log_list.add(put_data_actions(
                                response.getJSONObject(j).getString("UserId"),
                                TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("Cas")),
                                response.getJSONObject(j).getString("Akce")));
                    }
                }
                else{
                    Log.d("Chyba", "logs data_into_log_list");
                    nothinglayout.setVisibility(View.VISIBLE);
                }
                swipeLayout.setRefreshing(false);
                adapter.notifyDataSetChanged();
            }
        };
        Request.get(URL.User.getlogactionsbyiduzivatel(idUzivatel, timeSince, timeTo), arrayResponse);
    }

    private Map<String, String> put_data_actions(String userId, String cas, String akce) {
        HashMap<String, String> item = new HashMap();

        item.put("userId", userId);
        item.put("cas", cas);
        item.put("akce", akce);

        return item;

    }


    private void data_into_log_list_onlinetest(String idUzivatel, String timeSince, String timeTo) {
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                if (response.length() > 0)
                {
                    for (int j = 0; j < response.length(); j++)
                    {
                        log_list.add(data_into_log_list_onlinetest(
                                response.getJSONObject(j).getString("UserId"),
                                TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("Cas")),
                                response.getJSONObject(j).getString("TestId"),
                                response.getJSONObject(j).getString("OtazkaId"),
                                response.getJSONObject(j).getString("OdpovedId"),
                                response.getJSONObject(j).getString("Test"),
                                response.getJSONObject(j).getString("OtazkaNo"),
                                response.getJSONObject(j).getString("OdpovedNo"),
                                response.getJSONObject(j).getString("GeneratedTestId"),
                                response.getJSONObject(j).getString("Checked"),
                                response.getJSONObject(j).getString("Kategorie"),
                                response.getJSONObject(j).getString("OdpovedPocetVybrani"),
                                response.getJSONObject(j).getString("OtazkaPocetZobrazeni"),
                                response.getJSONObject(j).getString("OtazkaCas")));
                    }
                }
                else{
                    Log.d("Chyba", "logs data_into_log_list");
                    nothinglayout.setVisibility(View.VISIBLE);
                }
                swipeLayout.setRefreshing(false);
                adapter.notifyDataSetChanged();
            }
        };
        Request.get(URL.User.getlogonlinetestbyiduzivatel(idUzivatel, timeSince, timeTo), arrayResponse);
    }

    private Map<String, String> data_into_log_list_onlinetest(String userId, String cas, String testId, String otazkaId, String odpovedId,
                                                              String test, String otazkaNo, String odpovedNo, String generatedTestId, String checked,
                                                              String kategorie, String odpovedPocetVybrani, String otazkaPocetZobrazeni, String otazkaCas) {

        HashMap<String, String> item = new HashMap();

        item.put("userId", userId);
        item.put("cas", cas);
        item.put("testId", testId);
        item.put("otazkaId", otazkaId);
        item.put("odpovedId", odpovedId);
        item.put("test", test);
        item.put("otazkaNo", otazkaNo);
        item.put("odpovedNo", odpovedNo);
        item.put("generatedTestId", generatedTestId);
        item.put("checked", checked);
        item.put("kategorie", kategorie);
        item.put("odpovedPocetVybrani", odpovedPocetVybrani);
        item.put("otazkaPocetZobrazeni", otazkaPocetZobrazeni);
        item.put("otazkaCas", otazkaCas);

        return item;

    }
}
