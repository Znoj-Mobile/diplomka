package cz.vsb.elogika.ui.fragments.garant.question;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.dataTypes.Category;
import cz.vsb.elogika.dataTypes.Chapter;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.LogicParser;
import cz.vsb.elogika.utils.SpecialCharDialogButton;
import cz.vsb.elogika.utils.TimeLogika;
import cz.vsb.elogika.utils.TimePickerDialogButton_HMS;

public class Fragment_Question_New extends Fragment {

    boolean editing = false;
    private ArrayAdapter<Chapter> chapteradapter;
    private ArrayList<Chapter> chapter_list;
    private int selectedChapterPosition;
    private ArrayAdapter<Category> categoryadapter;
    private ArrayList<Category> category_list;
    private int selectedCategoryPosition;
    private int zarazeni;


    public Fragment setSpinnersData(ArrayAdapter<Chapter> chapteradapter, ArrayList<Chapter> chapter_list, int selectedChapterPosition, ArrayAdapter<Category> categoryadapter, ArrayList<Category> category_list, int selectedCategoryPosition, int zarazeni){

        this.editing = false;
        this.chapteradapter = chapteradapter;
        this.chapter_list = chapter_list;
        this.selectedChapterPosition = selectedChapterPosition;
        this.categoryadapter = categoryadapter;
        category_list.remove(0);
        this.category_list = category_list;
        this.selectedCategoryPosition = selectedCategoryPosition;
        this.zarazeni = zarazeni;
        return this;
    }


    LinearLayout answersLayout;
    LinearLayout stepsLayout;
    EditText questionText;
    CheckBox questionChecked;
    TimePickerDialogButton_HMS question_time;
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_question_new, container, false);
        Actionbar.addSubcategory(R.string.create_new_question, this);

        questionText = (EditText) rootView.findViewById(R.id.question_text);
        SpecialCharDialogButton specialCharDialogButton = (SpecialCharDialogButton) rootView.findViewById(R.id.question_add_special_char);
        specialCharDialogButton.linkWithEditText(questionText);


        questionChecked = (CheckBox) rootView.findViewById(R.id.question_checked_checkbox);
        question_time = (TimePickerDialogButton_HMS) rootView.findViewById(R.id.question_time);


        answersLayout = (LinearLayout) rootView.findViewById(R.id.question_answers_layout);
        NumberPicker questionsCountCouner = (NumberPicker) rootView.findViewById(R.id.question_question_count_picker);
        questionsCountCouner.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

                while (answersLayout.getChildCount() < newVal) //přidává odpovědi
                    addAnswer(inflater);
                while (answersLayout.getChildCount() > newVal) //maže odpovědi
                    answersLayout.removeViewAt(answersLayout.getChildCount() - 1);
            }
        });
        questionsCountCouner.setMinValue(1);
        questionsCountCouner.setMaxValue(999);
        questionsCountCouner.setWrapSelectorWheel(false);

        stepsLayout = (LinearLayout) rootView.findViewById(R.id.question_steps);

        initFAB(rootView);

        if(!editing) addAnswer(inflater);
        return rootView;
    }

    private void initFAB(View rootView) {
        Button fab = (Button) rootView.findViewById(R.id.question_add_new_button);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                insertquestion();
            }
        });
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initSpinners();
    }

    Spinner question_kapitola_spinner;
    Spinner question_kategorie_spinner;
    Spinner question_zarazeni_spinner;

    private void initSpinners() {
        question_kapitola_spinner = (Spinner) getActivity().findViewById(R.id.question_kapitola_spinner);
        question_kapitola_spinner.setAdapter(chapteradapter);
        question_kapitola_spinner.setSelection(selectedChapterPosition);

        question_kategorie_spinner = (Spinner) getActivity().findViewById(R.id.question_kategorie_spinner);
        question_kategorie_spinner.setAdapter(categoryadapter);
        question_kategorie_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getSteps(((Category) question_kategorie_spinner.getSelectedItem()).IDKategorie);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        question_zarazeni_spinner = (Spinner) getActivity().findViewById(R.id.question_zarazeni_spinner);
        List<String> list = new ArrayList<String>();
        list.add("Hlavní");
        list.add("Cvičný(á)");
        list.add("Ukázkový(á)");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                R.layout.spinner_item, list);
        dataAdapter.setDropDownViewResource(R.layout.spinner_item);
        question_zarazeni_spinner.setAdapter(dataAdapter);
    }


    private void addAnswer(LayoutInflater inflater){
        LinearLayout answers = (LinearLayout) inflater.inflate(R.layout.fragment_question_new_answer, answersLayout);
        final View answer = answers.getChildAt(answers.getChildCount() - 1);
        CheckBox spravna = (CheckBox) answer.findViewById(R.id.answer_correct_checkbox);
        EditText answer_text = (EditText) answer.findViewById(R.id.answer_text);
        //int poradi = answersLayout.getChildCount();
        EditText explanation_text = (EditText) answer.findViewById(R.id.explanation_text);
        SpecialCharDialogButton specialCharDialogButton = (SpecialCharDialogButton) answer.findViewById(R.id.question_add_special_char);
        specialCharDialogButton.linkWithEditText(answer_text);

        spravna.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) answer.setBackgroundColor(Color.argb(60, 100, 150, 100));
                else answer.setBackgroundColor(Color.argb(60, 150, 100, 100));
            }
        });
    }

    private void getSteps(int IDKategorie){
        stepsLayout.removeAllViews();
        stepsLayout.addView(new ProgressBar(getActivity()));
        ArrayResponse r = new ArrayResponse() {
            @Override
            public void onResponse(JSONArray response) throws JSONException {
                stepsLayout.removeAllViews();
                if (response.length() > 0)
                {
                    for (int j = 0; j < response.length(); j++)
                    {
                        CheckBox c = new CheckBox(getActivity());
                        c.setText((j+1) + ". " + response.getJSONObject(j).getString("Nazev"));
                        c.setTag(response.getJSONObject(j));
                        stepsLayout.addView(c);
                    }
                }
            }
        };
        Request.get(URL.Step.GetSteps(IDKategorie), r);
    }

    private void insertquestion(){
        Response r = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException {
                Log.d("insertquestion", response.toString());
                if (response.getBoolean("Success"))
                    insertAnswers(response.getInt("IdQuestion"));
                else
                    Toast.makeText(getActivity(), R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        };

        JSONObject parameters = new JSONObject();
        try {
            JSONObject question = new JSONObject();
            //    question.put("IDOtazka");
            //    question.put("IDVygenerovanyTest");
            //    question.put("TypOdpovedi", );
            //    question.put("IDVygenerovanaOtazka");
                question.put("IDZarazeni", question_zarazeni_spinner.getSelectedItemId()+1);
                question.put("IDKategorie", ((Category) question_kategorie_spinner.getSelectedItem()).IDKategorie);
                question.put("IDUzivatel", User.id);
                question.put("IDRole", User.roleID);
                question.put("Zadani", LogicParser.replaceSpecialChars(String.valueOf(questionText.getText())));
                question.put("PredpCasPrecteni",question_time.getTimeInSeconds());
                question.put("Smazana", false);
            //    question.put("IDBlok");
                question.put("DatumEvidence", TimeLogika.currentServerTime());
                question.put("IDUzivatelKontrola", User.id);                        //co udělat když neni vyplněno ?
                question.put("DatumKontrola", TimeLogika.currentServerTime());      //co udělat když neni vyplněno ?
                question.put("Prekontrolovano", questionChecked.isChecked());
            //    question.put("Vlastnik");
                question.put("KategorieNazev", ((Category) question_kategorie_spinner.getSelectedItem()).Nazev);
                question.put("KapitolaNazev", ((Chapter) question_kapitola_spinner.getSelectedItem()).Nazev);
            //    question.put("Variant");

                JSONArray Images = new JSONArray();
                question.put("Images", Images);
            parameters.put("question", question);

            JSONArray stepIds = new JSONArray();

            for (int i = 0; i< stepsLayout.getChildCount(); i++)
            {
                if (((CheckBox)stepsLayout.getChildAt(i)).isChecked())
                    stepIds.put(((JSONObject) stepsLayout.getChildAt(i).getTag()).getInt("IDKrok"));
            }
            parameters.put("stepIds", stepIds);

            JSONArray images = new JSONArray(); //TODO zvolene obrazky
            parameters.put("images", images);


            Log.d("insertquestion", parameters.toString(3));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Request.post(URL.Question.insertquestion(), parameters, r);

    }

    private void insertAnswers(int questionId){
        Response r = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException {
                Log.d("insertAnswers", response.toString());
                if (response.getString("Success").equals("true")){
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.question) + " " + getActivity().getString(R.string.question_was_inserted)));
                    getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, new Fragment_Question()).commit();
                }
                else {
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.question) + " " + getActivity().getString(R.string.question_was_not_inserted)));
                }
            }
        };

        JSONObject parameters = new JSONObject();
        try {
            JSONArray answerList = new JSONArray();

            for (int i = 0; i< answersLayout.getChildCount(); i++) //jednotlivé odpovědi
            {
                JSONObject answer = new JSONObject();
                answer.put("IDOdpoved", i+1);
                answer.put("Spravna", ((CheckBox) answersLayout.getChildAt(i).findViewById(R.id.answer_correct_checkbox)).isChecked());
                answer.put("TextOdpovedi", ((EditText) answersLayout.getChildAt(i).findViewById(R.id.answer_text)).getText());
                answer.put("Vysvetleni", ((EditText) answersLayout.getChildAt(i).findViewById(R.id.explanation_text)).getText());
                answer.put("PredpCasReseni", ((TimePickerDialogButton_HMS) answersLayout.getChildAt(i).findViewById(R.id.question_time)).getTimeInSeconds());
                answer.put("Smazana", false);
                //answer.put("OdpovedSpravna", ((EditText) answersLayout.getChildAt(i).findViewById(R.id.answer_text)).getText() + " - OK");
                //JSONArray Images = new JSONArray();
                //answer.put("Images", Images);
                answerList.put(answer);
            }

            parameters.put("answerList", answerList);
            parameters.put("questionId", questionId);
            JSONObject images = new JSONObject();
            JSONArray imagesField = new JSONArray();

            //todo
            for (int i = 0; i< answersLayout.getChildCount(); i++) //jednotlivé odpovědi
            {
                images.put(String.valueOf(i+1), imagesField);
            }
            parameters.put("images", images);


            Log.d("insertquestion", parameters.toString(3));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Request.post(URL.Answer.Insert(), parameters, r);

    }

}
