package cz.vsb.elogika.ui.fragments.garant.courseConditions;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Map;

import cz.vsb.elogika.R;
import cz.vsb.elogika.common.view.SlidingTabLayout;
import cz.vsb.elogika.ui.fragments.garant.emails.Fragment_email_message;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.Logging;

public class Fragment_cc_listing_students extends Fragment {
    private View rootView;

    static Fragment_cc_listing_info fragment_cc_listing_info;
    static Fragment_cc_listing_logged fragment_cc_listing_logged;
    static Fragment_email_message fragment_email_message;
    String idSkupinaAktivit;
    String idAktivita;
    static String minPoints = "0";
    private static ViewPager mViewPager;
    public static String idTermin;

    public static Fragment_cc_listing_students instance = null;

    public static ArrayList<Map<String, String>> logged = new ArrayList<>();

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        instance = this;
        rootView = inflater.inflate(R.layout.tabs_container, container, false);
        try{
            idSkupinaAktivit = getArguments().getString("idSkupinaAktivit");
        } catch(Exception e){        }
        try{
            idAktivita = getArguments().getString("idAktivita");
        } catch(Exception e){        }

        idTermin = getArguments().getString("idTermin");
        Logging.logAccess("/Pages/Other/StudentTermin.aspx?ID=" + idTermin);

        Actionbar.addSubcategory(R.string.listing_logged_students, this);
        //setActionBar();
        setViewPager();

        return rootView;
    }

    private void setViewPager() {
        mViewPager = (ViewPager) rootView.findViewById(R.id.viewpager);
        mViewPager.setAdapter(new FragmentStatePagerAdapter(getFragmentManager()) {
                                  String[] tabNames = new String[] {
                                          getString(R.string.informations),
                                          getString(R.string.logged_students),
                                          getString(R.string.write_message)};

                                  Bundle param = new Bundle();

                                  @Override
                                  public int getCount() {
                                      return tabNames.length;
                                  }
                                  @Override
                                  public CharSequence getPageTitle(int position) {
                                      return tabNames[position];
                                  }


                                  @Override
                                  public Fragment getItem(int position) {

                                      switch (position)
                                      {
                                          case 0:
                                              param.putString("nazevTyp", getArguments().getString("nazevTyp"));
                                              param.putString("nazev", getArguments().getString("nazev"));
                                              param.putString("aktivniOD", getArguments().getString("aktivniOD"));
                                              param.putString("aktivniDO", getArguments().getString("aktivniDO"));

                                              param.putString("idSkupinaAktivit", getArguments().getString("idSkupinaAktivit"));
                                              param.putString("idAktivita", getArguments().getString("idAktivita"));
                                              param.putString("idTermin", getArguments().getString("idTermin"));
                                              param.putString("idSkupinaAktivit", idSkupinaAktivit);
                                              param.putString("tab", getArguments().getString("tab"));


                                              fragment_cc_listing_info = new Fragment_cc_listing_info();
                                              fragment_cc_listing_info.setArguments(param);
                                              return fragment_cc_listing_info;
                                          case 1:
                                              fragment_cc_listing_logged = new Fragment_cc_listing_logged();
                                              return fragment_cc_listing_logged;
                                          case 2:
                                              param.putBoolean("logged", true);
                                              fragment_email_message = new Fragment_email_message();
                                              fragment_email_message.setArguments(param);
                                              fragment_email_message.set_logged();
                                              return fragment_email_message;
                                          default : return null;
                                      }
                                  }
                              }


        );
        SlidingTabLayout mSlidingTabLayout = (SlidingTabLayout) rootView.findViewById(R.id.sliding_tabs);
        mSlidingTabLayout.setViewPager(mViewPager);
    }
/*
    private void setActionBar() {
        final ActionBar ab = getActivity().getActionBar();
        if (ab != null) {
            ab.setDisplayShowHomeEnabled(false);
            ab.setDisplayShowTitleEnabled(false);
        }

        LayoutInflater mInflater = LayoutInflater.from(getActivity());
        final View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        TextView mTitleTextView = (TextView) mCustomView.findViewById(R.id.title_text);
        mTitleTextView.setText(getResources().getString(R.string.listing_logged_students));

        ImageButton imageButton = (ImageButton) mCustomView.findViewById(R.id.imageButton);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //zpet
                Bundle bundle = new Bundle();
                //arguenty potrebne pro dalsi navraty...
                bundle.putString("idSkupinaAktivit", idSkupinaAktivit);
                bundle.putString("tab", getArguments().getString("tab"));

                Fragment_cc_term fragment_cc_term = new Fragment_cc_term();
                fragment_cc_term.setArguments(bundle);
                getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment_cc_term).commit();

                if (ab != null) {
                    ab.setDisplayShowCustomEnabled(false);
                    ab.setDisplayShowHomeEnabled(true);
                    ab.setDisplayShowTitleEnabled(true);
                }
            }
        });

        if (ab != null) {
            ab.setCustomView(mCustomView);
            ab.setDisplayShowCustomEnabled(true);
        }
    }
*/
    public static void setMinPoints(String minPoints) {
        Fragment_cc_listing_students.minPoints = minPoints;
        fragment_cc_listing_logged.update();
        //prejeti na vypsane studenty
        mViewPager.setCurrentItem(1);
    }

    public static void go_to_history() {
        Bundle bundle = new Bundle();
        //zpet na patricnou zalozku
        bundle.putString("idSkupinaAktivit", instance.getArguments().getString("idSkupinaAktivit"));
        bundle.putString("idAktivita", instance.getArguments().getString("idAktivita"));
        bundle.putString("nazevTyp", instance.getArguments().getString("nazevTyp"));
        bundle.putString("nazev", instance.getArguments().getString("nazev"));
        bundle.putString("aktivniOD", instance.getArguments().getString("aktivniOD"));
        bundle.putString("aktivniDO", instance.getArguments().getString("aktivniDO"));
        bundle.putString("idTermin", instance.getArguments().getString("idTermin"));
        bundle.putString("tab", instance.getArguments().getString("tab"));
        Fragment_cc_listing_history fragment_cc_listing_history = new Fragment_cc_listing_history();
        fragment_cc_listing_history.setArguments(bundle);

        instance.getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment_cc_listing_history).commit();
    }
}
