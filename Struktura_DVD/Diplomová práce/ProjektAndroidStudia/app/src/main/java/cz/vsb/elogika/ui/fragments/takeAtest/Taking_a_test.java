package cz.vsb.elogika.ui.fragments.takeAtest;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.vsb.elogika.R;
import cz.vsb.elogika.common.view.SlidingTabLayout;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.ui.BaseActivity;
import cz.vsb.elogika.utils.Logging;
import cz.vsb.elogika.utils.TimeLogika;

/**
 * Třída pro funkčnost:
 *  +Vypracovat Test
 *  +Vypracovaný Test
 *  +Offline Test
 *  +Cvičný test
 *
 */
public class Taking_a_test extends AppCompatActivity {

    public static Taking_a_test getCurrentInstance;
    public static boolean[][] answers ;   //pole zaškrtlých odpovedi uživatelem
    public static String[] writtenAnswers;//pole odpovědí napsaných uživatelem
    public static int[][] answersClicks;  //pole počtu kliknutí uživatele na otazku
    public static long[] timeSpent;       //pole stráveného času nad jednotlivými otazkami
    public static int[] viewCount;        //pole kolikrat byla zobrazena otazka
    public static JSONArray log = new JSONArray();
    public static Bitmap[][] questionImages;
    public static Bitmap[][][] answerImages;
    //následujících 6 proměných je třeba incializovat a naplnit a teprvé poté lze spustit aktivitu
    public static JSONArray testInfo;  //info o testu z gettest
    public static JSONObject[] jsonQuestions; //pole otazek
    public static JSONObject[][] jsonQuestionOptions; //pole odpovedi na jednotlivé otazky
    public static String startAt;
    public static int generatedTestId;
    public static int userGeneratedTestId = -1;
    public static int timeMinutes;
    public static String testName;
    public static int mode;
    public static int TEST = 1;
    public static int TESTDRAWN = 2;
    //TEST DRAWN
    public static JSONArray testDrawnResponse;
    public static JSONArray[] testDrawnQuestionAnswers;
    public static String DateSendTo;
    public static long millisUntilFinished = 0;
    static int checkCounter = 3;
    private static long lastTime = 0;     //pomocná proměnná pro počítaní času
    private static int lastTab = 0;       //pomocná proměnná pro počítaní času
    private static MyCount timerCount;

    public static boolean editable = true;
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v13.app.FragmentStatePagerAdapter}.
     */
    SectionsPagerAdapter mSectionsPagerAdapter;
    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;
    int fragmentsCount;
    TextView countdowntext;
    Animation slidedown, slideup;
    LinearLayout uppertimelayout;
    android.text.format.Time time = new android.text.format.Time();

    /**
     * Inicializace UI
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        this.getCurrentInstance = this;
        userGeneratedTestId = -1;
        slidedown = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slidedown);
        slideup = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slideup);
        if(mode != TESTDRAWN){
            Logging.logAccess("/Pages/Other/GeneratedTest.aspx");
            slidedown.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    uppertimelayout.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animation animation) {

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        slideup.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                uppertimelayout.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        }
        else
        {
            editable = false;
        }

        jsonParse();


        if(answers == null)
        {   // vytvoření adekvátních polí podle otázek
            answers = new boolean[jsonQuestions.length][];
            writtenAnswers = new String[jsonQuestions.length];
            timeSpent = new long[jsonQuestions.length];
            answersClicks = new int[jsonQuestions.length][];
            viewCount = new int[jsonQuestions.length];
            for(int i = 0; i<answers.length; i++)
            {
                answers[i] = new boolean[jsonQuestionOptions[i].length];
                answersClicks[i] = new int[jsonQuestionOptions[i].length];
                for (int j = 0 ; j<answersClicks[i].length; j++)
                {
                    answersClicks[i][j] = 0;
                }
            }
        Log.d("elogika", "vytvořeno pole pro odpovědi");
            time.setToNow();
            lastTime = time.toMillis(false);
        }
        fragmentsCount = jsonQuestions.length+1;

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager());
        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setPageTransformer(false, new FadePageTransformer());
        mViewPager.setAdapter(mSectionsPagerAdapter);

        final SlidingTabLayout mSlidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);

        mSlidingTabLayout.setViewPager(mViewPager);
        mSlidingTabLayout.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mViewPager.setCurrentItem(position); //vybrání otazky v horním panelu
                try{
                    if(position == fragmentsCount-1) {
                        uppertimelayout.startAnimation(slideup);
                    }
                    //výpočet stráveného času na jednotlivých otázkách
                    if(lastTab == fragmentsCount-1)
                    {
                        uppertimelayout.startAnimation(slidedown);
                        time.setToNow();
                        lastTime = time.toMillis(false);
                        lastTab = position;
                    }
                    else
                    {
                        viewCount[lastTab]++;
                        Log.d("elogika","otazka " + lastTab + " zobrazena " + viewCount[lastTab] +"x");
                        time.setToNow();
                        timeSpent[lastTab] = timeSpent[lastTab]+((time.toMillis(false)-lastTime)/1000);
                        lastTime = time.toMillis(false);
                        Log.d("TIME TEST", lastTab +" má čas "+ Long.toString(timeSpent[lastTab]));
                        lastTab = position;
                    }
                } catch (NullPointerException e){}
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        final android.support.v7.app.ActionBar supportActionBar = getSupportActionBar();
        supportActionBar.setDisplayShowHomeEnabled(true);
        supportActionBar.setIcon(R.drawable.ic_launcher);
        supportActionBar.setTitle(User.name + " " + User.surname);
        supportActionBar.setSubtitle(testName);


        uppertimelayout = (LinearLayout) findViewById(R.id.taking_a_test_timeinfo);
        if(mode == TESTDRAWN)
            uppertimelayout.setVisibility(View.GONE);
        else{
        ((TextView)findViewById(R.id.taking_a_test_timefrom)).setText(getString(R.string.date_from)+ " " + TimeLogika.getFormatedTime(Taking_a_test.startAt));
        ((TextView) findViewById(R.id.taking_a_test_timeto)).setText(getString(R.string.date_to)+ " " + TimeLogika.getFormatedTime(TimeLogika.timePlusMinutes(Taking_a_test.startAt, Taking_a_test.timeMinutes)));//Taking_a_test.timeMinutes);
        countdowntext = (TextView) findViewById(R.id.taking_a_test_timecurrent);
        countdowntext.setText(TimeLogika.getTimeFromMilis(Taking_a_test.millisUntilFinished));

        if (timerCount == null){
        timerCount = new MyCount(timeMinutes * 60*1000, 1000);
        timerCount.start();

        if(!User.isOnline)
        BaseActivity.getDB().deleteTest(generatedTestId);
        Log.d("elogika test je připraven k vyhotovení" , "na test je : " + timeMinutes + " minut");
        }}

    }

    @Override
    public void onPause(){
        super.onPause();
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
      //  editor.putBoolean("isSet", isSet);

        // serialize the object
    /*    try {
            ByteArrayOutputStream bo = new ByteArrayOutputStream();
            ObjectOutputStream so = new ObjectOutputStream(bo);
            so.writeObject(this);
            so.flush();
            serializedObject = bo.toString();

        } catch (Exception e) {
            System.out.println(e);
        }
    }
        editor.
        editor.commit(); */

    }

    @Override
    public void onResume(){
        super.onResume();

      /*  SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);

        String chosen_year = sharedPref.getInt("chosen_year", chosen_year);

        try {
            byte b[] = serializedObject.getBytes();
            ByteArrayInputStream bi = new ByteArrayInputStream(b);
            ObjectInputStream si = new ObjectInputStream(bi);
            TestData obj = (TestData) si.readObject();
        } catch (Exception e) {
            System.out.println(e);
        }*/

    }

    /**
     * Metoda pro zpracování přijatých json dat ze serveru,
     * data se připraví do polí odkuď je potom UI čerpá a rychle zobrazuje
     */
    private void jsonParse() {

        try {
            generatedTestId = testInfo.getJSONObject(0).getInt("IDVygenerovanyTest");
            Taking_a_test.answerImages = new Bitmap[jsonQuestions.length][][];
            for (int poradi = 0; poradi<jsonQuestions.length;poradi++)
            {
                Taking_a_test.answerImages[poradi] = new Bitmap[jsonQuestionOptions[poradi].length][];
                for (int poradiodpovedi = 0; poradiodpovedi<jsonQuestionOptions[poradi].length;poradiodpovedi++)
                {
                    Taking_a_test.answerImages[poradi][poradiodpovedi] = new Bitmap[jsonQuestionOptions[poradi][poradiodpovedi].getJSONArray("Images").length()];
                    for(int poradiobrazkuvodpovedi = 0; poradiobrazkuvodpovedi<jsonQuestionOptions[poradi][poradiodpovedi].getJSONArray("Images").length();poradiobrazkuvodpovedi++)
                    {
                        String s =jsonQuestionOptions[poradi][poradiodpovedi].getJSONArray("Images").getJSONObject(poradiobrazkuvodpovedi).getString("Soubor");
                        byte[] decodedString = Base64.decode(s, Base64.DEFAULT);
                        answerImages[poradi][poradiodpovedi][poradiobrazkuvodpovedi] = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                    }
                }
            }
            //parse obrazku k otazkam
            questionImages = new Bitmap[jsonQuestions.length][];
            for (int i = 0; i<jsonQuestions.length;i++)
            {
                questionImages[i] = new Bitmap[jsonQuestions[i].getJSONObject("Question").getJSONArray("Images").length()];
                for (int j = 0; j<jsonQuestions[i].getJSONObject("Question").getJSONArray("Images").length();j++)
                {

                    String s = jsonQuestions[i]
                            .getJSONObject("Question")
                            .getJSONArray("Images")
                            .getJSONObject(j)
                            .getString("Soubor");

                    byte[] decodedString = Base64.decode(s, Base64.DEFAULT);
                    questionImages[i][j] = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /**
     * Metoda pro vyhodnocení testu
     */
    public void evaluate()
    {
       if(timerCount != null) timerCount.cancel();
        timerCount = null;
        taking_a_test_finishDialog = new Taking_a_test_FinishDialog(this) {
            @Override
            public void onRetryClicked() {
                saveGenerateTest(true);
            }
        };
        saveGenerateTest(true);
    }
    Taking_a_test_FinishDialog taking_a_test_finishDialog;

    /**
     * Tato metoda uložení odpovědi celého testu na server nebo do databáze jedná-li se o offline test
     */
    public void saveGenerateTest(final boolean saveStatistics) {
        Response response = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                Log.d("savegeneratetest response", response.toString());

                //if(Taking_a_test.userGeneratedTestId != 0) //zakomentovano 27.4.2016
                    Taking_a_test.userGeneratedTestId = (response.getInt("Result"));

                if(saveStatistics){
                    if(Taking_a_test.userGeneratedTestId == 0)
                    {
                        taking_a_test_finishDialog.setServerError();
                    }
                    else
                    {
                        InsertStatOtazkaUzivatel(Taking_a_test.userGeneratedTestId);
                        InsertStatOdpovedUzivatel(Taking_a_test.userGeneratedTestId);
                        addloglist(Taking_a_test.userGeneratedTestId);
                        checkResponses();
                    }
                }
            }

        };

        JSONObject parameters = new JSONObject();
        JSONArray AnsweredQuestions = new JSONArray();
        try
        {
            // parameters
            for(int i = 0; i<jsonQuestions.length;i++){
                JSONObject questionObject = new JSONObject();
                JSONArray Values = new JSONArray();
                boolean odpovedelnaotazku = false;


                if(jsonQuestions[i].getInt("AnswerType") == 2) // tak to bylo předtím: if(jsonQuestionOptions[i].length == 0)
                {
                    odpovedelnaotazku = true;
                    JSONObject value = new JSONObject();
                    value.put("Answer", writtenAnswers[i]);
                    value.put("AnswerId", -1);
                    Values.put(value);
                }
                else
                {
                    for(int j = 0; j<jsonQuestionOptions[i].length;j++){
                        if (answers[i][j]){
                            odpovedelnaotazku = true;
                            JSONObject value = new JSONObject();
                            value.put("Answer", jsonQuestionOptions[i][j].getString("TextOdpovedi"));
                            value.put("AnswerId", jsonQuestionOptions[i][j].getInt("IDOdpoved"));
                            value.put("Deleted", jsonQuestionOptions[i][j].getBoolean("Smazana"));
                            value.put("EstimatedTimeForAnswer", jsonQuestionOptions[i][j].getInt("PredpCasReseni"));
                            value.put("Help", jsonQuestionOptions[i][j].getString("Vysvetleni"));
                            value.put("Images", new JSONArray());
                            value.put("IsCorrect", jsonQuestionOptions[i][j].getBoolean("Spravna"));
                            value.put("TextAnswerCorrect", jsonQuestionOptions[i][j].getString("OdpovedSpravna"));
                            Values.put(value);
                        }
                    }
                }
                if(odpovedelnaotazku){
                    //otazka
                    JSONObject key = new JSONObject();
                    key.put("AuthorId", jsonQuestions[i].getJSONObject("Question").getInt("IDUzivatel"));
                    key.put("BlockId", jsonQuestions[i].getInt("IDBlok"));
                    key.put("CategoryId", jsonQuestions[i].getJSONObject("Question").getInt("IDKategorie"));
                    key.put("CategoryName", jsonQuestions[i].getJSONObject("Question").getString("KategorieNazev"));
                    key.put("ChapterName", jsonQuestions[i].getJSONObject("Question").getString("KapitolaNazev"));
                    key.put("Checked", jsonQuestions[i].getJSONObject("Question").getBoolean("Prekontrolovano"));
                    key.put("CheckedAt", jsonQuestions[i].getJSONObject("Question").getString("DatumKontrola"));
                    key.put("CreatedAt", jsonQuestions[i].getJSONObject("Question").getString("DatumEvidence"));
                    key.put("Deleted", jsonQuestions[i].getJSONObject("Question").getBoolean("Smazana"));
                    key.put("EstimatedTimeForQuestion", jsonQuestions[i].getJSONObject("Question").getInt("PredpCasPrecteni"));
                    key.put("GeneratedQuestionId", jsonQuestions[i].getInt("IDVygenerovanaOtazka"));
                    key.put("GeneratedTestId", jsonQuestions[i].getInt("IDVygenerovanyTest"));
                    key.put("Images", new JSONArray());
                    key.put("InclusionId", jsonQuestions[i].getJSONObject("Question").getInt("IDZarazeni")); //Variant //je proměnna co nevim kam dat
                    key.put("IsAuthor", jsonQuestions[i].getJSONObject("Question").getBoolean("Vlastnik"));
                    key.put("Question", jsonQuestions[i].getJSONObject("Question").getString("Zadani"));
                    key.put("QuestionId", jsonQuestions[i].getInt("IDOtazka"));
                    key.put("AnswerType", jsonQuestions[i].getInt("AnswerType")); //tohle by mělo byt spravně
                   // key.put("QuestionType", jsonQuestions[i].getInt("TypOdpovedi")); //tohle ma mario a stejně mu to jde
                    key.put("RoleId", jsonQuestions[i].getJSONObject("Question").getInt("IDRole"));
                    key.put("UserIdWhoChecked", jsonQuestions[i].getJSONObject("Question").getInt("IDUzivatelKontrola"));

                    questionObject.put("Key", key);
                    questionObject.put("Value", Values);
                AnsweredQuestions.put(questionObject);
                }
            }
            parameters.put("AnsweredQuestions",AnsweredQuestions );
            parameters.put("GeneratedTestId",Taking_a_test.generatedTestId );
            parameters.put("UserGeneratedTestId",userGeneratedTestId);
            parameters.put("StudentId", User.id);
            parameters.put("SchoolInfoId", User.schoolInfoID );
        }
        catch (JSONException e) {Log.e("saveGenerateTestKeyValue exception", e.toString());}


        if(User.isOnline)
        {
            Log.w("savegeneratetest request: " , parameters.toString());
            checkCounter = 4;
            Request.post(URL.GeneratedTest.savegenerateTestkeyvalue(), parameters, response);
            checkResponses();
        }
        else
        { //pokud se vypracovava test offline tak se jen uloží
            Log.w("savegeneratetest stored into db: ", parameters.toString());
            BaseActivity.getDB().insertOfflineTestResult(
                    Taking_a_test.generatedTestId,
                    Taking_a_test.DateSendTo,
                    Taking_a_test.testName,
                    parameters,
                    InsertStatOtazkaUzivatel(-1),
                    InsertStatOdpovedUzivatel(-1),
                    addloglist(-1)
            );

            Toast.makeText(getBaseContext(),
                    getString(R.string.test) + " \"" +testName+"\" "+ getString(R.string.will_be_sent_after_login),
                    Toast.LENGTH_LONG).show();

            finish();
        }
    }

    /**
     * metoda která uloží statistiky otázek na server
     *
     * @param IDVygenerovanyTestUzivatel id aktuálně vypracovavaného testu
     * @return vrací kompletní objekt, který se pošle na server jedná-li se o offline test pak se tento objekt uloží do databáze
     */
    private JSONObject InsertStatOtazkaUzivatel(int IDVygenerovanyTestUzivatel)
    {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                Log.d("InsertStatOtazkaUzivatel response", response.toString());
                checkResponses();
            }
        };

        JSONObject parameters = new JSONObject();
        JSONArray params = new JSONArray();
        try
        {
            // vkladam data o otazce
            for(int i = 0; i<jsonQuestions.length;i++){
                JSONObject questionfield = new JSONObject();
                questionfield.put("IDOtazka", jsonQuestions[i].getInt("IDOtazka"));
                questionfield.put("IDStatOtazkaUzivatel", null);
                questionfield.put("PocetZobrazeni",viewCount[i] );
                questionfield.put("StravenyCas",TimeLogika.getTimeFromMilis(timeSpent[i]*1000));

                params.put(questionfield);
            }
            parameters.put("QuestionStats",params);


            if(!User.isOnline)
                return parameters;
            for(int i = 0; i<parameters.getJSONArray("QuestionStats").length();i++)
                parameters.getJSONArray("QuestionStats").getJSONObject(i).put("IDVygenerovanyTestUzivatel", IDVygenerovanyTestUzivatel);
            Log.d("InsertStatOtazkaUzivatel request" , parameters.toString());
        }
        catch (JSONException e) {Log.d("elogika", e.toString());}
            Request.post(URL.OnlineStatistics.InsertStatOtazkaUzivatelList(), parameters, response);
            return null;
    }

    /**
     * metoda která uloží statistiky odpovědí na server
     *
     * @param IDVygenerovanyTestUzivatel id aktuálně vypracovavaného testu
     * @return vrací kompletní objekt, který se pošle na server jedná-li se o offline test pak se tento objekt uloží do databáze
     */
    private JSONObject InsertStatOdpovedUzivatel(int IDVygenerovanyTestUzivatel)
    {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                Log.d("InsertStatOdpovedUzivatel response", response.toString());
                checkResponses();
            }
        };

        JSONObject parameters = new JSONObject();
        JSONArray params = new JSONArray();
        try
        {    // vkladam data o odpovedi
            for(int i = 0; i<jsonQuestionOptions.length;i++){
                for(int j = 0; j<jsonQuestionOptions[i].length;j++){
                    JSONObject questionfield = new JSONObject();
                    questionfield.put("IDOdpoved",jsonQuestionOptions[i][j].getInt("IDOdpoved"));
                    questionfield.put("PocetKliku", answersClicks[i][j]);
                    questionfield.put("IDStatOdpovedUzicatel", null );
                    params.put(questionfield);
                }
            }
            parameters.put("AnswerStats",params);
            if(!User.isOnline)
                return parameters;
            for(int i = 0; i<parameters.getJSONArray("AnswerStats").length();i++)
                parameters.getJSONArray("AnswerStats").getJSONObject(i).put("IDVygenerovanyTestUzivatel", IDVygenerovanyTestUzivatel);
            Log.d("InsertStatOdpovedUzivatel request", parameters.toString());
        }
        catch (JSONException e) {Log.d("elogika", e.toString());}
        Request.post(URL.OnlineStatistics.InsertStatOdpovedUzivatelList(), parameters, response);
        return null;
    }

    /**
     * metoda která přidá kompletní statistiky testu na server
     *
     * @param userGeneratedTestId id aktuálně vypracovavaného testu
     * @return vrací kompletní objekt, který se pošle na server jedná-li se o offline test pak se tento objekt uloží do databáze
     */
    private JSONObject addloglist(int userGeneratedTestId)
    {
        JSONObject parameters = new JSONObject();
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException {
                Log.d("addloglist response", response.toString());
                checkResponses();
            }
        };
        try {
            parameters.put("TestLogs", log);
            if(!User.isOnline)
            return parameters;
            for(int i = 0; i<parameters.getJSONArray("TestLogs").length();i++)
            parameters.getJSONArray("TestLogs").getJSONObject(i).put("UserGeneratedTestId", userGeneratedTestId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.w("addloglist request: " , parameters.toString());
        Request.post(URL.CourseConditions.addloglist(), parameters, response);
        return  null;
    }

    /**
     * Při zmáčknutí tlačítka zpět jsou přepínany otázky směrem k začátku
     * na začátku je výzva pro ukončení testu
     */
    @Override
    public void onBackPressed()
    {
        if(mViewPager.getCurrentItem()!=0){
            mViewPager.setCurrentItem(mViewPager.getCurrentItem()-1);
        }

        else{
            if(mode == TESTDRAWN){
                finish();
            }
        else
        new AlertDialog.Builder(this)
                .setTitle(R.string.end_test)
                .setMessage(R.string.end_test_sure_no_evaluate)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        finish();
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                     //  hide
                    }
                })
                .setIcon(android.R.drawable.ic_delete)
                .show();
        }
    }

    private void checkResponses() {
        checkCounter--;
        if (checkCounter == 0)
        {
            Toast.makeText(getBaseContext(), getString(R.string.test)+" \""+ testName+"\" "+getString(R.string.was_successfully_uploaded_to_server), Toast.LENGTH_LONG).show();
            finish();
        }
    }


    /**
     * Metoda volaná při ukončení testu
     */
    @Override
    public void finish()
    {
        try{
            timerCount.cancel();
        } catch(NullPointerException e){}
        timerCount = null;
        answers = null;
        testInfo = null;
        jsonQuestions = null;
        jsonQuestionOptions = null;
        startAt = null;
        generatedTestId = 0;
        timeMinutes = 0;
       // mode = 0;
        testDrawnResponse = null;
        testDrawnQuestionAnswers = null;

        answers = null ;   //pole zaškrtlých odpovedi uživatelem
        answersClicks = null;  //pole počtu kliknutí uživatele na otazku
        timeSpent = null;      //pole stráveného času nad jednotlivými otazkami
        viewCount = null;       //pole kolikrat byla zobrazena otazka
        lastTime = 0;     //pomocná proměnná pro počítaní času
        lastTab = 0;       //pomocná proměnná pro počítaní času
        log = new JSONArray();
        questionImages = null;
        answerImages = null;
        timerCount = null;
        testName = null;

        try{
            taking_a_test_finishDialog.cancel();
        } catch(NullPointerException e){}
        super.finish();
        FragmentTest_Layout.refreshaftertest();
        Log.d("test", "finished");
    }

    /**
     * Stopky, které odpočítávají čas do konce testu, po uplynutí času se test automaticky ukončí
     */
    public static class MyCount extends CountDownTimer {
        public MyCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        /**
         * konec testu
         */
        @Override
        public void onFinish() {
            try{
                Taking_a_test.millisUntilFinished = 0;
                getCurrentInstance.countdowntext.setText(TimeLogika.getTimeFromMilis(0));
                Taking_a_Test_Fragment.countdowntext.setText(TimeLogika.getTimeFromMilis(0));
            } catch(NullPointerException e){}
            getCurrentInstance.evaluate();


        }

        /**
         * čas se odpočítává po 1 sekundě a překresluje se v UI
         *
         * @param millisUntilFinished počet zbývajícíh milisekund do konce
         */
        @Override
        public void onTick(long millisUntilFinished) {
            try{
                Taking_a_test.millisUntilFinished = millisUntilFinished;
                getCurrentInstance.countdowntext.setText(TimeLogika.getTimeFromMilis(millisUntilFinished));
                Taking_a_Test_Fragment.countdowntext.setText(TimeLogika.getTimeFromMilis(millisUntilFinished));
            }
            catch (Exception e) {}
        }

    }

    /**
     * Třída, která zobrazuje jednotlivé otázky velke sebe a je možno mezi nima přepínat přetažením
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        /**
         * Instancuje Otázku pro konkretní pořadí, otázka si sama vyzvedava data z připravených polí
         */
        @Override
        public Fragment getItem(int position) {
            // vrátí požadovanou objekt pro požadovanou pozici
            if(mode == TESTDRAWN){return new Taking_a_Test_Fragment();  }
            if (position == fragmentsCount-1) {
                return new Taking_a_Test_Fragment.Taking_a_Test_Fragment_Confirmation();// fragment vyhodnoceni
            }
            return new Taking_a_Test_Fragment();
        }
        @Override
        public int getCount() {
            if(mode == TESTDRAWN) return fragmentsCount-1;
            return fragmentsCount;
        }
        @Override
        public CharSequence getPageTitle(int position) {
            if(mode != TESTDRAWN)  Integer.toString(position+1);
            if (position == fragmentsCount-1)
                return "✔"; // fragment vyhodnoceni
            else
                return(Integer.toString(position+1));
        }
    }

    public class FadePageTransformer implements ViewPager.PageTransformer {
        public void transformPage(View view, float position) {
            int pageWidth = view.getWidth();

            LinearLayout questionsLayout = (LinearLayout) view.findViewById(R.id.question);
            if (questionsLayout == null) return;
            if (position < -1) { // [-Infinity,-1)
                // This page is way off-screen to the left.
          //      questionsLayout.setAlpha(0);

            } else if (position <= 1) { // [-1,1]


                for(int i = 0; i<questionsLayout.getChildCount(); i++)
                {
                    if(i%2 == 0) continue;
                    questionsLayout.getChildAt(i).setTranslationX((float) (position * 1.3 * pageWidth));
                    if (questionsLayout.getChildAt(i+1) != null) questionsLayout.getChildAt(i+1).setTranslationX((position) * (pageWidth / i+1));
                }
            } else { // (1,+Infinity]
                // This page is way off-screen to the right.
          //      questionsLayout.setAlpha(0);
            }
        }
    }
}