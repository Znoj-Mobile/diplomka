package cz.vsb.elogika.utils;

import android.graphics.Bitmap;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class Image
{
    public static byte[] scale(Bitmap image, int countOfPixels)
    {
        return scale(image, countOfPixels, 70);
    }



    public static byte[] scale(Bitmap image, int countOfPixels, int quality)
    {
        int width = image.getWidth();
        int height = image.getHeight();

        float ratio = width * height / countOfPixels;
        Bitmap bitmap = Bitmap.createScaledBitmap(image, (int) (width / Math.sqrt(ratio)), (int) (height / Math.sqrt(ratio)), false);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, quality, byteArrayOutputStream);
        //bitmap = BitmapFactory.decodeByteArray(byteArrayOutputStream.toByteArray(), 0, byteArrayOutputStream.size());
        byte[] bytes = byteArrayOutputStream.toByteArray();
        try
        {
            byteArrayOutputStream.close();
        }
        catch (IOException e) {}
        return bytes;
    }



    public static byte[] scaleByLimits(Bitmap bitmap, int widthLimit, int heightLimit)
    {
        return scaleByLimits(bitmap, widthLimit, heightLimit, 70);
    }



    public static byte[] scaleByLimits(Bitmap image, int widthLimit, int heightLimit, int quality)
    {
        int width = image.getWidth();
        int height = image.getHeight();

        if (width > widthLimit)
        {
            height = widthLimit * height / width;
            width = widthLimit;
        }
        if (height > heightLimit)
        {
            width = heightLimit * width / height;
            height = heightLimit;
        }

        Bitmap bitmap = Bitmap.createScaledBitmap(image, width, height, false);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, quality, byteArrayOutputStream);
        //bitmap = BitmapFactory.decodeByteArray(byteArrayOutputStream.toByteArray(), 0, byteArrayOutputStream.size());
        byte[] bytes = byteArrayOutputStream.toByteArray();
        try
        {
            byteArrayOutputStream.close();
        }
        catch (IOException e) {}
        return bytes;
    }
}
