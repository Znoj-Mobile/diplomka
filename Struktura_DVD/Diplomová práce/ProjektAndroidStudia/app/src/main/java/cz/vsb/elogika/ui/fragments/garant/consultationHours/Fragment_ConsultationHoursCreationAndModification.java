package cz.vsb.elogika.ui.fragments.garant.consultationHours;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Spinner;

import com.android.volley.VolleyError;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.dataTypes.ConsultationHour;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.DatePickerDialogButton;
import cz.vsb.elogika.utils.Logging;
import cz.vsb.elogika.utils.TimePickerDialogButton;

public class Fragment_ConsultationHoursCreationAndModification extends Fragment
{
    private boolean isCreation;
    private int id;
    private EditText description;
    private EditText room;
    private Spinner repeating;
    private CheckBox notifyViaEmail;
    private NumberPicker maxStudents;
    private Button roundButton;
    private DatePickerDialogButton dateFrom;
    private TimePickerDialogButton timeFrom;
    private DatePickerDialogButton dateTo;
    private TimePickerDialogButton timeTo;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        final Bundle bundle = getArguments();
        ConsultationHour consultationHour = null;
        if (bundle == null) this.isCreation = true;
        else this.isCreation = false;

        Actionbar.addSubcategory(consultationHour == null ? R.string.create_consultation_hour : R.string.edit_consultation_hour, this);
        Logging.logAccess("/Pages/Other/ConsultationHours/ConsultationHoursDetail.aspx");

        View rootView = inflater.inflate(R.layout.fragment_consultation_hour_creation_and_modification, container, false);

        this.description = (EditText) rootView.findViewById(R.id.description);
        this.dateFrom = (DatePickerDialogButton) rootView.findViewById(R.id.date_from);
        this.dateTo = (DatePickerDialogButton) rootView.findViewById(R.id.date_to);
        this.timeFrom = (TimePickerDialogButton) rootView.findViewById(R.id.time_from);
        this.timeTo = (TimePickerDialogButton) rootView.findViewById(R.id.time_to);
        this.room = (EditText) rootView.findViewById(R.id.room);
        this.repeating = (Spinner) rootView.findViewById(R.id.repeating);
        this.notifyViaEmail = (CheckBox) rootView.findViewById(R.id.notify_via_email);
        this.maxStudents = (NumberPicker) rootView.findViewById(R.id.max_students);

        this.repeating.setAdapter(new ArrayAdapter(getActivity(), R.layout.schedule_spinner, new String[]{getResources().getString(R.string.no), getResources().getString(R.string.weekly), getResources().getString(R.string.fortnightly), getResources().getString(R.string.monthly)}));
        this.maxStudents.setMinValue(0);
        this.maxStudents.setMaxValue(Integer.MAX_VALUE);
        this.maxStudents.setWrapSelectorWheel(false);

        if (!this.isCreation)
        {
            this.id = bundle.getInt("id");
            this.description.setText(bundle.getString("description"));
            this.room.setText(bundle.getString("room"));
            this.repeating.setSelection(bundle.getInt("repeatableType"));
            this.notifyViaEmail.setChecked(bundle.getBoolean("notifyViaEmail"));
            this.maxStudents.setValue(bundle.getInt("maximumOfStudents"));

            try
            {
                this.dateFrom.setDate(bundle.getString("from"));
                this.timeFrom.setTime(bundle.getString("fromHours"));
                this.dateTo.setDate(bundle.getString("to"));
                this.timeTo.setTime(bundle.getString("toHours"));
            }
            catch (ParseException e) {}
        }

        this.roundButton = (Button) rootView.findViewById(R.id.roundButton);
        this.roundButton.setText("✔");
        this.roundButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                roundButton.setEnabled(false);
                save();
            }
        });

        return rootView;
    }



    /**
     * Vytvoří nebo upraví konzultační hodinu.
     */
    private void save()
    {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                // TODO server nevrací success, až to bude podporovat server, tady to stačí odkomentovat
                /*if (response.getBoolean("Success"))
                {*/
                    Logging.logAccess("/Pages/Other/ConsultationHours/ConsultationHoursSummary.aspx");
                    SnackbarManager.show(Snackbar.with(getActivity()).text(isCreation ? R.string.consultation_hour_was_successfully_created : R.string.consultation_hour_was_successfully_edited));
                    Actionbar.jumpBack();
                /*}
                else
                {
                    SnackbarManager.show(Snackbar.with(getActivity()).text(R.string.lesson_was_not_created));
                    roundButton.setEnabled(true);
                }*/
            }

            @Override
            public void onError(VolleyError error)
            {
                super.onError(error);
                roundButton.setEnabled(true);
            }

            @Override
            public void onException(JSONException exception)
            {
                super.onException(exception);
                roundButton.setEnabled(true);
            }
        };
        JSONObject parameters = new JSONObject();
        try
        {
            if (!isCreation) parameters.put("Id", this.id);
            parameters.put("GarantId", User.id);
            parameters.put("CourseInfoId", User.courseInfoId);
            parameters.put("Description", this.description.getText().toString());
            parameters.put("From", this.dateFrom.getDate());
            parameters.put("To", this.dateTo.getDate());
            parameters.put("FromHours", this.timeFrom.getTime());
            parameters.put("ToHours", this.timeTo.getTime());
            parameters.put("Place", this.room.getText().toString());
            parameters.put("RepeatableType", this.repeating.getSelectedItemPosition());
            parameters.put("AlertByEmail", this.notifyViaEmail.isChecked());
            parameters.put("MaxStudents", this.maxStudents.getValue());
        }
        catch (JSONException e) {}
        Request.post(URL.ConsultationHours.Save(), parameters, response);
    }
}

