package cz.vsb.elogika.ui.fragments.garant.testsAndAcvtivities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.DatePickerDialogOnButton;
import cz.vsb.elogika.utils.DownloadFile;
import cz.vsb.elogika.utils.EnumLogika;
import cz.vsb.elogika.utils.Logging;
import cz.vsb.elogika.utils.SelectFileToUpload;
import cz.vsb.elogika.utils.TimePickerDialogOnButton;

public class Fragment_taa_add_activity extends Fragment {
    View rootView;

    TextView taa_add_a_title;
    TextView taa_add_a_description;
    TextView taa_add_a_sample_result;

    NumberPicker taa_add_a_min1;
    NumberPicker taa_add_a_min2;
    NumberPicker taa_add_a_min3;

    NumberPicker taa_add_a_rating1;
    NumberPicker taa_add_a_rating2;
    NumberPicker taa_add_a_rating3;


    NumberPicker taa_add_a_limit1;
    NumberPicker taa_add_a_limit2;
    NumberPicker taa_add_a_limit3;

    NumberPicker taa_add_a_efforts1;
    NumberPicker taa_add_a_efforts2;
    NumberPicker taa_add_a_efforts3;

    DatePickerDialogOnButton taa_add_a_date_to_date;
    TimePickerDialogOnButton taa_add_a_time_to_date;

    CheckBox taa_add_a_mandatory;
    CheckBox taa_add_a_choice;

    RadioGroup taa_add_a_radiogroup;

    Map<String, String> list = null;

    String FileName_description;
    String FileType_description;
    String FileinBase64_description;

    String FileName_sample_result;
    String FileType_sample_result;
    String FileinBase64_sample_result;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_taa_add_a, container, false);


        try {
            list = (Map<String, String>) getArguments().getSerializable("list");
        } catch (Exception e){   }
        if(list == null){
            Actionbar.addSubcategory(R.string.add_activity, this);
            Logging.logAccess("/Pages/Garant/TestyAktivityDetail/AktivitaForm.aspx");
        }
        else{
            Actionbar.addSubcategory(R.string.edit_activity, this);
            Logging.logAccess("/Pages/Garant/TestyAktivityDetail/AktivitaForm.aspx?IDA=" + list.get("idAktivita"));
        }
        setComponents();
        return rootView;
    }

    private void setComponents() {

        taa_add_a_title = (TextView) rootView.findViewById(R.id.taa_add_a_title);
        taa_add_a_description = (TextView) rootView.findViewById(R.id.taa_add_a_description);
        taa_add_a_sample_result = (TextView) rootView.findViewById(R.id.taa_add_a_sample_result);

        //nastaveni komponent NumberPicker
        taa_add_a_min1 = (NumberPicker) rootView.findViewById(R.id.taa_add_a_min1);
        taa_add_a_min1.setMaxValue(9);
        taa_add_a_min1.setMinValue(0);
        taa_add_a_min2 = (NumberPicker) rootView.findViewById(R.id.taa_add_a_min2);
        taa_add_a_min2.setMaxValue(9);
        taa_add_a_min2.setMinValue(0);
        taa_add_a_min3 = (NumberPicker) rootView.findViewById(R.id.taa_add_a_min3);
        taa_add_a_min3.setMaxValue(9);
        taa_add_a_min3.setMinValue(0);

        taa_add_a_rating1 = (NumberPicker) rootView.findViewById(R.id.taa_add_a_rating1);
        taa_add_a_rating1.setMaxValue(9);
        taa_add_a_rating1.setMinValue(0);
        taa_add_a_rating2 = (NumberPicker) rootView.findViewById(R.id.taa_add_a_rating2);
        taa_add_a_rating2.setMaxValue(9);
        taa_add_a_rating2.setMinValue(0);
        taa_add_a_rating3 = (NumberPicker) rootView.findViewById(R.id.taa_add_a_rating3);
        taa_add_a_rating3.setMaxValue(9);
        taa_add_a_rating3.setMinValue(0);

        taa_add_a_limit1 = (NumberPicker) rootView.findViewById(R.id.taa_add_a_limit1);
        taa_add_a_limit1.setMaxValue(9);
        taa_add_a_limit1.setMinValue(0);
        taa_add_a_limit2 = (NumberPicker) rootView.findViewById(R.id.taa_add_a_limit2);
        taa_add_a_limit2.setMaxValue(9);
        taa_add_a_limit2.setMinValue(0);
        taa_add_a_limit3 = (NumberPicker) rootView.findViewById(R.id.taa_add_a_limit3);
        taa_add_a_limit3.setMaxValue(9);
        taa_add_a_limit3.setMinValue(0);

        taa_add_a_efforts1 = (NumberPicker) rootView.findViewById(R.id.taa_add_a_efforts1);
        taa_add_a_efforts1.setMaxValue(9);
        taa_add_a_efforts1.setMinValue(0);
        taa_add_a_efforts2 = (NumberPicker) rootView.findViewById(R.id.taa_add_a_efforts2);
        taa_add_a_efforts2.setMaxValue(9);
        taa_add_a_efforts2.setMinValue(0);
        taa_add_a_efforts3 = (NumberPicker) rootView.findViewById(R.id.taa_add_a_efforts3);
        taa_add_a_efforts3.setMaxValue(9);
        taa_add_a_efforts3.setMinValue(0);


        //checkboxy
        taa_add_a_mandatory = (CheckBox) rootView.findViewById(R.id.taa_add_a_mandatory);
        taa_add_a_choice = (CheckBox) rootView.findViewById(R.id.taa_add_a_choice);

        //radioButtony
        taa_add_a_radiogroup = (RadioGroup) rootView.findViewById(R.id.taa_add_a_radiogroup);

        taa_add_a_date_to_date = new DatePickerDialogOnButton((Button) rootView.findViewById(R.id.taa_add_a_date_to_date));
        taa_add_a_time_to_date = new TimePickerDialogOnButton((Button) rootView.findViewById(R.id.taa_add_a_time_to_date));

        Button taa_add_a_description_in_file = (Button) rootView.findViewById(R.id.taa_add_a_description_in_file);
        taa_add_a_description_in_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SelectFileToUpload(getActivity()) {
                    @Override
                    public void selectedFile(String FileName, String FileType, String FileinBase64) {
                        TextView taa_add_a_description_in_file_text = (TextView)rootView.findViewById(R.id.taa_add_a_description_in_file_text);
                        taa_add_a_description_in_file_text.setText(FileName);
                        taa_add_a_description_in_file_text.setVisibility(View.VISIBLE);
                        Button btn_popis = (Button) rootView.findViewById(R.id.taa_add_a_description_in_file_download);
                        btn_popis.setVisibility(View.VISIBLE);

                        FileName_description = FileName;
                        FileType_description = FileType;
                        FileinBase64_description = FileinBase64;
                    }
                };
            }
        });

        Button taa_add_a_sample_result_in_file = (Button) rootView.findViewById(R.id.taa_add_a_sample_result_in_file);
        taa_add_a_sample_result_in_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SelectFileToUpload(getActivity()) {
                    @Override
                    public void selectedFile(String FileName, String FileType, String FileinBase64) {
                        TextView taa_add_a_sample_result_in_file_text = (TextView)rootView.findViewById(R.id.taa_add_a_sample_result_in_file_text);
                        taa_add_a_sample_result_in_file_text.setText(FileName);
                        taa_add_a_sample_result_in_file_text.setVisibility(View.VISIBLE);
                        Button btn_vzor = (Button) rootView.findViewById(R.id.taa_add_a_sample_result_in_file_download);
                        btn_vzor.setVisibility(View.VISIBLE);

                        FileName_sample_result = FileName;
                        FileType_sample_result = FileType;
                        FileinBase64_sample_result = FileinBase64;
                    }
                };
            }
        });


        Button taa_add_a_btn_send = (Button) rootView.findViewById(R.id.taa_add_a_btn_send);


        Button taa_add_a_btn_remove = (Button) rootView.findViewById(R.id.taa_add_a_btn_remove);
        taa_add_a_btn_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(R.string.activity_cancel);
                builder.setCancelable(false);
                builder.setMessage(R.string.activity_cancel_message);
                builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog2, int which) {
                        Actionbar.jumpBack();
                    }
                });

                builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog2, int which) {
                        ;
                    }
                }).setIcon(android.R.drawable.ic_dialog_alert);
                Dialog d = builder.create();
                d.show();
            }
        });

        if(list != null){
            it_is_update();
            taa_add_a_btn_send.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle(R.string.activity_update);
                    builder.setCancelable(false);
                    builder.setMessage(R.string.activity_update_message);
                    builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog2, int which) {
                            update(list.get("idAktivita"));
                        }
                    });
                    builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog2, int which) {
                            ;
                        }
                    }).setIcon(android.R.drawable.ic_dialog_alert);
                    Dialog d = builder.create();
                    d.show();
                }
            });
        }
        else{
            taa_add_a_btn_send.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle(R.string.activity_insert);
                    builder.setCancelable(false);
                    builder.setMessage(R.string.activity_insert_message);
                    builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog2, int which) {
                            insert();
                        }
                    });
                    builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog2, int which) {
                            ;
                        }
                    }).setIcon(android.R.drawable.ic_dialog_alert);
                    Dialog d = builder.create();
                    d.show();
                }
            });
        }
    }

    private void it_is_update() {
        taa_add_a_title.setText(list.get("nazev"));
        taa_add_a_description.setText(list.get("popis"));
        taa_add_a_sample_result.setText(list.get("vzorVysl"));

        int value = Integer.valueOf(list.get("minimum"));
        taa_add_a_min1.setValue(value / 100);
        value %= 100;
        taa_add_a_min2.setValue(value / 10);
        value %= 10;
        taa_add_a_min3.setValue(value);

        value = Integer.valueOf(list.get("doporHodnoceni"));
        taa_add_a_rating1.setValue(value/100);
        value %= 100;
        taa_add_a_rating2.setValue(value / 10);
        value %= 10;
        taa_add_a_rating3.setValue(value);

        value = Integer.valueOf(list.get("omezeniStudentu"));
        taa_add_a_limit1.setValue(value/100);
        value %= 100;
        taa_add_a_limit2.setValue(value / 10);
        value %= 10;
        taa_add_a_limit3.setValue(value);

        value = Integer.valueOf(list.get("pocetPokusu"));
        taa_add_a_efforts1.setValue(value/100);
        value %= 100;
        taa_add_a_efforts2.setValue(value / 10);
        value %= 10;
        taa_add_a_efforts3.setValue(value);


        if(list.get("vyberDoData") != "") {
            taa_add_a_date_to_date.setDateTime(taa_add_a_time_to_date, list.get("vyberDoData"));
        }
        if(list.get("povinny").equals("true")){
            taa_add_a_mandatory.setChecked(true);
        }
        else{
            taa_add_a_mandatory.setChecked(false);
        }
        if(list.get("vybiratelnost").equals("true")){
            taa_add_a_choice.setChecked(true);
        }
        else{
            taa_add_a_choice.setChecked(false);
        }

        if(list.get("typOmezeni").equals("0")){
            taa_add_a_radiogroup.check(R.id.taa_add_a_radiobutton_class);
        }
        else{
            taa_add_a_radiogroup.check(R.id.taa_add_a_radiobutton_course);
        }

        Button btn_popis = (Button) rootView.findViewById(R.id.taa_add_a_description_in_file_download);
        TextView taa_add_a_description_in_file_text = (TextView)rootView.findViewById(R.id.taa_add_a_description_in_file_text);
        if(!list.get("popisSouborNazev").equals("")) {
            btn_popis.setVisibility(View.VISIBLE);
            btn_popis.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (FileinBase64_description != null) {
                        downloadFile(FileName_description, FileinBase64_description);
                    } else {
                        SnackbarManager.show(
                                Snackbar.with(getActivity())
                                        .type(SnackbarType.MULTI_LINE)
                                        .duration(Snackbar.SnackbarDuration.LENGTH_INDEFINITE)
                                        .text(getResources().getString(R.string.file_is_downloaded)));
                        downloadInfoFile(list.get("popisSouborNazev"), list.get("idAktivita"));
                    }
                }
            });
            taa_add_a_description_in_file_text.setText(list.get("popisSouborNazev"));
            taa_add_a_description_in_file_text.setVisibility(View.VISIBLE);
        }
        else{
            btn_popis.setVisibility(View.GONE);
            btn_popis.setOnClickListener(null);
            taa_add_a_description_in_file_text.setVisibility(View.GONE);
        }

        Button btn_vzor = (Button) rootView.findViewById(R.id.taa_add_a_sample_result_in_file_download);
        TextView taa_add_a_sample_result_in_file_text = (TextView)rootView.findViewById(R.id.taa_add_a_sample_result_in_file_text);
        if(!list.get("vzorVyslSouborNazev").equals("")) {
            btn_vzor.setVisibility(View.VISIBLE);
            btn_vzor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(FileinBase64_sample_result != null) {
                        downloadFile(FileName_sample_result, FileinBase64_sample_result);
                    }
                    else{
                        SnackbarManager.show(
                                Snackbar.with(getActivity())
                                        .type(SnackbarType.MULTI_LINE)
                                        .duration(Snackbar.SnackbarDuration.LENGTH_INDEFINITE)
                                        .text(getResources().getString(R.string.file_is_downloaded)));
                        downloadSampleFile(list.get("vzorVyslSouborNazev"), list.get("idAktivita"));
                    }
                }
            });
            taa_add_a_sample_result_in_file_text.setText(list.get("vzorVyslSouborNazev"));
            taa_add_a_sample_result_in_file_text.setVisibility(View.VISIBLE);
        }
        else{
            btn_vzor.setVisibility(View.GONE);
            btn_vzor.setOnClickListener(null);
            taa_add_a_sample_result_in_file_text.setVisibility(View.GONE);
        }

    }

    private void downloadSampleFile(final String fileName, String activityId) {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                if (response.length() > 0)
                {
                    Log.d("download odpoved", response.toString());
                }
                else{
                    Log.d("Chyba", "Fragment_Activities downloadSampleFile");
                }
                Intent intent = DownloadFile.download(fileName, response.getString("VzorVyslSoubor"));
                getActivity().startActivity(intent);
                SnackbarManager.show(
                        Snackbar.with(getActivity())
                                .type(SnackbarType.MULTI_LINE)
                                .duration(Snackbar.SnackbarDuration.LENGTH_INDEFINITE)
                                .text(getResources().getString(R.string.file) + " '" + fileName + "' " + getResources().getString(R.string.is_downloaded_in_download_folder)));
            }
        };

        Request.get(URL.Activity.GetActivitySampleFile(Integer.valueOf(activityId)), response);
    }
    private void downloadFile(final String fileName, final String data){
        Intent intent = DownloadFile.download(fileName, data);
        getActivity().startActivity(intent);
        SnackbarManager.show(
                Snackbar.with(getActivity())
                        .type(SnackbarType.MULTI_LINE)
                        .duration(Snackbar.SnackbarDuration.LENGTH_INDEFINITE)
                        .text(getResources().getString(R.string.file) + " '" + fileName + "' " + getResources().getString(R.string.is_downloaded_in_download_folder)));

    }

    private void downloadInfoFile(final String fileName, String activityId) {
        Response response = new Response()
        {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                if (response.length() > 0)
                {
                    Log.d("download odpoved", response.toString());
                }
                else{
                    Log.d("Chyba", "Fragment_Activities downloadInfoFile");
                }
                Intent intent = DownloadFile.download(fileName, response.getString("PopisSoubor"));
                getActivity().startActivity(intent);
                SnackbarManager.show(
                        Snackbar.with(getActivity())
                                .type(SnackbarType.MULTI_LINE)
                                .duration(Snackbar.SnackbarDuration.LENGTH_INDEFINITE)
                                .text(getResources().getString(R.string.file) + " '" + fileName + "' " + getResources().getString(R.string.is_downloaded_in_download_folder)));
            }
        };

        Request.get(URL.Activity.GetActivityInfoFile(Integer.valueOf(activityId)), response);
    }

    @SuppressLint("LongLogTag")
    private void update(String idAktivita){
        Logging.logAccess("/Pages/Garant/TestyAktivityDetail/AktivitaSummary.aspx");
        final String name = taa_add_a_title.getText().toString();
        Response response = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException {
                Log.d("typ response: ", response.getString("Success"));
                if (response.getString("Success").equals("true")) {
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .type(SnackbarType.MULTI_LINE)
                                    .text(getActivity().getString(R.string.one_activity) + " " + name + " " + getActivity().getString(R.string.activity_was_updated)));
                    Actionbar.jumpBack();
                } else { //nepovedlo se
                    Log.d("Fragment_taa_add_activity", "update(): " + response.toString());
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .type(SnackbarType.MULTI_LINE)
                                    .text(getActivity().getString(R.string.one_activity_4p) + " " + name + " " + getActivity().getString(R.string.activity_was_not_updated)));
                }
            }
        };
        JSONObject parameters = new JSONObject();
        try {
            parameters.put("IDAktivita", idAktivita);
            int pocet = taa_add_a_efforts1.getValue() * 100 + taa_add_a_efforts2.getValue() * 10 + taa_add_a_efforts3.getValue();
            parameters.put("PocetPokusu", pocet);
            parameters.put("Nazev", taa_add_a_title.getText());
            parameters.put("Popis", taa_add_a_description.getText());
            parameters.put("VzorVysl", taa_add_a_sample_result.getText());
            pocet = taa_add_a_rating1.getValue() * 100 + taa_add_a_rating2.getValue() * 10 + taa_add_a_rating3.getValue();
            parameters.put("DoporHodnoceni", pocet);
            //parameters.put("Maximum", "");
            pocet = taa_add_a_min1.getValue() * 100 + taa_add_a_min2.getValue() * 10 + taa_add_a_min3.getValue();
            parameters.put("Minimum", pocet);
            if(taa_add_a_mandatory.isChecked()) {
                parameters.put("Povinny", true);
            }
            else{
                parameters.put("Povinny", false);
            }
            if(taa_add_a_choice.isChecked()) {
                parameters.put("Vybiratelnost", true);
            }
            else{
                parameters.put("Vybiratelnost", false);
            }

            parameters.put("VyberDoData", taa_add_a_date_to_date.getDate(taa_add_a_time_to_date));


            pocet = taa_add_a_limit1.getValue() * 100 + taa_add_a_limit2.getValue() * 10 + taa_add_a_limit3.getValue();
            parameters.put("OmezeniStudentu", pocet);
            String typOmezeni = ((RadioButton) rootView.findViewById(taa_add_a_radiogroup.getCheckedRadioButtonId())).getText().toString();

            parameters.put("TypOmezeni", EnumLogika.idTypOmezeni(typOmezeni));

            if(FileinBase64_description != null) {
                parameters.put("PopisSoubor", FileinBase64_description);
                parameters.put("PopisSouborNazev", FileName_description);
                parameters.put("PopisSouborContType", FileType_description);
            }
            else{
                parameters.put("PopisSoubor", new byte[0]);
                parameters.put("PopisSouborNazev", "");
                parameters.put("PopisSouborContType", "");
            }

            if(FileinBase64_sample_result != null) {
                parameters.put("VzorVyslSoubor", FileinBase64_sample_result);
                parameters.put("VzorVyslSouborNazev", FileName_sample_result);
                parameters.put("VzorVyslSouborContType", FileType_sample_result);
            }
            else{
                parameters.put("VzorVyslSoubor", new byte[0]);
                parameters.put("VzorVyslSouborNazev", "");
                parameters.put("VzorVyslSouborContType", "");
            }


            parameters.put("Smazana", false);
            parameters.put("IDTermin", "");
            parameters.put("NazevTermin", "");

            parameters.put("NazevSA", "");
            parameters.put("IsEditable", true);
            Log.d("", parameters + "");
        } catch (JSONException e) {
            Log.d("Fragment_taa_add_activity", "update() param: " + e.getMessage());
        }
        Request.post(URL.Activity.Update(), parameters, response);
    }

    @SuppressLint("LongLogTag")
    private void insert() {
        Logging.logAccess("/Pages/Garant/TestyAktivityDetail/AktivitaSummary.aspx");
        final String name = taa_add_a_title.getText().toString();
        Response response = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException {
                Log.d("typ response: ", response.getString("Success"));
                if (response.getString("Success").equals("true")) {
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .type(SnackbarType.MULTI_LINE)
                                    .text(getActivity().getString(R.string.one_activity) + " " + name + " " + getActivity().getString(R.string.activity_was_inserted)));
                    Actionbar.jumpBack();
                } else { //nepovedlo se
                    Log.d("Fragment_taa_add_activity", "insert(): " + response.toString());
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .type(SnackbarType.MULTI_LINE)
                                    .text(getActivity().getString(R.string.one_activity_4p) + " " + name + " " + getActivity().getString(R.string.activity_was_not_inserted)));
                }
            }
        };
        JSONObject parameters = new JSONObject();
        try {
            parameters.put("IDAktivita", "-1");
            int pocet = taa_add_a_efforts1.getValue() * 100 + taa_add_a_efforts2.getValue() * 10 + taa_add_a_efforts3.getValue();
            parameters.put("PocetPokusu", pocet);
            parameters.put("Nazev", taa_add_a_title.getText());
            parameters.put("Popis", taa_add_a_description.getText());
            parameters.put("VzorVysl", taa_add_a_sample_result.getText());
            pocet = taa_add_a_rating1.getValue() * 100 + taa_add_a_rating2.getValue() * 10 + taa_add_a_rating3.getValue();
            parameters.put("DoporHodnoceni", pocet);
            //parameters.put("Maximum", "");
            pocet = taa_add_a_min1.getValue() * 100 + taa_add_a_min2.getValue() * 10 + taa_add_a_min3.getValue();
            parameters.put("Minimum", pocet);
            if(taa_add_a_mandatory.isChecked()) {
                parameters.put("Povinny", true);
            }
            else{
                parameters.put("Povinny", false);
            }
            if(taa_add_a_choice.isChecked()) {
                parameters.put("Vybiratelnost", true);
            }
            else{
                parameters.put("Vybiratelnost", false);
            }

            parameters.put("VyberDoData", taa_add_a_date_to_date.getDate(taa_add_a_time_to_date));


            pocet = taa_add_a_limit1.getValue() * 100 + taa_add_a_limit2.getValue() * 10 + taa_add_a_limit3.getValue();
            parameters.put("OmezeniStudentu", pocet);
            String typOmezeni = ((RadioButton) rootView.findViewById(taa_add_a_radiogroup.getCheckedRadioButtonId())).getText().toString();

            parameters.put("TypOmezeni", EnumLogika.idTypOmezeni(typOmezeni));

            if(FileinBase64_description != null) {
                parameters.put("PopisSoubor", FileinBase64_description);
                parameters.put("PopisSouborNazev", FileName_description);
                parameters.put("PopisSouborContType", FileType_description);
            }
            else{
                parameters.put("PopisSoubor", new byte[0]);
                parameters.put("PopisSouborNazev", "");
                parameters.put("PopisSouborContType", "");
            }

            if(FileinBase64_sample_result != null) {
                parameters.put("VzorVyslSoubor", FileinBase64_sample_result);
                parameters.put("VzorVyslSouborNazev", FileName_sample_result);
                parameters.put("VzorVyslSouborContType", FileType_sample_result);
            }
            else{
                parameters.put("VzorVyslSoubor", new byte[0]);
                parameters.put("VzorVyslSouborNazev", "");
                parameters.put("VzorVyslSouborContType", "");
            }


            parameters.put("Smazana", false);
            parameters.put("IDTermin", "");
            parameters.put("NazevTermin", "");

            parameters.put("NazevSA", "");
            parameters.put("IsEditable", true);
            Log.d("", parameters + "");
        } catch (JSONException e) {
            Log.d("Fragment_taa_add_activity", "insert() param: " + e.getMessage());
        }
        Request.post(URL.Activity.Insert(Fragment_TestsAndActivity.groupActivityId, User.id), parameters, response);
    }
}
