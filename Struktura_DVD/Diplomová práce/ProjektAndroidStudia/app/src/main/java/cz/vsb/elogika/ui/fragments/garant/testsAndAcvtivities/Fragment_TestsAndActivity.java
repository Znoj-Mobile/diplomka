package cz.vsb.elogika.ui.fragments.garant.testsAndAcvtivities;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.vsb.elogika.R;
import cz.vsb.elogika.common.view.SlidingTabLayout;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.EnumLogika;
import cz.vsb.elogika.utils.Logging;

public class Fragment_TestsAndActivity extends Fragment {
    private View mainView;
    private View rootView;
    static String groupActivityId;
    static String nazevSA;
    //case 0
    static int spinnerPosition;

    static ArrayList<Map<String, String>> template_list;
    static ArrayList<String> template_list_names;
    static ArrayList<String> template_list_ids;


    static ArrayList<Map<String, String>> export_list;

    static Fragment_taa_a fragment_taa_a;
    static Fragment_taa_t fragment_taa_t;


    //head
    LinearLayout nothinglayout;
    ProgressBar loading_progress;
    ArrayAdapter<String> adapter_ag;
    List<String> toSpin;
    ArrayList<Map<String, String>> ag_list;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mainView = inflater.inflate(R.layout.fragment_taa, container, false);
        rootView = mainView.findViewById(R.id.tabs_container);
        spinnerPosition = 0;
        try {
            groupActivityId = getArguments().getString("idSkupinaAktivit");
            nazevSA = getArguments().getString("nazevSA");
        }
        catch(NullPointerException e){
            //jak se to nepovedlo, jdu z menu
        }

        if(groupActivityId != null) {
            Actionbar.addSubcategory(R.string.tests_and_activity_management, this);
            Logging.logAccess("/Pages/Garant/TestyAktivity.aspx?IDSA=" + groupActivityId);
        }
        else{
            //nastavuji prozatim nulu → dojde k aktualizaci jakmile bude k dispozici groupActivityId z Fragment_taa()
            Logging.logAccess("Pages/Garant/TestyAktivity.aspx");
            groupActivityId = "0";
            nazevSA = " ";
            Actionbar.newCategory(R.string.tests_and_activity_management, this);
        }

        template_list = new ArrayList<Map<String,String>>();
        template_list_names = new ArrayList<String>();
        template_list_ids = new ArrayList<String>();

        export_list = new ArrayList<Map<String,String>>();
        get_templates();
        setHead();

        ViewPager mViewPager = (ViewPager) rootView.findViewById(R.id.viewpager);
        mViewPager.setAdapter(new FragmentStatePagerAdapter(getFragmentManager()) {
                                  String[] tabNames = new String[] {
                                          getString(R.string.activity),
                                          getString(R.string.tests)};

                                  @Override
                                  public int getCount() {
                                      return tabNames.length;
                                  }
                                  @Override
                                  public CharSequence getPageTitle(int position) {
                                      return tabNames[position];
                                  }


                                  @Override
                                  public Fragment getItem(int position) {

                                      switch (position)
                                      {
                                          case 0:
                                              fragment_taa_a = new Fragment_taa_a();
                                              return fragment_taa_a;
                                          case 1:
                                              fragment_taa_t = new Fragment_taa_t();
                                              return fragment_taa_t;
                                          default : return null;
                                      }
                                  }
                              }


        );
        SlidingTabLayout mSlidingTabLayout = (SlidingTabLayout) rootView.findViewById(R.id.sliding_tabs);
        mSlidingTabLayout.setViewPager(mViewPager);

        return mainView;
    }

    private void setHead() {
        nothinglayout = (LinearLayout) mainView.findViewById(R.id.nothing_to_show);
        nothinglayout.setVisibility(View.GONE);
        //LinearLayout ll = (LinearLayout) rootView.findViewById(R.id.course_condition_tta_layout);
        loading_progress = (ProgressBar) mainView.findViewById(R.id.loading_progress);
        loading_progress.setVisibility(View.VISIBLE);

        Button export_btn = (Button) mainView.findViewById(R.id.taa_export);
        export_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println(export_list);
            }
        });


        toSpin = new ArrayList<String>();
        final Spinner taa_spinner = (Spinner) mainView.findViewById(R.id.taa_spinner);
        taa_spinner.setSelection(Fragment_TestsAndActivity.getSpinnerPosition());

        adapter_ag = new ArrayAdapter<String>(getActivity(),R.layout.spinner_item,toSpin);

        taa_spinner.setAdapter( adapter_ag );
        final boolean[] first = {true};
        taa_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view2, int position, long id) {
                //aby se to pri vytvoreni nevracelo vzdy na nulu
                //vytvari se to totiz vzdy pri strate contextu - takze kdyz jdu z tabu 3 na tab 1
                //musi to byt takto!!!
                if(!first[0]) {
                    //pokud se fragment jen znovu nacita, pak neni nutne pridelovat to stejne cislo,
                    //nebot by to vedlo k novemu stazeni aktivit a testu
                    if(position != Fragment_TestsAndActivity.getSpinnerPosition()) {
                        Fragment_TestsAndActivity.setSpinnerPosition(position);
                        setDataInll(mainView, position);
                    }
                }
                else{
                    first[0] = false;
                    taa_spinner.setSelection(Fragment_TestsAndActivity.getSpinnerPosition());
                    setDataInll(mainView, Fragment_TestsAndActivity.getSpinnerPosition());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

                setDataInll(mainView, 0);
            }

        });

        ag_list = get_activity_groups();
    }

    static void setSpinnerPosition(int newValue) {
        spinnerPosition = newValue;
    }

    static int getSpinnerPosition() {
        return spinnerPosition;
    }

    public static void updateGroupActivityId(String groupActivityId, String nazevSA) {
        Fragment_TestsAndActivity.groupActivityId = groupActivityId;
        Fragment_TestsAndActivity.nazevSA = nazevSA;
        if(fragment_taa_a != null)
            fragment_taa_a.update();
        if(fragment_taa_t != null)
            fragment_taa_t.update();
    }

    public void get_templates() {

        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                if (response.length() > 0)
                {
                    for (int j = 0; j < response.length(); j++)
                    {
                        template_list.add(put_data_template(
                                response.getJSONObject(j).getString("IDSablona"),
                                response.getJSONObject(j).getString("Nazev"),
                                response.getJSONObject(j).getString("Popis"),
                                response.getJSONObject(j).getString("MichaniBloku"),
                                response.getJSONObject(j).getString("MichatCele"),
                                response.getJSONObject(j).getString("IDKurzInfo"),
                                response.getJSONObject(j).getString("Smazana"),
                                response.getJSONObject(j).getString("PocetBloku"),
                                response.getJSONObject(j).getString("IsEditable")));
                        template_list_names.add(response.getJSONObject(j).getString("Nazev"));
                        template_list_ids.add(response.getJSONObject(j).getString("IDSablona"));
                    }
                }
                else{
                    Log.d("Chyba", "TestsAndActivity get_templates");
                }
            }
        };
        Request.get(URL.Template.GetTemplates(User.courseInfoId, User.id, User.roleID), arrayResponse);
    }

    private Map<String, String> put_data_template(String idSablona, String nazev, String popis, String michaniBloku, String michatCele,
                                                  String idKurzInfo, String smazana, String pocetBloku, String isEditable) {
        HashMap<String, String> item = new HashMap<String, String>();

        item.put("idSablona", idSablona);
        item.put("nazev", nazev);
        item.put("popis", popis);
        item.put("michaniBloku", michaniBloku);
        item.put("michatCele", michatCele);
        item.put("idKurzInfo", idKurzInfo);
        item.put("smazana", smazana);
        item.put("pocetBloku", pocetBloku);
        item.put("isEditable", isEditable);

        return item;
    }

    //spoliham na to, ze sablony budou nacteny driv nez se budou pozadovat v testech
    public static String add_template_name_into_test(String idSablona) {
        for(int i = 0; i < template_list.size(); i++){
            if(template_list.get(i).get("idSablona").equals(idSablona)){
                return template_list.get(i).get("nazev");
            }
        }
        return "";
    }



    //metody pro head
    public ArrayList<Map<String,String>> get_activity_groups() {
        final ArrayList<Map<String,String>> list = new ArrayList<Map<String,String>>();

        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                if (response.length() > 0)
                {
                    for (int j = 0; j < response.length(); j++)
                    {
                        list.add(put_data(
                                response.getJSONObject(j).getString("IDSkupinaAktivit"),
                                response.getJSONObject(j).getString("PocetPokusu"),
                                response.getJSONObject(j).getString("Nazev"),
                                response.getJSONObject(j).getString("IDRole"),
                                response.getJSONObject(j).getString("IDKurzInfo"),
                                response.getJSONObject(j).getString("Minimum"),
                                response.getJSONObject(j).getString("Maximum"),
                                response.getJSONObject(j).getString("Povinny"),
                                response.getJSONObject(j).getString("ZapocitatDoVysledku"),
                                response.getJSONObject(j).getString("FormaStudia"),
                                response.getJSONObject(j).getString("Smazana"),
                                response.getJSONObject(j).getString("IsEditable"),
                                response.getJSONObject(j).getString("Nulovat")));


                        if(response.getJSONObject(j).getString("FormaStudia").equals("0")) {
                            toSpin.add(response.getJSONObject(j).getString("Nazev") + " (" +  getResources().getString(R.string.present_short) + ")");
                        }
                        else{
                            toSpin.add(response.getJSONObject(j).getString("Nazev") + " (" + getResources().getString(R.string.combinated_short) + ")");
                        }
                    }
                }
                else{
                    Log.d("Chyba", "CourseConditions get_activity_groups");
                    nothinglayout.setVisibility(View.VISIBLE);
                    //muze byt: ll.setVisibility(View.GONE);
                }

                loading_progress.setVisibility(View.GONE);
                adapter_ag.notifyDataSetChanged();
            }
        };
        Request.get(URL.GroupActivities.getgroupactivitiesbycourse(User.courseInfoId, User.id, User.roleID), arrayResponse);
        return list;
    }

    private Map<String, String> put_data(String idSkupinaAktivit, String pocetPokusu, String nazev, String idRole, String idKurzInfo, String minimum, String maximum,
                                         String povinny, String zapocitatDoVysledku, String formaStudia, String smazana, String isEditable, String nulovat) {
        HashMap<String, String> item = new HashMap<String, String>();

        item.put("idSkupinaAktivit", idSkupinaAktivit);
        item.put("pocetPokusu", pocetPokusu);
        item.put("nazev", nazev);
        item.put("idRole", idRole);
        item.put("idKurzInfo", idKurzInfo);
        item.put("minimum", minimum);
        item.put("maximum", maximum);
        item.put("povinny", povinny);
        item.put("zapocitatDoVysledku", zapocitatDoVysledku);
        item.put("formaStudia", formaStudia);
        item.put("smazana", smazana);
        item.put("isEditable", isEditable);
        item.put("nulovat", nulovat);

        return item;
    }

    //naplneni hlavicky
    private void setDataInll(View view, int i) {

        Fragment_TestsAndActivity.updateGroupActivityId(ag_list.get(i).get("idSkupinaAktivit"), ag_list.get(i).get("nazev"));
        Fragment_TestsAndActivity.nazevSA = ag_list.get(i).get("nazev");

        TextView taa_title = (TextView) view.findViewById(R.id.taa_title);
        taa_title.setText(ag_list.get(i).get("nazev"));

        TextView taa_manage = (TextView) view.findViewById(R.id.taa_manage);
        taa_manage.setText(EnumLogika.role(ag_list.get(i).get("idRole")));

        TextView taa_mandatory = (TextView) view.findViewById(R.id.taa_mandatory);
        taa_mandatory.setText(EnumLogika.povinnyAnoNe(ag_list.get(i).get("povinny")));

        TextView taa_min = (TextView) view.findViewById(R.id.taa_min);
        taa_min.setText(ag_list.get(i).get("minimum"));

        TextView taa_max = (TextView) view.findViewById(R.id.taa_max);
        taa_max.setText(ag_list.get(i).get("maximum"));

        TextView taa_form_study = (TextView) view.findViewById(R.id.taa_form_study);
        taa_form_study.setText(EnumLogika.formaStudia(ag_list.get(i).get("formaStudia")));

    }
}
