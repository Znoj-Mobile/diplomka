package cz.vsb.elogika.utils;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Map;

import cz.vsb.elogika.R;
import cz.vsb.elogika.ui.fragments.Fragment_RegistrationForExaminations;

/**
 * adapter pro rozliseni barvy nazvu povinnych polozek pouzit v Aktivitach a Registraci
 * je nutne, aby v poli 'from' bylo na 1. pozici (from[0]) jmeno
 * a povinnost pojmenovana jako "mandatory"
 */
public class SpecialAdapter extends SimpleAdapter {

    ArrayList<Map<String, String>> items;
    int to;
    CheckBox dialog_checkbox;
    Context context;

    public SpecialAdapter(Context context, ArrayList<Map<String, String>> items, int resource, String[] from, int[] to) {
        super(context, items, resource, from, to);
        this.context = context;
        this.items = items;
        this.to = to[0];
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = super.getView(position, convertView, parent);

        if(items != null && items.get(position) != null) {
            if (items.get(position).containsKey("mandatory")) {
                if (items.get(position).get("mandatory").equals("true") || items.get(position).get("mandatory").toLowerCase().equals("ano")) {
                    TextView t = (TextView) view.findViewById(to);
                    t.setTextColor(view.getContext().getResources().getColor(R.color.elogika_mandatory));
                } else {
                    TextView t = (TextView) view.findViewById(to);
                    t.setTextColor(Color.parseColor("#ffffff"));
                }
            }
            if (items.get(position).containsKey("zapocitatDoVysledku")) {
                TextView tz = (TextView) view.findViewById(R.id.cc_include_to_result_detail);
                if (items.get(position).get("zapocitatDoVysledku").equals("true")) {
                    tz.setText(R.string.yes);
                } else {
                    tz.setText(R.string.no);
                }
            }
/*
        if(items.get(position).containsKey("formaStudia")) {
            TextView tz = (TextView) view.findViewById(R.id.cc_study_form_detail);
            if (Integer.valueOf(items.get(position).get("formaStudia")) == 0) {
                tz.setText(R.string.study_form_present);
            } else {
                tz.setText(R.string.study_form_combinated);
            }
        }
*/


            //pokud je polozka aktivni...
            //prihlasit na termin
            if (items.get(position).containsKey("sign_in_from") && items.get(position).containsKey("sign_in_to") &&
                    items.get(position).containsKey("sign_off_to") && items.get(position).containsKey("signed")) {
            /*
            Calendar sign_in_from = Calendar.getInstance();
            Calendar sign_in_to = Calendar.getInstance();
            Calendar sign_off_to = Calendar.getInstance();

            try {
                sign_in_from.setTime(sdf.parse(items.get(position).get("sign_in_from")));
                sign_in_to.setTime(sdf.parse(items.get(position).get("sign_in_to")));
                sign_off_to.setTime(sdf.parse(items.get(position).get("sign_off_to")));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            */

                //Log.d("SpecialAdapterTIME", "FROM" + sdf.format(sign_in_from.getTime()));
                //Log.d("SpecialAdapterTIME", "NOW" + sdf.format(rightNow.getTime()));
                //Log.d("SpecialAdapterTIME", "TO" + sdf.format(sign_in_to.getTime()));

                //ve vychozim stavu je checkbox neaktivni
                dialog_checkbox = (CheckBox) view.findViewById(R.id.dialog_checkbox);
                //aby se nevyvolaval checkbox pri opetovnem spusteni Special adapteru
                dialog_checkbox.setOnCheckedChangeListener(null);
                dialog_checkbox.setEnabled(false);

                if (items.get(position).get("signed").equals("true")) {
                    dialog_checkbox.setChecked(true);
                    //lze se odhlasit
                    if (TimeLogika.compare(items.get(position).get("sign_off_to")) >= 0) {
                        dialog_checkbox.setEnabled(true);
                    }
                } else {
                    dialog_checkbox.setChecked(false);
                    if (TimeLogika.isActive(items.get(position).get("sign_in_from"), items.get(position).get("sign_in_to"))) {
                        //lze se prihlasit
                        dialog_checkbox.setEnabled(true);
                    } else {
                        //nelze se prihlasit
                        dialog_checkbox.setEnabled(false);
                    }
                }
/*
            dialog_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @SuppressLint("LongLogTag")
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    Log.d("setOnCheckedChangeListener", "" + isChecked);
                    if (isChecked) {
                        Fragment_RegistrationForExaminations.login(context, items.get(position).get("date_id"), items.get(position).get("name"), dialog_checkbox);
                    } else {
                        Fragment_RegistrationForExaminations.logout(context, items.get(position).get("date_id"), items.get(position).get("name"), dialog_checkbox);
                    }
                }
            });
*/

                dialog_checkbox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (((CheckBox) v).isChecked()) {
                            Fragment_RegistrationForExaminations.getInstance().login(context, items.get(position).get("date_id"), items.get(position).get("name"), ((CheckBox) v));
                        } else {
                            Fragment_RegistrationForExaminations.getInstance().logout(context, items.get(position).get("date_id"), items.get(position).get("name"), ((CheckBox) v));
                        }
                    }
                });

            /*
            if((rightNow.compareTo(sign_in_from) >= 0 && rightNow.compareTo(sign_in_to) <= 0)){
                if(items.get(position).get("signed").equals("true"))
                    view.setBackgroundColor(Color.parseColor("#1B5E20"));
                else
                    //pruhledne
                    view.setBackgroundColor(0x00000000);
            }
            else{
                if(items.get(position).get("signed").equals("true")){
                    view.setBackgroundColor(Color.parseColor("#441B5E20"));
                }
                else {
                    //pruhledne
                    view.setBackgroundColor(0x00000000);
                    //sede pismo
                }
            }*/
            }

            //Activity

            else if (items.get(position).containsKey("active_from") && items.get(position).containsKey("active_to") && items.get(position).containsKey("signed")) {
            /*
            Calendar c_active_from = Calendar.getInstance();
            Calendar c_active_to = Calendar.getInstance();

            try {
                c_active_from.setTime(sdf.parse(items.get(position).get("active_from")));
                c_active_to.setTime(sdf.parse(items.get(position).get("active_to")));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            */
                if (TimeLogika.isActive(items.get(position).get("active_from"), items.get(position).get("active_to"))) {
                    if (items.get(position).get("signed").equals("true"))
                        view.setBackgroundColor(Color.parseColor("#1B5E20"));
                    else
                        //pruhledne
                        view.setBackgroundColor(0x00000000);
                } else {
                    if (items.get(position).get("signed").equals("true")) {
                        view.setBackgroundColor(Color.parseColor("#441B5E20"));
                    } else {
                        //pruhledne
                        view.setBackgroundColor(0x00000000);
                        //sede pismo
                    }
                }
            }
        }
        return view;
    }
}