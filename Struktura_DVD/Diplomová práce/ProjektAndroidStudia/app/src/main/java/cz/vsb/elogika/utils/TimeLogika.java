package cz.vsb.elogika.utils;

import android.util.Log;
import android.widget.DatePicker;
import android.widget.TimePicker;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import cz.vsb.elogika.R;

/**
 * <pre>
 * Třída slouží pro práci s časem (převod mezi formátem serveru a formátem používaným v aplikaci,
 * zaokrouhlení času, vrácení pouze datumů, kontroly, jestli se jedná o čas serveru, porovnání časů, ...)
 *
 * ukazka pouziti:
 * Log.d("test TimeLogika", TimeLogika.currentDate());
 * Log.d("test TimeLogika", TimeLogika.currentServerTime());
 *
 * Log.d("test TimeLogika", TimeLogika.getFormatedDate("2015-01-13T18:34:06.74"));
 * Log.d("test TimeLogika", TimeLogika.getFormatedDate("2015-01-23T22:27:49.8652409+01:00"));
 * Log.d("test TimeLogika", TimeLogika.getFormatedDateTime("2015-01-13T18:34:06.74"));
 * Log.d("test TimeLogika", TimeLogika.getFormatedDateTime("2015-01-23T22:27:49.8652409+01:00"));
 *
 * Log.d("test Time0", TimeLogika.isFormated("13.01.2015") + "");
 * Log.d("test TimeLogika", TimeLogika.isFormated("13.01.2015 10:35") + "");
 * Log.d("test TimeLogika", TimeLogika.isFormatedDateTime("13.01.2015 10:35") + "");
 * Log.d("test TimeLogika", TimeLogika.isServerTime("2015-01-13T18:34:06") + "");
 * Log.d("test TimeLogika", TimeLogika.isServerTime("2015-01-13T18:34:06.74") + "");
 * Log.d("test TimeLogika", TimeLogika.isServerTime("2015-01-23T22:27:49.8652409+01:00") + "");
 *
 * Log.d("test Time2", TimeLogika.compare("13.01.2015") + "");
 * Log.d("test TimeLogika", TimeLogika.compare("13.01.2015 10:35") + "");
 * Log.d("test TimeLogika", TimeLogika.compare("2015-01-13T18:34:06.74") + "");
 * Log.d("test TimeLogika", TimeLogika.compare("2015-01-23T22:27:49.8652409+01:00") + "");
 *
 * Log.d("test Time3", TimeLogika.compare("13.02.2015") + "");
 * Log.d("test TimeLogika", TimeLogika.compare("13.02.2015 10:35") + "");
 * Log.d("test TimeLogika", TimeLogika.compare("2015-02-13T18:34:06.74") + "");
 * Log.d("test TimeLogika", TimeLogika.compare("2015-02-23T22:27:49.8652409+01:00") + "");
 *
 * Log.d("test Time4", TimeLogika.compare("27.01.2015") + "");
 *
 * Log.d("test Time5", TimeLogika.isActive("2015-01-13T22:27:49.8652409+01:00", "2015-02-13T22:27:49.8652409+01:00") + "");
 * Log.d("test TimeLogika", TimeLogika.isActive("2015-01-13T18:34:06.74", "2015-02-13T18:34:06.74") + "");
 * Log.d("test TimeLogika", TimeLogika.isActive("2015-02-13T18:34:06.74", "2015-01-13T18:34:06.74") + "");
 * Log.d("test TimeLogika", TimeLogika.isActive("2015-02-13T18:34:06.74", "2015-02-13T18:34:06.74") + "");
 * Log.d("test TimeLogika", TimeLogika.isActive("2015-01-13T18:34:06.74", "2015-01-13T18:34:06.74") + "");
 * </pre>
 */
public class TimeLogika {
    /**
     * Z času serveru vytvoří datum vhodný pro tuto aplikaci
     * @param unformatedTime čas který vrací server ve formátu "yyyy-MM-ddTHH:mm:ss.ff"
     * @return datum ve formátu "dd.MM.yyyy"
     */
    public static String getFormatedDate(String unformatedTime)
    {
        //nevim jestli muze prijit i neco jinyhho...?
        if(unformatedTime.equals("") || unformatedTime.equals(null) || unformatedTime.equals("null")){
            return unformatedTime;
        }
        String date[] = unformatedTime.split("T");
        String a[] = date[0].split("-");
        return  a[2]+"."+a[1]+"."+a[0];
    }

    /**
     * Z času reprezentovaného v aplikaci vrací pouze datum
     * @param formatedTime čas ve formátu "dd.MM.yyyy HH:mm"
     * @return datum ve formátu "dd.MM.yyyy"
     */
    public static String getDate(String formatedTime)
    {
        String date[] = formatedTime.split(" ");
        return  date[0];
    }

    /**
     * převede čas ve formě, v jaké jej reprezentuje server na čas, který je reprezentován uvnitř aplikace
     * @param unformatedTime čas který vrací server ve formátu "yyyy-MM-ddTHH:mm:ss.ff"
     * @return čas ve formátu "dd.MM.yyyy HH:mm"
     */
    public static String getFormatedDateTime(String unformatedTime)
    {
        //nevim jestli muze prijit i neco jinyhho...?
        if(unformatedTime.equals("") || unformatedTime.equals(null) || unformatedTime.equals("null")){
            return unformatedTime;
        }
        String date[] = unformatedTime.split("T");
        String a[] = date[0].split("-");
        String b[] = date[1].split(":");
        return  a[2]+"."+a[1]+"."+a[0] + " " + b[0]+":" + b[1];
    }


    /**
     * z času ve tvaru serveru vrací čas bez datumu
     * @param unformatedTime čas který vrací server ve formátu "yyyy-MM-ddTHH:mm:ss.ff"
     * @return čas ve tvaru "HH:mm:ss"
     */
    public static String getFormatedTime(String unformatedTime)
    {
        //nevim jestli muze prijit i neco jinyhho...?
        if(unformatedTime.equals("") || unformatedTime.equals(null) || unformatedTime.equals("null")){
            return unformatedTime;
        }
        String date[] = unformatedTime.split("T");
        String b[] = date[1].split(":");
        return  b[0]+ ":" + b[1]+ ":" +b[2].substring(0,2);
    }

    /**
     * z času ve formátu "dd.MM.yyyy HH:mm" vrací čas bez datumu
     * @param formatedTime čas ve formátu "dd.MM.yyyy HH:mm"
     * @return čas ve tvaru "HH:mm:ss"
     */
    public static String getTime(String formatedTime)
    {
        String date[] = formatedTime.split(" ");
        String b[] = date[1].split(":");
        if(b.length > 2) {
            return b[0] + ":" + b[1] + ":" + b[2].substring(0, 2);
        }
        else{
            return b[0] + ":" + b[1];
        }
    }

    /**
     * převod milisekund na čas
     * @param milis číslo reprezentující počet milisekund
     * @return čas ve tvaru "HH:mm:ss"
     */
    public static String getTimeFromMilis(long milis)
    {
        int secUntilFinished = (int) (milis/1000);
        int a = secUntilFinished/ (60*60) %24;
        int b = secUntilFinished/ 60 % 60;
        int c = secUntilFinished % 60;
        String h,m,s;
        if (a<10)
            h = "0" + String.valueOf(a);
        else h = String.valueOf(a);
        if (b<10)
            m = "0" + b;
        else m = String.valueOf(b);
        if (c<10)
            s = "0" + c;
        else s = String.valueOf(c);
        String cas = h +":"+ m +":"+ s;

        //Log.d("navracen čas", h +":"+ m +":"+ s);
        return  cas;
    }

    /**
     * K zadanému času ve tvaru serveru přičte zadaný počet minut a vrací jej ve tvaru serveru
     * @param time čas ve formátu serveru "yyyy-MM-ddTHH:mm:ss.ff"
     * @param minutes přirozené číslo reprezentující počet minut
     * @return čas ve formátu serveru "yyyy-MM-ddTHH:mm:ss.ff" přičtený o "minutes" minut
     */
    public static String timePlusMinutes(String time , int minutes)
    {
        int minuty = minutes%60;
        int hodiny = minutes/60;
        String s1[] = time.split("T");
        String s2[] = s1[1].split(":");

        int hod = Integer.parseInt(s2[0]);
        int min = Integer.parseInt(s2[1]);
        hod = (hod+hodiny + (min+minuty)/60)%24;
        min = (min+minuty)%60;

        Log.d("puvodni cas", time);
        try{
        Log.d("přičtený cas", s1[0]+"T"+hod+":"+min+":"+s2[2]+":"+s2[3]);
        return s1[0]+"T"+hod+":"+min+":"+s2[2]+":"+s2[3];
        }catch(Exception e){
        return s1[0]+"T"+hod+":"+min+":"+s2[2];
        }
    }

    /**
     * zjišťuje, zda je zadaný čas v požadovaném tvaru
     * @param time řetězec reprezentující čas
     * @return vrací true, pokud je čas ve tvaru "dd.MM.yyyy HH:mm" nebo "dd.MM.yyyy"
     */
    public static boolean isFormated(String time) {
        //na konci by mel byt otaznik, ale nejede mi to s nim...
        return time.matches("\\d{2}.\\d{2}.\\d{4}[[ ]\\d{2}:\\d{2}]*");
    }

    /**
     * zjišťuje, zda je zadaný čas v požadovaném tvaru
     * @param time řetězec reprezentující čas
     * @return vrací true, pokud je čas ve tvaru "dd.MM.yyyy"
     */
    public static boolean isFormatedDate(String time) {
        return time.matches("\\d{2}.\\d{2}.\\d{4}");
    }

    /**
     * zjišťuje, zda je zadaný čas v požadovaném tvaru
     * @param time řetězec reprezentující čas
     * @return vrací true, pokud je čas ve tvaru "dd.MM.yyyy HH:mm"
     */
    public static boolean isFormatedDateTime(String time) {
        return time.matches("\\d{2}.\\d{2}.\\d{4}[ ]\\d{2}:\\d{2}");
    }

    /**
     * zjišťuje, zda je zadaný čas v požadovaném tvaru
     * @param time řetězec reprezentující čas
     * @return vrací true, pokud je čas ve tvaru "yyyy-MM-ddTHH:mm:ss.[f]*"
     */
    public static boolean isServerTime(String time){
        return time.matches("\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}.?[\\d*[+-:[0-9]]*]*");
    }


    //-1 ? datum uz byl
    //0 ? datum je ted
    //1 ? datum teprve bude
    //-2 ? datum je ve spatnem formatu

    /**
     * <pre>
     * Porovnává datum s aktuálním
     * -1 ? datum uz byl
     * 0 ? datum je ted
     * 1 ? datum teprve bude
     * -2 ? datum je ve špatném formátu
     * </pre>
     * @param date datum ve formátu "dd.MM.yyyy", "dd.MM.yyyy HH:mm" nebo "yyyy-MM-ddTHH:mm:ss.ff"
     * @return -1, 0, 1 nebo -2
     */
    public static int compare(String date){
        DateFormat sdf;
        Date given = new Date();
        Date now = new Date();

        Calendar cal = Calendar.getInstance();
        cal.setTime(now);

        //odstrihnu vteriny a milisekundy, protoze je nepouzivame
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        if(isFormatedDate(date)){
            sdf = new SimpleDateFormat("dd.MM.yyyy");
            //v tomto pripade se odstrihnou i hodiny a minuty
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
        }
        else if(isFormatedDateTime(date)){
            sdf = new SimpleDateFormat("dd.MM.yyyy' 'HH:mm");
        }
        else if(isServerTime(date)){
            date = getFormatedDateTime(date);
            sdf = new SimpleDateFormat("dd.MM.yyyy' 'HH:mm");
        }
        else {
            //cas je ve spatnem formatu
            return -2;
        }
        try {
            now = cal.getTime();
            given = sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //Log.d("TimeLogika", "given: " + given.toString() + "now: " + now.toString());
        if(given.before(now)){
            return -1;
        }
        else if(given.after(now)){
            return 1;
        }
        else{
            return 0;
        }
    }


    //-1 ? datum uz byl
    //0 ? datum je ted
    //1 ? datum teprve bude
    //-2 ? datum je ve spatnem formatu

    /**
     * <pre>
     * Porovnává 2 datumy
     * -1 ? datum date1 je dříve než datum date2
     * 0 ? datumy jsou stejné
     * 1 ? datum date1 je později než datum date2
     * -2 ? datum je ve špatném formátu
     * </pre>
     * @param date1 datum ve formátu "dd.MM.yyyy", "dd.MM.yyyy HH:mm" nebo "yyyy-MM-ddTHH:mm:ss.ff"
     * @param date2 datum ve stejném formátu jako date1
     * @return -1, 0, 1 nebo -2
     */
    public static int compare(String date1, String date2){
        DateFormat sdf;
        Date first = new Date();
        Date second = new Date();

        if(isFormatedDate(date1)){
            sdf = new SimpleDateFormat("dd.MM.yyyy");
        }
        else if(isFormatedDateTime(date1)){
            sdf = new SimpleDateFormat("dd.MM.yyyy' 'HH:mm");
        }
        else if(isServerTime(date1)){
            date1 = getFormatedDateTime(date1);
            date2 = getFormatedDateTime(date2);
            sdf = new SimpleDateFormat("dd.MM.yyyy' 'HH:mm");
        }
        else {
            //cas je ve spatnem formatu
            return -2;
        }
        try {
            first = sdf.parse(date1);
            second = sdf.parse(date2);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //Log.d("TimeLogika", "given: " + given.toString() + "now: " + now.toString());
        if(first.before(second)){
            return -1;
        }
        else if(second.after(first)){
            return 1;
        }
        else{
            return 0;
        }
    }
    /**
     * <pre>
     * Porovnává 2 časy
     * -1 ? čas 1 je dřívější než čas 2
     * 0 ? časy jsou stejné
     * 1 ? čas 1 je pozdější než čas 2
     * </pre>
     * @param time1 čas ve formátu "HH:mm"
     * @param time2 čas ve formátu "HH:mm"
     * @return -1, 0, 1 nebo -2
     */
    public static int compareTime(String time1, String time2){
        String splitted_time1[] = time1.split(":");
        String splitted_time2[] = time2.split(":");
        int hour1 = Integer.valueOf(splitted_time1[0]);
        int hour2 = Integer.valueOf(splitted_time2[0]);
        int minute1 = Integer.valueOf(splitted_time1[1]);
        int minute2 = Integer.valueOf(splitted_time2[1]);

        if(hour1 == hour2){
            if(minute1 == minute2){
                ///casy jsou stejne
                return 0;
            }
            else if(minute1 < minute2){
                //prvni cas je drivejsi
                return -1;
            }
            //minute1 > minute2
            else{
                //prvni cas je pozdejsi nez druhy
                return 1;
            }
        }
        else if(hour1 < hour2){
            //prvni cas je drivejsi
            return -1;
        }
        //hour1 > hour2
        else{
            //prvni cas je pozdejsi
            return 1;
        }
    }

    //lze pouzit cas bud ve tvaru serveru, nebo aplikace

    /**
     * Zjišťuje, jestli zadaný čas "activeFrom" je před aktuálním a "activeTo" je za aktuálním
     * @param activeFrom čas ve tvaru serveru nebo aplikace
     * @param activeTo čas ve tvaru serveru nebo aplikace
     * @return "true" pokud je čas "activeFrom" před aktuálním a "activeTo" za aktuálním. Jinak vrací "false"
     */
    public static boolean isActive(String activeFrom, String activeTo){
        int from = compare(activeFrom);
        int to = compare(activeTo);
        if((from == 0 || from == -1) && (to >= 0)){
            return true;
        }
        return false;
    }

    /**
     * Vrací aktuální čas ve formátu serveru
     * @return čas ve formátu serveru
     */
    public static String currentServerTime(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.'0000000'ZZZZZ");
        return sdf.format(new Date());
    }

    public static String currentServerTimeWithoutDate(){
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        return sdf.format(new Date());
    }

    public static String getTime(){
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        now = cal.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.'0000000'ZZZZZ");
        return sdf.format(now);
    }

    /**
     * Vrací zadaný čas ve formátu serveru
     * @param formatedTime čas ve formátu aplikace
     * @return čas ve formátu serveru
     */
    public static String getServerTimeFromFormated(String formatedTime){
        SimpleDateFormat sdf_in;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.'0000000'ZZZZZ");
        if(isFormatedDate(formatedTime)){
            sdf_in = new SimpleDateFormat("dd.MM.yyyy");
        }
        else if(isFormatedDateTime(formatedTime)){
            sdf_in = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        }
        else{
            Log.d("TimeLogika.getServerTimeFromFormated", "wrong format");
            return formatedTime;
        }

        try {
            return sdf.format(sdf_in.parse(formatedTime));
        } catch (ParseException e) {
            Log.d("TimeLogika.getServerTimeFromFormated", e.getMessage());
        }
        return formatedTime;
    }

    /**
     * Vrací aktuální datum ve formátu "dd.MM.yyyy"
     * @return aktuální datum ve formátu "dd.MM.yyyy"
     */
    public static String currentDate(){
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        return sdf.format(new Date());
    }

    /**
     * Vrací pole hodnot typu int: {rok, mesic, den, hodina, minuta} kde leden == 0
     * @param formatedTime čas ve formátu aplikace "dd.MM.yyyy HH:mm" nebo "dd.MM.yyyy"
     * @return [yyyy, MM, dd, HH, mm]
     */
    public static int[] getSeparatedTime(String formatedTime){
        if(!isFormated(formatedTime)){
            return null;
        }

        String[] time_date = formatedTime.split(" ");

        String[] date = time_date[0].split("\\.");
        int year = Integer.valueOf(date[2]);
        int month = Integer.valueOf(date[1]);
        int day = Integer.valueOf(date[0]);

        String[] time = time_date[1].split(":");
        int hour = Integer.valueOf(time[0]);
        int minute = Integer.valueOf(time[1]);
        int[] separatedTime = new int[5];
        separatedTime[0] = year;
        separatedTime[1] = month;
        separatedTime[2] = day;
        separatedTime[3] = hour;
        separatedTime[4] = minute;
        return separatedTime;
    }

    /**
     * Pro vybrané komponenty sestaví čas ve formátu aplikace
     * @param datePicker komponenta s reprezentací datumu
     * @param timePicker komponenta s reprezentací času
     * @return čas ve formátu aplikace - "dd.MM.yyyy HH:mm"
     */
    public static String getDateFromPickers(DatePicker datePicker, TimePicker timePicker){

        int day = datePicker.getDayOfMonth();
        int month = datePicker.getMonth();
        int year = datePicker.getYear();
        int hour = timePicker.getCurrentHour();
        int minute = timePicker.getCurrentMinute();


        String dayS = String.valueOf(day);
        if(day<10)
            dayS="0"+day;

        //mesic + 1, protoze je pocitan od 0
        String monthS = String.valueOf(month+1);
        if(month<10)
            monthS="0"+monthS;

        String yearS = String.valueOf(year);

        String hourS = String.valueOf(hour);
        if(hour<10)
            hourS="0"+hour;

        String minuteS = String.valueOf(minute);
        if(minute<10)
            minuteS="0"+minute;

        return dayS + "." + monthS + "." + yearS + " " + hourS + ":" + minuteS;
    }

    /**
     * Pro zadaný čas ve formátu serveru vrací o jaký den v týdnu se jedná
     * @param time čas ve formátu "yyyy-MM-dd'T'HH:mm:ss"
     * @return vrací číslo reprezentující den v týdnu
     */
    public static int getDayInWeek(String time)
    {
        try
        {
            Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(time);
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            int day = c.get(Calendar.DAY_OF_WEEK) - 2;
            if (day < 0) day += 7;
            return day;
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        return -1;
    }


    /**
     * Pro zadaný den vrací identifikátor dne v týdnu jazyka koncového zařízení
     * @param day pořadí dne v týdnu
     * @return den v týdnu jako R.string.*
     */
    public static int getNameOfDayInWeek(int day)
    {
        switch (day)
        {
            case 0:
                return R.string.monday;

            case 1:
                return R.string.tuesday;

            case 2:
                return R.string.wednesday;

            case 3:
                return R.string.thursday;

            case 4:
                return R.string.friday;

            case 5:
                return R.string.saturday;

            case 6:
                return R.string.sunday;

            default:
                return -1;
        }
    }
}
