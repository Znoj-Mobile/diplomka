package cz.vsb.elogika.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;

import cz.vsb.elogika.R;

public class TimePickerDialogButton_HMS extends Button{

    private int hours = 0, minutes = 1,seconds = 0;

    public TimePickerDialogButton_HMS(Context context, AttributeSet attrs) {
        super(context,attrs);
        init(context);
        refrestText();
    }
    public TimePickerDialogButton_HMS(final Context context){
        super(context);
        init(context);
        refrestText();
    }

    public void setTime(int seconds)
    {
        this.hours = seconds / 3600;
        this.minutes = (seconds%3600) / 60;
        this.seconds = seconds % 60;
    }
    public void setTime(int hours, int minutes, int seconds)    {
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
    }
    public int getTimeInSeconds(){
        return ((hours*60) + minutes)*60+seconds;
    }

    private void init(final Context context) {
        this.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final View hms = inflate(context, R.layout.dialog_hms_picker, null);
                final NumberPicker h = (NumberPicker) hms.findViewById(R.id.hms_picker_h);
                final NumberPicker m = (NumberPicker) hms.findViewById(R.id.hms_picker_m);
                final NumberPicker s = (NumberPicker) hms.findViewById(R.id.hms_picker_s);
                h.setMaxValue(24);
                m.setMaxValue(59);
                s.setMaxValue(59);
                h.setValue(hours);
                m.setValue(minutes);
                s.setValue(seconds);
                new AlertDialog.Builder(context)
                        .setTitle(R.string.time_setup)
                        .setView(hms)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                hours = h.getValue();
                                minutes = m.getValue();
                                seconds = s.getValue();
                                refrestText();
                            }
                        })
                        .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                //  hide
                            }
                        })
                        .setIcon(android.R.drawable.ic_menu_recent_history)
                        .show();
            }
        });
    }


    private void refrestText(){
        String s;
        if(hours != 0){
            s = hours + "h " + minutes + "m " + seconds + "s";
        }
        else if (minutes != 0){
            s = minutes + "m " + seconds + "s";
        }
        else{
            s = seconds + "s";
        }
        this.setText(s);
    }


}
