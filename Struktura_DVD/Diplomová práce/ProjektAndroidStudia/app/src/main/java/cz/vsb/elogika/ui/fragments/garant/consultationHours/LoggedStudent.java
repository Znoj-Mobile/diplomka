package cz.vsb.elogika.ui.fragments.garant.consultationHours;

public class LoggedStudent
{
    public boolean isChecked;
    public int id;
    public String student;
    public String email;
    public String loginDate;
    public String reason;
}