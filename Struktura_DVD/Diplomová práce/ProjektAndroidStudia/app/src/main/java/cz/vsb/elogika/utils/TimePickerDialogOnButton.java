package cz.vsb.elogika.utils;

import android.app.TimePickerDialog;
import android.view.View;
import android.widget.Button;
import android.widget.TimePicker;

import java.util.Calendar;
import java.util.TimeZone;

import cz.vsb.elogika.R;

public class TimePickerDialogOnButton
{
    public int hour;
    public int minute;
    private boolean isAlreadySet;
    private static Calendar currentTime;
    private Button btn;

    /**
     * Nastaví aktuální čas.
     * Nastaví na tlačítko listener, který vyvolá dialog pro nastavení času.
     * Nastaví aktuální hodinu.
     */
    private void initialize(){
        currentTime = Calendar.getInstance();
        currentTime.setTimeZone(TimeZone.getDefault());
        this.isAlreadySet = false;
        //pro potreby noveho konstruktoru bez nastaveni pocatecniho casu
        btn.setText(R.string.time);
        setListener(btn);
    }

    /**
     * Volá metodu pro inicializaci.
     * Nastaví aktuální hodinu.
     * @param button tlačítko, na které se má aplikovat listener
     */
    public TimePickerDialogOnButton(Button button)
    {
        this.btn = button;
        initialize();
        setHour();
    }


    /**
     * Volá metodu pro inicializaci.
     * Nastaví aktuální hodinu a minutu podle zadaných parametrů.
     * @param button tlačítko, na které se má aplikovat listener
     * @param hour inicializační hodina
     * @param minute inicializační minuta
     */
    public TimePickerDialogOnButton(Button button, int hour, int minute)
    {
        this.btn = button;
        initialize();
        this.hour = hour;
        this.minute = minute;
        this.btn.setText(getTime(this.hour, this.minute));
    }



    /**
     * Už uživatel nastavil čas?
     * @return true - ano, nastavil; false - ne, nenastavil
     */
    public boolean isAlreadySet()
    {
        return isAlreadySet;
    }



    /**
     * Nastaví hodinu i minutu.
     * @param time čas musí být ve formátu hh:mm
     */
    public void setTime(String time)
    {
        String[] parts = time.split(":");
        this.hour = Integer.parseInt(parts[0]);
        this.minute = Integer.parseInt(parts[1]);
        btn.setText(getTime(this.hour, this.minute));
    }

    /**
     * Nastaví hodinu i minutu.
     * @param hour hodina
     * @param minute minuta
     */
    public void setTime(int hour, int minute)
    {
        this.hour = hour;
        this.minute = minute;
        btn.setText(getTime(this.hour, this.minute));
    }

    /**
     * Nastaví aktuální hodinu i minutu.
     */
    public void setTime()
    {
        this.hour = currentTime.get(Calendar.HOUR_OF_DAY);
        this.minute = currentTime.get(Calendar.MINUTE);
        btn.setText(getTime(this.hour, this.minute));
    }

    /**
     * Nastaví aktuální hodinu.
     */
    public void setHour()
    {
        this.hour = currentTime.get(Calendar.HOUR_OF_DAY);
        this.minute = 0;
        btn.setText(getTime(this.hour, this.minute));
    }

    /**
     * Vrátí nastavenou hodinu.
     */
    public int getHour()
    {
        return this.hour;
    }

    /**
     * Nastaví komponentu pro čas i pro datum z času serveru
     * @param dpdb instance komponenty pro nastavení datumu
     * @param dateFromServer datum ve formátu serveru
     */
    public void setDateTime(DatePickerDialogOnButton dpdb, String dateFromServer)
    {
        //nastavi hodinu a minutu
        if (TimeLogika.isFormated(dateFromServer)) {

            this.setTime(TimeLogika.getFormatedTime(dateFromServer));
        }
        else if (TimeLogika.isServerTime(dateFromServer)){
            String date[] = dateFromServer.split("T");
            this.setTime(TimeLogika.getFormatedTime(date[1]));
        }
        //nejakej chybnej cas - na ten si dát pozor, takze to zatim bude padat :-)
        else {
            this.setTime(TimeLogika.getFormatedTime(dateFromServer));
        }
        dpdb.setDate(dateFromServer);
    }

    /**
     *
     * @return vrátí čas ve formátu hh:mm
     */
    public String getTime()
    {
        return getTime(this.hour, this.minute);
    }

    /**
     * Vrací kompletni datum s časem
     * @return vrátí datum ve formátu yyyy-MM-dd'T'kk:mm:ss
     */
    public String getDate(DatePickerDialogOnButton dpdb)
    {
        return getDate(dpdb.year, dpdb.month, dpdb.day, this.hour, this.minute);
    }

    private void setListener(final Button button)
    {
        button.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                TimePickerDialog.OnTimeSetListener listener = new TimePickerDialog.OnTimeSetListener()
                {
                    public void onTimeSet(TimePicker view, int hourOfDay, int minuteOfHour)
                    {
                        isAlreadySet = true;
                        hour = hourOfDay;
                        minute = minuteOfHour;
                        button.setText(getTime(hour, minute));
                    }
                };
                new TimePickerDialog(button.getContext(), listener, hour, minute, true).show();
            }
        });
    }



    private String getTime(int hours, int minutes)
    {
        return ((hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes));
    }

    private String getDate(int year, int month, int day, int hours, int minutes)
    {
        return year + "-" + (month < 10 ? "0" + month : month) + "-" + (day < 10 ? "0" + day : day) + (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes);
    }
}
