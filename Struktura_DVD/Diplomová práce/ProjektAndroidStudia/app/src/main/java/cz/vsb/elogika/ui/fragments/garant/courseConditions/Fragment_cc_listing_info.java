package cz.vsb.elogika.ui.fragments.garant.courseConditions;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;

import org.json.JSONException;
import org.json.JSONObject;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.DownloadFile;

public class Fragment_cc_listing_info extends Fragment {

    View rootView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_cc_listing_info, container, false);

        setDataFromCCterm();
        return rootView;
    }

    private void setDataFromCCterm() {
        TextView ccl_activity_title_and_type = (TextView) rootView.findViewById(R.id.ccl_activity_title_and_type);
        String s = getArguments().getString("nazevTyp");
        ccl_activity_title_and_type.setText(s);

        TextView ccl_term_title = (TextView) rootView.findViewById(R.id.ccl_term_title);
        ccl_term_title.setText(getArguments().getString("nazev"));

        TextView ccl_start_time = (TextView) rootView.findViewById(R.id.ccl_start_time);
        ccl_start_time.setText(getArguments().getString("aktivniOD"));

        TextView ccl_end_time = (TextView) rootView.findViewById(R.id.ccl_end_time);
        ccl_end_time.setText(getArguments().getString("aktivniDO"));

        final EditText ccl_minimal_points_for_showing = (EditText) rootView.findViewById(R.id.ccl_minimal_points_for_showing);

        Button ccl_minimal_points_btn = (Button) rootView.findViewById(R.id.ccl_minimal_points_btn);
        ccl_minimal_points_btn.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                String value = String.valueOf(ccl_minimal_points_for_showing.getText());
                if(value != null){
                    try{
                        int i = Integer.valueOf(value);
                        Fragment_cc_listing_students.setMinPoints(String.valueOf(i));
                        Log.d("Fragment_cc_listnig_info", Fragment_cc_listing_students.minPoints);
                    }
                    catch(NumberFormatException e){
                        Log.e("Fragment_cc_listnig_info", "NumberFormatException: " + e.getMessage());
                    }
                }
            }
        });

        Button ccl_print_studets_qr_codes = (Button) rootView.findViewById(R.id.ccl_print_studets_qr_codes);
        ccl_print_studets_qr_codes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SnackbarManager.show(
                        Snackbar.with(getActivity())
                                .type(SnackbarType.MULTI_LINE)
                                .duration(Snackbar.SnackbarDuration.LENGTH_INDEFINITE)
                                .text(getResources().getString(R.string.file_is_downloaded)));
                print_qr_codes();
            }
        });

        Button ccl_print_students_list = (Button) rootView.findViewById(R.id.ccl_print_students_list);
        ccl_print_students_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SnackbarManager.show(
                        Snackbar.with(getActivity())
                                .type(SnackbarType.MULTI_LINE)
                                .duration(Snackbar.SnackbarDuration.LENGTH_INDEFINITE)
                                .text(getResources().getString(R.string.file_is_downloaded)));
                print_list_of_students();
            }
        });



        Button ccl_history = (Button) rootView.findViewById(R.id.ccl_history);
        ccl_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment_cc_listing_students.go_to_history();
            }
        });

        Button ccl_study_form = (Button) rootView.findViewById(R.id.ccl_study_form);
        ccl_study_form.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
    }

    private void login() {
        Response response = new Response() {
            @SuppressLint("LongLogTag")
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                Log.d("typ response: ", response.getString("Success"));
                if (response.getString("Success").equals("true")) {
                    //novej toast
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.students_are_logged_id)));
                } else { //nepovedlo se
                    Log.d("Fragment_cc_listing_info", "login(): " + response.toString());
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text( getActivity().getString(R.string.students_are_not_logged_in)));
                }
            }
        };

        String fulltime = "false";
        String combined = "false";
        boolean set = false;
        CheckBox ccl_checkbox_present = (CheckBox) rootView.findViewById(R.id.ccl_checkbox_present);
        CheckBox ccl_checkbox_combinate = (CheckBox) rootView.findViewById(R.id.ccl_checkbox_combinate);
        if(ccl_checkbox_present.isChecked()){
            fulltime = "true";
            set = true;
        }
        if(ccl_checkbox_combinate.isChecked()){
            combined = "true";
            set = true;
        }

        if(set) {
            Request.post(URL.Date.LoginAllStudentsToDate(User.courseInfoId, Fragment_cc_listing_students.idTermin, fulltime, combined), response);
        }
        else{
            SnackbarManager.show(
                    Snackbar.with(getActivity())
                            .text( getActivity().getString(R.string.present_or_combinate_has_to_be_checked)));
        }
    }

    private void print_qr_codes() {
        Response response = new Response()
        {
            @SuppressLint("LongLogTag")
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                if (response.length() > 0)
                {
                    String fileName = "qr_codes_term_" + Fragment_cc_listing_students.idTermin + "_schoolInfoId_" + User.schoolInfoID + ".pdf";

                    Intent intent = DownloadFile.download(fileName, response.getString("QRPdf"));
                    getActivity().startActivity(intent);
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .type(SnackbarType.MULTI_LINE)
                                    .duration(Snackbar.SnackbarDuration.LENGTH_INDEFINITE)
                                    .text(getResources().getString(R.string.file) + " '" + fileName + "' " + getResources().getString(R.string.is_downloaded_in_download_folder)));
                }
                else{
                    Log.d("Fragment_cc_listing_info", "print_qr_codes, response.length() <= 0");
                }
            }
        };

        Request.get(URL.Date.GetStudentsQRCodes(Fragment_cc_listing_students.idTermin, String.valueOf(User.schoolInfoID)), response);
    }

    private void print_list_of_students() {
        Response response = new Response()
        {
            @SuppressLint("LongLogTag")
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                if (response.length() > 0)
                {
                    String fileName = "list_of_students_term_" + Fragment_cc_listing_students.idTermin + "_schoolInfoId_" + User.schoolInfoID + ".pdf";

                    Intent intent = DownloadFile.download(fileName, response.getString("Pdf"));
                    getActivity().startActivity(intent);
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .type(SnackbarType.MULTI_LINE)
                                    .duration(Snackbar.SnackbarDuration.LENGTH_INDEFINITE)
                                    .text(getResources().getString(R.string.file) + " '" + fileName + "' " + getResources().getString(R.string.is_downloaded_in_download_folder)));
                }
                else{
                    Log.d("Fragment_cc_listing_info", "print_list_of_students, response.length() <= 0");
                }
            }
        };

        Request.get(URL.Date.GetStudentspdf(Fragment_cc_listing_students.idTermin, String.valueOf(User.schoolInfoID)), response);
    }

}
