package cz.vsb.elogika.ui.fragments.garant.courseConditions;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.utils.Actionbar;
import cz.vsb.elogika.utils.TimeLogika;

public class Fragment_cc_sum_detail extends Fragment {
    private View rootView;
    private ListView listView;
    private ListView listViewSummary;

    private ProgressBar progressDialog;
    LinearLayout nothinglayout;

    SimpleAdapter adapter;
    SimpleAdapter adapterSummary;

    ArrayList<Map<String, String>> cc_sum_detail_list;
    ArrayList<Map<String, String>> cc_sum_detail_list_summary;

    String idSkupinaAktivit;
    boolean course_tabs = false;
    String userId;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_course_conditions_sum_detail, container, false);

        try {
            if(getArguments().getString("back").equals("course_tabs")){
                course_tabs = true;
            }
        }
        catch(Exception e){
            course_tabs = false;
        }

        if(course_tabs){
        }
        //souhrn pro vybranou skupinu aktivit
        else {
            Actionbar.addSubcategory(R.string.sum_detail_of_result_activity_group, this);
            //setActionBar();
        }

        listView = (ListView) rootView.findViewById(R.id.cc_sum_det_listView);
        listViewSummary = (ListView) rootView.findViewById(R.id.cc_sum_det_sum_listView);
        idSkupinaAktivit = getArguments().getString("idSkupinaAktivit");
        userId = getArguments().getString("userId");

        progressDialog = (ProgressBar) rootView.findViewById(R.id.cc_det_term_progress);
        nothinglayout = (LinearLayout) rootView.findViewById(R.id.cc_det_term_nothing_to_show);
        nothinglayout.setVisibility(View.GONE);

        cc_sum_detail_list_summary = new ArrayList<Map<String, String>>();
        cc_sum_detail_list = Data_into_sum_detail_list();

        String[] from = {"nazevAktivita", "datumVlozeni", "minMax", "vysledekRound"};
        int[] to = {R.id.cc_sum_det_title, R.id.cc_sum_det_insert_date, R.id.cc_sum_det_minmax, R.id.cc_sum_det_result};

        adapter = new SimpleAdapter(getActivity(), cc_sum_detail_list, R.layout.fragment_course_conditions_sum_detail_list, from, to);

        String[] fromBold = {"nazevAktivita", "datumVlozeni", "minMax", "vysledekRound", "prumerTrida", "prumerRocnik"};
        int[] toBold = {R.id.bcc_sum_det_title, R.id.bcc_sum_det_insert_date, R.id.bcc_sum_det_minmax, R.id.bcc_sum_det_result, R.id.bcc_sum_det_avg_class, R.id.bcc_sum_det_avg_year};
        adapterSummary = new SimpleAdapterSum(getActivity(), cc_sum_detail_list_summary, R.layout.fragment_course_conditions_sum_detail_list_bold, fromBold, toBold);

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(R.string.choose_result);
                builder.setCancelable(false);
                builder.setMessage(R.string.choose_result_question);
                builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog2, int which) {
                        choose_result(cc_sum_detail_list.get(position).get("idHodnoceniTest"));
                    }
                });
                builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog2, int which) {
                        ;
                    }
                }).setIcon(android.R.drawable.ic_dialog_alert);
                Dialog d = builder.create();
                d.show();
            }
        });

        listViewSummary.setAdapter(adapterSummary);

        return rootView;
    }

    private void setActionBar() {
        final ActionBar ab = getActivity().getActionBar();
        if (ab != null) {
            ab.setDisplayShowHomeEnabled(false);
            ab.setDisplayShowTitleEnabled(false);
        }

        LayoutInflater mInflater = LayoutInflater.from(getActivity());
        final View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        TextView mTitleTextView = (TextView) mCustomView.findViewById(R.id.title_text);
        mTitleTextView.setText(getResources().getString(R.string.sum_detail_of_result_activity_group));

        ImageButton imageButton = (ImageButton) mCustomView.findViewById(R.id.imageButton);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("idSkupinaAktivit", getArguments().getString("idSkupinaAktivit"));
                //zpet na patricnou zalozku
                bundle.putString("tab", getArguments().getString("tab"));
                Fragment_cc_summary fragment_cc_summary = new Fragment_cc_summary();
                fragment_cc_summary.setArguments(bundle);
                getFragmentManager().beginTransaction().replace(R.id.container_middle_frame, fragment_cc_summary).commit();
            }
        });
        if (ab != null) {
            ab.setCustomView(mCustomView);
            ab.setDisplayShowCustomEnabled(true);
        }
    }

    private void choose_result(final String evaluationTestId) {
        Response response = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                Log.d("typ response: ", response.getString("Success"));
                if (response.getString("Success").equals("true")) {
                    //novej toast
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.prioritize_test_result) + " " + evaluationTestId + " " + getActivity().getString(R.string.priority_was_updated)));
                    adapter.notifyDataSetChanged();
                }
                else{ //nepovedlo se
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.prioritize_test_result) + " " + evaluationTestId + " " + getActivity().getString(R.string.priority_was_not_updated)));
                }
            }
        };
        JSONObject parameters = new JSONObject();
        Request.post(URL.EvaluationTest.PrioritizeEvaluationTest(evaluationTestId), parameters, response);
    }

    private ArrayList<Map<String, String>> Data_into_sum_detail_list() {
        final ArrayList<Map<String, String>> list = new ArrayList<Map<String, String>>();
        final ArrayList<String> listPom = new ArrayList<>();

        ArrayResponse arrayResponse = new ArrayResponse() {
            @Override
            public void onResponse(JSONArray response) throws JSONException {
                if (response.length() > 0) {
                    for (int j = 0; j < response.length(); j++) {
                        list.add(put_data(
                                response.getJSONObject(j).getString("IDAktivita"),
                                response.getJSONObject(j).getString("IDTest"),
                                response.getJSONObject(j).getString("IDHodnoceniTest"),
                                response.getJSONObject(j).getString("NazevAktivita"),
                                response.getJSONObject(j).getString("ImageURL"),
                                TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("DatumVlozeni")),
                                response.getJSONObject(j).getString("Minimum"),
                                response.getJSONObject(j).getString("Maximum"),
                                response.getJSONObject(j).getString("Vysledek"),
                                TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("DatumPlatnosti")),
                                TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("PosledniZmena")),
                                response.getJSONObject(j).getString("Zapocitat"),
                                response.getJSONObject(j).getString("Splnil"),
                                response.getJSONObject(j).getString("IDZapsal"),
                                response.getJSONObject(j).getString("Jmeno"),
                                response.getJSONObject(j).getString("MinMax"),
                                response.getJSONObject(j).getString("Prijmeni"),
                                response.getJSONObject(j).getString("TitulPred"),
                                response.getJSONObject(j).getString("TitulZa"),
                                response.getJSONObject(j).getString("JmenoPosledniZmena"),
                                response.getJSONObject(j).getString("VysledekRound"),
                                response.getJSONObject(j).getString("PrumerTrida"),
                                response.getJSONObject(j).getString("PrumerRocnik")));
                        if(response.getJSONObject(j).getString("Zapocitat").equals("true")){
                            cc_sum_detail_list_summary.add(put_data(
                                    response.getJSONObject(j).getString("IDAktivita"),
                                    response.getJSONObject(j).getString("IDTest"),
                                    response.getJSONObject(j).getString("IDHodnoceniTest"),
                                    response.getJSONObject(j).getString("NazevAktivita"),
                                    response.getJSONObject(j).getString("ImageURL"),
                                    TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("DatumVlozeni")),
                                    response.getJSONObject(j).getString("Minimum"),
                                    response.getJSONObject(j).getString("Maximum"),
                                    response.getJSONObject(j).getString("Vysledek"),
                                    TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("DatumPlatnosti")),
                                    TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("PosledniZmena")),
                                    response.getJSONObject(j).getString("Zapocitat"),
                                    response.getJSONObject(j).getString("Splnil"),
                                    response.getJSONObject(j).getString("IDZapsal"),
                                    response.getJSONObject(j).getString("Jmeno"),
                                    response.getJSONObject(j).getString("MinMax"),
                                    response.getJSONObject(j).getString("Prijmeni"),
                                    response.getJSONObject(j).getString("TitulPred"),
                                    response.getJSONObject(j).getString("TitulZa"),
                                    response.getJSONObject(j).getString("JmenoPosledniZmena"),
                                    response.getJSONObject(j).getString("VysledekRound"),
                                    response.getJSONObject(j).getString("PrumerTrida"),
                                    response.getJSONObject(j).getString("PrumerRocnik")));
                        }
                    }
                } else {
                    Log.d("Chyba", "CourseConditions Data_into_sum_detail_list");
                    nothinglayout.setVisibility(View.VISIBLE);
                }
                for (int i = 0; i < listPom.size(); i++) {
                    for (int j = list.size() - listPom.size(); j < list.size(); j++) {
                        String x = cc_sum_detail_list.get(j).get("idTest");
                        if (x.equals(listPom.get(i))) {
                            cc_sum_detail_list_summary.add(cc_sum_detail_list.get(j));
                            break;
                        }
                    }
                }
                progressDialog.setVisibility(View.GONE);
                adapter.notifyDataSetChanged();
                adapterSummary.notifyDataSetChanged();
            }
        };
        Request.get(URL.StudentSummary.GetSummaryByGroupActivityAndStudent(idSkupinaAktivit, userId), arrayResponse);
        return list;
    }

    private Map<String, String> put_data(String idAktivita, String idTest, String idHodnoceniTest, String nazevAktivita, String imageURL, String datumVlozeni,
                                         String minimum, String maximum, String vysledek, String datumPlatnosti, String posledniZmena, String zapocitat,
                                         String splnil, String idZapsal, String jmeno, String minMax, String prijmeni, String titulPred, String titulZa,
                                         String jmenoPosledniZmena, String vysledekRound, String prumerTrida, String prumerRocnik) {
        HashMap<String, String> item = new HashMap<String, String>();

        item.put("idAktivita", idAktivita);
        item.put("idTest", idTest);
        item.put("idHodnoceniTest", idHodnoceniTest);
        item.put("nazevAktivita", nazevAktivita);
        item.put("imageURL", imageURL);
        item.put("datumVlozeni", datumVlozeni);
        item.put("minimum", minimum);
        item.put("maximum", maximum);
        item.put("vysledek", vysledek);
        item.put("datumPlatnosti", datumPlatnosti);
        item.put("posledniZmena", posledniZmena);
        item.put("zapocitat", zapocitat);
        item.put("splnil", splnil);
        item.put("idZapsal", idZapsal);
        item.put("jmeno", jmeno);
        item.put("minMax", minMax);
        item.put("prijmeni", prijmeni);
        item.put("titulPred", titulPred);
        item.put("titulZa", titulZa);
        item.put("jmenoPosledniZmena", jmenoPosledniZmena);
        item.put("vysledekRound", vysledekRound);
        item.put("prumerTrida", prumerTrida);
        item.put("prumerRocnik", prumerRocnik);

        return item;
    }

    private class SimpleAdapterSum extends SimpleAdapter {

        //ArrayList<Map<String, String>> items;
        //Button btn_listing_logged_students;

        public SimpleAdapterSum(Context activity, ArrayList<Map<String, String>> cc_sum_list, int fragment_course_conditions_sum_list, String[] from, int[] to) {
            super(activity, cc_sum_list, fragment_course_conditions_sum_list, from, to);
            //this.items = cc_sum_list;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = super.getView(position, convertView, parent);

/*
    if(items.get(position).containsKey("formaStudia")) {
        TextView tz = (TextView) view.findViewById(R.id.cc_sum_study_form);
        if (Integer.valueOf(items.get(position).get("formaStudia")) == 0) {
            tz.setText(R.string.study_form_present);
        } else {
            tz.setText(R.string.study_form_combinated);
        }
    }
*/
            return view;
        }

    }
}