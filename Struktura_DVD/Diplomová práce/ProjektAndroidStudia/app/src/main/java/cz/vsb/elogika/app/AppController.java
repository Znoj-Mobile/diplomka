package cz.vsb.elogika.app;

import android.app.Activity;
import android.content.Context;

public class AppController
{
    private static Activity activity;
    private static Context context;


    @Deprecated
    public static Context getContext()
    {
        return AppController.context;
    }
    public static Activity getActivity()
    {
        return AppController.activity;
    }

    public static void setActivity(Activity activity)
    {
        AppController.activity = activity;
        AppController.context = activity.getApplicationContext();
    }
}
