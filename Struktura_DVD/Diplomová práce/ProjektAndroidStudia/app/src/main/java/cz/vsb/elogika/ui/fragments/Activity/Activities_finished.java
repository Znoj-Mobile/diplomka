package cz.vsb.elogika.ui.fragments.Activity;

import android.app.Dialog;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cz.vsb.elogika.R;
import cz.vsb.elogika.communication.ArrayResponse;
import cz.vsb.elogika.communication.Request;
import cz.vsb.elogika.communication.Response;
import cz.vsb.elogika.communication.URL;
import cz.vsb.elogika.model.User;
import cz.vsb.elogika.utils.Logging;
import cz.vsb.elogika.utils.SelectFileToUpload;
import cz.vsb.elogika.utils.TimeLogika;

public class Activities_finished extends Activities {
    static ArrayList<Map<String, String>> list;

    @Override
    protected void initialize() {
        Logging.logAccess("/Pages/Student/Aktivity.aspx", "Solved");
        list = new ArrayList<>();
        list_detail();
        String[] from = {"af_name", "af_group", "af_term_name", "af_inserted", "af_result" };
        int[] to = { R.id.af_name, R.id.af_group, R.id.af_term_name , R.id.af_inserted, R.id.af_result};
        adapter = new SimpleAdapter(getActivity(), list, R.layout.fragment_activities_finished_list, from, to);
    }

    @Override
    protected void list_detail() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                Logging.logAccess("/Pages/Student/AktivitaVyreseneDetail/AktivitaVyreseneDetail.aspx");
                //Toast.makeText(getActivity(), list.get(position).toString(), Toast.LENGTH_SHORT).show();
                final Dialog dialog_f = new Dialog(getActivity());
                dialog_f.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog_f.setContentView(R.layout.fragment_activities_finished_detail);

                TextView af_group_item = (TextView) dialog_f.findViewById(R.id.af_group_item);
                af_group_item.setText(list.get(position).get("af_group"));
                TextView af_name_item = (TextView) dialog_f.findViewById(R.id.af_name_item);
                af_name_item.setText(list.get(position).get("af_name"));
                TextView af_max_item = (TextView) dialog_f.findViewById(R.id.af_max_item);
                af_max_item.setText(list.get(position).get("af_max"));
                TextView af_term_name_item = (TextView) dialog_f.findViewById(R.id.af_term_name_item);
                af_term_name_item.setText(list.get(position).get("af_term_name"));
                TextView af_inserted_item = (TextView) dialog_f.findViewById(R.id.af_inserted_item);
                af_inserted_item.setText(list.get(position).get("af_inserted"));
                TextView af_result_item = (TextView) dialog_f.findViewById(R.id.af_result_item);
                af_result_item.setText(list.get(position).get("af_result"));
                TextView af_result_date_item = (TextView) dialog_f.findViewById(R.id.af_result_date_item);
                af_result_date_item.setText(list.get(position).get("af_result_date"));
                TextView af_description_item = (TextView) dialog_f.findViewById(R.id.af_description_item);
                af_description_item.setText(list.get(position).get("af_description"));

                Button btn_change_activity = (Button) dialog_f.findViewById(R.id.btn_change_activity);
                btn_change_activity.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Dialog dialog_solution = new Dialog(getActivity());
                        dialog_solution.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog_solution.setContentView(R.layout.fragment_activities_solution);
                        //Log.d("test", list.get(position).get("af_solution") + ", " + list.get(position).get("af_note"));
                        final EditText sol_solution = (EditText) dialog_solution.findViewById(R.id.sol_solution);
                        sol_solution.setText(list.get(position).get("af_solution"));
                        final EditText sol_note = (EditText) dialog_solution.findViewById(R.id.sol_note);
                        sol_note.setText(list.get(position).get("af_note"));
                        //komponenta pro zobrazeni nazvu souboru po jeho vybrani...
                        sol_solution_chosen_file = (TextView) dialog_solution.findViewById(R.id.sol_solution_chosen_file);
                        sol_file_name = (TextView) dialog_solution.findViewById(R.id.sol_file_name);
                        Button btn_choose_file_activity = (Button) dialog_solution.findViewById(R.id.btn_choose_file_activity);
                        final Button btn_upload_activity = (Button) dialog_solution.findViewById(R.id.btn_upload_activity);
                        btn_choose_file_activity.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                new SelectFileToUpload(getActivity()) {
                                    @Override
                                    public void selectedFile(String FileName_2, String FileType_2, String FileinBase64_2) {

                                        FileName = FileName_2;
                                        FileType = FileType_2;
                                        byteArray = FileinBase64_2;
                                        btn_upload_activity.setEnabled(true);

                                        //pridani nazvu vybraneho souboru do dialogu...
                                        if (sol_file_name != null) {
                                            sol_file_name.setText(FileName);
                                            sol_file_name.setVisibility(View.VISIBLE);
                                            sol_solution_chosen_file.setVisibility(View.VISIBLE);
                                        }
                                    }
                                };
                            }
                        });
                        btn_upload_activity.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                SnackbarManager.show(
                                        Snackbar.with(getActivity())
                                                .type(SnackbarType.MULTI_LINE)
                                                .duration(Snackbar.SnackbarDuration.LENGTH_INDEFINITE)
                                                .text(getResources().getString(R.string.file_is_uploaded)));
                                change(list.get(position), sol_solution.getText().toString(), sol_note.getText().toString());
                                dialog_solution.dismiss();
                                dialog_f.dismiss();
                            }
                        });
                        dialog_solution.show();
                    }
                });

                Button btn_download_activity = (Button) dialog_f.findViewById(R.id.btn_download_finished_activity);
                if (list.get(position).get("af_description").equals("")) {
                    btn_download_activity.setEnabled(false);
                } else {
                    btn_download_activity.setEnabled(true);
                }
                btn_download_activity.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SnackbarManager.show(
                                Snackbar.with(getActivity())
                                        .type(SnackbarType.MULTI_LINE)
                                        .duration(Snackbar.SnackbarDuration.LENGTH_INDEFINITE)
                                        .text(getResources().getString(R.string.file_is_downloaded)));
                        download(list.get(position).get("af_description"), list.get(position).get("af_idAktivitaVyresene"));
                        dialog_f.dismiss();
                    }
                });
                dialog_f.show();
            }
        });
        listView.setAdapter(adapter);
    }

    @Override
    protected void data_into_list()
    {
        ArrayResponse arrayResponse = new ArrayResponse()
        {
            @Override
            public void onResponse(JSONArray response) throws JSONException
            {
                if (response.length() > 0)
                {
                    for (int j = 0; j < response.length(); j++)
                    {
                        //Log.d("-", "" + response.getJSONObject(j).getString("NazevSkupinaAktivit"));
                        list.add(put_finished_data(
                                response.getJSONObject(j).getString("NazevSkupinaAktivit"),
                                response.getJSONObject(j).getString("NazevAktivita"),
                                response.getJSONObject(j).getString("DoporHodnoceni"),
                                response.getJSONObject(j).getString("NazevTermin"),
                                TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("DatumVlozeni")),
                                response.getJSONObject(j).getString("Hodnoceni"),
                                TimeLogika.getFormatedDateTime(response.getJSONObject(j).getString("HodnoceniDatum")),
                                response.getJSONObject(j).getString("ReseniSouborNazev"),
                                response.getJSONObject(j).getString("ReseniSouborContent"),
                                response.getJSONObject(j).getString("Reseni"),
                                response.getJSONObject(j).getString("Poznamka"),
                                response.getJSONObject(j).getString("IDAktivitaVyresene"),
                                response.getJSONObject(j).getString("IDVlozil"),
                                response.getJSONObject(j).getString("IDTermin"),
                                response.getJSONObject(j).getString("IDAktivita"),
                                response.getJSONObject(j).getString("LoginSkola"),
                                response.getJSONObject(j).getString("LoginSystem"),
                                response.getJSONObject(j).getString("ReseniSoubor"),
                                response.getJSONObject(j).getString("Jmeno"),
                                response.getJSONObject(j).getString("Prijmeni"),
                                response.getJSONObject(j).getString("TitulPred"),
                                response.getJSONObject(j).getString("TitulZa")));
                    }
                }
                else{
                    Log.d("Chyba", "Fragment_Activities Data_into_list");
                    nothinglayout.setVisibility(View.VISIBLE);
                }
                swipeLayout.setRefreshing(false);
                adapter.notifyDataSetChanged();
            }
        };
        Request.get(URL.SolvedActivities.GetAll(User.id, User.courseInfoId), arrayResponse);
    }

    @Override
    protected void clear_list() {
        list.clear();
    }

    protected Map<String, String> put_finished_data(String nazevSkupinaAktivit, String nazevAktivita, String doporHodnoceni, String nazevTermin,
                                                    String datumVlozeni, String hodnoceni, String hodnoceniDatum, String reseniSouborNazev,
                                                    String reseniSouborContent, String reseni, String poznamka, String idAktivitaVyresene,
                                                    String idVlozil, String idTermin, String idAktivita, String loginSkola, String loginSystem,
                                                    String reseniSoubor, String jmeno, String prijmeni, String titulPred, String titulZa) {
        HashMap<String, String> item = new HashMap<>();
        item.put("af_group",nazevSkupinaAktivit);
        item.put("af_name",nazevAktivita);
        item.put("af_max",doporHodnoceni);
        item.put("af_term_name",nazevTermin);
        item.put("af_inserted",datumVlozeni);
        item.put("af_result",hodnoceni);
        item.put("af_result_date",hodnoceniDatum);
        item.put("af_description",reseniSouborNazev);
        item.put("af_description_type",reseniSouborContent);
        item.put("af_solution",reseni);
        item.put("af_note",poznamka);
        item.put("af_idAktivitaVyresene",idAktivitaVyresene);
        item.put("af_idVlozil",idVlozil);
        item.put("af_idTermin",idTermin);
        item.put("af_idAktivita",idAktivita);
        item.put("af_loginSkola",loginSkola);
        item.put("af_loginSystem",loginSystem);
        item.put("af_reseniSoubor",reseniSoubor);
        item.put("af_jmeno",jmeno);
        item.put("af_prijmeni",prijmeni);
        item.put("af_titulPred",titulPred);
        item.put("af_titulZa",titulZa);
        return item;
    }

    private void change(final Map<String, String> item, String solution, String note) {
        Logging.logAccess("/Pages/Student/AktivitaVyreseneDetail/AktivitaVyreseneSummary.aspx");
        Response response = new Response() {
            @Override
            public void onResponse(JSONObject response) throws JSONException
            {
                Log.d("typ response: ", response.getString("Result"));
                if (response.getString("Result").equals("true")) {
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(getActivity().getString(R.string.one_activity) + " " + item.get("af_jmeno")+ " " + getActivity().getString(R.string.activity_was_send)));
                }
                //aktualizace dat
                update();

            }
        };
        JSONObject parameters = new JSONObject();
        try
        {
            parameters.put("IDAktivitaVyresene", item.get("af_idAktivitaVyresene"));
            parameters.put("IDVlozil", item.get("af_idVlozil"));
            parameters.put("IDUzivatel", User.id);
            parameters.put("IDTermin", item.get("af_idTermin"));
            parameters.put("IDAktivita", item.get("af_idAktivita"));
            parameters.put("Hodnoceni", item.get("af_result"));
            parameters.put("HodnoceniDatum", item.get("af_result_date"));
            parameters.put("DatumVlozeni", TimeLogika.currentServerTime());
            parameters.put("LoginSkola", item.get("af_loginSkola"));
            parameters.put("LoginSystem", item.get("af_loginSystem"));
            parameters.put("Jmeno", item.get("af_jmeno"));
            parameters.put("Prijmeni", item.get("af_prijmeni"));
            parameters.put("TitulPred", item.get("af_titulPred"));
            parameters.put("TitulZa", item.get("af_titulZa"));
            parameters.put("NazevAktivita", item.get("af_name"));
            parameters.put("DoporHodnoceni", item.get("af_max"));
            parameters.put("NazevTermin", item.get("af_term_name"));
            parameters.put("NazevSkupinaAktivit", item.get("af_group"));


            parameters.put("Reseni", solution);
            parameters.put("Poznamka", note);

            if(byteArray != null){
                parameters.put("ReseniSouborNazev", FileName);
                parameters.put("ReseniSouborContent", FileType);

                //Log.d("file", Base64.encodeToString(byteArray, Base64.DEFAULT));
                parameters.put("ReseniSoubor", byteArray);
                //parameters.put("ReseniSoubor", item.get("ReseniSoubor"));
            }
            else{
                parameters.put("ReseniSouborNazev", item.get("af_description"));
                parameters.put("ReseniSouborContent", item.get("af_description_type"));
                //soubor se musi dostat pomoci api... pochopitelne
                parameters.put("ReseniSoubor", item.get("ReseniSoubor"));
            }
        }
        catch (JSONException e) {}

        swipeLayout.post(new Runnable() {
            @Override
            public void run() {
                nothinglayout.setVisibility(View.GONE);
                swipeLayout.setRefreshing(true);
            }
        });
        Request.post(URL.SolvedActivities.Post(), parameters, response);
    }
}
